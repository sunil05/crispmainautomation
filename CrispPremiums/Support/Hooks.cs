﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using System.Collections.ObjectModel;
using System.Diagnostics;
using OpenQA.Selenium.Support.UI;
using System.Threading;
using System.Collections.Generic;
using TechTalk.SpecFlow;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Interactions;
using AventStack.ExtentReports;
using System.IO;
using AventStack.ExtentReports.Gherkin.Model;
using System.Configuration;
using AventStack.ExtentReports.Reporter;
using System.Reflection;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.IE;
using RegressionPacks.Pages;

namespace RegressionPacks.Support
{
    [Binding]
    public class Hooks
    {
        public static RemoteWebDriver Driver = null;
        public IWebElement elementLocator;
        private static ExtentReports extent;
        private static ExtentTest featureName;
        private static ExtentTest scenario;

        [BeforeScenario]
        public void Initialisedriver()
        {
            Driver = Driver ?? StartDriver(ConfigurationManager.AppSettings["Browser"]);
            ScenarioContext.Current.Set(Driver, "currentDriver");    
            scenario = extent.CreateTest<Scenario>(ScenarioContext.Current.ScenarioInfo.Title);
            scenario = featureName.CreateNode<Scenario>(ScenarioContext.Current.ScenarioInfo.Title);
        }
        [AfterScenario]
        public void AfterScenario()
        {
            try
            {
                Exception lastError = ScenarioContext.Current.TestError;
                if (lastError != null)
                { // CaptureScreenShot();
                    Screenshot ss = ((ITakesScreenshot)Driver).GetScreenshot();
                    string title = ScenarioContext.Current.ScenarioInfo.Title;
                    string Runname = title + DateTime.Now.ToString("yyyy-MM-dd-HH_mm_ss");
                    string sceenshotPath = new FileInfo(ExtensionMethods.GetAbsolutePath(@"TestData\ErrorScreenshot\")).ToString();

                    //string sceenshotPath = @"\\tpgfile\NewCompany\IT\Department\QA Team\Current Projects\CRISP\CrispAutomation\Screenshots\";

                    string drive = Path.GetPathRoot(sceenshotPath);
                    if (!Directory.Exists(drive))
                    {
                        string localPath = @"\\tpgfile\NewCompany\IT\Department\QA Team\Current Projects\CRISP\CrispAutomation\Screenshots\";
                        ss.SaveAsFile(localPath + Runname + ".jpg", ScreenshotImageFormat.Jpeg);
                        //return localPath;
                    }
                    else
                    {
                        string screenshotfilename = sceenshotPath + Runname + ".jpg";
                        ss.SaveAsFile(screenshotfilename, ScreenshotImageFormat.Jpeg);
                        string urlfile = "http://storage/screenshots/" + Runname + ".jpg";
                        Console.WriteLine("" + urlfile);

                        //return screenshotfilename;
                        //Console.WriteLine(" ");
                    }
                }
            }
            catch
            {
                //Console.WriteLine("catch");   
            }
            finally
            {
                ScenarioContext.Current.Get<RemoteWebDriver>("currentDriver").Quit();
                Driver = null;
                ScenarioContext.Current.Clear();
               // log.Info($"Test Scenario Started For : {ScenarioContext.Current.ScenarioInfo}");
                if (Driver != null)
                {
                    Driver?.Quit();
                    Driver?.Close();
                    Driver?.Dispose();
                    Driver = null;
                    extent.Flush();
                }
            }

        }

        [AfterTestRun]
        public static void StopSeleniumAfterAllTests()
        {
            Driver?.Close();
            Driver?.Quit();
            Driver?.Dispose();
            extent.Flush();
        }

        [BeforeTestRun]
        public static void IntialiseReport()
        {
            //var htmlReporter = new ExtentHtmlReporter(@"C:\Crisp\crisp.automation\CrispAutomation\Reports\ExtentReports.html");
            var fileInfo = new FileInfo(ExtensionMethods.GetAbsolutePath(@"TestData\ExtentReports\ExtentReports.html"));
            var htmlReporter = new ExtentHtmlReporter(fileInfo.ToString());
            htmlReporter.Configuration().Theme = AventStack.ExtentReports.Reporter.Configuration.Theme.Dark;
            extent = new ExtentReports();
            extent.AttachReporter(htmlReporter);
        }

        [BeforeFeature]
        public static void BeforeFeature()
        {
            featureName = extent.CreateTest<Feature>(FeatureContext.Current.FeatureInfo.Title);
        }
        [AfterStep]
        public static void InsertReportingSteps()
        {
            Exception lastError = ScenarioContext.Current.TestError;
            var stepType = ScenarioStepContext.Current.StepInfo.StepDefinitionType.ToString();
            PropertyInfo pInfo = typeof(ScenarioContext).GetProperty("TestStatus", BindingFlags.Instance | BindingFlags.NonPublic);        

            if (lastError == null)
            {

                if (stepType == "Given")
                    scenario.CreateNode<Given>(ScenarioStepContext.Current.StepInfo.Text);
                if (stepType == "When")
                    scenario.CreateNode<When>(ScenarioStepContext.Current.StepInfo.Text);
                if (stepType == "And")
                    scenario.CreateNode<And>(ScenarioStepContext.Current.StepInfo.Text);
                if (stepType == "Then")
                    scenario.CreateNode<Then>(ScenarioStepContext.Current.StepInfo.Text);

            }
            else
            if (lastError != null)
            {
                var stacktrase = "<pre>" + lastError.StackTrace + "<pre>";
                //  string screenshotPath = CaptureScreenShot.Capture(Driver, "ScreenshotName");
                var stepInfo = ScenarioContext.Current.StepContext.StepInfo;
                var TestError = ScenarioContext.Current.TestError.InnerException;
                var stepDescription = stepInfo.StepDefinitionType + stepInfo.Text + TestError;
           
                if (stepType == "Given")
                    scenario.CreateNode<Given>(ScenarioStepContext.Current.StepInfo.Text).Fail(stepDescription);
                if (stepType == "When")
                    scenario.CreateNode<When>(ScenarioStepContext.Current.StepInfo.Text).Fail(stepDescription);
                if (stepType == "And")
                    scenario.CreateNode<And>(ScenarioStepContext.Current.StepInfo.Text).Fail(stepDescription);
                if (stepType == "Then")
                    scenario.CreateNode<Then>(ScenarioStepContext.Current.StepInfo.Text).Fail(stepDescription);
                
            }
         
        }
      
        private RemoteWebDriver StartDriver(string browser)
        {
            RemoteWebDriver driver = null;

            switch (browser)
            {
                case "chrome":
                    //var fileInfo = new FileInfo(ExtensionMethods.GetAbsolutePath(@"TestData"));
                    var fileInfo = new FileInfo(ExtensionMethods.GetAbsolutePath(@"TestData\Downloads"));
                    DirectoryInfo downloadFolder = new DirectoryInfo($"{fileInfo}");
                    String DownloadfilePath = downloadFolder.ToString();
                    Statics.DownloadFolder = DownloadfilePath;
                    var chromeOptions = new ChromeOptions();
                    chromeOptions.AddUserProfilePreference("download.default_directory", DownloadfilePath);
                    chromeOptions.AddUserProfilePreference("intl.accept_languages", "nl");
                    chromeOptions.AddUserProfilePreference("disable-popup-blocking", "true");
                    driver = new ChromeDriver(chromeOptions);
                    driver.Navigate().GoToUrl(ConfigurationManager.AppSettings["CrispUrl"]);
                    driver.Manage().Window.Maximize();

                    break;

                case "ie":
                    driver = new InternetExplorerDriver();
                    driver.Navigate().GoToUrl("http://crisp-dev-vip-a.mdisdev.local:65035/");
                    driver.Manage().Window.Maximize();

                    break;

                default:
                    driver = new ChromeDriver();
                    driver.Navigate().GoToUrl(ConfigurationManager.AppSettings["CrispUrl"]);
                    driver.Manage().Window.Maximize();
                    break;                   

            }
            return driver;
        }
    }
}
