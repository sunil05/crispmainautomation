﻿using RegressionPacks.Pages;

namespace RegressionPacks.Support
{
    public class Pages : Base
    {
        public static LoginPage Loginpage
        {
            get
            {
                var login = new LoginPage(Driver);
                return login;
            }
        }
        public static DashboardPage Dashboardpage
        {
            get
            {
                var dashboard = new DashboardPage(Driver);
                return dashboard;
            }
        }
        public static AddLABCQuotePage AddLABCQuotePage
        {
            get
            {
                var addlabcquotepage = new AddLABCQuotePage(Driver);
                return addlabcquotepage;
            }
        }
        public static AddPGQuotePage AddPGQuotePage
        {
            get
            {
                var addpgquotepage = new AddPGQuotePage(Driver);
                return addpgquotepage;
            }
        }
        public static AdditionalMethodsPage AdditionalMethodsPage
        {
            get
            {
                var addtionalMethods = new AdditionalMethodsPage(Driver);
                return addtionalMethods;
            }
        }

        public static SendQuotePage SendQuotePage
        {
            get
            {
                var sendquotepage = new SendQuotePage(Driver);
                return sendquotepage;
            }
        }
        public static AcceptQuotePage AcceptQuotePage
        {
            get
            {
                var acceptquotepage = new AcceptQuotePage(Driver);
                return acceptquotepage;
            }
        }
        public static LABCPremiums LABCPremiums
        {
            get
            {
                var lABCPremiums = new LABCPremiums(Driver);
                return lABCPremiums;
            }
        }
        public static PGPremiums PGPremiums
        {
            get
            {
                var pGPremiums = new PGPremiums(Driver);
                return pGPremiums;
            }
        }
        public static PremiumCovers PremiumCovers
        {
            get
            {
                var premiumCovers = new PremiumCovers(Driver);
                return premiumCovers;
            }
        }       
        public static CommercialPremiums CommercialPremiums
        {
            get
            {
                var commercialPremiums = new CommercialPremiums(Driver);
                return commercialPremiums;
            }
        }        
        public static CompletedHousingPremiums CompletedHousingPremiums
        {
            get
            {
                var completedHousingPremiums = new CompletedHousingPremiums(Driver);
                return completedHousingPremiums;
            }
        }
        public static SelfBuildPremiums SelfBuildPremiums
        {
            get
            {
                var selfBuildPremiums = new SelfBuildPremiums(Driver);
                return selfBuildPremiums;
            }
        }
        public static NewhomesPremiums NewhomesPremiums
        {
            get
            {
                var newhomesPremiums = new NewhomesPremiums(Driver);
                return newhomesPremiums;
            }
        }
        public static SocialHousingPremiums SocialHousingPremiums
        {
            get
            {
                var socialHousingPremiums = new SocialHousingPremiums(Driver);
                return socialHousingPremiums;
            }
        }
       
        public static PrivateRentalPremiums PrivateRentalPremiums
        {
            get
            {
                var privateRentalPremiums = new PrivateRentalPremiums(Driver);
                return privateRentalPremiums;
            }
        }
        public static ResidentialBCPremiums ResidentialBCPremiums
        {
            get
            {
                var residentialBCPremiums = new ResidentialBCPremiums(Driver);
                return residentialBCPremiums;
            }
        }
        public static CommercialBCPremiums CommercialBCPremiums
        {
            get
            {
                var commercialBCPremiums = new CommercialBCPremiums(Driver);
                return commercialBCPremiums;
            }
        }
        public static ChannelIslandsCommercialPremiums ChannelIslandsCommercialPremiums
        {
            get
            {
                var channelIslandsCommercialPremiums = new ChannelIslandsCommercialPremiums(Driver);
                return channelIslandsCommercialPremiums;
            }
        }
        public static ChannelIslandsResidentialPremiums ChannelIslandsResidentialPremiums
        {
            get
            {
                var channelIslandsResidentialPremiums = new ChannelIslandsResidentialPremiums(Driver);
                return channelIslandsResidentialPremiums;
            }
        }
        public static LABCSinglePlotPremiums LABCSinglePlotPremiums
        {
            get
            {
                var lABCSinglePlotPremiums = new LABCSinglePlotPremiums(Driver);
                return lABCSinglePlotPremiums;
            }
        }     
        public static PGSinglePlotPremiums PGSinglePlotPremiums
        {
            get
            {
                var pGSinglePlotPremiums = new PGSinglePlotPremiums(Driver);
                return pGSinglePlotPremiums;
            }
        }
        public static PGHVSPremiums PGHVSPremiums
        {
            get
            {
                var pGHVSPremiums = new PGHVSPremiums(Driver);
                return pGHVSPremiums;
            }
        }
        public static LABCHVSPremiums LABCHVSPremiums
        {
            get
            {
                var lABCHVSPremiums = new LABCHVSPremiums(Driver);
                return lABCHVSPremiums;
            }
        }       
       
        public static SecondaryLayerPremiums SecondaryLayerPremiums
        {
            get
            {
                var secondaryLayerPremiums = new SecondaryLayerPremiums(Driver);
                return secondaryLayerPremiums;
            }
        }

        public static LABCHVSAvivaPremiums LABCHVSAvivaPremiums
        {
            get
            {
                var lABCHVSAvivaPremiums = new LABCHVSAvivaPremiums(Driver);
                return lABCHVSAvivaPremiums;
            }
        }
        public static PGHVSAvivaPremiums PGHVSAvivaPremiums
        {
            get
            {
                var pGHVSAvivaPremiums = new PGHVSAvivaPremiums(Driver);
                return pGHVSAvivaPremiums;
            }
        }
        public static NewhomesHVSPremiums NewhomesHVSPremiums
        {
            get
            {
                var newhomesHVSPremiums = new NewhomesHVSPremiums(Driver);
                return newhomesHVSPremiums;
            }
        }
        public static SocialHousingHVSPremiums SocialHousingHVSPremiums
        {
            get
            {
                var socialHousingHVSPremiums = new SocialHousingHVSPremiums(Driver);
                return socialHousingHVSPremiums;
            }
        }
        public static SocialHousingHVSAvivaPremiums SocialHousingHVSAvivaPremiums
        {
            get
            {
                var socialHousingHVSAvivaPremiums = new SocialHousingHVSAvivaPremiums(Driver);
                return socialHousingHVSAvivaPremiums;
            }
        }
        public static NewHomesHVSAvivaPremiums NewHomesHVSAvivaPremiums
        {
            get
            {
                var newHomesHVSAvivaPremiums = new NewHomesHVSAvivaPremiums(Driver);
                return newHomesHVSAvivaPremiums;
            }
        }
        public static CommercialHVSPremiums CommercialHVSPremiums
        {
            get
            {
                var commercialHVSPremiums = new CommercialHVSPremiums(Driver);
                return commercialHVSPremiums;
            }
        }
        public static CommercialHVSAvivaPremiums CommercialHVSAvivaPremiums
        {
            get
            {
                var commercialHVSAvivaPremiums = new CommercialHVSAvivaPremiums(Driver);
                return commercialHVSAvivaPremiums;
            }
        }
        public static PrivateRentalHVSPremiums PrivateRentalHVSPremiums
        {
            get
            {
                var privateRentalHVSPremiums = new PrivateRentalHVSPremiums(Driver);
                return privateRentalHVSPremiums;
            }
        }
        public static PrivateRentalHVSAvivaPremiums PrivateRentalHVSAvivaPremiums
        {
            get
            {
                var privateRentalHVSAvivaPremiums = new PrivateRentalHVSAvivaPremiums(Driver);
                return privateRentalHVSAvivaPremiums;
            }
        }
        public static ChannelIslandsResidentialHVSPremiums ChannelIslandsResidentialHVSPremiums
        {
            get
            {
                var channelIslandsResidentialHVSPremiums = new ChannelIslandsResidentialHVSPremiums(Driver);
                return channelIslandsResidentialHVSPremiums;
            }
        }
        public static ChannelIslandsResidentialHVSAvivaPremiums ChannelIslandsResidentialHVSAvivaPremiums
        {
            get
            {
                var channelIslandsResidentialHVSAvivaPremiums = new ChannelIslandsResidentialHVSAvivaPremiums(Driver);
                return channelIslandsResidentialHVSAvivaPremiums;
            }
        }
        public static ChannelIslandsCommercialHVSPremiums ChannelIslandsCommercialHVSPremiums
        {
            get
            {
                var channelIslandsCommercialHVSPremiums = new ChannelIslandsCommercialHVSPremiums(Driver);
                return channelIslandsCommercialHVSPremiums;
            }
        }
        public static ChannelIslandsCommercialHVSAvivaPremiums ChannelIslandsCommercialHVSAvivaPremiums
        {
            get
            {
                var channelIslandsCommercialHVSAvivaPremiums = new ChannelIslandsCommercialHVSAvivaPremiums(Driver);
                return channelIslandsCommercialHVSAvivaPremiums;
            }
        }
    }
}

