﻿using ExcelDataReader;
using OfficeOpenXml;
using RegressionPacks.Pages;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RegressionPacks.Support
{
    public class ExcelPlay
    {
        //Reading the Data from master plot schedule sheet using Excel Data Reader 
        public static DataTable ExcelToDataTable(string masterplotdata)
        {

            //string masterfilepath = @"C:\Users\KumarS\Desktop\Temp\NewHomes-Plot Schedule.xlsx";
            //open file and returns as Stream
            var result = new DataSet();
            using (var stream = File.Open(masterplotdata, FileMode.Open, FileAccess.Read))
            using (var excelReader = ExcelReaderFactory.CreateOpenXmlReader(stream)) //.xlsx
            {
                //Createopenxmlreader via ExcelReaderFactory

                //Set the First Row as Column Name

                //Return as DataSet
                result = excelReader.AsDataSet(new ExcelDataSetConfiguration()
                {
                    ConfigureDataTable = (_) => new ExcelDataTableConfiguration()
                    {
                        UseHeaderRow = true
                    }
                });
            }
            //Get all the Tables
            DataTableCollection table = result.Tables;
            //Store it in DataTable
            DataTable resultTable = table["Plots"];
            //return
            return resultTable;
        }



        ////Creating Plot Schedule Sheet To Create a quote 
        public static ExcelPackage CreateSpreadsheet(DataRow plotData)
        {
            var excel = new ExcelPackage();
            var worksheet = excel.Workbook.Worksheets.Add("Plots");
            worksheet.Protection.IsProtected = true;
            //worksheet.InsertColumn();
            worksheet.Cells["A1"].Value = "Plot Name";
            worksheet.Cells["B1"].Value = "Warranty Product";
            worksheet.Cells["C1"].Value = "Building Control Product";
            worksheet.Cells["D1"].Value = "New Build/Conversion";
            worksheet.Cells["E1"].Value = "Unit Type";
            worksheet.Cells["F1"].Value = "Stage of Works";
            worksheet.Cells["G1"].Value = "Reconstruction Cost";
            worksheet.Cells["H1"].Value = "Estimated Sale Price";
            worksheet.Cells["I1"].Value = "Block Name";
            worksheet.Cells["J1"].Value = "Sq m";
            worksheet.Cells["K1"].Value = "Address 1";
            worksheet.Cells["L1"].Value = "Address 2";
            worksheet.Cells["M1"].Value = "Address 3";
            worksheet.Cells["N1"].Value = "Town";
            worksheet.Cells["O1"].Value = "Postcode";
            worksheet.Cells["P1"].Value = "Country";
            worksheet.Cells["Q1"].Value = "Confirmed Sales Price";

            worksheet.Cells["A2"].Value = plotData.ItemArray[0];
            worksheet.Cells["B2"].Value = plotData.ItemArray[1];
            worksheet.Cells["C2"].Value = plotData.ItemArray[2];
            worksheet.Cells["D2"].Value = plotData.ItemArray[3];
            worksheet.Cells["E2"].Value = plotData.ItemArray[4];
            worksheet.Cells["F2"].Value = plotData.ItemArray[5];
            worksheet.Cells["G2"].Value = plotData.ItemArray[6];
            worksheet.Cells["H2"].Value = plotData.ItemArray[7];
            worksheet.Cells["I2"].Value = plotData.ItemArray[8];
            worksheet.Cells["J2"].Value = plotData.ItemArray[9];
            worksheet.Cells["K2"].Value = plotData.ItemArray[10];
            worksheet.Cells["L2"].Value = plotData.ItemArray[11];
            worksheet.Cells["M2"].Value = plotData.ItemArray[12];
            worksheet.Cells["N2"].Value = plotData.ItemArray[13];
            worksheet.Cells["O2"].Value = plotData.ItemArray[14];
            worksheet.Cells["P2"].Value = plotData.ItemArray[15];
            worksheet.Cells["Q2"].Value = plotData.ItemArray[16];

            var s = $"Creating Quote for :  {worksheet.Cells["B2"].Value},{worksheet.Cells["D2"].Value},{worksheet.Cells["F2"].Value},{worksheet.Cells["G2"].Value},{worksheet.Cells["H2"].Value}";
            Console.WriteLine(s);

            var warrentyproduct = worksheet.Cells["B2"].Value;
            var bcProduct = worksheet.Cells["C2"].Value;
            var constructionType = worksheet.Cells["D2"].Value;
            var unitType = worksheet.Cells["E2"].Value;
            var stagesOfWorks = worksheet.Cells["F2"].Value;
            var reconstructionCost = worksheet.Cells["G2"].Value;
            var estimatedSalesPrice = worksheet.Cells["H2"].Value;
            var sqmeter = worksheet.Cells["J2"].Value;

            string singleplotdata = $@"\\tpgfile\NewCompany\IT\Department\QA Team\Current Projects\CRISP\CrispAutomation\CrispData\Premiums\PlotData\{warrentyproduct}.xlsx";

            excel.SaveAs(new FileInfo(singleplotdata));
         
            Premiums.PlotData = singleplotdata;

            //string masterfilepath = @"C:\Users\KumarS\Desktop\Temp\NewHomes-Plot Schedule.xlsx";
            //open file and returns as Stream
            var result = new DataSet();
            using (var stream = File.Open(singleplotdata, FileMode.Open, FileAccess.Read))
            using (var excelReader = ExcelReaderFactory.CreateOpenXmlReader(stream)) //.xlsx
            {
                //Createopenxmlreader via ExcelReaderFactory

                //Set the First Row as Column Name

                //Return as DataSet
                result = excelReader.AsDataSet(new ExcelDataSetConfiguration()
                {
                    ConfigureDataTable = (_) => new ExcelDataTableConfiguration()
                    {
                        UseHeaderRow = true
                    }
                });
            }
            //Get all the Tables
            DataTableCollection table = result.Tables;
            //Store it in DataTable
            DataTable resultTable = table["Plots"];

            Premiums.eachplotrow = new List<plotrows>();

            foreach (var row in resultTable.AsEnumerable())
            {
                Premiums.eachplotrow.Add(
                    new RegressionPacks.Pages.plotrows
                    {
                        Plots = row[0].ToString(),
                        ProductName = row[1].ToString(),
                        BCProduct = row[2].ToString(),
                        ConstructionType = row[3].ToString(),
                        UnitType = row[4].ToString(),
                        StageOfWork = row[5].ToString(),
                        ReconstructionCostValue = Convert.ToDouble(row[6].ToString()),
                        EstrimatedSalesPrice = Convert.ToDouble(row[7].ToString()),
                        SqMValue = Convert.ToDouble((row[9]).ToString()),
                    });
            }


            //Store Warrenty Product Name List

            ////  Premiums.PremiumsProductName = warrentyproduct.ToString();
            //  List<string> temp = new List<string>();
            //  temp.Add(warrentyproduct.ToString());
            //Get all the Tables       



            //Store Single Warrenty Product Name 
            Premiums.ProductName = warrentyproduct.ToString();

            //Store Building Control Provide Value 

            Premiums.BCProduct = bcProduct.ToString();

            // Store Construction type

            Premiums.ConstructionType = constructionType.ToString();

            //Store Unit Type 

            Premiums.UnitType = unitType.ToString();            

            //Store Stages Of Works 

            Premiums.StageOfWork = stagesOfWorks.ToString();

            //Store ReconstructionCost Price 

            Premiums.ReconstructionCost = Convert.ToDouble(reconstructionCost);

            //Store Estimated Sales Price 

            Premiums.EstimatedSalePrice = Convert.ToDouble(estimatedSalesPrice);          

            //Store Square Meter

            Premiums.SqMeter = sqmeter.ToString();

            return excel;
        }

        //Reading the Data from master plot schedule sheet using Excel Data Reader 
        public static void ReadExcelPlotData(string masterplotdata)
        {

            //string masterfilepath = @"C:\Users\KumarS\Desktop\Temp\NewHomes-Plot Schedule.xlsx";
            //open file and returns as Stream
            var result = new DataSet();
            using (var stream = File.Open(masterplotdata, FileMode.Open, FileAccess.Read))
            using (var excelReader = ExcelReaderFactory.CreateOpenXmlReader(stream)) //.xlsx
            {
                //Createopenxmlreader via ExcelReaderFactory

                //Set the First Row as Column Name

                //Return as DataSet
                result = excelReader.AsDataSet(new ExcelDataSetConfiguration()
                {
                    ConfigureDataTable = (_) => new ExcelDataTableConfiguration()
                    {
                        UseHeaderRow = true
                    }
                });
            }
            //Get all the Tables
            DataTableCollection table = result.Tables;
            //Store it in DataTable
            DataTable resultTable = table["Plots"];

            Premiums.eachplotrow = new List<plotrows>();

            foreach (var row in resultTable.AsEnumerable())
            {
                Premiums.eachplotrow.Add(
                    new RegressionPacks.Pages.plotrows
                    {
                        Plots = row[0].ToString(),
                        ProductName = row[1].ToString(),
                        BCProduct = row[2].ToString(),
                        ConstructionType = row[3].ToString(),
                        UnitType = row[4].ToString(),
                        StageOfWork = row[5].ToString(),
                        ReconstructionCostValue = Convert.ToDouble(row[6].ToString()),
                        EstrimatedSalesPrice = Convert.ToDouble(row[7].ToString()),
                        SqMValue = Convert.ToDouble((row[9]).ToString()),
                    });
            }
  

            var ProduName = resultTable.DefaultView
     .ToTable(true, "Warranty Product")
     .Rows
     .Cast<DataRow>()
     .Select(row => row["Warranty Product"])
     .ToList();
            Premiums.ProductNames = ProduName.Select(i => i.ToString()).ToList();


            var BCProduNamelist = resultTable.DefaultView
  .ToTable(true, "Building Control Product")
  .Rows
  .Cast<DataRow>()
  .Select(row => row["Building Control Product"])
  .ToList();
            Premiums.BCProducts = BCProduNamelist.Select(i => i.ToString()).ToList();

            var ConstructionTypelist = resultTable.DefaultView
.ToTable(true, "New Build/Conversion")
.Rows
.Cast<DataRow>()
.Select(row => row["New Build/Conversion"])
.ToList();
            Premiums.ConstructionTypes = ConstructionTypelist.Select(i => i.ToString()).ToList();

            var UnitTypelist = resultTable.DefaultView
.ToTable(true, "Unit Type")
.Rows
.Cast<DataRow>()
.Select(row => row["Unit Type"])
.ToList();
            Premiums.UnitTypes = UnitTypelist.Select(i => i.ToString()).ToList();

            var Works = resultTable.DefaultView
.ToTable(true, "Stage of Works")
.Rows
.Cast<DataRow>()
.Select(row => row["Stage of Works"])
.ToList();
            Premiums.StageOfWorks = Works.Select(i => i.ToString()).ToList();


        }
    }
}

