﻿using OpenQA.Selenium;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.PageObjects;
using RegressionPacks.Support;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using ExcelDataReader;

namespace RegressionPacks.Pages
{
    public class PrivateRentalHVSPremiums : Support.Pages
    {
        public string RatingValue;
        public IWebDriver wdriver;
        double iptCaluclationValue = 0.12;
        double vatCaluclationValue = 0.20;
        double BasePremium = 0;
        double LossOfRent = 0;
        double TAFee = 0;
        int RatingColumn;
        double AtkinsPremium = 0;
        double MinimumAtkinsFee = 0;
        DataTable excelTable;
        double InsolvencyPremium = 0;

        public PrivateRentalHVSPremiums(ISearchContext driver)
        {
            PageFactory.InitElements(driver, this);
            wdriver = (RemoteWebDriver)driver;
        }
        public void CoversOnPrivateRentalHVSProduct()
        {

            if (Premiums.eachplotrow.Any(o => o.ProductName.Contains("Private Rental - High Value")))
            {
                Premiums.ProductsCovers = PremiumCovers.ProductCoversList.Select(i => i.Text.ToString()).ToList();
                Premiums.ProductServices = PremiumCovers.ProductServicesList.Select(i => i.Text.ToString()).ToList();

                if (Premiums.ProductsCovers.Any(x => x.Contains("Defects")))
                {
                    Premiums.DefectsCover = PremiumCovers.DefectsCover.Count > 0 ? "Yes" : "No";
                }
                if (Premiums.ProductsCovers.Any(x => x.Contains("Contaminated Land")))
                {
                    Premiums.ContaminatedLandCover = PremiumCovers.ContaminatedLandList.Count > 0 ? "Yes" : "No";
                }
                if (Premiums.ProductsCovers.Any(x => x.Contains("Additional Cover for Local Authority Building Control Function")))
                {
                    Premiums.LocalAuthorityBuildingControlFunctionCover = PremiumCovers.LocalAuthorityBuildingControlFunctionCover.Count > 0 ? "Yes" : "No";
                }
                if (Premiums.ProductsCovers.Any(x => x.Contains("Insolvency of the Builder")))
                {
                    Premiums.InsolvencyCover = PremiumCovers.InsolvencyOfBuilderCover.Count > 0 ? "Yes" : "No";
                }
                if (Premiums.ProductsCovers.Any(x => x.Contains("Loss of Rent")))
                {
                    Premiums.LossOfRentCover = PremiumCovers.LossOfRentCover.Count > 0 ? "Yes" : "No";
                }
                if (Premiums.ProductsCovers.Any(x => x.Contains("Plastering")))
                {
                    Premiums.PlasteringCover = PremiumCovers.PlasteringCover.Count > 0 ? "Yes" : "No";
                }
                if (Premiums.ProductsCovers.Any(x => x.Contains("Sound Transmission")))
                {
                    Premiums.SoundTransmissionCover = PremiumCovers.SoundTransmissionCover.Count > 0 ? "Yes" : "No";
                }
                //Services

                if (Premiums.ProductServices.Any(x => x.Contains("Technical Audit Fee")))
                {
                    Premiums.ProductTechnicalAuditFeeService = PremiumCovers.ProductTechnicalAuditFee.Count > 0 ? "Yes" : "No";
                }
                if (Premiums.ProductServices.Any(x => x.Contains("Administration Fee")))
                {
                    Premiums.ProductAdminFeeService = PremiumCovers.ProductAdministrationFee.Count > 0 ? "Yes" : "No";
                }
                if (Premiums.ProductServices.Any(x => x.Contains("Atkins Fee")))
                {
                    Premiums.ProductAtkinsFeeService = PremiumCovers.ProductAtkinsFee.Count > 0 ? "Yes" : "No";
                }
                if (Premiums.ProductServices.Any(x => x.Contains("Cancellation Fee")))
                {
                    Premiums.ProductCancellationFeeService = PremiumCovers.ProductCancellationFee.Count > 0 ? "Yes" : "No";
                }
            }
        }
        //Caluclating Premiums For Each Row 
        public void PrivateRentalHVSPremiumsCaluclations()
        {
            Premiums.StructuralFee = 0;
            Premiums.DefectsFee = 0;
            Premiums.ContaminatedLandFee = 0;
            Premiums.LocalAuthorityBuildingControlFee = 0;
            Premiums.InsolvencyFee = 0;
            Premiums.LossOfRentFee = 0;            
            Premiums.PlasteringFee = 0;
            Premiums.SoundTransmissionFee = 0;
            Premiums.TechnicalAuditFee = 0;
            Premiums.AdministrationFee = 0;
            Premiums.CancellationFee = 0;
            Premiums.AtkinsFee = 0;
            var aggregatedPremium = new PrivateRentalHVSPremium();
            foreach (var plot in Premiums.eachplotrow)
            {
                var calculatedPremiums = PrivateRentalsHVSPremiumCaluclationsForEachPlotRow(plot);
                Premiums.StructuralFee += calculatedPremiums.structuralFee;
                Premiums.DefectsFee += calculatedPremiums.defectsFee;
                Premiums.ContaminatedLandFee += calculatedPremiums.contaminatedLandFee;
                Premiums.LocalAuthorityBuildingControlFee += calculatedPremiums.laBuldingControlFunctionFee;
                Premiums.InsolvencyFee += calculatedPremiums.insolvencyFee;
                Premiums.LossOfRentFee += calculatedPremiums.lossOfRentFee;
                Premiums.PlasteringFee += calculatedPremiums.plasteringFee;
                Premiums.SoundTransmissionFee += calculatedPremiums.soundTransmissionFee;
                //Services 
                Premiums.TechnicalAuditFee += calculatedPremiums.technicalFee;
                Premiums.AdministrationFee += calculatedPremiums.administrationFee;
                Premiums.CancellationFee += calculatedPremiums.cancellationFee;
                Premiums.AtkinsFee = calculatedPremiums.atkinsFee;
            }

            if (Premiums.AtkinsFee < 3000)
            {
                MinimumAtkinsFeeCaluclations();
                Premiums.AtkinsFee = Convert.ToDouble(MinimumAtkinsFee);
            }

            //IPT caluclations 
            Premiums.StructuralIPTFee = Convert.ToDouble(Premiums.StructuralFee * iptCaluclationValue);
            Premiums.DefectsIPTFee = Convert.ToDouble(Premiums.DefectsFee * iptCaluclationValue);
            Premiums.ContaminatedLandIPTFee = Convert.ToDouble(Premiums.ContaminatedLandFee * iptCaluclationValue);
            Premiums.LocalAuthorityBuildingControlIPTFee = Convert.ToDouble(Premiums.LocalAuthorityBuildingControlFee * iptCaluclationValue);
            Premiums.LossOfRentIPTFee = Convert.ToDouble(Premiums.LossOfRentFee * iptCaluclationValue);
            Premiums.InsolvencyIPTFee = Convert.ToDouble(Premiums.InsolvencyFee * iptCaluclationValue);
            Premiums.PlasteringIPTFee = Convert.ToDouble(Premiums.PlasteringFee * iptCaluclationValue);
            Premiums.SoundTransmissionIPTFee = Convert.ToDouble(Premiums.SoundTransmissionFee * iptCaluclationValue);
            //VAT Calucaltions 
            Premiums.TechnicalAuditVATFee = Convert.ToDouble(0.00);
            Premiums.AdministrationVATFee = Convert.ToDouble(0.00);
            Premiums.CancellationVATFee = Convert.ToDouble(0.00);
            Premiums.AtkinsVATFee = Convert.ToDouble(Premiums.AtkinsFee * iptCaluclationValue);
            VerfiyPrivateRentalHVSUIValues();
            if (Premiums.eachplotrow.Any(o => o.BCProduct.Contains("Residential Building Control")))
            {
                ResidentialBCPremiums.ResidentialBCPremiumNewPricesCalcs();
            }
        }

        public class PrivateRentalHVSPremium
        {
            public double defectsFee;

            public double structuralFee;

            public double contaminatedLandFee;

            public double laBuldingControlFunctionFee;

            public double insolvencyFee;

            public double lossOfRentFee;

            public double plasteringFee;

            public double soundTransmissionFee;

            public double technicalFee;

            public double administrationFee;

            public double cancellationFee;

            public double atkinsFee;

        }

        public PrivateRentalHVSPremium PrivateRentalsHVSPremiumCaluclationsForEachPlotRow(plotrows plot)
        {
            var calculatedPremiums = new PrivateRentalHVSPremium();
            //Retrive all the covers and Services from Fees Detaila Page 
            CoversOnPrivateRentalHVSProduct();
            //Reading Pricing Setup Book 
            var fileInfo = new FileInfo(ExtensionMethods.GetAbsolutePath($@"TestData\Premiums\ProductPricingSetup\Product Pricing Setup - HVS.xlsx"));
            string premiumsdata = fileInfo.ToString();
            excelTable = ReadPrivateRentalHVSMasterPremiumsPriceData(premiumsdata);
            //Structural Fee Caluclations Based on Banding    
            StuctralPremiumCaluclations(plot);
            SecondaryLayerPremiums.AddSecondaryLayerPremium(BasePremium, plot);
            BasePremium = Premiums.SecondaryLayerPremium;
            calculatedPremiums.structuralFee = Math.Round(BasePremium, 2);
            

            if (Premiums.ProductsCovers.Count > 0)
            {
                if (Premiums.DefectsCover == "Yes")
                {
                    calculatedPremiums.defectsFee = Convert.ToDouble(0.00);
                }
                if (Premiums.ContaminatedLandCover == "Yes")
                {
                    calculatedPremiums.contaminatedLandFee = Convert.ToDouble(0.00);
                }
                if (Premiums.LocalAuthorityBuildingControlFunctionCover == "Yes")
                {
                    calculatedPremiums.laBuldingControlFunctionFee = Convert.ToDouble(0.00);
                }

                if (Premiums.LossOfRentCover == "Yes")
                {
                    LossOfRentCaluclations(plot);
                    calculatedPremiums.lossOfRentFee = Math.Round((LossOfRent), 2);
                }
                if (Premiums.PlasteringCover == "Yes")
                {
                    calculatedPremiums.plasteringFee = Convert.ToDouble(0.00);
                }
                if (Premiums.SoundTransmissionCover == "Yes")
                {
                    calculatedPremiums.soundTransmissionFee = Convert.ToDouble(0.00);
                }
                if (Premiums.InsolvencyCover == "Yes")
                {
                    InsolvencyPremiumFatcor(plot);
                    calculatedPremiums.insolvencyFee = Convert.ToDouble(InsolvencyPremium);
                }
            }
          
            //Product Service Fee Values 
            if (Premiums.ProductServices.Count > 0)
            {

                if (Premiums.ProductTechnicalAuditFeeService == "Yes")
                {                    
                    TAFeeCaluclations(plot);
                    calculatedPremiums.technicalFee = Math.Round((TAFee), 2);
                }
                if (Premiums.ProductAdminFeeService == "Yes")
                {
                    calculatedPremiums.administrationFee = Convert.ToDouble(0.00);
                }
                if (Premiums.ProductCancellationFeeService == "Yes")
                {
                    calculatedPremiums.cancellationFee = Convert.ToDouble(0.00);
                }
                if (Premiums.ProductAtkinsFeeService == "Yes")
                {
                    AtkinsFeeCaluclations();
                    calculatedPremiums.atkinsFee = Math.Round(Convert.ToDouble(AtkinsPremium), 2);
                }
            }
            return calculatedPremiums;
        }



        //Reading Strucural Premium Banding Table  and Caluclate Base Premium
        public void StuctralPremiumCaluclations(plotrows plot)
        {
            // Reading Structural Premium Table From Private RentalHVS Pricing Setup Book
            var rating = Premiums.Rating;
            var ratingTableRow = -1;
            var ratingColumn = -1;
            //Get Rating Table from Structural Premium Primary Layer Table
            for (int i = 0; i < excelTable.Rows.Count; i++)
            {
                if (excelTable.Rows[i][1].ToString().Contains("Rating"))
                {
                    ratingTableRow = i;
                    break;
                }
            }
            if (ratingTableRow == -1)
                throw new Exception("Rating Table Row Not Found");


            //Get Rating Columns from Structural Premium Table

            for (int i = ratingTableRow; i < excelTable.Rows[ratingTableRow].ItemArray.Length; i++)
            {
                if (excelTable.Rows[ratingTableRow].ItemArray[i].ToString() == Premiums.Rating.ToString())
                {
                    ratingColumn = i;
                    RatingColumn = ratingColumn;
                }
            }

            if (ratingColumn == -1)
                throw new Exception("Rating Column Not Found");

            //Get Premium Value From structural Premium Table 
            double basicpremiumValue = Convert.ToDouble(excelTable.Rows[ratingTableRow + 1][RatingColumn]);
            BasePremium = Convert.ToDouble(plot.ReconstructionCostValue * basicpremiumValue);

            //Cover Loading Premiums 
            PeriodOfCoverLoadingCaluclation(plot);

            //Caluclate Premiums Based on Unit type 
            UnitTypeCaluclation(plot);
            //Caluclate Premiums Based on Construction type 
            if (Premiums.eachplotrow.Any(o => o.ConstructionType.Contains("New Build")) && Premiums.eachplotrow.Any(o => o.ConstructionType.Contains("Conversion")))
            {
                if (plot.ConstructionType.Contains("New Build"))
                {
                    ConstructionTypeCaluclationNewbuildandConversion(plot);
                }
                else
                {
                    ConstructionTypeCaluclation(plot);
                }
            }
            else
            {
                ConstructionTypeCaluclation(plot);
            }

            //Multi Project Loading Caluclation              

            if (SendQuotePage.MultiProjectDiscount.Count > 0)
            {
                Premiums.MultiProjectLoading = SendQuotePage.MultiProjectDiscount[0].Text;
                if (Premiums.MultiProjectLoading == "Yes")
                {
                    MultiProjectLoadngDiscountCaluclations(plot);
                }

            }
            if (SendQuotePage.DiscretionaryDiscount.Count > 0)
            {
                DiscretionaryDiscountCaluclations(plot);
            }
            if (SendQuotePage.StageOfWorksPercentage.Count > 0)
            {
                StageOfWorksLoadingPercentageCaluclations(plot);
            }
        }
        //Reading Period Of Cover Loading Table 
        public void PeriodOfCoverLoadingCaluclation(plotrows plot)
        {
            var periodOfCoverLoadingTableRow = -1;
            var periodOfCoverRow = -1;
            //Get Unit Type Loading Table from Structural Premium Table
            for (int i = 0; i < excelTable.Rows.Count; i++)
            {
                if (excelTable.Rows[i][0].ToString().Contains("Period of Cover Loading"))
                {
                    periodOfCoverLoadingTableRow = i;
                    break;
                }
            }
            if (periodOfCoverLoadingTableRow == -1)
                throw new Exception("Unit Type Loading Table Not Found");

            //Get UnitType Row from Unit Type Loading Table
            for (int i = periodOfCoverLoadingTableRow; i < excelTable.Rows.Count; i++)
            {
                if (excelTable.Rows[i][1].ToString() == Premiums.CoverLengthYears.ToString())
                {
                    periodOfCoverRow = i;
                    break;
                }
            }
            if (periodOfCoverRow == -1)
                throw new Exception("Period Of Cover Loading Row Not Found");
            double premiumValueBasedonPeriodOfCoverLoading = Convert.ToDouble(excelTable.Rows[periodOfCoverRow][2]);
            BasePremium = Convert.ToDouble(BasePremium * premiumValueBasedonPeriodOfCoverLoading);
        }
        //Reading Unit Type Loading Table and Caluclate Base Premium
        public void UnitTypeCaluclation(plotrows plot)
        {
            var unitTypeTableRow = -1;
            var unitTypeRow = -1;
            //Get Unit Type Loading Table from Structural Premium Table
            for (int i = 0; i < excelTable.Rows.Count; i++)
            {
                if (excelTable.Rows[i][0].ToString().Contains("Unit Type Loading"))
                {
                    unitTypeTableRow = i;
                    break;
                }
            }
            if (unitTypeTableRow == -1)
                throw new Exception("Unit Type Loading Table Not Found");

            //Get UnitType Row from Unit Type Loading Table
            for (int i = unitTypeTableRow; i < excelTable.Rows.Count; i++)
            {
                if (excelTable.Rows[i][1].ToString() == plot.UnitType.ToString())
                {
                    unitTypeRow = i;
                    break;
                }
            }
            if (unitTypeRow == -1)
                throw new Exception("Unit Type Row Not Found");
            double premiumValueBasedonUnitTypeLoading = Convert.ToDouble(excelTable.Rows[unitTypeRow][RatingColumn]);
            BasePremium = Convert.ToDouble(BasePremium * premiumValueBasedonUnitTypeLoading);
        }
        //Reading Construction Type Loading Table and Caluclate Base Premium
        public void ConstructionTypeCaluclation(plotrows plot)
        {
            var constructionTypeTableRow = -1;
            var constructionTypeRow = -1;
            //Get Construction Type Loading Table from Structural Premium Table
            for (int i = 0; i < excelTable.Rows.Count; i++)
            {
                if (excelTable.Rows[i][0].ToString().Contains("Construction Type Loading"))
                {
                    constructionTypeTableRow = i;
                    break;
                }
            }
            if (constructionTypeTableRow == -1)
                throw new Exception("Construction Type Loading Table Not Found");

            //Get Construction Type  Row from Construction Type Loading Table 
            for (int i = constructionTypeTableRow; i < excelTable.Rows.Count; i++)
            {
                if (excelTable.Rows[i][1].ToString() == plot.ConstructionType.ToString())
                {
                    constructionTypeRow = i;
                    break;
                }
            }
            if (constructionTypeRow == -1)
                throw new Exception("Construction Type Row Not Found");
            double premiumValueBasedOnConstructionType = Convert.ToDouble(excelTable.Rows[constructionTypeRow][RatingColumn]);
            BasePremium = Convert.ToDouble(BasePremium * premiumValueBasedOnConstructionType);
            if (plot.ConstructionType == "New Build")
            {
                NewBuildStagesOfWorkCaluclation(plot);
            }
            if (plot.ConstructionType == "Conversion")
            {
                ConversionStagesOfWorkCaluclation(plot);
            }
        }
        //Reading Construction Type Loading Table and Caluclate Base Premium
        public void ConstructionTypeCaluclationNewbuildandConversion(plotrows plot)
        {
            var constructionTypeTableRow = -1;
            var constructionTypeRow = -1;
            //Get Construction Type Loading Table from Structural Premium Table
            for (int i = 0; i < excelTable.Rows.Count; i++)
            {
                if (excelTable.Rows[i][0].ToString().Contains("Construction Type Loading"))
                {
                    constructionTypeTableRow = i;
                    break;
                }
            }
            if (constructionTypeTableRow == -1)
                throw new Exception("Construction Type Loading Table Not Found");

            //Get Construction Type  Row from Construction Type Loading Table 
            for (int i = constructionTypeTableRow; i < excelTable.Rows.Count; i++)
            {
                if (excelTable.Rows[i][1].ToString().Contains("New Build (with Conversion)"))
                {
                    constructionTypeRow = i;
                    break;
                }
            }
            if (constructionTypeRow == -1)
                throw new Exception("Construction Type Row Not Found");
            double premiumValueBasedOnConstructionType = Convert.ToDouble(excelTable.Rows[constructionTypeRow][RatingColumn]);
            BasePremium = Convert.ToDouble(BasePremium * premiumValueBasedOnConstructionType);
            if (plot.ConstructionType == "New Build")
            {
                NewBuildStagesOfWorkCaluclation(plot);
            }
            if (plot.ConstructionType == "Conversion")
            {
                ConversionStagesOfWorkCaluclation(plot);
            }
        }


        //Reading NewBuild Stages Of Work Loading Table and Caluclate Base Premium
        public void NewBuildStagesOfWorkCaluclation(plotrows plot)
        {
            double premiumValueBasedOnNewBuildSOW = 0.00;
            if (plot.StageOfWork == "No Works Started")
            {
                premiumValueBasedOnNewBuildSOW = Convert.ToDouble(1.00);
            }
            if (plot.StageOfWork == "Foundation/DPC")
            {
                premiumValueBasedOnNewBuildSOW = Convert.ToDouble(1.25);
            }
            if (plot.StageOfWork == "1st Floor")
            {
                premiumValueBasedOnNewBuildSOW = Convert.ToDouble(1.50);
            }
            if (plot.StageOfWork == "Wall Plate Level")
            {
                premiumValueBasedOnNewBuildSOW = Convert.ToDouble(1.75);
            }
            if (plot.StageOfWork == "Roof/Watertight")
            {
                premiumValueBasedOnNewBuildSOW = Convert.ToDouble(2.00);
            }
            BasePremium = Convert.ToDouble(BasePremium * premiumValueBasedOnNewBuildSOW);
        }

        //Reading Conversion Stages Of Work Loading Table and Caluclate Base Premium
        public void ConversionStagesOfWorkCaluclation(plotrows plot)
        {
            double premiumValueBasedOnConversionSOW = 0.00;
            if (plot.StageOfWork == "No Works Started")
            {
                premiumValueBasedOnConversionSOW = Convert.ToDouble(1.00);
            }
            if (plot.StageOfWork == "First Fix")
            {
                premiumValueBasedOnConversionSOW = Convert.ToDouble(1.25);
            }
            if (plot.StageOfWork == "Second Fix")
            {
                premiumValueBasedOnConversionSOW = Convert.ToDouble(1.75);
            }
            BasePremium = Convert.ToDouble(BasePremium * premiumValueBasedOnConversionSOW);
        }
        public void MultiProjectLoadngDiscountCaluclations(plotrows plot)
        {
            var multiProjectLoadngTableRow = -1;
            var multiProjectLoadingRow = -1;
            //Get Basment Loading (Site wide for all products) Table from Structural Premium Table
            for (int i = 0; i < excelTable.Rows.Count; i++)
            {
                if (excelTable.Rows[i][0].ToString().Contains("Multi Project Loading (Site wide for all products)"))
                {
                    multiProjectLoadngTableRow = i;
                    break;
                }
            }
            if (multiProjectLoadngTableRow == -1)
                throw new Exception("Multi Project Loading (Site wide for all products) Table Not Found");

            //Multi Basment Loading (Site wide for all products) Table Row from Multi Project Loading Table 
            for (int i = multiProjectLoadngTableRow; i < excelTable.Rows.Count; i++)
            {
                if (excelTable.Rows[i][1].ToString() == "Yes")
                {
                    multiProjectLoadingRow = i;
                    break;
                }
            }
            if (multiProjectLoadingRow == -1)
                throw new Exception("Multi Project Loading (Site wide for all products) Table Row Not Found");
            double premiumValueBasedOnMultiProjectLoadingValue = Convert.ToDouble(excelTable.Rows[multiProjectLoadingRow][2]);
            Premiums.MultiProjectDiscountInput = SendQuotePage.MultiProjectDiscount[0].Text;
            if (Premiums.MultiProjectDiscountInput == "Yes")
            {
                double multiProjectDiscount = Convert.ToDouble(BasePremium * premiumValueBasedOnMultiProjectLoadingValue);
                BasePremium = Convert.ToDouble(multiProjectDiscount);
            }
        }
        //Discretionary Discount Caluclations
        public void DiscretionaryDiscountCaluclations(plotrows plot)
        {
            string discretionaryDiscountText = SendQuotePage.DiscretionaryDiscount[0].Text;
            string discretionaryDiscountTextValue = discretionaryDiscountText.Replace("%", "");
            Premiums.DiscretionaryDiscountInput = Convert.ToDouble(discretionaryDiscountTextValue);
            if (Premiums.DiscretionaryDiscountInput > 0.00)
            {
                double discretionaryDiscount = Convert.ToDouble(BasePremium * Premiums.DiscretionaryDiscountInput);
                BasePremium = Convert.ToDouble(BasePremium - discretionaryDiscount);
            }
        }

        //Stages oF works Loading Caluclations
        public void StageOfWorksLoadingPercentageCaluclations(plotrows plot)
        {
            string sowPercentageText = SendQuotePage.StageOfWorksPercentage[0].Text;
            string sowPercentageTextValue = sowPercentageText.Replace("%", "");
            Premiums.StageOfWorksLoadingPercentageInput = Convert.ToDouble(sowPercentageTextValue);
            if (Premiums.StageOfWorksLoadingPercentageInput > 0.00)
            {
                double sowPercentage = Convert.ToDouble(BasePremium * Premiums.StageOfWorksLoadingPercentageInput);
                BasePremium = Convert.ToDouble(BasePremium + sowPercentage);
            }
        }


        //Reading Loss Of Rent Premiums Factor Table 
        public void LossOfRentCaluclations(plotrows plot)
        {
            var lossOfRentTable = -1;
            var lossOfRentRatingRow = -1;
            //Get Conversion Stages Of Works Table from Structural Premium Table
            for (int i = 0; i < excelTable.Rows.Count; i++)
            {
                if (excelTable.Rows[i][0].ToString().Contains("Loss of Rent Premium"))
                {
                    lossOfRentTable = i;
                    break;
                }
            }
            if (lossOfRentTable == -1)
                throw new Exception("Loss Of Rent Table Not Found");
            //Get Conversion Stages Of Works Row 
            for (int i = lossOfRentTable; i < excelTable.Rows.Count; i++)
            {
                if (excelTable.Rows[i][1].ToString().Contains("Rating"))
                {
                    lossOfRentRatingRow = i;
                    break;
                }
            }
            if (lossOfRentRatingRow == -1)
                throw new Exception("Loss Of Rent Banding Row Not Found");

            //Get Loss Of rent Premium Value From structural Premium Table 
            double lossofrentpremiumValue = Convert.ToDouble(excelTable.Rows[lossOfRentRatingRow + 1][RatingColumn]);
            double premiumValueBasedOnLossOfRent = Convert.ToDouble(lossofrentpremiumValue * plot.ReconstructionCostValue);
            LossOfRent = Convert.ToDouble(premiumValueBasedOnLossOfRent * Premiums.CoverLengthYears);

        }


        //Services Caluclations 
        //TA Fee Caluclations   
        //Basic TAFee Caluclations     
        public void TAFeeCaluclations(plotrows plot)
        {
            // Reading TA Fee Table From Private RentalHVS Pricing Setup Book           
            var TAFeeTableRow = -1;
            var bandingRow = -1;

            //Get TA Fee Table from Structural Premium Table
            for (int i = 0; i < excelTable.Rows.Count; i++)
            {
                if (excelTable.Rows[i][0].ToString() == "Technical Audit Fee")
                {
                    TAFeeTableRow = i;
                    break;
                }
            }
            if (TAFeeTableRow == -1)
                throw new Exception("TAFee Table Not Found");


            //Get TA FEE Banding Row from TA Fee Table
            for (int i = TAFeeTableRow; i < excelTable.Rows.Count; i++)
            {
                if (excelTable.Rows[i][1].ToString() == "Rate")
                {
                    bandingRow = i;
                    break;
                }
            }
            if (bandingRow == -1)
                throw new Exception("TA Fee Banding Row Not Found");

            //Get TA Fee Value From TAFee Table 
            double taFeeValue = Convert.ToDouble(excelTable.Rows[bandingRow][RatingColumn]);
            TAFee = Convert.ToDouble(plot.ReconstructionCostValue * taFeeValue);
        }


        public void AtkinsFeeCaluclations()
        {
            // Reading Consumer Code Fee Table From Private RentalHVS Pricing Setup Book            
            var atkinsPremiumTableRow = -1;


            //Get Consumer Code Fee per Plot Table from Structural Premium Table
            for (int i = 0; i < excelTable.Rows.Count; i++)
            {
                if (excelTable.Rows[i][0].ToString().Contains("Atkins Premium"))
                {
                    atkinsPremiumTableRow = i;
                    break;
                }
            }
            if (atkinsPremiumTableRow == -1)
                throw new Exception("Consumer Code Fee per Plot Table Not Found");

            double atkinsFeeValue = Convert.ToDouble(excelTable.Rows[atkinsPremiumTableRow + 2][2]);
            AtkinsPremium = Convert.ToDouble(atkinsFeeValue * 12);

        }
        public void MinimumAtkinsFeeCaluclations()
        {
            // Reading Consumer Code Fee Table From Newhomes Pricing Setup Book            
            var minimumAtkinsPremiumTableRow = -1;


            //Get Consumer Code Fee per Plot Table from Structural Premium Table
            for (int i = 0; i < excelTable.Rows.Count; i++)
            {
                if (excelTable.Rows[i][0].ToString().Contains("Atkins Premium"))
                {
                    minimumAtkinsPremiumTableRow = i;
                    break;
                }
            }
            if (minimumAtkinsPremiumTableRow == -1)
                throw new Exception("Consumer Code Fee per Plot Table Not Found");

            double atkinsFeeValue = Convert.ToDouble(excelTable.Rows[minimumAtkinsPremiumTableRow + 4][2]);
            MinimumAtkinsFee = Convert.ToDouble(atkinsFeeValue);

        }
        //Reading Insolvancy Premium Factor Table 
        public void InsolvencyPremiumFatcor(plotrows plot)
        {
            var insolvencyPremiumTable = -1;
            var insolvencyRow = -1;

            //Get Conversion Stages Of Works Table from Structural Premium Table
            for (int i = 0; i < excelTable.Rows.Count; i++)
            {
                if (excelTable.Rows[i][0].ToString() == "Insolvency Premium")
                {
                    insolvencyPremiumTable = i;
                    break;
                }
            }
            if (insolvencyPremiumTable == -1)
                throw new Exception("Insolvency Premium Factor Table Not Found");
            for (int i = insolvencyPremiumTable; i < excelTable.Rows.Count; i++)
            {
                if (excelTable.Rows[i][1].ToString() == "Rate")
                {
                    insolvencyRow = i;
                    break;
                }
            }
            if (insolvencyRow == -1)
                throw new Exception("Insolvency Premium Factor Value Not Found");
            //Get Insolvancy Premiums Factor  Row 
            double insolvencyValue = Convert.ToDouble(excelTable.Rows[insolvencyRow][RatingColumn]);

            InsolvencyPremium = Convert.ToDouble(plot.ReconstructionCostValue * insolvencyValue);

        }

        public void VerfiyPrivateRentalHVSUIValues()
        {
            var productName = Premiums.eachplotrow.FirstOrDefault().ProductName;
            //Covers Elements 
            //Veridy UI Structural Values
            var uiStructuralFee = $"//th[@class='product-title'][text()='{productName}']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[text()='Structural']//following-sibling::td[@class='right-align'][1]";
            var StructuralFee = Driver.FindElement(By.XPath(uiStructuralFee)).Text.Replace("£", "");
            double UIStructuralFee = Math.Round(Convert.ToDouble(StructuralFee), 2);
            double StructuralFeeDiff;
            var uiStructuralIPTFee = $"//th[@class='product-title'][text()='{productName}']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[text()='Structural']//following-sibling::td[@class='right-align'][2]";
            var StructuralIPTFee = Driver.FindElement(By.XPath(uiStructuralIPTFee)).Text.Replace("£", "");
            double UIStructuralIPTFee = Math.Round(Convert.ToDouble(StructuralIPTFee), 2);
            double StructuralIPTDiff;
            var uiStructuralTotalFee = $"//th[@class='product-title'][text()='{productName}']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[text()='Structural']//following-sibling::th[@class='right-align'][1]";
            var StructuralTotalFee = Driver.FindElement(By.XPath(uiStructuralTotalFee)).Text.Replace("£", "");
            double UIStructuralTotalFee = Math.Round(Convert.ToDouble(StructuralTotalFee), 2);
            double StructuralTotalValue = Math.Round(Convert.ToDouble(UIStructuralFee + UIStructuralIPTFee), 2);
            double StructuralTotalDiff;

            if (UIStructuralFee != Premiums.StructuralFee || UIStructuralIPTFee != Premiums.StructuralIPTFee || UIStructuralTotalFee != StructuralTotalValue)
            {
                StructuralFeeDiff = Convert.ToDouble(Premiums.StructuralFee - UIStructuralFee);
                StructuralIPTDiff = Convert.ToDouble(Premiums.StructuralIPTFee - UIStructuralIPTFee);
                StructuralTotalDiff = Convert.ToDouble(StructuralTotalValue - UIStructuralTotalFee);
                if (StructuralFeeDiff >= -1.01 && StructuralFeeDiff <= 1.01)
                {
                    Premiums.StructuralFee = UIStructuralFee;
                }
                if (StructuralIPTDiff >= -1.01 && StructuralIPTDiff <= 1.01)
                {
                    Premiums.StructuralIPTFee = UIStructuralIPTFee;
                }
                if (StructuralTotalDiff >= -1.01 && StructuralTotalDiff <= 1.01)
                {
                    StructuralTotalValue = UIStructuralTotalFee;
                }

                Assert.AreEqual(UIStructuralFee, Premiums.StructuralFee, $"StructuralFee Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.EstimatedSalePrice}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                Assert.AreEqual(UIStructuralIPTFee, Premiums.StructuralIPTFee, $"StructuralIPTFee Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.EstimatedSalePrice}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                Assert.AreEqual(UIStructuralTotalFee, StructuralTotalValue, $"StructuralTotalFee Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.EstimatedSalePrice}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
            }
            else
            {

                Assert.AreEqual(UIStructuralFee, Premiums.StructuralFee, $"StructuralFee Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.EstimatedSalePrice}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                Assert.AreEqual(UIStructuralIPTFee, Premiums.StructuralIPTFee, $"StructuralIPTFee Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.EstimatedSalePrice}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                Assert.AreEqual(UIStructuralTotalFee, StructuralTotalValue, $"StructuralTotalFee Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.EstimatedSalePrice}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
            }
            if (Premiums.ProductsCovers.Any(x => x.Contains("Defects")))
            {
                //Veirfy UI Contaminated Land values
                var uiDefectsFee = $"//th[@class='product-title'][text()='{productName}']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[text()='Defects']//following-sibling::td[@class='right-align'][1]";
                var DefectsFee = Driver.FindElement(By.XPath(uiDefectsFee)).Text.Replace("£", "");
                double UIDefectsFee = Math.Round(Convert.ToDouble(DefectsFee), 2);
                double DefectsFeeDiff;
                var uiDefectsIPTFee = $"//th[@class='product-title'][text()='{productName}']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[text()='Defects']//following-sibling::td[@class='right-align'][2]";
                var DefectsIPTFee = Driver.FindElement(By.XPath(uiDefectsIPTFee)).Text.Replace("£", "");
                double UIDefectsIPTFee = Math.Round(Convert.ToDouble(DefectsIPTFee), 2);
                double DefectsIPTDiff;
                var uiDefectsTotalFee = $"//th[@class='product-title'][text()='{productName}']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[text()='Defects']//following-sibling::th[@class='right-align'][1]";
                var DefectsTotalFee = Driver.FindElement(By.XPath(uiDefectsTotalFee)).Text.Replace("£", "");
                double UIDefectsTotalFee = Math.Round(Convert.ToDouble(DefectsTotalFee), 2);
                double DefectsTotalValue = Math.Round(Convert.ToDouble(UIDefectsFee + UIDefectsIPTFee), 2);
                double DefectsTotalDiff;

                if (UIDefectsFee != Premiums.DefectsFee || UIDefectsIPTFee != Premiums.DefectsIPTFee || UIDefectsTotalFee != DefectsTotalValue)
                {
                    DefectsFeeDiff = Convert.ToDouble(UIDefectsFee - Premiums.DefectsFee);
                    DefectsIPTDiff = Convert.ToDouble(UIDefectsIPTFee - Premiums.DefectsIPTFee);
                    DefectsTotalDiff = Convert.ToDouble(UIDefectsTotalFee - DefectsTotalValue);
                    if (DefectsFeeDiff >= -1.01 && DefectsFeeDiff <= 1.01)
                    {
                        Premiums.DefectsFee = UIDefectsFee;
                    }
                    if (DefectsIPTDiff >= -1.01 && DefectsIPTDiff <= 1.01)
                    {
                        Premiums.DefectsIPTFee = UIDefectsIPTFee;
                    }
                    if (DefectsTotalDiff >= -1.01 && DefectsTotalDiff <= 1.01)
                    {
                        DefectsTotalValue = UIDefectsTotalFee;
                    }

                    Assert.AreEqual(UIDefectsFee, Premiums.DefectsFee, $"DefectsFee Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.EstimatedSalePrice}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UIDefectsIPTFee, Premiums.DefectsIPTFee, $"DefectsIPTFee Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.EstimatedSalePrice}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UIDefectsTotalFee, DefectsTotalValue, $"DefectsTotalFee Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.EstimatedSalePrice}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                }
                else
                {
                    Assert.AreEqual(UIDefectsFee, Premiums.DefectsFee, $"DefectsFee Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.EstimatedSalePrice}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UIDefectsIPTFee, Premiums.DefectsIPTFee, $"DefectsIPTFee Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.EstimatedSalePrice}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UIDefectsTotalFee, DefectsTotalValue, $"DefectsTotalFee Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.EstimatedSalePrice}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                }
            }
            if (Premiums.ProductsCovers.Any(x => x.Contains("Contaminated Land")))
            {
                //Veirfy UI Contaminated Land values
                var uiContaminatedLandFee = $"//th[@class='product-title'][text()='{productName}']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[text()='Contaminated Land']//following-sibling::td[@class='right-align'][1]";
                var ContaminatedLandFee = Driver.FindElement(By.XPath(uiContaminatedLandFee)).Text.Replace("£", "");
                double UIContaminatedLandFee = Math.Round(Convert.ToDouble(ContaminatedLandFee), 2);
                double ContaminatedLandFeeDiff;
                var uiContaminatedLandIPTFee = $"//th[@class='product-title'][text()='{productName}']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[text()='Contaminated Land']//following-sibling::td[@class='right-align'][2]";
                var ContaminatedLandIPTFee = Driver.FindElement(By.XPath(uiContaminatedLandIPTFee)).Text.Replace("£", "");
                double UIContaminatedLandIPTFee = Math.Round(Convert.ToDouble(ContaminatedLandIPTFee), 2);
                double ContaminatedLandIPTDiff;
                var uiContaminatedLandTotalFee = $"//th[@class='product-title'][text()='{productName}']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[text()='Contaminated Land']//following-sibling::th[@class='right-align'][1]";
                var ContaminatedLandTotalFee = Driver.FindElement(By.XPath(uiContaminatedLandTotalFee)).Text.Replace("£", "");
                double UIContaminatedLandTotalFee = Math.Round(Convert.ToDouble(ContaminatedLandTotalFee), 2);
                double ContaminatedLandTotalValue = Math.Round(Convert.ToDouble(UIContaminatedLandFee + UIContaminatedLandIPTFee), 2);
                double ContaminatedLandTotalDiff;

                if (UIContaminatedLandFee != Premiums.ContaminatedLandFee || UIContaminatedLandIPTFee != Premiums.ContaminatedLandIPTFee || UIContaminatedLandTotalFee != ContaminatedLandTotalValue)
                {
                    ContaminatedLandFeeDiff = Convert.ToDouble(UIContaminatedLandFee - Premiums.ContaminatedLandFee);
                    ContaminatedLandIPTDiff = Convert.ToDouble(UIContaminatedLandIPTFee - Premiums.ContaminatedLandIPTFee);
                    ContaminatedLandTotalDiff = Convert.ToDouble(UIContaminatedLandTotalFee - ContaminatedLandTotalValue);
                    if (ContaminatedLandFeeDiff >= -1.01 && ContaminatedLandFeeDiff <= 1.01)
                    {
                        Premiums.ContaminatedLandFee = UIContaminatedLandFee;
                    }
                    if (ContaminatedLandIPTDiff >= -1.01 && ContaminatedLandIPTDiff <= 1.01)
                    {
                        Premiums.ContaminatedLandIPTFee = UIContaminatedLandIPTFee;
                    }
                    if (ContaminatedLandTotalDiff >= -1.01 && ContaminatedLandTotalDiff <= 1.01)
                    {
                        ContaminatedLandTotalValue = UIContaminatedLandTotalFee;
                    }

                    Assert.AreEqual(UIContaminatedLandFee, Premiums.ContaminatedLandFee, $"ContaminatedLandFee Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.EstimatedSalePrice}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UIContaminatedLandIPTFee, Premiums.ContaminatedLandIPTFee, $"ContaminatedLandIPTFee Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.EstimatedSalePrice}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UIContaminatedLandTotalFee, ContaminatedLandTotalValue, $"ContaminatedLandTotalFee Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.EstimatedSalePrice}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                }
                else
                {
                    Assert.AreEqual(UIContaminatedLandFee, Premiums.ContaminatedLandFee, $"ContaminatedLandFee Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.EstimatedSalePrice}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UIContaminatedLandIPTFee, Premiums.ContaminatedLandIPTFee, $"ContaminatedLandIPTFee Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.EstimatedSalePrice}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UIContaminatedLandTotalFee, ContaminatedLandTotalValue, $"ContaminatedLandTotalFee Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.EstimatedSalePrice}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                }
            }
            if (Premiums.ProductsCovers.Any(x => x.Contains("Additional Cover for Local Authority Building Control Function")))
            {

                //Verify UI Local Authority Building Control Values
                var uiLocalAuthorityBuildingControlFee = $"//th[@class='product-title'][text()='{productName}']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[text()='Additional Cover for Local Authority Building Control Function']//following-sibling::td[@class='right-align'][1]";
                var LocalAuthorityBuildingControlFee = Driver.FindElement(By.XPath(uiLocalAuthorityBuildingControlFee)).Text.Replace("£", "");
                double UILocalAuthorityBuildingControlFee = Math.Round(Convert.ToDouble(LocalAuthorityBuildingControlFee), 2);
                double LocalAuthorityBuildingControlFeeDiff;

                var uiLocalAuthorityBuildingControlIPTFee = $"//th[@class='product-title'][text()='{productName}']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[text()='Additional Cover for Local Authority Building Control Function']//following-sibling::td[@class='right-align'][2]";
                var LocalAuthorityBuildingControlIPTFee = Driver.FindElement(By.XPath(uiLocalAuthorityBuildingControlIPTFee)).Text.Replace("£", "");
                double UILocalAuthorityBuildingControlIPTFee = Math.Round(Convert.ToDouble(LocalAuthorityBuildingControlIPTFee), 2);
                double LocalAuthorityBuildingControlIPTDiff;

                var uiLocalAuthorityBuildingControlTotalFee = $"//th[@class='product-title'][text()='{productName}']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[text()='Additional Cover for Local Authority Building Control Function']//following-sibling::th[@class='right-align'][1]";
                var LocalAuthorityBuildingControlTotalFee = Driver.FindElement(By.XPath(uiLocalAuthorityBuildingControlTotalFee)).Text.Replace("£", "");
                double UILocalAuthorityBuildingControlTotalFee = Math.Round(Convert.ToDouble(LocalAuthorityBuildingControlTotalFee), 2);
                double LocalAuthorityBuildingControlTotalValue = Math.Round(Convert.ToDouble(UILocalAuthorityBuildingControlFee + UILocalAuthorityBuildingControlIPTFee), 2);
                double LocalAuthorityBuildingControlTotalDiff;

                if (UILocalAuthorityBuildingControlFee != Premiums.LocalAuthorityBuildingControlFee || UILocalAuthorityBuildingControlIPTFee != Premiums.LocalAuthorityBuildingControlIPTFee || UILocalAuthorityBuildingControlTotalFee != LocalAuthorityBuildingControlTotalValue)
                {
                    LocalAuthorityBuildingControlFeeDiff = Convert.ToDouble(Premiums.LocalAuthorityBuildingControlFee - UILocalAuthorityBuildingControlFee);
                    LocalAuthorityBuildingControlIPTDiff = Convert.ToDouble(Premiums.LocalAuthorityBuildingControlIPTFee - UILocalAuthorityBuildingControlIPTFee);
                    LocalAuthorityBuildingControlTotalDiff = Convert.ToDouble(LocalAuthorityBuildingControlTotalValue - UILocalAuthorityBuildingControlTotalFee);
                    if (LocalAuthorityBuildingControlFeeDiff >= -1.01 && LocalAuthorityBuildingControlFeeDiff <= 1.01)
                    {
                        Premiums.LocalAuthorityBuildingControlFee = UILocalAuthorityBuildingControlFee;
                    }
                    if (LocalAuthorityBuildingControlIPTDiff >= -1.01 && LocalAuthorityBuildingControlIPTDiff <= 1.01)
                    {
                        Premiums.LocalAuthorityBuildingControlIPTFee = UILocalAuthorityBuildingControlIPTFee;
                    }
                    if (LocalAuthorityBuildingControlTotalDiff >= -1.01 && LocalAuthorityBuildingControlTotalDiff <= 1.01)
                    {
                        LocalAuthorityBuildingControlTotalValue = UILocalAuthorityBuildingControlTotalFee;
                    }

                    Assert.AreEqual(UILocalAuthorityBuildingControlFee, Premiums.LocalAuthorityBuildingControlFee, $"LocalAuthorityBuildingControlFee Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UILocalAuthorityBuildingControlIPTFee, Premiums.LocalAuthorityBuildingControlIPTFee, $"LocalAuthorityBuildingControlIPTFee Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UILocalAuthorityBuildingControlTotalFee, LocalAuthorityBuildingControlTotalValue, $"LocalAuthorityBuildingControlTotalFee Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                }
                else
                {
                    Assert.AreEqual(UILocalAuthorityBuildingControlFee, Premiums.LocalAuthorityBuildingControlFee, $"LocalAuthorityBuildingControlFee Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UILocalAuthorityBuildingControlIPTFee, Premiums.LocalAuthorityBuildingControlIPTFee, $"LocalAuthorityBuildingControlIPTFee Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UILocalAuthorityBuildingControlTotalFee, LocalAuthorityBuildingControlTotalValue, $"LocalAuthorityBuildingControlTotalFee Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                }

            }
            if (Premiums.ProductsCovers.Any(x => x.Contains("Insolvency of the Builder")))
            {
                //Veirfy UI Contaminated Land values
                var uiInsolvencyFee = $"//th[@class='product-title'][text()='{productName}']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[text()='Insolvency of the Builder']//following-sibling::td[@class='right-align'][1]";
                var InsolvencyFee = Driver.FindElement(By.XPath(uiInsolvencyFee)).Text.Replace("£", "");
                double UIInsolvencyFee = Math.Round(Convert.ToDouble(InsolvencyFee), 2);
                double InsolvencyFeeDiff;
                var uiInsolvencyIPTFee = $"//th[@class='product-title'][text()='{productName}']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[text()='Insolvency of the Builder']//following-sibling::td[@class='right-align'][2]";
                var InsolvencyIPTFee = Driver.FindElement(By.XPath(uiInsolvencyIPTFee)).Text.Replace("£", "");
                double UIInsolvencyIPTFee = Math.Round(Convert.ToDouble(InsolvencyIPTFee), 2);
                double InsolvencyIPTDiff;
                var uiInsolvencyTotalFee = $"//th[@class='product-title'][text()='{productName}']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[text()='Insolvency of the Builder']//following-sibling::th[@class='right-align'][1]";
                var InsolvencyTotalFee = Driver.FindElement(By.XPath(uiInsolvencyTotalFee)).Text.Replace("£", "");
                double UIInsolvencyTotalFee = Math.Round(Convert.ToDouble(InsolvencyTotalFee), 2);
                double InsolvencyTotalValue = Math.Round(Convert.ToDouble(UIInsolvencyFee + UIInsolvencyIPTFee), 2);
                double InsolvencyTotalDiff;

                if (UIInsolvencyFee != Premiums.InsolvencyFee || UIInsolvencyIPTFee != Premiums.InsolvencyIPTFee || UIInsolvencyTotalFee != InsolvencyTotalValue)
                {
                    InsolvencyFeeDiff = Convert.ToDouble(Premiums.InsolvencyFee - UIInsolvencyFee);
                    InsolvencyIPTDiff = Convert.ToDouble(Premiums.InsolvencyIPTFee - UIInsolvencyIPTFee);
                    InsolvencyTotalDiff = Convert.ToDouble(InsolvencyTotalValue - UIInsolvencyTotalFee);
                    if (InsolvencyFeeDiff >= -1.01 && InsolvencyFeeDiff <= 1.01)
                    {
                        Premiums.InsolvencyFee = UIInsolvencyFee;
                    }
                    if (InsolvencyIPTDiff >= -1.01 && InsolvencyIPTDiff <= 1.01)
                    {
                        Premiums.InsolvencyIPTFee = UIInsolvencyIPTFee;
                    }
                    if (InsolvencyTotalDiff >= -1.01 && InsolvencyTotalDiff <= 1.01)
                    {
                        InsolvencyTotalValue = UIInsolvencyTotalFee;
                    }

                    Assert.AreEqual(UIInsolvencyFee, Premiums.InsolvencyFee, $"InsolvencyFee Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.EstimatedSalePrice}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UIInsolvencyIPTFee, Premiums.InsolvencyIPTFee, $"InsolvencyIPTFee Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.EstimatedSalePrice}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UIInsolvencyTotalFee, InsolvencyTotalValue, $"InsolvencyTotalFee Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.EstimatedSalePrice}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                }
                else
                {
                    Assert.AreEqual(UIInsolvencyFee, Premiums.InsolvencyFee, $"InsolvencyFee Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.EstimatedSalePrice}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UIInsolvencyIPTFee, Premiums.InsolvencyIPTFee, $"InsolvencyIPTFee Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.EstimatedSalePrice}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UIInsolvencyTotalFee, InsolvencyTotalValue, $"InsolvencyTotalFee Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.EstimatedSalePrice}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                }
            }
            if (Premiums.ProductsCovers.Any(x => x.Contains("Loss of Rent")))
            {

                //ui Loss of Rent  Values
                var uiLossOfRentFee = $"//th[@class='product-title'][text()='{productName}']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[text()='Loss of Rent']//following-sibling::td[@class='right-align'][1]";
                var LossOfRentFee = Driver.FindElement(By.XPath(uiLossOfRentFee)).Text.Replace("£", "");
                double UILossOfRentFee = Math.Round(Convert.ToDouble(LossOfRentFee), 2);
                double LossOfRentFeeDiff;

                var uiLossOfRentIPTFee = $"//th[@class='product-title'][text()='{productName}']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[text()='Loss of Rent']//following-sibling::td[@class='right-align'][2]";
                var LossOfRentIPTFee = Driver.FindElement(By.XPath(uiLossOfRentIPTFee)).Text.Replace("£", "");
                double UILossOfRentIPTFee = Math.Round(Convert.ToDouble(LossOfRentIPTFee), 2);
                double LossOfRentIPTDiff;

                var uiLossOfRentTotalFee = $"//th[@class='product-title'][text()='{productName}']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[text()='Loss of Rent']//following-sibling::th[@class='right-align'][1]";
                var LossOfRentTotalFee = Driver.FindElement(By.XPath(uiLossOfRentTotalFee)).Text.Replace("£", "");
                double UILossOfRentTotalFee = Math.Round(Convert.ToDouble(LossOfRentTotalFee), 2);
                double LossOfRentTotalValue = Math.Round(Convert.ToDouble(UILossOfRentFee + UILossOfRentIPTFee), 2);
                double LossOfRentTotalDiff;

                if (UILossOfRentFee != Premiums.LossOfRentFee || UILossOfRentIPTFee != Premiums.LossOfRentIPTFee || UILossOfRentTotalFee != LossOfRentTotalValue)
                {
                    LossOfRentFeeDiff = Convert.ToDouble(UILossOfRentFee - Premiums.LossOfRentFee);
                    LossOfRentIPTDiff = Convert.ToDouble(UILossOfRentIPTFee - Premiums.LossOfRentIPTFee);
                    LossOfRentTotalDiff = Convert.ToDouble(UILossOfRentTotalFee - LossOfRentTotalValue);
                    if (LossOfRentFeeDiff >= -1.01 && LossOfRentFeeDiff <= 1.01)
                    {
                        Premiums.LossOfRentFee = UILossOfRentFee;
                    }
                    if (LossOfRentIPTDiff >= -1.01 && LossOfRentIPTDiff <= 1.01)
                    {
                        Premiums.LossOfRentIPTFee = UILossOfRentIPTFee;
                    }
                    if (LossOfRentTotalDiff >= -1.01 && LossOfRentTotalDiff <= 1.01)
                    {
                        LossOfRentTotalValue = UILossOfRentTotalFee;
                    }

                    Assert.AreEqual(UILossOfRentFee, Premiums.LossOfRentFee, $"LossOfRentFee Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UILossOfRentIPTFee, Premiums.LossOfRentIPTFee, $"LossOfRentIPTFee Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UILossOfRentTotalFee, LossOfRentTotalValue, $"LossOfRentTotalValue Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                }
                else
                {
                    Assert.AreEqual(UILossOfRentFee, Premiums.LossOfRentFee, $"LossOfRentFee Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UILossOfRentIPTFee, Premiums.LossOfRentIPTFee, $"LossOfRentIPTFee Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UILossOfRentTotalFee, LossOfRentTotalValue, $"LossOfRentTotalValue Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                }
            }
            if (Premiums.ProductsCovers.Any(x => x.Contains("Plastering")))
            {

                //ui Loss of Rent Payable Values
                var uiPlasteringFee = $"//th[@class='product-title'][text()='{productName}']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[text()='Plastering']//following-sibling::td[@class='right-align'][1]";
                var PlasteringFee = Driver.FindElement(By.XPath(uiPlasteringFee)).Text.Replace("£", "");
                double UIPlasteringFee = Math.Round(Convert.ToDouble(PlasteringFee), 2);
                double PlasteringFeeDiff;

                var uiPlasteringIPTFee = $"//th[@class='product-title'][text()='{productName}']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[text()='Plastering']//following-sibling::td[@class='right-align'][2]";
                var PlasteringIPTFee = Driver.FindElement(By.XPath(uiPlasteringIPTFee)).Text.Replace("£", "");
                double UIPlasteringIPTFee = Math.Round(Convert.ToDouble(PlasteringIPTFee), 2);
                double PlasteringIPTDiff;

                var uiPlasteringTotalFee = $"//th[@class='product-title'][text()='{productName}']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[text()='Plastering']//following-sibling::th[@class='right-align'][1]";
                var PlasteringTotalFee = Driver.FindElement(By.XPath(uiPlasteringTotalFee)).Text.Replace("£", "");
                double UIPlasteringTotalFee = Math.Round(Convert.ToDouble(PlasteringTotalFee), 2);
                double PlasteringTotalValue = Math.Round(Convert.ToDouble(UIPlasteringFee + UIPlasteringIPTFee), 2);
                double PlasteringTotalDiff;

                if (UIPlasteringFee != Premiums.PlasteringFee || UIPlasteringIPTFee != Premiums.PlasteringIPTFee || UIPlasteringTotalFee != PlasteringTotalValue)
                {
                    PlasteringFeeDiff = Convert.ToDouble(UIPlasteringFee - Premiums.PlasteringFee);
                    PlasteringIPTDiff = Convert.ToDouble(UIPlasteringIPTFee - Premiums.PlasteringIPTFee);
                    PlasteringTotalDiff = Convert.ToDouble(UIPlasteringTotalFee - PlasteringTotalValue);
                    if (PlasteringFeeDiff >= -1.01 && PlasteringFeeDiff <= 1.01)
                    {
                        Premiums.PlasteringFee = UIPlasteringFee;
                    }
                    if (PlasteringIPTDiff >= -1.01 && PlasteringIPTDiff <= 1.01)
                    {
                        Premiums.PlasteringIPTFee = UIPlasteringIPTFee;
                    }
                    if (PlasteringTotalDiff >= -1.01 && PlasteringTotalDiff <= 1.01)
                    {
                        PlasteringTotalValue = UIPlasteringTotalFee;
                    }

                    Assert.AreEqual(UIPlasteringFee, Premiums.PlasteringFee, $"PlasteringFee Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UIPlasteringIPTFee, Premiums.PlasteringIPTFee, $"PlasteringIPTFee Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UIPlasteringTotalFee, PlasteringTotalValue, $"PlasteringTotalValue Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                }
                else
                {
                    Assert.AreEqual(UIPlasteringFee, Premiums.PlasteringFee, $"PlasteringFee Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UIPlasteringIPTFee, Premiums.PlasteringIPTFee, $"PlasteringIPTFee Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UIPlasteringTotalFee, PlasteringTotalValue, $"PlasteringTotalValue Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                }
            }
            if (Premiums.ProductsCovers.Any(x => x.Contains("Sound Transmission")))
            {

                //ui Loss of Rent Payable Values
                var uiSoundOfTransmissionFee = $"//th[@class='product-title'][text()='{productName}']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[text()='Sound Transmission']//following-sibling::td[@class='right-align'][1]";
                var SoundOfTransmissionFee = Driver.FindElement(By.XPath(uiSoundOfTransmissionFee)).Text.Replace("£", "");
                double UISoundOfTransmissionFee = Math.Round(Convert.ToDouble(SoundOfTransmissionFee), 2);
                double SoundOfTransmissionFeeDiff;

                var uiSoundOfTransmissionIPTFee = $"//th[@class='product-title'][text()='{productName}']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[text()='Sound Transmission']//following-sibling::td[@class='right-align'][2]";
                var SoundOfTransmissionIPTFee = Driver.FindElement(By.XPath(uiSoundOfTransmissionIPTFee)).Text.Replace("£", "");
                double UISoundOfTransmissionIPTFee = Math.Round(Convert.ToDouble(SoundOfTransmissionIPTFee), 2);
                double SoundOfTransmissionIPTDiff;

                var uiSoundOfTransmissionTotalFee = $"//th[@class='product-title'][text()='{productName}']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[text()='Sound Transmission']//following-sibling::th[@class='right-align'][1]";
                var SoundOfTransmissionTotalFee = Driver.FindElement(By.XPath(uiSoundOfTransmissionTotalFee)).Text.Replace("£", "");
                double UISoundOfTransmissionTotalFee = Math.Round(Convert.ToDouble(SoundOfTransmissionTotalFee), 2);
                double SoundOfTransmissionTotalValue = Math.Round(Convert.ToDouble(UISoundOfTransmissionFee + UISoundOfTransmissionIPTFee), 2);
                double SoundOfTransmissionTotalDiff;

                if (UISoundOfTransmissionFee != Premiums.SoundTransmissionFee || UISoundOfTransmissionIPTFee != Premiums.SoundTransmissionIPTFee || UISoundOfTransmissionTotalFee != SoundOfTransmissionTotalValue)
                {
                    SoundOfTransmissionFeeDiff = Convert.ToDouble(UISoundOfTransmissionFee - Premiums.SoundTransmissionFee);
                    SoundOfTransmissionIPTDiff = Convert.ToDouble(UISoundOfTransmissionIPTFee - Premiums.SoundTransmissionIPTFee);
                    SoundOfTransmissionTotalDiff = Convert.ToDouble(UISoundOfTransmissionTotalFee - SoundOfTransmissionTotalValue);
                    if (SoundOfTransmissionFeeDiff >= -1.01 && SoundOfTransmissionFeeDiff <= 1.01)
                    {
                        Premiums.SoundTransmissionFee = UISoundOfTransmissionFee;
                    }
                    if (SoundOfTransmissionIPTDiff >= -1.01 && SoundOfTransmissionIPTDiff <= 1.01)
                    {
                        Premiums.SoundTransmissionIPTFee = UISoundOfTransmissionIPTFee;
                    }
                    if (SoundOfTransmissionTotalDiff >= -1.01 && SoundOfTransmissionTotalDiff <= 1.01)
                    {
                        SoundOfTransmissionTotalValue = UISoundOfTransmissionTotalFee;
                    }

                    Assert.AreEqual(UISoundOfTransmissionFee, Premiums.SoundTransmissionFee, $"SoundOfTransmissionFee Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UISoundOfTransmissionIPTFee, Premiums.SoundTransmissionIPTFee, $"SoundOfTransmissionIPTFee Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UISoundOfTransmissionTotalFee, SoundOfTransmissionTotalValue, $"SoundOfTransmissionTotalValue Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                }
                else
                {
                    Assert.AreEqual(UISoundOfTransmissionFee, Premiums.SoundTransmissionFee, $"SoundOfTransmissionFee Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UISoundOfTransmissionIPTFee, Premiums.SoundTransmissionIPTFee, $"SoundOfTransmissionIPTFee Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UISoundOfTransmissionTotalFee, SoundOfTransmissionTotalValue, $"SoundOfTransmissionTotalValue Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                }
            }

            //Services Elements  and  Fee values  
            //Technical Audit Fee values  
            if (Premiums.ProductServices.Any(x => x.Contains("Technical Audit Fee")))
            {
                var uiTechnicalAuditFee = $"//th[@class='product-title'][text()='{productName}']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[text()='Technical Audit Fee']//following-sibling::td[@class='right-align'][1]";
                var TechnicalAuditFee = Driver.FindElement(By.XPath(uiTechnicalAuditFee)).Text.Replace("£", "");
                double UITechnicalAuditFee = Math.Round(Convert.ToDouble(TechnicalAuditFee), 2);
                double TechnicalAuditFeeDiff;

                var uiTechnicalAuditVATFee = $"//th[@class='product-title'][text()='{productName}']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[text()='Technical Audit Fee']//following-sibling::td[@class='right-align'][2]";
                var TechnicalAuditVATFee = Driver.FindElement(By.XPath(uiTechnicalAuditVATFee)).Text.Replace("£", "");
                double UITechnicalAuditVATFee = Math.Round(Convert.ToDouble(TechnicalAuditVATFee), 2);
                double TechnicalAuditVATDiff;

                var uiTechnicalAuditTotalFee = $"//th[@class='product-title'][text()='{productName}']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[text()='Technical Audit Fee']//following-sibling::th[@class='right-align'][1]";
                var TechnicalAuditTotalFee = Driver.FindElement(By.XPath(uiTechnicalAuditTotalFee)).Text.Replace("£", "");
                double UITechnicalAuditTotalFee = Math.Round(Convert.ToDouble(TechnicalAuditTotalFee), 2);
                double TechnicalAuditTotalValue = Math.Round(Convert.ToDouble(UITechnicalAuditFee + UITechnicalAuditVATFee), 2);
                double TechnicalAuditTotalDiff;

                if (UITechnicalAuditFee != Premiums.TechnicalAuditFee || UITechnicalAuditVATFee != Premiums.TechnicalAuditVATFee || UITechnicalAuditTotalFee != TechnicalAuditTotalValue)
                {
                    TechnicalAuditFeeDiff = Convert.ToDouble(UITechnicalAuditFee - Premiums.TechnicalAuditFee);
                    TechnicalAuditVATDiff = Convert.ToDouble(UITechnicalAuditVATFee - Premiums.TechnicalAuditVATFee);
                    TechnicalAuditTotalDiff = Convert.ToDouble(UITechnicalAuditTotalFee - TechnicalAuditTotalValue);
                    if (TechnicalAuditFeeDiff >= -1.01 && TechnicalAuditFeeDiff <= 1.01)
                    {
                        Premiums.TechnicalAuditFee = UITechnicalAuditFee;
                    }

                    if (TechnicalAuditVATDiff >= -1.01 && TechnicalAuditVATDiff <= 1.01)
                    {
                        Premiums.TechnicalAuditVATFee = UITechnicalAuditVATFee;
                    }
                    if (TechnicalAuditTotalDiff >= -1.01 && TechnicalAuditTotalDiff <= 1.01)
                    {
                        TechnicalAuditTotalValue = UITechnicalAuditTotalFee;
                    }

                    Assert.AreEqual(UITechnicalAuditFee, Premiums.TechnicalAuditFee, $"TechnicalAuditFee Services Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.EstimatedSalePrice}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UITechnicalAuditVATFee, Premiums.TechnicalAuditVATFee, $"TechnicalAuditVATFee Services Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.EstimatedSalePrice}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UITechnicalAuditTotalFee, TechnicalAuditTotalValue, $"TechnicalAuditTotalValue Services Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.EstimatedSalePrice}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                }
                else
                {
                    Assert.AreEqual(UITechnicalAuditFee, Premiums.TechnicalAuditFee, $"TechnicalAuditFee Services Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.EstimatedSalePrice}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UITechnicalAuditVATFee, Premiums.TechnicalAuditVATFee, $"TechnicalAuditVATFee Services Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.EstimatedSalePrice}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UITechnicalAuditTotalFee, TechnicalAuditTotalValue, $"TechnicalAuditTotalValue Services Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.EstimatedSalePrice}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");

                }
            }
            //Administrator Fee values    
            if (Premiums.ProductServices.Any(x => x.Contains("Administration Fee")))
            {

                var uiAdministrationFee = $"//th[@class='product-title'][text()='{productName}']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[text()='Administration Fee']//following-sibling::td[@class='right-align'][1]";
                var AdministrationFee = Driver.FindElement(By.XPath(uiAdministrationFee)).Text.Replace("£", "");
                double UIAdministrationFee = Math.Round(Convert.ToDouble(AdministrationFee), 2);
                double AdministrationFeeDiff;

                var uiAdministrationVATFee = $"//th[@class='product-title'][text()='{productName}']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[text()='Administration Fee']//following-sibling::td[@class='right-align'][2]";
                var AdministrationVATFee = Driver.FindElement(By.XPath(uiAdministrationVATFee)).Text.Replace("£", "");
                double UIAdministrationVATFee = Math.Round(Convert.ToDouble(AdministrationVATFee), 2);
                double AdministrationVATDiff;

                var uiAdministrationTotalFee = $"//th[@class='product-title'][text()='{productName}']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[text()='Administration Fee']//following-sibling::th[@class='right-align'][1]";
                var AdministrationTotalFee = Driver.FindElement(By.XPath(uiAdministrationTotalFee)).Text.Replace("£", "");
                double UIAdministrationTotalFee = Math.Round(Convert.ToDouble(AdministrationTotalFee), 2);
                double AdministrationTotalValue = Math.Round(Convert.ToDouble(UIAdministrationFee + UIAdministrationVATFee), 2);
                double AdministrationTotalDiff;

                if (UIAdministrationFee != Premiums.AdministrationFee || UIAdministrationVATFee != Premiums.AdministrationVATFee || UIAdministrationTotalFee != AdministrationTotalValue)
                {
                    AdministrationFeeDiff = Convert.ToDouble(UIAdministrationFee - Premiums.AdministrationFee);
                    AdministrationVATDiff = Convert.ToDouble(UIAdministrationVATFee - Premiums.AdministrationVATFee);
                    AdministrationTotalDiff = Convert.ToDouble(UIAdministrationTotalFee - AdministrationTotalValue);
                    if (AdministrationFeeDiff >= -1.01 && AdministrationFeeDiff <= 1.01)
                    {
                        Premiums.AdministrationFee = UIAdministrationFee;
                    }

                    if (AdministrationVATDiff >= -1.01 && AdministrationVATDiff <= 1.01)
                    {
                        Premiums.AdministrationVATFee = UIAdministrationVATFee;
                    }
                    if (AdministrationTotalDiff >= 1.01 && AdministrationTotalDiff <= 1.01)
                    {
                        AdministrationTotalValue = UIAdministrationTotalFee;
                    }

                    Assert.AreEqual(UIAdministrationFee, Premiums.AdministrationFee, $"AdministrationFee Services Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.EstimatedSalePrice}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UIAdministrationVATFee, Premiums.AdministrationVATFee, $"AdministrationVATFee Services Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.EstimatedSalePrice}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UIAdministrationTotalFee, AdministrationTotalValue, $"AdministrationTotalValue Services Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.EstimatedSalePrice}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                }
                else
                {
                    Assert.AreEqual(UIAdministrationFee, Premiums.AdministrationFee, $"AdministrationFee Services Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.EstimatedSalePrice}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UIAdministrationVATFee, Premiums.AdministrationVATFee, $"AdministrationVATFee Services Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.EstimatedSalePrice}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UIAdministrationTotalFee, AdministrationTotalValue, $"AdministrationTotalValue Services Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.EstimatedSalePrice}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                }
            }
            if (Premiums.ProductServices.Any(x => x.Contains("Atkins Fee")))
            {

                var uiAtkinsFee = $"//th[@class='product-title'][text()='{productName}']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[text()='Atkins Fee']//following-sibling::td[@class='right-align'][1]";
                var AtkinsFee = Driver.FindElement(By.XPath(uiAtkinsFee)).Text.Replace("£", "");
                double UIAtkinsFee = Math.Round(Convert.ToDouble(AtkinsFee), 2);
                double AtkinsFeeDiff;

                var uiAtkinsVATFee = $"//th[@class='product-title'][text()='{productName}']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[text()='Atkins Fee']//following-sibling::td[@class='right-align'][2]";
                var AtkinsVATFee = Driver.FindElement(By.XPath(uiAtkinsVATFee)).Text.Replace("£", "");
                double UIAtkinsVATFee = Math.Round(Convert.ToDouble(AtkinsVATFee), 2);
                double AtkinsVATDiff;

                var uiAtkinsTotalFee = $"//th[@class='product-title'][text()='{productName}']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[text()='Atkins Fee']//following-sibling::th[@class='right-align'][1]";
                var AtkinsTotalFee = Driver.FindElement(By.XPath(uiAtkinsTotalFee)).Text.Replace("£", "");
                double UIAtkinsTotalFee = Math.Round(Convert.ToDouble(AtkinsTotalFee), 2);
                double AtkinsTotalValue = Math.Round(Convert.ToDouble(UIAtkinsFee + UIAtkinsVATFee), 2);
                double AtkinsTotalDiff;

                if (UIAtkinsFee != Premiums.AtkinsFee || UIAtkinsVATFee != Premiums.AtkinsVATFee || UIAtkinsTotalFee != AtkinsTotalValue)
                {
                    AtkinsFeeDiff = Convert.ToDouble(Premiums.AtkinsFee - UIAtkinsFee);
                    AtkinsVATDiff = Convert.ToDouble(Premiums.AtkinsVATFee - UIAtkinsVATFee);
                    AtkinsTotalDiff = Convert.ToDouble(AtkinsTotalValue - UIAtkinsTotalFee);
                    if (AtkinsFeeDiff >= -1.01 && AtkinsFeeDiff <= 1.01)
                    {
                        Premiums.AtkinsFee = UIAtkinsFee;
                    }
                    if (AtkinsVATDiff >= -1.01 && AtkinsVATDiff <= 1.01)
                    {
                        Premiums.AtkinsVATFee = UIAtkinsVATFee;
                    }
                    if (AtkinsTotalDiff >= -1.01 && AtkinsTotalDiff <= 1.01)
                    {
                        AtkinsTotalValue = UIAtkinsTotalFee;
                    }

                    Assert.AreEqual(UIAtkinsFee, Premiums.AtkinsFee, $"AtkinsFee Services Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.EstimatedSalePrice}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UIAtkinsVATFee, Premiums.AtkinsVATFee, $"AtkinsVATFee Services Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.EstimatedSalePrice}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UIAtkinsTotalFee, AtkinsTotalValue, $"AtkinsTotalValue Services Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.EstimatedSalePrice}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                }
                else
                {
                    Assert.AreEqual(UIAtkinsFee, Premiums.AtkinsFee, $"AtkinsFee Services Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.EstimatedSalePrice}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UIAtkinsVATFee, Premiums.AtkinsVATFee, $"AtkinsVATFee Services Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.EstimatedSalePrice}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UIAtkinsTotalFee, AtkinsTotalValue, $"AtkinsTotalValue Services Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.EstimatedSalePrice}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                }
            }

            //Cancellation Fee values  
            if (Premiums.ProductServices.Any(x => x.Contains("Cancellation Fee")))
            {


                var uiCancellationFee = $"//th[@class='product-title'][text()='{productName}']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[text()='Cancellation Fee']//following-sibling::td[@class='right-align'][1]";
                var CancellationFee = Driver.FindElement(By.XPath(uiCancellationFee)).Text.Replace("£", "");
                double UICancellationFee = Math.Round(Convert.ToDouble(CancellationFee), 2);
                double CancellationFeeDiff;

                var uiCancellationVATFee = $"//th[@class='product-title'][text()='{productName}']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[text()='Cancellation Fee']//following-sibling::td[@class='right-align'][2]";
                var CancellationVATFee = Driver.FindElement(By.XPath(uiCancellationVATFee)).Text.Replace("£", "");
                double UICancellationVATFee = Math.Round(Convert.ToDouble(CancellationVATFee), 2);
                double CancellationVATDiff;

                var uiCancellationTotalFee = $"//th[@class='product-title'][text()='{productName}']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[text()='Cancellation Fee']//following-sibling::td[@class='right-align'][1]";
                var CancellationTotalFee = Driver.FindElement(By.XPath(uiCancellationTotalFee)).Text.Replace("£", "");
                double UICancellationTotalFee = Math.Round(Convert.ToDouble(CancellationTotalFee), 2);
                double CancellationTotalValue = Math.Round(Convert.ToDouble(UICancellationFee + UICancellationVATFee), 2);
                double CancellationTotalDiff;


                if (UICancellationFee != Premiums.CancellationFee || UICancellationVATFee != Premiums.CancellationVATFee || UICancellationTotalFee != CancellationTotalValue)
                {
                    CancellationFeeDiff = Convert.ToDouble(Premiums.CancellationFee - UICancellationFee);
                    CancellationVATDiff = Convert.ToDouble(Premiums.CancellationVATFee - UICancellationVATFee);
                    CancellationTotalDiff = Convert.ToDouble(CancellationTotalValue - UICancellationTotalFee);
                    if (CancellationFeeDiff >= -1.01 && CancellationFeeDiff <= 1.01)
                    {
                        Premiums.CancellationFee = UICancellationFee;
                    }

                    if (CancellationVATDiff >= -1.01 && CancellationVATDiff <= 1.01)
                    {
                        Premiums.CancellationVATFee = UICancellationVATFee;
                    }
                    if (CancellationTotalDiff >= -1.01 && CancellationTotalDiff <= 1.01)
                    {
                        CancellationTotalValue = UICancellationTotalFee;
                    }

                    Assert.AreEqual(UICancellationFee, Premiums.CancellationFee, $"CancellationFee Services Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UICancellationVATFee, Premiums.CancellationVATFee, $"CancellationVATFee Services Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UICancellationTotalFee, CancellationTotalValue, $"CancellationTotalValue Services Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                }
                else
                {
                    Assert.AreEqual(UICancellationFee, Premiums.CancellationFee, $"CancellationFee Services Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UICancellationVATFee, Premiums.CancellationVATFee, $"CancellationVATFee Services Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UICancellationTotalFee, CancellationTotalValue, $"CancellationTotalValue Services Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                }
            }
        }

        public DataTable ReadPrivateRentalHVSMasterPremiumsPriceData(string fileName)
        {

            var result = new DataSet();                                                                             //Set the First Row as Column Name
            using (var stream = File.Open(fileName, FileMode.Open, FileAccess.Read))
            using (var excelReader = ExcelReaderFactory.CreateOpenXmlReader(stream)) //.xlsx
            {
                //Return as DataSet
                result = excelReader.AsDataSet(new ExcelDataSetConfiguration()
                {
                    ConfigureDataTable = (_) => new ExcelDataTableConfiguration()
                    {
                        UseHeaderRow = true
                    }
                });
            }
            //Get all the Tables
            DataTableCollection table = result.Tables;
            //Reading Last Sheet From Product Pricing Setup 
            DataTable resultTable = table["Private Rental - HVS (1)"];
            return resultTable;
        }
    }
}
