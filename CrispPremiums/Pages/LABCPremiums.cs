﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.PageObjects;
using RegressionPacks.Support;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace RegressionPacks.Pages
{
    public class LABCPremiums : Support.Pages
    {
        public IWebDriver wdriver;
        public string RatingValue;
        string masterplotdata;

        public LABCPremiums(ISearchContext driver)
        {
            PageFactory.InitElements(driver, this);
            wdriver = (RemoteWebDriver)driver;
        }       
        public void LABCNewhomesPremiums()
        {
            masterplotdata = @"\\tpgfile\NewCompany\IT\Department\QA Team\Current Projects\CRISP\CrispAutomation\CrispData\Premiums\NHPremiums\NH-MasterPlotData.xlsx";
            ExcelPlay.ReadExcelPlotData(masterplotdata);
            CreateAndSendQuoteToVerifyPremiums();
        }
        public void LABCSocialHousingPremiums()
        {
            masterplotdata = @"\\tpgfile\NewCompany\IT\Department\QA Team\Current Projects\CRISP\CrispAutomation\CrispData\Premiums\SHPremiums\SH-MasterPlotData.xlsx";
            ExcelPlay.ReadExcelPlotData(masterplotdata);
            CreateAndSendQuoteToVerifyPremiums();

        }
        public void LABCPrivateRentalPremiums()
        {
            masterplotdata = @"\\tpgfile\NewCompany\IT\Department\QA Team\Current Projects\CRISP\CrispAutomation\CrispData\Premiums\PRSPremiums\PRS-MasterPlotData.xlsx";
            ExcelPlay.ReadExcelPlotData(masterplotdata);
            CreateAndSendQuoteToVerifyPremiums();
        }
        public void LABCCommercialPremiums()
        {
            masterplotdata = @"\\tpgfile\NewCompany\IT\Department\QA Team\Current Projects\CRISP\CrispAutomation\CrispData\Premiums\CommPremiums\Comm-MasterPlotData.xlsx";
            ExcelPlay.ReadExcelPlotData(masterplotdata);
            CreateAndSendQuoteToVerifyPremiums();           
        }
        public void LABCCompletedHousingPremiums()
        {
            masterplotdata = @"\\tpgfile\NewCompany\IT\Department\QA Team\Current Projects\CRISP\CrispAutomation\CrispData\Premiums\CompPremiums\Comp-MasterPlotData.xlsx";
            ExcelPlay.ReadExcelPlotData(masterplotdata);
            CreateAndSendQuoteToVerifyPremiums();
        }
        public void LABCSelfBuildPremiums()
        {
            masterplotdata = @"\\tpgfile\NewCompany\IT\Department\QA Team\Current Projects\CRISP\CrispAutomation\CrispData\Premiums\SBPremiums\SB-MasterPlotData.xlsx";
            ExcelPlay.ReadExcelPlotData(masterplotdata);
            CreateAndSendQuoteToVerifyPremiums();
        }


        public void LABCSocialHousingPremiumsFor12YearsCoverLength()
        {
            masterplotdata = @"\\tpgfile\NewCompany\IT\Department\QA Team\Current Projects\CRISP\CrispAutomation\CrispData\Premiums\SHPremiums\SH-MasterPlotData.xlsx";
            ExcelPlay.ReadExcelPlotData(masterplotdata);
            CreateAndSendQuoteToVerifyPremiumsFor12YearsCoverLength();

        }
        public void LABCPrivateRentalPremiumsFor12YearsCoverLength()
        {
            masterplotdata = @"\\tpgfile\NewCompany\IT\Department\QA Team\Current Projects\CRISP\CrispAutomation\CrispData\Premiums\PRSPremiums\PRS-MasterPlotData.xlsx";
            ExcelPlay.ReadExcelPlotData(masterplotdata);
            CreateAndSendQuoteToVerifyPremiumsFor12YearsCoverLength();
        }
        public void LABCCommercialPremiumsFor12YearsCoverLength()
        {
            masterplotdata = @"\\tpgfile\NewCompany\IT\Department\QA Team\Current Projects\CRISP\CrispAutomation\CrispData\Premiums\CommPremiums\Comm-MasterPlotData.xlsx";
            ExcelPlay.ReadExcelPlotData(masterplotdata);
            CreateAndSendQuoteToVerifyPremiumsFor12YearsCoverLength();
        }


        //Create a Quote From Master Plot Schedule Data 
        public void CreateAndSendQuoteToVerifyPremiums()
        {          
                // step3: create quote               
                WaitForElement(Dashboardpage.PlusButton);
                AddLABCQuotePage.SubmitQuote(masterplotdata);
                // step4: send quote and Iterate for ratings               
                SendQuotePage.ClickOnSendQuoteButton();

                if (Premiums.eachplotrow.Any(o => o.ProductName.Equals("New Homes")) ||
                    Premiums.eachplotrow.Any(o => o.ProductName.Equals("Social Housing")) ||
                    Premiums.eachplotrow.Any(o => o.ProductName.Equals("Private Rental")))
                {
                    SendQuoteRatingPage();
                }
                if (Premiums.eachplotrow.Any(o => o.ProductName.Equals("Commercial")))
                {
                    SendQuotePageBuilderExpPage();
                }
                if (Premiums.eachplotrow.Any(o => o.ProductName.Equals("Completed Housing")) || Premiums.eachplotrow.Any(o => o.ProductName.Equals("Self Build")))
                {
                    SendQuotePage.SendQuoteSecuritiesPage();
                    SendQuotePage.SendQuoteConditionsPage();
                    SendQuotePage.SendQuotepecialTermsPage();
                    SendQuoteFeesDetailsPage();                    
                }            
        }
        //Create a Quote From Master Plot Schedule Data 
        public void CreateAndSendQuoteToVerifyPremiumsFor12YearsCoverLength()
        {
            // step3: create quote               
            WaitForElement(Dashboardpage.PlusButton);
            AddLABCQuotePage.SubmitQuoteFor12YearsCoverLength(masterplotdata);
            // step4: send quote and Iterate for ratings               
            SendQuotePage.ClickOnSendQuoteButton();

            if (Premiums.eachplotrow.Any(o => o.ProductName.Equals("New Homes")) ||
                Premiums.eachplotrow.Any(o => o.ProductName.Equals("Social Housing")) ||
                Premiums.eachplotrow.Any(o => o.ProductName.Equals("Private Rental")))
            {
                SendQuoteRatingPage();
            }
            if (Premiums.eachplotrow.Any(o => o.ProductName.Equals("Commercial")))
            {
                SendQuotePageBuilderExpPage();
            }
            if (Premiums.eachplotrow.Any(o => o.ProductName.Equals("Completed Housing")) || Premiums.eachplotrow.Any(o => o.ProductName.Equals("Self Build")))
            {
                SendQuotePage.SendQuoteSecuritiesPage();
                SendQuotePage.SendQuoteConditionsPage();
                SendQuotePage.SendQuotepecialTermsPage();
                SendQuoteFeesDetailsPage();
            }
        }
        public void SendQuoteRatingPage()
        {
            Thread.Sleep(1000);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(1000);
            SendQuotePage.AddRatingDetails();
            WaitForElement(SendQuotePage.TechgRatingDropdown);
            Thread.Sleep(1000);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(1000);
            SendQuotePage.TechgRatingDropdown.Click();
            Thread.Sleep(1000);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(1000);
            WaitForElements(SendQuotePage.TechgRatingDropdownInput);
            var ratingCount = SendQuotePage.TechgRatingDropdownInput;
            // var ratingValues = ratingCount.Count;
            int[] ratingValues = { 0, 5, 10, 12, 21, 25 };
            for (int i = 0; i < ratingValues.Length; i++)
            {

                if (i != 0)
                {
                    Thread.Sleep(3000);
                    WaitForElement(SendQuotePage.TechgRatingDropdown);
                    Thread.Sleep(2000);
                    SendQuotePage.TechgRatingDropdown.Click();
                    Thread.Sleep(2000);
                    WaitForElements(SendQuotePage.TechgRatingDropdownInput);
                }

                RatingValue = SendQuotePage.TechgRatingDropdownInput.ElementAt(ratingValues[i]).Text;
                Premiums.Rating = RatingValue.ToString();
                Thread.Sleep(3000);
                var item = $"//ul[@class='dropdown-content select-dropdown active']/li[{ratingValues[i] + 1}]/span";
                Thread.Sleep(3000);
                Driver.FindElement(By.XPath(item)).Click();
                // Console.WriteLine(RatingValue);
                WaitForElement(SendQuotePage.RatingComment);
                SendQuotePage.RatingComment.Click();
                WaitForElement(SendQuotePage.RatingCommentText);
                SendQuotePage.RatingCommentText.Clear();
                SendQuotePage.RatingCommentText.SendKeys("good rate");
                Thread.Sleep(2000);
                SendQuoteNextButton();
                //SecuritiesDetails Page
                SendQuotePage.SendQuoteSecuritiesPage();
                if(Premiums.eachplotrow.Any(o => o.ProductName.Contains("New Homes")) && Premiums.eachplotrow.Any(o => o.ConstructionType.Contains("Conversion")) || Premiums.eachplotrow.Any(o => o.ProductName.Contains("Social Housing")) && Premiums.eachplotrow.Any(o => o.ConstructionType.Contains("Conversion")))
                {
                    if (ratingValues[i] < 23)
                    {
                        SendQuotePage.SendQuoteConditionsPage();
                        SendQuotePage.SendQuotepecialTermsPage();
                        SendQuoteFeesDetailsPage();
                        if (i != ratingValues.Length - 1)
                        {                          
                            WaitForElement(SendQuotePage.PreviousButton);                       
                            SendQuotePage.PreviousButton.Click();
                            Thread.Sleep(500);
                            CloseSpinneronDiv();
                            CloseSpinneronPage();
                            Thread.Sleep(500);
                            WaitForElement(SendQuotePage.PreviousButton);
                            SendQuotePage.PreviousButton.Click();
                            Thread.Sleep(500);
                            CloseSpinneronDiv();
                            CloseSpinneronPage();
                            Thread.Sleep(500);
                            WaitForElement(SendQuotePage.PreviousButton);                     
                            SendQuotePage.PreviousButton.Click();
                            Thread.Sleep(500);
                            CloseSpinneronDiv();
                            CloseSpinneronPage();
                            Thread.Sleep(500);
                            WaitForElement(SendQuotePage.PreviousButton);             
                            SendQuotePage.PreviousButton.Click();
                            Thread.Sleep(500);
                            CloseSpinneronDiv();
                            CloseSpinneronPage();
                            Thread.Sleep(500);

                        }
                        else
                        {
                            WaitForElement(SendQuotePage.CancelButton);
                            SendQuotePage.CancelButton.Click();
                            Thread.Sleep(500);
                            CloseSpinneronDiv();
                            CloseSpinneronPage();
                            Thread.Sleep(500);
                            WaitForElement(SendQuotePage.CancelOKButton);
                            SendQuotePage.CancelOKButton.Click();
                            Thread.Sleep(500);
                            CloseSpinneronDiv();
                            CloseSpinneronPage();
                            Thread.Sleep(500);
                        }
                    }
                    else
                    {
                        WaitForElement(SendQuotePage.CancelButton);
                        SendQuotePage.CancelButton.Click();
                        Thread.Sleep(500);
                        CloseSpinneronDiv();
                        CloseSpinneronPage();
                        Thread.Sleep(500);
                        WaitForElement(SendQuotePage.CancelOKButton);
                        SendQuotePage.CancelOKButton.Click();
                        Thread.Sleep(500);
                        CloseSpinneronDiv();
                        CloseSpinneronPage();
                        Thread.Sleep(500);
                    }
                }
                else
                {
                    SendQuotePage.SendQuoteConditionsPage();
                    SendQuotePage.SendQuotepecialTermsPage();
                    SendQuoteFeesDetailsPage();
                    if (i != ratingValues.Length - 1)
                    {
                        Thread.Sleep(500);                      
                        CloseSpinneronDiv();
                        CloseSpinneronPage();
                        Thread.Sleep(500);
                        WaitForElement(SendQuotePage.PreviousButton);                        
                        SendQuotePage.PreviousButton.Click();
                        Thread.Sleep(500);                       
                        CloseSpinneronDiv();
                        CloseSpinneronPage();
                        Thread.Sleep(500);
                        WaitForElement(SendQuotePage.PreviousButton);                     
                        SendQuotePage.PreviousButton.Click();
                        Thread.Sleep(500);
                        CloseSpinneronDiv();
                        CloseSpinneronPage();
                        Thread.Sleep(500);
                        WaitForElement(SendQuotePage.PreviousButton);                   
                        SendQuotePage.PreviousButton.Click();
                        Thread.Sleep(500);
                        CloseSpinneronDiv();
                        CloseSpinneronPage();
                        Thread.Sleep(500);
                        WaitForElement(SendQuotePage.PreviousButton);                 
                        SendQuotePage.PreviousButton.Click();
                        Thread.Sleep(500);
                        CloseSpinneronDiv();
                        CloseSpinneronPage();
                        Thread.Sleep(500);
                    }
                    else
                    {
                        WaitForElement(SendQuotePage.CancelButton);
                        SendQuotePage.CancelButton.Click();
                        Thread.Sleep(1000);
                        CloseSpinneronDiv();
                        CloseSpinneronPage();
                        Thread.Sleep(1000);
                        WaitForElement(SendQuotePage.CancelOKButton);
                        SendQuotePage.CancelOKButton.Click();
                        Thread.Sleep(1000);
                        CloseSpinneronDiv();
                        CloseSpinneronPage();
                        Thread.Sleep(1000);
                    }
                }
            }
        }
        public void SendQuotePageBuilderExpPage()
        {
            Thread.Sleep(1000);         
            WaitForElement(SendQuotePage.SendQuoteWizard);
            int[] ratingValues = {1, 4, 12 };
            // var ratingValues = ratingCount.Count;
            for (int i = 0; i < ratingValues.Length; i++)
            {
                Thread.Sleep(2000);
                WaitForElements(SendQuotePage.BuildersExperienceLabel);
                SendQuotePage.BuildersExperienceLabel[0].Click();
                Thread.Sleep(500);
                WaitForElement(SendQuotePage.BuildersExperienceInput);
                SendQuotePage.BuildersExperienceInput.Click();
                SendQuotePage.BuildersExperienceInput.Clear();
                SendQuotePage.BuildersExperienceInput.SendKeys(ratingValues[i].ToString());
                var buildingexpvalue = ratingValues[i].ToString();
                //  var buildingexpvalue = SendQuotePagePage.BuildersExperienceInput.GetAttribute(i.ToString());
                Premiums.BuilderExperiance = buildingexpvalue;
                Thread.Sleep(2000);
                SendQuoteNextButton();
                //SecuritiesDetails Page
                SendQuotePage.SendQuoteSecuritiesPage();
                SendQuotePage.SendQuoteConditionsPage();
                SendQuotePage.SendQuotepecialTermsPage();
                SendQuoteFeesDetailsPage();
                if (i != ratingValues.Length - 1)
                {      
                    Thread.Sleep(1000);
                    WaitForElement(SendQuotePage.PreviousButton);
                    Thread.Sleep(1000);
                    SendQuotePage.PreviousButton.Click();
                    Thread.Sleep(1000);
                  
                    Thread.Sleep(1000);
                    WaitForElement(SendQuotePage.PreviousButton);
                    Thread.Sleep(1000);
                    SendQuotePage.PreviousButton.Click();
                    Thread.Sleep(1000);
                
                    Thread.Sleep(1000);
                    WaitForElement(SendQuotePage.PreviousButton);
                    Thread.Sleep(2000);
                    SendQuotePage.PreviousButton.Click();
                    Thread.Sleep(1000);
              
                    Thread.Sleep(1000);
                    WaitForElement(SendQuotePage.PreviousButton);
                    Thread.Sleep(1000);
                    SendQuotePage.PreviousButton.Click();
                    Thread.Sleep(1000);
                }
            }
            WaitForElement(SendQuotePage.CancelButton);
            SendQuotePage.CancelButton.Click();
            Thread.Sleep(1000);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(1000);
            WaitForElement(SendQuotePage.CancelOKButton);
            SendQuotePage.CancelOKButton.Click();
            Thread.Sleep(1000);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(1000);

        }
        public void SendQuoteNextButton()
        {
            Thread.Sleep(500);            
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            WaitForElement(SendQuotePage.SendQuoteNextButton);  
            SendQuotePage.SendQuoteNextButton.Click();
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
        }
        public void SendQuoteFeesDetailsPage()
        {
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(1000);
            WaitForElements(SendQuotePage.FeesEle);
            Thread.Sleep(500);
            Assert.IsTrue(SendQuotePage.FeesEle.Count > 0);
            //Console.WriteLine("Fees details verified");
            Thread.Sleep(1000);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(1000);
            Premiums.Plots = SendQuotePage.PlotCount.Text;           
            //CoverlengthDetails 
            Thread.Sleep(1000);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(1000);
            WaitForElement(SendQuotePage.CoverLength);
            var CoverLengthValue = SendQuotePage.CoverLength.Text;
            Thread.Sleep(500);
            Premiums.CoverLengthYears = Convert.ToDouble(CoverLengthValue);
            //verify Cover Premiums Caluclations                        
            CaluclatePremiums();            
            Console.WriteLine($"Premim Details are Verified at {Premiums.Rating} : Structural Premium =  {Premiums.StructuralFee} , Insolvancy Premium = {Premiums.InsolvencyFee}, TA Fee = {Premiums.TechnicalAuditFee}");
            Thread.Sleep(2000);
        }

        //Verify Premium Details  
        public void CaluclatePremiums()
        {
            if (Premiums.eachplotrow.Any(o => o.ProductName == "New Homes"))
            {
                NewhomesPremiums.NewHomesPremiumsCaluclations();
            }
            if (Premiums.eachplotrow.Any(o => o.ProductName == "Social Housing"))
            {
                SocialHousingPremiums.SocialHousingPremiumsCaluclations();
            }
            if (Premiums.eachplotrow.Any(o => o.ProductName == "Private Rental"))
            {
                PrivateRentalPremiums.PrivateRentalPremiumsCaluclations();
            }
            if (Premiums.eachplotrow.Any(o => o.ProductName == "Self Build"))
            {
                SelfBuildPremiums.SelfBuildPremiumsCalcs();
            }
            if (Premiums.eachplotrow.Any(o => o.ProductName == "Commercial"))
            {
                CommercialPremiums.CommercialPremiumsCaluclations();
            }
         
            if (Premiums.eachplotrow.Any(o => o.ProductName == "Completed Housing"))
            {
                CompletedHousingPremiums.CompletedHousingPremiumCalcs();
            }

        }
    }
}
