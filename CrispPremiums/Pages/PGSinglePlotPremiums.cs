﻿using OpenQA.Selenium;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.PageObjects;
using RegressionPacks.Support;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace RegressionPacks.Pages
{
    public class PGSinglePlotPremiums : Support.Pages
    {
        public IWebDriver wdriver;
        public string RatingValue;
        string masterplotdata;

        public PGSinglePlotPremiums(ISearchContext driver)
        {
            PageFactory.InitElements(driver, this);
            wdriver = (RemoteWebDriver)driver;
        }
        public void PGNewhomesPremiums()
        {
            masterplotdata = @"\\tpgfile\NewCompany\IT\Department\QA Team\Current Projects\CRISP\CrispAutomation\CrispData\Premiums\NHPremiums\NH-PGMasterPlotData.xlsx";
            var dtExcel = ExcelPlay.ExcelToDataTable(masterplotdata);
            for (int i = 0; i < dtExcel.Rows.Count; i++)
            {
                ExcelPlay.CreateSpreadsheet(dtExcel.Rows[i]); 
                CreateAndSendQuoteToVerifyPremiums();
            }
        }
        public void PGSocialHousingPremiums()
        {
            masterplotdata = @"\\tpgfile\NewCompany\IT\Department\QA Team\Current Projects\CRISP\CrispAutomation\CrispData\Premiums\SHPremiums\SH-PGMasterPlotData.xlsx";
            var dtExcel = ExcelPlay.ExcelToDataTable(masterplotdata);
            for (int i = 0; i < dtExcel.Rows.Count; i++)
            {
                ExcelPlay.CreateSpreadsheet(dtExcel.Rows[i]);               
                CreateAndSendQuoteToVerifyPremiums();
            }
        }
        public void PGPrivateRentalPremiums()
        {
            masterplotdata = @"\\tpgfile\NewCompany\IT\Department\QA Team\Current Projects\CRISP\CrispAutomation\CrispData\Premiums\PRSPremiums\PRS-PGMasterPlotData.xlsx";
            var dtExcel = ExcelPlay.ExcelToDataTable(masterplotdata);
            for (int i = 0; i < dtExcel.Rows.Count; i++)
            {
                ExcelPlay.CreateSpreadsheet(dtExcel.Rows[i]);              
                CreateAndSendQuoteToVerifyPremiums();
            }
        }
        public void PGCommercialPremiums()
        {
            masterplotdata = @"\\tpgfile\NewCompany\IT\Department\QA Team\Current Projects\CRISP\CrispAutomation\CrispData\Premiums\CommPremiums\Comm-PGMasterPlotData.xlsx";
            var dtExcel = ExcelPlay.ExcelToDataTable(masterplotdata);
            for (int i = 0; i < dtExcel.Rows.Count; i++)
            {
                ExcelPlay.CreateSpreadsheet(dtExcel.Rows[i]);              
                CreateAndSendQuoteToVerifyPremiums();
            }
        }
        public void PGCompletedHousingPremiums()
        {
            masterplotdata = @"\\tpgfile\NewCompany\IT\Department\QA Team\Current Projects\CRISP\CrispAutomation\CrispData\Premiums\CompPremiums\Comp-PGMasterPlotData.xlsx";
            var dtExcel = ExcelPlay.ExcelToDataTable(masterplotdata);
            for (int i = 0; i < dtExcel.Rows.Count; i++)
            {
                ExcelPlay.CreateSpreadsheet(dtExcel.Rows[i]);              
                CreateAndSendQuoteToVerifyPremiums();
            }
        }
        public void PGSelfBuildPremiums()
        {
            masterplotdata = @"\\tpgfile\NewCompany\IT\Department\QA Team\Current Projects\CRISP\CrispAutomation\CrispData\Premiums\SBPremiums\SB-PGMasterPlotData.xlsx";
            var dtExcel = ExcelPlay.ExcelToDataTable(masterplotdata);
            for (int i = 0; i < dtExcel.Rows.Count; i++)
            {
                ExcelPlay.CreateSpreadsheet(dtExcel.Rows[i]);             
                CreateAndSendQuoteToVerifyPremiums();
            }
        }
        public void PGCIResidentialPremiums()
        {
            masterplotdata = @"\\tpgfile\NewCompany\IT\Department\QA Team\Current Projects\CRISP\CrispAutomation\CrispData\Premiums\CIPremiums\CIResi-PGMasterPlotData.xlsx";
            var dtExcel = ExcelPlay.ExcelToDataTable(masterplotdata);
            for (int i = 0; i < dtExcel.Rows.Count; i++)
            {
                ExcelPlay.CreateSpreadsheet(dtExcel.Rows[i]);             
                CreateAndSendQuoteToVerifyPremiums();
            }
        }
        public void PGCICommercialPremiums()
        {
            masterplotdata = @"\\tpgfile\NewCompany\IT\Department\QA Team\Current Projects\CRISP\CrispAutomation\CrispData\Premiums\CIPremiums\CIComm-PGMasterPlotData.xlsx";
            var dtExcel = ExcelPlay.ExcelToDataTable(masterplotdata);
            for (int i = 0; i < dtExcel.Rows.Count; i++)
            {
                ExcelPlay.CreateSpreadsheet(dtExcel.Rows[i]);             
                CreateAndSendQuoteToVerifyPremiums();
            }
        }

        public void ResidentialBCPremium()
        {
            masterplotdata = @"\\tpgfile\NewCompany\IT\Department\QA Team\Current Projects\CRISP\CrispAutomation\CrispData\Premiums\ResiBCPremiums\ResiBC-PGMasterPlotData.xlsx";
            var dtExcel = ExcelPlay.ExcelToDataTable(masterplotdata);
            for (int i = 0; i < dtExcel.Rows.Count; i++)
            {
                ExcelPlay.CreateSpreadsheet(dtExcel.Rows[i]);              
                CreateAndSendQuoteToVerifyPremiums();
            }
        }
        public void CommercialBCPremium()
        {
            masterplotdata = @"\\tpgfile\NewCompany\IT\Department\QA Team\Current Projects\CRISP\CrispAutomation\CrispData\Premiums\CommBCPremiums\CommBC-PGMasterPlotData.xlsx";
            var dtExcel = ExcelPlay.ExcelToDataTable(masterplotdata);
            for (int i = 0; i < dtExcel.Rows.Count; i++)
            {
                ExcelPlay.CreateSpreadsheet(dtExcel.Rows[i]);         
                CreateAndSendQuoteToVerifyPremiums();
            }
        }

        //12Years Cover Length
        public void PGSocialHousingPremiumsFor12YearsCoverLength()
        {
            masterplotdata = @"\\tpgfile\NewCompany\IT\Department\QA Team\Current Projects\CRISP\CrispAutomation\CrispData\Premiums\SHPremiums\SH-PGMasterPlotData.xlsx";
            var dtExcel = ExcelPlay.ExcelToDataTable(masterplotdata);
            for (int i = 0; i < dtExcel.Rows.Count; i++)
            {
                ExcelPlay.CreateSpreadsheet(dtExcel.Rows[i]);
                CreateAndSendQuoteToVerifyPremiumsFor12YearsCoverLength();
            }
        }
        public void PGPrivateRentalPremiumsFor12YearsCoverLength()
        {
            masterplotdata = @"\\tpgfile\NewCompany\IT\Department\QA Team\Current Projects\CRISP\CrispAutomation\CrispData\Premiums\PRSPremiums\PRS-PGMasterPlotData.xlsx";
            var dtExcel = ExcelPlay.ExcelToDataTable(masterplotdata);
            for (int i = 0; i < dtExcel.Rows.Count; i++)
            {
                ExcelPlay.CreateSpreadsheet(dtExcel.Rows[i]);
                CreateAndSendQuoteToVerifyPremiumsFor12YearsCoverLength();
            }
        }
        public void PGCommercialPremiumsFor12YearsCoverLength()
        {
            masterplotdata = @"\\tpgfile\NewCompany\IT\Department\QA Team\Current Projects\CRISP\CrispAutomation\CrispData\Premiums\CommPremiums\Comm-PGMasterPlotData.xlsx";
            var dtExcel = ExcelPlay.ExcelToDataTable(masterplotdata);
            for (int i = 0; i < dtExcel.Rows.Count; i++)
            {
                ExcelPlay.CreateSpreadsheet(dtExcel.Rows[i]);
                CreateAndSendQuoteToVerifyPremiumsFor12YearsCoverLength();
            }
        }
    
        public void PGCIResidentialPremiumsFor12YearsCoverLength()
        {
            masterplotdata = @"\\tpgfile\NewCompany\IT\Department\QA Team\Current Projects\CRISP\CrispAutomation\CrispData\Premiums\CIPremiums\CIResi-PGMasterPlotData.xlsx";
            var dtExcel = ExcelPlay.ExcelToDataTable(masterplotdata);
            for (int i = 0; i < dtExcel.Rows.Count; i++)
            {
                ExcelPlay.CreateSpreadsheet(dtExcel.Rows[i]);
                CreateAndSendQuoteToVerifyPremiumsFor12YearsCoverLength();
            }
        }
        public void PGCICommercialPremiumsFor12YearsCoverLength()
        {
            masterplotdata = @"\\tpgfile\NewCompany\IT\Department\QA Team\Current Projects\CRISP\CrispAutomation\CrispData\Premiums\CIPremiums\CIComm-PGMasterPlotData.xlsx";
            var dtExcel = ExcelPlay.ExcelToDataTable(masterplotdata);
            for (int i = 0; i < dtExcel.Rows.Count; i++)
            {
                ExcelPlay.CreateSpreadsheet(dtExcel.Rows[i]);
                CreateAndSendQuoteToVerifyPremiumsFor12YearsCoverLength();
            }
        }   



        //Create a Quote From Master Plot Schedule Data 
        public void CreateAndSendQuoteToVerifyPremiums()
        {
            // step3: create quote               
            WaitForElement(Dashboardpage.PlusButton);
            AddPGQuotePage.SubmitQuoteOnSinglePlot();
            // step4: send quote and Iterate for ratings               
            SendQuotePage.ClickOnSendQuoteButton();
            if (Premiums.eachplotrow.Any(o => o.ProductName.Equals("New Homes")) ||
                Premiums.eachplotrow.Any(o => o.ProductName.Equals("Social Housing")) ||
                Premiums.eachplotrow.Any(o => o.ProductName.Equals("Private Rental")))
            {
                PGPremiums.SendQuoteRatingPage();
            }
            if (Premiums.eachplotrow.Any(o => o.ProductName.Equals("Commercial")))
            {
                PGPremiums.SendQuotePageBuilderExpPage();
            }
            if (Premiums.eachplotrow.Any(o => o.ProductName.Equals("Completed Housing")) || Premiums.eachplotrow.Any(o => o.ProductName.Equals("Self Build")))
            {
                if (SendQuotePage.RatingEle.Count > 0)
                {
                    PGPremiums.SendQuoteRatingPagePremiums();
                }
                SendQuotePage.SendQuoteSecuritiesPage();
                SendQuotePage.SendQuoteConditionsPage();
                SendQuotePage.SendQuotepecialTermsPage();
                PGPremiums.SendQuoteFeesDetailsPage();
            }
            if (Premiums.eachplotrow.Any(o => o.ProductName.Equals("Channel Islands Residential")) || Premiums.eachplotrow.Any(o => o.ProductName.Contains("Channel Islands Commercial")))
            {
                PGPremiums.SendQuotePageBuilderExpPage();
            }

            if (Premiums.eachplotrow.Any(o => o.BCProduct.Equals("Residential Building Control")) && (Premiums.eachplotrow.All(o => o.ProductName == "")))
            {
                PGPremiums.SendQuoteRatingPage();
            }
            if (Premiums.eachplotrow.Any(o => o.BCProduct.Equals("Commercial Building Control")) && (Premiums.eachplotrow.All(o => o.ProductName == "")))
            {
                SendQuotePage.SendQuoteRatingPage();
                SendQuotePage.SendQuoteSecuritiesPage();
                SendQuotePage.SendQuoteConditionsPage();
                SendQuotePage.SendQuotepecialTermsPage();
                PGPremiums.SendQuoteFeesDetailsPage();
                WaitForElement(SendQuotePage.CancelButton);
                SendQuotePage.CancelButton.Click();
                WaitForElement(SendQuotePage.CancelOKButton);
                SendQuotePage.CancelOKButton.Click();
                Thread.Sleep(2000);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                Thread.Sleep(2000);
            }
        }

        public void CreateAndSendQuoteToVerifyPremiumsFor12YearsCoverLength()
        {
            // step3: create quote               
            WaitForElement(Dashboardpage.PlusButton);
            AddPGQuotePage.SubmitQuoteOnSinglePlotFor12YearsCoverLength();
            // step4: send quote and Iterate for ratings               
            SendQuotePage.ClickOnSendQuoteButton();
            if (Premiums.eachplotrow.Any(o => o.ProductName.Equals("New Homes")) ||
                Premiums.eachplotrow.Any(o => o.ProductName.Equals("Social Housing")) ||
                Premiums.eachplotrow.Any(o => o.ProductName.Equals("Private Rental")))
            {
                PGPremiums.SendQuoteRatingPage();
            }
            if (Premiums.eachplotrow.Any(o => o.ProductName.Equals("Commercial")))
            {
                PGPremiums.SendQuotePageBuilderExpPage();
            }
            if (Premiums.eachplotrow.Any(o => o.ProductName.Equals("Completed Housing")) || Premiums.eachplotrow.Any(o => o.ProductName.Equals("Self Build")))
            {
                if (SendQuotePage.RatingEle.Count > 0)
                {
                    PGPremiums.SendQuoteRatingPagePremiums();
                }
                SendQuotePage.SendQuoteSecuritiesPage();
                SendQuotePage.SendQuoteConditionsPage();
                SendQuotePage.SendQuotepecialTermsPage();
                PGPremiums.SendQuoteFeesDetailsPage();
            }
            if (Premiums.eachplotrow.Any(o => o.ProductName.Equals("Channel Islands Residential")) || Premiums.eachplotrow.Any(o => o.ProductName.Contains("Channel Islands Commercial")))
            {
                PGPremiums.SendQuotePageBuilderExpPage();
            }

            if (Premiums.eachplotrow.Any(o => o.BCProduct.Equals("Residential Building Control")) && (Premiums.eachplotrow.All(o => o.ProductName == "")))
            {
                PGPremiums.SendQuoteRatingPage();
            }
            if (Premiums.eachplotrow.Any(o => o.BCProduct.Equals("Commercial Building Control")) && (Premiums.eachplotrow.All(o => o.ProductName == "")))
            {
                SendQuotePage.SendQuoteRatingPage();
                SendQuotePage.SendQuoteSecuritiesPage();
                SendQuotePage.SendQuoteConditionsPage();
                SendQuotePage.SendQuotepecialTermsPage();
                PGPremiums.SendQuoteFeesDetailsPage();
                WaitForElement(SendQuotePage.CancelButton);
                SendQuotePage.CancelButton.Click();
                WaitForElement(SendQuotePage.CancelOKButton);
                SendQuotePage.CancelOKButton.Click();
                Thread.Sleep(2000);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                Thread.Sleep(2000);
            }
        }
    }
}
