﻿using ExcelDataReader;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.PageObjects;
using RegressionPacks.Support;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace RegressionPacks.Pages
{
    public class SelfBuildPremiums : Support.Pages

    {
        public IWebDriver wdriver;
        double iptCaluclationValue = 0.12;
        double BasePremium = 0;
        double TAFee = 0;
        double AdminFee = 0;
        int SQM;
        DataTable excelTable;

        public SelfBuildPremiums(ISearchContext driver)
        {
            PageFactory.InitElements(driver, this);
            wdriver = (RemoteWebDriver)driver;
        }
        public void CoversOnSelfBuildProduct()
        {
            if (Premiums.eachplotrow.Any(o => o.ProductName.Contains("Self Build")))
            {
                Premiums.ProductsCovers = PremiumCovers.ProductCoversList.Select(i => i.Text.ToString()).ToList();
                Premiums.ProductServices = PremiumCovers.ProductServicesList.Select(i => i.Text.ToString()).ToList();

                if (Premiums.ProductServices.Any(x => x.Contains("Technical Audit Fee")))
                {
                    Premiums.ProductTechnicalAuditFeeService = PremiumCovers.ProductTechnicalAuditFee.Count > 0 ? "Yes" : "No";
                }
                if (Premiums.ProductServices.Any(x => x.Contains("Administration Fee")))
                {
                    Premiums.ProductAdminFeeService = PremiumCovers.ProductAdministrationFee.Count > 0 ? "Yes" : "No";
                }
                if (Premiums.ProductServices.Any(x => x.Contains("Cancellation Fee")))
                {
                    Premiums.ProductCancellationFeeService = PremiumCovers.ProductCancellationFee.Count > 0 ? "Yes" : "No";
                }
            }
        }
        public void SelfBuildPremiumsCalcs()
        {
            Premiums.StructuralFee = 0;
            Premiums.ContaminatedLandFee = 0;
            Premiums.TechnicalAuditFee = 0;
            Premiums.AdministrationFee = 0;
            Premiums.CancellationFee = 0;
            var aggregatedPremium = new SelfBuildPremium();
            foreach (var plot in Premiums.eachplotrow)
            {
                //Actual Premiums Caluclaions for Each Plot Row 
                var calculatedPremiums = SelfBuildPremiumCaluclationsForEachPlotRow(plot);
                //Cover Premiums 
                Premiums.StructuralFee += calculatedPremiums.structuralFee;
                //Services  

                Premiums.TechnicalAuditFee += calculatedPremiums.technicalFee;
                Premiums.AdministrationFee += calculatedPremiums.administrationFee;
                Premiums.CancellationFee += calculatedPremiums.cancellationFee;

            }
            //IPT caluclations 
            Premiums.StructuralIPTFee = Convert.ToDouble(Premiums.StructuralFee * iptCaluclationValue);
            //VAT Calucaltions 
            Premiums.TechnicalAuditVATFee = Convert.ToDouble(0.00);
            Premiums.AdministrationVATFee = Convert.ToDouble(0.00);
            Premiums.CancellationVATFee = Convert.ToDouble(0.00);
            VerfiySelfBuildUIValues();
            if (Premiums.eachplotrow.Any(o => o.BCProduct.Contains("Residential Building Control")))
            {
                ResidentialBCPremiums.ResidentialBCPremiumNewPricesCalcs();
            }
            WaitForElement(SendQuotePage.CancelButton);
            SendQuotePage.CancelButton.Click();
            WaitForElement(SendQuotePage.CancelOKButton);
            SendQuotePage.CancelOKButton.Click();
            Thread.Sleep(2000);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(2000);
        }
        public class SelfBuildPremium
        {
            public double structuralFee;

            public double technicalFee;

            public double administrationFee;

            public double cancellationFee;
        }
        int getStructuralPremiumSQMTableRow = -1;
        int getStructuralPremiumBandsTableRow = -1;
        int getSQMStructuralFee = -1;
        int getStructuralPremiumBandsTableRowBetween500kto750k = -1;
        int getStructuralPremiumBandsTableRowBetween750kto1000k = -1;
        int getStructuralPremiumBandsTableRowOver1000000 = -1;
        int getStructuralPremiumConstructionTypeTableRow = -1;
        int getConstuctionTypeRow = 1;
        int getStagesOfWorksTableRow = -1;
        public SelfBuildPremium SelfBuildPremiumCaluclationsForEachPlotRow(plotrows plot)
        {
            //Caluclating Cover Premium Details 
            var calculatedPremiums = new SelfBuildPremium();
            CoversOnSelfBuildProduct();
            var fileInfo = new FileInfo(ExtensionMethods.GetAbsolutePath($@"TestData\Premiums\ProductPricingSetup\Product Pricing Setup - {plot.ProductName}.xlsx"));
            //Filter Premium Data for Commercials 
            string premiumsdata = fileInfo.ToString();
            excelTable = ReadSelfBuildMasterPremiumsPriceData(premiumsdata);
            //Caluclate Premium Data for Self Build            
            double getSQM = 0;

            double sqmInput = plot.SqMValue;
            //Retrive SquareMeter Structural Premium Value   
            double StructuralPremium = 0;
            if (sqmInput <= 150)
            {
                getSQM = 0;
            }
            if (sqmInput > 150 && sqmInput <= 250)
            {
                getSQM = 151;
            }
            if (sqmInput > 150 && sqmInput <= 250)
            {
                getSQM = 151;
            }
            if (sqmInput > 250 && sqmInput <= 350)
            {
                getSQM = 350;
            }
            if (sqmInput > 350 && sqmInput <= 400)
            {
                getSQM = 400;
            }
            if (sqmInput > 400 && sqmInput <= 450)
            {
                getSQM = 450;
            }
            if (sqmInput > 450 && sqmInput <= 500)
            {
                getSQM = 500;
            }
            if (sqmInput > 500 && sqmInput <= 550)
            {
                getSQM = 550;
            }
            if (sqmInput > 550 && sqmInput <= 600)
            {
                getSQM = 600;
            }
            if (sqmInput > 600 && sqmInput <= 650)
            {
                getSQM = 650;
            }
            if (sqmInput > 650 && sqmInput <= 700)
            {
                getSQM = 700;
            }
            if (sqmInput > 700 && sqmInput <= 750)
            {
                getSQM = 750;
            }
            if (sqmInput > 750 && sqmInput <= 800)
            {
                getSQM = 800;
            }
            if (sqmInput > 800 && sqmInput <= 850)
            {
                getSQM = 850;
            }
            if (sqmInput > 850 && sqmInput <= 900)
            {
                getSQM = 900;
            }
            if (sqmInput > 900 && sqmInput <= 950)
            {
                getSQM = 950;
            }
            if (sqmInput > 950 && sqmInput <= 1000)
            {
                getSQM = 1000;
            }
            if (sqmInput > 1000 && sqmInput <= 1050)
            {
                getSQM = 1050;
            }
            if (sqmInput > 1050 && sqmInput <= 1100)
            {
                getSQM = 1100;
            }
            if (sqmInput > 1100 && sqmInput <= 1150)
            {
                getSQM = 1150;
            }
            if (sqmInput > 1150 && sqmInput <= 1200)
            {
                getSQM = 1200;
            }
            if (sqmInput > 1200 && sqmInput <= 1250)
            {
                getSQM = 1250;
            }
            if (sqmInput > 1250 && sqmInput <= 1300)
            {
                getSQM = 1300;
            }
            if (sqmInput > 1350 && sqmInput <= 1400)
            {
                getSQM = 1400;
            }
            if (sqmInput > 1400 && sqmInput <= 1450)
            {
                getSQM = 1450;
            }
            if (sqmInput > 1450 && sqmInput <= 1500)
            {
                getSQM = 1500;
            }
            if (sqmInput > 1500 && sqmInput <= 1550)
            {
                getSQM = 1550;
            }
            if (sqmInput > 1550 && sqmInput <= 1600)
            {
                getSQM = 1600;
            }
            if (sqmInput > 1600 && sqmInput <= 1650)
            {
                getSQM = 1650;
            }
            if (sqmInput > 1650 && sqmInput <= 1700)
            {
                getSQM = 1700;
            }
            if (sqmInput > 1700 && sqmInput <= 1750)
            {
                getSQM = 1750;
            }
            if (sqmInput > 1750 && sqmInput <= 1800)
            {
                getSQM = 1800;
            }
            if (sqmInput > 1800 && sqmInput <= 1850)
            {
                getSQM = 1850;
            }
            if (sqmInput > 1850 && sqmInput <= 1900)
            {
                getSQM = 1900;
            }
            if (sqmInput > 1900 && sqmInput <= 1950)
            {
                getSQM = 1950;
            }
            if (sqmInput > 1950 && sqmInput <= 2000)
            {
                getSQM = 2000;
            }
            if (sqmInput > 2000 && sqmInput <= 2050)
            {
                getSQM = 2050;
            }
            if (sqmInput > 2050 && sqmInput <= 2100 || sqmInput > 2100)
            {
                getSQM = 2100;
            }

            SQM = Convert.ToInt32(getSQM);

            if (plot.ReconstructionCostValue < 500000)
            {
                for (int i = 0; i < excelTable.Rows.Count; i++)
                {
                    if (excelTable.Rows[i][1].ToString() == "SQM")
                    {
                        getStructuralPremiumSQMTableRow = i;
                        break;
                    }
                }
                if (getStructuralPremiumSQMTableRow == -1)
                    throw new Exception("Square Meter Table not found");
                for (int i = getStructuralPremiumSQMTableRow; i < excelTable.Rows.Count; i++)
                {
                    if (excelTable.Rows[i][1].ToString().Contains(SQM.ToString()))
                    {
                        getSQMStructuralFee = i;
                        break;
                    }
                }
                if (getSQMStructuralFee == -1)
                    throw new Exception("Square Meter Table Row Not Found");
                BasePremium = Convert.ToDouble(excelTable.Rows[getSQMStructuralFee][2].ToString());
            }
            if (plot.ReconstructionCostValue >= 500000 && plot.ReconstructionCostValue <= 750000 || plot.ReconstructionCostValue > 750000 && plot.ReconstructionCostValue <= 1000000 || plot.ReconstructionCostValue >= 1000000)
            {
                for (int i = 0; i < excelTable.Rows.Count; i++)
                {
                    if (excelTable.Rows[i][0].ToString() == "Structural Premium - Bands")
                    {
                        getStructuralPremiumBandsTableRow = i;
                        break;
                    }
                }
                if (getStructuralPremiumBandsTableRow == -1)
                    throw new Exception("Structural Premium Bands Table not found");

                if (plot.ReconstructionCostValue >= 500000 && plot.ReconstructionCostValue <= 750000)
                {
                    for (int i = getStructuralPremiumBandsTableRow; i < excelTable.Rows.Count; i++)
                    {
                        if (excelTable.Rows[i][1].ToString() == "500000")
                        {
                            getStructuralPremiumBandsTableRowBetween500kto750k = i;

                            break;
                        }
                    }
                    if (getStructuralPremiumBandsTableRowBetween500kto750k == -1)
                        throw new Exception("Structural Premium 500K to 750K Band Fee Table Not Found");

                    StructuralPremium = Convert.ToDouble((excelTable.Rows[getStructuralPremiumBandsTableRowBetween500kto750k][3].ToString()));
                    BasePremium = Convert.ToDouble(StructuralPremium * plot.ReconstructionCostValue);
                }
                if (plot.ReconstructionCostValue > 750000 && plot.ReconstructionCostValue <= 1000000)
                {

                    double BottomOfband = Convert.ToDouble(750000);
                    double TopOfBand = Convert.ToDouble(1000000);
                    double reconstructionDiff = Convert.ToDouble(plot.ReconstructionCostValue - 750000);
                    for (int i = getStructuralPremiumBandsTableRow; i < excelTable.Rows.Count; i++)
                    {
                        if (excelTable.Rows[i][1].ToString() == "750000")
                        {
                            getStructuralPremiumBandsTableRowBetween750kto1000k = i;
                            break;
                        }
                    }
                    if (getStructuralPremiumBandsTableRowBetween750kto1000k == -1)
                        throw new Exception("Structural Premium 750k to 100000K Band Fee Table Not Found");
                    StructuralPremium = Convert.ToDouble((excelTable.Rows[getStructuralPremiumBandsTableRowBetween750kto1000k][3].ToString()));
                    var BaseFee = Convert.ToDouble((excelTable.Rows[getStructuralPremiumBandsTableRowBetween750kto1000k][5].ToString()));
                    BasePremium = Convert.ToDouble((StructuralPremium * reconstructionDiff) + BaseFee);

                }
                if (plot.ReconstructionCostValue > 1000000)
                {
                    for (int i = getStructuralPremiumBandsTableRow; i < excelTable.Rows.Count; i++)
                    {
                        if (excelTable.Rows[i][1].ToString() == "1000000")
                        {
                            getStructuralPremiumBandsTableRowOver1000000 = i;

                            break;
                        }
                    }
                    if (getStructuralPremiumBandsTableRowOver1000000 == -1)
                        throw new Exception("Structural Premium Over 100000K Band Fee Table Not Found ");
                    StructuralPremium = Convert.ToDouble((excelTable.Rows[getStructuralPremiumBandsTableRowOver1000000][3].ToString()));
                    BasePremium = Convert.ToDouble((StructuralPremium * plot.ReconstructionCostValue));
                }
            }
            ArchitectInvolvementPremiumCaluclations(plot);
            ConstructionTypePremiumCaluclations(plot);  
            calculatedPremiums.structuralFee = BasePremium;

            if(Premiums.ProductTechnicalAuditFeeService == "Yes")
            {
                TechincalAuditFeeCaluclations(plot);
                calculatedPremiums.technicalFee = TAFee;
            }
            if (Premiums.ProductAdminFeeService == "Yes")
            {
                AdminFeeCaluclations(plot);
                calculatedPremiums.administrationFee = AdminFee;
            }

            calculatedPremiums.cancellationFee = Convert.ToDouble(0.00);
          


            return calculatedPremiums;
        }
        public void ArchitectInvolvementPremiumCaluclations(plotrows plot)
        {
            double noArchitect = Convert.ToDouble(excelTable.Rows[53][2]);
            double drawingPlansOnly = Convert.ToDouble(excelTable.Rows[54][2]);
            double overseeMajorityOfWorks = Convert.ToDouble(excelTable.Rows[55][2]);
            double overseeAndIssueCertificates = Convert.ToDouble(excelTable.Rows[56][2]);

            //Product Cover Premium Caluclations Based on Architect Involvement Loading    
            if (Premiums.ArchitectInvolvement == "No")
            {
                BasePremium = Convert.ToDouble(BasePremium * noArchitect);
            }
            if (Premiums.ArchitectInvolvement == "Yes")
            {
                if (Premiums.ArchitectInvolvementLoading == "Drawing Plans Only")
                {
                    BasePremium = Convert.ToDouble(BasePremium * drawingPlansOnly);

                }
                if (Premiums.ArchitectInvolvementLoading == "Oversee and Issue Certificates")
                {
                    BasePremium = Convert.ToDouble(BasePremium * overseeMajorityOfWorks);

                }
                if (Premiums.ArchitectInvolvementLoading == "Oversee Majority of Work")
                {
                    BasePremium = Convert.ToDouble(BasePremium * overseeAndIssueCertificates);
                }
            }
        }
        public void ConstructionTypePremiumCaluclations(plotrows plot)
        {
            for (int i = 0; i < excelTable.Rows.Count; i++)
            {
                if (excelTable.Rows[i][0].ToString() == "Construction Type Loading")
                {
                    getStructuralPremiumConstructionTypeTableRow = i;
                    break;
                }
            }
            if (getStructuralPremiumConstructionTypeTableRow == -1)
                throw new Exception("Construction Type Loading Table not found");
            for (int i = getStructuralPremiumConstructionTypeTableRow; i < excelTable.Rows.Count; i++)
            {
                if (excelTable.Rows[i][1].ToString() == plot.ConstructionType)
                {
                    getConstuctionTypeRow = i;
                    break;
                }
            }
            if (getConstuctionTypeRow == -1)
                throw new Exception("Construction Type Loading Fee Not Found");
            var ConstructionTypeLoadingValue = Convert.ToDouble(excelTable.Rows[getConstuctionTypeRow][2].ToString());
            BasePremium = Convert.ToDouble(ConstructionTypeLoadingValue * BasePremium);

            if (plot.ConstructionType == "New Build")
            {
                NewBuildSOWPremiumCaluclations(plot);
            }
            if (plot.ConstructionType == "Conversion")
            {
                ConversionSOWPremiumCaluclations(plot);
            }
        }
        public void NewBuildSOWPremiumCaluclations(plotrows plot)
        {
            //Product Cover Premium Caluclations Based on Stages Of Works              
            double noworksStarted = Convert.ToDouble(excelTable.Rows[65][2]);
            double foundationLevel = Convert.ToDouble(excelTable.Rows[66][2]);
            double firstFloorLevel = Convert.ToDouble(excelTable.Rows[67][2]);
            double wallPlateLevel = Convert.ToDouble(excelTable.Rows[68][2]);
            double roofLevel = Convert.ToDouble(excelTable.Rows[69][2]);
           
               

                if (plot.StageOfWork.Contains("No Works Started"))
                {
                    BasePremium = Convert.ToDouble(BasePremium * noworksStarted);
                }
                if (plot.StageOfWork.Contains("Foundation/DPC"))
                {
                    BasePremium = Convert.ToDouble(BasePremium * foundationLevel);

                }
                if (plot.StageOfWork.Contains("1st Floor"))
                {
                    BasePremium = Convert.ToDouble(BasePremium * firstFloorLevel);

                }
                if (plot.StageOfWork.Contains("Wall Plate Level"))
                {
                    BasePremium = Convert.ToDouble(BasePremium * wallPlateLevel);

                }
                if (plot.StageOfWork.Contains("Roof/Watertight"))
                {
                    BasePremium = Convert.ToDouble(BasePremium * roofLevel);
                }
        }
        public void ConversionSOWPremiumCaluclations(plotrows plot)
        {
            double noworksStarted = Convert.ToDouble(excelTable.Rows[65][2]);
            double firstFix = Convert.ToDouble(excelTable.Rows[66][2]);
            double secondFix = Convert.ToDouble(excelTable.Rows[68][2]);
           
                if (plot.StageOfWork.Contains("No Works Started"))
                {
                    BasePremium = Convert.ToDouble(BasePremium * noworksStarted);
                }
                if (plot.StageOfWork.Contains("First Fix"))
                {
                    BasePremium = Convert.ToDouble(BasePremium * firstFix);
                }
                if (plot.StageOfWork.Contains("Second Fix"))
                {
                    BasePremium = Convert.ToDouble(BasePremium * secondFix);
                }
        }
        
        public void TechincalAuditFeeCaluclations(plotrows plot)
        {
            //Retriving Services Fee Values from master product pricing setup book
            //Techinical Audit Fee Table
            var technicalAuditFeetable = -1;
            var technicalAuditFeeSQMRow = -1;
            var technicalAuditLoadingTable = -1;
            var taFeeValueLess1m = -1;
            var taFeeValueOver1m = -1;
            double TAFeeSQMValue = 0;
            double TAFeeBandingValue = 0;
            for (int i = 0; i < excelTable.Rows.Count; i++)
            {
                if (excelTable.Rows[i][0].ToString() == "Technical Audit Fee")
                {
                    technicalAuditFeetable = i;
                    break;
                }
            }
            if (technicalAuditFeetable == -1)
                throw new Exception("Technical Audit Fee not found");

            //Retrive Units Count 

            for (int i = technicalAuditFeetable; i < excelTable.Rows.Count; i++)
            {
                if (excelTable.Rows[i][1].ToString().Contains(SQM.ToString()))
                {
                    technicalAuditFeeSQMRow = i;                   
                    break;
                }
            }

            if (technicalAuditFeeSQMRow == -1)
                throw new Exception("Square Meter Fee Not Found");
            TAFeeSQMValue = Convert.ToDouble(excelTable.Rows[technicalAuditFeeSQMRow][2].ToString());
            TAFee = Convert.ToDouble(TAFeeSQMValue);

            for (int i = 0; i < excelTable.Rows.Count; i++)
            {
                if (excelTable.Rows[i][0].ToString() == "Technical Audit Fee Loading")
                {
                    technicalAuditLoadingTable = i;
                    break;
                }
            }
            if (technicalAuditLoadingTable == -1)
                throw new Exception("Technical Audit Fee Loading table  not found");

            if (plot.ReconstructionCostValue <= 1000000)
            {
                for (int i = technicalAuditLoadingTable; i < excelTable.Rows.Count; i++)
                {
                    if (excelTable.Rows[i][1].ToString() == "1m or less")
                    {
                        taFeeValueLess1m = i;
                        break;
                    }
                }

                if (taFeeValueLess1m == -1)
                    throw new Exception("Techincal Audit Fee Loading Table Over 1m or less Row Not Found");
                TAFeeBandingValue = Convert.ToDouble(excelTable.Rows[taFeeValueLess1m][2].ToString());
                TAFee = Convert.ToDouble(TAFee * TAFeeBandingValue);
            }
            //Technical Audit Fee Loading Table Based On Banding Value 
            if (plot.ReconstructionCostValue > 1000000)
            {
                for (int i = technicalAuditLoadingTable; i < excelTable.Rows.Count; i++)
                {
                    if (excelTable.Rows[i][1].ToString() == "Over 1m")
                    {
                        taFeeValueOver1m = i;                       
                        break;
                    }
                }

                if (taFeeValueOver1m == -1)
                    throw new Exception("Techincal Audit Fee Loading Table Over 1m Row Not Found");
                TAFeeBandingValue = Convert.ToDouble(excelTable.Rows[taFeeValueOver1m][2].ToString());                

                TAFee = Convert.ToDouble(TAFee * TAFeeBandingValue);
            }
           
        }

        public void AdminFeeCaluclations(plotrows plot)
        {
            var adminFeeTable = -1;
            for (int i = 0; i < excelTable.Rows.Count; i++)
            {
                if (excelTable.Rows[i][0].ToString() == "Administration Fee")
                {
                    adminFeeTable = i;
                    break;
                }
            }
            if (adminFeeTable == -1)
                throw new Exception("Admin Fee Table Not found");

            double administrationFee = Convert.ToDouble(excelTable.Rows[adminFeeTable+2][1].ToString());
             AdminFee = Convert.ToDouble(administrationFee);
        }
        
        public void VerfiySelfBuildUIValues()
        {
            var productName = Premiums.eachplotrow.FirstOrDefault().ProductName;

            //Covers Elements 

            //Veridy UI Structural Values
            var uiStructuralFee = $"//th[@class='product-title'][text()='{productName}']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[text()='Structural']//following-sibling::td[@class='right-align'][1]";
            var StructuralFee = Driver.FindElement(By.XPath(uiStructuralFee)).Text.Replace("£", "");
            double UIStructuralFee = Math.Round(Convert.ToDouble(StructuralFee), 2);
            double StructuralFeeDiff;
            var uiStructuralIPTFee = $"//th[@class='product-title'][text()='{productName}']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[text()='Structural']//following-sibling::td[@class='right-align'][2]";
            var StructuralIPTFee = Driver.FindElement(By.XPath(uiStructuralIPTFee)).Text.Replace("£", "");
            double UIStructuralIPTFee = Math.Round(Convert.ToDouble(StructuralIPTFee), 2);
            double StructuralIPTDiff;
            var uiStructuralTotalFee = $"//th[@class='product-title'][text()='{productName}']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[text()='Structural']//following-sibling::th[@class='right-align'][1]";
            var StructuralTotalFee = Driver.FindElement(By.XPath(uiStructuralTotalFee)).Text.Replace("£", "");
            double UIStructuralTotalFee = Math.Round(Convert.ToDouble(StructuralTotalFee), 2);
            double StructuralTotalValue = Math.Round(Convert.ToDouble(UIStructuralFee + UIStructuralIPTFee), 2);
            double StructuralTotalDiff;

            if (UIStructuralFee != Premiums.StructuralFee || UIStructuralIPTFee != Premiums.StructuralIPTFee || UIStructuralTotalFee != StructuralTotalValue)
            {
                StructuralFeeDiff = Convert.ToDouble(Premiums.StructuralFee - UIStructuralFee);
                StructuralIPTDiff = Convert.ToDouble(Premiums.StructuralIPTFee - UIStructuralIPTFee);
                StructuralTotalDiff = Convert.ToDouble(StructuralTotalValue - UIStructuralTotalFee);
                if (StructuralFeeDiff >= -1.01 && StructuralFeeDiff <= 1.01)
                {
                    Premiums.StructuralFee = UIStructuralFee;
                }
                if (StructuralIPTDiff >= -1.01 && StructuralIPTDiff <= 1.01)
                {
                    Premiums.StructuralIPTFee = UIStructuralIPTFee;
                }
                if (StructuralTotalDiff >= 1.01 && StructuralTotalDiff <= 1.01)
                {
                    StructuralTotalValue = UIStructuralTotalFee;
                }

                Assert.AreEqual(UIStructuralFee, Premiums.StructuralFee, $"StructuralFee Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                Assert.AreEqual(UIStructuralIPTFee, Premiums.StructuralIPTFee, $"StructuralIPTFee Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                Assert.AreEqual(UIStructuralTotalFee, StructuralTotalValue, $"StructuralTotalFee Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
            }
            else
            {

                Assert.AreEqual(UIStructuralFee, Premiums.StructuralFee, $"StructuralFee Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                Assert.AreEqual(UIStructuralIPTFee, Premiums.StructuralIPTFee, $"StructuralIPTFee Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                Assert.AreEqual(UIStructuralTotalFee, StructuralTotalValue, $"StructuralTotalFee Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
            }



            //Services Elements  and  Fee values  
            //Technical Audit Fee values  
            if (Premiums.ProductServices.Any(x => x.Contains("Technical Audit Fee")))
            {


                var uiTechnicalAuditFee = $"//th[@class='product-title'][text()='{productName}']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[text()='Technical Audit Fee']//following-sibling::td[@class='right-align'][1]";
                var TechnicalAuditFee = Driver.FindElement(By.XPath(uiTechnicalAuditFee)).Text.Replace("£", "");
                double UITechnicalAuditFee = Math.Round(Convert.ToDouble(TechnicalAuditFee), 2);
                double TechnicalAuditFeeDiff;

                var uiTechnicalAuditVATFee = $"//th[@class='product-title'][text()='{productName}']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[text()='Technical Audit Fee']//following-sibling::td[@class='right-align'][2]";
                var TechnicalAuditVATFee = Driver.FindElement(By.XPath(uiTechnicalAuditVATFee)).Text.Replace("£", "");
                double UITechnicalAuditVATFee = Math.Round(Convert.ToDouble(TechnicalAuditVATFee), 2);
                double TechnicalAuditVATDiff;

                var uiTechnicalAuditTotalFee = $"//th[@class='product-title'][text()='{productName}']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[text()='Technical Audit Fee']//following-sibling::th[@class='right-align'][1]";
                var TechnicalAuditTotalFee = Driver.FindElement(By.XPath(uiTechnicalAuditTotalFee)).Text.Replace("£", "");
                double UITechnicalAuditTotalFee = Math.Round(Convert.ToDouble(TechnicalAuditTotalFee), 2);
                double TechnicalAuditTotalValue = Math.Round(Convert.ToDouble(UITechnicalAuditFee + UITechnicalAuditVATFee), 2);
                double TechnicalAuditTotalDiff;

                if (UITechnicalAuditFee != Premiums.TechnicalAuditFee || UITechnicalAuditVATFee != Premiums.TechnicalAuditVATFee || UITechnicalAuditTotalFee != TechnicalAuditTotalValue)
                {
                    TechnicalAuditFeeDiff = Convert.ToDouble(Premiums.TechnicalAuditFee - UITechnicalAuditFee);
                    TechnicalAuditVATDiff = Convert.ToDouble(Premiums.TechnicalAuditVATFee - UITechnicalAuditVATFee);
                    TechnicalAuditTotalDiff = Convert.ToDouble(TechnicalAuditTotalValue - UITechnicalAuditTotalFee);
                    if (TechnicalAuditFeeDiff >= -1.01 && TechnicalAuditFeeDiff <= 1.01)
                    {
                        Premiums.TechnicalAuditFee = UITechnicalAuditFee;
                    }

                    if (TechnicalAuditVATDiff >= -1.01 && TechnicalAuditVATDiff <= 1.01)
                    {
                        Premiums.TechnicalAuditVATFee = UITechnicalAuditVATFee;
                    }
                    if (TechnicalAuditTotalDiff >= 1.01 && TechnicalAuditTotalDiff <= 1.01)
                    {
                        TechnicalAuditTotalValue = UITechnicalAuditTotalFee;
                    }

                    Assert.AreEqual(UITechnicalAuditFee, Premiums.TechnicalAuditFee, $"TechnicalAuditFee Services Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UITechnicalAuditVATFee, Premiums.TechnicalAuditVATFee, $"TechnicalAuditVATFee Services Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UITechnicalAuditTotalFee, TechnicalAuditTotalValue, $"TechnicalAuditTotalValue Services Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                }
                else
                {
                    Assert.AreEqual(UITechnicalAuditFee, Premiums.TechnicalAuditFee, $"TechnicalAuditFee Services Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UITechnicalAuditVATFee, Premiums.TechnicalAuditVATFee, $"TechnicalAuditVATFee Services Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UITechnicalAuditTotalFee, TechnicalAuditTotalValue, $"TechnicalAuditTotalValue Services Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");

                }
            }


            //Administrator Fee values    
            if (Premiums.ProductServices.Any(x => x.Contains("Administration Fee")))
            {

                var uiAdministrationFee = $"//th[@class='product-title'][text()='{productName}']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[text()='Administration Fee']//following-sibling::td[@class='right-align'][1]";
                var AdministrationFee = Driver.FindElement(By.XPath(uiAdministrationFee)).Text.Replace("£", "");
                double UIAdministrationFee = Math.Round(Convert.ToDouble(AdministrationFee), 2);
                double AdministrationFeeDiff;

                var uiAdministrationVATFee = $"//th[@class='product-title'][text()='{productName}']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[text()='Administration Fee']//following-sibling::td[@class='right-align'][2]";
                var AdministrationVATFee = Driver.FindElement(By.XPath(uiAdministrationVATFee)).Text.Replace("£", "");
                double UIAdministrationVATFee = Math.Round(Convert.ToDouble(AdministrationVATFee), 2);
                double AdministrationVATDiff;

                var uiAdministrationTotalFee = $"//th[@class='product-title'][text()='{productName}']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[text()='Administration Fee']//following-sibling::th[@class='right-align'][1]";
                var AdministrationTotalFee = Driver.FindElement(By.XPath(uiAdministrationTotalFee)).Text.Replace("£", "");
                double UIAdministrationTotalFee = Math.Round(Convert.ToDouble(AdministrationTotalFee), 2);
                double AdministrationTotalValue = Math.Round(Convert.ToDouble(UIAdministrationFee + UIAdministrationVATFee), 2);
                double AdministrationTotalDiff;

                if (UIAdministrationFee != Premiums.AdministrationFee || UIAdministrationVATFee != Premiums.AdministrationVATFee || UIAdministrationTotalFee != AdministrationTotalValue)
                {
                    AdministrationFeeDiff = Convert.ToDouble(Premiums.AdministrationFee - UIAdministrationFee);
                    AdministrationVATDiff = Convert.ToDouble(Premiums.AdministrationVATFee - UIAdministrationVATFee);
                    AdministrationTotalDiff = Convert.ToDouble(AdministrationTotalValue - UIAdministrationTotalFee);
                    if (AdministrationFeeDiff >= -1.01 && AdministrationFeeDiff <= 1.01)
                    {
                        Premiums.AdministrationFee = UIAdministrationFee;
                    }

                    if (AdministrationVATDiff >= -1.01 && AdministrationVATDiff <= 1.01)
                    {
                        Premiums.AdministrationVATFee = UIAdministrationVATFee;
                    }
                    if (AdministrationTotalDiff >= 1.01 && AdministrationTotalDiff <= 1.01)
                    {
                        AdministrationTotalValue = UIAdministrationTotalFee;
                    }

                    Assert.AreEqual(UIAdministrationFee, Premiums.AdministrationFee, $"AdministrationFee Services Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UIAdministrationVATFee, Premiums.AdministrationVATFee, $"AdministrationVATFee Services Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UIAdministrationTotalFee, AdministrationTotalValue, $"AdministrationTotalValue Services Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                }
                else
                {
                    Assert.AreEqual(UIAdministrationFee, Premiums.AdministrationFee, $"AdministrationFee Services Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UIAdministrationVATFee, Premiums.AdministrationVATFee, $"AdministrationVATFee Services Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UIAdministrationTotalFee, AdministrationTotalValue, $"AdministrationTotalValue Services Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                }
            }
            //Cancellation Fee values  
            if (Premiums.ProductServices.Any(x => x.Contains("Cancellation Fee")))
            {


                var uiCancellationFee = $"//th[@class='product-title'][text()='{productName}']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[text()='Cancellation Fee']//following-sibling::td[@class='right-align'][1]";
                var CancellationFee = Driver.FindElement(By.XPath(uiCancellationFee)).Text.Replace("£", "");
                double UICancellationFee = Math.Round(Convert.ToDouble(CancellationFee), 2);
                double CancellationFeeDiff;

                var uiCancellationVATFee = $"//th[@class='product-title'][text()='{productName}']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[text()='Cancellation Fee']//following-sibling::td[@class='right-align'][2]";
                var CancellationVATFee = Driver.FindElement(By.XPath(uiCancellationVATFee)).Text.Replace("£", "");
                double UICancellationVATFee = Math.Round(Convert.ToDouble(CancellationVATFee), 2);
                double CancellationVATDiff;

                var uiCancellationTotalFee = $"//th[@class='product-title'][text()='{productName}']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[text()='Cancellation Fee']//following-sibling::th[@class='right-align'][1]";
                var CancellationTotalFee = Driver.FindElement(By.XPath(uiCancellationTotalFee)).Text.Replace("£", "");
                double UICancellationTotalFee = Math.Round(Convert.ToDouble(CancellationTotalFee), 2);
                double CancellationTotalValue = Math.Round(Convert.ToDouble(UICancellationFee + UICancellationVATFee), 2);
                double CancellationTotalDiff;


                if (UICancellationFee != Premiums.CancellationFee || UICancellationVATFee != Premiums.CancellationVATFee || UICancellationTotalFee != CancellationTotalValue)
                {
                    CancellationFeeDiff = Convert.ToDouble(Premiums.CancellationFee - UICancellationFee);
                    CancellationVATDiff = Convert.ToDouble(Premiums.CancellationVATFee - UICancellationVATFee);
                    CancellationTotalDiff = Convert.ToDouble(CancellationTotalValue - UICancellationTotalFee);
                    if (CancellationFeeDiff >= -1.01 && CancellationFeeDiff <= 1.01)
                    {
                        Premiums.CancellationFee = UICancellationFee;
                    }

                    if (CancellationVATDiff >= -1.01 && CancellationVATDiff <= 1.01)
                    {
                        Premiums.CancellationVATFee = UICancellationVATFee;
                    }
                    if (CancellationTotalDiff >= -1.01 && CancellationTotalDiff <= 1.01)
                    {
                        CancellationTotalValue = UICancellationTotalFee;
                    }

                    Assert.AreEqual(UICancellationFee, Premiums.CancellationFee, $"CancellationFee Services Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UICancellationVATFee, Premiums.CancellationVATFee, $"CancellationVATFee Services Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UICancellationTotalFee, CancellationTotalValue, $"CancellationTotalValue Services Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                }
                else
                {
                    Assert.AreEqual(UICancellationFee, Premiums.CancellationFee, $"CancellationFee Services Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UICancellationVATFee, Premiums.CancellationVATFee, $"CancellationVATFee Services Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UICancellationTotalFee, CancellationTotalValue, $"CancellationTotalValue Services Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                }
            }

        }
        public DataTable ReadSelfBuildMasterPremiumsPriceData(string fileName)
        {

            var result = new DataSet();                                                                             //Set the First Row as Column Name
            using (var stream = File.Open(fileName, FileMode.Open, FileAccess.Read))
            using (var excelReader = ExcelReaderFactory.CreateOpenXmlReader(stream)) //.xlsx
            {
                //Return as DataSet
                result = excelReader.AsDataSet(new ExcelDataSetConfiguration()
                {
                    ConfigureDataTable = (_) => new ExcelDataTableConfiguration()
                    {
                        UseHeaderRow = true
                    }
                });
            }
            //Get all the Tables
            DataTableCollection table = result.Tables;
            DataTable resultTable = table[table.Count - 1];
            return resultTable;
        }

    }
}
