﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.PageObjects;
using RegressionPacks.Support;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace RegressionPacks.Pages
{
    public class AddLABCQuotePage :Support.Pages
    {
        public IWebDriver wdriver;

        public AddLABCQuotePage(ISearchContext driver)
        {
            PageFactory.InitElements(driver, this);
            wdriver = (RemoteWebDriver)driver;
        }

        [FindsBy(How = How.XPath, Using = "//crisp-dialog[@class='au-target']//div//crisp-header[@class='au-target']//nav")]
        public IWebElement AddNewQuoteDiv;

        //Key Site Details Page Elements

        [FindsBy(How = How.XPath, Using = "//crisp-input-radio[@label='Brand']//div[@class='au-target']//ul//li//label[text()='LABC Warranty']")]
        public IList<IWebElement> LABCRadioButton;

        [FindsBy(How = How.XPath, Using = "//crisp-content/crisp-input-object//div//label[text()='Quote Recipient']")]
        public IWebElement QuoteRecipientLabel;

        [FindsBy(How = How.XPath, Using = "//crisp-input-object/div[@class='au-target input-field']/span[@ref='inputContainer']/span/div/span[@class='au-target']")]
        public IList<IWebElement> QuoteRecipientInput;

        [FindsBy(How = How.XPath, Using = "//contact-list/crisp-list[@ref='listElm']/ul[@ref='theList']/li[@class='au-target collection-item']//crisp-list-item//a[contains(@class,'au-target row list-item-contents clickable hasAvatar')]")]
        public IList<IWebElement> ChooseBuilder;

        [FindsBy(How = How.CssSelector, Using = "crisp-input-text[label = 'Search'] > div > label")]
        public IWebElement SearchLabel;

        [FindsBy(How = How.CssSelector, Using = "crisp-input-text[label = 'Search'] > div > input")]
        public IWebElement SearchInput;

        [FindsBy(How = How.XPath, Using = "//crisp-list[@ref='listElm']//ul//li[@class='au-target collection-item']//crisp-list-item[@class='au-target']//a[contains(@class,'au-target row list-item-contents clickable hasAvatar')]")]
        public IList<IWebElement> QuoteRecipientList;

        [FindsBy(How = How.XPath, Using = "//crisp-input-object//div//label[contains(text(),'Office')]")]
        public IList<IWebElement> QuoteRecipientOfficeLabel;

        [FindsBy(How = How.XPath, Using = "//ul//li[@class='au-target collection-item']//crisp-list-item/a")]
        public IList<IWebElement> QuoteRecipientOfficeInput;

        [FindsBy(How = How.XPath, Using = "//crisp-content/crisp-input-object//div//label[text()='Site Address']")]
        public IWebElement SiteAddressLabel;

        [FindsBy(How = How.XPath, Using = "//crisp-content/crisp-input-object//div//span//view-address")]
        public IList<IWebElement> SiteAddressInput;

        [FindsBy(How = How.XPath, Using = "//crisp-button/span/button[text()='Enter Address Using Postcode'][@class='au-target waves-effect waves-light btn-flat']")]
        public IWebElement EnterAddressUsingPostcodeButton;

        [FindsBy(How = How.CssSelector, Using = "crisp-input-text[label='Enter postcode to search'] > div > label")]
        public IWebElement PostcodeSearchLabel;

        [FindsBy(How = How.CssSelector, Using = "crisp-input-text[label='Enter postcode to search'] > div > input")]
        public IWebElement PostcodeInput;

        [FindsBy(How = How.XPath, Using = "//crisp-picker[@label='Address']/div/div/input[@class='select-dropdown']")]
        public IWebElement AddressDropdown;

        [FindsBy(How = How.XPath, Using = "//ul[@class='dropdown-content select-dropdown active']/li[1]")]
        public IWebElement AddressDropdownInput;

        [FindsBy(How = How.XPath, Using = "//crisp-button/span/button[@class='au-target waves-effect waves-light btn'][text()='Set Address']")]
        public IWebElement SetAddressButton;

        [FindsBy(How = How.XPath, Using = "//crisp-picker[@label='Insurance Agreement Group']//div[@class='au-target input-field']//div")]
        public IList<IWebElement> InsuranceGroup;



        //crisp-input-date[@label='Product version date']//div//input

        [FindsBy(How = How.XPath, Using = "//ul[@class='dropdown-content select-dropdown active']//li//span[text()='Aviva']")]
        public IWebElement InsuranceGroupInput;

        [FindsBy(How = How.XPath, Using = "//ul[@class='dropdown-content select-dropdown active']//li//span")]
        public IList<IWebElement> InsuranceGroupInputs;

        [FindsBy(How = How.XPath, Using = "//crisp-input-object[@label='Quote Application Documents']//span[contains(@class,'au-target input-container multi-item')]//span")]
        public IWebElement AddQuoteAppDocs;

        [FindsBy(How = How.XPath, Using = "//crisp-dialog//crisp-header//div[text()='Upload document']")]
        public IWebElement UploadDocWizard;        

        [FindsBy(How = How.XPath, Using = "//crisp-input-file[@label='Select file...']//div//div//input[@class='au-target']")]
        public IWebElement UploadDoc;

        [FindsBy(How = How.XPath, Using = "//crisp-button[@click.delegate='ok()'][@class='au-target']//span//button[text()='OK'][@class='au-target waves-effect waves-light btn-flat']")]
        public IWebElement UploadOkButton;

        [FindsBy(How = How.XPath, Using = "//crisp-button[@class='au-target'][@click.delegate='wizard.goForward()']//span//button[@class='au-target waves-effect waves-light btn-flat'][text()='Next']")]
        public IWebElement NextButton;


        [FindsBy(How = How.XPath, Using = "//div[@class='modal-content']//crisp-wizard[@active-step.bind='activeStep']//crisp-stepper-step[contains(@class,'current')]//div[@title.bind='description'][contains(@class,'title')]")]
        public IWebElement StepTitle;


        //Oter Site Details Page Elements 

        [FindsBy(How = How.XPath, Using = "//crisp-input-date/div/label[text()='Construction start date']")]
        public IWebElement ConstructionStartDateLabel;

        [FindsBy(How = How.XPath, Using = "//div[contains(@class,'picker--focused')]/div/div/div/div/div[2]/table[@class='picker__table']/tbody//tr//td/div[text()='10']")]
        public IWebElement ConstructionStartDateInput;

        [FindsBy(How = How.XPath, Using = "//crisp-input-date[@label='Construction end date']/div/label")]
        public IWebElement ConstructionEndDateLabel;

        [FindsBy(How = How.XPath, Using = "//div[contains(@class,'picker--focused')]/div/div/div/div/div[2]/table[@class='picker__table']/tbody//tr//td/div[contains(@class,'day--infocus')][text()='28']")]
        public IWebElement ConstructionEndDateInput;

        [FindsBy(How = How.CssSelector, Using = "div:nth-child(1) > crisp-input-number:nth-child(3) > div > label")]
        public IWebElement NumberOffStoreysAboveGround;

        [FindsBy(How = How.CssSelector, Using = "div:nth-child(1) > crisp-input-number:nth-child(3) > div > input")]
        public IWebElement NumberOffStoreysAboveGroundInput;

        [FindsBy(How = How.CssSelector, Using = "div:nth-child(1) > crisp-input-number:nth-child(4) > div > label")]
        public IWebElement NumberOffStoreysBelowGround;

        [FindsBy(How = How.CssSelector, Using = "div:nth-child(1) > crisp-input-number:nth-child(4) > div > input")]
        public IWebElement NumberOffStoreysBelowGroundInput;

        [FindsBy(How = How.XPath, Using = "//crisp-input-radio[@label='Any innovative construction methods?']/div/ul/li/label[text()='No']")]
        public IWebElement InnovativeMethodsRadioButton;

        [FindsBy(How = How.XPath, Using = "//crisp-input-radio[@label='Is site in administration?']/div/ul/li/label[text()='No']")]
        public IWebElement SiteAdminRadioButton;

        //Plot Schedule Page Elements 

        [FindsBy(How = How.XPath, Using = "//crisp-header-button[@click.delegate='importPlots()']/span/button")]
        public IWebElement ImportPlotScheduleButton;

        [FindsBy(How = How.XPath, Using = "//div[@ref='theTable']//tbody//tr[@class='au-target collection-item']//td[2]")]
        public IWebElement WarrentyProduct;

        [FindsBy(How = How.XPath, Using = "//crisp-wizard-step[@step-title='Plot Schedule']//tbody//tr[@class='au-target collection-item']/td[3]")]
        public IWebElement BCProduct;

        [FindsBy(How = How.XPath, Using = "//crisp-wizard-step[@step-title='Plot Schedule']//tbody//tr[@class='au-target collection-item']/td[4]")]
        public IWebElement UnitType;

        [FindsBy(How = How.XPath, Using = "//crisp-wizard-step[@step-title='Plot Schedule']//tbody//tr[@class='au-target collection-item']/td[5]")]
        public IWebElement StagesOfWorks;

        //Product Details Page Elements

        [FindsBy(How = How.XPath, Using = "//div//crisp-input-object[@class='au-target']//div//label[text()='Product version']")]
        public IList<IWebElement> ProductVersion;

        [FindsBy(How = How.XPath, Using = "//crisp-wizard-step[@step-title='Products']//ul//li//crisp-list-item//div[@class='crisp-row']//span//dl//span//dd[contains(text(),'Aviva')]")]
        public IWebElement AvivaInsurer;

        [FindsBy(How = How.XPath, Using = "//crisp-input-radio[@label='Cover length']//div//ul//li//label[text()='10']")]
        public IList<IWebElement> CoverLengthList;

        [FindsBy(How = How.XPath, Using = "//crisp-input-radio[@label='Cover length']//div//ul//li//label[text()='12']")]
        public IList<IWebElement> CoverLengthList12Years;

        [FindsBy(How = How.XPath, Using = "//crisp-list/ul[@class='au-target collection has-header has-filter']//li[@class='au-target collection-item']")]
        public IList<IWebElement> ChooseProductVersionList;

        [FindsBy(How = How.XPath, Using = "//crisp-input-radio[@label='Are the works contracts under seal?']/div/ul/li/label[text()='Yes']")]
        public IList<IWebElement> ContractsUnderSeal;

        [FindsBy(How = How.XPath, Using = "//crisp-input-currency[@label='Contract cost']//div//label[text()='Contract cost']")]
        public IList<IWebElement> ContractCostLabel;

        [FindsBy(How = How.XPath, Using = "//crisp-input-currency[@label='Contract cost']//div//input")]
        public IList<IWebElement> ContractCostInput;       


        [FindsBy(How = How.XPath, Using = "//crisp-wizard-step[@step-title='Products']//crisp-card-content[@class='au-target']//crisp-input-object//div[@class='au-target input-field']//label[text()='Optional Covers']//parent::div//i[@click.delegate='addClicked()'][@class='fa fa-plus add action au-target']")]
        public IList<IWebElement> OptionalCovers;

        [FindsBy(How = How.XPath, Using = "//crisp-card[@class='au-target']//crisp-list//ul[@ref='theList']//li[@class='au-target collection-item']//a[@class='au-target row list-item-contents clickable']")]
        public IList<IWebElement> ChooseCoversList;

        [FindsBy(How = How.XPath, Using = "//crisp-card-actions//div//crisp-button[@click.delegate='ok()']//span//button[@class='au-target waves-effect waves-light btn']")]
        public IList<IWebElement> SelectButton;

        //Additional Questions Page Elements

        [FindsBy(How = How.XPath, Using = "//crisp-input-radio[@label='Are any units attached or structurally connected to any other structure not included within this application?']/div/ul/li/label[text()='No']")]
        public IList<IWebElement> NoOfUnitsAttachedRadioButton;

        [FindsBy(How = How.XPath, Using = "//crisp-input-radio[@label='Does the site include Curtain Walling/Glazing?']/div/ul/li/label[text()='Yes']")]
        public IList<IWebElement> WallingandGlazingRadioButton;

        [FindsBy(How = How.XPath, Using = "//crisp-wizard-step[@step-title='Additional Questions']//div//h3[text()='Conversions / Refurbishments']")]
        public IList<IWebElement> ConversionsAndRefurbishments;

        [FindsBy(How = How.XPath, Using = "//crisp-input-text[@label='Description of Works']//div//label")]
        public IWebElement DescriptionOfWorkLabel;

        [FindsBy(How = How.XPath, Using = "//crisp-input-text[@label='Description of Works']//div//input")]
        public IWebElement DescriptionOfWorkInput;

        [FindsBy(How = How.XPath, Using = "//div//crisp-input-year[@label='What was the approximate year of original build?']//div//label")]
        public IWebElement OrginalBuildLabel;

        [FindsBy(How = How.XPath, Using = "//div//crisp-input-year[@label='What was the approximate year of original build?']//div//input")]
        public IWebElement OrginalBuildInput;

        [FindsBy(How = How.XPath, Using = "//crisp-picker[@label='What was the previous use of the building?']//div/input")]
        public IWebElement PreviousBuildLabel;

        [FindsBy(How = How.XPath, Using = "//ul[@class='dropdown-content select-dropdown active']/li/span[text()='Residential']")]
        public IWebElement PreviousBuildInput;

        [FindsBy(How = How.XPath, Using = " //crisp-picker[@label='What is the listed status of the building?']//div/input")]
        public IWebElement StatusOfBuildLabel;

        [FindsBy(How = How.XPath, Using = "//ul[@class='dropdown-content select-dropdown active']/li/span[text()='Grade II']")]
        public IWebElement StatusOfBuildInput;

        [FindsBy(How = How.XPath, Using = "//crisp-input-radio[@label='Is the site in a conservation area?']//label[text()='No']")]
        public IWebElement ConversionArea;

        [FindsBy(How = How.XPath, Using = "//crisp-input-radio[@label='Have any other surveys or tests been carried out on the existing structure?']//label[text()='No']")]
        public IWebElement ExistingStructureSurvey;

        [FindsBy(How = How.XPath, Using = "//crisp-input-radio[@label='Has a condition survey been carried out?']//label[text()='No']")]
        public IWebElement ConditionSurvey;

        [FindsBy(How = How.XPath, Using = "//crisp-input-radio[@label='Has the Developer / Builder had experience in conversion or refurbishment projects?']//label[text()='No']")]
        public IWebElement BuilderExperienceInConversion;

        [FindsBy(How = How.XPath, Using = "//crisp-input-radio[@label='Does the development contain any barn conversions?']//label[text()='No']")]
        public IWebElement BarnConversion;       


        [FindsBy(How = How.XPath, Using = "//crisp-wizard-step//crisp-content//div//h3[text()='Self Build']")]
        public IList<IWebElement> SelfBuildAddtionalQuetions;

        [FindsBy(How = How.XPath, Using = "//crisp-input-radio[contains(@label,'Is an architect involved in this project?')]//div//ul//li//label")]
        public IList<IWebElement> IsArchitectOpen;

        [FindsBy(How = How.XPath, Using = "//crisp-picker[@label='Architect Level of Involvement']//div//input")]
        public IWebElement ArchitectLevelDropdown;

        [FindsBy(How = How.XPath, Using = "//ul[@class='dropdown-content select-dropdown active']//li")]
        public IList<IWebElement> ArchitectLevelInvolvementInput;        

        [FindsBy(How = How.XPath, Using = "//crisp-input-radio[contains(@label,'Has the home owner built')]/div/ul/li/label[text()='No']")]
        public IWebElement HomeOwnerBuilt;

        [FindsBy(How = How.XPath, Using = "//crisp-input-radio[contains(@label,'Is sole place of residence?')]/div/ul/li/label[text()='No']")]
        public IWebElement SolePlaceOFResidence;

        [FindsBy(How = How.XPath, Using = "//crisp-input-number[@label='Number of Stage Payments']//div//label[text()='Number of Stage Payments']")]
        public IWebElement StagePaymentLabel;

        [FindsBy(How = How.XPath, Using = "//crisp-input-number[@label='Number of Stage Payments']//div//input")]
        public IWebElement StagePaymentInput;

        [FindsBy(How = How.XPath, Using = "//crisp-wizard-step//crisp-content//div//h3[text()='Completed Housing']")]
        public IList<IWebElement> CompletedHousingAdditionalQuetions;

        [FindsBy(How = How.XPath, Using = "//crisp-wizard-step[@step-title='Additional Questions']//crisp-content//div//crisp-input-textarea[contains(@label,'Reason why a structural warranty was not previously put in place')]//div//label")]
        public IWebElement StructuralWarrentyReasonLabel;

        [FindsBy(How = How.XPath, Using = "//crisp-wizard-step[@step-title='Additional Questions']//crisp-content//div//crisp-input-textarea[contains(@label,'Reason why a structural warranty was not previously put in place')]//div//textarea")]
        public IWebElement StructuralWarrentyReasonInput;

        [FindsBy(How = How.XPath, Using = "//crisp-input-number[@label='Loss of Gross Profit - Amount']/div/label")]
        public IList<IWebElement> LossOfGrossProfit;

        [FindsBy(How = How.XPath, Using = "//crisp-input-number[@label='Loss of Gross Profit - Amount']/div/input")]
        public IWebElement LossOfGrossProfitInput;

        [FindsBy(How = How.XPath, Using = "//crisp-input-number[@label='Loss of Rent Receivable - Amount']/div/label")]
        public IList<IWebElement> LossOfRentReceivable;

        [FindsBy(How = How.XPath, Using = "//crisp-input-number[@label='Loss of Rent Receivable - Amount']/div/input")]
        public IWebElement LossOfRentReceivableInput;

        [FindsBy(How = How.XPath, Using = "//crisp-input-number[@label='Loss of Rent Payable - Amount']/div/label")]
        public IList<IWebElement> LossOfRentPayable;

        [FindsBy(How = How.XPath, Using = "//crisp-input-number[@label='Loss of Rent Payable - Amount']/div/input")]
        public IWebElement LossOfRentPayableInput;

        [FindsBy(How = How.XPath, Using = "//crisp-input-number[@label='Loss of Gross Profit - Number of years']/div/label")]
        public IList<IWebElement> LossOfGrossProfitYears;

        [FindsBy(How = How.XPath, Using = "//crisp-input-number[@label='Loss of Gross Profit - Number of years']/div/input")]
        public IWebElement LossOfGrossProfitYearsInput;

        [FindsBy(How = How.XPath, Using = "//crisp-input-number[@label='Loss of Rent Receivable - Number of years']/div/label")]
        public IList<IWebElement> LossOfRentReceivableYears;

        [FindsBy(How = How.XPath, Using = "//crisp-input-number[@label='Loss of Rent Receivable - Number of years']/div/input")]
        public IWebElement LossOfRentReceivableYearsInput;

        [FindsBy(How = How.XPath, Using = "//crisp-input-number[@label='Loss of Rent Payable - Number of years']/div/label")]
        public IList<IWebElement> LossOfRentPayableYears;

        [FindsBy(How = How.XPath, Using = "//crisp-input-number[@label='Loss of Rent Payable - Number of years']/div/input")]
        public IWebElement LossOfRentPayableYearsInput;

        [FindsBy(How = How.XPath, Using = "//crisp-input-number[@label='Duration in months atkins required on site?']/div/label")]
        public IList<IWebElement> AtkinsDurationLabel;

        [FindsBy(How = How.XPath, Using = "//crisp-input-number[@label='Duration in months atkins required on site?']/div/label[@class='au-target active']/following-sibling::input")]
        public IWebElement AtkinsDurationInput;

        //Roles Page Elements    

        [FindsBy(How = How.XPath, Using = "//crisp-wizard-step[@step-title='Roles']//div[@class='crisp-list-table-content au-target']//tbody[@class='collection']")]
        public IWebElement RolesTable;

        [FindsBy(How = How.XPath, Using = "//crisp-wizard-step[contains(@step-title,'Roles')]//role-editor//div//crisp-list//crisp-loader[@class='au-target aurelia-hide']//following-sibling::div[@class='au-target list-title has-filter']//crisp-header-button[@click.delegate='editRoles()']//span//button[@class='au-target waves-effect waves-light btn-flat']")]
        public IWebElement EditRoleButton;

        [FindsBy(How = How.XPath, Using = "//crisp-card-actions// div //crisp-button// span// button[@class='au-target waves-effect waves-light btn-flat'][text()='Cancel']")]
        public IWebElement CancelButton;

        [FindsBy(How = How.XPath, Using = "//crisp-card-content[@class='au-target']//ul[@ref='theList']//li[contains(@class,'au-target collection-item')]//crisp-list-item//a[contains(@class,'au-target row list-item-contents clickable')]//crisp-list-item-title//div//span[text()='Builder']")]
        public IList<IWebElement> BuilderRole;

        [FindsBy(How = How.XPath, Using = "//crisp-card-content[@class='au-target']//ul[@ref='theList']//li[@class='au-target collection-item']//crisp-list-item//a[contains(@class,'au-target row list-item-contents clickable')]//crisp-list-item-title//div//span[text()='Structural Referral Administrator']")]
        public IList<IWebElement> StructuralRefAdminRole;

        [FindsBy(How = How.XPath, Using = "//crisp-card-content[@class='au-target']//ul[@ref='theList']//li[@class='au-target collection-item']//crisp-list-item//a[contains(@class,'au-target row list-item-contents clickable')]//crisp-list-item-title//div//span[text()='Sales Account Manager']")]
        public IList<IWebElement> SalesAccountMaangerRole;

        [FindsBy(How = How.XPath, Using = "//crisp-card-content[@class='au-target']//ul[@ref='theList']//li[@class='au-target collection-item']//crisp-list-item//a[contains(@class,'au-target row list-item-contents clickable')]//crisp-list-item-title//div//span[text()='Hub Administrator']")]
        public IList<IWebElement> HubAdministratorRole;

        [FindsBy(How = How.XPath, Using = "//crisp-card-content[@class='au-target']//ul[@ref='theList']//li[@class='au-target collection-item']//crisp-list-item//a[contains(@class,'au-target row list-item-contents clickable')]//crisp-list-item-title//div//span[text()='Design Review Administrator']")]
        public IList<IWebElement> DesignReviewAdminRole;

        [FindsBy(How = How.XPath, Using = "//crisp-card-content[@class='au-target']//ul[@ref='theList']//li[@class='au-target collection-item']//crisp-list-item//a[contains(@class,'au-target row list-item-contents clickable')]//crisp-list-item-title//div//span[text()='Applicant (Self-build)']")]
        public IList<IWebElement> SelfBuildApplicantRole;

        [FindsBy(How = How.XPath, Using = "//crisp-card-content[@class='au-target']//ul[@ref='theList']//li[@class='au-target collection-item']//crisp-list-item//a[contains(@class,'au-target row list-item-contents clickable')]//crisp-list-item-title//div//span[text()='Architect']")]
        public IList<IWebElement> ArchitectRole;

        [FindsBy(How = How.XPath, Using = "//crisp-card-content[@class='au-target']//ul[@ref='theList']//li[@class='au-target collection-item']//crisp-list-item//a[contains(@class,'au-target row list-item-contents clickable')]//crisp-list-item-title//div//span[text()='Housing Association']")]
        public IList<IWebElement> HousingAssociationRole;

        [FindsBy(How = How.XPath, Using = "//crisp-card-content[@class='au-target']//ul[@ref='theList']//li[@class='au-target collection-item']//crisp-list-item//a[contains(@class,'au-target row list-item-contents clickable')]//crisp-list-item-title//div//span[text()='PRS Company']")]
        public IList<IWebElement> PRSCompanyRole;

        [FindsBy(How = How.XPath, Using = "//crisp-card-content[@class='au-target']//ul[@ref='theList']//li[@class='au-target collection-item']//crisp-list-item//a[contains(@class,'au-target row list-item-contents clickable')]//crisp-list-item-title//div//span[text()='Design Review Surveyor']")]
        public IList<IWebElement> DesignReviewSurveyorRole;

        [FindsBy(How = How.XPath, Using = "//crisp-card-content[@class='au-target']//ul[@ref='theList']//li[@class='au-target collection-item']//crisp-list-item//a[contains(@class,'au-target row list-item-contents clickable')]//crisp-list-item-title//div//span[text()='Developer']")]
        public IList<IWebElement> DeveloperRole;

        [FindsBy(How = How.XPath, Using = "//crisp-card-content[@class='au-target']//ul[@ref='theList']//li[@class='au-target collection-item']//crisp-list-item//a[contains(@class,'au-target row list-item-contents clickable')]//crisp-list-item-title//div//span[text()='Technical Administrator']")]
        public IList<IWebElement> TechAdminRole;

        //[FindsBy(How = How.XPath, Using = "//crisp-card-content[@class='au-target']//ul[@ref='theList']/li[@class='au-target collection-item']")]
        //public IList<IWebElement> LABCRole;

        [FindsBy(How = How.XPath, Using = "//crisp-wizard-step[@step-title='Roles']//tbody[@class='collection']//tr[@class='au-target collection-item']//td[@class='au-target fixed-width error']//em[text()='Not selected']")]
        public IList<IWebElement> MandetoryRole;

        [FindsBy(How = How.XPath, Using = "//crisp-wizard-step[@step-title='Roles']//tbody[@class='collection']//tr[@class='au-target collection-item']//td[@class='au-target fixed-width error'][1]")]
        public IList<IWebElement> Roles;

        [FindsBy(How = How.XPath, Using = "//crisp-card-content[@class='au-target']//ul[@ref='theList']//li[@class='au-target collection-item']//crisp-list-item//a[contains(@class,'au-target row list-item-contents clickable')]//crisp-list-item-title//div//span[text()='Land Owner']")]
        public IList<IWebElement> LandOwnerRole;

        [FindsBy(How = How.XPath, Using = "//crisp-card-content[@class='au-target']//ul[@ref='theList']//li[@class='au-target collection-item']//crisp-list-item//a[contains(@class,'au-target row list-item-contents clickable')]//crisp-list-item-title//div//span[text()='Surveyor']")]
        public IList<IWebElement> SurveyorRole;

        [FindsBy(How = How.XPath, Using = "//crisp-card-content[@class='au-target']//ul[@ref='theList']//li[@class='au-target collection-item']//crisp-list-item//a[contains(@class,'au-target row list-item-contents clickable')]//crisp-list-item-title//div//span[text()='Refurbishment Assessment Surveyor']")]
        public IList<IWebElement> RefurbishmentAssessmentSurveyorRole;

        [FindsBy(How = How.XPath, Using = "//crisp-card-content[@class='au-target']//ul[@ref='theList']//li[@class='au-target collection-item']//crisp-list-item//a[contains(@class,'au-target row list-item-contents clickable')]//crisp-list-item-title//div//span[text()='Building Control Provider']")]
        public IList<IWebElement> BuildingControlProviderRole;

        //Company List 
        [FindsBy(How = How.XPath, Using = "//crisp-list[@ref='listElm']//ul[@ref='theList']//li[@class='au-target collection-item']//crisp-list-item//a[contains(@class,'au-target row list-item-contents clickable hasAvatar')]")]
        public IList<IWebElement> CompanyList;

        [FindsBy(How = How.XPath, Using = "//crisp-card-content[@class='au-target']//ul[@ref='theList']//li[@class='au-target collection-item']//crisp-list-item[@class='au-target']//a[contains(@class,'au-target row list-item-contents clickable hasAvatar')]")]
        public IList<IWebElement> EmpList;

        [FindsBy(How = How.XPath, Using = "//crisp-card-content[@class='au-target']//span/input[@type='text']")]
        public IWebElement SearchRoleLabel;

        //[FindsBy(How = How.CssSelector, Using = "crisp-card > crisp-card-actions > div > crisp-button:nth-child(1) > span > button")]
        //public IWebElement ChooseCompanyButton;

        [FindsBy(How = How.XPath, Using = "//crisp-card-content/div/div/div/crisp-input-text/div/label[text()='Search']")]
        public IWebElement SearchEmployeeLabel;

        [FindsBy(How = How.XPath, Using = "//crisp-card-content/div/div/div/crisp-input-text/div/input")]
        public IWebElement SearchEmployeeInput;

        //Declaration Page Elements 

        [FindsBy(How = How.XPath, Using = "//crisp-input-radio[@label='Applicant has confirmed that all details are correct']/div/ul/li/label[text()='Yes']")]
        public IWebElement ApplicationConfirmationRadiobutton;

        [FindsBy(How = How.XPath, Using = "//crisp-wizard-step[@step-title='Declaration']//crisp-input-radio/div/ul/li/label[text()='No']")]
        public IList<IWebElement> DeclarationPageRadioButtons;

        [FindsBy(How = How.XPath, Using = "//crisp-footer/nav/div/crisp-button[@class='au-target']//span//button[text()='Save'][@class='au-target waves-effect waves-light btn-flat']")]
        public IWebElement SaveButton;

        [FindsBy(How = How.XPath, Using = "//crisp-footer/nav/div/crisp-button[@class='au-target']/span/button[text()='Submit'][@class='au-target waves-effect waves-light btn']")]
        public IWebElement SubmitButton;

        [FindsBy(How = How.XPath, Using = "//crisp-button[@class='au-target']/span/button[text()='Ok']")]
        public IWebElement OkButton;

        [FindsBy(How = How.XPath, Using = "//div//crisp-header-title[@class='au-target'][1]/div[@class='title']")]
        public IWebElement SiteRefDetails;

        //Additional Buttons 

        [FindsBy(How = How.XPath, Using = "//crisp-button[@click.delegate='additionalButtonClicked(btn)'][@class='au-target']//button[text()='Add Person'][@class='au-target waves-effect waves-light btn-flat']")]
        public IWebElement AddPersonButton;

        [FindsBy(How = How.XPath, Using = "//crisp-button[@click.delegate='additionalButtonClicked(btn)'][@class='au-target']//button[text()='Add Company'][@class='au-target waves-effect waves-light btn-flat']")]
        public IWebElement AddCompanyButton;

        [FindsBy(How = How.XPath, Using = "//crisp-button[@click.delegate='additionalButtonClicked(btn)'][@class='au-target']//button[text()='Add employee'][@class='au-target waves-effect waves-light btn-flat']")]
        public IList<IWebElement> AddEmployeeButton;

        //Site Address Elements 

        [FindsBy(How = How.XPath, Using = "//div[@class='card-content']//crisp-input-text[@label='Address Line 1']//div//label[@class='au-target']")]
        public IWebElement AddressLine1Label;

        [FindsBy(How = How.XPath, Using = "//div[@class='card-content']//crisp-input-text[@label='Address Line 1']//div//input")]
        public IWebElement AddressLine1Input;

        [FindsBy(How = How.XPath, Using = "//div[@class='card-content']//crisp-input-text[@label='Town']//div//label[@class='au-target']")]
        public IWebElement TownLabel;

        [FindsBy(How = How.XPath, Using = "//div[@class='card-content']//crisp-input-text[@label='Town']//div//input")]
        public IWebElement TownInput;

        [FindsBy(How = How.XPath, Using = "//div[@class='card-content']//crisp-input-text[@label='Postcode']//div//label[@class='au-target']")]
        public IWebElement SitePostcodeLabel;

        [FindsBy(How = How.XPath, Using = "//div[@class='card-content']//crisp-input-text[@label='Postcode']//div//input")]
        public IWebElement SitePostcodeInput;

        //Local Autority email address 

        [FindsBy(How = How.XPath, Using = "//crisp-tabs//div//ul[@ref='tabHeader']/li[@class='au-target tab']//span//a[contains(text(),'Conditions')]")]
        public IWebElement ConditionsTab;

        [FindsBy(How = How.XPath, Using = "//condition-list[@class='custom-element au-target']//div[@class='au-target crisp-row table-body with-header']//table[@ref='table']//tbody//tr")]
        public IList<IWebElement> ConditionsRows;

        [FindsBy(How = How.XPath, Using = "//crisp-tabs//div//ul[@ref='tabHeader']/li[@class='au-target tab']//span//a[contains(text(),'Roles')]")]
        public IWebElement RolesTab;

        [FindsBy(How = How.XPath, Using = "//div[@ref='theTable']//tbody//tr//td[contains(text(),'Planning and Local Authority')]//following-sibling::td[2][contains(@title.bind,'Email')]")]
        public IList<IWebElement> LocalAuthorityEmails;

        [FindsBy(How = How.XPath, Using = "//div[@class='card-content']//crisp-card-actions//crisp-button[@click.delegate='editContact(selectedRole)']//span//button")]
        public IWebElement EditCompanyButton;

        [FindsBy(How = How.XPath, Using = "//div//crisp-dialog//div//crisp-header//nav/div[@ref='wrapper']")]
        public IWebElement EditCompanyDiv;

        [FindsBy(How = How.XPath, Using = "//crisp-button[@click.delegate='goForward()']//span//button")]
        public IWebElement EditCompanyNextButton;

        [FindsBy(How = How.XPath, Using = "//crisp-list[@list-title='Offices']/ul[@ref='theList']//li[contains(@class,'au-target collection-item')]//crisp-list-item//a//crisp-list-item-details//div//div[contains(text(),'Registered office')]")]
        public IList<IWebElement> OfficeAddress;

        [FindsBy(How = How.XPath, Using = "//crisp-card-actions//div/crisp-action-button[@click.delegate='editOffice(selectedOffice)']//div//a//i[text()='edit']")]
        public IWebElement EditOfficeAddressButton;

        [FindsBy(How = How.XPath, Using = "//crisp-tabs//div//ul[@ref='tabHeader']/li[@class='au-target tab']//span//a[contains(text(),'Overview')]")]
        public IWebElement OverviewTab;

        [FindsBy(How = How.XPath, Using = "//crisp-button[@click.delegate='save()']//span//button[@class='au-target waves-effect waves-light btn']")]
        public IWebElement SaveCompanyButton;

        //Add Local Authority Role 

        [FindsBy(How = How.XPath, Using = "//div[@class='modal-content']/crisp-input-text[@label='Name']/div/input[@ref='input'][@type='text']")]
        public IWebElement OfficeNameInput;

        [FindsBy(How = How.XPath, Using = "//crisp-input-text[@label='Email']/div/label")]
        public IList<IWebElement> EmailLabel;

        [FindsBy(How = How.XPath, Using = "//crisp-input-text[@label='Email']/div/input[@ref='input']")]
        public IWebElement EmailInput;

        [FindsBy(How = How.XPath, Using = "//crisp-button[@enabled.bind='isOfficeDataValid']/span/button")]
        public IWebElement AddOfficeDataButton;

        [FindsBy(How = How.XPath, Using = "//crisp-button[@enabled.bind='canGoForward'][@click.delegate='goForward()']/span/button[@ref='button'][text()='Next']")]
        public IWebElement EmpNextButton;


        //Select product Version Date

        [FindsBy(How = How.XPath, Using = "//crisp-input-date[@label='Product version date']//div//input")]
        public IWebElement ProductVersionInput;

        [FindsBy(How = How.XPath, Using = "//div[contains(@class,'picker--opened')]//div[@class='picker__header']//select[@title='Select a year']")]
        public IWebElement SelectYearDropdown;

        [FindsBy(How = How.XPath, Using = "//div[contains(@class,'picker--opened')]//div[@class='picker__header']//select[@title='Select a year']//option")]
        public IList<IWebElement> SelectYearInput;

        [FindsBy(How = How.XPath, Using = "//div[contains(@class,'picker--opened')]//table[@class='picker__table']/tbody//tr//td//div[contains(@class,'day--infocus')]")]
        public IList<IWebElement> SelectDate;
        //FutureData
        static DateTime date = DateTime.Today.AddDays(+1); // will give the date for today       
        private string productVersionDate = date.ToString("04/02/2020");
        public string contractcost = "12000";
        

        public void SelectProductVersionDate()
        {
            WaitForLoadElement(ProductVersionInput);
            ProductVersionInput.Click();
            Thread.Sleep(2000);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(2000);
            WaitForElement(SelectYearDropdown);
            SelectYearDropdown.Click();
            Thread.Sleep(2000);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(2000);
            WaitForElements(SelectYearInput);
            var ProductVersionYear = SelectYearInput.FirstOrDefault(x => x.Text == ConfigurationManager.AppSettings["ProductVersionYear"]);
            if (ProductVersionYear != null)
            {
                ProductVersionYear.Click();
            }
            Thread.Sleep(1000);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(1000);
            WaitForElements(SelectDate);
            var ProductVersionDate = SelectDate.FirstOrDefault(x => x.Text == ConfigurationManager.AppSettings["ProductVersionDate"]);
            if (ProductVersionDate != null)
            {
                ProductVersionDate.Click();
            }
            Thread.Sleep(2000);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(2000);
            //Thread.Sleep(1000);
            //var DateInput = $"//div[contains(@class,'picker--opened')]//div[@class='picker__box']//tbody//tr//td//div[@aria-label='04/02/2019']";
            //CloseSpinneronDiv();
            //CloseSpinneronPage();
            //Thread.Sleep(1000);
            //Driver.FindElement(By.XPath(DateInput)).Click();
            //CloseSpinneronDiv();
            //CloseSpinneronPage();
            //Thread.Sleep(1000);
        }

        public void UATSiteAddress()
        {
            Random rnd = new Random();
            int number = rnd.Next(1, 3);
            CloseSpinneronDiv();
            WaitForElement(AddressLine1Label);
            //Thread.Sleep(2000);
            WaitForElement(AddressLine1Input);
            AddressLine1Input.SendKeys($"LABC UAT Surveyor Site {number}");
            WaitForElement(TownLabel);
            TownLabel.Click();
            //Thread.Sleep(2000);
            WaitForElement(TownInput);
            TownInput.SendKeys("Test Town");
            WaitForElement(SitePostcodeLabel);
            SitePostcodeLabel.Click();
            //Thread.Sleep(2000);
            WaitForElement(SitePostcodeInput);
            SitePostcodeInput.SendKeys("L1");
            //Thread.Sleep(2000);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(2000);
        }


        // Adding Key Site Details for LABC quote 
        public void KeySiteDetailsPage()
        {
            WaitForElement(StepTitle);
            Assert.IsTrue(StepTitle.Text.Contains("KEY SITE DETAILS"), $"Failed to Display {StepTitle.Text}");
            //Adding Key SiteDetails Page 
            CloseSpinneronDiv();
            CloseSpinneronPage();
            if (LABCRadioButton.Count > 0)
            {
                WaitForElement(LABCRadioButton[0]);
                LABCRadioButton[0].Click();
            }
            CloseSpinneronDiv();
            CloseSpinneronPage();

            if (QuoteRecipientInput.Count == 0)
            {
                CloseSpinneronDiv();
                CloseSpinneronPage();
                WaitForElement(QuoteRecipientLabel);
                QuoteRecipientLabel.Click();
                CloseSpinneronDiv();
                //Thread.Sleep(500);
                WaitForElement(SearchLabel);
                CloseSpinneronDiv();
                SearchLabel.Click();
                //Thread.Sleep(1000);
                WaitForElement(SearchInput);
                //Thread.Sleep(1000);
                SearchInput.SendKeys("Mr Sunil ");
                //Thread.Sleep(500);
                SearchInput.SendKeys("Sunkishala");
                Thread.Sleep(1000);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                Thread.Sleep(1000);
                if (QuoteRecipientList.Count > 0)
                {
                    //Thread.Sleep(3000);
                    CloseSpinneronDiv();
                    //Thread.Sleep(3000);
                    WaitForElement(QuoteRecipientList[0]);
                    //Thread.Sleep(3000);
                    CloseSpinneronDiv();
                    //Thread.Sleep(3000);
                    QuoteRecipientList[0].Click();
                    CloseSpinneronDiv();
                    //Thread.Sleep(3000);
                }
                else
                {
                    WaitForElement(AddPersonButton);
                    AddPersonButton.Click();
                    AdditionalMethodsPage.AddPerson();
                    Thread.Sleep(1000);
                    CloseSpinneronPage();
                    CloseSpinneronDiv();
                    Thread.Sleep(1000);
                }
            }

            if (SiteAddressInput.Count == 0)
            {
                Thread.Sleep(1000);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                Thread.Sleep(1000);
                WaitForElement(SiteAddressLabel);
                //Thread.Sleep(2000);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                //Thread.Sleep(2000);
                SiteAddressLabel.Click();
                Thread.Sleep(1000);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                Thread.Sleep(1000);
                UATSiteAddress();
                //WaitForElement(EnterAddressUsingPostcodeButton);
                //EnterAddressUsingPostcodeButton.Click();
                //WaitForElement(PostcodeSearchLabel);
                //PostcodeSearchLabel.Click();
                //WaitForElement(PostcodeInput);
                //PostcodeInput.SendKeys("CH41 1AU");
                //WaitForElement(AddressDropdown);
                //AddressDropdown.Click();
                Thread.Sleep(500);
                //WaitForElement(AddressDropdownInput);
                //AddressDropdownInput.Click();
                WaitForElement(SetAddressButton);
                SetAddressButton.Click();
            }
            //Thread.Sleep(1000);
            CloseCrispCard();
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(1000);
            if (QuoteRecipientOfficeLabel.Count > 0)
            {
                //Thread.Sleep(1000);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                //Thread.Sleep(1000);
                QuoteRecipientOfficeLabel[0].Click();
                //Thread.Sleep(1000);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                //Thread.Sleep(1000);
                WaitForElements(QuoteRecipientOfficeInput);
                //Thread.Sleep(1000);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                //Thread.Sleep(1000);
                if (QuoteRecipientOfficeInput.Count > 0)
                {
                    QuoteRecipientOfficeInput[0].Click();
                    //Thread.Sleep(1000);
                }
            }
            CloseSpinneronDiv();
            CloseSpinneronPage();
            CloseCrispCard();
            Thread.Sleep(1000);
            if (Statics.ProductNameList.Any(x => x.Contains("High Value")))
            {
                if (InsuranceGroup.Count > 0)
                {
                    Thread.Sleep(1000);
                    CloseSpinneronPage();
                    CloseSpinneronDiv();
                    Thread.Sleep(1000);
                    WaitForElement(InsuranceGroup[0]);
                    WaitForElementToClick(InsuranceGroup[0]);
                    InsuranceGroup[0].Click();
                    CloseSpinneronDiv();
                    //Thread.Sleep(1000);
                    WaitForElements(InsuranceGroupInputs);
                    var insuranceGroup = InsuranceGroupInputs.FirstOrDefault(o => o.Text.Contains("Axa HVS"));
                    if (insuranceGroup.Displayed)
                    {
                        insuranceGroup.Click();
                    }
                    CloseSpinneronDiv();
                    //Thread.Sleep(1000);
                }
            }
            //SelectProductVersionDate();
            CloseSpinneronDiv();
            CloseSpinneronPage();
            CloseCrispCard();
            Thread.Sleep(1000);
            WaitForElement(AddQuoteAppDocs);
            AddQuoteAppDocs.Click();
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(2000);
            WaitForElement(UploadDocWizard);
            Assert.IsTrue(UploadDocWizard.Displayed);
            var fileInfo = new FileInfo(ExtensionMethods.GetAbsolutePath(@"TestData\QuotesData\CRISP install guide.docx"));
            UploadDoc.SendKeys(fileInfo.ToString());
            //Thread.Sleep(1000);
            CloseSpinneronDiv();
            WaitForElement(UploadOkButton);
            //Thread.Sleep(500);
            UploadOkButton.Click();
            //Thread.Sleep(1000);
            WaitForElement(NextButton);
            NextButton.Click();
            CloseSpinneronDiv();
            CloseSpinneronPage();
        }
        // Adding Key Site Details for LABC quote 
        public void KeySiteDetailsForAviva()
        {
            WaitForElement(StepTitle);
            Assert.IsTrue(StepTitle.Text.Contains("KEY SITE DETAILS"), $"Failed to Display {StepTitle.Text}");
            //Adding Key SiteDetails Page 
            if (LABCRadioButton.Count > 0)
            {
                WaitForElement(LABCRadioButton[0]);
                LABCRadioButton[0].Click();
            }

            if (QuoteRecipientInput.Count == 0)
            {
                WaitForElement(QuoteRecipientLabel);
                QuoteRecipientLabel.Click();
                CloseSpinneronDiv();
                //Thread.Sleep(500);
                WaitForElement(SearchLabel);
                CloseSpinneronDiv();
                SearchLabel.Click();
                //Thread.Sleep(1000);
                WaitForElement(SearchInput);
                //Thread.Sleep(1000);
                SearchInput.SendKeys("Mr Sunil ");
                //Thread.Sleep(500);
                SearchInput.SendKeys("Sunkishala");
                Thread.Sleep(1000);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                Thread.Sleep(1000);
                if (QuoteRecipientList.Count > 0)
                {
                    //Thread.Sleep(3000);
                    CloseSpinneronDiv();
                    //Thread.Sleep(3000);
                    WaitForElement(QuoteRecipientList[0]);
                    //Thread.Sleep(3000);
                    CloseSpinneronDiv();
                    //Thread.Sleep(3000);
                    QuoteRecipientList[0].Click();
                    CloseSpinneronDiv();
                    //Thread.Sleep(3000);
                }
                else
                {
                    WaitForElement(AddPersonButton);
                    AddPersonButton.Click();
                    AdditionalMethodsPage.AddPerson();
                    Thread.Sleep(1000);
                    CloseSpinneronPage();
                    CloseSpinneronDiv();
                    Thread.Sleep(1000);
                }
            }

            if (SiteAddressInput.Count == 0)
            {
                Thread.Sleep(1000);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                Thread.Sleep(1000);
                WaitForElement(SiteAddressLabel);
                //Thread.Sleep(2000);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                //Thread.Sleep(2000);
                SiteAddressLabel.Click();
                Thread.Sleep(1000);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                Thread.Sleep(1000);
                UATSiteAddress();
                //WaitForElement(EnterAddressUsingPostcodeButton);
                //EnterAddressUsingPostcodeButton.Click();
                //WaitForElement(PostcodeSearchLabel);
                //PostcodeSearchLabel.Click();
                //WaitForElement(PostcodeInput);
                //PostcodeInput.SendKeys("CH41 1AU");
                //WaitForElement(AddressDropdown);
                //AddressDropdown.Click();
                Thread.Sleep(500);
                //WaitForElement(AddressDropdownInput);
                //AddressDropdownInput.Click();
                WaitForElement(SetAddressButton);
                SetAddressButton.Click();
            }
            //Thread.Sleep(1000);
            CloseCrispCard();
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(1000);
            if (QuoteRecipientOfficeLabel.Count > 0)
            {
                //Thread.Sleep(1000);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                //Thread.Sleep(1000);
                QuoteRecipientOfficeLabel[0].Click();
                //Thread.Sleep(1000);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                //Thread.Sleep(1000);
                WaitForElements(QuoteRecipientOfficeInput);
                //Thread.Sleep(1000);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                //Thread.Sleep(1000);
                if (QuoteRecipientOfficeInput.Count > 0)
                {
                    QuoteRecipientOfficeInput[0].Click();
                    //Thread.Sleep(1000);
                }
            }
            CloseSpinneronDiv();
            CloseSpinneronPage();
            CloseCrispCard();
            Thread.Sleep(1000);
            if (Statics.ProductNameList.Any(x => x.Contains("High Value")))
            {
                    if (InsuranceGroup.Count > 0)
                    {
                        Thread.Sleep(1000);
                        CloseSpinneronPage();
                        CloseSpinneronDiv();
                        Thread.Sleep(1000);
                        WaitForElement(InsuranceGroup[0]);
                        WaitForElementToClick(InsuranceGroup[0]);
                        InsuranceGroup[0].Click();
                        CloseSpinneronDiv();
                        //Thread.Sleep(1000);
                        WaitForElements(InsuranceGroupInputs);
                        var insuranceGroup = InsuranceGroupInputs.FirstOrDefault(o => o.Text.Contains("Aviva"));
                        if (insuranceGroup.Displayed)
                        {
                            insuranceGroup.Click();
                        }
                        CloseSpinneronDiv();
                        //Thread.Sleep(1000);
                    }
                
            }
            //SelectProductVersionDate();
            CloseSpinneronDiv();
            CloseSpinneronPage();
            CloseCrispCard();
            Thread.Sleep(1000);
            WaitForElement(AddQuoteAppDocs);
            AddQuoteAppDocs.Click();
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(2000);
            WaitForElement(UploadDocWizard);
            Assert.IsTrue(UploadDocWizard.Displayed);
            var fileInfo = new FileInfo(ExtensionMethods.GetAbsolutePath(@"TestData\QuotesData\CRISP install guide.docx"));
            UploadDoc.SendKeys(fileInfo.ToString());
            //Thread.Sleep(1000);
            CloseSpinneronDiv();
            WaitForElement(UploadOkButton);
            //Thread.Sleep(500);
            UploadOkButton.Click();
            //Thread.Sleep(1000);
            WaitForElement(NextButton);
            NextButton.Click();
            CloseSpinneronDiv();
            CloseSpinneronPage();
        }
        // Adding Key Site Details for LABC quote 
        public void KeySiteDetailsPageOnLatestProductVersion()
        {
            WaitForElement(StepTitle);
            Assert.IsTrue(StepTitle.Text.Contains("KEY SITE DETAILS"), $"Failed to Display {StepTitle.Text}");
            //Adding Key SiteDetails Page 
            CloseSpinneronDiv();
            CloseSpinneronPage();
            if (LABCRadioButton.Count > 0)
            {
                WaitForElement(LABCRadioButton[0]);
                LABCRadioButton[0].Click();
            }
            CloseSpinneronDiv();
            CloseSpinneronPage();

            if (QuoteRecipientInput.Count == 0)
            {
                CloseSpinneronDiv();
                CloseSpinneronPage();
                WaitForElement(QuoteRecipientLabel);
                QuoteRecipientLabel.Click();
                CloseSpinneronDiv();
                //Thread.Sleep(500);
                WaitForElement(SearchLabel);
                CloseSpinneronDiv();
                SearchLabel.Click();
                //Thread.Sleep(1000);
                WaitForElement(SearchInput);
                //Thread.Sleep(1000);
                SearchInput.SendKeys("Mr Sunil ");
                //Thread.Sleep(500);
                SearchInput.SendKeys("Sunkishala");
                Thread.Sleep(1000);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                Thread.Sleep(1000);
                if (QuoteRecipientList.Count > 0)
                {
                    //Thread.Sleep(3000);
                    CloseSpinneronDiv();
                    //Thread.Sleep(3000);
                    WaitForElement(QuoteRecipientList[0]);
                    //Thread.Sleep(3000);
                    CloseSpinneronDiv();
                    //Thread.Sleep(3000);
                    QuoteRecipientList[0].Click();
                    CloseSpinneronDiv();
                    //Thread.Sleep(3000);
                }
                else
                {
                    WaitForElement(AddPersonButton);
                    AddPersonButton.Click();
                    AdditionalMethodsPage.AddPerson();
                    Thread.Sleep(1000);
                    CloseSpinneronPage();
                    CloseSpinneronDiv();
                    Thread.Sleep(1000);
                }
            }

            if (SiteAddressInput.Count == 0)
            {
                Thread.Sleep(1000);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                Thread.Sleep(1000);
                WaitForElement(SiteAddressLabel);
                //Thread.Sleep(2000);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                //Thread.Sleep(2000);
                SiteAddressLabel.Click();
                Thread.Sleep(1000);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                Thread.Sleep(1000);
                UATSiteAddress();
                //WaitForElement(EnterAddressUsingPostcodeButton);
                //EnterAddressUsingPostcodeButton.Click();
                //WaitForElement(PostcodeSearchLabel);
                //PostcodeSearchLabel.Click();
                //WaitForElement(PostcodeInput);
                //PostcodeInput.SendKeys("CH41 1AU");
                //WaitForElement(AddressDropdown);
                //AddressDropdown.Click();
                Thread.Sleep(500);
                //WaitForElement(AddressDropdownInput);
                //AddressDropdownInput.Click();
                WaitForElement(SetAddressButton);
                SetAddressButton.Click();
            }
            //Thread.Sleep(1000);
            CloseCrispCard();
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(1000);
            if (QuoteRecipientOfficeLabel.Count > 0)
            {
                //Thread.Sleep(1000);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                //Thread.Sleep(1000);
                QuoteRecipientOfficeLabel[0].Click();
                //Thread.Sleep(1000);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                //Thread.Sleep(1000);
                WaitForElements(QuoteRecipientOfficeInput);
                //Thread.Sleep(1000);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                //Thread.Sleep(1000);
                if (QuoteRecipientOfficeInput.Count > 0)
                {
                    QuoteRecipientOfficeInput[0].Click();
                    //Thread.Sleep(1000);
                }
            }
            CloseSpinneronDiv();
            CloseSpinneronPage();
            CloseCrispCard();
            Thread.Sleep(1000);
            if (Statics.ProductNameList.Any(x => x.Contains("High Value")))
            {
                if (InsuranceGroup.Count > 0)
                {
                    Thread.Sleep(1000);
                    CloseSpinneronPage();
                    CloseSpinneronDiv();
                    Thread.Sleep(1000);
                    WaitForElement(InsuranceGroup[0]);
                    WaitForElementToClick(InsuranceGroup[0]);
                    InsuranceGroup[0].Click();
                    CloseSpinneronDiv();
                    //Thread.Sleep(1000);
                    WaitForElements(InsuranceGroupInputs);
                    var insuranceGroup = InsuranceGroupInputs.FirstOrDefault(o => o.Text.Contains("Axa HVS"));
                    if (insuranceGroup.Displayed)
                    {
                        insuranceGroup.Click();
                    }
                    CloseSpinneronDiv();
                    //Thread.Sleep(1000);
                }
            }
            SelectProductVersionDate();
            CloseSpinneronDiv();
            CloseSpinneronPage();
            CloseCrispCard();
            Thread.Sleep(1000);
            WaitForElement(AddQuoteAppDocs);
            AddQuoteAppDocs.Click();
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(2000);
            WaitForElement(UploadDocWizard);
            Assert.IsTrue(UploadDocWizard.Displayed);
            var fileInfo = new FileInfo(ExtensionMethods.GetAbsolutePath(@"TestData\QuotesData\CRISP install guide.docx"));
            UploadDoc.SendKeys(fileInfo.ToString());
            //Thread.Sleep(1000);
            CloseSpinneronDiv();
            WaitForElement(UploadOkButton);
            //Thread.Sleep(500);
            UploadOkButton.Click();
            //Thread.Sleep(1000);
            WaitForElement(NextButton);
            NextButton.Click();
            CloseSpinneronDiv();
            CloseSpinneronPage();
        }
        // Adding Key Site Details for LABC quote 
        public void KeySiteDetailsForAvivaOnLatestProductVersion()
        {
            WaitForElement(StepTitle);
            Assert.IsTrue(StepTitle.Text.Contains("KEY SITE DETAILS"), $"Failed to Display {StepTitle.Text}");
            //Adding Key SiteDetails Page 
            CloseSpinneronDiv();
            CloseSpinneronPage();
            if (LABCRadioButton.Count > 0)
            {
                WaitForElement(LABCRadioButton[0]);
                LABCRadioButton[0].Click();
            }
            CloseSpinneronDiv();
            CloseSpinneronPage();

            if (QuoteRecipientInput.Count == 0)
            {
                CloseSpinneronDiv();
                CloseSpinneronPage();
                WaitForElement(QuoteRecipientLabel);
                QuoteRecipientLabel.Click();
                CloseSpinneronDiv();
                //Thread.Sleep(500);
                WaitForElement(SearchLabel);
                CloseSpinneronDiv();
                SearchLabel.Click();
                //Thread.Sleep(1000);
                WaitForElement(SearchInput);
                //Thread.Sleep(1000);
                SearchInput.SendKeys("Mr Sunil ");
                //Thread.Sleep(500);
                SearchInput.SendKeys("Sunkishala");
                Thread.Sleep(1000);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                Thread.Sleep(1000);
                if (QuoteRecipientList.Count > 0)
                {
                    //Thread.Sleep(3000);
                    CloseSpinneronDiv();
                    //Thread.Sleep(3000);
                    WaitForElement(QuoteRecipientList[0]);
                    //Thread.Sleep(3000);
                    CloseSpinneronDiv();
                    //Thread.Sleep(3000);
                    QuoteRecipientList[0].Click();
                    CloseSpinneronDiv();
                    //Thread.Sleep(3000);
                }
                else
                {
                    WaitForElement(AddPersonButton);
                    AddPersonButton.Click();
                    AdditionalMethodsPage.AddPerson();
                    Thread.Sleep(1000);
                    CloseSpinneronPage();
                    CloseSpinneronDiv();
                    Thread.Sleep(1000);
                }
            }

            if (SiteAddressInput.Count == 0)
            {
                Thread.Sleep(1000);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                Thread.Sleep(1000);
                WaitForElement(SiteAddressLabel);
                //Thread.Sleep(2000);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                //Thread.Sleep(2000);
                SiteAddressLabel.Click();
                Thread.Sleep(1000);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                Thread.Sleep(1000);
                UATSiteAddress();
                //WaitForElement(EnterAddressUsingPostcodeButton);
                //EnterAddressUsingPostcodeButton.Click();
                //WaitForElement(PostcodeSearchLabel);
                //PostcodeSearchLabel.Click();
                //WaitForElement(PostcodeInput);
                //PostcodeInput.SendKeys("CH41 1AU");
                //WaitForElement(AddressDropdown);
                //AddressDropdown.Click();
                Thread.Sleep(500);
                //WaitForElement(AddressDropdownInput);
                //AddressDropdownInput.Click();
                WaitForElement(SetAddressButton);
                SetAddressButton.Click();
            }
            //Thread.Sleep(1000);
            CloseCrispCard();
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(1000);
            if (QuoteRecipientOfficeLabel.Count > 0)
            {
                //Thread.Sleep(1000);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                //Thread.Sleep(1000);
                QuoteRecipientOfficeLabel[0].Click();
                //Thread.Sleep(1000);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                //Thread.Sleep(1000);
                WaitForElements(QuoteRecipientOfficeInput);
                //Thread.Sleep(1000);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                //Thread.Sleep(1000);
                if (QuoteRecipientOfficeInput.Count > 0)
                {
                    QuoteRecipientOfficeInput[0].Click();
                    //Thread.Sleep(1000);
                }
            }
            CloseSpinneronDiv();
            CloseSpinneronPage();
            CloseCrispCard();
            Thread.Sleep(1000);
            if (Statics.ProductNameList.Any(x => x.Contains("High Value")))
            {
                if (InsuranceGroup.Count > 0)
                {
                    Thread.Sleep(1000);
                    CloseSpinneronPage();
                    CloseSpinneronDiv();
                    Thread.Sleep(1000);
                    WaitForElement(InsuranceGroup[0]);
                    WaitForElementToClick(InsuranceGroup[0]);
                    InsuranceGroup[0].Click();
                    CloseSpinneronDiv();
                    Thread.Sleep(1000);
                    WaitForElements(InsuranceGroupInputs);
                    var insuranceGroup = InsuranceGroupInputs.FirstOrDefault(o => o.Text.Contains("Aviva"));
                    if (insuranceGroup.Displayed)
                    {
                        insuranceGroup.Click();
                    }
                    CloseSpinneronDiv();
                    Thread.Sleep(1000);
                }

            }
            SelectProductVersionDate();
            CloseSpinneronDiv();
            CloseSpinneronPage();
            CloseCrispCard();
            Thread.Sleep(1000);
            WaitForElement(AddQuoteAppDocs);
            AddQuoteAppDocs.Click();
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(2000);
            WaitForElement(UploadDocWizard);
            Assert.IsTrue(UploadDocWizard.Displayed);
            var fileInfo = new FileInfo(ExtensionMethods.GetAbsolutePath(@"TestData\QuotesData\CRISP install guide.docx"));
            UploadDoc.SendKeys(fileInfo.ToString());
            //Thread.Sleep(1000);
            CloseSpinneronDiv();
            WaitForElement(UploadOkButton);
            //Thread.Sleep(500);
            UploadOkButton.Click();
            //Thread.Sleep(1000);
            WaitForElement(NextButton);
            NextButton.Click();
            CloseSpinneronDiv();
            CloseSpinneronPage();
        }
        //Other Site Details 
        public void OtherSiteDetailsPage()
        {
            WaitForElement(AddLABCQuotePage.StepTitle);
            Assert.IsTrue(AddLABCQuotePage.StepTitle.Text.Contains("OTHER SITE DETAILS"), $"Failed to Display {StepTitle.Text}");
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(1000);
            WaitForElement(ConstructionStartDateLabel);
            ConstructionStartDateLabel.Click();
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(1000);
            WaitForElement(ConstructionStartDateInput);
            ConstructionStartDateInput.Click();
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(1000);
            WaitForElement(ConstructionEndDateLabel);
            ConstructionEndDateLabel.Click();
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(1000);
            WaitForElement(ConstructionEndDateInput);
            ConstructionEndDateInput.Click();
            Premiums.StoreyLoadingInput = 2;
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(1000);
            WaitForElement(NumberOffStoreysAboveGround);
            NumberOffStoreysAboveGround.Click();
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(1000);
            WaitForElement(NumberOffStoreysAboveGroundInput);
            NumberOffStoreysAboveGroundInput.SendKeys($"{Premiums.StoreyLoadingInput}");
            Premiums.StoreyLoading = Premiums.StoreyLoadingInput >= 15 ? "Yes" : "No";
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(1000);
            Premiums.BasementLoadingInput = 2;
            WaitForElement(NumberOffStoreysBelowGround);
            NumberOffStoreysBelowGround.Click();
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(1000);
            WaitForElement(NumberOffStoreysBelowGroundInput);
            NumberOffStoreysBelowGroundInput.SendKeys($"{Premiums.BasementLoadingInput}");
            Premiums.BasementLoading = Premiums.BasementLoadingInput >=0 ? "Yes" : "No";
            CloseSpinneronDiv();
            CloseSpinneronPage();
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(1000);
            WaitForElement(InnovativeMethodsRadioButton);
            InnovativeMethodsRadioButton.Click();
            WaitForElement(SiteAdminRadioButton);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(1000);
            SiteAdminRadioButton.Click();
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(1000);
            WaitForElement(NextButton);
            NextButton.Click();
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(1000);
            //Thread.Sleep(1000);
        }

        //LABC Create Quote - Importing Plot Schedule Data for all products
        public void PlotSchedulePage(string plotdata)
        {
            ExtensionMethods.ReadExcelPlotData(plotdata);
            WaitForElement(AddLABCQuotePage.StepTitle);
            Assert.IsTrue(AddLABCQuotePage.StepTitle.Text.Contains("PLOT SCHEDULE"));
            try
            {
                WaitForElement(ImportPlotScheduleButton);
                ImportPlotScheduleButton.Click();
                CloseSpinneronDiv();
                Thread.Sleep(2000);
                UploadDoc.SendKeys(plotdata);
               // log.Info("File has been uploaded");
                Thread.Sleep(1000);
            }
            catch (Exception e)
            {                
                throw new Exception("Failed to Upload PlotSchedule Data Due To Network Issues , please re run the test");               
            }
            //Thread.Sleep(1000);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            WaitForElement(UploadOkButton);
            UploadOkButton.Click();
            //Thread.Sleep(1000);
            WaitForElement(WarrentyProduct);
            var productName = WarrentyProduct.Text;
            Statics.ProductName = productName;
            CloseSpinneronDiv();
            //Thread.Sleep(1000);
            WaitForElement(NextButton);
            NextButton.Click();
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(1000);
        }

        //LABC Create Quote - Product Details Page
        public void ProductsDetailsPage()
        {
            Thread.Sleep(1000);
            WaitForElement(AddLABCQuotePage.StepTitle);
            Assert.IsTrue(AddLABCQuotePage.StepTitle.Text.Contains("PRODUCTS"), $"Failed to Display {StepTitle.Text}");
            CloseSpinneronDiv();
            CloseSpinneronPage();
            if (ProductVersion.Count > 0)
            {
                WaitForElements(ProductVersion);
                for (int i = 0; i < ProductVersion.Count; i++)
                {
                    WaitForElement(ProductVersion[i]);
                    ProductVersion[i].Click();
                    CloseSpinneronDiv();
                    CloseSpinneronPage();
                    WaitForElements(ChooseProductVersionList);
                    if (ChooseProductVersionList.Count > 0)
                    {
                        WaitForElements(ChooseProductVersionList);
                        ChooseProductVersionList[0].Click();
                        CloseSpinneronDiv();
                        CloseCrispCard();
                    }
                }
            }
            CloseSpinneronDiv();
            CloseSpinneronPage();
            if (CoverLengthList.Count > 0)
            {
                WaitForElements(CoverLengthList);
                foreach (var coverlenghthinput in CoverLengthList)
                {
                    if (coverlenghthinput.Selected == false)
                    {
                        coverlenghthinput.Click();
                    }
                }
            }
            CloseSpinneronDiv();
            CloseSpinneronPage();
            if (ContractsUnderSeal.Count > 0)
            {
                foreach (var eachcontractunderseal in ContractsUnderSeal)
                {
                    if (eachcontractunderseal.Selected == false)
                    {
                        eachcontractunderseal.Click();
                    }
                    CloseSpinneronDiv();
                }
            }
            CloseSpinneronDiv();
            CloseSpinneronPage();
            if (OptionalCovers.Count > 0)
            {
                WaitForElements(OptionalCovers);
                foreach (var addcover in OptionalCovers)
                {
                    //Thread.Sleep(500);
                    CloseSpinneronDiv();
                    CloseSpinneronPage();
                    //Thread.Sleep(500);
                    WaitForElement(addcover);
                    addcover.Click();
                    //Thread.Sleep(500);
                    CloseSpinneronDiv();
                    CloseSpinneronPage();
                    if (ChooseCoversList.Count > 0)
                    {
                        //Choose Core Services List
                        foreach (var eachcover in ChooseCoversList)
                        {
                            eachcover.Click();
                        }
                        //Thread.Sleep(500);
                        CloseSpinneronDiv();
                        CloseSpinneronPage();
                        if (SelectButton.Count > 0)
                        {
                            WaitForElements(SelectButton);
                            SelectButton[0].Click();
                            CloseCrispCard();
                        }
                        Thread.Sleep(500);
                        CloseSpinneronDiv();
                        CloseSpinneronPage();
                        Thread.Sleep(500);
                    }
                }
            }
            CloseSpinneronDiv();
            CloseSpinneronPage();
            if (Statics.ProductNameList.Any(x=>x.Contains("Social Housing")) || Statics.ProductNameList.Any(x => x.Contains("Social Housing - High Value")) || Statics.ProductNameList.Any(x => x.Contains("Private Rental")) || Statics.ProductNameList.Any(x => x.Contains("Private Rental - High Value")))
            {
                if (ContractCostInput.Count > 0)
                {
                    foreach (var eachcontractcost in ContractCostInput)
                    {
                        Thread.Sleep(500);
                        CloseSpinneronDiv();
                        CloseSpinneronPage();
                        Thread.Sleep(500);
                        WaitForElement(ContractCostLabel[0]);
                        ContractCostLabel[0].Click();
                        eachcontractcost.Clear();
                        eachcontractcost.SendKeys($"{contractcost}");
                        Premiums.ContractValue = Convert.ToInt32(contractcost);
                        Thread.Sleep(500);
                        CloseSpinneronDiv();
                        CloseSpinneronPage();
                        Thread.Sleep(500);
                    }
                }
            }
            CloseSpinneronDiv();
            WaitForElement(NextButton);
            NextButton.Click();
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(2000);
        }

        //LABC Create Quote - Product Details Page
        public void ProductsDetailsPageFor12Years()
        {
            Thread.Sleep(1000);
            WaitForElement(AddLABCQuotePage.StepTitle);
            Assert.IsTrue(AddLABCQuotePage.StepTitle.Text.Contains("PRODUCTS"), $"Failed to Display {StepTitle.Text}");
            CloseSpinneronDiv();
            CloseSpinneronPage();
            if (ProductVersion.Count > 0)
            {
                WaitForElements(ProductVersion);
                for (int i = 0; i < ProductVersion.Count; i++)
                {
                    WaitForElement(ProductVersion[i]);
                    ProductVersion[i].Click();
                    CloseSpinneronDiv();
                    CloseSpinneronPage();
                    WaitForElements(ChooseProductVersionList);
                    if (ChooseProductVersionList.Count > 0)
                    {
                        WaitForElements(ChooseProductVersionList);
                        ChooseProductVersionList[0].Click();
                        CloseSpinneronDiv();
                        CloseCrispCard();
                    }
                }
            }
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            if (CoverLengthList12Years.Count > 0)
            {
                WaitForElements(CoverLengthList12Years);
                foreach (var coverlenghthinput in CoverLengthList12Years)
                {
                    if (coverlenghthinput.Selected == false)
                    {
                        coverlenghthinput.Click();
                    }
                }
            }
            CloseSpinneronDiv();
            CloseSpinneronPage();          
            if (ContractsUnderSeal.Count > 0)
            {
                foreach (var eachcontractunderseal in ContractsUnderSeal)
                {
                    if (eachcontractunderseal.Selected == false)
                    {
                        eachcontractunderseal.Click();
                    }
                    CloseSpinneronDiv();
                }
            }
            CloseSpinneronDiv();
            CloseSpinneronPage();
            if (OptionalCovers.Count > 0)
            {
                WaitForElements(OptionalCovers);
                foreach (var addcover in OptionalCovers)
                {
                    //Thread.Sleep(500);
                    CloseSpinneronDiv();
                    CloseSpinneronPage();
                    //Thread.Sleep(500);
                    WaitForElement(addcover);
                    addcover.Click();
                    //Thread.Sleep(500);
                    CloseSpinneronDiv();
                    CloseSpinneronPage();
                    if (ChooseCoversList.Count > 0)
                    {
                        //Choose Core Services List
                        foreach (var eachcover in ChooseCoversList)
                        {
                            eachcover.Click();
                        }
                        //Thread.Sleep(500);
                        CloseSpinneronDiv();
                        CloseSpinneronPage();
                        if (SelectButton.Count > 0)
                        {
                            WaitForElements(SelectButton);
                            SelectButton[0].Click();
                            CloseCrispCard();
                        }
                        Thread.Sleep(500);
                        CloseSpinneronDiv();
                        CloseSpinneronPage();
                        Thread.Sleep(500);
                    }
                }
            }
            CloseSpinneronDiv();
            CloseSpinneronPage();
            if (Statics.ProductNameList.Any(x => x.Contains("Social Housing")) || Statics.ProductNameList.Any(x => x.Contains("Social Housing - High Value")) || Statics.ProductNameList.Any(x => x.Contains("Private Rental")) || Statics.ProductNameList.Any(x => x.Contains("Private Rental - High Value")))
            {
                if (ContractCostInput.Count > 0)
                {
                    foreach (var eachcontractcost in ContractCostInput)
                    {
                        Thread.Sleep(500);
                        CloseSpinneronDiv();
                        CloseSpinneronPage();
                        Thread.Sleep(500);
                        WaitForElement(ContractCostLabel[0]);
                        ContractCostLabel[0].Click();
                        eachcontractcost.Clear();
                        eachcontractcost.SendKeys($"{contractcost}");
                        Premiums.ContractValue = Convert.ToInt32(contractcost);
                        Thread.Sleep(500);
                        CloseSpinneronDiv();
                        CloseSpinneronPage();
                        Thread.Sleep(500);
                    }
                }
            }
            CloseSpinneronDiv();
            WaitForElement(NextButton);
            NextButton.Click();
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(2000);
        }
        //Additional Quoetions Page
        public void AdditionalQuestionsPage()
        {
            //Thread.Sleep(2000);
            WaitForElement(AddLABCQuotePage.StepTitle);
            Assert.IsTrue(AddLABCQuotePage.StepTitle.Text.Contains("ADDITIONAL QUESTIONS"), $"Failed to Display {StepTitle.Text}");
            if (NoOfUnitsAttachedRadioButton.Count > 0)
            {
                foreach (var eachunit in NoOfUnitsAttachedRadioButton)
                {
                    eachunit.Click();
                    CloseSpinneronDiv();
                }
            }
            //Thread.Sleep(2000);
            if (WallingandGlazingRadioButton.Count > 0)
            {
                foreach (var eachunit in WallingandGlazingRadioButton)
                {
                    eachunit.Click();
                    CloseSpinneronDiv();
                }
            }
            Premiums.HasCurtainWalling = WallingandGlazingRadioButton.Count > 0 ? "Yes" : "No";

            if (AtkinsDurationLabel.Count > 0)
            {
                WaitForElement(AtkinsDurationLabel[0]);
                AtkinsDurationLabel[0].Click();
                WaitForElement(AtkinsDurationInput);
                AtkinsDurationInput.Click();
                AtkinsDurationInput.Clear();
                AtkinsDurationInput.SendKeys("12");
            }

            //Thread.Sleep(2000);
            if (ConversionsAndRefurbishments.Count > 0)
            {
                if (DescriptionOfWorkLabel.Displayed)
                {
                    WaitForElement(DescriptionOfWorkLabel);
                    DescriptionOfWorkLabel.Click();
                    WaitForElement(DescriptionOfWorkInput);
                    DescriptionOfWorkInput.Click();
                    DescriptionOfWorkInput.Clear();
                    DescriptionOfWorkInput.SendKeys("Conversion");
                }

                if (OrginalBuildLabel.Displayed)
                {
                    WaitForElement(OrginalBuildLabel);
                    OrginalBuildLabel.Click();
                    WaitForElement(OrginalBuildInput);
                    OrginalBuildInput.Click();
                    OrginalBuildInput.Clear();
                    OrginalBuildInput.SendKeys("2016");
                }

                if (PreviousBuildLabel.Displayed)
                {
                    WaitForElement(PreviousBuildLabel);
                    PreviousBuildLabel.Click();
                    WaitForElement(PreviousBuildInput);
                    PreviousBuildInput.Click();
                }

                if (StatusOfBuildLabel.Displayed)
                {
                    WaitForElement(StatusOfBuildLabel);
                    StatusOfBuildLabel.Click();
                    WaitForElement(StatusOfBuildInput);
                    StatusOfBuildInput.Click();
                }

                if (ConversionArea.Displayed)
                {
                    WaitForElement(ConversionArea);
                    ConversionArea.Click();
                }
                if (ExistingStructureSurvey.Displayed)
                {
                    WaitForElement(ExistingStructureSurvey);
                    ExistingStructureSurvey.Click();
                }
                if (ConditionSurvey.Displayed)
                {
                    WaitForElement(ConditionSurvey);
                    ConditionSurvey.Click();
                }
                if (BuilderExperienceInConversion.Displayed)
                {
                    WaitForElement(BuilderExperienceInConversion);
                    BuilderExperienceInConversion.Click();
                }
                if (BarnConversion.Displayed)
                {
                    WaitForElement(BarnConversion);
                    BarnConversion.Click();
                }
                CloseSpinneronDiv();

            }
            //Selfbuild AdditionalQuotions
            if (SelfBuildAddtionalQuetions.Count > 0)
            {
                WaitForElements(IsArchitectOpen);
                if(IsArchitectOpen.Count>0)
                {
                    List<string> Architect = IsArchitectOpen.Select(i => i.Text).ToList();
                    if (Architect.Contains("Yes"))
                    {
                        IsArchitectOpen[0].Click();
                        WaitForElement(ArchitectLevelDropdown);
                        ArchitectLevelDropdown.Click();
                        CloseSpinneronDiv();
                        WaitForElements(ArchitectLevelInvolvementInput);
                        List<string> ArchitectInvolvements = ArchitectLevelInvolvementInput.Select(i => i.Text).ToList();
                        Premiums.ArchitectInvolvement = ArchitectInvolvements.Count > 0 ? "Yes" : "No";

                        //if (ArchitectInvolvements.Contains("Drawing Plans Only"))
                        //{
                        //    Premiums.ArchitectInvolvementLoading = ArchitectInvolvements.FirstOrDefault(x => x.Contains("Drawing Plans Only"));
                        //    ArchitectLevelInvolvementInput[0].Click();

                        //}
                        //if (ArchitectInvolvements.Contains("Oversee and Issue Certificates"))
                        //{
                        //    Premiums.ArchitectInvolvementLoading = ArchitectInvolvements.FirstOrDefault(x => x.Contains("Oversee and Issue Certificates"));
                        //    ArchitectLevelInvolvementInput[1].Click();

                        //}
                        if (ArchitectInvolvements.Contains("Oversee Majority of Work"))
                        {
                            Premiums.ArchitectInvolvementLoading = ArchitectInvolvements.FirstOrDefault(x => x.Contains("Oversee Majority of Work"));
                            ArchitectLevelInvolvementInput[2].Click();
                        }

                    }

                }              
                
                WaitForElement(HomeOwnerBuilt);
                HomeOwnerBuilt.Click();
                WaitForElement(SolePlaceOFResidence);
                SolePlaceOFResidence.Click();
                WaitForElement(StagePaymentLabel);
                StagePaymentLabel.Click();
                WaitForElement(StagePaymentInput);
                StagePaymentInput.Clear();
                StagePaymentInput.SendKeys("12");
                //Thread.Sleep(1000);
            }
            //Completed Houing Additional Quetions
            if (CompletedHousingAdditionalQuetions.Count > 0)
            {
                WaitForElement(StructuralWarrentyReasonLabel);
                StructuralWarrentyReasonLabel.Click();
                WaitForElement(StructuralWarrentyReasonInput);
                StructuralWarrentyReasonInput.SendKeys("There was no availability");
                //Thread.Sleep(1000);
            }
            //Commercials Conversion Addtional Questions 
            if (LossOfGrossProfit.Count > 0)
            {
                WaitForElements(LossOfGrossProfit);
                LossOfGrossProfit[0].Click();
                WaitForElement(LossOfGrossProfitInput);
                Premiums.LossOfGrossProfitInput = 20000;              
                LossOfGrossProfitInput.SendKeys($"{Premiums.LossOfGrossProfitInput}");
            }
            if (LossOfRentReceivable.Count > 0)
            {
                WaitForElements(LossOfRentReceivable);
                LossOfRentReceivable[0].Click();
                WaitForElement(LossOfRentReceivableInput);
                Premiums.LossOfRentReceivableInput = 20000;
                LossOfRentReceivableInput.SendKeys($"{Premiums.LossOfRentReceivableInput}");
            }
            if (LossOfRentPayable.Count > 0)
            {
                WaitForElements(LossOfRentPayable);
                LossOfRentPayable[0].Click();
                WaitForElement(LossOfRentPayableInput);
                Premiums.LossOfRentPayableInput = 20000;
                LossOfRentPayableInput.SendKeys($"{Premiums.LossOfRentPayableInput}");
            }
            if (LossOfGrossProfitYears.Count > 0)
            {
                WaitForElements(LossOfGrossProfitYears);
                LossOfGrossProfitYears[0].Click();
                WaitForElement(LossOfGrossProfitYearsInput);
                Premiums.LossOfGrossProfitNumberOfYearsInput = 5;
                LossOfGrossProfitYearsInput.SendKeys($"{Premiums.LossOfGrossProfitNumberOfYearsInput}");
            }
            if (LossOfRentReceivableYears.Count > 0)
            {
                WaitForElements(LossOfRentReceivableYears);
                LossOfRentReceivableYears[0].Click();
                WaitForElement(LossOfRentReceivableYearsInput);
                Premiums.LossOfRentReceivableNumberOfYearsInput = 5;
                LossOfRentReceivableYearsInput.SendKeys($"{Premiums.LossOfRentReceivableNumberOfYearsInput}");
            }
            if (LossOfRentPayableYears.Count > 0)
            {
                WaitForElements(LossOfRentPayableYears);
                LossOfRentPayableYears[0].Click();
                WaitForElement(LossOfRentPayableYearsInput);
                Premiums.LossOfRentPayableNumberOfYearsInput = 5;
                LossOfRentPayableYearsInput.SendKeys($"{Premiums.LossOfRentPayableNumberOfYearsInput}");
            }
            WaitForElement(NextButton);
            NextButton.Click();
            CloseSpinneronDiv();
        }

        //Create Quote - Declaration Details Page
        public void DeclarationDetailsPage()
        {
            CloseSpinneronDiv();
            WaitForElement(AddLABCQuotePage.StepTitle);
            Assert.IsTrue(AddLABCQuotePage.StepTitle.Text.Contains("DECLARATION"), $"Failed to Display {StepTitle.Text}");
            WaitForElements(DeclarationPageRadioButtons);
            foreach (var eachradiobutton in DeclarationPageRadioButtons)
            {
                eachradiobutton.Click();
            }
            WaitForElement(ApplicationConfirmationRadiobutton);
            ApplicationConfirmationRadiobutton.Click();
            //Thread.Sleep(1500);
        }

        //LABC Create Quote - Roles Details Page
        public void RolesDetails()
        {

            Thread.Sleep(1000);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(1000);
            WaitForElement(AddLABCQuotePage.StepTitle);
            Assert.IsTrue(AddLABCQuotePage.StepTitle.Text.Contains("ROLES"), $"Failed to Display {StepTitle.Text}");
            WaitForElement(RolesTable);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(1000);
            if (MandetoryRole.Count > 0)
            {
                WaitForElements(MandetoryRole);
                WaitForElements(Roles);
                List<string> RoleNames = Roles.Select(i => i.Text).ToList();
                if (RoleNames.Contains("Builder"))
                {
                    SetBuilderRole();
                    Thread.Sleep(500);
                    CloseCrispCard();
                    CloseSpinneronDiv();
                    CloseSpinneronPage();
                    Thread.Sleep(500);
                }
                if (RoleNames.Contains("Developer"))
                {
                    SetDeveloperRole();
                    Thread.Sleep(500);
                    CloseCrispCard();
                    CloseSpinneronDiv();
                    CloseSpinneronPage();
                    Thread.Sleep(500);

                }
                if (RoleNames.Contains("Sales Account Manager"))
                {
                    SetSalesAccountManagerRole();
                    Thread.Sleep(500);
                    CloseCrispCard();
                    CloseSpinneronDiv();
                    CloseSpinneronPage();
                    Thread.Sleep(500);
                }
                if (RoleNames.Contains("Structural Referral Administrator"))
                {
                    SetStructurralReferralAdminRole();
                    Thread.Sleep(500);
                    CloseCrispCard();
                    CloseSpinneronDiv();
                    CloseSpinneronPage();
                    Thread.Sleep(500);
                }
                if (RoleNames.Contains("Design Review Administrator"))
                {
                    SetDesignReviewAdminRole();
                    Thread.Sleep(500);
                    CloseCrispCard();
                    CloseSpinneronDiv();
                    CloseSpinneronPage();
                    Thread.Sleep(500);
                }
                if (RoleNames.Contains("Technical Administrator"))
                {
                    SetTechnicalAdministratorRole();
                    Thread.Sleep(500);
                    CloseCrispCard();
                    CloseSpinneronDiv();
                    CloseSpinneronPage();
                    Thread.Sleep(500);
                }

                if (RoleNames.Contains("Building Control Provider"))
                {
                    SetBCProviderRole();
                    Thread.Sleep(500);
                    CloseCrispCard();
                    CloseSpinneronDiv();
                    CloseSpinneronPage();
                    Thread.Sleep(500);
                }

                SetLandOwnerRole();
                Thread.Sleep(500);
                CloseCrispCard();
                CloseSpinneronDiv();
                CloseSpinneronPage();
                Thread.Sleep(500);

                if (RoleNames.Contains("Surveyor"))
                {
                    AddLABCQuotePage.SetSurveyorRole();
                    Thread.Sleep(500);
                    CloseCrispCard();
                    CloseSpinneronDiv();
                    CloseSpinneronPage();
                    Thread.Sleep(500);
                }

                if (RoleNames.Contains("Design Review Surveyor"))
                {
                    AddLABCQuotePage.SetDesignReviewSurveyorRole();
                    Thread.Sleep(500);
                    CloseCrispCard();
                    CloseSpinneronDiv();
                    CloseSpinneronPage();
                    Thread.Sleep(500);
                }

                if (Statics.ProductNameList.Any(x=>x.Contains("Social Housing")) || Statics.ProductNameList.Any(x => x.Contains("Social Housing - High Value")))
                {

                    SetSocialHousingRole();
                    Thread.Sleep(500);
                    CloseCrispCard();
                    CloseSpinneronDiv();
                    CloseSpinneronPage();
                    Thread.Sleep(500);

                }
                if(RoleNames.Contains("Hub Administrator"))
                {
                    SetHubAdministratorRole();
                    Thread.Sleep(500);
                    CloseCrispCard();
                    CloseSpinneronDiv();
                    CloseSpinneronPage();
                    Thread.Sleep(500);
                }

                if (Statics.ProductNameList.Any(x => x.Contains("Self Build")))
                {
                    if (RoleNames.Contains("Applicant (Self-build)"))
                    {
                        SetSelfBuildRole();
                        Thread.Sleep(500);
                        CloseCrispCard();
                        Thread.Sleep(500);
                        CloseSpinneronDiv();
                        CloseSpinneronPage();
                        Thread.Sleep(500);
                    }

                    if (RoleNames.Contains("Architect"))
                    {
                        SetArchitectRole();
                        Thread.Sleep(500);
                        CloseCrispCard();
                        CloseSpinneronDiv();
                        CloseSpinneronPage();
                        Thread.Sleep(500);
                    }

               }

                if (Statics.ProductNameList.Any(x => x.Contains("Private Rental")) || Statics.ProductNameList.Any(x => x.Contains("Private Rental - High Value")))
                {

                    SetPRSCompanyRole();
                    Thread.Sleep(500);
                    CloseCrispCard();
                    CloseSpinneronDiv();
                    CloseSpinneronPage();
                    Thread.Sleep(500);

                }
            }
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            WaitForElement(NextButton);
            WaitForElementToClick(NextButton);
            NextButton.Click();
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(2000);
        }

        // LABC Create Quote -   Roles Details For Social Housing Product
        public void SetSocialHousingRole()
        {
            Thread.Sleep(1000);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(1000);
            WaitForElement(EditRoleButton);
            EditRoleButton.Click();
            //Thread.Sleep(1000);
            //Thread.Sleep(1000);
            WaitForElement(SearchRoleLabel);
            SearchRoleLabel.Click();
            SearchRoleLabel.Clear();
            SearchRoleLabel.SendKeys("Housing Association");
            Thread.Sleep(1000);
            CloseSpinneronDiv();
            if (HousingAssociationRole.Count > 0)
            {
                WaitForElements(HousingAssociationRole);
                //Thread.Sleep(1000);
                HousingAssociationRole[0].Click();
                Thread.Sleep(500);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                Thread.Sleep(500);
                if (SelectButton.Count > 0)
                {
                    WaitForElements(SelectButton);
                    SelectButton[0].Click();
                    Thread.Sleep(500);
                    CloseSpinneronDiv();
                    CloseSpinneronPage();
                    Thread.Sleep(500);
                    if (ChooseBuilder.Count > 0)
                    {
                        WaitForElement(SearchLabel);
                        SearchLabel.Click();
                        WaitForElement(ChooseBuilder[0]);
                        CloseSpinneronDiv();
                        //Thread.Sleep(1000);
                        ChooseBuilder[0].Click();
                        //Thread.Sleep(1500);
                        CloseSpinneronDiv();
                        //Thread.Sleep(1000);
                        //if (Statics.RoleName != null)
                        //{
                        //    //Thread.Sleep(1000);
                        //    WaitForElement(SearchInput);
                        //    SearchInput.Clear();
                        //    SearchInput.SendKeys(Statics.RoleName);
                        //    Thread.Sleep(1000);
                        //    CloseSpinneronDiv();
                        //    CloseSpinneronPage();
                        //    Thread.Sleep(1000);
                        //    if (ChooseBuilder.Count > 0)
                        //    {
                        //        WaitForElement(ChooseBuilder[0]);
                        //        CloseSpinneronDiv();
                        //        //Thread.Sleep(1000);
                        //        ChooseBuilder[0].Click();
                        //        //Thread.Sleep(1500);
                        //        CloseSpinneronDiv();
                        //        //Thread.Sleep(1000);
                        //    }
                        //    else
                        //    {
                        //        CloseSpinneronDiv();
                        //        WaitForElement(AddCompanyButton);
                        //        WaitForElementToClick(AddCompanyButton);
                        //        AddCompanyButton.Click();
                        //        AdditionalMethodsPage.AddCompany();
                        //    }
                        //}
                        //else
                        //{
                        //    CloseSpinneronDiv();
                        //    WaitForElement(AddCompanyButton);
                        //    WaitForElementToClick(AddCompanyButton);
                        //    AddCompanyButton.Click();
                        //    AdditionalMethodsPage.AddCompany();                  

                    }
                    else
                    {
                        CloseSpinneronDiv();
                        WaitForElement(AddCompanyButton);
                        WaitForElementToClick(AddCompanyButton);
                        AddCompanyButton.Click();
                        AdditionalMethodsPage.AddCompany();
                    }
                }
            }

            else
            {
                WaitForElement(CancelButton);
                CancelButton.Click();
                //Thread.Sleep(1000);
                CloseSpinneronDiv();
                CloseSpinneronPage();
            }
        }
        // LABC Create Quote -   Roles Details For SelfBuild Product
        public void SetSelfBuildRole()
        {
            Thread.Sleep(1000);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(1000);
            WaitForElement(EditRoleButton);
            EditRoleButton.Click();
            //Thread.Sleep(2000);
            WaitForElement(SearchRoleLabel);
            SearchRoleLabel.Click();
            SearchRoleLabel.Clear();
            SearchRoleLabel.SendKeys("Applicant (Self-build)");
            Thread.Sleep(1000);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(1000);
            if (SelfBuildApplicantRole.Count > 0)
            {
                WaitForElements(SelfBuildApplicantRole);
                WaitForElementToClick(SelfBuildApplicantRole[0]);
                SelfBuildApplicantRole[0].Click();
                Thread.Sleep(1000);
                CloseSpinneronDiv();
                Thread.Sleep(1000);
                if (SelectButton.Count > 0)
                {
                    //Thread.Sleep(1500);
                    CloseSpinneronDiv();
                    //Thread.Sleep(1000);
                    WaitForElements(SelectButton);
                    SelectButton[0].Click();
                    //Thread.Sleep(1500);
                    CloseSpinneronDiv();
                    //Thread.Sleep(1000);
                    WaitForElements(CompanyList);
                    //Thread.Sleep(1500);
                    CloseSpinneronDiv();
                    //Thread.Sleep(1000);
                    if (CompanyList.Count > 0)
                    {
                        WaitForElement(CompanyList[0]);
                        WaitForElementToClick(CompanyList[0]);
                        CloseSpinneronDiv();
                        CompanyList[0].Click();
                        Thread.Sleep(1500);
                        CloseSpinneronDiv();
                        CloseSpinneronPage();
                        Thread.Sleep(1000);
                        //WaitForElement(SearchEmployeeLabel);
                        ////Thread.Sleep(500);
                        //SearchEmployeeLabel.Click();
                        ////Thread.Sleep(500);
                        //WaitForElement(SearchEmployeeInput);
                        //SearchEmployeeInput.SendKeys("Ms Clare Hebden-Rawson");
                        ////Thread.Sleep(1000);
                        ////Thread.Sleep(1000);
                        CloseSpinneronDiv();
                        CloseSpinneronPage();
                        Thread.Sleep(1000);
                        if (EmpList.Count > 0)
                        {
                            //Thread.Sleep(1500);
                            CloseSpinneronDiv();
                            //Thread.Sleep(1000);                          
                            WaitForElement(EmpList[0]);
                            WaitForElementToClick(EmpList[0]);
                            EmpList[0].Click();
                            //Thread.Sleep(1500);
                            CloseSpinneronDiv();
                            //Thread.Sleep(1000);
                        }                       
                    }
                }
                else
                {
                    WaitForElement(CancelButton);
                    CancelButton.Click();
                    //Thread.Sleep(1000);
                    CloseSpinneronDiv();
                    CloseSpinneronPage();
                }
            }
            else

            {
                WaitForElement(CancelButton);
                CancelButton.Click();
                //Thread.Sleep(1000);
                CloseSpinneronDiv();
                CloseSpinneronPage();
            }
        }
        //Set Architect Role 
        public void SetArchitectRole()
        {
            Thread.Sleep(1000);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(1000);
            WaitForElement(EditRoleButton);
            WaitForLoadElementtobeclickable(EditRoleButton);
            EditRoleButton.Click();
            Thread.Sleep(1000);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(1000);
            WaitForElement(SearchRoleLabel);
            SearchRoleLabel.Click();
            SearchRoleLabel.Clear();
            SearchRoleLabel.SendKeys("Architect");
            Thread.Sleep(1000);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(1000);
            if (ArchitectRole.Count > 0)
            {
                WaitForElements(ArchitectRole);
                WaitForElementToClick(ArchitectRole[0]);
                WaitForLoadElementtobeclickable(ArchitectRole[0]);
                ArchitectRole[0].Click();
                Thread.Sleep(1000);
                CloseSpinneronDiv();
                Thread.Sleep(1000);
                if (SelectButton.Count > 0)
                {
                    //Thread.Sleep(1500);
                    CloseSpinneronDiv();
                    //Thread.Sleep(1000);
                    WaitForElements(SelectButton);
                    SelectButton[0].Click();
                    //Thread.Sleep(1500);
                    CloseSpinneronDiv();
                    //Thread.Sleep(1000);
                    WaitForElements(CompanyList);
                    //Thread.Sleep(1500);
                    CloseSpinneronDiv();
                    //Thread.Sleep(1000);
                    if (CompanyList.Count > 0)
                    {
                        WaitForElement(CompanyList[0]);
                        WaitForElementToClick(CompanyList[0]);
                        CloseSpinneronDiv();
                        CompanyList[0].Click();
                        Thread.Sleep(1500);
                        CloseSpinneronDiv();
                        CloseSpinneronPage();
                        Thread.Sleep(1000);
                       // WaitForElement(SearchEmployeeLabel);
                        ////Thread.Sleep(500);
                        //SearchEmployeeLabel.Click();
                        ////Thread.Sleep(500);
                        //WaitForElement(SearchEmployeeInput);
                        //SearchEmployeeInput.SendKeys("Ms Clare Hebden-Rawson");
                        ////Thread.Sleep(1000);
                        ////Thread.Sleep(1000);
                        CloseSpinneronDiv();
                        CloseSpinneronPage();
                        Thread.Sleep(1000);
                        if (EmpList.Count > 0)
                        {
                            //Thread.Sleep(1500);
                            CloseSpinneronDiv();
                            //Thread.Sleep(1000);                          
                            WaitForElement(EmpList[0]);
                            WaitForElementToClick(EmpList[0]);
                            EmpList[0].Click();
                            //Thread.Sleep(1500);
                            CloseSpinneronDiv();
                            //Thread.Sleep(1000);
                        }                       
                    }
                }
                else
                {
                    WaitForElement(CancelButton);
                    CancelButton.Click();
                    //Thread.Sleep(1000);
                    CloseSpinneronDiv();
                    CloseSpinneronPage();
                }
            }
            else

            {
                WaitForElement(CancelButton);
                CancelButton.Click();
                //Thread.Sleep(1000);
                CloseSpinneronDiv();
                CloseSpinneronPage();
            }
        }

        // LABC Create Quote -   Roles Details For PrivateRental
        public void SetPRSCompanyRole()
        {
            Thread.Sleep(1000);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(1000);
            WaitForElement(EditRoleButton);
            EditRoleButton.Click();
            //Thread.Sleep(2000);
            WaitForElement(SearchRoleLabel);
            SearchRoleLabel.Click();
            SearchRoleLabel.Clear();
            SearchRoleLabel.SendKeys("PRS Company");
            Thread.Sleep(500);
            CloseSpinneronDiv();
            //Thread.Sleep(1000);
            if (PRSCompanyRole.Count > 0)
            {
                WaitForElements(PRSCompanyRole);
                PRSCompanyRole[0].Click();

                CloseSpinneronDiv();
                //Thread.Sleep(1000);
                if (SelectButton.Count > 0)
                {
                    WaitForElements(SelectButton);
                    SelectButton[0].Click();
                    Thread.Sleep(500);
                    CloseSpinneronDiv();
                    CloseSpinneronPage();
                    //Thread.Sleep(1000);
                    if (ChooseBuilder.Count > 0)
                    {
                        WaitForElement(SearchLabel);
                        SearchLabel.Click();
                        WaitForElement(ChooseBuilder[0]);
                        CloseSpinneronDiv();
                        //Thread.Sleep(1000);
                        ChooseBuilder[0].Click();
                        //Thread.Sleep(1500);
                        CloseSpinneronDiv();
                        //Thread.Sleep(1000);
                        //if (Statics.RoleName != null)
                        //{
                        //    //Thread.Sleep(1000);
                        //    WaitForElement(SearchInput);
                        //    SearchInput.Clear();
                        //    SearchInput.SendKeys(Statics.RoleName);
                        //    Thread.Sleep(1000);
                        //    CloseSpinneronDiv();
                        //    CloseSpinneronPage();
                        //    Thread.Sleep(1000);
                        //    if (ChooseBuilder.Count > 0)
                        //    {
                        //        WaitForElement(ChooseBuilder[0]);
                        //        CloseSpinneronDiv();
                        //        //Thread.Sleep(1000);
                        //        ChooseBuilder[0].Click();
                        //        //Thread.Sleep(1500);
                        //        CloseSpinneronDiv();
                        //        //Thread.Sleep(1000);
                        //    }
                        //    else
                        //    {
                        //        CloseSpinneronDiv();
                        //        WaitForElement(AddCompanyButton);
                        //        WaitForElementToClick(AddCompanyButton);
                        //        AddCompanyButton.Click();
                        //        AdditionalMethodsPage.AddCompany();
                        //    }
                        //}
                        //else
                        //{
                        //    CloseSpinneronDiv();
                        //    WaitForElement(AddCompanyButton);
                        //    WaitForElementToClick(AddCompanyButton);
                        //    AddCompanyButton.Click();
                        //    AdditionalMethodsPage.AddCompany();                  

                    }
                    else
                    {
                        CloseSpinneronDiv();
                        WaitForElement(AddCompanyButton);
                        WaitForElementToClick(AddCompanyButton);
                        AddCompanyButton.Click();
                        AdditionalMethodsPage.AddCompany();
                    }
                }
            }
            //if (ChooseBuilder.Count > 0)
            //        {
            //            WaitForElement(SearchLabel);
            //            SearchLabel.Click();
            //            if (Statics.RoleName != null)
            //            {
            //                //Thread.Sleep(1000);
            //                WaitForElement(SearchInput);
            //                SearchInput.Clear();
            //                SearchInput.SendKeys(Statics.RoleName);
            //                Thread.Sleep(1000);
            //                CloseSpinneronDiv();
            //                CloseSpinneronPage();
            //                Thread.Sleep(1000);
            //                if (ChooseBuilder.Count > 0)
            //                {
            //                    WaitForElement(ChooseBuilder[0]);
            //                    CloseSpinneronDiv();
            //                    //Thread.Sleep(1000);
            //                    ChooseBuilder[0].Click();
            //                    //Thread.Sleep(1500);
            //                    CloseSpinneronDiv();
            //                    //Thread.Sleep(1000);
            //                }
            //                else
            //                {
            //                    WaitForElement(AddCompanyButton);
            //                    WaitForElementToClick(AddCompanyButton);
            //                    AddCompanyButton.Click();
            //                    AdditionalMethodsPage.AddCompany();
            //                }
            //            }
            //            else
            //            {
            //                WaitForElement(AddCompanyButton);
            //                WaitForElementToClick(AddCompanyButton);
            //                AddCompanyButton.Click();
            //                AdditionalMethodsPage.AddCompany();
            //            }
            //        }
            //        else
            //        {
            //            WaitForElement(AddCompanyButton);
            //            WaitForElementToClick(AddCompanyButton);
            //            AddCompanyButton.Click();
            //            AdditionalMethodsPage.AddCompany();
            //        }
            //    }
            //}
            else
            {
                WaitForElement(CancelButton);
                CancelButton.Click();
                //Thread.Sleep(1000);
                CloseSpinneronDiv();
                CloseSpinneronPage();
            }
        }
        //set Builder Role 
        public void SetBuilderRole()
        {
            Thread.Sleep(1000);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(1000);
            WaitForElement(EditRoleButton);
            EditRoleButton.Click();
            //Thread.Sleep(1500);
            WaitForElement(SearchRoleLabel);
            SearchRoleLabel.Click();
            SearchRoleLabel.Clear();
            SearchRoleLabel.SendKeys("Builder");
            Thread.Sleep(1000);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(1000);
            WaitForElements(BuilderRole);
            if (BuilderRole.Count > 0)
            {
                WaitForElement(BuilderRole[0]);
                WaitForElementToClick(BuilderRole[0]);
                CloseSpinneronDiv();
                BuilderRole[0].Click();
                //Thread.Sleep(1500);
                CloseSpinneronDiv();
                //Thread.Sleep(1000);
                if (SelectButton.Count > 0)
                {
                    WaitForElements(SelectButton);
                    SelectButton[0].Click();
                    Thread.Sleep(1000);
                    CloseSpinneronDiv();
                    CloseSpinneronPage();
                    Thread.Sleep(1000);
                    if (ChooseBuilder.Count > 0)
                    {
                        WaitForElement(SearchLabel);
                        SearchLabel.Click();
                        Thread.Sleep(1000);
                        CloseSpinneronDiv();
                        CloseSpinneronPage();
                        Thread.Sleep(1000);
                        WaitForElement(ChooseBuilder[0]);
                        CloseSpinneronDiv();
                        //Thread.Sleep(1000);
                        ChooseBuilder[0].Click();
                        //Thread.Sleep(1500);
                        CloseSpinneronDiv();
                        //Thread.Sleep(1000);
                        //if (Statics.RoleName != null)
                        //{
                        //    //Thread.Sleep(1000);
                        //    WaitForElement(SearchInput);
                        //    SearchInput.Clear();
                        //    SearchInput.SendKeys(Statics.RoleName);
                        //    Thread.Sleep(1000);
                        //    CloseSpinneronDiv();
                        //    CloseSpinneronPage();
                        //    Thread.Sleep(1000);
                        //    if (ChooseBuilder.Count > 0)
                        //    {
                        //        WaitForElement(ChooseBuilder[0]);
                        //        CloseSpinneronDiv();
                        //        //Thread.Sleep(1000);
                        //        ChooseBuilder[0].Click();
                        //        //Thread.Sleep(1500);
                        //        CloseSpinneronDiv();
                        //        //Thread.Sleep(1000);
                        //    }
                        //    else
                        //    {
                        //        CloseSpinneronDiv();
                        //        WaitForElement(AddCompanyButton);
                        //        WaitForElementToClick(AddCompanyButton);
                        //        AddCompanyButton.Click();
                        //        AdditionalMethodsPage.AddCompany();
                        //    }
                    //}
                    //else
                    //{
                    //    CloseSpinneronDiv();
                    //    WaitForElement(AddCompanyButton);
                    //    WaitForElementToClick(AddCompanyButton);
                    //    AddCompanyButton.Click();
                    //    AdditionalMethodsPage.AddCompany();                  
                
                    }
                    else
                    {
                        CloseSpinneronDiv();
                        WaitForElement(AddCompanyButton);
                        WaitForElementToClick(AddCompanyButton);
                        AddCompanyButton.Click();
                        AdditionalMethodsPage.AddCompany();
                    }
                }
            }
            else
            {
                WaitForElement(CancelButton);
                CancelButton.Click();
                //Thread.Sleep(1000);
                CloseSpinneronDiv();
                CloseSpinneronPage();
            }
        }
        //Set Developer Role
        public void SetDeveloperRole()
        {
            Thread.Sleep(1000);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(1000);
            WaitForElement(EditRoleButton);
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            WaitForElementToClick(EditRoleButton);
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            EditRoleButton.Click();
            //Thread.Sleep(1500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(1000);
            WaitForElement(SearchRoleLabel);
            SearchRoleLabel.Click();
            SearchRoleLabel.Clear();
            SearchRoleLabel.SendKeys("Developer");
            Thread.Sleep(1000);
            CloseSpinneronDiv();
            //Thread.Sleep(1000);
            if (DeveloperRole.Count > 0)
            {
                WaitForElement(DeveloperRole[0]);
                WaitForElementToClick(DeveloperRole[0]);
                CloseSpinneronDiv();
                DeveloperRole[0].Click();
                //Thread.Sleep(1500);
                CloseSpinneronDiv();
                //Thread.Sleep(1000);
                Thread.Sleep(500);
                //Thread.Sleep(1500);
                CloseSpinneronDiv();
                //Thread.Sleep(1000);
                if (SelectButton.Count > 0)
                {
                    WaitForElements(SelectButton);
                    SelectButton[0].Click();
                    Thread.Sleep(1000);
                    CloseSpinneronDiv();
                    CloseSpinneronPage();
                    Thread.Sleep(1000);
                    if (ChooseBuilder.Count > 0)
                    {
                        WaitForElement(SearchLabel);
                        SearchLabel.Click();
                        WaitForElement(ChooseBuilder[0]);
                        CloseSpinneronDiv();
                        //Thread.Sleep(1000);
                        WaitForElementToClick(ChooseBuilder[0]);
                        ChooseBuilder[0].Click();
                        //Thread.Sleep(1500);
                        CloseSpinneronDiv();
                        //Thread.Sleep(1000);
                        //if (Statics.RoleName != null)
                        //{
                        //    //Thread.Sleep(1000);
                        //    WaitForElement(SearchInput);
                        //    SearchInput.Clear();
                        //    SearchInput.SendKeys(Statics.RoleName);
                        //    Thread.Sleep(1000);
                        //    CloseSpinneronDiv();
                        //    CloseSpinneronPage();
                        //    Thread.Sleep(1000);
                        //    if (ChooseBuilder.Count > 0)
                        //    {
                        //        WaitForElement(ChooseBuilder[0]);
                        //        CloseSpinneronDiv();
                        //        //Thread.Sleep(1000);
                        //        ChooseBuilder[0].Click();
                        //        //Thread.Sleep(1500);
                        //        CloseSpinneronDiv();
                        //        //Thread.Sleep(1000);
                        //    }
                        //    else
                        //    {
                        //        CloseSpinneronDiv();
                        //        WaitForElement(AddCompanyButton);
                        //        WaitForElementToClick(AddCompanyButton);
                        //        AddCompanyButton.Click();
                        //        AdditionalMethodsPage.AddCompany();
                        //    }
                        //}
                        //else
                        //{
                        //    CloseSpinneronDiv();
                        //    WaitForElement(AddCompanyButton);
                        //    WaitForElementToClick(AddCompanyButton);
                        //    AddCompanyButton.Click();
                        //    AdditionalMethodsPage.AddCompany();                  

                    }
                    else
                    {
                        CloseSpinneronDiv();
                        WaitForElement(AddCompanyButton);
                        WaitForElementToClick(AddCompanyButton);
                        AddCompanyButton.Click();
                        AdditionalMethodsPage.AddCompany();
                    }
                }
            }
            else
            {
                WaitForElement(CancelButton);
                CancelButton.Click();
                //Thread.Sleep(1000);
                CloseSpinneronDiv();
                CloseSpinneronPage();
            }
        }
        //Set Sales Account Manager Role 
        public void SetSalesAccountManagerRole()
        {
            Thread.Sleep(1000);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(1000);
            WaitForElement(EditRoleButton);
            WaitForElementToClick(EditRoleButton);
            Thread.Sleep(1000);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(1000);
            EditRoleButton.Click();
            //Thread.Sleep(1000);
            WaitForElement(SearchRoleLabel);
            SearchRoleLabel.Click();
            SearchRoleLabel.Clear();
            SearchRoleLabel.SendKeys("Sales Account Manager");
            //Thread.Sleep(1500);
            CloseSpinneronDiv();
            Thread.Sleep(500);
            if (SalesAccountMaangerRole.Count > 0)
            {
                //Thread.Sleep(1500);
                CloseSpinneronDiv();
                //Thread.Sleep(1000);
                WaitForElements(SalesAccountMaangerRole);
                WaitForElementToClick(SalesAccountMaangerRole[0]);
                SalesAccountMaangerRole[0].Click();
                Thread.Sleep(500);
                CloseSpinneronDiv();
                //Thread.Sleep(1000);
                if (SelectButton.Count > 0)
                {
                    //Thread.Sleep(1500);
                    CloseSpinneronDiv();
                    //Thread.Sleep(1000);
                    WaitForElements(SelectButton);
                    SelectButton[0].Click();
                    //Thread.Sleep(1500);
                    CloseSpinneronDiv();
                    //Thread.Sleep(1000);
                    WaitForElements(CompanyList);
                    //Thread.Sleep(1500);
                    CloseSpinneronDiv();
                    //Thread.Sleep(1000);
                    if (CompanyList.Count > 0)
                    {
                        WaitForElement(CompanyList[0]);
                        WaitForElementToClick(CompanyList[0]);
                        CloseSpinneronDiv();
                        CompanyList[0].Click();
                        Thread.Sleep(1500);
                        CloseSpinneronDiv();
                        CloseSpinneronPage();
                        Thread.Sleep(1000);
                       // WaitForElement(SearchEmployeeLabel);
                        ////Thread.Sleep(500);
                        //SearchEmployeeLabel.Click();
                        ////Thread.Sleep(500);
                        //WaitForElement(SearchEmployeeInput);
                        //SearchEmployeeInput.SendKeys("Ms Clare Hebden-Rawson");
                        ////Thread.Sleep(1000);
                        ////Thread.Sleep(1000);
                        CloseSpinneronDiv();
                        CloseSpinneronPage();
                        Thread.Sleep(1000);
                        if (EmpList.Count > 0)
                        {
                            //Thread.Sleep(1500);
                            CloseSpinneronDiv();
                            CloseSpinneronPage();
                            //Thread.Sleep(1000);                          
                            WaitForElement(EmpList[0]);
                            WaitForElementToClick(EmpList[0]);
                            EmpList[0].Click();
                            //Thread.Sleep(1500);
                            CloseSpinneronDiv();
                            CloseCrispCard();
                            //Thread.Sleep(1000);
                        }
                        else
                        {
                            Thread.Sleep(1500);
                            CloseSpinneronDiv();
                            CloseSpinneronPage();
                            Thread.Sleep(1000);
                            WaitForElement(EmpList[0]);
                            WaitForElementToClick(EmpList[0]);
                            EmpList[0].Click();
                            //Thread.Sleep(1500);
                            CloseSpinneronDiv();
                            //Thread.Sleep(1000);

                        }
                    }
                }
                else
                {
                    WaitForElement(CancelButton);
                    CancelButton.Click();
                    //Thread.Sleep(1000);
                    CloseSpinneronDiv();
                    CloseSpinneronPage();
                }
            }
            else

            {
                WaitForElement(CancelButton);
                CancelButton.Click();
                //Thread.Sleep(1000);
                CloseSpinneronDiv();
                CloseSpinneronPage();
            }
            Thread.Sleep(1000);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            CloseCrispCard();
            Thread.Sleep(1000);

        }
        //Set Sales Account Manager Role 
        public void SetHubAdministratorRole()
        {
            Thread.Sleep(1000);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(1000);
            WaitForElement(EditRoleButton);
            EditRoleButton.Click();
            //Thread.Sleep(1000);
            WaitForElement(SearchRoleLabel);
            SearchRoleLabel.Click();
            SearchRoleLabel.Clear();
            SearchRoleLabel.SendKeys("Hub Administrator");
            //Thread.Sleep(1500);
            CloseSpinneronDiv();
            Thread.Sleep(500);
            if (HubAdministratorRole.Count > 0)
            {
                //Thread.Sleep(1500);
                CloseSpinneronDiv();
                //Thread.Sleep(1000);
                WaitForElements(HubAdministratorRole);
                WaitForElementToClick(HubAdministratorRole[0]);
                HubAdministratorRole[0].Click();
                Thread.Sleep(500);
                CloseSpinneronDiv();
                //Thread.Sleep(1000);
                if (SelectButton.Count > 0)
                {
                    //Thread.Sleep(1500);
                    CloseSpinneronDiv();
                    //Thread.Sleep(1000);
                    WaitForElements(SelectButton);
                    SelectButton[0].Click();
                    //Thread.Sleep(1500);
                    CloseSpinneronDiv();
                    //Thread.Sleep(1000);
                    WaitForElements(CompanyList);
                    //Thread.Sleep(1500);
                    CloseSpinneronDiv();
                    //Thread.Sleep(1000);
                    if (CompanyList.Count > 0)
                    {
                        WaitForElement(CompanyList[0]);
                        WaitForElementToClick(CompanyList[0]);
                        CloseSpinneronDiv();
                        CompanyList[0].Click();
                        Thread.Sleep(1500);
                        CloseSpinneronDiv();
                        CloseSpinneronPage();
                        Thread.Sleep(1000);
                        WaitForElement(SearchEmployeeLabel);
                        ////Thread.Sleep(500);
                        //SearchEmployeeLabel.Click();
                        ////Thread.Sleep(500);
                        //WaitForElement(SearchEmployeeInput);
                        //SearchEmployeeInput.SendKeys("Ms Clare Hebden-Rawson");
                        ////Thread.Sleep(1000);
                        ////Thread.Sleep(1000);
                        CloseSpinneronDiv();
                        CloseSpinneronPage();
                        Thread.Sleep(1000);
                        if (EmpList.Count > 0)
                        {
                            //Thread.Sleep(1500);
                            CloseSpinneronDiv();
                            //Thread.Sleep(1000);                          
                            WaitForElement(EmpList[0]);
                            WaitForElementToClick(EmpList[0]);
                            EmpList[0].Click();
                            Thread.Sleep(1500);
                            CloseSpinneronDiv();
                            Thread.Sleep(1000);
                        }
                        else
                        {
                            Thread.Sleep(1500);
                            CloseSpinneronDiv();
                            CloseSpinneronPage();
                            Thread.Sleep(1000);
                            WaitForElement(EmpList[0]);
                            WaitForElementToClick(EmpList[0]);
                            EmpList[0].Click();
                            //Thread.Sleep(1500);
                            CloseSpinneronDiv();
                            //Thread.Sleep(1000);

                        }
                    }
                }
                else
                {
                    WaitForElement(CancelButton);
                    CancelButton.Click();
                    //Thread.Sleep(1000);
                    CloseSpinneronDiv();
                    CloseSpinneronPage();
                }
            }
            else

            {
                WaitForElement(CancelButton);
                CancelButton.Click();
                //Thread.Sleep(1000);
                CloseSpinneronDiv();
                CloseSpinneronPage();
            }
        }


        //Set Structurral Referral Administrator Role 
        public void SetStructurralReferralAdminRole()
        {

            Thread.Sleep(1000);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(1000);
            WaitForElement(EditRoleButton);
            EditRoleButton.Click();
            //Thread.Sleep(1000);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(1000);
            WaitForElement(SearchRoleLabel);
            SearchRoleLabel.Click();
            SearchRoleLabel.Clear();
            SearchRoleLabel.SendKeys("Structural Referral Administrator");
            //Thread.Sleep(1000);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(1000);
            if (StructuralRefAdminRole.Count > 0)
            {
                //Thread.Sleep(1000);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                //Thread.Sleep(1000);
                WaitForElements(StructuralRefAdminRole);
                //Thread.Sleep(1000);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                //Thread.Sleep(1000);
                StructuralRefAdminRole[0].Click();
                //Thread.Sleep(1000);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                //Thread.Sleep(1000);
                if (SelectButton.Count > 0)
                {
                    //Thread.Sleep(1000);
                    CloseSpinneronDiv();
                    CloseSpinneronPage();
                    //Thread.Sleep(1000);
                    WaitForElements(SelectButton);
                    SelectButton[0].Click();
                    //Thread.Sleep(1000);
                    CloseSpinneronDiv();
                    CloseSpinneronPage();
                    //Thread.Sleep(1000);
                    WaitForElements(CompanyList);
                    CloseSpinneronDiv();
                    //Thread.Sleep(1000);
                    CloseSpinneronDiv();
                    CloseSpinneronPage();
                    //Thread.Sleep(1000);
                    if (CompanyList.Count > 0)
                    {
                        //Thread.Sleep(1000);
                        CloseSpinneronDiv();
                        CloseSpinneronPage();
                        //Thread.Sleep(1000);
                        WaitForElement(CompanyList[0]);
                        CloseSpinneronDiv();
                        CompanyList[0].Click();
                        Thread.Sleep(1000);
                        CloseSpinneronDiv();
                        CloseSpinneronPage();
                        Thread.Sleep(1000);
                        //WaitForElement(SearchEmployeeLabel);
                        //Thread.Sleep(500);
                        //SearchEmployeeLabel.Click();
                        //Thread.Sleep(1000);
                        //CloseSpinneronDiv();
                        //CloseSpinneronPage();
                        //Thread.Sleep(1000);
                        //WaitForElement(SearchEmployeeInput);
                        //SearchEmployeeInput.SendKeys("Ms Clare Hebden-Rawson");
                        //Thread.Sleep(1000);
                        CloseSpinneronDiv();
                        CloseSpinneronPage();
                        //Thread.Sleep(1000);
                        if (EmpList.Count > 0)
                        {
                            //Thread.Sleep(1500);
                            CloseSpinneronDiv();
                            //Thread.Sleep(1000);                          
                            WaitForElement(EmpList[0]);
                            WaitForElementToClick(EmpList[0]);
                            EmpList[0].Click();
                            //Thread.Sleep(1500);
                            CloseSpinneronDiv();
                            //Thread.Sleep(1000);
                        }
                        else
                        {
                            Thread.Sleep(1500);
                            CloseSpinneronDiv();
                            CloseSpinneronPage();
                            Thread.Sleep(1000);
                            WaitForElement(EmpList[0]);
                            WaitForElementToClick(EmpList[0]);
                            EmpList[0].Click();
                            //Thread.Sleep(1500);
                            CloseSpinneronDiv();
                            //Thread.Sleep(1000);

                        }
                    }
                }
                else

                {
                    WaitForElement(CancelButton);
                    CancelButton.Click();
                    //Thread.Sleep(1000);
                    CloseSpinneronDiv();
                    CloseSpinneronPage();
                }
            }
        }
        //Set Sales Account Manager Role 
        public void SetTechnicalAdministratorRole()
        {
            Thread.Sleep(1000);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(1000);
            WaitForElement(EditRoleButton);
            EditRoleButton.Click();
            //Thread.Sleep(1000);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(1000);
            WaitForElement(SearchRoleLabel);
            SearchRoleLabel.Click();
            SearchRoleLabel.Clear();
            SearchRoleLabel.SendKeys("Technical Administrator");
            //Thread.Sleep(1000);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(1000);
            if (TechAdminRole.Count > 0)
            {
                //Thread.Sleep(1000);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                //Thread.Sleep(1000);
                WaitForElements(TechAdminRole);
                TechAdminRole[0].Click();
                //Thread.Sleep(1000);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                //Thread.Sleep(1000);
                if (SelectButton.Count > 0)
                {
                    //Thread.Sleep(1000);
                    CloseSpinneronDiv();
                    CloseSpinneronPage();
                    //Thread.Sleep(1000);
                    WaitForElements(SelectButton);
                    SelectButton[0].Click();
                    //Thread.Sleep(1000);
                    CloseSpinneronDiv();
                    CloseSpinneronPage();
                    //Thread.Sleep(1000);
                    WaitForElements(CompanyList);
                    CloseSpinneronDiv();
                    //Thread.Sleep(1000);
                    CloseSpinneronDiv();
                    CloseSpinneronPage();
                    //Thread.Sleep(1000);
                    if (CompanyList.Count > 0)
                    {
                        //Thread.Sleep(1000);
                        CloseSpinneronDiv();
                        CloseSpinneronPage();
                        //Thread.Sleep(1000);
                        WaitForElement(CompanyList[0]);
                        CloseSpinneronDiv();
                        CompanyList[0].Click();
                        Thread.Sleep(1000);
                        CloseSpinneronDiv();
                        CloseSpinneronPage();
                        Thread.Sleep(1000);
                        //WaitForElement(SearchEmployeeLabel);
                        //Thread.Sleep(500);
                        //SearchEmployeeLabel.Click();
                        //Thread.Sleep(1000);
                        //CloseSpinneronDiv();
                        //CloseSpinneronPage();
                        //Thread.Sleep(1000);
                        //WaitForElement(SearchEmployeeInput);
                        //SearchEmployeeInput.SendKeys("Ms Clare Hebden-Rawson");
                        //Thread.Sleep(1000);
                        CloseSpinneronDiv();
                        CloseSpinneronPage();
                        //Thread.Sleep(1000);
                        if (EmpList.Count > 0)
                        {
                            //Thread.Sleep(1500);
                            CloseSpinneronDiv();
                            //Thread.Sleep(1000);                          
                            WaitForElement(EmpList[0]);
                            WaitForElementToClick(EmpList[0]);
                            EmpList[0].Click();
                            //Thread.Sleep(1500);
                            CloseSpinneronDiv();
                            //Thread.Sleep(1000);
                        }
                        else
                        {
                            Thread.Sleep(1500);
                            CloseSpinneronDiv();
                            CloseSpinneronPage();
                            Thread.Sleep(1000);
                            WaitForElement(EmpList[0]);
                            WaitForElementToClick(EmpList[0]);
                            EmpList[0].Click();
                            //Thread.Sleep(1500);
                            CloseSpinneronDiv();
                            //Thread.Sleep(1000);

                        }
                    }
                }
            }
            else

            {
                WaitForElement(CancelButton);
                CancelButton.Click();
                //Thread.Sleep(1000);
                CloseSpinneronDiv();
                CloseSpinneronPage();
            }
        }

        //set design review admin role 
        public void SetDesignReviewAdminRole()
        {
            Thread.Sleep(1000);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(1000);
            WaitForElement(EditRoleButton);
            EditRoleButton.Click();
            //Thread.Sleep(1000);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(1000);
            WaitForElement(SearchRoleLabel);
            SearchRoleLabel.Click();
            SearchRoleLabel.Clear();
            SearchRoleLabel.SendKeys("Design Review Administrator");
            //Thread.Sleep(1000);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(1000);
            if (DesignReviewAdminRole.Count > 0)
            {
                //Thread.Sleep(1000);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                //Thread.Sleep(1000);
                WaitForElements(DesignReviewAdminRole);
                DesignReviewAdminRole[0].Click();
                //Thread.Sleep(1000);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                //Thread.Sleep(1000);
                if (SelectButton.Count > 0)
                {
                    //Thread.Sleep(1000);
                    CloseSpinneronDiv();
                    CloseSpinneronPage();
                    //Thread.Sleep(1000);
                    WaitForElements(SelectButton);
                    SelectButton[0].Click();
                    //Thread.Sleep(1000);
                    CloseSpinneronDiv();
                    CloseSpinneronPage();
                    //Thread.Sleep(1000);
                    WaitForElements(CompanyList);
                    //Thread.Sleep(1000);
                    CloseSpinneronDiv();
                    CloseSpinneronPage();
                    //Thread.Sleep(1000);
                    if (CompanyList.Count > 0)
                    {
                        //Thread.Sleep(1000);
                        CloseSpinneronDiv();
                        CloseSpinneronPage();
                        //Thread.Sleep(1000);
                        WaitForElement(CompanyList[0]);
                        CloseSpinneronDiv();
                        CompanyList[0].Click();
                        Thread.Sleep(1000);
                        CloseSpinneronDiv();
                        CloseSpinneronPage();
                        Thread.Sleep(1000);
                        //WaitForElement(SearchEmployeeLabel);
                        //Thread.Sleep(500);
                        //SearchEmployeeLabel.Click();
                        //Thread.Sleep(1000);
                        //CloseSpinneronDiv();
                        //CloseSpinneronPage();
                        //Thread.Sleep(1000);
                        //WaitForElement(SearchEmployeeInput);
                        //SearchEmployeeInput.SendKeys("Ms Clare Hebden-Rawson");
                        //Thread.Sleep(1000);
                        CloseSpinneronDiv();
                        CloseSpinneronPage();
                        //Thread.Sleep(1000);
                        if (EmpList.Count > 0)
                        {
                            //Thread.Sleep(1500);
                            CloseSpinneronDiv();
                            //Thread.Sleep(1000);                          
                            WaitForElement(EmpList[0]);
                            WaitForElementToClick(EmpList[0]);
                            EmpList[0].Click();
                            //Thread.Sleep(1500);
                            CloseSpinneronDiv();
                            //Thread.Sleep(1000);
                        }
                        else
                        {
                            Thread.Sleep(1500);
                            CloseSpinneronDiv();
                            CloseSpinneronPage();
                            Thread.Sleep(1000);
                            WaitForElement(EmpList[0]);
                            WaitForElementToClick(EmpList[0]);
                            EmpList[0].Click();
                            //Thread.Sleep(1500);
                            CloseSpinneronDiv();
                            //Thread.Sleep(1000);

                        }
                    }
                }
                else

                {
                    WaitForElement(CancelButton);
                    CancelButton.Click();
                    //Thread.Sleep(1000);
                    CloseSpinneronDiv();
                    CloseSpinneronPage();
                }

            }
        }
        //set Land Owner Role 
        public void SetLandOwnerRole()
        {
            Thread.Sleep(1000);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(1000);
            WaitForElement(EditRoleButton);
            EditRoleButton.Click();
            Thread.Sleep(1000);
            CloseSpinneronDiv();
            //Thread.Sleep(1000);
            WaitForElement(SearchRoleLabel);
            SearchRoleLabel.Click();
            SearchRoleLabel.Clear();
            SearchRoleLabel.SendKeys("Land Owner");
            Thread.Sleep(1000);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(1000);
            WaitForElements(LandOwnerRole);
            if (LandOwnerRole.Count > 0)
            {
                //Thread.Sleep(1500);
                CloseSpinneronDiv();
                //Thread.Sleep(1000);
                WaitForElement(LandOwnerRole[0]);
                CloseSpinneronDiv();
                LandOwnerRole[0].Click();
                Thread.Sleep(1000);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                Thread.Sleep(1000);
                Assert.IsTrue(SelectButton.Count > 0);

                if (SelectButton.Count > 0)
                {
                    WaitForElements(SelectButton);
                    SelectButton[0].Click();
                    Thread.Sleep(1000);
                    CloseSpinneronDiv();
                    CloseSpinneronPage();
                    Thread.Sleep(1000);
                    if (ChooseBuilder.Count > 0)
                    {
                        WaitForElement(SearchLabel);
                        SearchLabel.Click();
                        WaitForElement(ChooseBuilder[0]);
                        CloseSpinneronDiv();
                        Thread.Sleep(1000);
                        ChooseBuilder[0].Click();
                        Thread.Sleep(1500);
                        CloseSpinneronDiv();
                        Thread.Sleep(1000);
                        //if (Statics.RoleName != null)
                        //{
                        //    //Thread.Sleep(1000);
                        //    WaitForElement(SearchInput);
                        //    SearchInput.Clear();
                        //    SearchInput.SendKeys(Statics.RoleName);
                        //    Thread.Sleep(1000);
                        //    CloseSpinneronDiv();
                        //    CloseSpinneronPage();
                        //    Thread.Sleep(1000);
                        //    if (ChooseBuilder.Count > 0)
                        //    {
                        //        WaitForElement(ChooseBuilder[0]);
                        //        CloseSpinneronDiv();
                        //        //Thread.Sleep(1000);
                        //        ChooseBuilder[0].Click();
                        //        //Thread.Sleep(1500);
                        //        CloseSpinneronDiv();
                        //        //Thread.Sleep(1000);
                        //    }
                        //    else
                        //    {
                        //        CloseSpinneronDiv();
                        //        WaitForElement(AddCompanyButton);
                        //        WaitForElementToClick(AddCompanyButton);
                        //        AddCompanyButton.Click();
                        //        AdditionalMethodsPage.AddCompany();
                        //    }
                        //}
                        //else
                        //{
                        //    CloseSpinneronDiv();
                        //    WaitForElement(AddCompanyButton);
                        //    WaitForElementToClick(AddCompanyButton);
                        //    AddCompanyButton.Click();
                        //    AdditionalMethodsPage.AddCompany();                  

                    }
                    else
                    {
                        CloseSpinneronDiv();
                        WaitForElement(AddCompanyButton);
                        WaitForElementToClick(AddCompanyButton);
                        AddCompanyButton.Click();
                        AdditionalMethodsPage.AddCompany();
                    }
                }
            }

            //if (SelectButton.Count > 0)
            //    {
            //        WaitForElements(SelectButton);
            //        WaitForElementToClick(SelectButton[0]);
            //        SelectButton[0].Click();
            //        Thread.Sleep(1000);
            //        CloseSpinneronDiv();
            //        CloseSpinneronPage();
            //        Thread.Sleep(1000);
                    
            //        if (ChooseBuilder.Count > 0)
            //        {
            //            WaitForElement(SearchLabel);
            //            SearchLabel.Click();
            //            if (Statics.RoleName != null)
            //            {
            //                //Thread.Sleep(1000);
            //                WaitForElement(SearchInput);
            //                SearchInput.Clear();
            //                SearchInput.SendKeys(Statics.RoleName);
            //                Thread.Sleep(1000);
            //                CloseSpinneronDiv();
            //                CloseSpinneronPage();
            //                Thread.Sleep(1000);
            //                WaitForElements(ChooseBuilder);
            //                if (ChooseBuilder.Count > 0)
            //                {
            //                    WaitForElement(ChooseBuilder[0]);
            //                    CloseSpinneronDiv();
            //                    //Thread.Sleep(1000);
            //                    ChooseBuilder[0].Click();
            //                    //Thread.Sleep(1500);
            //                    CloseSpinneronDiv();
            //                    //Thread.Sleep(1000);
            //                }
            //                else
            //                {
            //                    CloseSpinneronDiv();
            //                    WaitForElement(AddCompanyButton);
            //                    WaitForElementToClick(AddCompanyButton);
            //                    AddCompanyButton.Click();
            //                    AdditionalMethodsPage.AddCompany();
            //                }
            //            }
            //            else
            //            {
            //                CloseSpinneronDiv();
            //                WaitForElement(AddCompanyButton);
            //                WaitForElementToClick(AddCompanyButton);
            //                AddCompanyButton.Click();
            //                AdditionalMethodsPage.AddCompany();
            //            }
            //        }
            //        else
            //        {
            //            CloseSpinneronDiv();
            //            WaitForElement(AddCompanyButton);
            //            WaitForElementToClick(AddCompanyButton);
            //            AddCompanyButton.Click();
            //            AdditionalMethodsPage.AddCompany();
            //        }

            //    }                
            //}
            else
            {
                WaitForElement(CancelButton);
                CancelButton.Click();
                //Thread.Sleep(1000);
                CloseSpinneronDiv();
                CloseSpinneronPage();
            }
        }

        //Set Building Control Provider Role 
        public void SetBCProviderRole()
        {
            Thread.Sleep(1000);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(1000);
            WaitForElement(EditRoleButton);
            EditRoleButton.Click();
            //Thread.Sleep(1000);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(1000);
            WaitForElement(SearchRoleLabel);
            SearchRoleLabel.Click();
            SearchRoleLabel.Clear();
            SearchRoleLabel.SendKeys("Building Control Provider");
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            if (BuildingControlProviderRole.Count > 0)
            {
                //Thread.Sleep(1000);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                //Thread.Sleep(1000);
                WaitForElement(BuildingControlProviderRole[0]);
                //Thread.Sleep(1000);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                //Thread.Sleep(1000);
                BuildingControlProviderRole[0].Click();
                CloseSpinneronDiv();
                //Thread.Sleep(1000);
                Thread.Sleep(500);
                //Thread.Sleep(1500);
                CloseSpinneronDiv();
                //Thread.Sleep(1000);

                if (SelectButton.Count > 0)
                {
                    WaitForElements(SelectButton);
                    SelectButton[0].Click();
                    Thread.Sleep(1000);
                    CloseSpinneronDiv();
                    CloseSpinneronPage();
                    Thread.Sleep(1000);
                    if (ChooseBuilder.Count > 0)
                    {
                        WaitForElement(SearchLabel);
                        SearchLabel.Click();
                        WaitForElement(ChooseBuilder[0]);
                        CloseSpinneronDiv();
                        //Thread.Sleep(1000);
                        ChooseBuilder[0].Click();
                        //Thread.Sleep(1500);
                        CloseSpinneronDiv();
                        //Thread.Sleep(1000);
                        //if (Statics.RoleName != null)
                        //{
                        //    //Thread.Sleep(1000);
                        //    WaitForElement(SearchInput);
                        //    SearchInput.Clear();
                        //    SearchInput.SendKeys(Statics.RoleName);
                        //    Thread.Sleep(1000);
                        //    CloseSpinneronDiv();
                        //    CloseSpinneronPage();
                        //    Thread.Sleep(1000);
                        //    if (ChooseBuilder.Count > 0)
                        //    {
                        //        WaitForElement(ChooseBuilder[0]);
                        //        CloseSpinneronDiv();
                        //        //Thread.Sleep(1000);
                        //        ChooseBuilder[0].Click();
                        //        //Thread.Sleep(1500);
                        //        CloseSpinneronDiv();
                        //        //Thread.Sleep(1000);
                        //    }
                        //    else
                        //    {
                        //        CloseSpinneronDiv();
                        //        WaitForElement(AddCompanyButton);
                        //        WaitForElementToClick(AddCompanyButton);
                        //        AddCompanyButton.Click();
                        //        AdditionalMethodsPage.AddCompany();
                        //    }
                        //}
                        //else
                        //{
                        //    CloseSpinneronDiv();
                        //    WaitForElement(AddCompanyButton);
                        //    WaitForElementToClick(AddCompanyButton);
                        //    AddCompanyButton.Click();
                        //    AdditionalMethodsPage.AddCompany();                  

                    }
                    else
                    {
                        CloseSpinneronDiv();
                        WaitForElement(AddCompanyButton);
                        WaitForElementToClick(AddCompanyButton);
                        AddCompanyButton.Click();
                        AdditionalMethodsPage.AddCompany();
                    }
                }
            }

            //if (SelectButton.Count > 0)
            //    {
            //        WaitForElements(SelectButton);
            //        SelectButton[0].Click();
            //        Thread.Sleep(500);
            //        CloseSpinneronDiv();
            //        CloseSpinneronPage();
            //        Thread.Sleep(500);
            //        if (ChooseBuilder.Count > 0)
            //        {
            //            WaitForElement(SearchLabel);
            //            SearchLabel.Click();
            //            if (Statics.RoleName != null)
            //            {
            //                //Thread.Sleep(1000);
            //                WaitForElement(SearchInput);
            //                SearchInput.Clear();
            //                SearchInput.SendKeys(Statics.RoleName);
            //                Thread.Sleep(1000);
            //                CloseSpinneronDiv();
            //                CloseSpinneronPage();
            //                Thread.Sleep(1000);
            //                if (ChooseBuilder.Count > 0)
            //                {
            //                    WaitForElement(ChooseBuilder[0]);
            //                    CloseSpinneronDiv();
            //                    //Thread.Sleep(1000);
            //                    ChooseBuilder[0].Click();
            //                    //Thread.Sleep(1500);
            //                    CloseSpinneronDiv();
            //                    //Thread.Sleep(1000);
            //                }
            //                else
            //                {
            //                    CloseSpinneronDiv();
            //                    WaitForElement(AddCompanyButton);
            //                    WaitForElementToClick(AddCompanyButton);
            //                    AddCompanyButton.Click();
            //                    AdditionalMethodsPage.AddCompany();
            //                }
            //            }
            //            else
            //            {
            //                CloseSpinneronDiv();
            //                WaitForElement(AddCompanyButton);
            //                WaitForElementToClick(AddCompanyButton);
            //                AddCompanyButton.Click();
            //                AdditionalMethodsPage.AddCompany();
            //            }
            //        }
            //        else
            //        {
            //            CloseSpinneronDiv();
            //            WaitForElement(AddCompanyButton);
            //            WaitForElementToClick(AddCompanyButton);
            //            AddCompanyButton.Click();
            //            AdditionalMethodsPage.AddCompany();
            //        }
            //    }
            //}
            else
            {
                WaitForElement(CancelButton);
                CancelButton.Click();
                //Thread.Sleep(1000);
                CloseSpinneronDiv();
                CloseSpinneronPage();
            }
        }
        public void SetDesignReviewSurveyorRole()
        {
            Thread.Sleep(1000);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(1000);
            WaitForElement(EditRoleButton);
            EditRoleButton.Click();
            //Thread.Sleep(2000);
            WaitForElement(SearchRoleLabel);
            SearchRoleLabel.Click();
            SearchRoleLabel.Clear();
            SearchRoleLabel.SendKeys("Design Review Surveyor");
            //Thread.Sleep(1500);
            CloseSpinneronDiv();
            //Thread.Sleep(1000);
            if (DesignReviewSurveyorRole.Count > 0)
            {
                WaitForElements(DesignReviewSurveyorRole);
                DesignReviewSurveyorRole[0].Click();
                //Thread.Sleep(1000);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                //Thread.Sleep(1000);
                if (SelectButton.Count > 0)
                {
                    //Thread.Sleep(1000);
                    CloseSpinneronDiv();
                    CloseSpinneronPage();
                    //Thread.Sleep(1000);
                    WaitForElements(SelectButton);
                    SelectButton[0].Click();
                    //Thread.Sleep(1000);
                    CloseSpinneronDiv();
                    CloseSpinneronPage();
                    //Thread.Sleep(1000);
                    WaitForElements(CompanyList);
                    CloseSpinneronDiv();
                    //Thread.Sleep(1000);
                    CloseSpinneronDiv();
                    CloseSpinneronPage();
                    //Thread.Sleep(1000);
                    if (CompanyList.Count > 0)
                    {
                        //Thread.Sleep(1000);
                        CloseSpinneronDiv();
                        CloseSpinneronPage();
                        //Thread.Sleep(1000);
                        WaitForElement(CompanyList[0]);
                        CloseSpinneronDiv();
                        CompanyList[0].Click();
                        Thread.Sleep(1000);
                        CloseSpinneronDiv();
                        CloseSpinneronPage();
                        Thread.Sleep(1000);
                        //WaitForElement(SearchEmployeeLabel);
                        //Thread.Sleep(500);
                        //SearchEmployeeLabel.Click();
                        //Thread.Sleep(1000);
                        //CloseSpinneronDiv();
                        //CloseSpinneronPage();
                        //Thread.Sleep(1000);
                        //WaitForElement(SearchEmployeeInput);
                        //SearchEmployeeInput.SendKeys("Ms Clare Hebden-Rawson");
                        //Thread.Sleep(1000);
                        CloseSpinneronDiv();
                        CloseSpinneronPage();
                        //Thread.Sleep(1000);
                        if (EmpList.Count > 0)
                        {
                            //Thread.Sleep(1500);
                            CloseSpinneronDiv();
                            //Thread.Sleep(1000);                          
                            WaitForElement(EmpList[0]);
                            WaitForElementToClick(EmpList[0]);
                            EmpList[0].Click();
                            //Thread.Sleep(1500);
                            CloseSpinneronDiv();
                            //Thread.Sleep(1000);
                        }
                        else
                        {
                            Thread.Sleep(1500);
                            CloseSpinneronDiv();
                            CloseSpinneronPage();
                            Thread.Sleep(1000);
                            WaitForElement(EmpList[0]);
                            WaitForElementToClick(EmpList[0]);
                            EmpList[0].Click();
                            //Thread.Sleep(1500);
                            CloseSpinneronDiv();
                            //Thread.Sleep(1000);

                        }
                    }
                }
            }
            else

            {
                WaitForElement(CancelButton);
                CancelButton.Click();
                //Thread.Sleep(1000);
                CloseSpinneronDiv();
                CloseSpinneronPage();
            }
        }
        public void SetRefurbishmentAssessmentSurveyorRole()
        {
            Thread.Sleep(1000);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(1000);
            WaitForElement(EditRoleButton);
            EditRoleButton.Click();
            //Thread.Sleep(1000);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(1000);
            WaitForElement(SearchRoleLabel);
            SearchRoleLabel.Click();
            SearchRoleLabel.Clear();
            SearchRoleLabel.SendKeys("Refurbishment Assessment Surveyor");
            //Thread.Sleep(1000);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(1000);
            if (RefurbishmentAssessmentSurveyorRole.Count > 0)
            {
                //Thread.Sleep(1000);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                //Thread.Sleep(1000);
                WaitForElements(RefurbishmentAssessmentSurveyorRole);
                RefurbishmentAssessmentSurveyorRole[0].Click();
                //Thread.Sleep(1000);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                //Thread.Sleep(1000);
                if (SelectButton.Count > 0)
                {
                    //Thread.Sleep(1000);
                    CloseSpinneronDiv();
                    CloseSpinneronPage();
                    //Thread.Sleep(1000);
                    WaitForElements(SelectButton);
                    SelectButton[0].Click();
                    //Thread.Sleep(1000);
                    CloseSpinneronDiv();
                    CloseSpinneronPage();
                    //Thread.Sleep(1000);
                    WaitForElements(CompanyList);
                    CloseSpinneronDiv();
                    //Thread.Sleep(1000);
                    CloseSpinneronDiv();
                    CloseSpinneronPage();
                    //Thread.Sleep(1000);
                    if (CompanyList.Count > 0)
                    {
                        //Thread.Sleep(1000);
                        CloseSpinneronDiv();
                        CloseSpinneronPage();
                        //Thread.Sleep(1000);
                        WaitForElement(CompanyList[0]);
                        CloseSpinneronDiv();
                        CompanyList[0].Click();
                        Thread.Sleep(1000);
                        CloseSpinneronDiv();
                        CloseSpinneronPage();
                        Thread.Sleep(1000);
                        //WaitForElement(SearchEmployeeLabel);
                        //Thread.Sleep(500);
                        //SearchEmployeeLabel.Click();
                        //Thread.Sleep(1000);
                        //CloseSpinneronDiv();
                        //CloseSpinneronPage();
                        //Thread.Sleep(1000);
                        //WaitForElement(SearchEmployeeInput);
                        //SearchEmployeeInput.SendKeys("Ms Clare Hebden-Rawson");
                        //Thread.Sleep(1000);
                        CloseSpinneronDiv();
                        CloseSpinneronPage();
                        //Thread.Sleep(1000);
                        if (EmpList.Count > 0)
                        {
                            //Thread.Sleep(1500);
                            CloseSpinneronDiv();
                            //Thread.Sleep(1000);                          
                            WaitForElement(EmpList[0]);
                            WaitForElementToClick(EmpList[0]);
                            EmpList[0].Click();
                            //Thread.Sleep(1500);
                            CloseSpinneronDiv();
                            //Thread.Sleep(1000);
                        }
                        else
                        {
                            Thread.Sleep(1500);
                            CloseSpinneronDiv();
                            CloseSpinneronPage();
                            Thread.Sleep(1000);
                            WaitForElement(EmpList[0]);
                            WaitForElementToClick(EmpList[0]);
                            EmpList[0].Click();
                            //Thread.Sleep(1500);
                            CloseSpinneronDiv();
                            //Thread.Sleep(1000);

                        }
                    }
                }
            }
            else

            {
                WaitForElement(CancelButton);
                CancelButton.Click();
                //Thread.Sleep(1000);
                CloseSpinneronDiv();
                CloseSpinneronPage();
            }
        }

        //Set Surveyor Role
        public void SetSurveyorRole()
        {
            Thread.Sleep(1000);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(1000);
            WaitForElement(EditRoleButton);
            EditRoleButton.Click();
            //Thread.Sleep(1000);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(1000);
            WaitForElement(SearchRoleLabel);
            SearchRoleLabel.Click();
            SearchRoleLabel.Clear();
            SearchRoleLabel.SendKeys("Surveyor");
            //Thread.Sleep(1000);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(1000);
            if (SurveyorRole.Count > 0)
            {
                //Thread.Sleep(1000);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                //Thread.Sleep(1000);
                WaitForElements(SurveyorRole);
                SurveyorRole[0].Click();
                //Thread.Sleep(1000);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                //Thread.Sleep(1000);
                if (SelectButton.Count > 0)
                {
                    //Thread.Sleep(1000);
                    CloseSpinneronDiv();
                    CloseSpinneronPage();
                    //Thread.Sleep(1000);
                    WaitForElements(SelectButton);
                    SelectButton[0].Click();
                    //Thread.Sleep(1000);
                    CloseSpinneronDiv();
                    CloseSpinneronPage();
                    //Thread.Sleep(1000);
                    WaitForElements(CompanyList);
                    CloseSpinneronDiv();
                    //Thread.Sleep(1000);
                    CloseSpinneronDiv();
                    CloseSpinneronPage();
                    //Thread.Sleep(1000);
                    if (CompanyList.Count > 0)
                    {
                        //Thread.Sleep(1000);
                        CloseSpinneronDiv();
                        CloseSpinneronPage();
                        //Thread.Sleep(1000);
                        WaitForElement(CompanyList[0]);
                        CloseSpinneronDiv();
                        CompanyList[0].Click();
                        Thread.Sleep(1000);
                        CloseSpinneronDiv();
                        CloseSpinneronPage();
                        Thread.Sleep(1000);
                        //WaitForElement(SearchEmployeeLabel);
                        //Thread.Sleep(500);
                        //SearchEmployeeLabel.Click();
                        //Thread.Sleep(1000);
                        //CloseSpinneronDiv();
                        //CloseSpinneronPage();
                        //Thread.Sleep(1000);
                        //WaitForElement(SearchEmployeeInput);
                        //SearchEmployeeInput.SendKeys("Ms Clare Hebden-Rawson");
                        //Thread.Sleep(1000);
                        CloseSpinneronDiv();
                        CloseSpinneronPage();
                        //Thread.Sleep(1000);
                        if (EmpList.Count > 0)
                        {
                            //Thread.Sleep(1500);
                            CloseSpinneronDiv();
                            //Thread.Sleep(1000);                          
                            WaitForElement(EmpList[0]);
                            WaitForElementToClick(EmpList[0]);
                            EmpList[0].Click();
                            //Thread.Sleep(1500);
                            CloseSpinneronDiv();
                            //Thread.Sleep(1000);
                        }
                        else
                        {
                            Thread.Sleep(1500);
                            CloseSpinneronDiv();
                            CloseSpinneronPage();
                            Thread.Sleep(1000);
                            WaitForElement(EmpList[0]);
                            WaitForElementToClick(EmpList[0]);
                            EmpList[0].Click();
                            //Thread.Sleep(1500);
                            CloseSpinneronDiv();
                            //Thread.Sleep(1000);

                        }
                    }
                }
            }
            else

            {
                WaitForElement(CancelButton);
                CancelButton.Click();
                //Thread.Sleep(1000);
                CloseSpinneronDiv();
                CloseSpinneronPage();
            }
        }
        //Setting up Local Authority Email address 
        public void SetupEmailonLocalAuthority()
        {
            WaitForElement(ConditionsTab);
            ConditionsTab.Click();
           Thread.Sleep(2000);
            CloseSpinneronPage();
            CloseSpinneronDiv();
           Thread.Sleep(2000);          
            if (ConditionsRows.Count > 0)
            {
                //Thread.Sleep(2000);
                var SettingofLocalAuthorityRole = ConditionsRows.Any(x => x.Text.Contains("Setting of Local Authority role - with valid email address"));
                if (SettingofLocalAuthorityRole == true)
                {
                    WaitForElement(RolesTab);
                    RolesTab.Click();
                    Thread.Sleep(2000);
                    CloseSpinneronPage();
                    CloseSpinneronDiv();
                    Thread.Sleep(2000);
                    if (LocalAuthorityEmails.Count > 0)
                    {
                        LocalAuthorityEmails[0].Click();
                        WaitForElement(EditCompanyButton);
                        EditCompanyButton.Click();
                        Thread.Sleep(1000);
                        CloseSpinneronPage();
                        CloseSpinneronDiv();
                        Thread.Sleep(1000);
                        WaitForElement(EditCompanyDiv);
                        Assert.IsTrue(EditCompanyDiv.Displayed);
                        WaitForElement(EditCompanyNextButton);
                        WaitForElementToClick(EditCompanyNextButton);
                        EditCompanyNextButton.Click();
                        Thread.Sleep(2000);
                        CloseSpinneronPage();
                        CloseSpinneronDiv();
                        Thread.Sleep(2000);
                        if (OfficeAddress.Count > 0)
                        {
                            WaitForElementToClick(OfficeAddress[0]);
                            OfficeAddress[0].Click();
                            WaitForElement(EditOfficeAddressButton);
                            EditOfficeAddressButton.Click();
                            WaitForElement(OfficeNameInput);
                            Thread.Sleep(1000);
                            CloseSpinneronDiv();
                            CloseSpinneronPage();
                            Thread.Sleep(1000);
                            if (EmailLabel.Count > 0)
                            {
                                EmailLabel[0].Click();
                            }
                            EmailInput.Click();
                            EmailInput.Clear();
                            EmailInput.SendKeys(ConfigurationManager.AppSettings["Email"]);
                            CloseSpinneronDiv();
                            CloseSpinneronPage();
                            Thread.Sleep(1000);
                            WaitForElement(AddOfficeDataButton);
                            AddOfficeDataButton.Click();
                            Thread.Sleep(1000);
                            CloseSpinneronDiv();
                            CloseSpinneronPage();
                            Thread.Sleep(1000);                         
                            WaitForElement(EditCompanyNextButton);
                            EditCompanyNextButton.Click();
                        }
                        WaitForElement(SaveCompanyButton);
                        WaitForElementToClick(SaveCompanyButton);
                        SaveCompanyButton.Click();
                        Thread.Sleep(1000);
                        CloseSpinneronDiv();
                        CloseSpinneronPage();
                        Thread.Sleep(1000);
                    }
                }
                else
                {
                    WaitForElement(OverviewTab);
                    OverviewTab.Click();
                    //Thread.Sleep(2000);
                    CloseSpinneronPage();
                    CloseSpinneronDiv();
                    //Thread.Sleep(2000);
                }
            }
            else
            {
                WaitForElement(OverviewTab);
                OverviewTab.Click();
                //Thread.Sleep(2000);
                CloseSpinneronPage();
                CloseSpinneronDiv();
                //Thread.Sleep(2000);
            }
        }
        public void SubmitQuote(string masterPlotData)
        {
            ExtensionMethods.ReadExcelPlotData(masterPlotData);
            CloseSpinneronPage();
            CloseSpinneronDiv();
            Thread.Sleep(2000);
            WaitForElement(Dashboardpage.PlusButton);
            Dashboardpage.PlusButton.Click();
            WaitForElement(Dashboardpage.QuoteOption);
            Dashboardpage.QuoteOption.Click();
            WaitForElement(AddNewQuoteDiv);
            CloseSpinneronPage();
            CloseSpinneronDiv();
            KeySiteDetailsPageOnLatestProductVersion();
            OtherSiteDetailsPage();
            PlotSchedulePage(masterPlotData);
            ProductsDetailsPage();
            AdditionalQuestionsPage();
            RolesDetails();
            DeclarationDetailsPage();
            WaitForElement(SaveButton);
            SaveButton.Click();
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(2000);
            WaitForElement(SubmitButton);
            SubmitButton.Click();
            Thread.Sleep(2000);
            CloseSpinneronPage();
            CloseSpinneronDiv();
            Thread.Sleep(2000);
            WaitForElement(Dashboardpage.QuoteApplicationStatusSubmitted);
            Assert.IsTrue(Dashboardpage.QuoteApplicationStatusSubmitted.Displayed);
        }
        public void SubmitQuoteOnSinglePlot()
        {
            ExtensionMethods.ReadExcelPlotData(Premiums.PlotData);
            CloseSpinneronPage();
            CloseSpinneronDiv();
            Thread.Sleep(2000);
            WaitForElement(Dashboardpage.PlusButton);
            Dashboardpage.PlusButton.Click();
            WaitForElement(Dashboardpage.QuoteOption);
            Dashboardpage.QuoteOption.Click();
            WaitForElement(AddNewQuoteDiv);
            CloseSpinneronPage();
            CloseSpinneronDiv();
            KeySiteDetailsPageOnLatestProductVersion();
            OtherSiteDetailsPage();
            PlotSchedulePage(Premiums.PlotData);
            ProductsDetailsPage();
            AdditionalQuestionsPage();
            RolesDetails();
            DeclarationDetailsPage();
            WaitForElement(SaveButton);
            SaveButton.Click();
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(2000);
            WaitForElement(SubmitButton);
            SubmitButton.Click();
            Thread.Sleep(2000);
            CloseSpinneronPage();
            CloseSpinneronDiv();
            Thread.Sleep(2000);
            WaitForElement(Dashboardpage.QuoteApplicationStatusSubmitted);
            Assert.IsTrue(Dashboardpage.QuoteApplicationStatusSubmitted.Displayed);
        }
        public void SubmitQuoteForAvivaInsurer(string masterPlotData)
        {
            ExtensionMethods.ReadExcelPlotData(masterPlotData);
            CloseSpinneronPage();
            CloseSpinneronDiv();
            Thread.Sleep(2000);
            WaitForElement(Dashboardpage.PlusButton);
            Dashboardpage.PlusButton.Click();
            WaitForElement(Dashboardpage.QuoteOption);
            Dashboardpage.QuoteOption.Click();
            WaitForElement(AddNewQuoteDiv);
            CloseSpinneronPage();
            CloseSpinneronDiv();
            KeySiteDetailsForAvivaOnLatestProductVersion();
            OtherSiteDetailsPage();
            PlotSchedulePage(masterPlotData);
            ProductsDetailsPage();
            AdditionalQuestionsPage();
            RolesDetails();
            DeclarationDetailsPage();
            WaitForElement(SaveButton);
            SaveButton.Click();
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(2000);
            WaitForElement(SubmitButton);
            SubmitButton.Click();
            Thread.Sleep(2000);
            CloseSpinneronPage();
            CloseSpinneronDiv();
            Thread.Sleep(2000);
            WaitForElement(Dashboardpage.QuoteApplicationStatusSubmitted);
            Assert.IsTrue(Dashboardpage.QuoteApplicationStatusSubmitted.Displayed);
        }
        public void SubmitQuoteFor12YearsCoverLength(string masterPlotData)
        {
            ExtensionMethods.ReadExcelPlotData(masterPlotData);
            CloseSpinneronPage();
            CloseSpinneronDiv();
            Thread.Sleep(2000);
            WaitForElement(Dashboardpage.PlusButton);
            Dashboardpage.PlusButton.Click();
            WaitForElement(Dashboardpage.QuoteOption);
            Dashboardpage.QuoteOption.Click();
            WaitForElement(AddNewQuoteDiv);
            CloseSpinneronPage();
            CloseSpinneronDiv();
            KeySiteDetailsPageOnLatestProductVersion();
            OtherSiteDetailsPage();
            PlotSchedulePage(masterPlotData);
            ProductsDetailsPageFor12Years();
            AdditionalQuestionsPage();
            RolesDetails();
            DeclarationDetailsPage();
            WaitForElement(SaveButton);
            SaveButton.Click();
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(2000);
            WaitForElement(SubmitButton);
            SubmitButton.Click();
            Thread.Sleep(2000);
            CloseSpinneronPage();
            CloseSpinneronDiv();
            Thread.Sleep(2000);
            WaitForElement(Dashboardpage.QuoteApplicationStatusSubmitted);
            Assert.IsTrue(Dashboardpage.QuoteApplicationStatusSubmitted.Displayed);
        }
        public void SubmitQuoteOnSinglePlotFor12YearsCoverLength()
        {
            ExtensionMethods.ReadExcelPlotData(Premiums.PlotData);
            CloseSpinneronPage();
            CloseSpinneronDiv();
            Thread.Sleep(2000);
            WaitForElement(Dashboardpage.PlusButton);
            Dashboardpage.PlusButton.Click();
            WaitForElement(Dashboardpage.QuoteOption);
            Dashboardpage.QuoteOption.Click();
            WaitForElement(AddNewQuoteDiv);
            CloseSpinneronPage();
            CloseSpinneronDiv();
            KeySiteDetailsPageOnLatestProductVersion();
            OtherSiteDetailsPage();
            PlotSchedulePage(Premiums.PlotData);
            ProductsDetailsPageFor12Years();
            AdditionalQuestionsPage();
            RolesDetails();
            DeclarationDetailsPage();
            WaitForElement(SaveButton);
            SaveButton.Click();
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(2000);
            WaitForElement(SubmitButton);
            SubmitButton.Click();
            Thread.Sleep(2000);
            CloseSpinneronPage();
            CloseSpinneronDiv();
            Thread.Sleep(2000);
            WaitForElement(Dashboardpage.QuoteApplicationStatusSubmitted);
            Assert.IsTrue(Dashboardpage.QuoteApplicationStatusSubmitted.Displayed);
        }
        public void SubmitQuoteForAvivaInsurerFor12YearsCoverLength(string masterPlotData)
        {
            ExtensionMethods.ReadExcelPlotData(masterPlotData);
            CloseSpinneronPage();
            CloseSpinneronDiv();
            Thread.Sleep(2000);
            WaitForElement(Dashboardpage.PlusButton);
            Dashboardpage.PlusButton.Click();
            WaitForElement(Dashboardpage.QuoteOption);
            Dashboardpage.QuoteOption.Click();
            WaitForElement(AddNewQuoteDiv);
            CloseSpinneronPage();
            CloseSpinneronDiv();
            KeySiteDetailsForAvivaOnLatestProductVersion();
            OtherSiteDetailsPage();
            PlotSchedulePage(masterPlotData);
            ProductsDetailsPageFor12Years();
            AdditionalQuestionsPage();
            RolesDetails();
            DeclarationDetailsPage();
            WaitForElement(SaveButton);
            SaveButton.Click();
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(2000);
            WaitForElement(SubmitButton);
            SubmitButton.Click();
            Thread.Sleep(2000);
            CloseSpinneronPage();
            CloseSpinneronDiv();
            Thread.Sleep(2000);
            WaitForElement(Dashboardpage.QuoteApplicationStatusSubmitted);
            Assert.IsTrue(Dashboardpage.QuoteApplicationStatusSubmitted.Displayed);
        }
    }
}
