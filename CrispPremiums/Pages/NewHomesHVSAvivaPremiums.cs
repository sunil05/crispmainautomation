﻿using OpenQA.Selenium;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.PageObjects;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using System.IO;
using ExcelDataReader;
using RegressionPacks.Support;

namespace RegressionPacks.Pages
{
    public class NewHomesHVSAvivaPremiums : Support.Pages
    {
        public string RatingValue;
        public IWebDriver wdriver;
        double iptCaluclationValue = 0.12;
        double vatCaluclationValue = 0.20;
        double BasePremium = 0;
        double SecondaryPremium = 0;
        double TAFee = 0;
        double RefurbishmentFee = 0;
        double ConsumerCodeFee = 0;
        double InsolvencyPremium = 0;
        double Banding;
        int RatingColumn;
        double AtkinsPremium = 0;
        double MinimumAtkinsFee = 0;
        double CurtainWallingPremium = 0;
        double StoreyLoadingPremium = 0;
        double BasementLoadingPremium = 0;
        DataTable excelTable;
        public NewHomesHVSAvivaPremiums(ISearchContext driver)
        {
            PageFactory.InitElements(driver, this);
            wdriver = (RemoteWebDriver)driver;
        }
        //Retrieve Covers and Services on Fees Details Page 
        public void CoversOnNewhomesProduct()
        {

            if (Premiums.eachplotrow.Any(o => o.ProductName.Contains("New Homes")))
            {
                Premiums.ProductsCovers = PremiumCovers.ProductCoversList.Select(i => i.Text.ToString()).ToList();
                Premiums.ProductServices = PremiumCovers.ProductServicesList.Select(i => i.Text.ToString()).ToList();

                //Products Covers
                if (Premiums.ProductsCovers.Any(x => x.Contains("Insolvency of the Developer")))
                {
                    Premiums.InsolvencyCover = PremiumCovers.InsolvencyOfDeveloperCover.Count > 0 ? "Yes" : "No";
                }
                if (Premiums.ProductsCovers.Any(x => x.Contains("Defects")))
                {
                    Premiums.DefectsCover = PremiumCovers.DefectsCover.Count > 0 ? "Yes" : "No";
                }
                if (Premiums.ProductsCovers.Any(x => x.Contains("Contaminated Land")))
                {
                    Premiums.ContaminatedLandCover = PremiumCovers.ContaminatedLandList.Count > 0 ? "Yes" : "No";
                }
                if(Premiums.ProductsCovers.Any(x=>x.Contains("Additional Cover for Local Authority Building Control Function")))
                {
                    Premiums.LocalAuthorityBuildingControlFunctionCover = PremiumCovers.LocalAuthorityBuildingControlFunctionCover.Count > 0 ? "Yes" : "No";
                }
                if (Premiums.ProductsCovers.Any(x => x.Contains("Additional Cover for Approved Inspector Building Control Function")))
                {
                    Premiums.LocalAuthorityBuildingControlFunctionCover = PremiumCovers.ApprovedInspectorBuildingControlFunctionCover.Count > 0 ? "Yes" : "No";
                }
                //Products Services 
                if (Premiums.ProductServices.Any(x => x.Contains("Technical Audit Fee")))
                {
                    Premiums.ProductTechnicalAuditFeeService = PremiumCovers.ProductTechnicalAuditFee.Count > 0 ? "Yes" : "No";
                }
                if (Premiums.ProductServices.Any(x => x.Contains("Consumer Code Fee")))
                {
                    Premiums.ProductConsumerCodeService = PremiumCovers.ProductConsumerCodeFee.Count > 0 ? "Yes" : "No";
                }
                if (Premiums.ProductServices.Any(x => x.Contains("Refurbishment Assessment Fee")))
                {
                    Premiums.ProductRefurbishmentAssessmentService = PremiumCovers.ProductRefurbishmentAssessmentFee.Count > 0 ? "Yes" : "No";
                }
                if (Premiums.ProductServices.Any(x => x.Contains("Administration Fee")))
                {
                    Premiums.ProductAdminFeeService = PremiumCovers.ProductAdministrationFee.Count > 0 ? "Yes" : "No";
                }
                if (Premiums.ProductServices.Any(x => x.Contains("Atkins Fee")))
                {
                    Premiums.ProductAtkinsFeeService = PremiumCovers.ProductAtkinsFee.Count > 0 ? "Yes" : "No";
                }
                if (Premiums.ProductServices.Any(x => x.Contains("Cancellation Fee")))
                {
                    Premiums.ProductCancellationFeeService = PremiumCovers.ProductCancellationFee.Count > 0 ? "Yes" : "No";
                }               
            }
        }
        //Caluclating Premiums For Each Row 
        public void NewHomesHVSAvivaPremiumsCaluclations()
        {
            Premiums.StructuralFee = 0;
            Premiums.InsolvencyFee = 0;
            Premiums.DefectsFee = 0;
            Premiums.ContaminatedLandFee = 0;
            Premiums.LocalAuthorityBuildingControlFee = 0;
            Premiums.ApprovedInspectorBuildingControlFee = 0;
            Premiums.TechnicalAuditFee = 0;
            Premiums.AdministrationFee = 0;
            Premiums.RefurbishmentAssessmentFee = 0;
            Premiums.ConsumerCodeFee = 0;
            Premiums.AtkinsFee = 0;
            Premiums.CancellationFee = 0;
            var aggregatedPremium = new NewhomesPremium();
        
            foreach (var plot in Premiums.eachplotrow)
            {
                var calculatedPremiums = NewhomePremiumCaluclationsForEachPlotRow(plot);
                //Cover Premiums Adding For Each Plot Row              
                Premiums.StructuralFee += calculatedPremiums.structuralFee;
                Premiums.InsolvencyFee += calculatedPremiums.insolvencyFee;
                Premiums.DefectsFee += calculatedPremiums.defectsFee;
                Premiums.ContaminatedLandFee += calculatedPremiums.contaminatedLandFee;
                Premiums.LocalAuthorityBuildingControlFee += calculatedPremiums.laBuildingControlFee;
                Premiums.ApprovedInspectorBuildingControlFee += calculatedPremiums.approvedInspectorBuildingControlFee;
                //Service Fees Adding For Each Plot Row
                Premiums.TechnicalAuditFee += calculatedPremiums.technicalFee;
                Premiums.ConsumerCodeFee += calculatedPremiums.consumerCodeFee;
                Premiums.RefurbishmentAssessmentFee += calculatedPremiums.refurbishmentAssessmentFee;
                Premiums.AdministrationFee += calculatedPremiums.administrationFee;
                Premiums.AtkinsFee = calculatedPremiums.atkinsFee;
                Premiums.CancellationFee += calculatedPremiums.cancellationFee;
            }
            if(Premiums.AtkinsFee < 3000)
            {
                MinimumAtkinsFeeCaluclations();
                Premiums.AtkinsFee =Convert.ToDouble(MinimumAtkinsFee);
            }
            //Caluclate Refurbishment Fee            
            Premiums.RefurbishmentAssessmentFee = RefurbishmentFee;
            //IPT Caluclations 
            Premiums.StructuralIPTFee = Math.Round(Convert.ToDouble(Premiums.StructuralFee * iptCaluclationValue), 2);
            Premiums.InsolvencyIPTFee = Math.Round(Convert.ToDouble(Premiums.InsolvencyFee * iptCaluclationValue), 2);
            Premiums.DefectsIPTFee = Math.Round(Convert.ToDouble(0.00));
            Premiums.ContaminatedLandIPTFee = Math.Round(Convert.ToDouble(0.00));
            //VAT Caluclations
            Premiums.TechnicalAuditVATFee = Math.Round(0.00);
            Premiums.AdministrationVATFee = Math.Round(0.00);
            Premiums.RefurbishmentAssessmentVATFee = Math.Round(0.00);
            Premiums.ConsumerCodeVATFee = Math.Round(0.00);
            Premiums.CancellationVATFee = Math.Round(0.00);
            Premiums.AtkinsVATFee = Math.Round(Convert.ToDouble(Premiums.AtkinsFee * iptCaluclationValue));
            VerfiyNewhomesUIValues();
            if (Premiums.eachplotrow.Any(o => o.BCProduct.Contains("Residential Building Control")))
            {
                ResidentialBCPremiums.ResidentialBCPremiumNewPricesCalcs();
            }
        }

        public class NewhomesPremium
        {
            public double structuralFee;

            public double insolvencyFee;

            public double defectsFee;

            public double contaminatedLandFee;

            public double laBuildingControlFee;

            public double approvedInspectorBuildingControlFee;

            public double technicalFee;

            public double administrationFee;

            public double refurbishmentAssessmentFee;

            public double consumerCodeFee;

            public double cancellationFee;

            public double atkinsFee;

        }

        public NewhomesPremium NewhomePremiumCaluclationsForEachPlotRow(plotrows plot)
        {
            var calculatedPremiums = new NewhomesPremium();
            //Retrive all the covers and Services from Fees Detaila Page 
            CoversOnNewhomesProduct();
            //Reading Pricing Setup Book 
            var fileInfo = new FileInfo(ExtensionMethods.GetAbsolutePath($@"TestData\Premiums\ProductPricingSetup\Product Pricing Setup - HVS Aviva.xlsx"));
            string premiumsdata = fileInfo.ToString();
            excelTable = ReadNewhomesMasterPremiumsPriceData(premiumsdata);
            //Structural Fee Caluclations Based on Banding           
            StuctralPremiumCaluclations(plot);
           
            //Caluclate Insolvancy Factor
            if (Premiums.InsolvencyCover == "Yes")
            {
                InsolvencyPremiumFatcor(plot);
                calculatedPremiums.insolvencyFee = InsolvencyPremium;
            }
            if (Premiums.ProductAtkinsFeeService == "Yes")
            {
                AtkinsFeeCaluclations();
                calculatedPremiums.atkinsFee = Math.Round(Convert.ToDouble(AtkinsPremium), 2);
            }
            //Cover Services Caluclations 
            TAFeeCaluclations(plot);

            //Consumer Code Fee Caluclations
            ConsumerCodeFeeCaluclations();
            calculatedPremiums.structuralFee = Math.Round(BasePremium,2);
            calculatedPremiums.contaminatedLandFee = Convert.ToDouble(0.00);        
            calculatedPremiums.defectsFee = Convert.ToDouble(0.00);
            calculatedPremiums.laBuildingControlFee = Convert.ToDouble(0.00);
            calculatedPremiums.approvedInspectorBuildingControlFee = Convert.ToDouble(0.00);
            calculatedPremiums.technicalFee = TAFee;
            calculatedPremiums.administrationFee = Convert.ToDouble(0.00);
            calculatedPremiums.refurbishmentAssessmentFee = RefurbishmentFee;
            calculatedPremiums.consumerCodeFee = ConsumerCodeFee;
            calculatedPremiums.cancellationFee = Convert.ToDouble(0.00);        

            return calculatedPremiums;
        }


        //Reading Strucural Premium Banding Table  and Caluclate Base Premium
        public void StuctralPremiumCaluclations(plotrows plot)
        {

            Banding = Convert.ToDouble(plot.ReconstructionCostValue);
            // Reading Structural Premium Table From Newhomes Pricing Setup Book
            var rating = Premiums.Rating;
            var ratingTableRow = -1;
            var ratingColumn = -1;
            //Get Rating Table from Structural Premium Primary Layer Table
            for (int i = 0; i < excelTable.Rows.Count; i++)
            {
                if (excelTable.Rows[i][1].ToString().Contains("Rating"))
                {
                    ratingTableRow = i;
                    break;
                }
            }
            if (ratingTableRow == -1)
                throw new Exception("Rating Table Row Not Found");


            //Get Rating Columns from Structural Premium Table

            for (int i = ratingTableRow; i < excelTable.Rows[ratingTableRow].ItemArray.Length; i++)
            {
                if (excelTable.Rows[ratingTableRow].ItemArray[i].ToString() == Premiums.Rating.ToString())
                {
                    ratingColumn = i;
                    RatingColumn = ratingColumn;
                }
            }

            if (ratingColumn == -1)
                throw new Exception("Rating Column Not Found");

            //Get Premium Value From structural Premium Table 
            double basicpremiumValue = Convert.ToDouble(excelTable.Rows[ratingTableRow + 1][RatingColumn]);
            BasePremium = Convert.ToDouble(plot.ReconstructionCostValue * basicpremiumValue);

            //Caluclate Premiums Based on Unit type 
            UnitTypeCaluclation(plot);
            //Caluclate Premiums Based on Construction type 
          
            if (Premiums.eachplotrow.Any(o => o.ConstructionType.Contains("New Build")) && Premiums.eachplotrow.Any(o => o.ConstructionType.Contains("Conversion")))
            {
                if(plot.ConstructionType.Contains("New Build"))
                {
                    ConstructionTypeCaluclationNewbuildandConversion(plot);
                }else
                {
                    ConstructionTypeCaluclation(plot);
                }              
            }
            else
            {
                ConstructionTypeCaluclation(plot);
            }   
            
            //Multi Project Loading Caluclation              

            if (SendQuotePage.MultiProjectDiscount.Count > 0)
            {
                Premiums.MultiProjectLoading = SendQuotePage.MultiProjectDiscount[0].Text;
                if (Premiums.MultiProjectLoading == "Yes")
                {
                    MultiProjectLoadngDiscountCaluclations(plot);
                }
               
            }
            if (SendQuotePage.DiscretionaryDiscount.Count > 0)
            {
                DiscretionaryDiscountCaluclations(plot);
            }
            if (SendQuotePage.StageOfWorksPercentage.Count > 0)
            {
                StageOfWorksLoadingPercentageCaluclations(plot);
            }
            //Curtaibn Walling Caluclations 
            if(Premiums.HasCurtainWalling == "Yes"
                || Premiums.StoreyLoading == "Yes"
                || Premiums.BasementLoading == "Yes")
            {
                if (Premiums.HasCurtainWalling == "Yes")
                {
                    CurtainWallingCaluclations(plot);
                }
                //Storey Loading Calucaltions 
                if (Premiums.StoreyLoading == "Yes")
                {
                    StoreyLoadingCaluclations(plot);
                }
                //Basement Loading Caluclations
                if (Premiums.BasementLoading == "Yes")
                {
                    BasementLoadingCaluclations(plot);
                }
                BasePremium = Convert.ToDouble(BasePremium + CurtainWallingPremium + StoreyLoadingPremium + BasementLoadingPremium);
            }            
        }
        //Reading Unit Type Loading Table and Caluclate Base Premium
        public void UnitTypeCaluclation(plotrows plot)
        {
            var unitTypeTableRow = -1;
            var unitTypeRow = -1;
            //Get Unit Type Loading Table from Structural Premium Table
            for (int i = 0; i < excelTable.Rows.Count; i++)
            {
                if (excelTable.Rows[i][0].ToString().Contains("Unit Type Loading"))
                {
                    unitTypeTableRow = i;
                    break;
                }
            }
            if (unitTypeTableRow == -1)
                throw new Exception("Unit Type Loading Table Not Found");

            //Get UnitType Row from Unit Type Loading Table
            for (int i = unitTypeTableRow; i < excelTable.Rows.Count; i++)
            {
                if (excelTable.Rows[i][1].ToString() == plot.UnitType.ToString())
                {
                    unitTypeRow = i;
                    break;
                }
            }
            if (unitTypeRow == -1)
                throw new Exception("Unit Type Row Not Found");
            double premiumValueBasedonUnitTypeLoading = Convert.ToDouble(excelTable.Rows[unitTypeRow][RatingColumn]);
            BasePremium = Convert.ToDouble(BasePremium * premiumValueBasedonUnitTypeLoading);
        }
        //Reading Construction Type Loading Table and Caluclate Base Premium
        public void ConstructionTypeCaluclation(plotrows plot)
        {
            var constructionTypeTableRow = -1;
            var constructionTypeRow = -1;
            //Get Construction Type Loading Table from Structural Premium Table
            for (int i = 0; i < excelTable.Rows.Count; i++)
            {
                if (excelTable.Rows[i][0].ToString().Contains("Construction Type Loading"))
                {
                    constructionTypeTableRow = i;
                    break;
                }
            }
            if (constructionTypeTableRow == -1)
                throw new Exception("Construction Type Loading Table Not Found");

            //Get Construction Type  Row from Construction Type Loading Table 
            for (int i = constructionTypeTableRow; i < excelTable.Rows.Count; i++)
            {
                if (excelTable.Rows[i][1].ToString() == plot.ConstructionType.ToString())
                {
                    constructionTypeRow = i;
                    break;
                }
            }
            if (constructionTypeRow == -1)
                throw new Exception("Construction Type Row Not Found");
            double premiumValueBasedOnConstructionType = Convert.ToDouble(excelTable.Rows[constructionTypeRow][RatingColumn]);
            BasePremium = Convert.ToDouble(BasePremium * premiumValueBasedOnConstructionType);
            if (plot.ConstructionType == "New Build")
            {
                NewBuildStagesOfWorkCaluclation(plot);
            }
            if (plot.ConstructionType == "Conversion")
            {
                ConversionStagesOfWorkCaluclation(plot);
            }
        }
        //Reading Construction Type Loading Table and Caluclate Base Premium
        public void ConstructionTypeCaluclationNewbuildandConversion(plotrows plot)
        {
            var constructionTypeTableRow = -1;
            var constructionTypeRow = -1;
            //Get Construction Type Loading Table from Structural Premium Table
            for (int i = 0; i < excelTable.Rows.Count; i++)
            {
                if (excelTable.Rows[i][0].ToString().Contains("Construction Type Loading"))
                {
                    constructionTypeTableRow = i;
                    break;
                }
            }
            if (constructionTypeTableRow == -1)
                throw new Exception("Construction Type Loading Table Not Found");

            //Get Construction Type  Row from Construction Type Loading Table 
            for (int i = constructionTypeTableRow; i < excelTable.Rows.Count; i++)
            {
                if (excelTable.Rows[i][1].ToString().Contains("New Build (with Conversion)"))
                {
                    constructionTypeRow = i;
                    break;
                }
            }
            if (constructionTypeRow == -1)
                throw new Exception("Construction Type Row Not Found");
            double premiumValueBasedOnConstructionType = Convert.ToDouble(excelTable.Rows[constructionTypeRow][RatingColumn]);
            BasePremium = Convert.ToDouble(BasePremium * premiumValueBasedOnConstructionType);
            if (plot.ConstructionType == "New Build")
            {
                NewBuildStagesOfWorkCaluclation(plot);
            }
            if (plot.ConstructionType == "Conversion")
            {
                ConversionStagesOfWorkCaluclation(plot);
            }
        }

        //Reading NewBuild Stages Of Work Loading Table and Caluclate Base Premium
        public void NewBuildStagesOfWorkCaluclation(plotrows plot)
        {
            double premiumValueBasedOnNewBuildSOW = 0.00;
            if (plot.StageOfWork == "No Works Started")
            {
                premiumValueBasedOnNewBuildSOW = Convert.ToDouble(1.00);
            }
            if (plot.StageOfWork == "Foundation/DPC")
            {
                premiumValueBasedOnNewBuildSOW = Convert.ToDouble(1.25);
            }
            if (plot.StageOfWork == "1st Floor")
            {
                premiumValueBasedOnNewBuildSOW = Convert.ToDouble(1.50);
            }
            if (plot.StageOfWork == "Wall Plate Level")
            {
                premiumValueBasedOnNewBuildSOW = Convert.ToDouble(1.75);
            }
            if (plot.StageOfWork == "Roof/Watertight")
            {
                premiumValueBasedOnNewBuildSOW = Convert.ToDouble(2.00);
            }
            BasePremium = Convert.ToDouble(BasePremium * premiumValueBasedOnNewBuildSOW);
        }

        //Reading Conversion Stages Of Work Loading Table and Caluclate Base Premium
        public void ConversionStagesOfWorkCaluclation(plotrows plot)
        {
            double premiumValueBasedOnConversionSOW = 0.00;
            if (plot.StageOfWork == "No Works Started")
            {
                premiumValueBasedOnConversionSOW = Convert.ToDouble(1.00);
            }
            if (plot.StageOfWork == "First Fix")
            {
                premiumValueBasedOnConversionSOW = Convert.ToDouble(1.25);
            }
            if (plot.StageOfWork == "Second Fix")
            {
                premiumValueBasedOnConversionSOW = Convert.ToDouble(1.75);
            }
            BasePremium = Convert.ToDouble(BasePremium * premiumValueBasedOnConversionSOW);
        }

        //Multi Project Loading Caluclation
        public void MultiProjectLoadingCaluclation(plotrows plot)
        {
            var multiProjectLoadingTableRow = -1;
            var multiProjectRow = -1;
            //Get Multi Project Loading Table from Structural Premium Table
            for (int i = 0; i < excelTable.Rows.Count; i++)
            {
                if (excelTable.Rows[i][0].ToString().Contains("Multi Project Loading (Site wide for all products)"))
                {
                    multiProjectLoadingTableRow = i;
                    break;
                }
            }
            if (multiProjectLoadingTableRow == -1)
                throw new Exception("Multi Project Loading Table Not Found");

            //Multi Project Loading Table Row from Multi Project Loading Table 
            for (int i = multiProjectLoadingTableRow; i < excelTable.Rows.Count; i++)
            {
                if (excelTable.Rows[i][1].ToString() == "Yes")
                {
                    multiProjectRow = i;
                    break;
                }
            }
            if (multiProjectRow == -1)
                throw new Exception("Multi Project Loading Table Row Not Found");
            double premiumValueBasedOnMultiProjectLoadingValue = Convert.ToDouble(excelTable.Rows[multiProjectRow][2]);
            double multiProjectLoadingPremiumPrice  = Convert.ToDouble(BasePremium * premiumValueBasedOnMultiProjectLoadingValue);
            BasePremium = Convert.ToDouble(BasePremium +  multiProjectLoadingPremiumPrice);
        }

        //Curtain Walling Caluclations 
        public void CurtainWallingCaluclations(plotrows plot)
        {
            var curtainWallingTableRow = -1;
            var curtainWallingRow = -1;
            //Get Has Curtain Walling (Site wide for all products) Table from Structural Premium Table
            for (int i = 0; i < excelTable.Rows.Count; i++)
            {
                if (excelTable.Rows[i][0].ToString().Contains("Has Curtain Walling (Site wide for all products)"))
                {
                    curtainWallingTableRow = i;
                    break;
                }
            }
            if (curtainWallingTableRow == -1)
                throw new Exception("Has Curtain Walling Loading Table Not Found");

            //Has Curtain Walling (Site wide for all products) Table Row from Multi Project Loading Table 
            for (int i = curtainWallingTableRow; i < excelTable.Rows.Count; i++)
            {
                if (excelTable.Rows[i][1].ToString() == "Yes")
                {
                    curtainWallingRow = i;
                    break;
                }
            }
            if (curtainWallingRow == -1)
                throw new Exception("Has Curtain Walling Table Row Not Found");
            double premiumValueBasedOnCurtainWallingValue = Convert.ToDouble(excelTable.Rows[curtainWallingRow][2]);
            double curtainWallingPremiumPrice = Convert.ToDouble(BasePremium * premiumValueBasedOnCurtainWallingValue);
            CurtainWallingPremium = Convert.ToDouble(curtainWallingPremiumPrice);
        }


        //Storey Loading Calucaltions    
        public void StoreyLoadingCaluclations(plotrows plot)
        {
            var storeyloadingTableRow = -1;
            var storeyLoadingRow = -1;
            //Get Storey Loading (Site wide for all products)Table from Structural Premium Table
            for (int i = 0; i < excelTable.Rows.Count; i++)
            {
                if (excelTable.Rows[i][0].ToString().Contains("Storey Loading (Site wide for all products)"))
                {
                    storeyloadingTableRow = i;
                    break;
                }
            }
            if (storeyloadingTableRow == -1)
                throw new Exception("Storey Loading (Site wide for all products) Table Not Found");

            //Storey Loading (Site wide for all products) Table Row from Multi Project Loading Table 
            for (int i = storeyloadingTableRow; i < excelTable.Rows.Count; i++)
            {
                if (excelTable.Rows[i][1].ToString() == "Yes")
                {
                    storeyLoadingRow = i;
                    break;
                }
            }
            if (storeyLoadingRow == -1)
                throw new Exception("Storey Loading (Site wide for all products) Table Row Not Found");
            double premiumValueBasedOnStoryLoadingValue = Convert.ToDouble(excelTable.Rows[storeyLoadingRow][2]);
            double storyLoadingPremiumPrice = Convert.ToDouble(BasePremium * premiumValueBasedOnStoryLoadingValue);
            StoreyLoadingPremium = Convert.ToDouble(storyLoadingPremiumPrice);
        }

        //Basement Loading Caluclations
        public void BasementLoadingCaluclations(plotrows plot)
        {
            var basementLoadngTableRow = -1;
            var basementLoadingRow = -1;
            //Get Basment Loading (Site wide for all products) Table from Structural Premium Table
            for (int i = 0; i < excelTable.Rows.Count; i++)
            {
                if (excelTable.Rows[i][0].ToString().Contains("Basment Loading (Site wide for all products)"))
                {
                    basementLoadngTableRow = i;
                    break;
                }
            }
            if (basementLoadngTableRow == -1)
                throw new Exception("Basment Loading (Site wide for all products) Table Not Found");

            //Multi Basment Loading (Site wide for all products) Table Row from Multi Project Loading Table 
            for (int i = basementLoadngTableRow; i < excelTable.Rows.Count; i++)
            {
                if (excelTable.Rows[i][1].ToString() == "Yes")
                {
                    basementLoadingRow = i;
                    break;
                }
            }
            if (basementLoadingRow == -1)
                throw new Exception("Basment Loading (Site wide for all products) Table Row Not Found");
            double premiumValueBasedOnBasementLoadingValue = Convert.ToDouble(excelTable.Rows[basementLoadingRow][2]);
            double basementLoadingPremiumPrice = Convert.ToDouble(BasePremium * premiumValueBasedOnBasementLoadingValue);
            BasementLoadingPremium = Convert.ToDouble(basementLoadingPremiumPrice);
        }


        public void MultiProjectLoadngDiscountCaluclations(plotrows plot)
        {
            var multiProjectLoadngTableRow = -1;
            var multiProjectLoadingRow = -1;
            //Get Basment Loading (Site wide for all products) Table from Structural Premium Table
            for (int i = 0; i < excelTable.Rows.Count; i++)
            {
                if (excelTable.Rows[i][0].ToString().Contains("Multi Project Loading (Site wide for all products)"))
                {
                    multiProjectLoadngTableRow = i;
                    break;
                }
            }
            if (multiProjectLoadngTableRow == -1)
                throw new Exception("Multi Project Loading (Site wide for all products) Table Not Found");

            //Multi Basment Loading (Site wide for all products) Table Row from Multi Project Loading Table 
            for (int i = multiProjectLoadngTableRow; i < excelTable.Rows.Count; i++)
            {
                if (excelTable.Rows[i][1].ToString() == "Yes")
                {
                    multiProjectLoadingRow = i;
                    break;
                }
            }
            if (multiProjectLoadingRow == -1)
                throw new Exception("Multi Project Loading (Site wide for all products) Table Row Not Found");
            double premiumValueBasedOnMultiProjectLoadingValue = Convert.ToDouble(excelTable.Rows[multiProjectLoadingRow][2]);
            Premiums.MultiProjectDiscountInput = SendQuotePage.MultiProjectDiscount[0].Text;
            if (Premiums.MultiProjectDiscountInput == "Yes")
            {
                double multiProjectDiscount = Convert.ToDouble(BasePremium * premiumValueBasedOnMultiProjectLoadingValue);
                BasePremium = Convert.ToDouble(multiProjectDiscount);
            }
        }
        //Discretionary Discount Caluclations
        public void DiscretionaryDiscountCaluclations(plotrows plot)
        {
            string discretionaryDiscountText = SendQuotePage.DiscretionaryDiscount[0].Text;
            string discretionaryDiscountTextValue = discretionaryDiscountText.Replace("%", "");
            Premiums.DiscretionaryDiscountInput = Convert.ToDouble(discretionaryDiscountTextValue);
            if(Premiums.DiscretionaryDiscountInput > 0.00)
            {
                double discretionaryDiscount = Convert.ToDouble(BasePremium * Premiums.DiscretionaryDiscountInput);
                BasePremium = Convert.ToDouble(BasePremium - discretionaryDiscount);
            }
        }

        //Discretionary Discount Caluclations
        public void StageOfWorksLoadingPercentageCaluclations(plotrows plot)
        {
            string sowPercentageText = SendQuotePage.StageOfWorksPercentage[0].Text;
            string sowPercentageTextValue = sowPercentageText.Replace("%", "");
            Premiums.StageOfWorksLoadingPercentageInput = Convert.ToDouble(sowPercentageTextValue);
            if (Premiums.StageOfWorksLoadingPercentageInput > 0.00)
            {
                double sowPercentage =  Convert.ToDouble(BasePremium * Premiums.StageOfWorksLoadingPercentageInput);
                BasePremium = Convert.ToDouble(BasePremium + sowPercentage);
            }
        }

        //Reading Insolvancy Premium Factor Table 
        public void InsolvencyPremiumFatcor(plotrows plot)
        {
            var insolvencyPremiumTable = -1;
            var insolvencyRow = -1;

            //Get Conversion Stages Of Works Table from Structural Premium Table
            for (int i = 0; i < excelTable.Rows.Count; i++)
            {
                if (excelTable.Rows[i][0].ToString() == "Insolvency Premium")
                {
                    insolvencyPremiumTable = i;
                    break;
                }
            }
            if (insolvencyPremiumTable == -1)
                throw new Exception("Insolvency Premium Factor Table Not Found");
            for (int i = insolvencyPremiumTable; i < excelTable.Rows.Count; i++)
            {
                if (excelTable.Rows[i][1].ToString() == "Rate")
                {
                    insolvencyRow = i;
                    break;
                }
            }
            if (insolvencyRow == -1)
                throw new Exception("Insolvency Premium Factor Value Not Found");
            //Get Insolvancy Premiums Factor  Row 
            double insolvencyValue = Convert.ToDouble(excelTable.Rows[insolvencyRow][RatingColumn]);

            InsolvencyPremium = Convert.ToDouble(plot.ReconstructionCostValue * insolvencyValue);

        }

        //Services Caluclations 
        //TA Fee Caluclations   
        //Basic TAFee Caluclations     
        public void TAFeeCaluclations(plotrows plot)
        {
            // Reading TA Fee Table From Newhomes Pricing Setup Book           
            var TAFeeTableRow = -1;
            var bandingRow = -1;

            //Get TA Fee Table from Structural Premium Table
            for (int i = 0; i < excelTable.Rows.Count; i++)
            {
                if (excelTable.Rows[i][0].ToString() == "Technical Audit Fee")
                {
                    TAFeeTableRow = i;
                    break;
                }
            }
            if (TAFeeTableRow == -1)
                throw new Exception("TAFee Table Not Found");


            //Get TA FEE Banding Row from TA Fee Table
            for (int i = TAFeeTableRow; i < excelTable.Rows.Count; i++)
            {
                if (excelTable.Rows[i][1].ToString() == "Rate")
                {
                    bandingRow = i;
                    break;
                }
            }
            if (bandingRow == -1)
                throw new Exception("TA Fee Banding Row Not Found");

            //Get TA Fee Value From TAFee Table 
            double taFeeValue = Convert.ToDouble(excelTable.Rows[bandingRow][RatingColumn]);
            TAFee = Convert.ToDouble(Banding * taFeeValue);
        }
        //Consumer Code Fee per Plot
        public void ConsumerCodeFeeCaluclations()
        {
            // Reading Consumer Code Fee Table From Newhomes Pricing Setup Book            
            var consumerCodeFeeTableRow = -1;


            //Get Consumer Code Fee per Plot Table from Structural Premium Table
            for (int i = 0; i < excelTable.Rows.Count; i++)
            {
                if (excelTable.Rows[i][0].ToString().Contains("Consumer Code Fee"))
                {
                    consumerCodeFeeTableRow = i;
                    break;
                }
            }
            if (consumerCodeFeeTableRow == -1)
                throw new Exception("Consumer Code Fee per Plot Table Not Found");

            double consumerCodeFeeValue = Convert.ToDouble(excelTable.Rows[consumerCodeFeeTableRow + 2][1]);
            ConsumerCodeFee = Convert.ToDouble(consumerCodeFeeValue);

        }
        public void AtkinsFeeCaluclations()
        {
            // Reading Consumer Code Fee Table From Newhomes Pricing Setup Book            
            var atkinsPremiumTableRow = -1;


            //Get Consumer Code Fee per Plot Table from Structural Premium Table
            for (int i = 0; i < excelTable.Rows.Count; i++)
            {
                if (excelTable.Rows[i][0].ToString().Contains("Atkins Premium"))
                {
                    atkinsPremiumTableRow = i;
                    break;
                }
            }
            if (atkinsPremiumTableRow == -1)
                throw new Exception("Atkins Fee Table Not Found");

            double atkinsFeeValue = Convert.ToDouble(excelTable.Rows[atkinsPremiumTableRow + 2][2]);
            AtkinsPremium = Convert.ToDouble(atkinsFeeValue * 12);

        }
        public void MinimumAtkinsFeeCaluclations()
        {
            // Reading Consumer Code Fee Table From Newhomes Pricing Setup Book            
            var minimumAtkinsPremiumTableRow = -1;


            //Get Consumer Code Fee per Plot Table from Structural Premium Table
            for (int i = 0; i < excelTable.Rows.Count; i++)
            {
                if (excelTable.Rows[i][0].ToString().Contains("Atkins Premium"))
                {
                    minimumAtkinsPremiumTableRow = i;
                    break;
                }
            }
            if (minimumAtkinsPremiumTableRow == -1)
                throw new Exception("Consumer Code Fee per Plot Table Not Found");

            double atkinsFeeValue = Convert.ToDouble(excelTable.Rows[minimumAtkinsPremiumTableRow + 4][2]);
            MinimumAtkinsFee = Convert.ToDouble(atkinsFeeValue);

        }

        public void VerfiyNewhomesUIValues()
        {
            var productName = Premiums.eachplotrow.FirstOrDefault().ProductName;
            //Covers Elements 
            //Veridy UI Structural Values
            var uiStructuralFee = $"//th[@class='product-title'][text()='{productName}']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[text()='Structural']//following-sibling::td[@class='right-align'][1]";
            var StructuralFee = Driver.FindElement(By.XPath(uiStructuralFee)).Text.Replace("£", "");
            double UIStructuralFee = Math.Round(Convert.ToDouble(StructuralFee), 2);
            double StructuralFeeDiff;
            var uiStructuralIPTFee = $"//th[@class='product-title'][text()='{productName}']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[text()='Structural']//following-sibling::td[@class='right-align'][2]";
            var StructuralIPTFee = Driver.FindElement(By.XPath(uiStructuralIPTFee)).Text.Replace("£", "");
            double UIStructuralIPTFee = Math.Round(Convert.ToDouble(StructuralIPTFee), 2);
            double StructuralIPTDiff;
            var uiStructuralTotalFee = $"//th[@class='product-title'][text()='{productName}']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[text()='Structural']//following-sibling::th[@class='right-align'][1]";
            var StructuralTotalFee = Driver.FindElement(By.XPath(uiStructuralTotalFee)).Text.Replace("£", "");
            double UIStructuralTotalFee = Math.Round(Convert.ToDouble(StructuralTotalFee), 2);
            double StructuralTotalValue = Math.Round(Convert.ToDouble(UIStructuralFee + UIStructuralIPTFee), 2);
            double StructuralTotalDiff;

            if (UIStructuralFee != Premiums.StructuralFee || UIStructuralIPTFee != Premiums.StructuralIPTFee || UIStructuralTotalFee != StructuralTotalValue)
            {
                StructuralFeeDiff = Convert.ToDouble(Premiums.StructuralFee - UIStructuralFee);
                StructuralIPTDiff = Convert.ToDouble(Premiums.StructuralIPTFee - UIStructuralIPTFee);
                StructuralTotalDiff = Convert.ToDouble(StructuralTotalValue - UIStructuralTotalFee);
                if (StructuralFeeDiff >= -1.01 && StructuralFeeDiff <= 1.01)
                {
                    Premiums.StructuralFee = UIStructuralFee;
                }
                if (StructuralIPTDiff >= -1.01 && StructuralIPTDiff <= 1.01)
                {
                    Premiums.StructuralIPTFee = UIStructuralIPTFee;
                }
                if (StructuralTotalDiff >= -1.01 && StructuralTotalDiff <= 1.01)
                {
                    StructuralTotalValue = UIStructuralTotalFee;
                }

                Assert.AreEqual(UIStructuralFee, Premiums.StructuralFee, $"StructuralFee Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.EstimatedSalePrice}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                Assert.AreEqual(UIStructuralIPTFee, Premiums.StructuralIPTFee, $"StructuralIPTFee Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.EstimatedSalePrice}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                Assert.AreEqual(UIStructuralTotalFee, StructuralTotalValue, $"StructuralTotalFee Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.EstimatedSalePrice}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
            }
            else
            {

                Assert.AreEqual(UIStructuralFee, Premiums.StructuralFee, $"StructuralFee Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.EstimatedSalePrice}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                Assert.AreEqual(UIStructuralIPTFee, Premiums.StructuralIPTFee, $"StructuralIPTFee Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.EstimatedSalePrice}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                Assert.AreEqual(UIStructuralTotalFee, StructuralTotalValue, $"StructuralTotalFee Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.EstimatedSalePrice}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
            }
            if (Premiums.ProductsCovers.Any(x => x.Contains("Insolvency of the Developer")))
            {
                //Veirfy UI Contaminated Land values
                var uiInsolvencyFee = $"//th[@class='product-title'][text()='{productName}']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[text()='Insolvency of the Developer']//following-sibling::td[@class='right-align'][1]";
                var InsolvencyFee = Driver.FindElement(By.XPath(uiInsolvencyFee)).Text.Replace("£", "");
                double UIInsolvencyFee = Math.Round(Convert.ToDouble(InsolvencyFee), 2);
                double InsolvencyFeeDiff;
                var uiInsolvencyIPTFee = $"//th[@class='product-title'][text()='{productName}']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[text()='Insolvency of the Developer']//following-sibling::td[@class='right-align'][2]";
                var InsolvencyIPTFee = Driver.FindElement(By.XPath(uiInsolvencyIPTFee)).Text.Replace("£", "");
                double UIInsolvencyIPTFee = Math.Round(Convert.ToDouble(InsolvencyIPTFee), 2);
                double InsolvencyIPTDiff;
                var uiInsolvencyTotalFee = $"//th[@class='product-title'][text()='{productName}']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[text()='Insolvency of the Developer']//following-sibling::th[@class='right-align'][1]";
                var InsolvencyTotalFee = Driver.FindElement(By.XPath(uiInsolvencyTotalFee)).Text.Replace("£", "");
                double UIInsolvencyTotalFee = Math.Round(Convert.ToDouble(InsolvencyTotalFee), 2);
                double InsolvencyTotalValue = Math.Round(Convert.ToDouble(UIInsolvencyFee + UIInsolvencyIPTFee), 2);
                double InsolvencyTotalDiff;

                if (UIInsolvencyFee != Premiums.InsolvencyFee || UIInsolvencyIPTFee != Premiums.InsolvencyIPTFee || UIInsolvencyTotalFee != InsolvencyTotalValue)
                {
                    InsolvencyFeeDiff = Convert.ToDouble(Premiums.InsolvencyFee - UIInsolvencyFee);
                    InsolvencyIPTDiff = Convert.ToDouble(Premiums.InsolvencyIPTFee - UIInsolvencyIPTFee);
                    InsolvencyTotalDiff = Convert.ToDouble(InsolvencyTotalValue - UIInsolvencyTotalFee);
                    if (InsolvencyFeeDiff >= -1.01 && InsolvencyFeeDiff <= 1.01)
                    {
                        Premiums.InsolvencyFee = UIInsolvencyFee;
                    }
                    if (InsolvencyIPTDiff >= -1.01 && InsolvencyIPTDiff <= 1.01)
                    {
                        Premiums.InsolvencyIPTFee = UIInsolvencyIPTFee;
                    }
                    if (InsolvencyTotalDiff >= -1.01 && InsolvencyTotalDiff <= 1.01)
                    {
                        InsolvencyTotalValue = UIInsolvencyTotalFee;
                    }

                    Assert.AreEqual(UIInsolvencyFee, Premiums.InsolvencyFee, $"InsolvencyFee Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.EstimatedSalePrice}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UIInsolvencyIPTFee, Premiums.InsolvencyIPTFee, $"InsolvencyIPTFee Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.EstimatedSalePrice}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UIInsolvencyTotalFee, InsolvencyTotalValue, $"InsolvencyTotalFee Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.EstimatedSalePrice}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                }
                else
                {
                    Assert.AreEqual(UIInsolvencyFee, Premiums.InsolvencyFee, $"InsolvencyFee Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.EstimatedSalePrice}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UIInsolvencyIPTFee, Premiums.InsolvencyIPTFee, $"InsolvencyIPTFee Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.EstimatedSalePrice}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UIInsolvencyTotalFee, InsolvencyTotalValue, $"InsolvencyTotalFee Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.EstimatedSalePrice}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                }
            }
            if (Premiums.ProductsCovers.Any(x => x.Contains("Defects")))
            {
                //Veirfy UI Contaminated Land values
                var uiDefectsFee = $"//th[@class='product-title'][text()='{productName}']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[text()='Defects']//following-sibling::td[@class='right-align'][1]";
                var DefectsFee = Driver.FindElement(By.XPath(uiDefectsFee)).Text.Replace("£", "");
                double UIDefectsFee = Math.Round(Convert.ToDouble(DefectsFee), 2);
                double DefectsFeeDiff;
                var uiDefectsIPTFee = $"//th[@class='product-title'][text()='{productName}']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[text()='Defects']//following-sibling::td[@class='right-align'][2]";
                var DefectsIPTFee = Driver.FindElement(By.XPath(uiDefectsIPTFee)).Text.Replace("£", "");
                double UIDefectsIPTFee = Math.Round(Convert.ToDouble(DefectsIPTFee), 2);
                double DefectsIPTDiff;
                var uiDefectsTotalFee = $"//th[@class='product-title'][text()='{productName}']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[text()='Defects']//following-sibling::th[@class='right-align'][1]";
                var DefectsTotalFee = Driver.FindElement(By.XPath(uiDefectsTotalFee)).Text.Replace("£", "");
                double UIDefectsTotalFee = Math.Round(Convert.ToDouble(DefectsTotalFee), 2);
                double DefectsTotalValue = Math.Round(Convert.ToDouble(UIDefectsFee + UIDefectsIPTFee), 2);
                double DefectsTotalDiff;

                if (UIDefectsFee != Premiums.DefectsFee || UIDefectsIPTFee != Premiums.DefectsIPTFee || UIDefectsTotalFee != DefectsTotalValue)
                {
                    DefectsFeeDiff = Convert.ToDouble(Premiums.DefectsFee - UIDefectsFee);
                    DefectsIPTDiff = Convert.ToDouble(Premiums.DefectsIPTFee - UIDefectsIPTFee);
                    DefectsTotalDiff = Convert.ToDouble(DefectsTotalValue - UIDefectsTotalFee);
                    if (DefectsFeeDiff >= -1.01 && DefectsFeeDiff <= 1.01)
                    {
                        Premiums.DefectsFee = UIDefectsFee;
                    }
                    if (DefectsIPTDiff >= -1.01 && DefectsIPTDiff <= 1.01)
                    {
                        Premiums.DefectsIPTFee = UIDefectsIPTFee;
                    }
                    if (DefectsTotalDiff >= -1.01 && DefectsTotalDiff <= 1.01)
                    {
                        DefectsTotalValue = UIDefectsTotalFee;
                    }

                    Assert.AreEqual(UIDefectsFee, Premiums.DefectsFee, $"DefectsFee Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.EstimatedSalePrice}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UIDefectsIPTFee, Premiums.DefectsIPTFee, $"DefectsIPTFee Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.EstimatedSalePrice}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UIDefectsTotalFee, DefectsTotalValue, $"DefectsTotalFee Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.EstimatedSalePrice}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                }
                else
                {
                    Assert.AreEqual(UIDefectsFee, Premiums.DefectsFee, $"DefectsFee Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.EstimatedSalePrice}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UIDefectsIPTFee, Premiums.DefectsIPTFee, $"DefectsIPTFee Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.EstimatedSalePrice}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UIDefectsTotalFee, DefectsTotalValue, $"DefectsTotalFee Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.EstimatedSalePrice}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                }
            }
            if (Premiums.ProductsCovers.Any(x => x.Contains("Contaminated Land")))
            {
                //Veirfy UI Contaminated Land values
                var uiContaminatedLandFee = $"//th[@class='product-title'][text()='{productName}']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[text()='Contaminated Land']//following-sibling::td[@class='right-align'][1]";
                var ContaminatedLandFee = Driver.FindElement(By.XPath(uiContaminatedLandFee)).Text.Replace("£", "");
                double UIContaminatedLandFee = Math.Round(Convert.ToDouble(ContaminatedLandFee), 2);
                double ContaminatedLandFeeDiff;
                var uiContaminatedLandIPTFee = $"//th[@class='product-title'][text()='{productName}']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[text()='Contaminated Land']//following-sibling::td[@class='right-align'][2]";
                var ContaminatedLandIPTFee = Driver.FindElement(By.XPath(uiContaminatedLandIPTFee)).Text.Replace("£", "");
                double UIContaminatedLandIPTFee = Math.Round(Convert.ToDouble(ContaminatedLandIPTFee), 2);
                double ContaminatedLandIPTDiff;
                var uiContaminatedLandTotalFee = $"//th[@class='product-title'][text()='{productName}']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[text()='Contaminated Land']//following-sibling::th[@class='right-align'][1]";
                var ContaminatedLandTotalFee = Driver.FindElement(By.XPath(uiContaminatedLandTotalFee)).Text.Replace("£", "");
                double UIContaminatedLandTotalFee = Math.Round(Convert.ToDouble(ContaminatedLandTotalFee), 2);
                double ContaminatedLandTotalValue = Math.Round(Convert.ToDouble(UIContaminatedLandFee + UIContaminatedLandIPTFee), 2);
                double ContaminatedLandTotalDiff;

                if (UIContaminatedLandFee != Premiums.ContaminatedLandFee || UIContaminatedLandIPTFee != Premiums.ContaminatedLandIPTFee || UIContaminatedLandTotalFee != ContaminatedLandTotalValue)
                {
                    ContaminatedLandFeeDiff = Convert.ToDouble(Premiums.ContaminatedLandFee - UIContaminatedLandFee);
                    ContaminatedLandIPTDiff = Convert.ToDouble(Premiums.ContaminatedLandIPTFee - UIContaminatedLandIPTFee);
                    ContaminatedLandTotalDiff = Convert.ToDouble(ContaminatedLandTotalValue - UIContaminatedLandTotalFee);
                    if (ContaminatedLandFeeDiff >= -1.01 && ContaminatedLandFeeDiff <= 1.01)
                    {
                        Premiums.ContaminatedLandFee = UIContaminatedLandFee;
                    }
                    if (ContaminatedLandIPTDiff >= -1.01 && ContaminatedLandIPTDiff <= 1.01)
                    {
                        Premiums.ContaminatedLandIPTFee = UIContaminatedLandIPTFee;
                    }
                    if (ContaminatedLandTotalDiff >= -1.01 && ContaminatedLandTotalDiff <= 1.01)
                    {
                        ContaminatedLandTotalValue = UIContaminatedLandTotalFee;
                    }

                    Assert.AreEqual(UIContaminatedLandFee, Premiums.ContaminatedLandFee, $"ContaminatedLandFee Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.EstimatedSalePrice}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UIContaminatedLandIPTFee, Premiums.ContaminatedLandIPTFee, $"ContaminatedLandIPTFee Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.EstimatedSalePrice}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UIContaminatedLandTotalFee, ContaminatedLandTotalValue, $"ContaminatedLandTotalFee Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.EstimatedSalePrice}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                }
                else
                {
                    Assert.AreEqual(UIContaminatedLandFee, Premiums.ContaminatedLandFee, $"ContaminatedLandFee Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.EstimatedSalePrice}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UIContaminatedLandIPTFee, Premiums.ContaminatedLandIPTFee, $"ContaminatedLandIPTFee Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.EstimatedSalePrice}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UIContaminatedLandTotalFee, ContaminatedLandTotalValue, $"ContaminatedLandTotalFee Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.EstimatedSalePrice}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                }
            }

            //verify Additional Cover For Local Authoriy Building Control function UIVlues
            if (Premiums.ProductsCovers.Any(x => x.Contains("Additional Cover for Local Authority Building Control Function")))
            {

                //Verify UI Local Authority Building Control Values
                var uiLocalAuthorityBuildingControlFee = $"//th[@class='product-title'][text()='{productName}']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[text()='Additional Cover for Local Authority Building Control Function']//following-sibling::td[@class='right-align'][1]";
                var LocalAuthorityBuildingControlFee = Driver.FindElement(By.XPath(uiLocalAuthorityBuildingControlFee)).Text.Replace("£", "");
                double UILocalAuthorityBuildingControlFee = Math.Round(Convert.ToDouble(LocalAuthorityBuildingControlFee), 2);
                double LocalAuthorityBuildingControlFeeDiff;

                var uiLocalAuthorityBuildingControlIPTFee = $"//th[@class='product-title'][text()='{productName}']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[text()='Additional Cover for Local Authority Building Control Function']//following-sibling::td[@class='right-align'][2]";
                var LocalAuthorityBuildingControlIPTFee = Driver.FindElement(By.XPath(uiLocalAuthorityBuildingControlIPTFee)).Text.Replace("£", "");
                double UILocalAuthorityBuildingControlIPTFee = Math.Round(Convert.ToDouble(LocalAuthorityBuildingControlIPTFee), 2);
                double LocalAuthorityBuildingControlIPTDiff;

                var uiLocalAuthorityBuildingControlTotalFee = $"//th[@class='product-title'][text()='{productName}']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[text()='Additional Cover for Local Authority Building Control Function']//following-sibling::th[@class='right-align'][1]";
                var LocalAuthorityBuildingControlTotalFee = Driver.FindElement(By.XPath(uiLocalAuthorityBuildingControlTotalFee)).Text.Replace("£", "");
                double UILocalAuthorityBuildingControlTotalFee = Math.Round(Convert.ToDouble(LocalAuthorityBuildingControlTotalFee), 2);
                double LocalAuthorityBuildingControlTotalValue = Math.Round(Convert.ToDouble(UILocalAuthorityBuildingControlFee + UILocalAuthorityBuildingControlIPTFee), 2);
                double LocalAuthorityBuildingControlTotalDiff;

                if (UILocalAuthorityBuildingControlFee != Premiums.LocalAuthorityBuildingControlFee || UILocalAuthorityBuildingControlIPTFee != Premiums.LocalAuthorityBuildingControlIPTFee || UILocalAuthorityBuildingControlTotalFee != LocalAuthorityBuildingControlTotalValue)
                {
                    LocalAuthorityBuildingControlFeeDiff = Convert.ToDouble(Premiums.LocalAuthorityBuildingControlFee - UILocalAuthorityBuildingControlFee);
                    LocalAuthorityBuildingControlIPTDiff = Convert.ToDouble(Premiums.LocalAuthorityBuildingControlIPTFee - UILocalAuthorityBuildingControlIPTFee);
                    LocalAuthorityBuildingControlTotalDiff = Convert.ToDouble(LocalAuthorityBuildingControlTotalValue - UILocalAuthorityBuildingControlTotalFee);
                    if (LocalAuthorityBuildingControlFeeDiff >= -1.01 && LocalAuthorityBuildingControlFeeDiff <= 1.01)
                    {
                        Premiums.LocalAuthorityBuildingControlFee = UILocalAuthorityBuildingControlFee;
                    }
                    if (LocalAuthorityBuildingControlIPTDiff >= -1.01 && LocalAuthorityBuildingControlIPTDiff <= 1.01)
                    {
                        Premiums.LocalAuthorityBuildingControlIPTFee = UILocalAuthorityBuildingControlIPTFee;
                    }
                    if (LocalAuthorityBuildingControlTotalDiff >= -1.01 && LocalAuthorityBuildingControlTotalDiff <= 1.01)
                    {
                        LocalAuthorityBuildingControlTotalValue = UILocalAuthorityBuildingControlTotalFee;
                    }

                    Assert.AreEqual(UILocalAuthorityBuildingControlFee, Premiums.LocalAuthorityBuildingControlFee, $"LocalAuthorityBuildingControlFee Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UILocalAuthorityBuildingControlIPTFee, Premiums.LocalAuthorityBuildingControlIPTFee, $"LocalAuthorityBuildingControlIPTFee Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UILocalAuthorityBuildingControlTotalFee, LocalAuthorityBuildingControlTotalValue, $"LocalAuthorityBuildingControlTotalFee Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                }
                else
                {
                    Assert.AreEqual(UILocalAuthorityBuildingControlFee, Premiums.LocalAuthorityBuildingControlFee, $"LocalAuthorityBuildingControlFee Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UILocalAuthorityBuildingControlIPTFee, Premiums.LocalAuthorityBuildingControlIPTFee, $"LocalAuthorityBuildingControlIPTFee Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UILocalAuthorityBuildingControlTotalFee, LocalAuthorityBuildingControlTotalValue, $"LocalAuthorityBuildingControlTotalFee Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                }

            }
            //verify Additional Cover For Approved Inspectoe Building Control Function UIVlues
            if (Premiums.ProductsCovers.Any(x => x.Contains("Additional Cover for Approved Inspector Building Control Function")))
            {

                //Verify UI Local Authority Building Control Values
                var uiApprovedInspectorBuildingControlFee = $"//th[@class='product-title'][text()='{productName}']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[text()='Additional Cover for Approved Inspector Building Control Function']//following-sibling::td[@class='right-align'][1]";
                var ApprovedInspectorBuildingControlFee = Driver.FindElement(By.XPath(uiApprovedInspectorBuildingControlFee)).Text.Replace("£", "");
                double UIApprovedInspectorBuildingControlFee = Math.Round(Convert.ToDouble(ApprovedInspectorBuildingControlFee), 2);
                double ApprovedInspectorBuildingControlFeeDiff;

                var uiApprovedInspectorBuildingControlIPTFee = $"//th[@class='product-title'][text()='{productName}']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[text()='Additional Cover for Approved Inspector Building Control Function']//following-sibling::td[@class='right-align'][2]";
                var ApprovedInspectorBuildingControlIPTFee = Driver.FindElement(By.XPath(uiApprovedInspectorBuildingControlIPTFee)).Text.Replace("£", "");
                double UIApprovedInspectorBuildingControlIPTFee = Math.Round(Convert.ToDouble(ApprovedInspectorBuildingControlIPTFee), 2);
                double ApprovedInspectorBuildingControlIPTDiff;

                var uiApprovedInspectorBuildingControlTotalFee = $"//th[@class='product-title'][text()='{productName}']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[text()='Additional Cover for Approved Inspector Building Control Function']//following-sibling::th[@class='right-align'][1]";
                var ApprovedInspectorBuildingControlTotalFee = Driver.FindElement(By.XPath(uiApprovedInspectorBuildingControlTotalFee)).Text.Replace("£", "");
                double UIApprovedInspectorBuildingControlTotalFee = Math.Round(Convert.ToDouble(ApprovedInspectorBuildingControlTotalFee), 2);
                double ApprovedInspectorBuildingControlTotalValue = Math.Round(Convert.ToDouble(UIApprovedInspectorBuildingControlFee + UIApprovedInspectorBuildingControlIPTFee), 2);
                double ApprovedInspectorBuildingControlTotalDiff;

                if (UIApprovedInspectorBuildingControlFee != Premiums.ApprovedInspectorBuildingControlFee || UIApprovedInspectorBuildingControlIPTFee != Premiums.ApprovedInspectorBuildingControlIPTFee || UIApprovedInspectorBuildingControlTotalFee != ApprovedInspectorBuildingControlTotalValue)
                {
                    ApprovedInspectorBuildingControlFeeDiff = Convert.ToDouble(Premiums.ApprovedInspectorBuildingControlFee - UIApprovedInspectorBuildingControlFee);
                    ApprovedInspectorBuildingControlIPTDiff = Convert.ToDouble(Premiums.ApprovedInspectorBuildingControlIPTFee - UIApprovedInspectorBuildingControlIPTFee);
                    ApprovedInspectorBuildingControlTotalDiff = Convert.ToDouble(ApprovedInspectorBuildingControlTotalValue - UIApprovedInspectorBuildingControlTotalFee);
                    if (ApprovedInspectorBuildingControlFeeDiff >= -1.01 && ApprovedInspectorBuildingControlFeeDiff <= 1.01)
                    {
                        Premiums.ApprovedInspectorBuildingControlFee = UIApprovedInspectorBuildingControlFee;
                    }
                    if (ApprovedInspectorBuildingControlIPTDiff >= -1.01 && ApprovedInspectorBuildingControlIPTDiff <= 1.01)
                    {
                        Premiums.ApprovedInspectorBuildingControlIPTFee = UIApprovedInspectorBuildingControlIPTFee;
                    }
                    if (ApprovedInspectorBuildingControlTotalDiff >= -1.01 && ApprovedInspectorBuildingControlTotalDiff <= 1.01)
                    {
                        ApprovedInspectorBuildingControlTotalValue = UIApprovedInspectorBuildingControlTotalFee;
                    }

                    Assert.AreEqual(UIApprovedInspectorBuildingControlFee, Premiums.ApprovedInspectorBuildingControlFee, $"ApprovedInspectorBuildingControlFee Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UIApprovedInspectorBuildingControlIPTFee, Premiums.ApprovedInspectorBuildingControlIPTFee, $"ApprovedInspectorBuildingControlIPTFee Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UIApprovedInspectorBuildingControlTotalFee, ApprovedInspectorBuildingControlTotalValue, $"ApprovedInspectorBuildingControlTotalFee Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                }
                else
                {
                    Assert.AreEqual(UIApprovedInspectorBuildingControlFee, Premiums.ApprovedInspectorBuildingControlFee, $"ApprovedInspectorBuildingControlFee Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UIApprovedInspectorBuildingControlIPTFee, Premiums.ApprovedInspectorBuildingControlIPTFee, $"ApprovedInspectorBuildingControlIPTFee Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UIApprovedInspectorBuildingControlTotalFee, ApprovedInspectorBuildingControlTotalValue, $"ApprovedInspectorBuildingControlTotalFee Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                }

            }


            //Services Elements  and  Fee values  
            //Technical Audit Fee values  
            if (Premiums.ProductServices.Any(x => x.Contains("Technical Audit Fee")))
            {
                var uiTechnicalAuditFee = $"//th[@class='product-title'][text()='{productName}']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[text()='Technical Audit Fee']//following-sibling::td[@class='right-align'][1]";
                var TechnicalAuditFee = Driver.FindElement(By.XPath(uiTechnicalAuditFee)).Text.Replace("£", "");
                double UITechnicalAuditFee = Math.Round(Convert.ToDouble(TechnicalAuditFee), 2);
                double TechnicalAuditFeeDiff;

                var uiTechnicalAuditVATFee = $"//th[@class='product-title'][text()='{productName}']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[text()='Technical Audit Fee']//following-sibling::td[@class='right-align'][2]";
                var TechnicalAuditVATFee = Driver.FindElement(By.XPath(uiTechnicalAuditVATFee)).Text.Replace("£", "");
                double UITechnicalAuditVATFee = Math.Round(Convert.ToDouble(TechnicalAuditVATFee), 2);
                double TechnicalAuditVATDiff;

                var uiTechnicalAuditTotalFee = $"//th[@class='product-title'][text()='{productName}']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[text()='Technical Audit Fee']//following-sibling::th[@class='right-align'][1]";
                var TechnicalAuditTotalFee = Driver.FindElement(By.XPath(uiTechnicalAuditTotalFee)).Text.Replace("£", "");
                double UITechnicalAuditTotalFee = Math.Round(Convert.ToDouble(TechnicalAuditTotalFee), 2);
                double TechnicalAuditTotalValue = Math.Round(Convert.ToDouble(UITechnicalAuditFee + UITechnicalAuditVATFee), 2);
                double TechnicalAuditTotalDiff;

                if (UITechnicalAuditFee != Premiums.TechnicalAuditFee || UITechnicalAuditVATFee != Premiums.TechnicalAuditVATFee || UITechnicalAuditTotalFee != TechnicalAuditTotalValue)
                {
                    TechnicalAuditFeeDiff = Convert.ToDouble(Premiums.TechnicalAuditFee - UITechnicalAuditFee);
                    TechnicalAuditVATDiff = Convert.ToDouble(Premiums.TechnicalAuditVATFee - UITechnicalAuditVATFee);
                    TechnicalAuditTotalDiff = Convert.ToDouble(TechnicalAuditTotalValue - UITechnicalAuditTotalFee);
                    if (TechnicalAuditFeeDiff >= -1.01 && TechnicalAuditFeeDiff <= 1.01)
                    {
                        Premiums.TechnicalAuditFee = UITechnicalAuditFee;
                    }

                    if (TechnicalAuditVATDiff >= -1.01 && TechnicalAuditVATDiff <= 1.01)
                    {
                        Premiums.TechnicalAuditVATFee = UITechnicalAuditVATFee;
                    }
                    if (TechnicalAuditVATDiff >= -1.01 && TechnicalAuditTotalDiff <= 1.01)
                    {
                        TechnicalAuditTotalValue = UITechnicalAuditTotalFee;
                    }

                    Assert.AreEqual(UITechnicalAuditFee, Premiums.TechnicalAuditFee, $"TechnicalAuditFee Services Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.EstimatedSalePrice}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UITechnicalAuditVATFee, Premiums.TechnicalAuditVATFee, $"TechnicalAuditVATFee Services Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.EstimatedSalePrice}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UITechnicalAuditTotalFee, TechnicalAuditTotalValue, $"TechnicalAuditTotalValue Services Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.EstimatedSalePrice}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                }
                else
                {
                    Assert.AreEqual(UITechnicalAuditFee, Premiums.TechnicalAuditFee, $"TechnicalAuditFee Services Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.EstimatedSalePrice}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UITechnicalAuditVATFee, Premiums.TechnicalAuditVATFee, $"TechnicalAuditVATFee Services Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.EstimatedSalePrice}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UITechnicalAuditTotalFee, TechnicalAuditTotalValue, $"TechnicalAuditTotalValue Services Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.EstimatedSalePrice}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");

                }
            }
            //ConsumerCode Fee values 

            if (Premiums.ProductServices.Any(x => x.Contains("Consumer Code Fee")))
            {
                var uiConsumerCodeFee = $"//th[@class='product-title'][text()='{productName}']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[text()='Consumer Code Fee']//following-sibling::td[@class='right-align'][1]";
                var ConsumerCodeFee = Driver.FindElement(By.XPath(uiConsumerCodeFee)).Text.Replace("£", "");
                double UIConsumerCodeFee = Math.Round(Convert.ToDouble(ConsumerCodeFee), 2);
                double ConsumerCodeFeeDiff;

                var uiConsumerCodeVATFee = $"//th[@class='product-title'][text()='{productName}']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[text()='Consumer Code Fee']//following-sibling::td[@class='right-align'][2]";
                var ConsumerCodeVATFee = Driver.FindElement(By.XPath(uiConsumerCodeVATFee)).Text.Replace("£", "");
                double UIConsumerCodeVATFee = Math.Round(Convert.ToDouble(ConsumerCodeVATFee), 2);
                double ConsumerCodeVATDiff;

                var uiConsumerCodeTotalFee = $"//th[@class='product-title'][text()='{productName}']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[text()='Consumer Code Fee']//following-sibling::th[@class='right-align'][1]";
                var ConsumerCodeTotalFee = Driver.FindElement(By.XPath(uiConsumerCodeTotalFee)).Text.Replace("£", "");
                double UIConsumerCodeTotalFee = Math.Round(Convert.ToDouble(ConsumerCodeTotalFee), 2);
                double ConsumerCodeTotalValue = Math.Round(Convert.ToDouble(UIConsumerCodeFee + UIConsumerCodeVATFee), 2);
                double ConsumerCodeTotalDiff;


                if (UIConsumerCodeFee != Premiums.ConsumerCodeFee || UIConsumerCodeVATFee != Premiums.ConsumerCodeVATFee || UIConsumerCodeTotalFee != ConsumerCodeTotalValue)
                {
                    ConsumerCodeFeeDiff = Convert.ToDouble(Premiums.ConsumerCodeFee - UIConsumerCodeFee);
                    ConsumerCodeVATDiff = Convert.ToDouble(Premiums.ConsumerCodeVATFee - UIConsumerCodeVATFee);
                    ConsumerCodeTotalDiff = Convert.ToDouble(ConsumerCodeTotalValue - UIConsumerCodeTotalFee);
                    if (ConsumerCodeFeeDiff >= -1.01 && ConsumerCodeFeeDiff <= 1.01)
                    {
                        Premiums.ConsumerCodeFee = UIConsumerCodeFee;
                    }

                    if (ConsumerCodeVATDiff >= -1.01 && ConsumerCodeVATDiff <= 1.01)
                    {
                        Premiums.ConsumerCodeVATFee = UIConsumerCodeVATFee;
                    }
                    if (ConsumerCodeTotalDiff >= -1.01 && ConsumerCodeTotalDiff <= 1.01)
                    {
                        ConsumerCodeTotalValue = UIConsumerCodeTotalFee;
                    }

                    Assert.AreEqual(UIConsumerCodeFee, Premiums.ConsumerCodeFee, $"ConsumerCodeFee Services Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.EstimatedSalePrice}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UIConsumerCodeVATFee, Premiums.ConsumerCodeVATFee, $"ConsumerCodeVATFee Services Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.EstimatedSalePrice}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UIConsumerCodeTotalFee, ConsumerCodeTotalValue, $"ConsumerCodeTotalValue Services Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.EstimatedSalePrice}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                }
                else
                {
                    Assert.AreEqual(UIConsumerCodeFee, Premiums.ConsumerCodeFee, $"ConsumerCodeFee Services Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.EstimatedSalePrice}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UIConsumerCodeVATFee, Premiums.ConsumerCodeVATFee, $"ConsumerCodeVATFee Services Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.EstimatedSalePrice}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UIConsumerCodeTotalFee, ConsumerCodeTotalValue, $"ConsumerCodeTotalValue Services Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.EstimatedSalePrice}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                }
            }
            //Refurbishment Assessment Fee values  
            if (Premiums.ProductServices.Any(x => x.Contains("Refurbishment Assessment Fee")))
            {
                var uiRefurbishmentFee = $"//th[@class='product-title'][text()='{productName}']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[text()='Refurbishment Assessment Fee']//following-sibling::td[@class='right-align'][1]";
                var RefurbishmentFee = Driver.FindElement(By.XPath(uiRefurbishmentFee)).Text.Replace("£", "");
                double UIRefurbishmentFee = Math.Round(Convert.ToDouble(RefurbishmentFee), 2);
                double RefurbishmentFeeDiff;

                var uiRefurbishmentVATFee = $"//th[@class='product-title'][text()='{productName}']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[text()='Refurbishment Assessment Fee']//following-sibling::td[@class='right-align'][2]";
                var RefurbishmentVATFee = Driver.FindElement(By.XPath(uiRefurbishmentVATFee)).Text.Replace("£", "");
                double UIRefurbishmentVATFee = Math.Round(Convert.ToDouble(RefurbishmentVATFee), 2);
                double RefurbishmentVATDiff;

                var uiRefurbishmentTotalFee = $"//th[@class='product-title'][text()='{productName}']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[text()='Refurbishment Assessment Fee']//following-sibling::th[@class='right-align'][1]";
                var RefurbishmentTotalFee = Driver.FindElement(By.XPath(uiRefurbishmentTotalFee)).Text.Replace("£", "");
                double UIRefurbishmentTotalFee = Math.Round(Convert.ToDouble(RefurbishmentTotalFee), 2);
                double RefurbishmentTotalValue = Math.Round(Convert.ToDouble(UIRefurbishmentFee + UIRefurbishmentVATFee), 2);
                double RefurbishmentTotalDiff;


                if (UIRefurbishmentFee != Premiums.RefurbishmentAssessmentFee || UIRefurbishmentVATFee != Premiums.RefurbishmentAssessmentVATFee || UIRefurbishmentTotalFee != RefurbishmentTotalValue)
                {
                    RefurbishmentFeeDiff = Convert.ToDouble(Premiums.RefurbishmentAssessmentFee - UIRefurbishmentFee);
                    RefurbishmentVATDiff = Convert.ToDouble(Premiums.RefurbishmentAssessmentVATFee - UIRefurbishmentVATFee);
                    RefurbishmentTotalDiff = Convert.ToDouble(RefurbishmentTotalValue - UIRefurbishmentTotalFee);
                    if (RefurbishmentFeeDiff >= -1.01 && RefurbishmentFeeDiff <= 1.01)
                    {
                        Premiums.RefurbishmentAssessmentFee = UIRefurbishmentFee;
                    }

                    if (RefurbishmentVATDiff >= -1.01 && RefurbishmentVATDiff <= 1.01)
                    {
                        Premiums.RefurbishmentAssessmentVATFee = UIRefurbishmentVATFee;
                    }
                    if (RefurbishmentTotalDiff >= -1.01 && RefurbishmentTotalDiff <= 1.01)
                    {
                        RefurbishmentTotalValue = UIRefurbishmentTotalFee;
                    }

                    Assert.AreEqual(UIRefurbishmentFee, Premiums.RefurbishmentAssessmentFee, $"RefurbishmentFee Services Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.EstimatedSalePrice}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UIRefurbishmentVATFee, Premiums.RefurbishmentAssessmentVATFee, $"RefurbishmentVATFee Services Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.EstimatedSalePrice}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UIRefurbishmentTotalFee, RefurbishmentTotalValue, $"RefurbishmentTotalValue Services Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.EstimatedSalePrice}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                }
                else
                {
                    Assert.AreEqual(UIRefurbishmentFee, Premiums.RefurbishmentAssessmentFee, $"RefurbishmentFee Services Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.EstimatedSalePrice}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UIRefurbishmentVATFee, Premiums.RefurbishmentAssessmentVATFee, $"RefurbishmentVATFee Services Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.EstimatedSalePrice}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UIRefurbishmentTotalFee, RefurbishmentTotalValue, $"RefurbishmentTotalValue Services Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.EstimatedSalePrice}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                }
            }
            //Administrator Fee values    
            if (Premiums.ProductServices.Any(x => x.Contains("Administration Fee")))
            {

                var uiAdministrationFee = $"//th[@class='product-title'][text()='{productName}']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[text()='Administration Fee']//following-sibling::td[@class='right-align'][1]";
                var AdministrationFee = Driver.FindElement(By.XPath(uiAdministrationFee)).Text.Replace("£", "");
                double UIAdministrationFee = Math.Round(Convert.ToDouble(AdministrationFee), 2);
                double AdministrationFeeDiff;

                var uiAdministrationVATFee = $"//th[@class='product-title'][text()='{productName}']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[text()='Administration Fee']//following-sibling::td[@class='right-align'][2]";
                var AdministrationVATFee = Driver.FindElement(By.XPath(uiAdministrationVATFee)).Text.Replace("£", "");
                double UIAdministrationVATFee = Math.Round(Convert.ToDouble(AdministrationVATFee), 2);
                double AdministrationVATDiff;

                var uiAdministrationTotalFee = $"//th[@class='product-title'][text()='{productName}']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[text()='Administration Fee']//following-sibling::th[@class='right-align'][1]";
                var AdministrationTotalFee = Driver.FindElement(By.XPath(uiAdministrationTotalFee)).Text.Replace("£", "");
                double UIAdministrationTotalFee = Math.Round(Convert.ToDouble(AdministrationTotalFee), 2);
                double AdministrationTotalValue = Math.Round(Convert.ToDouble(UIAdministrationFee + UIAdministrationVATFee), 2);
                double AdministrationTotalDiff;

                if (UIAdministrationFee != Premiums.AdministrationFee || UIAdministrationVATFee != Premiums.AdministrationVATFee || UIAdministrationTotalFee != AdministrationTotalValue)
                {
                    AdministrationFeeDiff = Convert.ToDouble(Premiums.AdministrationFee - UIAdministrationFee);
                    AdministrationVATDiff = Convert.ToDouble(Premiums.AdministrationVATFee - UIAdministrationVATFee);
                    AdministrationTotalDiff = Convert.ToDouble(AdministrationTotalValue - UIAdministrationTotalFee);
                    if (AdministrationFeeDiff >= -1.01 && AdministrationFeeDiff <= 1.01)
                    {
                        Premiums.AdministrationFee = UIAdministrationFee;
                    }

                    if (AdministrationVATDiff >= -1.01 && AdministrationVATDiff <= 1.01)
                    {
                        Premiums.AdministrationVATFee = UIAdministrationVATFee;
                    }
                    if (AdministrationTotalDiff >= -1.01 && AdministrationTotalDiff <= 1.01)
                    {
                        AdministrationTotalValue = UIAdministrationTotalFee;
                    }

                    Assert.AreEqual(UIAdministrationFee, Premiums.AdministrationFee, $"AdministrationFee Services Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.EstimatedSalePrice}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UIAdministrationVATFee, Premiums.AdministrationVATFee, $"AdministrationVATFee Services Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.EstimatedSalePrice}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UIAdministrationTotalFee, AdministrationTotalValue, $"AdministrationTotalValue Services Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.EstimatedSalePrice}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                }
                else
                {
                    Assert.AreEqual(UIAdministrationFee, Premiums.AdministrationFee, $"AdministrationFee Services Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.EstimatedSalePrice}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UIAdministrationVATFee, Premiums.AdministrationVATFee, $"AdministrationVATFee Services Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.EstimatedSalePrice}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UIAdministrationTotalFee, AdministrationTotalValue, $"AdministrationTotalValue Services Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.EstimatedSalePrice}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                }
            }
            //Verify UI Atkins Fee Values
            if (Premiums.ProductServices.Any(x => x.Contains("Atkins Fee")))
            {

                var uiAtkinsFee = $"//th[@class='product-title'][text()='{productName}']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[text()='Atkins Fee']//following-sibling::td[@class='right-align'][1]";
                var AtkinsFee = Driver.FindElement(By.XPath(uiAtkinsFee)).Text.Replace("£", "");
                double UIAtkinsFee = Math.Round(Convert.ToDouble(AtkinsFee), 2);
                double AtkinsFeeDiff;

                var uiAtkinsVATFee = $"//th[@class='product-title'][text()='{productName}']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[text()='Atkins Fee']//following-sibling::td[@class='right-align'][2]";
                var AtkinsVATFee = Driver.FindElement(By.XPath(uiAtkinsVATFee)).Text.Replace("£", "");
                double UIAtkinsVATFee = Math.Round(Convert.ToDouble(AtkinsVATFee), 2);
                double AtkinsVATDiff;

                var uiAtkinsTotalFee = $"//th[@class='product-title'][text()='{productName}']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[text()='Atkins Fee']//following-sibling::th[@class='right-align'][1]";
                var AtkinsTotalFee = Driver.FindElement(By.XPath(uiAtkinsTotalFee)).Text.Replace("£", "");
                double UIAtkinsTotalFee = Math.Round(Convert.ToDouble(AtkinsTotalFee), 2);
                double AtkinsTotalValue = Math.Round(Convert.ToDouble(UIAtkinsFee + UIAtkinsVATFee), 2);
                double AtkinsTotalDiff;

                if (UIAtkinsFee != Premiums.AtkinsFee || UIAtkinsVATFee != Premiums.AtkinsVATFee || UIAtkinsTotalFee != AtkinsTotalValue)
                {
                    AtkinsFeeDiff = Convert.ToDouble(Premiums.AtkinsFee - UIAtkinsFee);
                    AtkinsVATDiff = Convert.ToDouble(Premiums.AtkinsVATFee - UIAtkinsVATFee);
                    AtkinsTotalDiff = Convert.ToDouble(AtkinsTotalValue - UIAtkinsTotalFee);
                    if (AtkinsFeeDiff >= -1.01 && AtkinsFeeDiff <= 1.01)
                    {
                        Premiums.AtkinsFee = UIAtkinsFee;
                    }

                    if (AtkinsVATDiff >= -1.01 && AtkinsVATDiff <= 1.01)
                    {
                        Premiums.AtkinsVATFee = UIAtkinsVATFee;
                    }
                    if (AtkinsTotalDiff >= -1.01 && AtkinsTotalDiff <= 1.01)
                    {
                        AtkinsTotalValue = UIAtkinsTotalFee;
                    }

                    Assert.AreEqual(UIAtkinsFee, Premiums.AtkinsFee, $"AtkinsFee Services Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.EstimatedSalePrice}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UIAtkinsVATFee, Premiums.AtkinsVATFee, $"AtkinsVATFee Services Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.EstimatedSalePrice}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UIAtkinsTotalFee, AtkinsTotalValue, $"AtkinsTotalValue Services Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.EstimatedSalePrice}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                }
                else
                {
                    Assert.AreEqual(UIAtkinsFee, Premiums.AtkinsFee, $"AtkinsFee Services Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.EstimatedSalePrice}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UIAtkinsVATFee, Premiums.AtkinsVATFee, $"AtkinsVATFee Services Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.EstimatedSalePrice}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UIAtkinsTotalFee, AtkinsTotalValue, $"AtkinsTotalValue Services Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.EstimatedSalePrice}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                }
            }


            //Cancellation Fee values  
            if (Premiums.ProductServices.Any(x => x.Contains("Cancellation Fee")))
            {


                var uiCancellationFee = $"//th[@class='product-title'][text()='{productName}']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[text()='Cancellation Fee']//following-sibling::td[@class='right-align'][1]";
                var CancellationFee = Driver.FindElement(By.XPath(uiCancellationFee)).Text.Replace("£", "");
                double UICancellationFee = Math.Round(Convert.ToDouble(CancellationFee), 2);
                double CancellationFeeDiff;

                var uiCancellationVATFee = $"//th[@class='product-title'][text()='{productName}']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[text()='Cancellation Fee']//following-sibling::td[@class='right-align'][2]";
                var CancellationVATFee = Driver.FindElement(By.XPath(uiCancellationVATFee)).Text.Replace("£", "");
                double UICancellationVATFee = Math.Round(Convert.ToDouble(CancellationVATFee), 2);
                double CancellationVATDiff;

                var uiCancellationTotalFee = $"//th[@class='product-title'][text()='{productName}']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[text()='Cancellation Fee']//following-sibling::th[@class='right-align'][1]";
                var CancellationTotalFee = Driver.FindElement(By.XPath(uiCancellationTotalFee)).Text.Replace("£", "");
                double UICancellationTotalFee = Math.Round(Convert.ToDouble(CancellationTotalFee), 2);
                double CancellationTotalValue = Math.Round(Convert.ToDouble(UICancellationFee + UICancellationVATFee), 2);
                double CancellationTotalDiff;


                if (UICancellationFee != Premiums.CancellationFee || UICancellationVATFee != Premiums.CancellationVATFee || UICancellationTotalFee != CancellationTotalValue)
                {
                    CancellationFeeDiff = Convert.ToDouble(Premiums.CancellationFee - UICancellationFee);
                    CancellationVATDiff = Convert.ToDouble(Premiums.CancellationVATFee - UICancellationVATFee);
                    CancellationTotalDiff = Convert.ToDouble(CancellationTotalValue - UICancellationTotalFee);
                    if (CancellationFeeDiff >= -1.01 && CancellationFeeDiff <= 1.01)
                    {
                        Premiums.CancellationFee = UICancellationFee;
                    }

                    if (CancellationVATDiff >= -1.01 && CancellationVATDiff <= 1.01)
                    {
                        Premiums.CancellationVATFee = UICancellationVATFee;
                    }
                    if (CancellationTotalDiff >= -1.01 && CancellationTotalDiff <= 1.01)
                    {
                        CancellationTotalValue = UICancellationTotalFee;
                    }

                    Assert.AreEqual(UICancellationFee, Premiums.CancellationFee, $"CancellationFee Services Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.EstimatedSalePrice}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UICancellationVATFee, Premiums.CancellationVATFee, $"CancellationVATFee Services Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.EstimatedSalePrice}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UICancellationTotalFee, CancellationTotalValue, $"CancellationTotalValue Services Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.EstimatedSalePrice}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                }
                else
                {
                    Assert.AreEqual(UICancellationFee, Premiums.CancellationFee, $"CancellationFee Services Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.EstimatedSalePrice}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UICancellationVATFee, Premiums.CancellationVATFee, $"CancellationVATFee Services Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.EstimatedSalePrice}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UICancellationTotalFee, CancellationTotalValue, $"CancellationTotalValue Services Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.EstimatedSalePrice}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                }
            }
        }

        public DataTable ReadNewhomesMasterPremiumsPriceData(string fileName)
        {

            var result = new DataSet();                                                                             //Set the First Row as Column Name
            using (var stream = File.Open(fileName, FileMode.Open, FileAccess.Read))
            using (var excelReader = ExcelReaderFactory.CreateOpenXmlReader(stream)) //.xlsx
            {
                //Return as DataSet
                result = excelReader.AsDataSet(new ExcelDataSetConfiguration()
                {
                    ConfigureDataTable = (_) => new ExcelDataTableConfiguration()
                    {
                        UseHeaderRow = true
                    }
                });
            }
            //Get all the Tables
            DataTableCollection table = result.Tables;
            //Reading Last Sheet From Product Pricing Setup 
            DataTable resultTable = table["New Homes - HVS Aviva (1)"];
            return resultTable;
        }
    }
}
