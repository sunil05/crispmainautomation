﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.PageObjects;
using RegressionPacks.Support;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace RegressionPacks.Pages
{
    public  class LABCHVSPremiums : Support.Pages
    {
        public IWebDriver wdriver;
        public string RatingValue;
        string masterplotdata;

        public LABCHVSPremiums(ISearchContext driver)
        {
            PageFactory.InitElements(driver, this);
            wdriver = (RemoteWebDriver)driver;
        }
        public void LABCNewhomesHVSPremiums()
        {
            masterplotdata = @"\\tpgfile\NewCompany\IT\Department\QA Team\Current Projects\CRISP\CrispAutomation\CrispData\Premiums\NHHVSPremiums\NHHVS-MasterPlotData.xlsx";
            ExcelPlay.ReadExcelPlotData(masterplotdata);
            CreateAndSendQuoteToVerifyPremiums();
        }
        public void LABCSocialHousingHVSPremiums()
        {
            masterplotdata = @"\\tpgfile\NewCompany\IT\Department\QA Team\Current Projects\CRISP\CrispAutomation\CrispData\Premiums\SHHVSPremiums\SHHVS-MasterPlotData.xlsx";
            ExcelPlay.ReadExcelPlotData(masterplotdata);
            CreateAndSendQuoteToVerifyPremiums();

        }
        public void LABCPrivateRentalHVSPremiums()
        {           
            masterplotdata = @"\\tpgfile\NewCompany\IT\Department\QA Team\Current Projects\CRISP\CrispAutomation\CrispData\Premiums\PRSHVSPremiums\PRSHVS-MasterPlotData.xlsx";
            ExcelPlay.ReadExcelPlotData(masterplotdata);
            CreateAndSendQuoteToVerifyPremiums();
        }
        public void LABCCommercialHVSPremiums()
        {
            masterplotdata = @"\\tpgfile\NewCompany\IT\Department\QA Team\Current Projects\CRISP\CrispAutomation\CrispData\Premiums\CommHVSPremiums\CommHVS-MasterPlotData.xlsx";
            ExcelPlay.ReadExcelPlotData(masterplotdata);
            CreateAndSendQuoteToVerifyPremiums();
        }

        public void LABCCommercialHVSPremiumsFor12YearsCoverLength()
        {
            masterplotdata = @"\\tpgfile\NewCompany\IT\Department\QA Team\Current Projects\CRISP\CrispAutomation\CrispData\Premiums\CommHVSPremiums\CommHVS-MasterPlotData.xlsx";
            ExcelPlay.ReadExcelPlotData(masterplotdata);
            CreateAndSendQuoteToVerifyPremiumsFor12YearsCoverLength();
        }

        public void LABCSocialHousingHVSPremiumsFor12YearsCoverLength()
        {
            masterplotdata = @"\\tpgfile\NewCompany\IT\Department\QA Team\Current Projects\CRISP\CrispAutomation\CrispData\Premiums\SHHVSPremiums\SHHVS-MasterPlotData.xlsx";
            ExcelPlay.ReadExcelPlotData(masterplotdata);
            CreateAndSendQuoteToVerifyPremiumsFor12YearsCoverLength();

        }

        //Create a Quote From Master Plot Schedule Data 
        public void CreateAndSendQuoteToVerifyPremiums()
        {
            // step3: create quote               
            WaitForElement(Dashboardpage.PlusButton);
            AddLABCQuotePage.SubmitQuote(masterplotdata);
            // step4: send quote and Iterate for ratings               
            SendQuotePage.ClickOnSendQuoteButton();
     

            if (Premiums.eachplotrow.Any(o => o.ProductName.Contains("New Homes - High Value")) ||
                Premiums.eachplotrow.Any(o => o.ProductName.Contains("Social Housing - High Value")) ||
                Premiums.eachplotrow.Any(o => o.ProductName.Contains("Private Rental - High Value")))
            {
                SendQuoteRatingPage();
            }
            if (Premiums.eachplotrow.Any(o => o.ProductName.Contains("Commercial - High Value")))
            {
                SendQuoteRatingPage();
            }            
        }
        //Create a Quote From Master Plot Schedule Data 
        public void CreateAndSendQuoteToVerifyPremiumsFor12YearsCoverLength()
        {
            // step3: create quote               
            WaitForElement(Dashboardpage.PlusButton);
            AddLABCQuotePage.SubmitQuoteFor12YearsCoverLength(masterplotdata);
            // step4: send quote and Iterate for ratings               
            SendQuotePage.ClickOnSendQuoteButton();


            if (Premiums.eachplotrow.Any(o => o.ProductName.Contains("New Homes - High Value")) ||
                Premiums.eachplotrow.Any(o => o.ProductName.Contains("Social Housing - High Value")) ||
                Premiums.eachplotrow.Any(o => o.ProductName.Contains("Private Rental - High Value")))
            {
                SendQuoteRatingPage();
            }
            if (Premiums.eachplotrow.Any(o => o.ProductName.Contains("Commercial - High Value")))
            {
                SendQuoteRatingPage();
            }
        }
        public void SendQuoteNextButton()
        {
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            WaitForElement(SendQuotePage.SendQuoteNextButton);
            SendQuotePage.SendQuoteNextButton.Click();
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
        }

        public void SendQuoteRatingPage()
        {
            Thread.Sleep(1000);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(1000);
            SendQuotePage.AddRatingDetails();
            
            WaitForElement(SendQuotePage.TechgRatingDropdown);
            Thread.Sleep(1000);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(1000);
            SendQuotePage.TechgRatingDropdown.Click();
            Thread.Sleep(1000);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(1000);
            WaitForElements(SendQuotePage.TechgRatingDropdownInput);
            var ratingCount = SendQuotePage.TechgRatingDropdownInput;
            // var ratingValues = ratingCount.Count;
            int[] ratingValues = { 0, 5, 9 };
            for (int i = 0; i < ratingValues.Length; i++)
            {
                if (i != 0)
                {
                    Thread.Sleep(3000);
                    WaitForElement(SendQuotePage.TechgRatingDropdown);
                    Thread.Sleep(2000);
                    SendQuotePage.TechgRatingDropdown.Click();
                    Thread.Sleep(2000);
                    WaitForElements(SendQuotePage.TechgRatingDropdownInput);
                }

                RatingValue = SendQuotePage.TechgRatingDropdownInput.ElementAt(ratingValues[i]).Text;
                Premiums.Rating = RatingValue.ToString();
                Thread.Sleep(3000);
                var item = $"//ul[@class='dropdown-content select-dropdown active']/li[{ratingValues[i] + 1}]/span";
                Thread.Sleep(3000);
                Driver.FindElement(By.XPath(item)).Click();
                // Console.WriteLine(RatingValue);
                WaitForElement(SendQuotePage.RatingComment);
                SendQuotePage.RatingComment.Click();
                WaitForElement(SendQuotePage.RatingCommentText);
                SendQuotePage.RatingCommentText.Clear();
                SendQuotePage.RatingCommentText.SendKeys("good rate");
                Thread.Sleep(2000);
                if (SendQuotePage.BuildersExperienceLabel.Count > 0)
                {
                    WaitForElements(SendQuotePage.BuildersExperienceLabel);
                    SendQuotePage.BuildersExperienceLabel[0].Click();
                    Thread.Sleep(500);
                    WaitForElement(SendQuotePage.BuildersExperienceInput);
                    SendQuotePage.BuildersExperienceInput.Click();
                    SendQuotePage.BuildersExperienceInput.Clear();                  
                    SendQuotePage.BuildersExperienceInput.SendKeys(ratingValues[i]+1.ToString());
                    var buildingexpvalue = ratingValues[i]+1.ToString();
                    //  var buildingexpvalue = SendQuotePagePage.BuildersExperienceInput.GetAttribute(i.ToString());
                    Premiums.BuilderExperiance = buildingexpvalue;
                    Thread.Sleep(2000);
                }
                SendQuoteNextButton();
                //SecuritiesDetails Page
                SendQuotePage.SendQuoteSecuritiesPage();                
                SendQuotePage.SendQuoteConditionsPage();
                SendQuotePage.SendQuotepecialTermsPage();
                SendQuoteFeesDetailsPage();
                if (i != ratingValues.Length - 1)
                { 
                        Thread.Sleep(500);
                        CloseSpinneronDiv();
                        CloseSpinneronPage();
                        Thread.Sleep(500);
                        WaitForElement(SendQuotePage.PreviousButton);
                        SendQuotePage.PreviousButton.Click();
                        Thread.Sleep(500);
                        CloseSpinneronDiv();
                        CloseSpinneronPage();
                        Thread.Sleep(500);
                        WaitForElement(SendQuotePage.PreviousButton);
                        SendQuotePage.PreviousButton.Click();
                        Thread.Sleep(500);
                        CloseSpinneronDiv();
                        CloseSpinneronPage();
                        Thread.Sleep(500);
                        WaitForElement(SendQuotePage.PreviousButton);
                        SendQuotePage.PreviousButton.Click();
                        Thread.Sleep(500);
                        CloseSpinneronDiv();
                        CloseSpinneronPage();
                        Thread.Sleep(500);
                        WaitForElement(SendQuotePage.PreviousButton);
                        SendQuotePage.PreviousButton.Click();
                        Thread.Sleep(500);
                        CloseSpinneronDiv();
                        CloseSpinneronPage();
                        Thread.Sleep(500);
                    }
                    else
                    {
                        WaitForElement(SendQuotePage.CancelButton);
                        SendQuotePage.CancelButton.Click();
                        Thread.Sleep(1000);
                        CloseSpinneronDiv();
                        CloseSpinneronPage();
                        Thread.Sleep(1000);
                        WaitForElement(SendQuotePage.CancelOKButton);
                        SendQuotePage.CancelOKButton.Click();
                        Thread.Sleep(1000);
                        CloseSpinneronDiv();
                        CloseSpinneronPage();
                        Thread.Sleep(1000);
                    }                
            }

        }
      
        //Rating Page for New Homes Product
        public void SendQuoteBuilderExpPage()
        {
            Thread.Sleep(1000);
            WaitForElement(SendQuotePage.SendQuoteWizard);
            int[] ratingValues = { 1, 4, 12 };
            // var ratingValues = ratingCount.Count;
            for (int i = 0; i < ratingValues.Length; i++)
            {
                Thread.Sleep(2000);
                WaitForElements(SendQuotePage.BuildersExperienceLabel);
                SendQuotePage.BuildersExperienceLabel[0].Click();
                Thread.Sleep(500);
                WaitForElement(SendQuotePage.BuildersExperienceInput);
                SendQuotePage.BuildersExperienceInput.Click();
                SendQuotePage.BuildersExperienceInput.Clear();
                SendQuotePage.BuildersExperienceInput.SendKeys(ratingValues[i].ToString());
                var buildingexpvalue = ratingValues[i].ToString();
                //  var buildingexpvalue = SendQuotePagePage.BuildersExperienceInput.GetAttribute(i.ToString());
                Premiums.BuilderExperiance = buildingexpvalue;
                Thread.Sleep(2000);
                SendQuoteNextButton();
                //SecuritiesDetails Page
                SendQuotePage.SendQuoteSecuritiesPage();
                SendQuotePage.SendQuoteConditionsPage();
                SendQuotePage.SendQuotepecialTermsPage();
                SendQuoteFeesDetailsPage();
                if (i != ratingValues.Length - 1)
                {
                    Thread.Sleep(1000);
                    WaitForElement(SendQuotePage.PreviousButton);
                    Thread.Sleep(1000);
                    SendQuotePage.PreviousButton.Click();
                    Thread.Sleep(1000);
                    Thread.Sleep(1000);
                    WaitForElement(SendQuotePage.PreviousButton);
                    Thread.Sleep(1000);
                    SendQuotePage.PreviousButton.Click();
                    Thread.Sleep(1000);
                    Thread.Sleep(1000);
                    WaitForElement(SendQuotePage.PreviousButton);
                    Thread.Sleep(2000);
                    SendQuotePage.PreviousButton.Click();
                    Thread.Sleep(1000);

                    Thread.Sleep(1000);
                    WaitForElement(SendQuotePage.PreviousButton);
                    Thread.Sleep(1000);
                    SendQuotePage.PreviousButton.Click();
                    Thread.Sleep(1000);
                }
            }
            WaitForElement(SendQuotePage.CancelButton);
            SendQuotePage.CancelButton.Click();
            Thread.Sleep(1000);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(1000);
            WaitForElement(SendQuotePage.CancelOKButton);
            SendQuotePage.CancelOKButton.Click();
            Thread.Sleep(1000);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(1000);
        }

        public void SendQuoteFeesDetailsPage()
        {
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(1000);
            WaitForElements(SendQuotePage.FeesEle);
            Thread.Sleep(500);
            Assert.IsTrue(SendQuotePage.FeesEle.Count > 0);
            //Console.WriteLine("Fees details verified");
            Thread.Sleep(1000);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(1000);
            Premiums.Plots = SendQuotePage.PlotCount.Text;
            //CoverlengthDetails 
            Thread.Sleep(1000);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(1000);
            WaitForElement(SendQuotePage.CoverLength);
            var CoverLengthValue = SendQuotePage.CoverLength.Text;
            Thread.Sleep(500);
            Premiums.CoverLengthYears = Convert.ToDouble(CoverLengthValue);
            //verify Cover Premiums Caluclations                        
            CaluclatePremiums();           
            Thread.Sleep(2000);
        }

        //Verify Premium Details  
        public void CaluclatePremiums()
        {
            if (Premiums.eachplotrow.Any(o => o.ProductName == "New Homes - High Value"))
            {
                NewhomesHVSPremiums.NewHomesHVSPremiumsCaluclations();
            }
            if (Premiums.eachplotrow.Any(o => o.ProductName == "Social Housing - High Value"))
            {
                SocialHousingHVSPremiums.SocialHousingHVSPremiumsCaluclations();
            }                    
            if (Premiums.eachplotrow.Any(o => o.ProductName == "Commercial - High Value"))
            {
                CommercialHVSPremiums.CommercialHVSPremiumsCaluclations();
            }            
        }
    }
}
