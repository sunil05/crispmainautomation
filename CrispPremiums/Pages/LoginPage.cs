﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RegressionPacks.Pages
{
    public class LoginPage
    {
        public LoginPage(ISearchContext driver)
        {
            PageFactory.InitElements(driver, this);
        }

        [FindsBy(How = How.Id, Using = "Username")]
        public IWebElement Username;

        [FindsBy(How = How.Id, Using = "Password")]
        public IWebElement Password;

        [FindsBy(How = How.XPath, Using = "//BUTTON[@class='btn btn-primary'][text()='Login']")]
        public IWebElement LoginButton;
    }
}
