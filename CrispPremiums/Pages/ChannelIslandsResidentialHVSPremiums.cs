﻿using ExcelDataReader;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.PageObjects;
using RegressionPacks.Support;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RegressionPacks.Pages
{
    public class ChannelIslandsResidentialHVSPremiums : Support.Pages
    {
        public string RatingValue;
        public IWebDriver wdriver;
        double iptCaluclationValue = 0.00;
        double iptCaluclationForAtkinsValue = 0.12;
        double vatCaluclationValue = 0.20;
        double BasePremium = 0;  
        double SeepageFee = 0;
        double LossOfGrossProfitPremium = 0;
        double LossOfRentPayablePremium = 0;
        double LossOfRentReceivablePremium = 0;
        double WaviersArchitect = 0;
        double WaviersEngineer = 0;
        double WaviersBuilder = 0;     
        double TAFee = 0;
        int RatingColumn;
        double AtkinsPremium = 0;
        double MinimumAtkinsFee = 0;
        DataTable excelTable;
      


        public ChannelIslandsResidentialHVSPremiums(ISearchContext driver)
        {
            PageFactory.InitElements(driver, this);
            wdriver = (RemoteWebDriver)driver;
        }
        public void CoversOnChannelIslandsResidentialHVSProduct()
        {
            if (Premiums.eachplotrow.Any(o => o.ProductName.Contains("Channel Islands Residential - High Value")))
            {
                Premiums.ProductsCovers = PremiumCovers.ProductCoversList.Select(i => i.Text.ToString()).ToList();
                Premiums.ProductServices = PremiumCovers.ProductServicesList.Select(i => i.Text.ToString()).ToList();

                if (Premiums.ProductsCovers.Any(x => x.Contains("Seepage")))
                {
                    Premiums.SeepageCover = PremiumCovers.SeeapageCover.Count > 0 ? "Yes" : "No";
                }
                if (Premiums.ProductsCovers.Any(x => x.Contains("Waivers of Subrogation Rights - Builder")))
                {
                    Premiums.WaiversOfSubrogationRightsBuilderCover = PremiumCovers.WaiversBuilderCover.Count > 0 ? "Yes" : "No";
                }

                if (Premiums.ProductsCovers.Any(x => x.Contains("Waivers of Subrogation Rights - Engineer")))
                {
                    Premiums.WaiversOfSubrogationRightsEngineerCover = PremiumCovers.WaiversEngineerCover.Count > 0 ? "Yes" : "No";
                }
                if (Premiums.ProductServices.Any(x => x.Contains("Technical Audit Fee")))
                {
                    Premiums.ProductTechnicalAuditFeeService = PremiumCovers.ProductTechnicalAuditFee.Count > 0 ? "Yes" : "No";
                }
                if (Premiums.ProductServices.Any(x => x.Contains("Refurbishment Assessment Fee")))
                {
                    Premiums.ProductRefurbishmentAssessmentService = PremiumCovers.ProductRefurbishmentAssessmentFee.Count > 0 ? "Yes" : "No";
                }
                if (Premiums.ProductServices.Any(x => x.Contains("Administration Fee")))
                {
                    Premiums.ProductAdminFeeService = PremiumCovers.ProductAdministrationFee.Count > 0 ? "Yes" : "No";
                }
                if (Premiums.ProductServices.Any(x => x.Contains("Atkins Fee")))
                {
                    Premiums.ProductAtkinsFeeService = PremiumCovers.ProductAtkinsFee.Count > 0 ? "Yes" : "No";
                }
                if (Premiums.ProductServices.Any(x => x.Contains("Cancellation Fee")))
                {
                    Premiums.ProductCancellationFeeService = PremiumCovers.ProductCancellationFee.Count > 0 ? "Yes" : "No";
                }
            }
        }

        public void ChannelIslandsResidentialHVSPremiumsCaluclations()
        {
            Premiums.StructuralFee = 0;
            Premiums.SeepageFee = 0;
            Premiums.WaiversOfSubrogationBuilderFee = 0;
            Premiums.WaiversOfSubrogationEngineerFee = 0;
            Premiums.TechnicalAuditFee = 0;
            Premiums.AdministrationFee = 0;
            Premiums.RefurbishmentAssessmentFee = 0;
            Premiums.CancellationFee = 0;
            Premiums.AtkinsFee = 0;
           var aggregatedPremium = new ChannelIslandsResidentialHVSPremium();

            foreach (var plot in Premiums.eachplotrow)
            {
                //Actual Premiums Caluclaions for Each Plot Row 
                var calculatedPremiums = ChannelIslandsResidentialHVSCaluclationsForEachPlotRow(plot);
                //Cover Premiums              
                Premiums.StructuralFee += calculatedPremiums.structuralFee;
                Premiums.SeepageFee += calculatedPremiums.seepageFee;
                Premiums.WaiversOfSubrogationBuilderFee += calculatedPremiums.waviersOfSubrogationRightsBuilderFee;
                Premiums.WaiversOfSubrogationEngineerFee += calculatedPremiums.waviersOfSubrogationRightsEngineerFee;

                //Services 
                Premiums.TechnicalAuditFee += calculatedPremiums.technicalFee;
                Premiums.RefurbishmentAssessmentFee += calculatedPremiums.refurbishAssessmentFee;
                Premiums.AdministrationFee += calculatedPremiums.administrationFee;
                Premiums.CancellationFee += calculatedPremiums.cancellationFee;
                Premiums.AtkinsFee = calculatedPremiums.atkinsFee;
            }

            if (Premiums.ProductRefurbishmentAssessmentService == "Yes")
            {
                Premiums.RefurbishmentAssessmentFee = Math.Round((0.00), 2);
            }
                
       
            //IPT Caluclations 
            Premiums.StructuralIPTFee = Math.Round(Convert.ToDouble(Premiums.StructuralFee * iptCaluclationValue), 2);
            Premiums.SeepageIPTFee = Convert.ToDouble(Premiums.SeepageFee * iptCaluclationValue);
            Premiums.WaiversOfSubrogationBuilderIPTFee = Convert.ToDouble(Premiums.WaiversOfSubrogationBuilderFee * iptCaluclationValue);
            Premiums.WaiversOfSubrogationEngineerIPTFee = Convert.ToDouble(Premiums.WaiversOfSubrogationEngineerFee * iptCaluclationValue);

            //VAT Caluclations
            Premiums.TechnicalAuditVATFee = Math.Round(0.00);
            Premiums.AdministrationVATFee = Math.Round(0.00);
            Premiums.RefurbishmentAssessmentVATFee = Math.Round(0.00);
            Premiums.CancellationVATFee = Math.Round(0.00);
            Premiums.AtkinsVATFee = Convert.ToDouble(Premiums.AtkinsFee * iptCaluclationForAtkinsValue);
            VerfiyChannelIslandsResidentialHVSUIValues();
            if (Premiums.eachplotrow.Any(o => o.BCProduct.Contains("Residential Building Control")))
            {
                ResidentialBCPremiums.ResidentialBCPremiumNewPricesCalcs();
            }
        }
        public class ChannelIslandsResidentialHVSPremium
        {
            
            public double structuralFee;

            public double contaminatedLandFee;
         
            public double seepageFee;

            public double waviersOfSubrogationRightsBuilderFee;

            public double lossOfGrossProfitFee;

            public double lossOfRentPayableFee;

            public double lossOfRentReceivableFee;

            public double waviersOfSubrogationRightsEngineerFee;

            public double waviersOfSubrogationRightsArchitectFee;

            public double technicalFee;

            public double refurbishAssessmentFee;

            public double administrationFee;

            public double cancellationFee;

            public double atkinsFee;
        }

      
        public ChannelIslandsResidentialHVSPremium ChannelIslandsResidentialHVSCaluclationsForEachPlotRow(plotrows plot)
        {
            var calculatedPremiums = new ChannelIslandsResidentialHVSPremium();
            //Retrieve all the covers and Services from Fees Detail Page 
            CoversOnChannelIslandsResidentialHVSProduct();
            //Reading Pricing Setup Book 
            var fileInfo = new FileInfo(ExtensionMethods.GetAbsolutePath($@"TestData\Premiums\ProductPricingSetup\Product Pricing Setup - HVS.xlsx"));
            string premiumsdata = fileInfo.ToString();
            excelTable = ReadChannelIslandsResidentialHVSMasterPremiumsPriceData(premiumsdata);
            //Structural Fee Caluclations Based on Banding           
            StuctralPremiumCaluclations(plot);
            SecondaryLayerPremiums.AddSecondaryLayerPremium(BasePremium, plot);
            BasePremium = Premiums.SecondaryLayerPremium;
            calculatedPremiums.structuralFee = Math.Round(BasePremium, 2);
          

            if (Premiums.ProductsCovers.Count > 0)
            {
                if (Premiums.ContaminatedLandCover == "Yes")
                {
                    calculatedPremiums.contaminatedLandFee = Convert.ToDouble(0.00);
                }
             
                if (Premiums.SeepageCover == "Yes")
                {
                    calculatedPremiums.seepageFee = Math.Round((SeepageFee), 2);
                }
                if (Premiums.WaiversOfSubrogationRightsBuilderCover == "Yes")
                {
                    WaiversBuilderPremiumCaluclations(plot);
                    calculatedPremiums.waviersOfSubrogationRightsBuilderFee = Math.Round((WaviersBuilder), 2);
                }
                if (Premiums.LossOfGrossProfitCover == "Yes")
                {
                    LossOfRentCaluclations(plot);
                    calculatedPremiums.lossOfGrossProfitFee = Math.Round((LossOfGrossProfitPremium), 2);
                }
                if (Premiums.LossOfRentPayableCover == "Yes")
                {
                    LossOfRentCaluclations(plot);
                    calculatedPremiums.lossOfRentPayableFee = Math.Round((LossOfRentPayablePremium), 2);
                }
                if (Premiums.LossOfRentReceivableCover == "Yes")
                {
                    LossOfRentCaluclations(plot);
                    calculatedPremiums.lossOfRentReceivableFee = Math.Round((LossOfRentReceivablePremium), 2);
                }
                if (Premiums.WaiversOfSubrogationRightsEngineerCover == "Yes")
                {
                    WaiversEngineerPremiumCaluclations(plot);
                    calculatedPremiums.waviersOfSubrogationRightsEngineerFee = Math.Round((WaviersEngineer), 2);
                }
                if (Premiums.WaiversOfSubrogationRightsArchitectCover == "Yes")
                {
                    WaiversArchitectPremiumCaluclations(plot);
                    calculatedPremiums.waviersOfSubrogationRightsEngineerFee = Math.Round((WaviersArchitect), 2);
                }
            }

            //Product Service Fee Values 
            if (Premiums.ProductServices.Count > 0)
            {

                if (Premiums.ProductTechnicalAuditFeeService == "Yes")
                {
                    //Cover Services Caluclations 
                    TAFeeCaluclations(plot);
                    calculatedPremiums.technicalFee = Math.Round((TAFee), 2);
                }
                if (Premiums.ProductRefurbishmentAssessmentService == "Yes")
                {
                    calculatedPremiums.refurbishAssessmentFee = Convert.ToDouble(0.00);
                }
                if (Premiums.ProductAdminFeeService == "Yes")
                {
                    calculatedPremiums.administrationFee = Convert.ToDouble(0.00);
                }
                if (Premiums.ProductCancellationFeeService == "Yes")
                {
                    calculatedPremiums.cancellationFee = Convert.ToDouble(0.00);
                }
                if (Premiums.ProductAtkinsFeeService == "Yes")
                {
                    AtkinsFeeCaluclations();
                    calculatedPremiums.atkinsFee = Math.Round(Convert.ToDouble(AtkinsPremium), 2);
                }
            }

            return calculatedPremiums;
        }



        //Reading Strucural Premium Banding Table  and Caluclate Base Premium
        public void StuctralPremiumCaluclations(plotrows plot)
        {
            // Reading Structural Premium Table From CommercialHVS Pricing Setup Book
            var rating = Premiums.Rating;
            var ratingTableRow = -1;
            var ratingColumn = -1;
            //Get Rating Table from Structural Premium Primary Layer Table
            for (int i = 0; i < excelTable.Rows.Count; i++)
            {
                if (excelTable.Rows[i][1].ToString().Contains("Rating"))
                {
                    ratingTableRow = i;
                    break;
                }
            }
            if (ratingTableRow == -1)
                throw new Exception("Rating Table Row Not Found");


            //Get Rating Columns from Structural Premium Table

            for (int i = ratingTableRow; i < excelTable.Rows[ratingTableRow].ItemArray.Length; i++)
            {
                if (excelTable.Rows[ratingTableRow].ItemArray[i].ToString() == Premiums.Rating.ToString())
                {
                    ratingColumn = i;
                    RatingColumn = ratingColumn;
                }
            }

            if (ratingColumn == -1)
                throw new Exception("Rating Column Not Found");

            //Get Premium Value From structural Premium Table 
            double basicpremiumValue = Convert.ToDouble(excelTable.Rows[ratingTableRow + 1][RatingColumn]);
            BasePremium = Convert.ToDouble(plot.ReconstructionCostValue * basicpremiumValue);

            //Cover Loading Premiums 
            PeriodOfCoverLoadingCaluclation(plot);

            //Caluclate Premiums Based on Unit type 
            UnitTypeCaluclation(plot);
            //Caluclate Premiums Based on Construction type 
            if (Premiums.eachplotrow.Any(o => o.ConstructionType.Contains("New Build")) && Premiums.eachplotrow.Any(o => o.ConstructionType.Contains("Conversion")))
            {
                if (plot.ConstructionType.Contains("New Build"))
                {
                    ConstructionTypeCaluclationNewbuildandConversion(plot);
                }
                else
                {
                    ConstructionTypeCaluclation(plot);
                }
            }
            else
            {
                ConstructionTypeCaluclation(plot);
            }
            //Multi Project Loading Caluclation 
            if (SendQuotePage.MultiProjectDiscount.Count > 0)
            {
                Premiums.MultiProjectLoading = SendQuotePage.MultiProjectDiscount[0].Text;
                if (Premiums.MultiProjectLoading == "Yes")
                {
                    MultiProjectLoadngDiscountCaluclations(plot);
                }
            }
            if (SendQuotePage.DiscretionaryDiscount.Count > 0)
            {
                DiscretionaryDiscountCaluclations(plot);
            }
            if (SendQuotePage.StageOfWorksPercentage.Count > 0)
            {
                StageOfWorksLoadingPercentageCaluclations(plot);
            }
        }
        //Reading Period Of Cover Loading Table 
        public void PeriodOfCoverLoadingCaluclation(plotrows plot)
        {
            var periodOfCoverLoadingTableRow = -1;
            var periodOfCoverRow = -1;
            //Get Unit Type Loading Table from Structural Premium Table
            for (int i = 0; i < excelTable.Rows.Count; i++)
            {
                if (excelTable.Rows[i][0].ToString().Contains("Period of Cover Loading"))
                {
                    periodOfCoverLoadingTableRow = i;
                    break;
                }
            }
            if (periodOfCoverLoadingTableRow == -1)
                throw new Exception("Unit Type Loading Table Not Found");

            //Get UnitType Row from Unit Type Loading Table
            for (int i = periodOfCoverLoadingTableRow; i < excelTable.Rows.Count; i++)
            {
                if (excelTable.Rows[i][1].ToString() == Premiums.CoverLengthYears.ToString())
                {
                    periodOfCoverRow = i;
                    break;
                }
            }
            if (periodOfCoverRow == -1)
                throw new Exception("Period Of Cover Loading Row Not Found");
            double premiumValueBasedonPeriodOfCoverLoading = Convert.ToDouble(excelTable.Rows[periodOfCoverRow][2]);
            BasePremium = Convert.ToDouble(BasePremium * premiumValueBasedonPeriodOfCoverLoading);
        }
        //Reading Unit Type Loading Table and Caluclate Base Premium
        public void UnitTypeCaluclation(plotrows plot)
        {
            var unitTypeTableRow = -1;
            var unitTypeRow = -1;
            //Get Unit Type Loading Table from Structural Premium Table
            for (int i = 0; i < excelTable.Rows.Count; i++)
            {
                if (excelTable.Rows[i][0].ToString().Contains("Unit Type Loading"))
                {
                    unitTypeTableRow = i;
                    break;
                }
            }
            if (unitTypeTableRow == -1)
                throw new Exception("Unit Type Loading Table Not Found");

            //Get UnitType Row from Unit Type Loading Table
            for (int i = unitTypeTableRow; i < excelTable.Rows.Count; i++)
            {
                if (excelTable.Rows[i][1].ToString() == plot.UnitType.ToString())
                {
                    unitTypeRow = i;
                    break;
                }
            }
            if (unitTypeRow == -1)
                throw new Exception("Unit Type Row Not Found");
            double premiumValueBasedonUnitTypeLoading = Convert.ToDouble(excelTable.Rows[unitTypeRow][RatingColumn]);
            BasePremium = Convert.ToDouble(BasePremium * premiumValueBasedonUnitTypeLoading);
        }
        //Reading Construction Type Loading Table and Caluclate Base Premium
        public void ConstructionTypeCaluclation(plotrows plot)
        {
            var constructionTypeTableRow = -1;
            var constructionTypeRow = -1;
            //Get Construction Type Loading Table from Structural Premium Table
            for (int i = 0; i < excelTable.Rows.Count; i++)
            {
                if (excelTable.Rows[i][0].ToString().Contains("Construction Type Loading"))
                {
                    constructionTypeTableRow = i;
                    break;
                }
            }
            if (constructionTypeTableRow == -1)
                throw new Exception("Construction Type Loading Table Not Found");

            //Get Construction Type  Row from Construction Type Loading Table 
            for (int i = constructionTypeTableRow; i < excelTable.Rows.Count; i++)
            {
                if (excelTable.Rows[i][1].ToString() == plot.ConstructionType.ToString())
                {
                    constructionTypeRow = i;
                    break;
                }
            }
            if (constructionTypeRow == -1)
                throw new Exception("Construction Type Row Not Found");
            double premiumValueBasedOnConstructionType = Convert.ToDouble(excelTable.Rows[constructionTypeRow][RatingColumn]);
            BasePremium = Convert.ToDouble(BasePremium * premiumValueBasedOnConstructionType);
            if (plot.ConstructionType == "New Build")
            {
                NewBuildStagesOfWorkCaluclation(plot);
            }
            if (plot.ConstructionType == "Conversion")
            {
                ConversionStagesOfWorkCaluclation(plot);
            }
        }
        //Reading Construction Type Loading Table and Caluclate Base Premium
        public void ConstructionTypeCaluclationNewbuildandConversion(plotrows plot)
        {
            var constructionTypeTableRow = -1;
            var constructionTypeRow = -1;
            //Get Construction Type Loading Table from Structural Premium Table
            for (int i = 0; i < excelTable.Rows.Count; i++)
            {
                if (excelTable.Rows[i][0].ToString().Contains("Construction Type Loading"))
                {
                    constructionTypeTableRow = i;
                    break;
                }
            }
            if (constructionTypeTableRow == -1)
                throw new Exception("Construction Type Loading Table Not Found");

            //Get Construction Type  Row from Construction Type Loading Table 
            for (int i = constructionTypeTableRow; i < excelTable.Rows.Count; i++)
            {
                if (excelTable.Rows[i][1].ToString().Contains("New Build (with Conversion)"))
                {
                    constructionTypeRow = i;
                    break;
                }
            }
            if (constructionTypeRow == -1)
                throw new Exception("Construction Type Row Not Found");
            double premiumValueBasedOnConstructionType = Convert.ToDouble(excelTable.Rows[constructionTypeRow][RatingColumn]);
            BasePremium = Convert.ToDouble(BasePremium * premiumValueBasedOnConstructionType);
            if (plot.ConstructionType == "New Build")
            {
                NewBuildStagesOfWorkCaluclation(plot);
            }
            if (plot.ConstructionType == "Conversion")
            {
                ConversionStagesOfWorkCaluclation(plot);
            }
        }


        //Reading NewBuild Stages Of Work Loading Table and Caluclate Base Premium
        public void NewBuildStagesOfWorkCaluclation(plotrows plot)
        {
            double premiumValueBasedOnNewBuildSOW = 0.00;
            if (plot.StageOfWork == "No Works Started")
            {
                premiumValueBasedOnNewBuildSOW = Convert.ToDouble(1.00);
            }
            if (plot.StageOfWork == "Foundation/DPC")
            {
                premiumValueBasedOnNewBuildSOW = Convert.ToDouble(1.25);
            }
            if (plot.StageOfWork == "1st Floor")
            {
                premiumValueBasedOnNewBuildSOW = Convert.ToDouble(1.50);
            }
            if (plot.StageOfWork == "Wall Plate Level")
            {
                premiumValueBasedOnNewBuildSOW = Convert.ToDouble(1.75);
            }
            if (plot.StageOfWork == "Roof/Watertight")
            {
                premiumValueBasedOnNewBuildSOW = Convert.ToDouble(2.00);
            }
            BasePremium = Convert.ToDouble(BasePremium * premiumValueBasedOnNewBuildSOW);
        }

        //Reading Conversion Stages Of Work Loading Table and Caluclate Base Premium
        public void ConversionStagesOfWorkCaluclation(plotrows plot)
        {
            double premiumValueBasedOnConversionSOW = 0.00;
            if (plot.StageOfWork == "No Works Started")
            {
                premiumValueBasedOnConversionSOW = Convert.ToDouble(1.00);
            }
            if (plot.StageOfWork == "First Fix")
            {
                premiumValueBasedOnConversionSOW = Convert.ToDouble(1.25);
            }
            if (plot.StageOfWork == "Second Fix")
            {
                premiumValueBasedOnConversionSOW = Convert.ToDouble(1.75);
            }
            BasePremium = Convert.ToDouble(BasePremium * premiumValueBasedOnConversionSOW);
        }
        public void MultiProjectLoadngDiscountCaluclations(plotrows plot)
        {
            var multiProjectLoadngTableRow = -1;
            var multiProjectLoadingRow = -1;
            //Get Basment Loading (Site wide for all products) Table from Structural Premium Table
            for (int i = 0; i < excelTable.Rows.Count; i++)
            {
                if (excelTable.Rows[i][0].ToString().Contains("Multi Project Loading (Site wide for all products)"))
                {
                    multiProjectLoadngTableRow = i;
                    break;
                }
            }
            if (multiProjectLoadngTableRow == -1)
                throw new Exception("Multi Project Loading (Site wide for all products) Table Not Found");

            //Multi Basment Loading (Site wide for all products) Table Row from Multi Project Loading Table 
            for (int i = multiProjectLoadngTableRow; i < excelTable.Rows.Count; i++)
            {
                if (excelTable.Rows[i][1].ToString() == "Yes")
                {
                    multiProjectLoadingRow = i;
                    break;
                }
            }
            if (multiProjectLoadingRow == -1)
                throw new Exception("Multi Project Loading (Site wide for all products) Table Row Not Found");
            double premiumValueBasedOnMultiProjectLoadingValue = Convert.ToDouble(excelTable.Rows[multiProjectLoadingRow][2]);
            Premiums.MultiProjectDiscountInput = SendQuotePage.MultiProjectDiscount[0].Text;
            if (Premiums.MultiProjectDiscountInput == "Yes")
            {
                double multiProjectDiscount = Convert.ToDouble(BasePremium * premiumValueBasedOnMultiProjectLoadingValue);
                BasePremium = Convert.ToDouble(multiProjectDiscount);
            }
        }
        //Discretionary Discount Caluclations
        public void DiscretionaryDiscountCaluclations(plotrows plot)
        {
            string discretionaryDiscountText = SendQuotePage.DiscretionaryDiscount[0].Text;
            string discretionaryDiscountTextValue = discretionaryDiscountText.Replace("%", "");
            Premiums.DiscretionaryDiscountInput = Convert.ToDouble(discretionaryDiscountTextValue);
            if (Premiums.DiscretionaryDiscountInput > 0.00)
            {
                double discretionaryDiscount = Convert.ToDouble(BasePremium * Premiums.DiscretionaryDiscountInput);
                BasePremium = Convert.ToDouble(BasePremium - discretionaryDiscount);
            }
        }

        //Stages oF works Loading Caluclations
        public void StageOfWorksLoadingPercentageCaluclations(plotrows plot)
        {
            string sowPercentageText = SendQuotePage.StageOfWorksPercentage[0].Text;
            string sowPercentageTextValue = sowPercentageText.Replace("%", "");
            Premiums.StageOfWorksLoadingPercentageInput = Convert.ToDouble(sowPercentageTextValue);
            if (Premiums.StageOfWorksLoadingPercentageInput > 0.00)
            {
                double sowPercentage = Convert.ToDouble(BasePremium * Premiums.StageOfWorksLoadingPercentageInput);
                BasePremium = Convert.ToDouble(BasePremium + sowPercentage);
            }
        }

        //Reading Loss Of Rent Premiums Factor Table 
        public void LossOfRentCaluclations(plotrows plot)
        {
            var lossOfRentTable = -1;
            var lossOfRentRatingRow = -1;
            //Get Conversion Stages Of Works Table from Structural Premium Table
            for (int i = 0; i < excelTable.Rows.Count; i++)
            {
                if (excelTable.Rows[i][0].ToString().Contains("Loss of Rent Premium"))
                {
                    lossOfRentTable = i;
                    break;
                }
            }
            if (lossOfRentTable == -1)
                throw new Exception("Loss Of Rent Table Not Found");
            //Get Conversion Stages Of Works Row 
            for (int i = lossOfRentTable; i < excelTable.Rows.Count; i++)
            {
                if (excelTable.Rows[i][1].ToString().Contains("Rating"))
                {
                    lossOfRentRatingRow = i;
                    break;
                }
            }
            if (lossOfRentRatingRow == -1)
                throw new Exception("Loss Of Rent Banding Row Not Found");

            //Get Premium Value From structural Premium Table 
            double lossofrentpremiumValue = Convert.ToDouble(excelTable.Rows[lossOfRentRatingRow + 1][RatingColumn]);
            int plotCount = Convert.ToInt32(Premiums.Plots);
            var annualAmountPerPlot = Convert.ToDouble(Premiums.LossOfGrossProfitInput / plotCount);
            var lossOfRentPremiumValue = (annualAmountPerPlot * Premiums.LossOfGrossProfitNumberOfYearsInput) * lossofrentpremiumValue;
            LossOfRentPayablePremium = Convert.ToDouble(lossOfRentPremiumValue);
            LossOfRentReceivablePremium = Convert.ToDouble(lossOfRentPremiumValue);
            LossOfGrossProfitPremium = Convert.ToDouble(lossOfRentPremiumValue);

        }

        //Reading Waviers Architect Premiums Factor Table 
        public void WaiversArchitectPremiumCaluclations(plotrows plot)
        {
            var waiversArchitectTable = -1;

            //Get Subrogation Waivers - Architect Premium Table from Structural Premium Table
            for (int i = 0; i < excelTable.Rows.Count; i++)
            {
                if (excelTable.Rows[i][0].ToString().Contains("Subrogation Waivers - Architect Premium"))
                {
                    waiversArchitectTable = i;
                    break;
                }
            }
            if (waiversArchitectTable == -1)
                throw new Exception("Subrogation Waivers - Architect Premium Table Not Found");


            //Get CSubrogation Waivers - Architect Premium Row 
            double waiversArchitectPremium = Convert.ToDouble(excelTable.Rows[waiversArchitectTable + 2][1]);
            WaviersArchitect = Convert.ToDouble(plot.ReconstructionCostValue * waiversArchitectPremium);
        }
        //Reading Waviers Builder Premiums Factor Table 
        public void WaiversBuilderPremiumCaluclations(plotrows plot)
        {
            var waiversBuilderTable = -1;

            //Get Subrogation Waivers - Builder Premium Table from Structural Premium Table
            for (int i = 0; i < excelTable.Rows.Count; i++)
            {
                if (excelTable.Rows[i][0].ToString().Contains("Subrogation Waivers - Builder Premium"))
                {
                    waiversBuilderTable = i;
                    break;
                }
            }
            if (waiversBuilderTable == -1)
                throw new Exception("Subrogation Waivers - Builder Premium Table Not Found");


            //Get Subrogation Waivers - Builder PremiumRow 
            double waiversBuilderPremium = Convert.ToDouble(excelTable.Rows[waiversBuilderTable + 2][1]);
            WaviersBuilder = Convert.ToDouble(plot.ReconstructionCostValue * waiversBuilderPremium);
        }
        //Reading Waviers Engineer Premiums Factor Table 
        public void WaiversEngineerPremiumCaluclations(plotrows plot)
        {
            var waiversEngineerTable = -1;

            //GetSubrogation Waivers - Engineer Premium Table from Structural Premium Table
            for (int i = 0; i < excelTable.Rows.Count; i++)
            {
                if (excelTable.Rows[i][0].ToString().Contains("Subrogation Waivers - Engineer Premium"))
                {
                    waiversEngineerTable = i;
                    break;
                }
            }
            if (waiversEngineerTable == -1)
                throw new Exception("Subrogation Waivers - Engineer Premium Table Not Found");


            //Get Subrogation Waivers - Builder PremiumRow 
            double waiversEngineerPremium = Convert.ToDouble(excelTable.Rows[waiversEngineerTable + 2][1]);
            WaviersEngineer = Convert.ToDouble(plot.ReconstructionCostValue * waiversEngineerPremium);
        }     


        //Services Caluclations 
        //TA Fee Caluclations   
        //Basic TAFee Caluclations     
        public void TAFeeCaluclations(plotrows plot)
        {
            // Reading TA Fee Table From CommercialHVS Pricing Setup Book           
            var TAFeeTableRow = -1;
            var bandingRow = -1;

            //Get TA Fee Table from Structural Premium Table
            for (int i = 0; i < excelTable.Rows.Count; i++)
            {
                if (excelTable.Rows[i][0].ToString() == "Technical Audit Fee")
                {
                    TAFeeTableRow = i;
                    break;
                }
            }
            if (TAFeeTableRow == -1)
                throw new Exception("TAFee Table Not Found");


            //Get TA FEE Banding Row from TA Fee Table
            for (int i = TAFeeTableRow; i < excelTable.Rows.Count; i++)
            {
                if (excelTable.Rows[i][1].ToString() == "Rate")
                {
                    bandingRow = i;
                    break;
                }
            }
            if (bandingRow == -1)
                throw new Exception("TA Fee Banding Row Not Found");

            //Get TA Fee Value From TAFee Table 
            double taFeeValue = Convert.ToDouble(excelTable.Rows[bandingRow][RatingColumn]);
            TAFee = Convert.ToDouble(plot.ReconstructionCostValue * taFeeValue);
        }


        public void AtkinsFeeCaluclations()
        {
            // Reading Consumer Code Fee Table From CommercialHVS Pricing Setup Book            
            var atkinsPremiumTableRow = -1;


            //Get Consumer Code Fee per Plot Table from Structural Premium Table
            for (int i = 0; i < excelTable.Rows.Count; i++)
            {
                if (excelTable.Rows[i][0].ToString().Contains("Atkins Premium"))
                {
                    atkinsPremiumTableRow = i;
                    break;
                }
            }
            if (atkinsPremiumTableRow == -1)
                throw new Exception("Consumer Code Fee per Plot Table Not Found");

            double atkinsFeeValue = Convert.ToDouble(excelTable.Rows[atkinsPremiumTableRow + 2][2]);
            AtkinsPremium = Convert.ToDouble(atkinsFeeValue * 12);

        }

        public void VerfiyChannelIslandsResidentialHVSUIValues()
        {
            var productName = Premiums.eachplotrow.FirstOrDefault().ProductName;


            //Covers Elements 
            //Veridy UI Structural Values
            var uiStructuralFee = $"//th[@class='product-title'][text()='{productName}']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[text()='Structural']//following-sibling::td[@class='right-align'][1]";
            var StructuralFee = Driver.FindElement(By.XPath(uiStructuralFee)).Text.Replace("£", "");
            double UIStructuralFee = Math.Round(Convert.ToDouble(StructuralFee), 2);
            double StructuralFeeDiff;
            var uiStructuralIPTFee = $"//th[@class='product-title'][text()='{productName}']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[text()='Structural']//following-sibling::td[@class='right-align'][2]";
            var StructuralIPTFee = Driver.FindElement(By.XPath(uiStructuralIPTFee)).Text.Replace("£", "");
            double UIStructuralIPTFee = Math.Round(Convert.ToDouble(StructuralIPTFee), 2);
            double StructuralIPTDiff;
            var uiStructuralTotalFee = $"//th[@class='product-title'][text()='{productName}']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[text()='Structural']//following-sibling::th[@class='right-align'][1]";
            var StructuralTotalFee = Driver.FindElement(By.XPath(uiStructuralTotalFee)).Text.Replace("£", "");
            double UIStructuralTotalFee = Math.Round(Convert.ToDouble(StructuralTotalFee), 2);
            double StructuralTotalValue = Math.Round(Convert.ToDouble(UIStructuralFee + UIStructuralIPTFee), 2);
            double StructuralTotalDiff;

            if (UIStructuralFee != Premiums.StructuralFee || UIStructuralIPTFee != Premiums.StructuralIPTFee || UIStructuralTotalFee != StructuralTotalValue)
            {
                StructuralFeeDiff = Convert.ToDouble(UIStructuralFee - Premiums.StructuralFee);
                StructuralIPTDiff = Convert.ToDouble(UIStructuralIPTFee - Premiums.StructuralIPTFee);
                StructuralTotalDiff = Convert.ToDouble(UIStructuralTotalFee - StructuralTotalValue);
                if (StructuralFeeDiff >= -1.01 && StructuralFeeDiff <= 1.01)
                {
                    Premiums.StructuralFee = UIStructuralFee;
                }
                if (StructuralIPTDiff >= -1.01 && StructuralIPTDiff <= 1.01)
                {
                    Premiums.StructuralIPTFee = UIStructuralIPTFee;
                }
                if (StructuralTotalDiff >= -1.01 && StructuralTotalDiff <= 1.01)
                {
                    StructuralTotalValue = UIStructuralTotalFee;
                }

                Assert.AreEqual(UIStructuralFee, Premiums.StructuralFee, $"StructuralFee Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.EstimatedSalePrice}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                Assert.AreEqual(UIStructuralIPTFee, Premiums.StructuralIPTFee, $"StructuralIPTFee Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.EstimatedSalePrice}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                Assert.AreEqual(UIStructuralTotalFee, StructuralTotalValue, $"StructuralTotalFee Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.EstimatedSalePrice}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
            }
            else
            {

                Assert.AreEqual(UIStructuralFee, Premiums.StructuralFee, $"StructuralFee Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.EstimatedSalePrice}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                Assert.AreEqual(UIStructuralIPTFee, Premiums.StructuralIPTFee, $"StructuralIPTFee Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.EstimatedSalePrice}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                Assert.AreEqual(UIStructuralTotalFee, StructuralTotalValue, $"StructuralTotalFee Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.EstimatedSalePrice}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
            }
            if (Premiums.ProductsCovers.Any(x => x.Contains("Seepage")))
            {
                //UI Seepage Values
                var uiSeepageFee = $"//th[@class='product-title'][text()='{productName}']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[text()='Seepage']//following-sibling::td[@class='right-align'][1]";
                var SeepageFee = Driver.FindElement(By.XPath(uiSeepageFee)).Text.Replace("£", "");
                double UISeepageFee = Math.Round(Convert.ToDouble(SeepageFee), 2);
                double SeepageFeeDiff;

                var uiSeepageIPTFee = $"//th[@class='product-title'][text()='{productName}']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[text()='Seepage']//following-sibling::td[@class='right-align'][2]";
                var SeepageIPTFee = Driver.FindElement(By.XPath(uiSeepageIPTFee)).Text.Replace("£", "");
                double UISeepageIPTFee = Math.Round(Convert.ToDouble(SeepageIPTFee), 2);
                double SeepageIPTDiff;

                var uiSeepageTotalFee = $"//th[@class='product-title'][text()='{productName}']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[text()='Seepage']//following-sibling::th[@class='right-align'][1]";
                var SeepageTotalFee = Driver.FindElement(By.XPath(uiSeepageTotalFee)).Text.Replace("£", "");
                double UISeepageTotalFee = Math.Round(Convert.ToDouble(SeepageTotalFee), 2);
                double SeepageTotalValue = Math.Round(Convert.ToDouble(UISeepageFee + UISeepageIPTFee), 2);
                double SeepageTotalDiff;

                if (UISeepageFee != Premiums.SeepageFee || UISeepageIPTFee != Premiums.SeepageIPTFee || UISeepageTotalFee != SeepageTotalValue)
                {
                    SeepageFeeDiff = Convert.ToDouble(Premiums.SeepageFee - UISeepageFee);
                    SeepageIPTDiff = Convert.ToDouble(Premiums.SeepageIPTFee - UISeepageIPTFee);
                    SeepageTotalDiff = Convert.ToDouble(SeepageTotalValue - UISeepageTotalFee);
                    if (SeepageFeeDiff >= -1.01 && SeepageFeeDiff <= 1.01)
                    {
                        Premiums.SeepageFee = UISeepageFee;
                    }
                    if (SeepageIPTDiff >= -1.01 && SeepageIPTDiff <= 1.01)
                    {
                        Premiums.SeepageIPTFee = UISeepageIPTFee;
                    }
                    if (SeepageTotalDiff >= -1.01 && SeepageTotalDiff <= 1.01)
                    {
                        SeepageTotalValue = UISeepageTotalFee;
                    }

                    Assert.AreEqual(UISeepageFee, Premiums.SeepageFee, $"SeepageFee Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UISeepageIPTFee, Premiums.SeepageIPTFee, $"SeepageIPTFee Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UISeepageTotalFee, SeepageTotalValue, $"SeepageTotalValue Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                }
                else
                {
                    Assert.AreEqual(UISeepageFee, Premiums.SeepageFee, $"SeepageFee Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UISeepageIPTFee, Premiums.SeepageIPTFee, $"SeepageIPTFee Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UISeepageTotalFee, SeepageTotalValue, $"SeepageTotalValue Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                }
            }
            if (Premiums.ProductsCovers.Any(x => x.Contains("Waivers of Subrogation Rights - Engineer")))
            {

                //Verify UI Waivers of Subrogation Rights - Builder values
                var uiWaiversOfSubrogationRightsEngineerFee = $"//th[@class='product-title'][text()='{productName}']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[text()='Waivers of Subrogation Rights - Engineer']//following-sibling::td[@class='right-align'][1]";
                var WaiversOfSubrogationRightsEngineerFee = Driver.FindElement(By.XPath(uiWaiversOfSubrogationRightsEngineerFee)).Text.Replace("£", "");
                double UIWaiversOfSubrogationRightsEngineerFee = Math.Round(Convert.ToDouble(WaiversOfSubrogationRightsEngineerFee), 2);
                double WaiversOfSubrogationRightsEngineerFeeDiff;

                var uiWaiversOfSubrogationRightsEngineerIPTFee = $"//th[@class='product-title'][text()='{productName}']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[text()='Waivers of Subrogation Rights - Engineer']//following-sibling::td[@class='right-align'][2]";
                var WaiversOfSubrogationRightsEngineerIPTFee = Driver.FindElement(By.XPath(uiWaiversOfSubrogationRightsEngineerIPTFee)).Text.Replace("£", "");
                double UIWaiversOfSubrogationRightsEngineerIPTFee = Math.Round(Convert.ToDouble(WaiversOfSubrogationRightsEngineerIPTFee), 2);
                double WaiversOfSubrogationRightsEngineerIPTDiff;

                var uiWaiversOfSubrogationRightsEngineerTotalFee = $"//th[@class='product-title'][text()='{productName}']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[text()='Waivers of Subrogation Rights - Engineer']//following-sibling::th[@class='right-align'][1]";
                var WaiversOfSubrogationRightsEngineerTotalFee = Driver.FindElement(By.XPath(uiWaiversOfSubrogationRightsEngineerTotalFee)).Text.Replace("£", "");
                double UIWaiversOfSubrogationRightsEngineerTotalFee = Math.Round(Convert.ToDouble(WaiversOfSubrogationRightsEngineerTotalFee), 2);
                double WaiversOfSubrogationRightsEngineerTotalValue = Math.Round(Convert.ToDouble(UIWaiversOfSubrogationRightsEngineerFee + UIWaiversOfSubrogationRightsEngineerIPTFee), 2);
                double WaiversOfSubrogationRightsEngineerTotalDiff;

                if (UIWaiversOfSubrogationRightsEngineerFee != Premiums.WaiversOfSubrogationEngineerFee || UIWaiversOfSubrogationRightsEngineerIPTFee != Premiums.WaiversOfSubrogationEngineerIPTFee || UIWaiversOfSubrogationRightsEngineerTotalFee != WaiversOfSubrogationRightsEngineerTotalValue)
                {
                    WaiversOfSubrogationRightsEngineerFeeDiff = Convert.ToDouble(Premiums.WaiversOfSubrogationEngineerFee - UIWaiversOfSubrogationRightsEngineerFee);
                    WaiversOfSubrogationRightsEngineerIPTDiff = Convert.ToDouble(Premiums.WaiversOfSubrogationEngineerIPTFee - UIWaiversOfSubrogationRightsEngineerIPTFee);
                    WaiversOfSubrogationRightsEngineerTotalDiff = Convert.ToDouble(WaiversOfSubrogationRightsEngineerTotalValue - UIWaiversOfSubrogationRightsEngineerTotalFee);
                    if (WaiversOfSubrogationRightsEngineerFeeDiff >= -1.01 && WaiversOfSubrogationRightsEngineerFeeDiff <= 1.01)
                    {
                        Premiums.WaiversOfSubrogationEngineerFee = UIWaiversOfSubrogationRightsEngineerFee;
                    }

                    if (WaiversOfSubrogationRightsEngineerIPTDiff >= -1.01 && WaiversOfSubrogationRightsEngineerIPTDiff <= 1.01)
                    {
                        Premiums.WaiversOfSubrogationEngineerIPTFee = UIWaiversOfSubrogationRightsEngineerIPTFee;
                    }
                    if (WaiversOfSubrogationRightsEngineerTotalDiff >= -1.01 && WaiversOfSubrogationRightsEngineerTotalDiff <= 1.01)
                    {
                        WaiversOfSubrogationRightsEngineerTotalValue = UIWaiversOfSubrogationRightsEngineerTotalFee;
                    }

                    Assert.AreEqual(UIWaiversOfSubrogationRightsEngineerFee, Premiums.WaiversOfSubrogationEngineerFee, $"WaiversOfSubrogationEngineerFee Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UIWaiversOfSubrogationRightsEngineerIPTFee, Premiums.WaiversOfSubrogationEngineerIPTFee, $"WaiversOfSubrogationEngineerIPTFee Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UIWaiversOfSubrogationRightsEngineerTotalFee, WaiversOfSubrogationRightsEngineerTotalValue, $"WaiversOfSubrogationRightsEngineerTotalValue Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                }
                else
                {
                    Assert.AreEqual(UIWaiversOfSubrogationRightsEngineerFee, Premiums.WaiversOfSubrogationEngineerFee, $"WaiversOfSubrogationEngineerFee Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UIWaiversOfSubrogationRightsEngineerIPTFee, Premiums.WaiversOfSubrogationEngineerIPTFee, $"WaiversOfSubrogationEngineerIPTFee Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UIWaiversOfSubrogationRightsEngineerTotalFee, WaiversOfSubrogationRightsEngineerTotalValue, $"WaiversOfSubrogationRightsEngineerTotalValue Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                }
            }
            if (Premiums.ProductsCovers.Any(x => x.Contains("Waivers of Subrogation Rights - Builder")))
            {

                //ui Waivers of Subrogation Rights - Builder values
                var uiWaiversOfSubrogationRightsBuilderFee = $"//th[@class='product-title'][text()='{productName}']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[text()='Waivers of Subrogation Rights - Builder']//following-sibling::td[@class='right-align'][1]";
                var WaiversOfSubrogationRightsBuilderFee = Driver.FindElement(By.XPath(uiWaiversOfSubrogationRightsBuilderFee)).Text.Replace("£", "");
                double UIWaiversOfSubrogationRightsBuilderFee = Math.Round(Convert.ToDouble(WaiversOfSubrogationRightsBuilderFee), 2);
                double WaiversOfSubrogationRightsBuilderFeeDiff;

                var uiWaiversOfSubrogationRightsBuilderIPTFee = $"//th[@class='product-title'][text()='{productName}']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[text()='Waivers of Subrogation Rights - Builder']//following-sibling::td[@class='right-align'][2]";
                var WaiversOfSubrogationRightsBuilderIPTFee = Driver.FindElement(By.XPath(uiWaiversOfSubrogationRightsBuilderIPTFee)).Text.Replace("£", "");
                double UIWaiversOfSubrogationRightsBuilderIPTFee = Math.Round(Convert.ToDouble(WaiversOfSubrogationRightsBuilderIPTFee), 2);
                double WaiversOfSubrogationRightsBuilderIPTDiff;

                var uiWaiversOfSubrogationRightsBuilderTotalFee = $"//th[@class='product-title'][text()='{productName}']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[text()='Waivers of Subrogation Rights - Builder']//following-sibling::th[@class='right-align'][1]";
                var WaiversOfSubrogationRightsBuilderTotalFee = Driver.FindElement(By.XPath(uiWaiversOfSubrogationRightsBuilderTotalFee)).Text.Replace("£", "");
                double UIWaiversOfSubrogationRightsBuilderTotalFee = Math.Round(Convert.ToDouble(WaiversOfSubrogationRightsBuilderTotalFee), 2);
                double WaiversOfSubrogationRightsBuilderTotalValue = Math.Round(Convert.ToDouble(UIWaiversOfSubrogationRightsBuilderFee + UIWaiversOfSubrogationRightsBuilderIPTFee), 2);
                double WaiversOfSubrogationRightsBuilderTotalDiff;

                if (UIWaiversOfSubrogationRightsBuilderFee != Premiums.WaiversOfSubrogationBuilderFee || UIWaiversOfSubrogationRightsBuilderIPTFee != Premiums.WaiversOfSubrogationBuilderIPTFee || UIWaiversOfSubrogationRightsBuilderTotalFee != WaiversOfSubrogationRightsBuilderTotalValue)
                {
                    WaiversOfSubrogationRightsBuilderFeeDiff = Convert.ToDouble(Premiums.WaiversOfSubrogationBuilderFee - UIWaiversOfSubrogationRightsBuilderFee);
                    WaiversOfSubrogationRightsBuilderIPTDiff = Convert.ToDouble(Premiums.WaiversOfSubrogationBuilderIPTFee - UIWaiversOfSubrogationRightsBuilderIPTFee);
                    WaiversOfSubrogationRightsBuilderTotalDiff = Convert.ToDouble(WaiversOfSubrogationRightsBuilderTotalValue - UIWaiversOfSubrogationRightsBuilderTotalFee);
                    if (WaiversOfSubrogationRightsBuilderFeeDiff >= -1.01 && WaiversOfSubrogationRightsBuilderFeeDiff <= 1.01)
                    {
                        Premiums.WaiversOfSubrogationBuilderFee = UIWaiversOfSubrogationRightsBuilderFee;
                    }
                    if (WaiversOfSubrogationRightsBuilderIPTDiff >= -1.01 && WaiversOfSubrogationRightsBuilderIPTDiff <= 1.01)
                    {
                        Premiums.WaiversOfSubrogationBuilderIPTFee = UIWaiversOfSubrogationRightsBuilderIPTFee;
                    }
                    if (WaiversOfSubrogationRightsBuilderTotalDiff >= -1.01 && WaiversOfSubrogationRightsBuilderTotalDiff <= 1.01)
                    {
                        WaiversOfSubrogationRightsBuilderTotalValue = UIWaiversOfSubrogationRightsBuilderTotalFee;
                    }

                    Assert.AreEqual(UIWaiversOfSubrogationRightsBuilderFee, Premiums.WaiversOfSubrogationBuilderFee, $"WaiversOfSubrogationBuilderFee Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UIWaiversOfSubrogationRightsBuilderIPTFee, Premiums.WaiversOfSubrogationBuilderIPTFee, $"WaiversOfSubrogationBuilderIPTFee Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UIWaiversOfSubrogationRightsBuilderTotalFee, WaiversOfSubrogationRightsBuilderTotalValue, $"WaiversOfSubrogationRightsBuilderTotalValue Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                }
                else
                {
                    Assert.AreEqual(UIWaiversOfSubrogationRightsBuilderFee, Premiums.WaiversOfSubrogationBuilderFee, $"WaiversOfSubrogationBuilderFee Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UIWaiversOfSubrogationRightsBuilderIPTFee, Premiums.WaiversOfSubrogationBuilderIPTFee, $"WaiversOfSubrogationBuilderIPTFee Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UIWaiversOfSubrogationRightsBuilderTotalFee, WaiversOfSubrogationRightsBuilderTotalValue, $"WaiversOfSubrogationRightsBuilderTotalValue Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                }
            }

            //Services Elements  and  Fee values  
            //Technical Audit Fee values  
            if (Premiums.ProductServices.Any(x => x.Contains("Technical Audit Fee")))
            {
                var uiTechnicalAuditFee = $"//th[@class='product-title'][text()='{productName}']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[text()='Technical Audit Fee']//following-sibling::td[@class='right-align'][1]";
                var TechnicalAuditFee = Driver.FindElement(By.XPath(uiTechnicalAuditFee)).Text.Replace("£", "");
                double UITechnicalAuditFee = Math.Round(Convert.ToDouble(TechnicalAuditFee), 2);
                double TechnicalAuditFeeDiff;

                var uiTechnicalAuditVATFee = $"//th[@class='product-title'][text()='{productName}']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[text()='Technical Audit Fee']//following-sibling::td[@class='right-align'][2]";
                var TechnicalAuditVATFee = Driver.FindElement(By.XPath(uiTechnicalAuditVATFee)).Text.Replace("£", "");
                double UITechnicalAuditVATFee = Math.Round(Convert.ToDouble(TechnicalAuditVATFee), 2);
                double TechnicalAuditVATDiff;

                var uiTechnicalAuditTotalFee = $"//th[@class='product-title'][text()='{productName}']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[text()='Technical Audit Fee']//following-sibling::th[@class='right-align'][1]";
                var TechnicalAuditTotalFee = Driver.FindElement(By.XPath(uiTechnicalAuditTotalFee)).Text.Replace("£", "");
                double UITechnicalAuditTotalFee = Math.Round(Convert.ToDouble(TechnicalAuditTotalFee), 2);
                double TechnicalAuditTotalValue = Math.Round(Convert.ToDouble(UITechnicalAuditFee + UITechnicalAuditVATFee), 2);
                double TechnicalAuditTotalDiff;

                if (UITechnicalAuditFee != Premiums.TechnicalAuditFee || UITechnicalAuditVATFee != Premiums.TechnicalAuditVATFee || UITechnicalAuditTotalFee != TechnicalAuditTotalValue)
                {
                    TechnicalAuditFeeDiff = Convert.ToDouble(UITechnicalAuditFee - Premiums.TechnicalAuditFee);
                    TechnicalAuditVATDiff = Convert.ToDouble(UITechnicalAuditVATFee - Premiums.TechnicalAuditVATFee);
                    TechnicalAuditTotalDiff = Convert.ToDouble(UITechnicalAuditTotalFee - TechnicalAuditTotalValue);
                    if (TechnicalAuditFeeDiff >= -1.01 && TechnicalAuditFeeDiff <= 1.01)
                    {
                        Premiums.TechnicalAuditFee = UITechnicalAuditFee;
                    }

                    if (TechnicalAuditVATDiff >= -1.01 && TechnicalAuditVATDiff <= 1.01)
                    {
                        Premiums.TechnicalAuditVATFee = UITechnicalAuditVATFee;
                    }
                    if (TechnicalAuditTotalDiff >= -1.01 && TechnicalAuditTotalDiff <= 1.01)
                    {
                        TechnicalAuditTotalValue = UITechnicalAuditTotalFee;
                    }

                    Assert.AreEqual(UITechnicalAuditFee, Premiums.TechnicalAuditFee, $"TechnicalAuditFee Services Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.EstimatedSalePrice}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UITechnicalAuditVATFee, Premiums.TechnicalAuditVATFee, $"TechnicalAuditVATFee Services Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.EstimatedSalePrice}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UITechnicalAuditTotalFee, TechnicalAuditTotalValue, $"TechnicalAuditTotalValue Services Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.EstimatedSalePrice}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                }
                else
                {
                    Assert.AreEqual(UITechnicalAuditFee, Premiums.TechnicalAuditFee, $"TechnicalAuditFee Services Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.EstimatedSalePrice}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UITechnicalAuditVATFee, Premiums.TechnicalAuditVATFee, $"TechnicalAuditVATFee Services Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.EstimatedSalePrice}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UITechnicalAuditTotalFee, TechnicalAuditTotalValue, $"TechnicalAuditTotalValue Services Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.EstimatedSalePrice}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");

                }
            }

            //Refurbishment Assessment Fee values  
            if (Premiums.ProductServices.Any(x => x.Contains("Refurbishment Assessment Fee")))
            {
                var uiRefurbishmentFee = $"//th[@class='product-title'][text()='{productName}']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[text()='Refurbishment Assessment Fee']//following-sibling::td[@class='right-align'][1]";
                var RefurbishmentFee = Driver.FindElement(By.XPath(uiRefurbishmentFee)).Text.Replace("£", "");
                double UIRefurbishmentFee = Math.Round(Convert.ToDouble(RefurbishmentFee), 2);
                double RefurbishmentFeeDiff;

                var uiRefurbishmentVATFee = $"//th[@class='product-title'][text()='{productName}']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[text()='Refurbishment Assessment Fee']//following-sibling::td[@class='right-align'][2]";
                var RefurbishmentVATFee = Driver.FindElement(By.XPath(uiRefurbishmentVATFee)).Text.Replace("£", "");
                double UIRefurbishmentVATFee = Math.Round(Convert.ToDouble(RefurbishmentVATFee), 2);
                double RefurbishmentVATDiff;

                var uiRefurbishmentTotalFee = $"//th[@class='product-title'][text()='{productName}']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[text()='Refurbishment Assessment Fee']//following-sibling::th[@class='right-align'][1]";
                var RefurbishmentTotalFee = Driver.FindElement(By.XPath(uiRefurbishmentTotalFee)).Text.Replace("£", "");
                double UIRefurbishmentTotalFee = Math.Round(Convert.ToDouble(RefurbishmentTotalFee), 2);
                double RefurbishmentTotalValue = Math.Round(Convert.ToDouble(UIRefurbishmentFee + UIRefurbishmentVATFee), 2);
                double RefurbishmentTotalDiff;


                if (UIRefurbishmentFee != Premiums.RefurbishmentAssessmentFee || UIRefurbishmentVATFee != Premiums.RefurbishmentAssessmentVATFee || UIRefurbishmentTotalFee != RefurbishmentTotalValue)
                {
                    RefurbishmentFeeDiff = Convert.ToDouble(Premiums.RefurbishmentAssessmentFee - UIRefurbishmentFee);
                    RefurbishmentVATDiff = Convert.ToDouble(Premiums.RefurbishmentAssessmentVATFee - UIRefurbishmentVATFee);
                    RefurbishmentTotalDiff = Convert.ToDouble(RefurbishmentTotalValue - UIRefurbishmentTotalFee);
                    if (RefurbishmentFeeDiff >= -1.01 && RefurbishmentFeeDiff <= 1.01)
                    {
                        Premiums.RefurbishmentAssessmentFee = UIRefurbishmentFee;
                    }

                    if (RefurbishmentVATDiff >= -1.01 && RefurbishmentVATDiff <= 1.01)
                    {
                        Premiums.RefurbishmentAssessmentVATFee = UIRefurbishmentVATFee;
                    }
                    if (RefurbishmentTotalDiff >= -1.01 && RefurbishmentTotalDiff <= 1.01)
                    {
                        RefurbishmentTotalValue = UIRefurbishmentTotalFee;
                    }

                    Assert.AreEqual(UIRefurbishmentFee, Premiums.RefurbishmentAssessmentFee, $"RefurbishmentFee Services Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.EstimatedSalePrice}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UIRefurbishmentVATFee, Premiums.RefurbishmentAssessmentVATFee, $"RefurbishmentVATFee Services Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.EstimatedSalePrice}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UIRefurbishmentTotalFee, RefurbishmentTotalValue, $"RefurbishmentTotalValue Services Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.EstimatedSalePrice}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                }
                else
                {
                    Assert.AreEqual(UIRefurbishmentFee, Premiums.RefurbishmentAssessmentFee, $"RefurbishmentFee Services Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.EstimatedSalePrice}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UIRefurbishmentVATFee, Premiums.RefurbishmentAssessmentVATFee, $"RefurbishmentVATFee Services Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.EstimatedSalePrice}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UIRefurbishmentTotalFee, RefurbishmentTotalValue, $"RefurbishmentTotalValue Services Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.EstimatedSalePrice}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                }
            }

            //Administrator Fee values    
            if (Premiums.ProductServices.Any(x => x.Contains("Administration Fee")))
            {

                var uiAdministrationFee = $"//th[@class='product-title'][text()='{productName}']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[text()='Administration Fee']//following-sibling::td[@class='right-align'][1]";
                var AdministrationFee = Driver.FindElement(By.XPath(uiAdministrationFee)).Text.Replace("£", "");
                double UIAdministrationFee = Math.Round(Convert.ToDouble(AdministrationFee), 2);
                double AdministrationFeeDiff;

                var uiAdministrationVATFee = $"//th[@class='product-title'][text()='{productName}']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[text()='Administration Fee']//following-sibling::td[@class='right-align'][2]";
                var AdministrationVATFee = Driver.FindElement(By.XPath(uiAdministrationVATFee)).Text.Replace("£", "");
                double UIAdministrationVATFee = Math.Round(Convert.ToDouble(AdministrationVATFee), 2);
                double AdministrationVATDiff;

                var uiAdministrationTotalFee = $"//th[@class='product-title'][text()='{productName}']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[text()='Administration Fee']//following-sibling::th[@class='right-align'][1]";
                var AdministrationTotalFee = Driver.FindElement(By.XPath(uiAdministrationTotalFee)).Text.Replace("£", "");
                double UIAdministrationTotalFee = Math.Round(Convert.ToDouble(AdministrationTotalFee), 2);
                double AdministrationTotalValue = Math.Round(Convert.ToDouble(UIAdministrationFee + UIAdministrationVATFee), 2);
                double AdministrationTotalDiff;

                if (UIAdministrationFee != Premiums.AdministrationFee || UIAdministrationVATFee != Premiums.AdministrationVATFee || UIAdministrationTotalFee != AdministrationTotalValue)
                {
                    AdministrationFeeDiff = Convert.ToDouble(UIAdministrationFee - Premiums.AdministrationFee);
                    AdministrationVATDiff = Convert.ToDouble(UIAdministrationVATFee - Premiums.AdministrationVATFee);
                    AdministrationTotalDiff = Convert.ToDouble(UIAdministrationTotalFee - AdministrationTotalValue);
                    if (AdministrationFeeDiff >= -1.01 && AdministrationFeeDiff <= 1.01)
                    {
                        Premiums.AdministrationFee = UIAdministrationFee;
                    }

                    if (AdministrationVATDiff >= -1.01 && AdministrationVATDiff <= 1.01)
                    {
                        Premiums.AdministrationVATFee = UIAdministrationVATFee;
                    }
                    if (AdministrationTotalDiff >= 1.01 && AdministrationTotalDiff <= 1.01)
                    {
                        AdministrationTotalValue = UIAdministrationTotalFee;
                    }

                    Assert.AreEqual(UIAdministrationFee, Premiums.AdministrationFee, $"AdministrationFee Services Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.EstimatedSalePrice}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UIAdministrationVATFee, Premiums.AdministrationVATFee, $"AdministrationVATFee Services Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.EstimatedSalePrice}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UIAdministrationTotalFee, AdministrationTotalValue, $"AdministrationTotalValue Services Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.EstimatedSalePrice}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                }
                else
                {
                    Assert.AreEqual(UIAdministrationFee, Premiums.AdministrationFee, $"AdministrationFee Services Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.EstimatedSalePrice}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UIAdministrationVATFee, Premiums.AdministrationVATFee, $"AdministrationVATFee Services Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.EstimatedSalePrice}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UIAdministrationTotalFee, AdministrationTotalValue, $"AdministrationTotalValue Services Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.EstimatedSalePrice}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                }
            }
            //Atkins Fee values    
            if (Premiums.ProductServices.Any(x => x.Contains("Atkins Fee")))
            {

                var uiAtkinsFee = $"//th[@class='product-title'][text()='{productName}']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[text()='Atkins Fee']//following-sibling::td[@class='right-align'][1]";
                var AtkinsFee = Driver.FindElement(By.XPath(uiAtkinsFee)).Text.Replace("£", "");
                double UIAtkinsFee = Math.Round(Convert.ToDouble(AtkinsFee), 2);
                double AtkinsFeeDiff;

                var uiAtkinsVATFee = $"//th[@class='product-title'][text()='{productName}']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[text()='Atkins Fee']//following-sibling::td[@class='right-align'][2]";
                var AtkinsVATFee = Driver.FindElement(By.XPath(uiAtkinsVATFee)).Text.Replace("£", "");
                double UIAtkinsVATFee = Math.Round(Convert.ToDouble(AtkinsVATFee), 2);
                double AtkinsVATDiff;

                var uiAtkinsTotalFee = $"//th[@class='product-title'][text()='{productName}']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[text()='Atkins Fee']//following-sibling::th[@class='right-align'][1]";
                var AtkinsTotalFee = Driver.FindElement(By.XPath(uiAtkinsTotalFee)).Text.Replace("£", "");
                double UIAtkinsTotalFee = Math.Round(Convert.ToDouble(AtkinsTotalFee), 2);
                double AtkinsTotalValue = Math.Round(Convert.ToDouble(UIAtkinsFee + UIAtkinsVATFee), 2);
                double AtkinsTotalDiff;

                if (UIAtkinsFee != Premiums.AtkinsFee || UIAtkinsVATFee != Premiums.AtkinsVATFee || UIAtkinsTotalFee != AtkinsTotalValue)
                {
                    AtkinsFeeDiff = Convert.ToDouble(Premiums.AtkinsFee - UIAtkinsFee);
                    AtkinsVATDiff = Convert.ToDouble(Premiums.AtkinsVATFee - UIAtkinsVATFee);
                    AtkinsTotalDiff = Convert.ToDouble(AtkinsTotalValue - UIAtkinsTotalFee);
                    if (AtkinsFeeDiff >= -1.01 && AtkinsFeeDiff <= 1.01)
                    {
                        Premiums.AtkinsFee = UIAtkinsFee;
                    }

                    if (AtkinsVATDiff >= -1.01 && AtkinsVATDiff <= 1.01)
                    {
                        Premiums.AtkinsVATFee = UIAtkinsVATFee;
                    }
                    if (AtkinsTotalDiff >= -1.01 && AtkinsTotalDiff <= 1.01)
                    {
                        AtkinsTotalValue = UIAtkinsTotalFee;
                    }

                    Assert.AreEqual(UIAtkinsFee, Premiums.AtkinsFee, $"AtkinsFee Services Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.EstimatedSalePrice}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UIAtkinsVATFee, Premiums.AtkinsVATFee, $"AtkinsVATFee Services Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.EstimatedSalePrice}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UIAtkinsTotalFee, AtkinsTotalValue, $"AtkinsTotalValue Services Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.EstimatedSalePrice}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                }
                else
                {
                    Assert.AreEqual(UIAtkinsFee, Premiums.AtkinsFee, $"AtkinsFee Services Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.EstimatedSalePrice}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UIAtkinsVATFee, Premiums.AtkinsVATFee, $"AtkinsVATFee Services Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.EstimatedSalePrice}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UIAtkinsTotalFee, AtkinsTotalValue, $"AtkinsTotalValue Services Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.EstimatedSalePrice}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                }
            }
            //Cancellation Fee values  
            if (Premiums.ProductServices.Any(x => x.Contains("Cancellation Fee")))
            {


                var uiCancellationFee = $"//th[@class='product-title'][text()='{productName}']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[text()='Cancellation Fee']//following-sibling::td[@class='right-align'][2]";
                var CancellationFee = Driver.FindElement(By.XPath(uiCancellationFee)).Text.Replace("£", "");
                double UICancellationFee = Math.Round(Convert.ToDouble(CancellationFee), 2);
                double CancellationFeeDiff;

                var uiCancellationVATFee = $"//th[@class='product-title'][text()='{productName}']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[text()='Cancellation Fee']//following-sibling::td[@class='right-align'][2]";
                var CancellationVATFee = Driver.FindElement(By.XPath(uiCancellationVATFee)).Text.Replace("£", "");
                double UICancellationVATFee = Math.Round(Convert.ToDouble(CancellationVATFee), 2);
                double CancellationVATDiff;

                var uiCancellationTotalFee = $"//th[@class='product-title'][text()='{productName}']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[text()='Cancellation Fee']//following-sibling::td[@class='right-align'][2]";
                var CancellationTotalFee = Driver.FindElement(By.XPath(uiCancellationTotalFee)).Text.Replace("£", "");
                double UICancellationTotalFee = Math.Round(Convert.ToDouble(CancellationTotalFee), 2);
                double CancellationTotalValue = Math.Round(Convert.ToDouble(UICancellationFee + UICancellationVATFee), 2);
                double CancellationTotalDiff;


                if (UICancellationFee != Premiums.CancellationFee || UICancellationVATFee != Premiums.CancellationVATFee || UICancellationTotalFee != CancellationTotalValue)
                {
                    CancellationFeeDiff = Convert.ToDouble(UICancellationFee - Premiums.CancellationFee);
                    CancellationVATDiff = Convert.ToDouble(UICancellationVATFee - Premiums.CancellationVATFee);
                    CancellationTotalDiff = Convert.ToDouble(UICancellationTotalFee - CancellationTotalValue);
                    if (CancellationFeeDiff >= -1.01 && CancellationFeeDiff <= 1.01)
                    {
                        Premiums.CancellationFee = UICancellationFee;
                    }

                    if (CancellationVATDiff >= -1.01 && CancellationVATDiff <= 1.01)
                    {
                        Premiums.CancellationVATFee = UICancellationVATFee;
                    }
                    if (CancellationTotalDiff >= -1.01 && CancellationTotalDiff <= 1.01)
                    {
                        CancellationTotalValue = UICancellationTotalFee;
                    }

                    Assert.AreEqual(UICancellationFee, Premiums.CancellationFee, $"CancellationFee Services Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.EstimatedSalePrice}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UICancellationVATFee, Premiums.CancellationVATFee, $"CancellationVATFee Services Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.EstimatedSalePrice}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UICancellationTotalFee, CancellationTotalValue, $"CancellationTotalValue Services Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.EstimatedSalePrice}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                }
                else
                {
                    Assert.AreEqual(UICancellationFee, Premiums.CancellationFee, $"CancellationFee Services Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.EstimatedSalePrice}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UICancellationVATFee, Premiums.CancellationVATFee, $"CancellationVATFee Services Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.EstimatedSalePrice}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UICancellationTotalFee, CancellationTotalValue, $"CancellationTotalValue Services Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.EstimatedSalePrice}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                }
            }
        }

        public DataTable ReadChannelIslandsResidentialHVSMasterPremiumsPriceData(string fileName)
        {

            var result = new DataSet();                                                                             //Set the First Row as Column Name
            using (var stream = File.Open(fileName, FileMode.Open, FileAccess.Read))
            using (var excelReader = ExcelReaderFactory.CreateOpenXmlReader(stream)) //.xlsx
            {
                //Return as DataSet
                result = excelReader.AsDataSet(new ExcelDataSetConfiguration()
                {
                    ConfigureDataTable = (_) => new ExcelDataTableConfiguration()
                    {
                        UseHeaderRow = true
                    }
                });
            }
            //Get all the Tables
            DataTableCollection table = result.Tables;
            //Reading Last Sheet From Product Pricing Setup 
            DataTable resultTable = table["CI - Residential - HVS (1)"];
            return resultTable;
        }
    }
}