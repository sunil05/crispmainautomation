﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.PageObjects;
using RegressionPacks.Support;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace RegressionPacks.Pages
{
    public class AddPGQuotePage : Support.Pages
    {
        public IWebDriver wdriver;

        public AddPGQuotePage(ISearchContext driver)
        {
            PageFactory.InitElements(driver, this);
            wdriver = (RemoteWebDriver)driver;
        }

        //Key Site Details Page Elements
        [FindsBy(How = How.XPath, Using = "//crisp-dialog[@class='au-target']//div//crisp-header[@class='au-target']//nav")]
        public IWebElement AddNewQuoteDiv;   

        [FindsBy(How = How.XPath, Using = "//crisp-input-object/div[@class='au-target input-field']/span[@ref='inputContainer']/span/div/span[@class='au-target']")]
        public IList<IWebElement> QuoteRecipientInput;

        [FindsBy(How = How.XPath, Using = "//crisp-content/crisp-input-object//div//label[text()='Quote Recipient']")]
        public IWebElement QuoteRecipientLabel;

        [FindsBy(How = How.XPath, Using = "//contact-list/crisp-list[@ref='listElm']/ul[@ref='theList']/li[@class='au-target collection-item']")]
        public IList<IWebElement> ChooseBuilder;

        [FindsBy(How = How.CssSelector, Using = "crisp-input-text[label = 'Search'] > div > label")]
        public IWebElement SearchLabel;

        [FindsBy(How = How.CssSelector, Using = "crisp-input-text[label = 'Search'] > div > input")]
        public IWebElement SearchInput;

        [FindsBy(How = How.XPath, Using = "//crisp-list[@ref='listElm']//ul//li[@class='au-target collection-item']//crisp-list-item//a[contains(@class,'au-target row list-item-contents clickable hasAvatar')]")]
        public IList<IWebElement> QuoteRecipientList;

        [FindsBy(How = How.XPath, Using = "//crisp-input-object//div//label[contains(text(),'Office')]")]
        public IList<IWebElement> QuoteRecipientOfficeLabel;

        [FindsBy(How = How.XPath, Using = "//ul//li[@class='au-target collection-item']//crisp-list-item/a")]
        public IList<IWebElement> QuoteRecipientOfficeInput;

        [FindsBy(How = How.XPath, Using = "//crisp-content/crisp-input-object//div//label[text()='Site Address']")]
        public IWebElement SiteAddressLabel;

        [FindsBy(How = How.XPath, Using = "//crisp-content/crisp-input-object//div//span//view-address")]
        public IList<IWebElement> SiteAddressInput;

        [FindsBy(How = How.XPath, Using = "//crisp-button/span/button[text()='Enter Address Using Postcode'][@class='au-target waves-effect waves-light btn-flat']")]
        public IWebElement EnterAddressUsingPostcodeButton;

        [FindsBy(How = How.CssSelector, Using = "crisp-input-text[label='Enter postcode to search'] > div > label")]
        public IWebElement PostcodeSearchLabel;

        [FindsBy(How = How.CssSelector, Using = "crisp-input-text[label='Enter postcode to search'] > div > input")]
        public IWebElement PostcodeInput;

        [FindsBy(How = How.XPath, Using = "//crisp-picker[@label='Address']/div/div/input[@class='select-dropdown']")]
        public IWebElement AddressDropdown;

        [FindsBy(How = How.XPath, Using = "//ul[@class='dropdown-content select-dropdown active']/li[1]")]
        public IWebElement AddressDropdownInput;

        [FindsBy(How = How.XPath, Using = "//crisp-button/span/button[@class='au-target waves-effect waves-light btn'][text()='Set Address']")]
        public IWebElement SetAddressButton;

        [FindsBy(How = How.XPath, Using = "//crisp-picker[@label='Insurance Agreement Group']//div[@class='au-target input-field']//div")]
        public IList<IWebElement> InsuranceGroup;

        [FindsBy(How = How.XPath, Using = "//ul[@class='dropdown-content select-dropdown active']//li//span[text()='Aviva']")]
        public IWebElement InsuranceGroupInput;

        [FindsBy(How = How.XPath, Using = "//crisp-input-object[@label='Quote Application Documents']//span[contains(@class,'au-target input-container multi-item')]//span")]
        public IWebElement AddQuoteAppDocs;

        [FindsBy(How = How.XPath, Using = "//crisp-dialog//crisp-header//div[text()='Upload document']")]
        public IWebElement UploadDocWizard;

        [FindsBy(How = How.XPath, Using = "//crisp-input-file[@label='Select file...']//div//div//input[@class='au-target']")]
        public IWebElement UploadDoc;

        [FindsBy(How = How.XPath, Using = "//crisp-button[@click.delegate='ok()'][@class='au-target']//span//button[text()='OK'][@class='au-target waves-effect waves-light btn-flat']")]
        public IWebElement UploadOkButton;

        [FindsBy(How = How.XPath, Using = "//crisp-button[@class='au-target'][@click.delegate='wizard.goForward()']//span//button[@class='au-target waves-effect waves-light btn-flat'][text()='Next']")]
        public IWebElement NextButton;

        [FindsBy(How = How.XPath, Using = "//crisp-footer/nav/div/crisp-button[@class='au-target']/span/button[text()='Save']")]
        public IWebElement SaveButton;

        [FindsBy(How = How.XPath, Using = "//crisp-footer/nav/div/crisp-button[@class='au-target']/span/button[text()='Submit']")]
        public IWebElement SubmitButton;

        [FindsBy(How = How.XPath, Using = "//crisp-button[@class='au-target']/span/button[text()='Ok']")]
        public IWebElement OkButton;

        [FindsBy(How = How.XPath, Using = "//div//crisp-header-title[@class='au-target'][1]/div[@class='title']")]
        public IWebElement QuoteRefDetails;

        [FindsBy(How = How.XPath, Using = "//ul[@class='dropdown-content select-dropdown active']//li//span")]
        public IList<IWebElement> InsuranceGroupInputs;

        //Additional Buttons 

        [FindsBy(How = How.XPath, Using = "//crisp-button[@click.delegate='additionalButtonClicked(btn)'][@class='au-target']//button[text()='Add Person']")]
        public IWebElement AddPersonButton;

        [FindsBy(How = How.XPath, Using = "//crisp-button[@click.delegate='additionalButtonClicked(btn)'][@class='au-target']//button[text()='Add Company']")]
        public IWebElement AddCompanyButton;

        [FindsBy(How = How.XPath, Using = "//crisp-button[@click.delegate='additionalButtonClicked(btn)'][@class='au-target']//button[text()='Add Employee']")]
        public IWebElement AddEmployeeButton;

        [FindsBy(How = How.XPath, Using = "//crisp-input-radio[@label='Brand']/div/ul/li/label[text()='Premier Guarantee']")]
        public IList<IWebElement> PGRadioButton;

        //Site Address Elements 

        [FindsBy(How = How.XPath, Using = "//div[@class='card-content']//crisp-input-text[@label='Address Line 1']//div//label[@class='au-target']")]
        public IWebElement AddressLine1Label;

        [FindsBy(How = How.XPath, Using = "//div[@class='card-content']//crisp-input-text[@label='Address Line 1']//div//input")]
        public IWebElement AddressLine1Input;

        [FindsBy(How = How.XPath, Using = "//div[@class='card-content']//crisp-input-text[@label='Town']//div//label[@class='au-target']")]
        public IWebElement TownLabel;

        [FindsBy(How = How.XPath, Using = "//div[@class='card-content']//crisp-input-text[@label='Town']//div//input")]
        public IWebElement TownInput;

        [FindsBy(How = How.XPath, Using = "//div[@class='card-content']//crisp-input-text[@label='Postcode']//div//label[@class='au-target']")]
        public IWebElement SitePostcodeLabel;

        [FindsBy(How = How.XPath, Using = "//div[@class='card-content']//crisp-input-text[@label='Postcode']//div//input")]
        public IWebElement SitePostcodeInput;
        public void UATSiteAddress()
        {
            Random rnd = new Random();
            int number = rnd.Next(1, 3);
            CloseSpinneronDiv();
            WaitForElement(AddressLine1Label);
            //Thread.Sleep(2000);
            WaitForElement(AddressLine1Input);
            AddressLine1Input.SendKeys($"PG UAT Surveyor Site {number}");
            WaitForElement(TownLabel);
            TownLabel.Click();
            //Thread.Sleep(2000);
            WaitForElement(TownInput);
            TownInput.SendKeys("Test Town");
            WaitForElement(SitePostcodeLabel);
            SitePostcodeLabel.Click();
            //Thread.Sleep(2000);
            WaitForElement(SitePostcodeInput);
            SitePostcodeInput.SendKeys("L1");
            //Thread.Sleep(2000);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(2000);
        }

        // PG Create Quote - Developmenet information details 
        public void PGKeySiteDetailsPage()
        {
            WaitForElement(AddLABCQuotePage.StepTitle);
            Assert.IsTrue(AddLABCQuotePage.StepTitle.Text.Contains("KEY SITE DETAILS"), $"Failed to Display {AddLABCQuotePage.StepTitle.Text}");
            if (PGRadioButton.Count > 0)
            {
                WaitForElements(PGRadioButton);
                PGRadioButton[0].Click();
            }
            if (QuoteRecipientInput.Count == 0)
            {
                WaitForElement(QuoteRecipientLabel);
                QuoteRecipientLabel.Click();
                CloseSpinneronDiv();
                //Thread.Sleep(500);
                WaitForElement(SearchLabel);
                CloseSpinneronDiv();
                SearchLabel.Click();
                //Thread.Sleep(1000);
                WaitForElement(SearchInput);
                //Thread.Sleep(1000);
                SearchInput.SendKeys("Mr Sunil ");
                //Thread.Sleep(500);
                SearchInput.SendKeys("Sunkishala");
                Thread.Sleep(1000);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                Thread.Sleep(1000);
                if (QuoteRecipientList.Count > 0)
                {
                    //Thread.Sleep(3000);
                    CloseSpinneronDiv();
                    //Thread.Sleep(3000);
                    WaitForElement(QuoteRecipientList[0]);
                    //Thread.Sleep(3000);
                    CloseSpinneronDiv();
                    //Thread.Sleep(3000);
                    QuoteRecipientList[0].Click();
                    CloseSpinneronDiv();
                    //Thread.Sleep(3000);
                }
                else
                {
                    WaitForElement(AddPersonButton);
                    AddPersonButton.Click();
                    AdditionalMethodsPage.AddPerson();
                    Thread.Sleep(1000);
                    CloseSpinneronPage();
                    CloseSpinneronDiv();
                    Thread.Sleep(1000);
                }
            }

            if (SiteAddressInput.Count == 0)
            {
                Thread.Sleep(1000);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                Thread.Sleep(1000);
                WaitForElement(SiteAddressLabel);
                //Thread.Sleep(2000);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                //Thread.Sleep(2000);
                SiteAddressLabel.Click();
                Thread.Sleep(1000);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                Thread.Sleep(1000);
                UATSiteAddress();
                //WaitForElement(EnterAddressUsingPostcodeButton);
                //EnterAddressUsingPostcodeButton.Click();
                //WaitForElement(PostcodeSearchLabel);
                //PostcodeSearchLabel.Click();
                //WaitForElement(PostcodeInput);
                //PostcodeInput.SendKeys("CH41 1AU");
                //WaitForElement(AddressDropdown);
                //AddressDropdown.Click();
                Thread.Sleep(500);
                //WaitForElement(AddressDropdownInput);
                //AddressDropdownInput.Click();
                WaitForElement(SetAddressButton);
                SetAddressButton.Click();
            }
            //Thread.Sleep(1000);
            CloseCrispCard();
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(1000);
            if (QuoteRecipientOfficeLabel.Count > 0)
            {
                //Thread.Sleep(1000);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                //Thread.Sleep(1000);
                QuoteRecipientOfficeLabel[0].Click();
                //Thread.Sleep(1000);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                //Thread.Sleep(1000);
                WaitForElements(QuoteRecipientOfficeInput);
                //Thread.Sleep(1000);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                //Thread.Sleep(1000);
                if (QuoteRecipientOfficeInput.Count > 0)
                {
                    QuoteRecipientOfficeInput[0].Click();
                    //Thread.Sleep(1000);
                }
            }
            CloseSpinneronDiv();
            CloseSpinneronPage();
            CloseCrispCard();
            Thread.Sleep(1000);
            if (Statics.ProductNameList.Any(x => x.Contains("High Value")))
            {
                if (InsuranceGroup.Count > 0)
                {
                    Thread.Sleep(1000);
                    CloseSpinneronPage();
                    CloseSpinneronDiv();
                    Thread.Sleep(1000);
                    WaitForElement(InsuranceGroup[0]);
                    WaitForElementToClick(InsuranceGroup[0]);
                    InsuranceGroup[0].Click();
                    CloseSpinneronDiv();
                    //Thread.Sleep(1000);
                    WaitForElements(InsuranceGroupInputs);
                    var insuranceGroup = InsuranceGroupInputs.FirstOrDefault(o => o.Text.Contains("Axa HVS"));
                    if (insuranceGroup.Displayed)
                    {
                        insuranceGroup.Click();
                    }
                    CloseSpinneronDiv();
                    //Thread.Sleep(1000);
                }
            }           
            CloseSpinneronDiv();
            CloseSpinneronPage();
            CloseCrispCard();
            Thread.Sleep(1000);
            WaitForElement(AddQuoteAppDocs);
            AddQuoteAppDocs.Click();
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(2000);
            WaitForElement(UploadDocWizard);
            Assert.IsTrue(UploadDocWizard.Displayed);
            var fileInfo = new FileInfo(ExtensionMethods.GetAbsolutePath(@"TestData\QuotesData\CRISP install guide.docx"));
            UploadDoc.SendKeys(fileInfo.ToString());
            //Thread.Sleep(1000);
            CloseSpinneronDiv();
            WaitForElement(UploadOkButton);
            //Thread.Sleep(500);
            UploadOkButton.Click();
            //Thread.Sleep(1000);
            WaitForElement(NextButton);
            NextButton.Click();
            CloseSpinneronDiv();
            CloseSpinneronPage();
        }
        // Adding Key Site Details for LABC quote 
        public void PGKeySiteDetailsForAviva()
        {
            WaitForElement(AddLABCQuotePage.StepTitle);
            Assert.IsTrue(AddLABCQuotePage.StepTitle.Text.Contains("KEY SITE DETAILS"), $"Failed to Display {AddLABCQuotePage.StepTitle.Text}");
            //Adding Key SiteDetails Page 
            if (PGRadioButton.Count > 0)
            {
                WaitForElements(PGRadioButton);
                PGRadioButton[0].Click();
            }

            if (QuoteRecipientInput.Count == 0)
            {
                WaitForElement(QuoteRecipientLabel);
                QuoteRecipientLabel.Click();
                CloseSpinneronDiv();
                //Thread.Sleep(500);
                WaitForElement(SearchLabel);
                CloseSpinneronDiv();
                SearchLabel.Click();
                //Thread.Sleep(1000);
                WaitForElement(SearchInput);
                //Thread.Sleep(1000);
                SearchInput.SendKeys("Mr Sunil ");
                //Thread.Sleep(500);
                SearchInput.SendKeys("Sunkishala");
                Thread.Sleep(1000);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                Thread.Sleep(1000);
                if (QuoteRecipientList.Count > 0)
                {
                    //Thread.Sleep(3000);
                    CloseSpinneronDiv();
                    //Thread.Sleep(3000);
                    WaitForElement(QuoteRecipientList[0]);
                    //Thread.Sleep(3000);
                    CloseSpinneronDiv();
                    //Thread.Sleep(3000);
                    QuoteRecipientList[0].Click();
                    CloseSpinneronDiv();
                    //Thread.Sleep(3000);
                }
                else
                {
                    WaitForElement(AddPersonButton);
                    AddPersonButton.Click();
                    AdditionalMethodsPage.AddPerson();
                    Thread.Sleep(1000);
                    CloseSpinneronPage();
                    CloseSpinneronDiv();
                    Thread.Sleep(1000);
                }
            }

            if (SiteAddressInput.Count == 0)
            {
                Thread.Sleep(1000);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                Thread.Sleep(1000);
                WaitForElement(SiteAddressLabel);
                //Thread.Sleep(2000);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                //Thread.Sleep(2000);
                SiteAddressLabel.Click();
                Thread.Sleep(1000);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                Thread.Sleep(1000);
                UATSiteAddress();
                //WaitForElement(EnterAddressUsingPostcodeButton);
                //EnterAddressUsingPostcodeButton.Click();
                //WaitForElement(PostcodeSearchLabel);
                //PostcodeSearchLabel.Click();
                //WaitForElement(PostcodeInput);
                //PostcodeInput.SendKeys("CH41 1AU");
                //WaitForElement(AddressDropdown);
                //AddressDropdown.Click();
                Thread.Sleep(500);
                //WaitForElement(AddressDropdownInput);
                //AddressDropdownInput.Click();
                WaitForElement(SetAddressButton);
                SetAddressButton.Click();
            }
            //Thread.Sleep(1000);
            CloseCrispCard();
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(1000);
            if (QuoteRecipientOfficeLabel.Count > 0)
            {
                //Thread.Sleep(1000);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                //Thread.Sleep(1000);
                QuoteRecipientOfficeLabel[0].Click();
                //Thread.Sleep(1000);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                //Thread.Sleep(1000);
                WaitForElements(QuoteRecipientOfficeInput);
                //Thread.Sleep(1000);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                //Thread.Sleep(1000);
                if (QuoteRecipientOfficeInput.Count > 0)
                {
                    QuoteRecipientOfficeInput[0].Click();
                    //Thread.Sleep(1000);
                }
            }
            CloseSpinneronDiv();
            CloseSpinneronPage();
            CloseCrispCard();
            Thread.Sleep(1000);
            if (Statics.ProductNameList.Any(x => x.Contains("High Value")))
            {
                if (InsuranceGroup.Count > 0)
                {
                    Thread.Sleep(1000);
                    CloseSpinneronPage();
                    CloseSpinneronDiv();
                    Thread.Sleep(1000);
                    WaitForElement(InsuranceGroup[0]);
                    WaitForElementToClick(InsuranceGroup[0]);
                    InsuranceGroup[0].Click();
                    CloseSpinneronDiv();
                    //Thread.Sleep(1000);
                    WaitForElements(AddLABCQuotePage.InsuranceGroupInputs);
                    var insuranceGroup = AddLABCQuotePage.InsuranceGroupInputs.FirstOrDefault(o => o.Text.Contains("Aviva"));
                    if (insuranceGroup.Displayed)
                    {
                        insuranceGroup.Click();
                    }
                    CloseSpinneronDiv();
                    //Thread.Sleep(1000);
                }

            }
           
            CloseSpinneronDiv();
            CloseSpinneronPage();
            CloseCrispCard();
            Thread.Sleep(1000);
            WaitForElement(AddQuoteAppDocs);
            AddQuoteAppDocs.Click();
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(2000);
            WaitForElement(UploadDocWizard);
            Assert.IsTrue(UploadDocWizard.Displayed);
            var fileInfo = new FileInfo(ExtensionMethods.GetAbsolutePath(@"TestData\QuotesData\CRISP install guide.docx"));
            UploadDoc.SendKeys(fileInfo.ToString());
            //Thread.Sleep(1000);
            CloseSpinneronDiv();
            WaitForElement(UploadOkButton);
            //Thread.Sleep(500);
            UploadOkButton.Click();
            //Thread.Sleep(1000);
            WaitForElement(NextButton);
            NextButton.Click();
            CloseSpinneronDiv();
            CloseSpinneronPage();
        }
        // PG Create Quote - Developmenet information details 
        public void PGKeySiteDetailsPageOnLatestProductVersion()
        {
            WaitForElement(AddLABCQuotePage.StepTitle);
            Assert.IsTrue(AddLABCQuotePage.StepTitle.Text.Contains("KEY SITE DETAILS"), $"Failed to Display {AddLABCQuotePage.StepTitle.Text}");
            if (PGRadioButton.Count > 0)
            {
                WaitForElements(PGRadioButton);
                PGRadioButton[0].Click();
            }
            if (QuoteRecipientInput.Count == 0)
            {
                WaitForElement(QuoteRecipientLabel);
                QuoteRecipientLabel.Click();
                CloseSpinneronDiv();
                //Thread.Sleep(500);
                WaitForElement(SearchLabel);
                CloseSpinneronDiv();
                SearchLabel.Click();
                //Thread.Sleep(1000);
                WaitForElement(SearchInput);
                //Thread.Sleep(1000);
                SearchInput.SendKeys("Mr Sunil ");
                //Thread.Sleep(500);
                SearchInput.SendKeys("Sunkishala");
                Thread.Sleep(1000);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                Thread.Sleep(1000);
                if (QuoteRecipientList.Count > 0)
                {
                    //Thread.Sleep(3000);
                    CloseSpinneronDiv();
                    //Thread.Sleep(3000);
                    WaitForElement(QuoteRecipientList[0]);
                    //Thread.Sleep(3000);
                    CloseSpinneronDiv();
                    //Thread.Sleep(3000);
                    QuoteRecipientList[0].Click();
                    CloseSpinneronDiv();
                    //Thread.Sleep(3000);
                }
                else
                {
                    WaitForElement(AddPersonButton);
                    AddPersonButton.Click();
                    AdditionalMethodsPage.AddPerson();
                    Thread.Sleep(1000);
                    CloseSpinneronPage();
                    CloseSpinneronDiv();
                    Thread.Sleep(1000);
                }
            }

            if (SiteAddressInput.Count == 0)
            {
                Thread.Sleep(1000);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                Thread.Sleep(1000);
                WaitForElement(SiteAddressLabel);
                //Thread.Sleep(2000);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                //Thread.Sleep(2000);
                SiteAddressLabel.Click();
                Thread.Sleep(1000);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                Thread.Sleep(1000);
                UATSiteAddress();
                //WaitForElement(EnterAddressUsingPostcodeButton);
                //EnterAddressUsingPostcodeButton.Click();
                //WaitForElement(PostcodeSearchLabel);
                //PostcodeSearchLabel.Click();
                //WaitForElement(PostcodeInput);
                //PostcodeInput.SendKeys("CH41 1AU");
                //WaitForElement(AddressDropdown);
                //AddressDropdown.Click();
                Thread.Sleep(500);
                //WaitForElement(AddressDropdownInput);
                //AddressDropdownInput.Click();
                WaitForElement(SetAddressButton);
                SetAddressButton.Click();
            }
            //Thread.Sleep(1000);
            CloseCrispCard();
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(1000);
            if (QuoteRecipientOfficeLabel.Count > 0)
            {
                //Thread.Sleep(1000);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                //Thread.Sleep(1000);
                QuoteRecipientOfficeLabel[0].Click();
                //Thread.Sleep(1000);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                //Thread.Sleep(1000);
                WaitForElements(QuoteRecipientOfficeInput);
                //Thread.Sleep(1000);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                //Thread.Sleep(1000);
                if (QuoteRecipientOfficeInput.Count > 0)
                {
                    QuoteRecipientOfficeInput[0].Click();
                    //Thread.Sleep(1000);
                }
            }
            CloseSpinneronDiv();
            CloseSpinneronPage();
            CloseCrispCard();
            Thread.Sleep(1000);
            if (Statics.ProductNameList.Any(x => x.Contains("High Value")))
            {
                if (InsuranceGroup.Count > 0)
                {
                    Thread.Sleep(1000);
                    CloseSpinneronPage();
                    CloseSpinneronDiv();
                    Thread.Sleep(1000);
                    WaitForElement(InsuranceGroup[0]);
                    WaitForElementToClick(InsuranceGroup[0]);
                    InsuranceGroup[0].Click();
                    CloseSpinneronDiv();
                    //Thread.Sleep(1000);
                    WaitForElements(InsuranceGroupInputs);
                    var insuranceGroup = InsuranceGroupInputs.FirstOrDefault(o => o.Text.Contains("Axa HVS"));
                    if (insuranceGroup.Displayed)
                    {
                        insuranceGroup.Click();
                    }
                    CloseSpinneronDiv();
                    //Thread.Sleep(1000);
                }
            }
            AddLABCQuotePage.SelectProductVersionDate();
            CloseSpinneronDiv();
            CloseSpinneronPage();
            CloseCrispCard();
            Thread.Sleep(1000);
            WaitForElement(AddQuoteAppDocs);
            AddQuoteAppDocs.Click();
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(2000);
            WaitForElement(UploadDocWizard);
            Assert.IsTrue(UploadDocWizard.Displayed);
            var fileInfo = new FileInfo(ExtensionMethods.GetAbsolutePath(@"TestData\QuotesData\CRISP install guide.docx"));
            UploadDoc.SendKeys(fileInfo.ToString());
            //Thread.Sleep(1000);
            CloseSpinneronDiv();
            WaitForElement(UploadOkButton);
            //Thread.Sleep(500);
            UploadOkButton.Click();
            //Thread.Sleep(1000);
            WaitForElement(NextButton);
            NextButton.Click();
            CloseSpinneronDiv();
            CloseSpinneronPage();
        }
        // Adding Key Site Details for LABC quote 
        public void PGKeySiteDetailsForAvivaOnLatestProductVersion()
        {
            WaitForElement(AddLABCQuotePage.StepTitle);
            Assert.IsTrue(AddLABCQuotePage.StepTitle.Text.Contains("KEY SITE DETAILS"), $"Failed to Display {AddLABCQuotePage.StepTitle.Text}");
            //Adding Key SiteDetails Page 
            if (PGRadioButton.Count > 0)
            {
                WaitForElements(PGRadioButton);
                PGRadioButton[0].Click();
            }

            if (QuoteRecipientInput.Count == 0)
            {
                WaitForElement(QuoteRecipientLabel);
                QuoteRecipientLabel.Click();
                CloseSpinneronDiv();
                //Thread.Sleep(500);
                WaitForElement(SearchLabel);
                CloseSpinneronDiv();
                SearchLabel.Click();
                //Thread.Sleep(1000);
                WaitForElement(SearchInput);
                //Thread.Sleep(1000);
                SearchInput.SendKeys("Mr Sunil ");
                //Thread.Sleep(500);
                SearchInput.SendKeys("Sunkishala");
                Thread.Sleep(1000);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                Thread.Sleep(1000);
                if (QuoteRecipientList.Count > 0)
                {
                    //Thread.Sleep(3000);
                    CloseSpinneronDiv();
                    //Thread.Sleep(3000);
                    WaitForElement(QuoteRecipientList[0]);
                    //Thread.Sleep(3000);
                    CloseSpinneronDiv();
                    //Thread.Sleep(3000);
                    QuoteRecipientList[0].Click();
                    CloseSpinneronDiv();
                    //Thread.Sleep(3000);
                }
                else
                {
                    WaitForElement(AddPersonButton);
                    AddPersonButton.Click();
                    AdditionalMethodsPage.AddPerson();
                    Thread.Sleep(1000);
                    CloseSpinneronPage();
                    CloseSpinneronDiv();
                    Thread.Sleep(1000);
                }
            }

            if (SiteAddressInput.Count == 0)
            {
                Thread.Sleep(1000);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                Thread.Sleep(1000);
                WaitForElement(SiteAddressLabel);
                //Thread.Sleep(2000);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                //Thread.Sleep(2000);
                SiteAddressLabel.Click();
                Thread.Sleep(1000);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                Thread.Sleep(1000);
                UATSiteAddress();
                //WaitForElement(EnterAddressUsingPostcodeButton);
                //EnterAddressUsingPostcodeButton.Click();
                //WaitForElement(PostcodeSearchLabel);
                //PostcodeSearchLabel.Click();
                //WaitForElement(PostcodeInput);
                //PostcodeInput.SendKeys("CH41 1AU");
                //WaitForElement(AddressDropdown);
                //AddressDropdown.Click();
                Thread.Sleep(500);
                //WaitForElement(AddressDropdownInput);
                //AddressDropdownInput.Click();
                WaitForElement(SetAddressButton);
                SetAddressButton.Click();
            }
            //Thread.Sleep(1000);
            CloseCrispCard();
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(1000);
            if (QuoteRecipientOfficeLabel.Count > 0)
            {
                //Thread.Sleep(1000);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                //Thread.Sleep(1000);
                QuoteRecipientOfficeLabel[0].Click();
                //Thread.Sleep(1000);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                //Thread.Sleep(1000);
                WaitForElements(QuoteRecipientOfficeInput);
                //Thread.Sleep(1000);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                //Thread.Sleep(1000);
                if (QuoteRecipientOfficeInput.Count > 0)
                {
                    QuoteRecipientOfficeInput[0].Click();
                    //Thread.Sleep(1000);
                }
            }
            CloseSpinneronDiv();
            CloseSpinneronPage();
            CloseCrispCard();
            Thread.Sleep(1000);
            if (Statics.ProductNameList.Any(x => x.Contains("High Value")))
            {
                if (InsuranceGroup.Count > 0)
                {
                    Thread.Sleep(1000);
                    CloseSpinneronPage();
                    CloseSpinneronDiv();
                    Thread.Sleep(1000);
                    WaitForElement(InsuranceGroup[0]);
                    WaitForElementToClick(InsuranceGroup[0]);
                    InsuranceGroup[0].Click();
                    CloseSpinneronDiv();
                    //Thread.Sleep(1000);
                    WaitForElements(AddLABCQuotePage.InsuranceGroupInputs);
                    var insuranceGroup = AddLABCQuotePage.InsuranceGroupInputs.FirstOrDefault(o => o.Text.Contains("Aviva"));
                    if (insuranceGroup.Displayed)
                    {
                        insuranceGroup.Click();
                    }
                    CloseSpinneronDiv();
                    //Thread.Sleep(1000);
                }

            }
            AddLABCQuotePage.SelectProductVersionDate();
            CloseSpinneronDiv();
            CloseSpinneronPage();
            CloseCrispCard();
            Thread.Sleep(1000);
            WaitForElement(AddQuoteAppDocs);
            AddQuoteAppDocs.Click();
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(2000);
            WaitForElement(UploadDocWizard);
            Assert.IsTrue(UploadDocWizard.Displayed);
            var fileInfo = new FileInfo(ExtensionMethods.GetAbsolutePath(@"TestData\QuotesData\CRISP install guide.docx"));
            UploadDoc.SendKeys(fileInfo.ToString());
            //Thread.Sleep(1000);
            CloseSpinneronDiv();
            WaitForElement(UploadOkButton);
            //Thread.Sleep(500);
            UploadOkButton.Click();
            //Thread.Sleep(1000);
            WaitForElement(NextButton);
            NextButton.Click();
            CloseSpinneronDiv();
            CloseSpinneronPage();
        }
        public void SubmitQuote(string masterPlotData)
        {
            ExtensionMethods.ReadExcelPlotData(masterPlotData);
            CloseSpinneronPage();
            CloseSpinneronDiv();
            Thread.Sleep(2000);
            WaitForElement(Dashboardpage.PlusButton);
            Dashboardpage.PlusButton.Click();
            WaitForElement(Dashboardpage.QuoteOption);
            Dashboardpage.QuoteOption.Click();
            WaitForElement(AddNewQuoteDiv);
            CloseSpinneronPage();
            CloseSpinneronDiv();
            PGKeySiteDetailsPageOnLatestProductVersion();
            AddLABCQuotePage.OtherSiteDetailsPage();
            AddLABCQuotePage.PlotSchedulePage(masterPlotData);
            AddLABCQuotePage.ProductsDetailsPage();
            AddLABCQuotePage.AdditionalQuestionsPage();
            AddLABCQuotePage.RolesDetails();
            AddLABCQuotePage.DeclarationDetailsPage();
            WaitForElement(SaveButton);
            SaveButton.Click();
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(2000);
            WaitForElement(SubmitButton);
            SubmitButton.Click();
            Thread.Sleep(2000);
            CloseSpinneronPage();
            CloseSpinneronDiv();
            Thread.Sleep(2000);
            WaitForElement(Dashboardpage.QuoteApplicationStatusSubmitted);
            Assert.IsTrue(Dashboardpage.QuoteApplicationStatusSubmitted.Displayed);
        }


        public void SubmitQuoteOnSinglePlot()
        {
            ExtensionMethods.ReadExcelPlotData(Premiums.PlotData);
            ExcelPlay.ReadExcelPlotData(Premiums.PlotData);
            CloseSpinneronPage();
            CloseSpinneronDiv();
            Thread.Sleep(2000);
            WaitForElement(Dashboardpage.PlusButton);
            Dashboardpage.PlusButton.Click();
            WaitForElement(Dashboardpage.QuoteOption);
            Dashboardpage.QuoteOption.Click();
            WaitForElement(AddNewQuoteDiv);
            CloseSpinneronPage();
            CloseSpinneronDiv();
            PGKeySiteDetailsPageOnLatestProductVersion();
            AddLABCQuotePage.OtherSiteDetailsPage();
            AddLABCQuotePage.PlotSchedulePage(Premiums.PlotData);
            AddLABCQuotePage.ProductsDetailsPage();
            AddLABCQuotePage.AdditionalQuestionsPage();
            AddLABCQuotePage.RolesDetails();
            AddLABCQuotePage.DeclarationDetailsPage();
            WaitForElement(SaveButton);
            SaveButton.Click();
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(2000);
            WaitForElement(SubmitButton);
            SubmitButton.Click();
            Thread.Sleep(2000);
            CloseSpinneronPage();
            CloseSpinneronDiv();
            Thread.Sleep(2000);
            WaitForElement(Dashboardpage.QuoteApplicationStatusSubmitted);
            Assert.IsTrue(Dashboardpage.QuoteApplicationStatusSubmitted.Displayed);
        }
        public void SubmitQuoteForAvivaInsurer(string masterPlotData)
        {
            ExtensionMethods.ReadExcelPlotData(masterPlotData);
            CloseSpinneronPage();
            CloseSpinneronDiv();
            Thread.Sleep(2000);
            WaitForElement(Dashboardpage.PlusButton);
            Dashboardpage.PlusButton.Click();
            WaitForElement(Dashboardpage.QuoteOption);
            Dashboardpage.QuoteOption.Click();
            WaitForElement(AddNewQuoteDiv);
            CloseSpinneronPage();
            CloseSpinneronDiv();
            PGKeySiteDetailsForAvivaOnLatestProductVersion();
            AddLABCQuotePage.OtherSiteDetailsPage();
            AddLABCQuotePage.PlotSchedulePage(masterPlotData);
            AddLABCQuotePage.ProductsDetailsPage();
            AddLABCQuotePage.AdditionalQuestionsPage();
            AddLABCQuotePage.RolesDetails();
            AddLABCQuotePage.DeclarationDetailsPage();
            WaitForElement(SaveButton);
            SaveButton.Click();
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(2000);
            WaitForElement(SubmitButton);
            SubmitButton.Click();
            Thread.Sleep(2000);
            CloseSpinneronPage();
            CloseSpinneronDiv();
            Thread.Sleep(2000);
            WaitForElement(Dashboardpage.QuoteApplicationStatusSubmitted);
            Assert.IsTrue(Dashboardpage.QuoteApplicationStatusSubmitted.Displayed);
        }
        public void SubmitQuoteFor12YearsCoverLength(string masterPlotData)
        {
            ExtensionMethods.ReadExcelPlotData(masterPlotData);
            CloseSpinneronPage();
            CloseSpinneronDiv();
            Thread.Sleep(2000);
            WaitForElement(Dashboardpage.PlusButton);
            Dashboardpage.PlusButton.Click();
            WaitForElement(Dashboardpage.QuoteOption);
            Dashboardpage.QuoteOption.Click();
            WaitForElement(AddNewQuoteDiv);
            CloseSpinneronPage();
            CloseSpinneronDiv();
            PGKeySiteDetailsPageOnLatestProductVersion();
            AddLABCQuotePage.OtherSiteDetailsPage();
            AddLABCQuotePage.PlotSchedulePage(masterPlotData);
            AddLABCQuotePage.ProductsDetailsPageFor12Years();
            AddLABCQuotePage.AdditionalQuestionsPage();
            AddLABCQuotePage.RolesDetails();
            AddLABCQuotePage.DeclarationDetailsPage();
            WaitForElement(SaveButton);
            SaveButton.Click();
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(2000);
            WaitForElement(SubmitButton);
            SubmitButton.Click();
            Thread.Sleep(2000);
            CloseSpinneronPage();
            CloseSpinneronDiv();
            Thread.Sleep(2000);
            WaitForElement(Dashboardpage.QuoteApplicationStatusSubmitted);
            Assert.IsTrue(Dashboardpage.QuoteApplicationStatusSubmitted.Displayed);
        }


        public void SubmitQuoteOnSinglePlotFor12YearsCoverLength()
        {
            ExtensionMethods.ReadExcelPlotData(Premiums.PlotData);
            ExcelPlay.ReadExcelPlotData(Premiums.PlotData);
            CloseSpinneronPage();
            CloseSpinneronDiv();
            Thread.Sleep(2000);
            WaitForElement(Dashboardpage.PlusButton);
            Dashboardpage.PlusButton.Click();
            WaitForElement(Dashboardpage.QuoteOption);
            Dashboardpage.QuoteOption.Click();
            WaitForElement(AddNewQuoteDiv);
            CloseSpinneronPage();
            CloseSpinneronDiv();
            PGKeySiteDetailsPageOnLatestProductVersion();
            AddLABCQuotePage.OtherSiteDetailsPage();
            AddLABCQuotePage.PlotSchedulePage(Premiums.PlotData);
            AddLABCQuotePage.ProductsDetailsPageFor12Years();
            AddLABCQuotePage.AdditionalQuestionsPage();
            AddLABCQuotePage.RolesDetails();
            AddLABCQuotePage.DeclarationDetailsPage();
            WaitForElement(SaveButton);
            SaveButton.Click();
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(2000);
            WaitForElement(SubmitButton);
            SubmitButton.Click();
            Thread.Sleep(2000);
            CloseSpinneronPage();
            CloseSpinneronDiv();
            Thread.Sleep(2000);
            WaitForElement(Dashboardpage.QuoteApplicationStatusSubmitted);
            Assert.IsTrue(Dashboardpage.QuoteApplicationStatusSubmitted.Displayed);
        }
        public void SubmitQuoteForAvivaInsurerFor12YearsCoverLength(string masterPlotData)
        {
            ExtensionMethods.ReadExcelPlotData(masterPlotData);
            CloseSpinneronPage();
            CloseSpinneronDiv();
            Thread.Sleep(2000);
            WaitForElement(Dashboardpage.PlusButton);
            Dashboardpage.PlusButton.Click();
            WaitForElement(Dashboardpage.QuoteOption);
            Dashboardpage.QuoteOption.Click();
            WaitForElement(AddNewQuoteDiv);
            CloseSpinneronPage();
            CloseSpinneronDiv();
            PGKeySiteDetailsForAvivaOnLatestProductVersion();
            AddLABCQuotePage.OtherSiteDetailsPage();
            AddLABCQuotePage.PlotSchedulePage(masterPlotData);
            AddLABCQuotePage.ProductsDetailsPageFor12Years();
            AddLABCQuotePage.AdditionalQuestionsPage();
            AddLABCQuotePage.RolesDetails();
            AddLABCQuotePage.DeclarationDetailsPage();
            WaitForElement(SaveButton);
            SaveButton.Click();
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(2000);
            WaitForElement(SubmitButton);
            SubmitButton.Click();
            Thread.Sleep(2000);
            CloseSpinneronPage();
            CloseSpinneronDiv();
            Thread.Sleep(2000);
            WaitForElement(Dashboardpage.QuoteApplicationStatusSubmitted);
            Assert.IsTrue(Dashboardpage.QuoteApplicationStatusSubmitted.Displayed);
        }
        public void SendQuote()
        {          
            SendQuotePage.SendquoteMainMethodPG();
        }
        public void AcceptQuote()
        {
            AcceptQuotePage.AcceptQuoteMain();
        }

    }
}
