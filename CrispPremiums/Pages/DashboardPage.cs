﻿using OpenQA.Selenium;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.PageObjects;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace RegressionPacks.Pages
{
    public class DashboardPage : Support.Pages
    {
        public IWebDriver wdriver;
        public DashboardPage(ISearchContext driver)
        {
            PageFactory.InitElements(driver, this);
            wdriver = (RemoteWebDriver)driver;
        }

        [FindsBy(How = How.XPath, Using = "//i[@class='au-target fa fa-home']")]
        public IWebElement HomeButton;

        [FindsBy(How = How.XPath, Using = "//i[@class='au-target fa fa-plus']")]
        public IWebElement PlusButton;

        [FindsBy(How = How.XPath, Using = "//div//ul[@class='dropdown-content au-target active']//a[text()='Person']")]
        public IWebElement PersonButton;

        [FindsBy(How = How.XPath, Using = "//div//ul[@class='dropdown-content au-target active']//a[text()='Company']")]
        public IWebElement CompanyButton;

        [FindsBy(How = How.XPath, Using = "//body[@class='grey lighten-5 black-text']//crisp-header//nav//div//crisp-header-avatar")]
        public IWebElement CompanyImage;

        [FindsBy(How = How.XPath, Using = "//body[@class='grey lighten-5 black-text']//crisp-header//nav//div//crisp-header-title[1]//div[@class='title']")]
        public IWebElement CompanyTitle;

        [FindsBy(How = How.XPath, Using = "//i[@class='au-target fa fa-search']")]
        public IWebElement SearchButton;

        [FindsBy(How = How.XPath, Using = "//i[@class='au-target fa fa-envelope-o']")]
        public IWebElement EmailButton;

        [FindsBy(How = How.CssSelector, Using = "crisp-nav-bar > a >span > i")]
        public IList<IWebElement> NavItems; // except last 3 items

        [FindsBy(How = How.XPath, Using = "//div//ul[@class='dropdown-content au-target active']//a[text()='Contacts']")]
        public IWebElement ContactOption;

        [FindsBy(How = How.XPath, Using = "//i[@class='au-target fa fa-cog']")]
        public IWebElement AdministrationButton;

        [FindsBy(How = How.XPath, Using = "//div//ul[@class='dropdown-content au-target active']//a[text()='Company Groups']")]
        public IWebElement CompanyGroupsOption;

        [FindsBy(How = How.XPath, Using = "//div//ul[@class='dropdown-content au-target active']//a[text()='Manage Company Lists']")]
        public IWebElement ManageCompanyList;

        [FindsBy(How = How.XPath, Using = "//div//ul[@class='dropdown-content au-target active']//a[text()='Lead']")]
        public IWebElement LeadOption;

        [FindsBy(How = How.XPath, Using = "//body[@class='grey lighten-5 black-text']//crisp-header//nav//div//crisp-header-avatar")]
        public IWebElement LeadImage;

        [FindsBy(How = How.XPath, Using = "//div//ul[@class='dropdown-content au-target active']//a[text()='Quote']")]
        public IWebElement QuoteOption;

        [FindsBy(How = How.XPath, Using = "//body[@class='grey lighten-5 black-text']//crisp-header//nav//div[@class='nav-wrapper au-target']")]
        public IWebElement QuoteApplication;

        [FindsBy(How = How.XPath, Using = "//crisp-header-title/div[@class='title'][contains(text(),'Application Submitted')]")]
        public IWebElement QuoteApplicationStatusSubmitted;

        [FindsBy(How = How.XPath, Using = "//crisp-header-button[@class='au-target']//span//button[text()='Reload'][@class='au-target waves-effect waves-light btn-flat']")]
        public IWebElement ReloadButton;

        [FindsBy(How = How.XPath, Using = "//body[@class='grey lighten-5 black-text']//crisp-header//crisp-header-title//div[@class='title']")]
        public IWebElement QuoteApplicationStatus;

        [FindsBy(How = How.XPath, Using = "//body[@class='grey lighten-5 black-text']//crisp-header//crisp-header-avatar//span")]
        public IWebElement OrderStatus;

        [FindsBy(How = How.XPath, Using = "//crisp-header-actions[@class='au-target']//a[contains(text(),'Actions')]")]
        public IWebElement ActionsButton;

        [FindsBy(How = How.XPath, Using = "//crisp-header-actions[@class='au-target']//a[contains(text(),'Decision')]")]
        public IWebElement QuoteDecisionButton;

        [FindsBy(How = How.XPath, Using = "//crisp-header//nav//div//crisp-header-title//div[contains(text(),'Unqualified')]")]
        public IWebElement LeadUnqualifiedState;

        [FindsBy(How = How.XPath, Using = "//crisp-header//nav//div//crisp-header-title//div[contains(text(),'Rejected')]")]
        public IWebElement LeadRejectedState;

        [FindsBy(How = How.XPath, Using = "//crisp-header-actions//ul[@class='dropdown-content au-target active']//li//crisp-header-dropdown-button//span//button")]
        public IList<IWebElement> ActiveBannerList;

        public IWebElement QuoteApplicationButton => ActiveBannerList.FirstOrDefault(x => x.Text.Contains("QUOTE APPLICATION"));

        public IWebElement SendQuoteButton => ActiveBannerList.FirstOrDefault(x => x.Text.Contains("SEND QUOTE"));
        public IWebElement AcceptQuoteButton => ActiveBannerList.FirstOrDefault(x => x.Text.Contains("ACCEPT QUOTE"));

        public IWebElement IssuePlotIC => ActiveBannerList.FirstOrDefault(x => x.Text.Contains("ISSUE PLOT ICS"));

        public IWebElement IssueDevIC => ActiveBannerList.FirstOrDefault(x => x.Text.Contains("ISSUE DEVELOPMENT IC"));

        public IWebElement IssueCOI => ActiveBannerList.FirstOrDefault(x => x.Text.Contains("ISSUE COI"));

        public IWebElement SiteDetails => ActiveBannerList.FirstOrDefault(x => x.Text.Contains("SITE DETAILS"));

        //Search for Order
        [FindsBy(How = How.CssSelector, Using = "span > i[class='au-target fa fa-search']")]
        public IWebElement SearchOption;

        [FindsBy(How = How.XPath, Using = "//div//ul[@class='dropdown-content au-target active']//a[text()='Sites']")]
        public IWebElement SiteOption;

        [FindsBy(How = How.XPath, Using = "//crisp-input-bool[@label='Include quotes?']/label")]
        public IWebElement IncludeQuotes;

        [FindsBy(How = How.XPath, Using = "//crisp-input-bool[@label='Include leads?']/label")]
        public IWebElement IncludeLeads;

        [FindsBy(How = How.XPath, Using = "//crisp-input-bool[@label='Include orders?']/label")]
        public IWebElement IncludeOrders;

        [FindsBy(How = How.XPath, Using = "//crisp-input-bool[contains(@label,'Include')]")]
        public IList<IWebElement> IncludeCheckBoxes;

        [FindsBy(How = How.XPath, Using = "//crisp-input-text[@label='Reference']/div/label")]
        public IWebElement ReferenceLabel;

        [FindsBy(How = How.XPath, Using = "//crisp-input-text[@label='Reference']/div/input")]
        public IWebElement ReferenceInput;

        [FindsBy(How = How.XPath, Using = "//ul[@class='au-target collection has-header']//li[@class='au-target collection-item']//crisp-list-item//a")]
        public IList<IWebElement> OrderList;

        [FindsBy(How = How.XPath, Using = "//ul[@class='au-target collection has-header']//li[@class='au-target collection-item']//crisp-list-item//a//crisp-list-item-title[@class='au-target']//div")]
        public IList<IWebElement> OrderItems;

        public IWebElement OrderRefItem => OrderItems.FirstOrDefault(x => x.Text.Contains(Statics.OrderRef));

        public IWebElement QuoteRefItem => OrderItems.FirstOrDefault(x => x.Text.Contains(Statics.QuoteRef));

        [FindsBy(How = How.XPath, Using = "//crisp-nav-bar//a//span//i[@class='au-target fa fa-user']")]
        public IWebElement UserButton;

        [FindsBy(How = How.XPath, Using = "//ul[@class='dropdown-content au-target active']//li//a[text()='Logout']")]
        public IWebElement LogOutOption;

        [FindsBy(How = How.XPath, Using = "//div[@class='page-header logged-out']//div//a[@class='PostLogoutRedirectUri']")]
        public IList<IWebElement> ReDirectLink;


        //Login back to crisp and search for the site
        public void CrispLoginMethod()
        {
            Driver.Navigate().GoToUrl(ConfigurationManager.AppSettings["CrispUrl"]);
            SearchforSite();
        }

        //Search for Site
        public void SearchforLead()
        {
            WaitForElement(SearchOption);
            SearchOption.Click();
            WaitForElement(SiteOption);
            SiteOption.Click();
            Thread.Sleep(1000);
            WaitForElement(ReferenceLabel);
            ReferenceLabel.Click();
            WaitForElement(ReferenceInput);
            ReferenceInput.SendKeys(Statics.QuoteRef);
            Thread.Sleep(2000);
            WaitForElement(IncludeLeads);
            Thread.Sleep(2000);
            WaitForElement(IncludeQuotes);
            Thread.Sleep(3000);
            WaitForElement(IncludeOrders);
            WaitForElements(IncludeCheckBoxes);
            for (int i = 0; i < IncludeCheckBoxes.Count; i++)
            {
                if (IncludeCheckBoxes[i].Selected == true)
                {
                    IncludeCheckBoxes[i].Click();
                    Thread.Sleep(2000);
                }
            }
            Thread.Sleep(1000);
            WaitForElements(OrderList);
            if (OrderList.Count > 0)
            {
                WaitForElement(QuoteRefItem);
                QuoteRefItem.Click();
            }
        }
        //Search for Site
        public void SearchforSite()
        {
            Thread.Sleep(1000);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(1000);
            WaitForElement(SearchOption);
            SearchOption.Click();
            Thread.Sleep(1000);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(1000);
            WaitForElement(SiteOption);
            SiteOption.Click();
            Thread.Sleep(1000);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(1000);
            WaitForElement(ReferenceLabel);
            ReferenceLabel.Click();
            WaitForElement(ReferenceInput);
            ReferenceInput.SendKeys(Statics.OrderRef);
            Thread.Sleep(2000);
            WaitForElement(IncludeLeads);
            Thread.Sleep(2000);
            WaitForElement(IncludeQuotes);
            Thread.Sleep(3000);
            WaitForElement(IncludeOrders);
            WaitForElements(IncludeCheckBoxes);
            for (int i = 0; i < IncludeCheckBoxes.Count; i++)
            {
                if (IncludeCheckBoxes[i].Selected == false)
                {
                    IncludeCheckBoxes[i].Click();
                    Thread.Sleep(2000);
                }
            }
            Thread.Sleep(1000);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(1000);
            WaitForElements(OrderList);
            if (OrderList.Count > 0)
            {
                WaitForElement(OrderRefItem);
                OrderRefItem.Click();
            }
        }
        //public void SearchContact()
        //{
        //    SearchButton.Click();
        //    WaitForElement(ContactOption);
        //    ContactOption.Click();
        //    WaitForElement(IncludeCompaniesCheckbox);
        //    ManagePermissionsPage.IncludeCompaniesCheckbox.Click();
        //    WaitForElement(ManagePermissionsPage.IncludeEmployeesCheckbox);
        //    ManagePermissionsPage.IncludeEmployeesCheckbox.Click();
        //    WaitForElement(Contactsearchpage.NameField);
        //    Contactsearchpage.NameField.Click();
        //    Contactsearchpage.EnterName.Click();
        //    Contactsearchpage.EnterName.SendKeys("Sunil Sunkishala");
        //    Thread.Sleep(2000);
        //}
        public void PermissionsLoginMethod()
        {
            if (NavItems.Count > 0)
            {
                Thread.Sleep(1000);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                Thread.Sleep(1000);
                WaitForElement(UserButton);
                UserButton.Click();
                Thread.Sleep(1000);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                Thread.Sleep(1000);
                WaitForElement(LogOutOption);
                LogOutOption.Click();
                Thread.Sleep(1000);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                Thread.Sleep(1000);
                if (ReDirectLink.Count > 0)
                {
                    ReDirectLink[0].Click();
                    Thread.Sleep(1000);
                    CloseSpinneronDiv();
                    CloseSpinneronPage();
                    Thread.Sleep(1000);
                }
                Driver.Navigate().GoToUrl(ConfigurationManager.AppSettings["CrispUrl"]);
                Thread.Sleep(1000);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                Thread.Sleep(1000);
                WaitForElement(Loginpage.Username);
                Loginpage.Username.Click();
                Loginpage.Username.Clear();
                Loginpage.Username.SendKeys(ConfigurationManager.AppSettings["PermissionUsername"]);
                WaitForElement(Loginpage.Password);
                Loginpage.Password.Click();
                Loginpage.Password.Clear();
                Loginpage.Password.SendKeys(ConfigurationManager.AppSettings["PermissionPassword"]);
                Loginpage.LoginButton.Click();
                Thread.Sleep(1000);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                Thread.Sleep(1000);
            }
            else
            {
                Driver.Navigate().GoToUrl(ConfigurationManager.AppSettings["CrispUrl"]);
                Thread.Sleep(1000);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                Thread.Sleep(1000);
                WaitForElement(Loginpage.Username);
                Loginpage.Username.Click();
                Loginpage.Username.Clear();
                Loginpage.Username.SendKeys(ConfigurationManager.AppSettings["PermissionUsername"]);
                WaitForElement(Loginpage.Password);
                Loginpage.Password.Click();
                Loginpage.Password.Clear();
                Loginpage.Password.SendKeys(ConfigurationManager.AppSettings["PermissionPassword"]);
                Loginpage.LoginButton.Click();
                Thread.Sleep(1000);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                Thread.Sleep(1000);
            }
        }
        //public void ContactGuidId()
        //{
        //    string currentURL = wdriver.Url;
        //    string[] temp = currentURL.Split('/');
        //    var guid = temp[temp.Length - 1];
        //    Statics.PersonId = guid;
        //}
    }
}
