﻿using OpenQA.Selenium;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.PageObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RegressionPacks.Pages
{
     public class SecondaryLayerPremiums : Support.Pages
    {
        public IWebDriver wdriver;
        public SecondaryLayerPremiums(ISearchContext driver)
        {
            PageFactory.InitElements(driver, this);
            wdriver = (RemoteWebDriver)driver;
        }
        public double AddSecondaryLayerPremium(double BasePremium, plotrows plot)
        {

            if (Premiums.CoverLengthYears == 10 && plot.ReconstructionCostValue <= 17499999)
            {
                BasePremium = Convert.ToDouble((BasePremium) + (BasePremium * 0.18));
                BasePremium = Math.Round(BasePremium, 2);
            }
            if (Premiums.CoverLengthYears == 10 && (plot.ReconstructionCostValue >= 17500000 && plot.ReconstructionCostValue <= 19999999))
            {
                BasePremium = Convert.ToDouble((BasePremium) + (BasePremium * 0.2096));
                BasePremium = Math.Round(BasePremium, 2);
            }
            if (Premiums.CoverLengthYears == 10 && (plot.ReconstructionCostValue >= 20000000 && plot.ReconstructionCostValue <= 22499999))
            {
                BasePremium = Convert.ToDouble((BasePremium) + (BasePremium * 0.2242));
                BasePremium = Math.Round(BasePremium, 2);
            }
            if (Premiums.CoverLengthYears == 10 && (plot.ReconstructionCostValue >= 22500000 && plot.ReconstructionCostValue <= 24999999))
            {
                BasePremium = Convert.ToDouble((BasePremium) + (BasePremium * 0.2344));
                BasePremium = Math.Round(BasePremium, 2);
            }
            if (Premiums.CoverLengthYears == 10 && (plot.ReconstructionCostValue >= 25000000 && plot.ReconstructionCostValue <= 27499999))
            {
                BasePremium = Convert.ToDouble((BasePremium) + (BasePremium * 0.2446));
                BasePremium = Math.Round(BasePremium, 2);
            }
            if (Premiums.CoverLengthYears == 10 && (plot.ReconstructionCostValue >= 27500000 && plot.ReconstructionCostValue <= 29999999))
            {
                BasePremium = Convert.ToDouble((BasePremium) + (BasePremium * 0.2498));
                BasePremium = Math.Round(BasePremium, 2);
            }
            if (Premiums.CoverLengthYears == 10 && (plot.ReconstructionCostValue >= 30000000 && plot.ReconstructionCostValue <= 32499999))
            {
                BasePremium = Convert.ToDouble((BasePremium) + (BasePremium * 0.2548));
                BasePremium = Math.Round(BasePremium, 2);
            }
            if (Premiums.CoverLengthYears == 10 && (plot.ReconstructionCostValue >= 32500000 && plot.ReconstructionCostValue <= 34999999))
            {
                BasePremium = Convert.ToDouble((BasePremium) + (BasePremium * 0.2598));
                BasePremium = Math.Round(BasePremium, 2);
            }
            if (Premiums.CoverLengthYears == 10 && (plot.ReconstructionCostValue >= 35000000 && plot.ReconstructionCostValue <= 37499999))
            {
                BasePremium = Convert.ToDouble((BasePremium) + (BasePremium * 0.2668));
                BasePremium = Math.Round(BasePremium, 2);
            }
            if (Premiums.CoverLengthYears == 10 && (plot.ReconstructionCostValue >= 37500000 && plot.ReconstructionCostValue <= 39999999))
            {
                BasePremium = Convert.ToDouble((BasePremium) + (BasePremium * 0.2737));
                BasePremium = Math.Round(BasePremium, 2);
            }
            if (Premiums.CoverLengthYears == 10 && (plot.ReconstructionCostValue >= 40000000 && plot.ReconstructionCostValue <= 44999999))
            {
                BasePremium = Convert.ToDouble((BasePremium) + (BasePremium * 0.2784));
                BasePremium = Math.Round(BasePremium, 2);
            }
            if (Premiums.CoverLengthYears == 10 && (plot.ReconstructionCostValue >= 45000000 && plot.ReconstructionCostValue <= 49999999))
            {
                BasePremium = Convert.ToDouble((BasePremium) + (BasePremium * 0.2847));
                BasePremium = Math.Round(BasePremium, 2);
            }
            if (Premiums.CoverLengthYears == 10 && (plot.ReconstructionCostValue >= 50000000 && plot.ReconstructionCostValue <= 59999999))
            {
                BasePremium = Convert.ToDouble((BasePremium) + (BasePremium * 0.2897));
                BasePremium = Math.Round(BasePremium, 2);
            }
            if (Premiums.CoverLengthYears == 10 && (plot.ReconstructionCostValue >= 60000000 && plot.ReconstructionCostValue <= 69999999))
            {
                BasePremium = Convert.ToDouble((BasePremium) + (BasePremium * 0.2947));
                BasePremium = Math.Round(BasePremium, 2);
            }
            if (Premiums.CoverLengthYears == 10 && (plot.ReconstructionCostValue >= 70000000 && plot.ReconstructionCostValue <= 79999999))
            {
                BasePremium = Convert.ToDouble((BasePremium) + (BasePremium * 0.2987));
                BasePremium = Math.Round(BasePremium, 2);
            }
            if (Premiums.CoverLengthYears == 10 && (plot.ReconstructionCostValue >= 80000000 && plot.ReconstructionCostValue <= 89999999))
            {
                BasePremium = Convert.ToDouble((BasePremium) + (BasePremium * 0.3032));
                BasePremium = Math.Round(BasePremium, 2);
            }
            if (Premiums.CoverLengthYears == 10 && (plot.ReconstructionCostValue >= 90000000))
            {
                BasePremium = Convert.ToDouble((BasePremium) + (BasePremium * 0.3087));
                BasePremium = Math.Round(BasePremium, 2);
            }

            // CoverLength Years == 12 

            if (Premiums.CoverLengthYears == 12 && plot.ReconstructionCostValue <= 12499999)
            {
                BasePremium = Convert.ToDouble((BasePremium) + (BasePremium * 0.1891));
                BasePremium = Math.Round(BasePremium, 2);
            }

            if (Premiums.CoverLengthYears == 12 && (plot.ReconstructionCostValue >= 12500000 && plot.ReconstructionCostValue <= 14999999))
            {
                BasePremium = Convert.ToDouble((BasePremium) + (BasePremium * 0.26));
                BasePremium = Math.Round(BasePremium, 2);
            }

            if (Premiums.CoverLengthYears == 12 && (plot.ReconstructionCostValue >= 15000000 && plot.ReconstructionCostValue <= 17499999))
            {
                BasePremium = Convert.ToDouble((BasePremium) + (BasePremium * 0.32249));
                BasePremium = Math.Round(BasePremium, 2);
            }
            if (Premiums.CoverLengthYears == 12 && (plot.ReconstructionCostValue >= 17500000 && plot.ReconstructionCostValue <= 19999999))
            {
                BasePremium = Convert.ToDouble((BasePremium) + (BasePremium * 0.37035));
                BasePremium = Math.Round(BasePremium, 2);
            }
            if (Premiums.CoverLengthYears == 12 && (plot.ReconstructionCostValue >= 20000000 && plot.ReconstructionCostValue <= 22499999))
            {
                BasePremium = Convert.ToDouble((BasePremium) + (BasePremium * 0.401));
                BasePremium = Math.Round(BasePremium, 2);
            }
            if (Premiums.CoverLengthYears == 12 && (plot.ReconstructionCostValue >= 22500000 && plot.ReconstructionCostValue <= 24999999))
            {
                BasePremium = Convert.ToDouble((BasePremium) + (BasePremium * 0.44));
                BasePremium = Math.Round(BasePremium, 2);
            }
            if (Premiums.CoverLengthYears == 12 && (plot.ReconstructionCostValue >= 25000000 && plot.ReconstructionCostValue <= 27499999))
            {
                BasePremium = Convert.ToDouble((BasePremium) + (BasePremium * 0.4375));
                BasePremium = Math.Round(BasePremium, 2);
            }
            if (Premiums.CoverLengthYears == 12 && (plot.ReconstructionCostValue >= 27500000 && plot.ReconstructionCostValue <= 29999999))
            {
                BasePremium = Convert.ToDouble((BasePremium) + (BasePremium * 0.429));
                BasePremium = Math.Round(BasePremium, 2);
            }
            if (Premiums.CoverLengthYears == 12 && (plot.ReconstructionCostValue >= 30000000 && plot.ReconstructionCostValue <= 32499999))
            {
                BasePremium = Convert.ToDouble((BasePremium) + (BasePremium * 0.412));
                BasePremium = Math.Round(BasePremium, 2);
            }
            if (Premiums.CoverLengthYears == 12 && (plot.ReconstructionCostValue >= 32500000 && plot.ReconstructionCostValue <= 34999999))
            {
                BasePremium = Convert.ToDouble((BasePremium) + (BasePremium * 0.4058));
                BasePremium = Math.Round(BasePremium, 2);
            }
            if (Premiums.CoverLengthYears == 12 && (plot.ReconstructionCostValue >= 35000000 && plot.ReconstructionCostValue <= 37499999))
            {
                BasePremium = Convert.ToDouble((BasePremium) + (BasePremium * 0.40206));
                BasePremium = Math.Round(BasePremium, 2);
            }
            if (Premiums.CoverLengthYears == 12 && (plot.ReconstructionCostValue >= 37500000 && plot.ReconstructionCostValue <= 39999999))
            {
                BasePremium = Convert.ToDouble((BasePremium) + (BasePremium * 0.4106));
                BasePremium = Math.Round(BasePremium, 2);
            }
            if (Premiums.CoverLengthYears == 12 && (plot.ReconstructionCostValue >= 40000000 && plot.ReconstructionCostValue <= 44999999))
            {
                BasePremium = Convert.ToDouble((BasePremium) + (BasePremium * 0.41759));
                BasePremium = Math.Round(BasePremium, 2);
            }
            if (Premiums.CoverLengthYears == 12 && (plot.ReconstructionCostValue >= 45000000 && plot.ReconstructionCostValue <= 49999999))
            {
                BasePremium = Convert.ToDouble((BasePremium) + (BasePremium * 0.42712));
                BasePremium = Math.Round(BasePremium, 2);
            }
            if (Premiums.CoverLengthYears == 12 && (plot.ReconstructionCostValue >= 50000000 && plot.ReconstructionCostValue <= 59999999))
            {
                BasePremium = Convert.ToDouble((BasePremium) + (BasePremium * 0.43461));
                BasePremium = Math.Round(BasePremium, 2);
            }
            if (Premiums.CoverLengthYears == 12 && (plot.ReconstructionCostValue >= 60000000 && plot.ReconstructionCostValue <= 69999999))
            {
                BasePremium = Convert.ToDouble((BasePremium) + (BasePremium * 0.4421));
                BasePremium = Math.Round(BasePremium, 2);
            }
            if (Premiums.CoverLengthYears == 12 && (plot.ReconstructionCostValue >= 70000000 && plot.ReconstructionCostValue <= 79999999))
            {
                BasePremium = Convert.ToDouble((BasePremium) + (BasePremium * 0.4481));
                BasePremium = Math.Round(BasePremium, 2);
            }
            if (Premiums.CoverLengthYears == 12 && (plot.ReconstructionCostValue >= 80000000 && plot.ReconstructionCostValue <= 89999999))
            {
                BasePremium = Convert.ToDouble((BasePremium) + (BasePremium * 0.46308));
                BasePremium = Math.Round(BasePremium, 2);
            }
            if (Premiums.CoverLengthYears == 12 && (plot.ReconstructionCostValue >= 90000000))
            {
                BasePremium = Convert.ToDouble((BasePremium) + (BasePremium * 0.3087));
                BasePremium = Math.Round(BasePremium, 2);
            }
            Premiums.SecondaryLayerPremium = BasePremium;
            return BasePremium;
                  
        }
    }
}
