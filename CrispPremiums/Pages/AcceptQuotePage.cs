﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.PageObjects;
using RegressionPacks.Support;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace RegressionPacks.Pages
{
    public class AcceptQuotePage : Support.Pages
    {
        public IWebDriver wdriver;
        public AcceptQuotePage(ISearchContext driver)
        {
            PageFactory.InitElements(driver, this);
            wdriver = (RemoteWebDriver)driver;
        }
        [FindsBy(How = How.XPath, Using = "//ai-dialog-container//crisp-dialog[@class='au-target']//div[@class='au-target modal dialog brand pg modal-fixed-footer modal-full-bleed modal-wide modal-tall modal-has-header']//crisp-header[@class='au-target']")]
        public IWebElement AcceptQuoteDialogueWindow;

        [FindsBy(How = How.XPath, Using = "//ai-dialog-container//crisp-dialog[@class='au-target']//div[@class='au-target modal dialog brand labc modal-fixed-footer modal-full-bleed modal-wide modal-tall modal-has-header']//crisp-header[@class='au-target']")]
        public IWebElement LabcAcceptQuoteDialogueWindow;

        [FindsBy(How = How.CssSelector, Using = "crisp-card-content > div > crisp-input-file > div > div.btn")]
        public IWebElement FileButton;

        [FindsBy(How = How.XPath, Using = "//crisp-input-file/div/div[1]/input")]
        public IWebElement QuoteAcceptenceDoc;

        [FindsBy(How = How.XPath, Using = "//crisp-input-date[@label='Date received']/div/label")]
        public IWebElement Datereceived;

        [FindsBy(How = How.XPath, Using = "//div[contains(@class,'picker--focused')]/div/div/div/div/div[3][@class='picker__footer']/button[text()='Today']")]
        public IWebElement ReceivedDateInput;

        [FindsBy(How = How.XPath, Using = "//crisp-footer/nav/div/crisp-footer-button[@click.delegate='tryComplete()'][@class='au-target']/button[text()='Complete']")]
        public IWebElement CompleteButton;

        [FindsBy(How = How.XPath, Using = "//crisp-stepper-step[contains(@class,'au-target current')]")]
        public IWebElement ProductsTab;

        [FindsBy(How = How.XPath, Using = "//crisp-wizard-step[@class='au-target active']//products-covers[@class='custom-element au-target']//div[@class='reset-recalculate-btn']")]
        public IWebElement ResetRecaluclateFees;

        [FindsBy(How = How.XPath, Using = "//crisp-footer-button[@click.delegate='goForward()'][@class='au-target']//button[text()='Next'][@class='au-target waves-effect waves-light btn-flat']")]
        public IWebElement AcceptQuoteNextButton;

        [FindsBy(How = How.XPath, Using = "//crisp-stepper-step[contains(@class,'au-target current')]")]
        public IWebElement ConditionsTab;

        [FindsBy(How = How.XPath, Using = "//crisp-wizard-step[@class='au-target active']//conditions[@class='custom-element au-target']//div[@class='table-title']//span[text()='Conditions']")]
        public IWebElement ConditionsTitle;

        [FindsBy(How = How.XPath, Using = "//crisp-wizard-step[@step-title='Conditions'][@class='au-target active']//conditions//crisp-table[@class='custom-element au-target custom-element']//span[text()='Conditions']")]
        public IWebElement ConditionsOptions;

        [FindsBy(How = How.XPath, Using = "//crisp-wizard-step[@step-title='Conditions'][@class='au-target active']//conditions//condition-list//crisp-table[@class='custom-element au-target custom-element']//div//table[@class='au-target']")]
        public IWebElement Conditionstable;

        [FindsBy(How = How.XPath, Using = "//crisp-wizard-step[@step-title='Conditions'][@class='au-target active']//conditions//condition-list//crisp-table[@class='custom-element au-target custom-element']//div//table[@class='au-target']/tbody/tr[@class='au-target']")]
        public IList<IWebElement> ConditionsRows;

        [FindsBy(How = How.XPath, Using = "//crisp-wizard-step[@step-title='Conditions'][@class='au-target active']//conditions//condition-list//crisp-table[@class='custom-element au-target custom-element']")]
        public IWebElement ConditionsEle;

        [FindsBy(How = How.XPath, Using = "//crisp-stepper-step[contains(@class,'au-target current')]")]
        public IWebElement FileReviewTab;

        [FindsBy(How = How.XPath, Using = "//crisp-wizard-step[@class='au-target active']//file-review[@class='custom-element au-target']//div[@class='card-title']//span[text()='File Review Summary']")]
        public IWebElement FileReviewTitle;

        [FindsBy(How = How.XPath, Using = "//crisp-stepper-step[contains(@class,'au-target current')]")]
        public IWebElement ConfirmTab;

        [FindsBy(How = How.XPath, Using = "//crisp-wizard-step[@class='au-target active']//confirm[@class='custom-element au-target']//div[@class='card-title']//span[contains(text(),'Confirm Quote Acceptance Details')]")]
        public IWebElement ConfirmPageTitle;

        [FindsBy(How = How.XPath, Using = "//crisp-stepper-step[contains(@class,'au-target current')]")]
        public IWebElement InternalRolesTab;

        [FindsBy(How = How.XPath, Using = "//crisp-wizard-step[@class='au-target active']//internal-roles[@class='custom-element au-target']//span[contains(text(),'Internal Roles')]")]
        public IWebElement InternalRoles;

        [FindsBy(How = How.XPath, Using = "//crisp-wizard-step[@step-title='internal Roles']//table//tbody[@class='collection']//tr[@class='au-target collection-item']//td[@class='au-target fixed-width error']//em[text()='Not selected']")]
        public IList<IWebElement> MandetoryRole;

        [FindsBy(How = How.XPath, Using = "//crisp-wizard-step[@step-title='internal Roles']//tbody[@class='collection']//tr[@class='au-target collection-item']//td[@class='au-target fixed-width error'][1]")]
        public IList<IWebElement> Role;

        [FindsBy(How = How.XPath, Using = "//ul[@ref='theList']//li[@class='au-target collection-item']//crisp-list-item//crisp-list-item-title//div//span[text()='Technical Administrator']")]
        public IList<IWebElement> TechAdminRole;

        [FindsBy(How = How.XPath, Using = "//crisp-card-actions// div //crisp-button// span// button[@class='au-target waves-effect waves-light btn-flat'][text()='Cancel']")]
        public IWebElement CancelButton;


        [FindsBy(How = How.XPath, Using = "//crisp-input-html[@class='au-target']//div//table//tbody//tr//td//p//a[@href='https://www.premierguarantee.co.uk/']//img")]
        public IList<IWebElement> PGGenerateQuote;

        [FindsBy(How = How.XPath, Using = "//crisp-stepper-step[contains(@class,'au-target last-step current')]")]
        public IWebElement GenerateQuoteAcceptanceTab;

        [FindsBy(How = How.XPath, Using = "//crisp-input-html[@class='au-target']//div//table//tbody//tr//td//h3//a[@href='https://www.labcwarranty.co.uk/']//img")]
        public IList<IWebElement> LABCGenerateQuote;

        [FindsBy(How = How.XPath, Using = "//div[@class='au-target input-field']//span[@ref='inputContainer']//span")]
        public IWebElement QuoteAcceptenceDocument;

        [FindsBy(How = How.XPath, Using = "//tr[@class='au-target collection-item']//td[contains(text(),'Quote Acceptance.pdf ')]")]
        public IWebElement QuoteAcceptenceDocumentInput;

        [FindsBy(How = How.XPath, Using = "//crisp-card-actions[@class='au-target']//div//crisp-button//button[text()='Select']")]
        public IWebElement SelectButton;

        [FindsBy(How = How.XPath, Using = "//div//crisp-header-title[@class='au-target'][1]/div[@class='title']")]
        public IWebElement OrderRefDetails;

        [FindsBy(How = How.XPath, Using = "//crisp-input-html[@class='au-target']//div//table//tbody//tr//td")]
        public IList<IWebElement> Correspondence;

        [FindsBy(How = How.XPath, Using = "//div[@class='modal-content']//crisp-wizard[@active-step.bind='activeStep']//crisp-stepper-step[contains(@class,'current')]//div[@title.bind='description'][contains(@class,'title')]")]
        public IWebElement StepTitle;

        //Accept Quote Product Page
        public void AcceptQuoteProductsPage()
        {
            WaitForElement(StepTitle);
            Assert.IsTrue(StepTitle.Text.Contains("PRODUCTS"), $"Failed to Display {StepTitle.Text} Page on Accept Quote");
            //Thread.Sleep(500);
            WaitForElement(ProductsTab);
            Assert.IsTrue(ProductsTab.Displayed);
            WaitForElement(ResetRecaluclateFees);
            Assert.IsTrue(ResetRecaluclateFees.Displayed);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(500);
            AcceptQuoteNextButtonMethod();
        }
        //Accept Quote Product Page
        public void AcceptQuoteConditionsPage()
        {
            WaitForElement(ConditionsTab);
            WaitForElement(StepTitle);
            Assert.IsTrue(StepTitle.Text.Contains("CONDITIONS"), $"Failed to Display {StepTitle.Text} Page on Accept Quote");
            Assert.IsTrue(ConditionsTab.Displayed);
            //Thread.Sleep(500);
            WaitForElement(ConditionsOptions);
            //Thread.Sleep(500);
            Assert.IsTrue(ConditionsOptions.Displayed);
            //Thread.Sleep(500);
            WaitForElement(ConditionsEle);
            //Thread.Sleep(500);
            Assert.IsTrue(ConditionsEle.Displayed);
            //Thread.Sleep(500);
            WaitForElement(Conditionstable);
            //Thread.Sleep(500);
            Assert.IsTrue(Conditionstable.Displayed);
            //Thread.Sleep(2000);
            if (SendQuotePage.ConditionsRows.Count > 0)
            {
                WaitForElements(ConditionsRows);
                Assert.IsTrue(ConditionsRows.Count >= 0);
                foreach (var eachrow in ConditionsRows)
                {
                    Assert.IsTrue(eachrow.Displayed);
                }
            }
            //Thread.Sleep(4000);
            Console.WriteLine("conditions details verified");
            //Thread.Sleep(3000);
            AcceptQuoteNextButtonMethod();
        }
        //Accept Quote File Review Page 
        public void AcceptQuoteFileReviewPage()
        {
            WaitForElement(StepTitle);
            Assert.IsTrue(StepTitle.Text.Contains("FILE REVIEW"), $"Failed to Display {StepTitle.Text}");
            WaitForElement(FileReviewTab);
            Assert.IsTrue(FileReviewTab.Displayed);
            //Thread.Sleep(500);
            WaitForElement(FileReviewTitle);
            Assert.IsTrue(FileReviewTitle.Displayed);
            //Thread.Sleep(500);
            AcceptQuoteNextButtonMethod();

        }
        //Accept Quote Confirm page 
        public void AcceptQuoteConfirmPage()
        {
            WaitForElement(StepTitle);
            Assert.IsTrue(StepTitle.Text.Contains("CONFIRM"), $"Failed to Display {StepTitle.Text}");
            WaitForElement(ConfirmTab);
            Assert.IsTrue(ConfirmTab.Displayed);
            //Thread.Sleep(1500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            WaitForElement(ConfirmPageTitle);
            Assert.IsTrue(ConfirmPageTitle.Displayed);
            ConfirmDetailsPage();
            //Thread.Sleep(500);
            AcceptQuoteNextButtonMethod();

        }
        //Accept Quote Internal Roles Page 
        public void AcceptQuoteInternalRolesPage()
        {
            WaitForElement(StepTitle);
            Assert.IsTrue(StepTitle.Text.Contains("INTERNAL ROLES"), $"Failed to Display {StepTitle.Text} Page on Accept Quote");
            WaitForElement(InternalRolesTab);
            Assert.IsTrue(InternalRolesTab.Displayed);
            //Thread.Sleep(500);
            WaitForElement(InternalRoles);
            Assert.IsTrue(InternalRoles.Displayed);
            //Thread.Sleep(1000);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(1000);
            if (MandetoryRole.Count > 0)
            {
                WaitForElements(MandetoryRole);
                WaitForElements(Role);
                List<string> RoleNames = Role.Select(i => i.Text).ToList();
                if (RoleNames.Contains("Builder"))
                {
                    AddLABCQuotePage.SetBuilderRole();
                    Thread.Sleep(1000);
                    CloseCrispCard();
                    CloseSpinneronDiv();
                    CloseSpinneronPage();
                    Thread.Sleep(1000);
                }
                if (RoleNames.Contains("Developer"))
                {
                    AddLABCQuotePage.SetDeveloperRole();
                    Thread.Sleep(1000);
                    CloseCrispCard();
                    CloseSpinneronDiv();
                    CloseSpinneronPage();
                    Thread.Sleep(1000);
                }
                if (RoleNames.Contains("Sales Account Manager"))
                {
                    AddLABCQuotePage.SetSalesAccountManagerRole();                  
                    Thread.Sleep(1000);
                    CloseCrispCard();
                    CloseSpinneronDiv();
                    CloseSpinneronPage();
                    Thread.Sleep(1000);
                }
                if (RoleNames.Contains("Structural Referral Administrator"))
                {
                    AddLABCQuotePage.SetStructurralReferralAdminRole();
                    Thread.Sleep(1000);
                    CloseCrispCard();
                    CloseSpinneronDiv();
                    CloseSpinneronPage();
                    Thread.Sleep(1000);
                }
                if (RoleNames.Contains("Design Review Administrator"))
                {
                    AddLABCQuotePage.SetDesignReviewAdminRole();
                    Thread.Sleep(1000);
                    CloseCrispCard();
                    CloseSpinneronDiv();
                    CloseSpinneronPage();
                    Thread.Sleep(1000);
                }
                if (RoleNames.Contains("Technical Administrator"))
                {
                    AddLABCQuotePage.SetTechnicalAdministratorRole();
                    Thread.Sleep(1000);
                    CloseCrispCard();
                    CloseSpinneronDiv();
                    CloseSpinneronPage();
                    Thread.Sleep(1000);
                }
                if (RoleNames.Contains("Building Control Provider"))
                {
                    AddLABCQuotePage.SetBCProviderRole();
                    Thread.Sleep(1000);
                    CloseCrispCard();
                    CloseSpinneronDiv();
                    CloseSpinneronPage();
                    Thread.Sleep(1000);
                }

                if (RoleNames.Contains("Land Owner"))
                {
                    AddLABCQuotePage.SetLandOwnerRole();
                    Thread.Sleep(1000);
                    CloseCrispCard();
                    CloseSpinneronDiv();
                    CloseSpinneronPage();
                    Thread.Sleep(1000);
                }
                if (RoleNames.Contains("Surveyor"))
                {
                    AddLABCQuotePage.SetSurveyorRole();
                    Thread.Sleep(1000);
                    CloseCrispCard();
                    CloseSpinneronDiv();
                    CloseSpinneronPage();
                    Thread.Sleep(1000);
                }

                if (RoleNames.Contains("Design Review Surveyor"))
                {
                    AddLABCQuotePage.SetDesignReviewSurveyorRole();
                    Thread.Sleep(1000);
                    CloseCrispCard();
                    CloseSpinneronDiv();
                    CloseSpinneronPage();
                    Thread.Sleep(1000);
                }

                if (RoleNames.Contains("Refurbishment Assessment Surveyor"))
                {
                    AddLABCQuotePage.SetRefurbishmentAssessmentSurveyorRole();
                    Thread.Sleep(1000);
                    CloseCrispCard();
                    CloseSpinneronDiv();
                    CloseSpinneronPage();
                    Thread.Sleep(1000);
                }
                if (RoleNames.Contains("Hub Administrator"))
                {
                    AddLABCQuotePage.SetHubAdministratorRole();
                    Thread.Sleep(1000);
                    CloseCrispCard();
                    CloseSpinneronDiv();
                    CloseSpinneronPage();
                    Thread.Sleep(1000);
                }


                if (Statics.ProductName.Contains("Social Housing") || Statics.ProductName.Contains("Social Housing - High Value"))
                {
                    if (RoleNames.Contains("Housing Association"))
                    {
                        AddLABCQuotePage.SetSocialHousingRole();
                        Thread.Sleep(1000);
                        CloseCrispCard();
                        CloseSpinneronDiv();
                        CloseSpinneronPage();
                        Thread.Sleep(1000);
                    }
                }

                if (Statics.ProductName.Contains("Self Build"))
                {
                    if (RoleNames.Contains("Applicant (Self-build)"))
                    {
                        AddLABCQuotePage.SetSelfBuildRole();
                        CloseCrispCard();
                        Thread.Sleep(1000);
                        CloseSpinneronDiv();
                        CloseSpinneronPage();
                        Thread.Sleep(1000);
                    }
                }

                if (Statics.ProductName.Contains("Private Rental") || Statics.ProductName.Contains("Private Rental - High Value"))
                {
                    if (RoleNames.Contains("PRS Company"))
                    {
                        AddLABCQuotePage.SetPRSCompanyRole();
                        CloseCrispCard();
                        Thread.Sleep(1000);
                        CloseSpinneronDiv();
                        CloseSpinneronPage();
                        Thread.Sleep(1000);
                    }
                }
            }


            AcceptQuoteNextButtonMethod();
            //Thread.Sleep(4000);
            CloseSpinneronDiv();
            CloseSpinneronPage();
        }

        //Accept Quote Correspondence Page
        public void AcceptQuoteCorrespondencePage()
        {
            WaitForElement(StepTitle);
            Assert.IsTrue(StepTitle.Text.Contains("GENERATE QUOTE ACCEPTANCE"), $"Failed to Display {StepTitle.Text} Page on Accept Quote");
            CloseSpinneronDiv();
            CloseSpinneronPage();
            WaitForElements(Correspondence);
            if (LABCGenerateQuote.Count > 0)
            {
                //Thread.Sleep(1000);
                CloseSpinneronDiv();
                //Thread.Sleep(1000);
                WaitForElement(LABCGenerateQuote[0]);
                //Thread.Sleep(1000);
                Assert.IsTrue(LABCGenerateQuote[0].Displayed);
                Console.WriteLine("LABC Generate  Quote details verified");
            }
            if (PGGenerateQuote.Count > 0)
            {
                //Thread.Sleep(1000);
                CloseSpinneronDiv();
                //Thread.Sleep(1000);
                WaitForElement(PGGenerateQuote[0]);
                //Thread.Sleep(1000);
                Assert.IsTrue(PGGenerateQuote[0].Displayed);
                Console.WriteLine("LABC Generate  Quote details verified");
            }
        }
        public void AcceptQuoteNextButtonMethod()
        {
            Thread.Sleep(1000);
            CloseSpinneronPage();
            CloseSpinneronDiv();
            Thread.Sleep(1000);
            WaitForElement(AcceptQuoteNextButton);
            AcceptQuoteNextButton.Click();
            CloseSpinneronPage();
            CloseSpinneronDiv();
            Thread.Sleep(1000);
        }
        public void CompleteButtonAcceptquoteWindow()
        {
            CloseSpinneronPage();
            CloseSpinneronDiv();
            //Thread.Sleep(2000);
            WaitForElement(CompleteButton);
            CompleteButton.Click();
            CloseSpinneronPage();
            CloseSpinneronDiv();
            CloseCrispDiv(1);
            //Thread.Sleep(2000);
        }
        public void OrderDetailsOnTheDashboard()
        {
            //Thread.Sleep(1000);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(1000);
            WaitForElement(Dashboardpage.QuoteApplication);
            CloseSpinneronDiv();
            Assert.IsTrue(Dashboardpage.QuoteApplication.Displayed);
            //Thread.Sleep(3000);
            var orderStatus = Dashboardpage.OrderStatus.Text;
            if (orderStatus == "O")
            {
                Assert.IsTrue(orderStatus == "O");
            }
            else
            {
                WaitForElement(Dashboardpage.ReloadButton);
                Dashboardpage.ReloadButton.Click();
            }

            StoreOrderRefDetails();
        }
        //Accept Quote -Accept Details Page For PG and LABC products
        public void ConfirmDetailsPage()
        {
            //Thread.Sleep(1000);
            WaitForElement(QuoteAcceptenceDocument);
            QuoteAcceptenceDocument.Click();
            //Thread.Sleep(1000);
            //Thread.Sleep(1000);
            AdditionalMethodsPage.UploadNewFile();
            //Thread.Sleep(1000);  
        }
        public void StoreOrderRefDetails()
        {         
            WaitForElement(OrderRefDetails);
            var OrderRefNumber = OrderRefDetails.Text;
            string currentURL = wdriver.Url;
            string[] temp = currentURL.Split('/');
            var OrderId = temp[temp.Length - 1];
            Statics.OrderRef = OrderRefNumber;
            Statics.OrderId = OrderId;
            ExtensionMethods.CreateSpreadsheet();
        }


        public void AcceptQuoteMain()
        {
            AcceptQuoteProductsPage();
            AcceptQuoteConditionsPage();
            AcceptQuoteFileReviewPage();
            AcceptQuoteConfirmPage();
            AcceptQuoteInternalRolesPage();
            AcceptQuoteCorrespondencePage();
            OrderDetailsOnTheDashboard();
        }
    }
}
