﻿using ExcelDataReader;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.PageObjects;
using RegressionPacks.Support;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RegressionPacks.Pages
{
    public class CommercialPremiums : Support.Pages
    {
        public IWebDriver wdriver;
        DataTable excelTable;
        double iptCaluclationValue = 0.12;
        double minimumTAFee = 0.00;        
        double minimumstructuralFee = 0.00;
        double lossOfGrossProfitFee = 0.00;
        double lossOfRentReceivableFee = 0.00;
        double lossOfRentPayableFee = 0.00;
        int plotCount = 0;

        public CommercialPremiums(ISearchContext driver)
        {
            PageFactory.InitElements(driver, this);
            wdriver = (RemoteWebDriver)driver;
        }
        
        public void CoversOnCommercialProduct()
        {
            if (Premiums.eachplotrow.Any(o => o.ProductName.Contains("Commercial")))
            {
                Premiums.ProductsCovers = PremiumCovers.ProductCoversList.Select(i => i.Text.ToString()).ToList();
                Premiums.ProductServices = PremiumCovers.ProductServicesList.Select(i => i.Text.ToString()).ToList();

                if (Premiums.ProductsCovers.Any(x => x.Contains("Contaminated Land")))
                {
                    Premiums.ContaminatedLandCover = PremiumCovers.ContaminatedCover.Count > 0 ? "Yes" : "No";
                }
                if (Premiums.ProductsCovers.Any(x => x.Contains("Additional Cover for Local Authority Building Control Function")))
                {
                    Premiums.LocalAuthorityBuildingControlFunctionCover = PremiumCovers.LocalAuthorityBuildingControlFunctionCover.Count > 0 ? "Yes" : "No";
                }
                if (Premiums.ProductsCovers.Any(x => x.Contains("Seepage")))
                {
                    Premiums.SeepageCover = PremiumCovers.SeeapageCover.Count > 0 ? "Yes" : "No";
                }
                if (Premiums.ProductsCovers.Any(x => x.Contains("Waivers of Subrogation Rights - Builder")))
                {
                    Premiums.WaiversOfSubrogationRightsBuilderCover = PremiumCovers.WaiversBuilderCover.Count > 0 ? "Yes" : "No";
                }
                if (Premiums.ProductsCovers.Any(x => x.Contains("Loss of Gross Profit")))
                {
                    Premiums.LossOfGrossProfitCover = PremiumCovers.LossOfGrossProfitCover.Count > 0 ? "Yes" : "No";
                }
                if (Premiums.ProductsCovers.Any(x => x.Contains("Loss of Rent Payable")))
                {
                    Premiums.LossOfRentPayableCover = PremiumCovers.LossOfRentPayableCover.Count > 0 ? "Yes" : "No";
                }
                if (Premiums.ProductsCovers.Any(x => x.Contains("Loss of Rent Receivable")))
                {
                    Premiums.LossOfRentReceivableCover = PremiumCovers.LossOfRentReceivableCover.Count > 0 ? "Yes" : "No";
                }
                if (Premiums.ProductsCovers.Any(x => x.Contains("Waivers of Subrogation Rights - Engineer")))
                {
                    Premiums.WaiversOfSubrogationRightsEngineerCover = PremiumCovers.WaiversEngineerCover.Count > 0 ? "Yes" : "No";
                }
                if (Premiums.ProductServices.Any(x => x.Contains("Technical Audit Fee")))
                {
                    Premiums.ProductTechnicalAuditFeeService = PremiumCovers.ProductTechnicalAuditFee.Count > 0 ? "Yes" : "No";
                }
                if (Premiums.ProductServices.Any(x => x.Contains("Administration Fee")))
                {
                    Premiums.ProductAdminFeeService = PremiumCovers.ProductAdministrationFee.Count > 0 ? "Yes" : "No";
                }
                if (Premiums.ProductServices.Any(x => x.Contains("Cancellation Fee")))
                {
                    Premiums.ProductCancellationFeeService = PremiumCovers.ProductCancellationFee.Count > 0 ? "Yes" : "No";
                }
            }
        }       

        public  void CommercialPremiumsCaluclations()
        {
            Premiums.StructuralFee = 0;
            Premiums.ContaminatedLandFee = 0;
            Premiums.LocalAuthorityBuildingControlFee = 0;
            Premiums.SeepageFee = 0;
            Premiums.WaiversOfSubrogationBuilderFee = 0;
            Premiums.LossOfGrossProfitFee = 0;
            Premiums.LossOfRentPayableFee = 0;
            Premiums.LossOfRentReceivableFee = 0;
            Premiums.WaiversOfSubrogationEngineerFee = 0;
            Premiums.TechnicalAuditFee = 0;
            Premiums.AdministrationFee = 0;
            Premiums.CancellationFee = 0;     
            var aggregatedPremium = new CommercialPremium();
            foreach (var plot in Premiums.eachplotrow)
            {
                //Actual Premiums Caluclaions for Each Plot Row 
                var calculatedPremiums = CommericalPremiumCaluclationsForEachPlotRow(plot);  
                //Cover Premiums 
                Premiums.StructuralFee += calculatedPremiums.structuralFee;               
                Premiums.ContaminatedLandFee += calculatedPremiums.contaminatedLandFee;
                Premiums.LocalAuthorityBuildingControlFee += calculatedPremiums.lABuildingControlFuntionFee;
                Premiums.SeepageFee += calculatedPremiums.seepageFee;
                Premiums.WaiversOfSubrogationBuilderFee += calculatedPremiums.waviersOfSubrogationRightsBuilderFee;
                Premiums.LossOfGrossProfitFee += calculatedPremiums.lossOfGrossProfitFee;          
                Premiums.LossOfRentPayableFee += calculatedPremiums.lossOfRentPayableFee;               
                Premiums.LossOfRentReceivableFee += calculatedPremiums.lossOfRentReceivableFee;               
                Premiums.WaiversOfSubrogationEngineerFee += calculatedPremiums.waviersOfSubrogationRightsEngineerFee;                
                //Services 
                Premiums.TechnicalAuditFee += calculatedPremiums.technicalFee;            
                Premiums.AdministrationFee += calculatedPremiums.administrationFee;
                Premiums.CancellationFee += calculatedPremiums.cancellationFee;
           
            }
            if (Premiums.StructuralFee < minimumstructuralFee)
            {
                Premiums.StructuralFee = Math.Round((minimumstructuralFee), 2);
                Premiums.StructuralIPTFee = Math.Round((Convert.ToDouble(Premiums.StructuralFee * iptCaluclationValue)), 2);
            }
            else
            {
                Premiums.StructuralFee = Math.Round((Premiums.StructuralFee), 2);
                Premiums.StructuralIPTFee = Math.Round((Convert.ToDouble(Premiums.StructuralFee * iptCaluclationValue)), 2);
            }      
            //Product Service Fee Values 
            if (Premiums.ProductServices.Count > 0)
            {

                if (Premiums.ProductTechnicalAuditFeeService == "Yes")
                {
                    if (Premiums.TechnicalAuditFee < minimumTAFee)
                    {
                        Premiums.TechnicalAuditFee = Math.Round((minimumTAFee), 2);
                        Premiums.TechnicalAuditVATFee = Convert.ToDouble(0.00);
                    }
                    else
                    {
                        Premiums.TechnicalAuditFee = Math.Round(((Premiums.TechnicalAuditFee)), 2);
                        Premiums.TechnicalAuditVATFee = Convert.ToDouble(0.00);
                    }
                }
            }
            //IPT caluclations 
            Premiums.StructuralIPTFee = Convert.ToDouble(Premiums.StructuralFee * iptCaluclationValue);
            Premiums.ContaminatedLandIPTFee = Convert.ToDouble(Premiums.ContaminatedLandFee * iptCaluclationValue);
            Premiums.LocalAuthorityBuildingControlIPTFee = Convert.ToDouble(Premiums.LocalAuthorityBuildingControlFee * iptCaluclationValue);
            Premiums.SeepageIPTFee = Convert.ToDouble(Premiums.SeepageFee * iptCaluclationValue);
            Premiums.WaiversOfSubrogationBuilderIPTFee = Convert.ToDouble(Premiums.WaiversOfSubrogationBuilderFee * iptCaluclationValue);
            Premiums.LossOfGrossProfitIPTFee = Convert.ToDouble(Premiums.LossOfGrossProfitFee * iptCaluclationValue);
            Premiums.LossOfRentPayableIPTFee = Convert.ToDouble(Premiums.LossOfRentPayableFee * iptCaluclationValue);
            Premiums.LossOfRentReceivableIPTFee = Convert.ToDouble(Premiums.LossOfRentReceivableFee * iptCaluclationValue);
            Premiums.WaiversOfSubrogationEngineerIPTFee = Convert.ToDouble(Premiums.WaiversOfSubrogationEngineerFee * iptCaluclationValue);
            //VAT Calucaltions 
            Premiums.TechnicalAuditVATFee = Convert.ToDouble(0.00);
            Premiums.AdministrationVATFee = Convert.ToDouble(0.00);
            Premiums.CancellationVATFee = Convert.ToDouble(0.00);  
            VerfiyCommercialUIValues();
            if (Premiums.eachplotrow.Any(o => o.BCProduct.Contains("Commercial Building Control")))
            {
                CommercialBCPremiums.CommercialBCPremiumCalcs();
            }
        }

        public class CommercialPremium
        {
            public double structuralFee;
       
            public double contaminatedLandFee;
         
            public double lABuildingControlFuntionFee;
           
            public double seepageFee;
        
            public double waviersOfSubrogationRightsBuilderFee;
         
            public double lossOfGrossProfitFee;
      
            public double lossOfRentPayableFee;
         
            public double lossOfRentReceivableFee;
        
            public double waviersOfSubrogationRightsEngineerFee;
    
            public double technicalFee;
        
            public double administrationFee;
        
            public double cancellationFee;           
        }

        public CommercialPremium CommericalPremiumCaluclationsForEachPlotRow(plotrows plot)
        {
            var calculatedPremiums = new CommercialPremium();

            CoversOnCommercialProduct();           
            var fileInfo = new FileInfo(ExtensionMethods.GetAbsolutePath($@"TestData\Premiums\ProductPricingSetup\Product Pricing Setup - {plot.ProductName}.xlsx"));
            //Filter Premium Data for Commercials 
            string premiumsdata = fileInfo.ToString();
            excelTable = ReadCommercialMasterPremiumsData(premiumsdata);
            double structuralPremium = Convert.ToDouble(excelTable.Rows[1][1]);
            minimumstructuralFee = Convert.ToDouble(excelTable.Rows[44][1]);
            double tenYearLength = Convert.ToDouble(excelTable.Rows[5][2]);
            double twelveYearLength = Convert.ToDouble(excelTable.Rows[6][2]);
            double builderExperianceBelow4Years = Convert.ToDouble(excelTable.Rows[15][2]);
            double builderExperianceAbove4Years = Convert.ToDouble(excelTable.Rows[16][2]);
            //Product Cover Premium Caluclations
            calculatedPremiums.structuralFee = Convert.ToDouble(plot.ReconstructionCostValue * structuralPremium);
            double noworksStarted = Convert.ToDouble(excelTable.Rows[25][2]);
            double foundationLevel = Convert.ToDouble(excelTable.Rows[26][2]);
            double firstFloorLevel = Convert.ToDouble(excelTable.Rows[27][2]);
            double wallPlateLevel = Convert.ToDouble(excelTable.Rows[28][2]);
            double roofLevel = Convert.ToDouble(excelTable.Rows[29][2]);
            double firstFix = Convert.ToDouble(excelTable.Rows[26][2]);
            double secondFix = Convert.ToDouble(excelTable.Rows[28][2]);
            double completedBuildBelow1Year = Convert.ToDouble(excelTable.Rows[35][2]);
            double completedBuildIn2to4Years = Convert.ToDouble(excelTable.Rows[38][2]);
            double completedBuildAbove5Years = Convert.ToDouble(excelTable.Rows[39][2]);
            double TAFee = Convert.ToDouble(excelTable.Rows[68][1]);
            minimumTAFee = Convert.ToDouble(excelTable.Rows[73][1]);
            //Technical Aditfee Caluclations
            calculatedPremiums.technicalFee = Convert.ToDouble(plot.ReconstructionCostValue * TAFee);
           
        
            //Loss of Gross Profit Premium
            double seepagePremium = Convert.ToDouble(excelTable.Rows[60][1]);
            var seepageValue = Convert.ToDouble(plot.ReconstructionCostValue * seepagePremium);

            double waviersOfSubrogationRightsPremium = Convert.ToDouble(excelTable.Rows[64][1]); 
            var waviersOfSubrogationBuilderValue = Convert.ToDouble(plot.ReconstructionCostValue * waviersOfSubrogationRightsPremium);
           

            waviersOfSubrogationRightsPremium = Convert.ToDouble(excelTable.Rows[64][1]); ;
            var waviersOfSubrogationEngineerValue = Convert.ToDouble(plot.ReconstructionCostValue * waviersOfSubrogationRightsPremium);

            //Coverpremium value Based on Cover Length 
            if (Premiums.CoverLengthYears == 10)
            {
                calculatedPremiums.structuralFee = Convert.ToDouble(calculatedPremiums.structuralFee * tenYearLength);
            }
            else
            {
                calculatedPremiums.structuralFee = Convert.ToDouble(calculatedPremiums.structuralFee * twelveYearLength);
            }
            //Coverpremium value Based on Builder Experience 
            if (Premiums.BuilderExperiance == "1" || Premiums.BuilderExperiance == "2" || Premiums.BuilderExperiance == "3" || Premiums.BuilderExperiance == "4")
            {
                calculatedPremiums.structuralFee = Convert.ToDouble(calculatedPremiums.structuralFee * builderExperianceBelow4Years);
            }
            else
            {
                calculatedPremiums.structuralFee = Convert.ToDouble(calculatedPremiums.structuralFee * builderExperianceAbove4Years);
            }
            //Coverpremium value Based on Newbuild Unit types and Stages of Wroks 
            if (plot.ConstructionType == "New Build")
            {
                if (plot.StageOfWork == "No Works Started")
                {
                    calculatedPremiums.structuralFee = Convert.ToDouble(calculatedPremiums.structuralFee * noworksStarted);
                }
                if (plot.StageOfWork == "Foundation/DPC")
                {
                    calculatedPremiums.structuralFee = Convert.ToDouble(calculatedPremiums.structuralFee * foundationLevel);

                }
                if (plot.StageOfWork == "1st Floor")
                {
                    calculatedPremiums.structuralFee = Convert.ToDouble(calculatedPremiums.structuralFee * firstFloorLevel);

                }
                if (plot.StageOfWork == "Wall Plate Level")
                {
                    calculatedPremiums.structuralFee = Convert.ToDouble(calculatedPremiums.structuralFee * wallPlateLevel);

                }
                if (plot.StageOfWork == "Roof/Watertight")
                {
                    calculatedPremiums.structuralFee = Convert.ToDouble(calculatedPremiums.structuralFee * roofLevel);
                }
            }

            //Coverpremium value Based on Conversion Unit Types and Stages of Wroks 
            if (plot.ConstructionType == "Conversion")
            {
                if (plot.StageOfWork == "No Works Started")
                {
                    calculatedPremiums.structuralFee = Convert.ToDouble(calculatedPremiums.structuralFee * noworksStarted);
                }
                if (plot.StageOfWork == "First Fix")
                {
                    calculatedPremiums.structuralFee = Convert.ToDouble(calculatedPremiums.structuralFee * firstFix);
                }
                if (plot.StageOfWork == "Second Fix")
                {
                    calculatedPremiums.structuralFee = Convert.ToDouble(calculatedPremiums.structuralFee * secondFix);
                }
            }

           
            if (Premiums.ProductsCovers.Count > 0)
            {
                if (Premiums.ContaminatedLandCover == "Yes")
                {
                    calculatedPremiums.contaminatedLandFee = Convert.ToDouble(0.00);
                }
                if (Premiums.LocalAuthorityBuildingControlFunctionCover == "Yes")
                {
                    calculatedPremiums.lABuildingControlFuntionFee = Convert.ToDouble(0.00);
                }
                if (Premiums.SeepageCover == "Yes")
                {
                    calculatedPremiums.seepageFee = Math.Round((seepageValue), 2);
                }
                if (Premiums.WaiversOfSubrogationRightsBuilderCover == "Yes")
                {
                   calculatedPremiums.waviersOfSubrogationRightsBuilderFee= Math.Round((waviersOfSubrogationBuilderValue), 2);
                }
                if (Premiums.LossOfGrossProfitCover == "Yes")
                {
                    LossOfGrossProfitCaluclations(plot);
                    calculatedPremiums.lossOfGrossProfitFee = Math.Round((lossOfGrossProfitFee), 2);
                }
                if (Premiums.LossOfRentPayableCover == "Yes")
                {
                    LossOfRentPayableCaluclations(plot);
                    calculatedPremiums.lossOfRentPayableFee = Math.Round((lossOfRentPayableFee), 2);
                }
                if (Premiums.LossOfRentReceivableCover == "Yes")
                {
                    LossOfRentReceivableCaluclations(plot);
                    calculatedPremiums.lossOfRentReceivableFee = Math.Round((lossOfRentReceivableFee), 2);
                }
                if (Premiums.WaiversOfSubrogationRightsEngineerCover == "Yes")
                {
                    calculatedPremiums.waviersOfSubrogationRightsEngineerFee = Math.Round((waviersOfSubrogationEngineerValue), 2);
                }
            }

            //Product Service Fee Values 
            if (Premiums.ProductServices.Count > 0)
            {

                if (Premiums.ProductTechnicalAuditFeeService == "Yes")
                {                    
                   calculatedPremiums.technicalFee= Math.Round((calculatedPremiums.technicalFee), 2);                  
                }
                if (Premiums.ProductAdminFeeService == "Yes")
                {
                    calculatedPremiums.administrationFee = Convert.ToDouble(0.00);
                }
                if (Premiums.ProductCancellationFeeService == "Yes")
                {
                    calculatedPremiums.cancellationFee = Convert.ToDouble(0.00);
                }
            }           

            return calculatedPremiums;
        }
       
        public void LossOfGrossProfitCaluclations(plotrows plot)
        {
            plotCount = Convert.ToInt32(Premiums.Plots);
            double lossOFGrossProfitFromPriceTable = Convert.ToDouble(excelTable.Rows[48][1]);       
            var annualAmountPerPlot =Convert.ToDouble(Premiums.LossOfGrossProfitInput / plotCount);
            var lossOfGrossProfitValue = (annualAmountPerPlot * Premiums.LossOfGrossProfitNumberOfYearsInput) * lossOFGrossProfitFromPriceTable;
            lossOfGrossProfitFee = Convert.ToDouble(lossOfGrossProfitValue);

        }
        public void LossOfRentReceivableCaluclations(plotrows plot)
        {
            plotCount = Convert.ToInt32(Premiums.Plots);            
            double lossOFRentReceivableFromPriceTable = Convert.ToDouble(excelTable.Rows[52][1]);
            var annualAmountPerPlot = Convert.ToDouble(Premiums.LossOfRentReceivableInput / plotCount);
            var lossOfRentReceivableValue = (annualAmountPerPlot * Premiums.LossOfRentReceivableNumberOfYearsInput) * lossOFRentReceivableFromPriceTable;
            lossOfRentReceivableFee = Convert.ToDouble(lossOfRentReceivableValue);

        }
        public void LossOfRentPayableCaluclations(plotrows plot)
        {
            plotCount = Convert.ToInt32(Premiums.Plots);
            double lossOFRentPayableFromPricingTable = Convert.ToDouble(excelTable.Rows[56][1]);         
            var annualAmountPerPlot = Convert.ToDouble(Premiums.LossOfRentPayableInput / plotCount);
            var lossOfRentPayableValue = (annualAmountPerPlot * Premiums.LossOfRentPayableNumberOfYearsInput) * lossOFRentPayableFromPricingTable;
            lossOfRentPayableFee = Convert.ToDouble(lossOfRentPayableValue);

        }


        public DataTable ReadCommercialMasterPremiumsData(string fileName)
        {

            var result = new DataSet();                                                                             //Set the First Row as Column Name
            using (var stream = File.Open(fileName, FileMode.Open, FileAccess.Read))
            using (var excelReader = ExcelReaderFactory.CreateOpenXmlReader(stream)) //.xlsx
            {
                //Return as DataSet
                result = excelReader.AsDataSet(new ExcelDataSetConfiguration()
                {
                    ConfigureDataTable = (_) => new ExcelDataTableConfiguration()
                    {
                        UseHeaderRow = true
                    }
                });
            }
            //Get all the Tables
            DataTableCollection table = result.Tables;
            DataTable resultTable = table[table.Count - 1];
            return resultTable;
        }



        //Verify the UI values against Speadsheet values
        public void VerfiyCommercialUIValues()
        {
            var productName = Premiums.eachplotrow.FirstOrDefault().ProductName;          

            //Veridy UI Structural Values
            var uiStructuralFee = $"//th[@class='product-title'][text()='{productName}']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[text()='Structural']//following-sibling::td[@class='right-align'][1]";
            var StructuralFee = Driver.FindElement(By.XPath(uiStructuralFee)).Text.Replace("£", "");
            double UIStructuralFee = Math.Round(Convert.ToDouble(StructuralFee), 2);
            double StructuralFeeDiff;
            var uiStructuralIPTFee = $"//th[@class='product-title'][text()='{productName}']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[text()='Structural']//following-sibling::td[@class='right-align'][2]";
            var StructuralIPTFee = Driver.FindElement(By.XPath(uiStructuralIPTFee)).Text.Replace("£", "");
            double UIStructuralIPTFee = Math.Round(Convert.ToDouble(StructuralIPTFee), 2);
            double StructuralIPTDiff;
            var uiStructuralTotalFee = $"//th[@class='product-title'][text()='{productName}']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[text()='Structural']//following-sibling::th[@class='right-align'][1]";
            var StructuralTotalFee = Driver.FindElement(By.XPath(uiStructuralTotalFee)).Text.Replace("£", "");
            double UIStructuralTotalFee = Math.Round(Convert.ToDouble(StructuralTotalFee), 2);
            double StructuralTotalValue = Math.Round(Convert.ToDouble(UIStructuralFee + UIStructuralIPTFee), 2);
            double StructuralTotalDiff;

            if (UIStructuralFee != Premiums.StructuralFee || UIStructuralIPTFee != Premiums.StructuralIPTFee || UIStructuralTotalFee != StructuralTotalValue)
            {
                StructuralFeeDiff = Convert.ToDouble(Premiums.StructuralFee - UIStructuralFee);
                StructuralIPTDiff = Convert.ToDouble(Premiums.StructuralIPTFee - UIStructuralIPTFee);
                StructuralTotalDiff = Convert.ToDouble(StructuralTotalValue - UIStructuralTotalFee);
                if (StructuralFeeDiff >= -1.01 && StructuralFeeDiff <= 1.01)
                {
                    Premiums.StructuralFee = UIStructuralFee;
                }
                if (StructuralIPTDiff >= -1.01 && StructuralIPTDiff <= 1.01)
                {
                    Premiums.StructuralIPTFee = UIStructuralIPTFee;
                }
                if (StructuralTotalDiff >= -1.01 && StructuralTotalDiff <= 1.01)
                {
                    StructuralTotalValue = UIStructuralTotalFee;
                }

                Assert.AreEqual(UIStructuralFee, Premiums.StructuralFee, $"StructuralFee Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                Assert.AreEqual(UIStructuralIPTFee, Premiums.StructuralIPTFee, $"StructuralIPTFee Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                Assert.AreEqual(UIStructuralTotalFee, StructuralTotalValue, $"StructuralTotalFee Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
            }
            else
            {

                Assert.AreEqual(UIStructuralFee, Premiums.StructuralFee, $"StructuralFee Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                Assert.AreEqual(UIStructuralIPTFee, Premiums.StructuralIPTFee, $"StructuralIPTFee Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                Assert.AreEqual(UIStructuralTotalFee, StructuralTotalValue, $"StructuralTotalFee Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
            }

            if (Premiums.ProductsCovers.Any(x => x.Contains("Contaminated Land")))
            {
                //Veirfy UI Contaminated Land values
                var uiContaminatedLandFee = $"//th[@class='product-title'][text()='{productName}']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[text()='Contaminated Land']//following-sibling::td[@class='right-align'][1]";
                var ContaminatedLandFee = Driver.FindElement(By.XPath(uiContaminatedLandFee)).Text.Replace("£", "");
                double UIContaminatedLandFee = Math.Round(Convert.ToDouble(ContaminatedLandFee), 2);
                double ContaminatedLandFeeDiff;
                var uiContaminatedLandIPTFee = $"//th[@class='product-title'][text()='{productName}']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[text()='Contaminated Land']//following-sibling::td[@class='right-align'][2]";
                var ContaminatedLandIPTFee = Driver.FindElement(By.XPath(uiContaminatedLandIPTFee)).Text.Replace("£", "");
                double UIContaminatedLandIPTFee = Math.Round(Convert.ToDouble(ContaminatedLandIPTFee), 2);
                double ContaminatedLandIPTDiff;
                var uiContaminatedLandTotalFee = $"//th[@class='product-title'][text()='{productName}']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[text()='Contaminated Land']//following-sibling::th[@class='right-align'][1]";
                var ContaminatedLandTotalFee = Driver.FindElement(By.XPath(uiContaminatedLandTotalFee)).Text.Replace("£", "");
                double UIContaminatedLandTotalFee = Math.Round(Convert.ToDouble(ContaminatedLandTotalFee), 2);
                double ContaminatedLandTotalValue = Math.Round(Convert.ToDouble(UIContaminatedLandFee + UIContaminatedLandIPTFee), 2);
                double ContaminatedLandTotalDiff;

                if (UIContaminatedLandFee != Premiums.ContaminatedLandFee || UIContaminatedLandIPTFee != Premiums.ContaminatedLandIPTFee || UIContaminatedLandTotalFee != ContaminatedLandTotalValue)
                {
                    ContaminatedLandFeeDiff = Convert.ToDouble(Premiums.ContaminatedLandFee - UIContaminatedLandFee);
                    ContaminatedLandIPTDiff = Convert.ToDouble(Premiums.ContaminatedLandIPTFee - UIContaminatedLandIPTFee);
                    ContaminatedLandTotalDiff = Convert.ToDouble(ContaminatedLandTotalValue - UIContaminatedLandTotalFee);
                    if (ContaminatedLandFeeDiff >= -1.01 && ContaminatedLandFeeDiff <= 1.01)
                    {
                        Premiums.ContaminatedLandFee = UIContaminatedLandFee;
                    }
                    if (ContaminatedLandIPTDiff >= -1.01 && ContaminatedLandIPTDiff <= 1.01)
                    {
                        Premiums.ContaminatedLandIPTFee = UIContaminatedLandIPTFee;
                    }
                    if (ContaminatedLandTotalDiff >= -1.01 && ContaminatedLandTotalDiff <= 1.01)
                    {
                        ContaminatedLandTotalValue = UIContaminatedLandTotalFee;
                    }

                    Assert.AreEqual(UIContaminatedLandFee, Premiums.ContaminatedLandFee, $"ContaminatedLandFee Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UIContaminatedLandIPTFee, Premiums.ContaminatedLandIPTFee, $"ContaminatedLandIPTFee Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UIContaminatedLandTotalFee, ContaminatedLandTotalValue, $"ContaminatedLandTotalFee Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                }
                else
                {
                    Assert.AreEqual(UIContaminatedLandFee, Premiums.ContaminatedLandFee, $"ContaminatedLandFee Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UIContaminatedLandIPTFee, Premiums.ContaminatedLandIPTFee, $"ContaminatedLandIPTFee Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UIContaminatedLandTotalFee, ContaminatedLandTotalValue, $"ContaminatedLandTotalFee Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                }
            }
            if (Premiums.ProductsCovers.Any(x => x.Contains("Additional Cover for Local Authority Building Control Function")))
            {

                //Verify UI Local Authority Building Control Values
                var uiLocalAuthorityBuildingControlFee = $"//th[@class='product-title'][text()='{productName}']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[text()='Additional Cover for Local Authority Building Control Function']//following-sibling::td[@class='right-align'][1]";
                var LocalAuthorityBuildingControlFee = Driver.FindElement(By.XPath(uiLocalAuthorityBuildingControlFee)).Text.Replace("£", "");
                double UILocalAuthorityBuildingControlFee = Math.Round(Convert.ToDouble(LocalAuthorityBuildingControlFee), 2);
                double LocalAuthorityBuildingControlFeeDiff;

                var uiLocalAuthorityBuildingControlIPTFee = $"//th[@class='product-title'][text()='{productName}']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[text()='Additional Cover for Local Authority Building Control Function']//following-sibling::td[@class='right-align'][2]";
                var LocalAuthorityBuildingControlIPTFee = Driver.FindElement(By.XPath(uiLocalAuthorityBuildingControlIPTFee)).Text.Replace("£", "");
                double UILocalAuthorityBuildingControlIPTFee = Math.Round(Convert.ToDouble(LocalAuthorityBuildingControlIPTFee), 2);
                double LocalAuthorityBuildingControlIPTDiff;

                var uiLocalAuthorityBuildingControlTotalFee = $"//th[@class='product-title'][text()='{productName}']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[text()='Additional Cover for Local Authority Building Control Function']//following-sibling::th[@class='right-align'][1]";
                var LocalAuthorityBuildingControlTotalFee = Driver.FindElement(By.XPath(uiLocalAuthorityBuildingControlTotalFee)).Text.Replace("£", "");
                double UILocalAuthorityBuildingControlTotalFee = Math.Round(Convert.ToDouble(LocalAuthorityBuildingControlTotalFee), 2);
                double LocalAuthorityBuildingControlTotalValue = Math.Round(Convert.ToDouble(UILocalAuthorityBuildingControlFee + UILocalAuthorityBuildingControlIPTFee), 2);
                double LocalAuthorityBuildingControlTotalDiff;

                if (UILocalAuthorityBuildingControlFee != Premiums.LocalAuthorityBuildingControlFee || UILocalAuthorityBuildingControlIPTFee != Premiums.LocalAuthorityBuildingControlIPTFee || UILocalAuthorityBuildingControlTotalFee != LocalAuthorityBuildingControlTotalValue)
                {
                    LocalAuthorityBuildingControlFeeDiff = Convert.ToDouble(Premiums.LocalAuthorityBuildingControlFee - UILocalAuthorityBuildingControlFee);
                    LocalAuthorityBuildingControlIPTDiff = Convert.ToDouble(Premiums.LocalAuthorityBuildingControlIPTFee - UILocalAuthorityBuildingControlIPTFee);
                    LocalAuthorityBuildingControlTotalDiff = Convert.ToDouble(LocalAuthorityBuildingControlTotalValue - UILocalAuthorityBuildingControlTotalFee);
                    if (LocalAuthorityBuildingControlFeeDiff >= -1.01 && LocalAuthorityBuildingControlFeeDiff <= 1.01)
                    {
                        Premiums.LocalAuthorityBuildingControlFee = UILocalAuthorityBuildingControlFee;
                    }
                    if (LocalAuthorityBuildingControlIPTDiff >= -1.01 && LocalAuthorityBuildingControlIPTDiff <= 1.01)
                    {
                        Premiums.LocalAuthorityBuildingControlIPTFee = UILocalAuthorityBuildingControlIPTFee;
                    }
                    if (LocalAuthorityBuildingControlTotalDiff >= -1.01 && LocalAuthorityBuildingControlTotalDiff <= 1.01)
                    {
                        LocalAuthorityBuildingControlTotalValue = UILocalAuthorityBuildingControlTotalFee;
                    }

                    Assert.AreEqual(UILocalAuthorityBuildingControlFee, Premiums.LocalAuthorityBuildingControlFee, $"LocalAuthorityBuildingControlFee Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UILocalAuthorityBuildingControlIPTFee, Premiums.LocalAuthorityBuildingControlIPTFee, $"LocalAuthorityBuildingControlIPTFee Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UILocalAuthorityBuildingControlTotalFee, LocalAuthorityBuildingControlTotalValue, $"LocalAuthorityBuildingControlTotalFee Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                }
                else
                {
                    Assert.AreEqual(UILocalAuthorityBuildingControlFee, Premiums.LocalAuthorityBuildingControlFee, $"LocalAuthorityBuildingControlFee Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UILocalAuthorityBuildingControlIPTFee, Premiums.LocalAuthorityBuildingControlIPTFee, $"LocalAuthorityBuildingControlIPTFee Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UILocalAuthorityBuildingControlTotalFee, LocalAuthorityBuildingControlTotalValue, $"LocalAuthorityBuildingControlTotalFee Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                }

            }
            if (Premiums.ProductsCovers.Any(x => x.Contains("Seepage")))
            {
                //UI Seepage Values
                var uiSeepageFee = $"//th[@class='product-title'][text()='{productName}']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[text()='Seepage']//following-sibling::td[@class='right-align'][1]";
                var SeepageFee = Driver.FindElement(By.XPath(uiSeepageFee)).Text.Replace("£", "");
                double UISeepageFee = Math.Round(Convert.ToDouble(SeepageFee), 2);
                double SeepageFeeDiff;

                var uiSeepageIPTFee = $"//th[@class='product-title'][text()='{productName}']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[text()='Seepage']//following-sibling::td[@class='right-align'][2]";
                var SeepageIPTFee = Driver.FindElement(By.XPath(uiSeepageIPTFee)).Text.Replace("£", "");
                double UISeepageIPTFee = Math.Round(Convert.ToDouble(SeepageIPTFee), 2);
                double SeepageIPTDiff;

                var uiSeepageTotalFee = $"//th[@class='product-title'][text()='{productName}']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[text()='Seepage']//following-sibling::th[@class='right-align'][1]";
                var SeepageTotalFee = Driver.FindElement(By.XPath(uiSeepageTotalFee)).Text.Replace("£", "");
                double UISeepageTotalFee = Math.Round(Convert.ToDouble(SeepageTotalFee), 2);
                double SeepageTotalValue = Math.Round(Convert.ToDouble(UISeepageFee + UISeepageIPTFee), 2);
                double SeepageTotalDiff;

                if (UISeepageFee != Premiums.SeepageFee || UISeepageIPTFee != Premiums.SeepageIPTFee || UISeepageTotalFee != SeepageTotalValue)
                {
                    SeepageFeeDiff = Convert.ToDouble(Premiums.SeepageFee - UISeepageFee);
                    SeepageIPTDiff = Convert.ToDouble(Premiums.SeepageIPTFee - UISeepageIPTFee);
                    SeepageTotalDiff = Convert.ToDouble(SeepageTotalValue - UISeepageTotalFee);
                    if (SeepageFeeDiff >= -1.01 && SeepageFeeDiff <= 1.01)
                    {
                        Premiums.SeepageFee = UISeepageFee;
                    }
                    if (SeepageIPTDiff >= -1.01 && SeepageIPTDiff <= 1.01)
                    {
                        Premiums.SeepageIPTFee = UISeepageIPTFee;
                    }
                    if (SeepageTotalDiff >= -1.01 && SeepageTotalDiff <= 1.01)
                    {
                        SeepageTotalValue = UISeepageTotalFee;
                    }

                    Assert.AreEqual(UISeepageFee, Premiums.SeepageFee, $"SeepageFee Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UISeepageIPTFee, Premiums.SeepageIPTFee, $"SeepageIPTFee Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UISeepageTotalFee, SeepageTotalValue, $"SeepageTotalValue Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                }
                else
                {
                    Assert.AreEqual(UISeepageFee, Premiums.SeepageFee, $"SeepageFee Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UISeepageIPTFee, Premiums.SeepageIPTFee, $"SeepageIPTFee Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UISeepageTotalFee, SeepageTotalValue, $"SeepageTotalValue Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                }
            }
            if (Premiums.ProductsCovers.Any(x => x.Contains("Waivers of Subrogation Rights - Builder")))
            {

                //ui Waivers of Subrogation Rights - Builder values
                var uiWaiversOfSubrogationRightsBuilderFee = $"//th[@class='product-title'][text()='{productName}']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[text()='Waivers of Subrogation Rights - Builder']//following-sibling::td[@class='right-align'][1]";
                var WaiversOfSubrogationRightsBuilderFee = Driver.FindElement(By.XPath(uiWaiversOfSubrogationRightsBuilderFee)).Text.Replace("£", "");
                double UIWaiversOfSubrogationRightsBuilderFee = Math.Round(Convert.ToDouble(WaiversOfSubrogationRightsBuilderFee), 2);
                double WaiversOfSubrogationRightsBuilderFeeDiff;

                var uiWaiversOfSubrogationRightsBuilderIPTFee = $"//th[@class='product-title'][text()='{productName}']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[text()='Waivers of Subrogation Rights - Builder']//following-sibling::td[@class='right-align'][2]";
                var WaiversOfSubrogationRightsBuilderIPTFee = Driver.FindElement(By.XPath(uiWaiversOfSubrogationRightsBuilderIPTFee)).Text.Replace("£", "");
                double UIWaiversOfSubrogationRightsBuilderIPTFee = Math.Round(Convert.ToDouble(WaiversOfSubrogationRightsBuilderIPTFee), 2);
                double WaiversOfSubrogationRightsBuilderIPTDiff;

                var uiWaiversOfSubrogationRightsBuilderTotalFee = $"//th[@class='product-title'][text()='{productName}']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[text()='Waivers of Subrogation Rights - Builder']//following-sibling::th[@class='right-align'][1]";
                var WaiversOfSubrogationRightsBuilderTotalFee = Driver.FindElement(By.XPath(uiWaiversOfSubrogationRightsBuilderTotalFee)).Text.Replace("£", "");
                double UIWaiversOfSubrogationRightsBuilderTotalFee = Math.Round(Convert.ToDouble(WaiversOfSubrogationRightsBuilderTotalFee), 2);
                double WaiversOfSubrogationRightsBuilderTotalValue = Math.Round(Convert.ToDouble(UIWaiversOfSubrogationRightsBuilderFee + UIWaiversOfSubrogationRightsBuilderIPTFee), 2);
                double WaiversOfSubrogationRightsBuilderTotalDiff;

                if (UIWaiversOfSubrogationRightsBuilderFee != Premiums.WaiversOfSubrogationBuilderFee || UIWaiversOfSubrogationRightsBuilderIPTFee != Premiums.WaiversOfSubrogationBuilderIPTFee || UIWaiversOfSubrogationRightsBuilderTotalFee != WaiversOfSubrogationRightsBuilderTotalValue)
                {
                    WaiversOfSubrogationRightsBuilderFeeDiff = Convert.ToDouble(Premiums.WaiversOfSubrogationBuilderFee - UIWaiversOfSubrogationRightsBuilderFee);
                    WaiversOfSubrogationRightsBuilderIPTDiff = Convert.ToDouble(Premiums.WaiversOfSubrogationBuilderIPTFee - UIWaiversOfSubrogationRightsBuilderIPTFee);
                    WaiversOfSubrogationRightsBuilderTotalDiff = Convert.ToDouble(WaiversOfSubrogationRightsBuilderTotalValue - UIWaiversOfSubrogationRightsBuilderTotalFee);
                    if (WaiversOfSubrogationRightsBuilderFeeDiff >= -1.01 && WaiversOfSubrogationRightsBuilderFeeDiff <= 1.01)
                    {
                        Premiums.WaiversOfSubrogationBuilderFee = UIWaiversOfSubrogationRightsBuilderFee;
                    }
                    if (WaiversOfSubrogationRightsBuilderIPTDiff >= -1.01 && WaiversOfSubrogationRightsBuilderIPTDiff <= 1.01)
                    {
                        Premiums.WaiversOfSubrogationBuilderIPTFee = UIWaiversOfSubrogationRightsBuilderIPTFee;
                    }
                    if (WaiversOfSubrogationRightsBuilderTotalDiff >= -1.01 && WaiversOfSubrogationRightsBuilderTotalDiff <= 1.01)
                    {
                        WaiversOfSubrogationRightsBuilderTotalValue = UIWaiversOfSubrogationRightsBuilderTotalFee;
                    }

                    Assert.AreEqual(UIWaiversOfSubrogationRightsBuilderFee, Premiums.WaiversOfSubrogationBuilderFee, $"WaiversOfSubrogationBuilderFee Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UIWaiversOfSubrogationRightsBuilderIPTFee, Premiums.WaiversOfSubrogationBuilderIPTFee, $"WaiversOfSubrogationBuilderIPTFee Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UIWaiversOfSubrogationRightsBuilderTotalFee, WaiversOfSubrogationRightsBuilderTotalValue, $"WaiversOfSubrogationRightsBuilderTotalValue Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                }
                else
                {
                    Assert.AreEqual(UIWaiversOfSubrogationRightsBuilderFee, Premiums.WaiversOfSubrogationBuilderFee, $"WaiversOfSubrogationBuilderFee Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UIWaiversOfSubrogationRightsBuilderIPTFee, Premiums.WaiversOfSubrogationBuilderIPTFee, $"WaiversOfSubrogationBuilderIPTFee Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UIWaiversOfSubrogationRightsBuilderTotalFee, WaiversOfSubrogationRightsBuilderTotalValue, $"WaiversOfSubrogationRightsBuilderTotalValue Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                }
            }
            if (Premiums.ProductsCovers.Any(x => x.Contains("Loss of Gross Profit")))
            {

                //ui Loss of Gross Profit Values
                var uiLossOfGrossProfitFee = $"//th[@class='product-title'][text()='{productName}']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[text()='Loss of Gross Profit']//following-sibling::td[@class='right-align'][1]";
                var LossOfGrossProfitFee = Driver.FindElement(By.XPath(uiLossOfGrossProfitFee)).Text.Replace("£", "");
                double UILossOfGrossProfitFee = Math.Round(Convert.ToDouble(LossOfGrossProfitFee), 2);
                double LossOfGrossProfitFeeDiff;

                var uiLossOfGrossProfitIPTFee = $"//th[@class='product-title'][text()='{productName}']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[text()='Loss of Gross Profit']//following-sibling::td[@class='right-align'][2]";
                var LossOfGrossProfitIPTFee = Driver.FindElement(By.XPath(uiLossOfGrossProfitIPTFee)).Text.Replace("£", "");
                double UILossOfGrossProfitIPTFee = Math.Round(Convert.ToDouble(LossOfGrossProfitIPTFee), 2);
                double LossOfGrossProfitIPTDiff;

                var uiLossOfGrossProfitTotalFee = $"//th[@class='product-title'][text()='{productName}']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[text()='Loss of Gross Profit']//following-sibling::th[@class='right-align'][1]";
                var LossOfGrossProfitTotalFee = Driver.FindElement(By.XPath(uiLossOfGrossProfitTotalFee)).Text.Replace("£", "");
                double UILossOfGrossProfitTotalFee = Math.Round(Convert.ToDouble(LossOfGrossProfitTotalFee), 2);
                double LossOfGrossProfitTotalValue = Math.Round(Convert.ToDouble(UILossOfGrossProfitFee + UILossOfGrossProfitIPTFee), 2);
                double LossOfGrossProfitTotalDiff;

                if (UILossOfGrossProfitFee != Premiums.LossOfGrossProfitFee || UILossOfGrossProfitIPTFee != Premiums.LossOfGrossProfitIPTFee || UILossOfGrossProfitTotalFee != LossOfGrossProfitTotalValue)
                {
                    LossOfGrossProfitFeeDiff = Convert.ToDouble(Premiums.LossOfGrossProfitFee - UILossOfGrossProfitFee);
                    LossOfGrossProfitIPTDiff = Convert.ToDouble(Premiums.LossOfGrossProfitIPTFee - UILossOfGrossProfitIPTFee);
                    LossOfGrossProfitTotalDiff = Convert.ToDouble(LossOfGrossProfitTotalValue - UILossOfGrossProfitTotalFee);
                    if (LossOfGrossProfitFeeDiff >= -1.01 && LossOfGrossProfitFeeDiff <= 1.01)
                    {
                        Premiums.LossOfGrossProfitFee = UILossOfGrossProfitFee;
                    }
                    if (LossOfGrossProfitIPTDiff >= -1.01 && LossOfGrossProfitIPTDiff <= 1.01)
                    {
                        Premiums.LossOfGrossProfitIPTFee = UILossOfGrossProfitIPTFee;
                    }
                    if (LossOfGrossProfitTotalDiff >= -1.01 && LossOfGrossProfitTotalDiff <= 1.01)
                    {
                        LossOfGrossProfitTotalValue = UILossOfGrossProfitTotalFee;
                    }

                    Assert.AreEqual(UILossOfGrossProfitFee, Premiums.LossOfGrossProfitFee, $"LossOfGrossProfitFee Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UILossOfGrossProfitIPTFee, Premiums.LossOfGrossProfitIPTFee, $"LossOfGrossProfitIPTFee Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UILossOfGrossProfitTotalFee, LossOfGrossProfitTotalValue, $"LossOfGrossProfitTotalValue Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                }
                else
                {
                    Assert.AreEqual(UILossOfGrossProfitFee, Premiums.LossOfGrossProfitFee, $"LossOfGrossProfitFee Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UILossOfGrossProfitIPTFee, Premiums.LossOfGrossProfitIPTFee, $"LossOfGrossProfitIPTFee Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UILossOfGrossProfitTotalFee, LossOfGrossProfitTotalValue, $"LossOfGrossProfitTotalValue Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                }
            }
            if (Premiums.ProductsCovers.Any(x => x.Contains("Loss of Rent Payable")))
            {

                //ui Loss of Rent Payable Values
                var uiLossOfRentPayableFee = $"//th[@class='product-title'][text()='{productName}']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[text()='Loss of Rent Payable']//following-sibling::td[@class='right-align'][1]";
                var LossOfRentPayableFee = Driver.FindElement(By.XPath(uiLossOfRentPayableFee)).Text.Replace("£", "");
                double UILossOfRentPayableFee = Math.Round(Convert.ToDouble(LossOfRentPayableFee), 2);
                double LossOfRentPayableFeeDiff;

                var uiLossOfRentPayableIPTFee = $"//th[@class='product-title'][text()='{productName}']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[text()='Loss of Rent Payable']//following-sibling::td[@class='right-align'][2]";
                var LossOfRentPayableIPTFee = Driver.FindElement(By.XPath(uiLossOfRentPayableIPTFee)).Text.Replace("£", "");
                double UILossOfRentPayableIPTFee = Math.Round(Convert.ToDouble(LossOfRentPayableIPTFee), 2);
                double LossOfRentPayableIPTDiff;

                var uiLossOfRentPayableTotalFee = $"//th[@class='product-title'][text()='{productName}']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[text()='Loss of Rent Payable']//following-sibling::th[@class='right-align'][1]";
                var LossOfRentPayableTotalFee = Driver.FindElement(By.XPath(uiLossOfRentPayableTotalFee)).Text.Replace("£", "");
                double UILossOfRentPayableTotalFee = Math.Round(Convert.ToDouble(LossOfRentPayableTotalFee), 2);
                double LossOfRentPayableTotalValue = Math.Round(Convert.ToDouble(UILossOfRentPayableFee + UILossOfRentPayableIPTFee), 2);
                double LossOfRentPayableTotalDiff;

                if (UILossOfRentPayableFee != Premiums.LossOfRentPayableFee || UILossOfRentPayableIPTFee != Premiums.LossOfRentPayableIPTFee || UILossOfRentPayableTotalFee != LossOfRentPayableTotalValue)
                {
                    LossOfRentPayableFeeDiff = Convert.ToDouble(Premiums.LossOfRentPayableFee - UILossOfRentPayableFee);
                    LossOfRentPayableIPTDiff = Convert.ToDouble(Premiums.LossOfRentPayableIPTFee - UILossOfRentPayableIPTFee);
                    LossOfRentPayableTotalDiff = Convert.ToDouble(LossOfRentPayableTotalValue - UILossOfRentPayableTotalFee);
                    if (LossOfRentPayableFeeDiff >= -1.01 && LossOfRentPayableFeeDiff <= 1.01)
                    {
                        Premiums.LossOfRentPayableFee = UILossOfRentPayableFee;
                    }
                    if (LossOfRentPayableIPTDiff >= -1.01 && LossOfRentPayableIPTDiff <= 1.01)
                    {
                        Premiums.LossOfRentPayableIPTFee = UILossOfRentPayableIPTFee;
                    }
                    if (LossOfRentPayableTotalDiff >= -1.01 && LossOfRentPayableTotalDiff <= 1.01)
                    {
                        LossOfRentPayableTotalValue = UILossOfRentPayableTotalFee;
                    }

                    Assert.AreEqual(UILossOfRentPayableFee, Premiums.LossOfRentPayableFee, $"LossOfRentPayableFee Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UILossOfRentPayableIPTFee, Premiums.LossOfRentPayableIPTFee, $"LossOfRentPayableIPTFee Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UILossOfRentPayableTotalFee, LossOfRentPayableTotalValue, $"LossOfRentPayableTotalValue Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                }
                else
                {
                    Assert.AreEqual(UILossOfRentPayableFee, Premiums.LossOfRentPayableFee, $"LossOfRentPayableFee Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UILossOfRentPayableIPTFee, Premiums.LossOfRentPayableIPTFee, $"LossOfRentPayableIPTFee Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UILossOfRentPayableTotalFee, LossOfRentPayableTotalValue, $"LossOfRentPayableTotalValue Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                }
            }

            if (Premiums.ProductsCovers.Any(x => x.Contains("Loss of Rent Receivable")))
            {

                //ui Loss of Rent Receivable Values
                var uiLossofRentReceivableFee = $"//th[@class='product-title'][text()='{productName}']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[text()='Loss of Rent Receivable']//following-sibling::td[@class='right-align'][1]";
                var LossofRentReceivableFee = Driver.FindElement(By.XPath(uiLossofRentReceivableFee)).Text.Replace("£", "");
                double UILossofRentReceivableFee = Math.Round(Convert.ToDouble(LossofRentReceivableFee), 2);
                double LossofRentReceivableFeeDiff;

                var uiLossofRentReceivableIPTFee = $"//th[@class='product-title'][text()='{productName}']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[text()='Loss of Rent Receivable']//following-sibling::td[@class='right-align'][2]";
                var LossofRentReceivableIPTFee = Driver.FindElement(By.XPath(uiLossofRentReceivableIPTFee)).Text.Replace("£", "");
                double UILossofRentReceivableIPTFee = Math.Round(Convert.ToDouble(LossofRentReceivableIPTFee), 2);
                double LossofRentReceivableIPTDiff;

                var uiLossofRentReceivableTotalFee = $"//th[@class='product-title'][text()='{productName}']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[text()='Loss of Rent Receivable']//following-sibling::th[@class='right-align'][1]";
                var LossofRentReceivableTotalFee = Driver.FindElement(By.XPath(uiLossofRentReceivableTotalFee)).Text.Replace("£", "");
                double UILossofRentReceivableTotalFee = Math.Round(Convert.ToDouble(LossofRentReceivableTotalFee), 2);
                double LossofRentReceivableTotalValue = Math.Round(Convert.ToDouble(UILossofRentReceivableFee + UILossofRentReceivableIPTFee), 2);
                double LossofRentReceivableTotalDiff;

                if (UILossofRentReceivableFee != Premiums.LossOfRentReceivableFee || UILossofRentReceivableIPTFee != Premiums.LossOfRentReceivableIPTFee || UILossofRentReceivableTotalFee != LossofRentReceivableTotalValue)
                {
                    LossofRentReceivableFeeDiff = Convert.ToDouble(Premiums.LossOfRentReceivableFee - UILossofRentReceivableFee);
                    LossofRentReceivableIPTDiff = Convert.ToDouble(Premiums.LossOfRentReceivableIPTFee - UILossofRentReceivableIPTFee);
                    LossofRentReceivableTotalDiff = Convert.ToDouble(LossofRentReceivableTotalValue - UILossofRentReceivableTotalFee);

                    if (LossofRentReceivableFeeDiff >= -1.01 && LossofRentReceivableFeeDiff <= 1.01)
                    {
                        Premiums.LossOfRentReceivableFee = UILossofRentReceivableFee;
                    }
                    if (LossofRentReceivableIPTDiff >= -1.01 && LossofRentReceivableIPTDiff <= 1.01)
                    {
                        Premiums.LossOfRentReceivableIPTFee = UILossofRentReceivableIPTFee;
                    }
                    if (LossofRentReceivableTotalDiff >= -1.01 && LossofRentReceivableTotalDiff <= 1.01)
                    {
                        LossofRentReceivableTotalValue = UILossofRentReceivableTotalFee;
                    }

                    Assert.AreEqual(UILossofRentReceivableFee, Premiums.LossOfRentReceivableFee, $"LossOfRentReceivableFee Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UILossofRentReceivableIPTFee, Premiums.LossOfRentReceivableIPTFee, $"LossOfRentReceivableIPTFee Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UILossofRentReceivableTotalFee, LossofRentReceivableTotalValue, $"LossofRentReceivableTotalValue Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                }
                else
                {
                    Assert.AreEqual(UILossofRentReceivableFee, Premiums.LossOfRentReceivableFee, $"LossOfRentReceivableFee Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UILossofRentReceivableIPTFee, Premiums.LossOfRentReceivableIPTFee, $"LossOfRentReceivableIPTFee Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UILossofRentReceivableTotalFee, LossofRentReceivableTotalValue, $"LossofRentReceivableTotalValue Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                }
            }
            if (Premiums.ProductsCovers.Any(x => x.Contains("Waivers of Subrogation Rights - Engineer")))
            {

                //Verify UI Waivers of Subrogation Rights - Builder values
                var uiWaiversOfSubrogationRightsEngineerFee = $"//th[@class='product-title'][text()='{productName}']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[text()='Waivers of Subrogation Rights - Engineer']//following-sibling::td[@class='right-align'][1]";
                var WaiversOfSubrogationRightsEngineerFee = Driver.FindElement(By.XPath(uiWaiversOfSubrogationRightsEngineerFee)).Text.Replace("£", "");
                double UIWaiversOfSubrogationRightsEngineerFee = Math.Round(Convert.ToDouble(WaiversOfSubrogationRightsEngineerFee), 2);
                double WaiversOfSubrogationRightsEngineerFeeDiff;

                var uiWaiversOfSubrogationRightsEngineerIPTFee = $"//th[@class='product-title'][text()='{productName}']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[text()='Waivers of Subrogation Rights - Engineer']//following-sibling::td[@class='right-align'][2]";
                var WaiversOfSubrogationRightsEngineerIPTFee = Driver.FindElement(By.XPath(uiWaiversOfSubrogationRightsEngineerIPTFee)).Text.Replace("£", "");
                double UIWaiversOfSubrogationRightsEngineerIPTFee = Math.Round(Convert.ToDouble(WaiversOfSubrogationRightsEngineerIPTFee), 2);
                double WaiversOfSubrogationRightsEngineerIPTDiff;

                var uiWaiversOfSubrogationRightsEngineerTotalFee = $"//th[@class='product-title'][text()='{productName}']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[text()='Waivers of Subrogation Rights - Engineer']//following-sibling::th[@class='right-align'][1]";
                var WaiversOfSubrogationRightsEngineerTotalFee = Driver.FindElement(By.XPath(uiWaiversOfSubrogationRightsEngineerTotalFee)).Text.Replace("£", "");
                double UIWaiversOfSubrogationRightsEngineerTotalFee = Math.Round(Convert.ToDouble(WaiversOfSubrogationRightsEngineerTotalFee), 2);
                double WaiversOfSubrogationRightsEngineerTotalValue = Math.Round(Convert.ToDouble(UIWaiversOfSubrogationRightsEngineerFee + UIWaiversOfSubrogationRightsEngineerIPTFee), 2);
                double WaiversOfSubrogationRightsEngineerTotalDiff;

                if (UIWaiversOfSubrogationRightsEngineerFee != Premiums.WaiversOfSubrogationEngineerFee || UIWaiversOfSubrogationRightsEngineerIPTFee != Premiums.WaiversOfSubrogationEngineerIPTFee || UIWaiversOfSubrogationRightsEngineerTotalFee != WaiversOfSubrogationRightsEngineerTotalValue)
                {
                    WaiversOfSubrogationRightsEngineerFeeDiff = Convert.ToDouble(Premiums.WaiversOfSubrogationEngineerFee - UIWaiversOfSubrogationRightsEngineerFee);
                    WaiversOfSubrogationRightsEngineerIPTDiff = Convert.ToDouble(Premiums.WaiversOfSubrogationEngineerIPTFee - UIWaiversOfSubrogationRightsEngineerIPTFee);
                    WaiversOfSubrogationRightsEngineerTotalDiff = Convert.ToDouble(WaiversOfSubrogationRightsEngineerTotalValue - UIWaiversOfSubrogationRightsEngineerTotalFee);
                    if (WaiversOfSubrogationRightsEngineerFeeDiff >= -1.01 && WaiversOfSubrogationRightsEngineerFeeDiff <= 1.01)
                    {
                        Premiums.WaiversOfSubrogationEngineerFee = UIWaiversOfSubrogationRightsEngineerFee;
                    }

                    if (WaiversOfSubrogationRightsEngineerIPTDiff >= -1.01 && WaiversOfSubrogationRightsEngineerIPTDiff <= 1.01)
                    {
                        Premiums.WaiversOfSubrogationEngineerIPTFee = UIWaiversOfSubrogationRightsEngineerIPTFee;
                    }
                    if (WaiversOfSubrogationRightsEngineerTotalDiff >= -1.01 && WaiversOfSubrogationRightsEngineerTotalDiff <= 1.01)
                    {
                        WaiversOfSubrogationRightsEngineerTotalValue = UIWaiversOfSubrogationRightsEngineerTotalFee;
                    }

                    Assert.AreEqual(UIWaiversOfSubrogationRightsEngineerFee, Premiums.WaiversOfSubrogationEngineerFee, $"WaiversOfSubrogationEngineerFee Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UIWaiversOfSubrogationRightsEngineerIPTFee, Premiums.WaiversOfSubrogationEngineerIPTFee, $"WaiversOfSubrogationEngineerIPTFee Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UIWaiversOfSubrogationRightsEngineerTotalFee, WaiversOfSubrogationRightsEngineerTotalValue, $"WaiversOfSubrogationRightsEngineerTotalValue Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                }
                else
                {
                    Assert.AreEqual(UIWaiversOfSubrogationRightsEngineerFee, Premiums.WaiversOfSubrogationEngineerFee, $"WaiversOfSubrogationEngineerFee Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UIWaiversOfSubrogationRightsEngineerIPTFee, Premiums.WaiversOfSubrogationEngineerIPTFee, $"WaiversOfSubrogationEngineerIPTFee Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UIWaiversOfSubrogationRightsEngineerTotalFee, WaiversOfSubrogationRightsEngineerTotalValue, $"WaiversOfSubrogationRightsEngineerTotalValue Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                }
            }
            //Services Elements  and  Fee values  
            //Technical Audit Fee values  
            if (Premiums.ProductServices.Any(x => x.Contains("Technical Audit Fee")))
            {


                var uiTechnicalAuditFee = $"//th[@class='product-title'][text()='{productName}']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[text()='Technical Audit Fee']//following-sibling::td[@class='right-align'][1]";
                var TechnicalAuditFee = Driver.FindElement(By.XPath(uiTechnicalAuditFee)).Text.Replace("£", "");
                double UITechnicalAuditFee = Math.Round(Convert.ToDouble(TechnicalAuditFee), 2);
                double TechnicalAuditFeeDiff;

                var uiTechnicalAuditVATFee = $"//th[@class='product-title'][text()='{productName}']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[text()='Technical Audit Fee']//following-sibling::td[@class='right-align'][2]";
                var TechnicalAuditVATFee = Driver.FindElement(By.XPath(uiTechnicalAuditVATFee)).Text.Replace("£", "");
                double UITechnicalAuditVATFee = Math.Round(Convert.ToDouble(TechnicalAuditVATFee), 2);
                double TechnicalAuditVATDiff;

                var uiTechnicalAuditTotalFee = $"//th[@class='product-title'][text()='{productName}']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[text()='Technical Audit Fee']//following-sibling::th[@class='right-align'][1]";
                var TechnicalAuditTotalFee = Driver.FindElement(By.XPath(uiTechnicalAuditTotalFee)).Text.Replace("£", "");
                double UITechnicalAuditTotalFee = Math.Round(Convert.ToDouble(TechnicalAuditTotalFee), 2);
                double TechnicalAuditTotalValue = Math.Round(Convert.ToDouble(UITechnicalAuditFee + UITechnicalAuditVATFee), 2);
                double TechnicalAuditTotalDiff;

                if (UITechnicalAuditFee != Premiums.TechnicalAuditFee || UITechnicalAuditVATFee != Premiums.TechnicalAuditVATFee || UITechnicalAuditTotalFee != TechnicalAuditTotalValue)
                {
                    TechnicalAuditFeeDiff = Convert.ToDouble(Premiums.TechnicalAuditFee - UITechnicalAuditFee);
                    TechnicalAuditVATDiff = Convert.ToDouble(Premiums.TechnicalAuditVATFee - UITechnicalAuditVATFee);
                    TechnicalAuditTotalDiff = Convert.ToDouble(TechnicalAuditTotalValue - UITechnicalAuditTotalFee);
                    if (TechnicalAuditFeeDiff >= -1.01 && TechnicalAuditFeeDiff <= 1.01)
                    {
                        Premiums.TechnicalAuditFee = UITechnicalAuditFee;
                    }

                    if (TechnicalAuditVATDiff >= -1.01 && TechnicalAuditVATDiff <= 1.01)
                    {
                        Premiums.TechnicalAuditVATFee = UITechnicalAuditVATFee;
                    }
                    if (TechnicalAuditTotalDiff >= -1.01 && TechnicalAuditTotalDiff <= 1.01)
                    {
                        TechnicalAuditTotalValue = UITechnicalAuditTotalFee;
                    }

                    Assert.AreEqual(UITechnicalAuditFee, Premiums.TechnicalAuditFee, $"TechnicalAuditFee Services Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UITechnicalAuditVATFee, Premiums.TechnicalAuditVATFee, $"TechnicalAuditVATFee Services Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UITechnicalAuditTotalFee, TechnicalAuditTotalValue, $"TechnicalAuditTotalValue Services Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                }
                else
                {
                    Assert.AreEqual(UITechnicalAuditFee, Premiums.TechnicalAuditFee, $"TechnicalAuditFee Services Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UITechnicalAuditVATFee, Premiums.TechnicalAuditVATFee, $"TechnicalAuditVATFee Services Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UITechnicalAuditTotalFee, TechnicalAuditTotalValue, $"TechnicalAuditTotalValue Services Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");

                }
            }


            //Administrator Fee values    
            if (Premiums.ProductServices.Any(x => x.Contains("Administration Fee")))
            {

                var uiAdministrationFee = $"//th[@class='product-title'][text()='{productName}']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[text()='Administration Fee']//following-sibling::td[@class='right-align'][1]";
                var AdministrationFee = Driver.FindElement(By.XPath(uiAdministrationFee)).Text.Replace("£", "");
                double UIAdministrationFee = Math.Round(Convert.ToDouble(AdministrationFee), 2);
                double AdministrationFeeDiff;

                var uiAdministrationVATFee = $"//th[@class='product-title'][text()='{productName}']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[text()='Administration Fee']//following-sibling::td[@class='right-align'][2]";
                var AdministrationVATFee = Driver.FindElement(By.XPath(uiAdministrationVATFee)).Text.Replace("£", "");
                double UIAdministrationVATFee = Math.Round(Convert.ToDouble(AdministrationVATFee), 2);
                double AdministrationVATDiff;

                var uiAdministrationTotalFee = $"//th[@class='product-title'][text()='{productName}']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[text()='Administration Fee']//following-sibling::th[@class='right-align'][1]";
                var AdministrationTotalFee = Driver.FindElement(By.XPath(uiAdministrationTotalFee)).Text.Replace("£", "");
                double UIAdministrationTotalFee = Math.Round(Convert.ToDouble(AdministrationTotalFee), 2);
                double AdministrationTotalValue = Math.Round(Convert.ToDouble(UIAdministrationFee + UIAdministrationVATFee), 2);
                double AdministrationTotalDiff;

                if (UIAdministrationFee != Premiums.AdministrationFee || UIAdministrationVATFee != Premiums.AdministrationVATFee || UIAdministrationTotalFee != AdministrationTotalValue)
                {
                    AdministrationFeeDiff = Convert.ToDouble(Premiums.AdministrationFee - UIAdministrationFee);
                    AdministrationVATDiff = Convert.ToDouble(Premiums.AdministrationVATFee - UIAdministrationVATFee);
                    AdministrationTotalDiff = Convert.ToDouble(AdministrationTotalValue - UIAdministrationTotalFee);
                    if (AdministrationFeeDiff >= -1.01 && AdministrationFeeDiff <= 1.01)
                    {
                        Premiums.AdministrationFee = UIAdministrationFee;
                    }

                    if (AdministrationVATDiff >= -1.01 && AdministrationVATDiff <= 1.01)
                    {
                        Premiums.AdministrationVATFee = UIAdministrationVATFee;
                    }
                    if (AdministrationTotalDiff >= 1.01 && AdministrationTotalDiff <= 1.01)
                    {
                        AdministrationTotalValue = UIAdministrationTotalFee;
                    }

                    Assert.AreEqual(UIAdministrationFee, Premiums.AdministrationFee, $"AdministrationFee Services Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UIAdministrationVATFee, Premiums.AdministrationVATFee, $"AdministrationVATFee Services Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UIAdministrationTotalFee, AdministrationTotalValue, $"AdministrationTotalValue Services Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                }
                else
                {
                    Assert.AreEqual(UIAdministrationFee, Premiums.AdministrationFee, $"AdministrationFee Services Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UIAdministrationVATFee, Premiums.AdministrationVATFee, $"AdministrationVATFee Services Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UIAdministrationTotalFee, AdministrationTotalValue, $"AdministrationTotalValue Services Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                }
            }
            //Cancellation Fee values  
            if (Premiums.ProductServices.Any(x => x.Contains("Cancellation Fee")))
            {


                var uiCancellationFee = $"//th[@class='product-title'][text()='{productName}']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[text()='Cancellation Fee']//following-sibling::td[@class='right-align'][1]";
                var CancellationFee = Driver.FindElement(By.XPath(uiCancellationFee)).Text.Replace("£", "");
                double UICancellationFee = Math.Round(Convert.ToDouble(CancellationFee), 2);
                double CancellationFeeDiff;

                var uiCancellationVATFee = $"//th[@class='product-title'][text()='{productName}']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[text()='Cancellation Fee']//following-sibling::td[@class='right-align'][2]";
                var CancellationVATFee = Driver.FindElement(By.XPath(uiCancellationVATFee)).Text.Replace("£", "");
                double UICancellationVATFee = Math.Round(Convert.ToDouble(CancellationVATFee), 2);
                double CancellationVATDiff;

                var uiCancellationTotalFee = $"//th[@class='product-title'][text()='{productName}']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[text()='Cancellation Fee']//following-sibling::td[@class='right-align'][1]";
                var CancellationTotalFee = Driver.FindElement(By.XPath(uiCancellationTotalFee)).Text.Replace("£", "");
                double UICancellationTotalFee = Math.Round(Convert.ToDouble(CancellationTotalFee), 2);
                double CancellationTotalValue = Math.Round(Convert.ToDouble(UICancellationFee + UICancellationVATFee), 2);
                double CancellationTotalDiff;


                if (UICancellationFee != Premiums.CancellationFee || UICancellationVATFee != Premiums.CancellationVATFee || UICancellationTotalFee != CancellationTotalValue)
                {
                    CancellationFeeDiff = Convert.ToDouble(Premiums.CancellationFee - UICancellationFee);
                    CancellationVATDiff = Convert.ToDouble(Premiums.CancellationVATFee - UICancellationVATFee);
                    CancellationTotalDiff = Convert.ToDouble(CancellationTotalValue - UICancellationTotalFee);
                    if (CancellationFeeDiff >= -1.01 && CancellationFeeDiff <= 1.01)
                    {
                        Premiums.CancellationFee = UICancellationFee;
                    }

                    if (CancellationVATDiff >= -1.01 && CancellationVATDiff <= 1.01)
                    {
                        Premiums.CancellationVATFee = UICancellationVATFee;
                    }
                    if (CancellationTotalDiff >= -1.01 && CancellationTotalDiff <= 1.01)
                    {
                        CancellationTotalValue = UICancellationTotalFee;
                    }

                    Assert.AreEqual(UICancellationFee, Premiums.CancellationFee, $"CancellationFee Services Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UICancellationVATFee, Premiums.CancellationVATFee, $"CancellationVATFee Services Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UICancellationTotalFee, CancellationTotalValue, $"CancellationTotalValue Services Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                }
                else
                {
                    Assert.AreEqual(UICancellationFee, Premiums.CancellationFee, $"CancellationFee Services Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UICancellationVATFee, Premiums.CancellationVATFee, $"CancellationVATFee Services Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UICancellationTotalFee, CancellationTotalValue, $"CancellationTotalValue Services Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                }
            }

        }
      
    }
}
