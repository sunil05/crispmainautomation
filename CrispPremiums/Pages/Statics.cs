﻿using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RegressionPacks.Pages
{
    public static class Statics
    {
        public static string DownloadFolder { get; set; }
        public static ExcelPackage Excel { get; set; }
        public static int CurrentExcelRow { get; set; }
        public static List<string> Plots { get; set; }
        public static List<string> ProductNameList { get; set; }
        public static List<string> ConstructionType { get; set; }
        public static List<string> BCProducts { get; set; }
        public static List<string> UnitTypes { get; set; }
        public static List<string> StageOfWorks { get; set; }
        public static string quoteId { get; set; }
        public static string QuoteRef { get; set; }
        public static string OrderRef { get; set; }
        public static string OrderId { get; set; }
        public static string ProductName { get; set; }
        public static string RoleName { get; set; }
        public static string EmpName { get; set; }
        public static string ContactName { get; set; }
    }

    public static class Premiums
    {
        //Multiple Plots data from plotdata sheet
        public static List<plotrows> eachplotrow { get; set; }
        //public static List<string> PlotRow { get; set; }
        public static string ProductVersionDate { get; set; }
        public static string Plots { get; set; }
        public static string BCPlots { get; set; }
        public static List<string> ProductNames { get; set; }
        public static List<string> BCProducts { get; set; }
        public static List<string> ConstructionTypes { get; set; }
        public static List<string> UnitTypes { get; set; }
        public static List<string> StageOfWorks { get; set; }
        public static List<double> ReconstructionCosts { get; set; }
        public static List<double> EstimatedSalePrices { get; set; }

        //Reading Single Plot data from plotdata sheet
        public static string PlotData { get; set; }
        public static string ProductName { get; set; }
        public static string BCProduct { get; set; }
        public static string ConstructionType { get; set; }
        public static string UnitType { get; set; }
        public static string StageOfWork { get; set; }
        public static double ReconstructionCost { get; set; }
        public static double EstimatedSalePrice { get; set; }
        public static string SqMeter { get; set; }     
        public static double CoverLengthYears { get; set; }
        public static string Rating { get; set; }
        public static string BuilderExperiance { get; set; }

        //Product Cover Fee Values After Caluclations 
        public static double StructuralFee { get; set; }
        public static double SecondaryLayerPremium { get; set; }
        public static double StructuralIPTFee { get; set; }
        public static double ContaminatedLandFee { get; set; }
        public static double ContaminatedLandIPTFee { get; set; }
        public static double ApprovedInspectorBuildingControlFee { get; set; }
        public static double ApprovedInspectorBuildingControlIPTFee { get; set; }
        public static double LocalAuthorityBuildingControlFee { get; set; }
        public static double LocalAuthorityBuildingControlIPTFee { get; set; }
        public static double SeepageFee { get; set; }
        public static double SeepageIPTFee { get; set; }
        public static double WaiversOfSubrogationBuilderFee { get; set; }
        public static double WaiversOfSubrogationBuilderIPTFee { get; set; }
        public static double WaiversOfSubrogationArchitectFee { get; set; }
        public static double WaiversOfSubrogationArchitectIPTFee { get; set; }
        public static double LossOfGrossProfitFee { get; set; }
        public static double LossOfGrossProfitIPTFee { get; set; }
        public static double LossOfRentPayableFee { get; set; }
        public static double LossOfRentPayableIPTFee { get; set; }
        public static double LossOfRentReceivableFee { get; set; }
        public static double LossOfRentReceivableIPTFee { get; set; }
        public static double WaiversOfSubrogationEngineerFee { get; set; }
        public static double WaiversOfSubrogationEngineerIPTFee { get; set; }
        public static double InsolvencyFee { get; set; }
        public static double InsolvencyIPTFee { get; set; }
        public static double DefectsFee { get; set; }
        public static double DefectsIPTFee { get; set; }

        //Services Fee Values 
        public static double TechnicalAuditFee { get; set; }
        public static double TechnicalAuditVATFee { get; set; }
        public static double ConsumerCodeFee { get; set; }
        public static double ConsumerCodeVATFee { get; set; }
        public static double AdministrationFee { get; set; }
        public static double AdministrationVATFee { get; set; }
        public static double CancellationFee { get; set; }
        public static double CancellationVATFee { get; set; }
        public static double RefurbishmentAssessmentFee { get; set; }
        public static double RefurbishmentAssessmentVATFee { get; set; }

    


        //BC Product Fee Values  
        public static double BCTechnicalFee { get; set; }
        public static double BCTechnicalVATFee { get; set; }
        public static double BCAdministrationFee { get; set; }
        public static double BCAdministrationVATFee { get; set; }
        public static double BCCancellationFee { get; set; }
        public static double BCCancellationVATFee { get; set; }

        //Covers For Commercials Product
        public static List<string> ProductsCovers { get; set; }
        public static string ContaminatedLandCover { get; set; }
        public static string LocalAuthorityBuildingControlFunctionCover{get; set;}
        public static string SeepageCover { get; set; }
        public static string WaiversOfSubrogationRightsBuilderCover { get; set; }
        public static string LossOfGrossProfitCover { get; set; }
        public static string LossOfRentPayableCover { get; set; }
        public static string LossOfRentReceivableCover { get; set; }
        public static string WaiversOfSubrogationRightsEngineerCover { get; set; }
        public static string WaiversOfSubrogationRightsArchitectCover { get; set; }
        //Services For Commercials Product
        public static List<string> ProductServices { get; set; }
        public static string ProductTechnicalAuditFeeService { get; set; }
        public static string ProductAdminFeeService { get; set; }
        public static string ProductCancellationFeeService { get; set; }
        public static double LossOfGrossProfitInput { get; set; }
        public static double LossOfRentReceivableInput { get; set; }
        public static double LossOfRentPayableInput { get; set; }
        public static int LossOfGrossProfitNumberOfYearsInput { get; set; }
        public static int LossOfRentReceivableNumberOfYearsInput { get; set; }
        public static int LossOfRentPayableNumberOfYearsInput { get; set; }


        //Covers on SelfBuild Product 
        public static string ArchitectInvolvement { get; set; }
        public static string ArchitectInvolvementLoading { get; set; }

        //Covers For Newhomes Products
        public static string InsolvencyCover { get; set; }    
        public static string DefectsCover { get; set; }

        //services For NewHomes Product 
        public static string ProductConsumerCodeService { get; set; }

        public static string ProductRefurbishmentAssessmentService { get; set; }

        //Covers For Social Housing  Products

        public static string LossOfRentCover { get; set; }
        public static double LossOfRentFee { get; set; }
        public static double LossOfRentIPTFee { get; set; }
        public static double ContractValue { get; set; }
        public static string PlasteringCover { get; set; }
        public static double PlasteringFee { get; set; }
        public static double PlasteringIPTFee { get; set; }
        public static string SoundTransmissionCover { get; set; }
        public static double SoundTransmissionFee { get; set; }
        public static double SoundTransmissionIPTFee { get; set; }

        //HVS Products 
        public static string ProductAtkinsFeeService { get; set; }
        public static double AtkinsFee { get; set; }
        public static double AtkinsVATFee { get; set; }
        public static int StoreyLoadingInput { get; set; }
        public static int BasementLoadingInput { get; set; }
        public static string StoreyLoading { get; set; }
        public static string BasementLoading { get; set; }
        public static string HasCurtainWalling { get; set; }
        public static string MultiProjectLoading { get; set; }
        public static double DiscretionaryDiscountInput { get; set; }
        public static double StageOfWorksLoadingPercentageInput { get; set; }
        public static string MultiProjectDiscountInput { get; set; }

    }

    public class plotrows
    {
        public string Plots { get; set; }
        public string ProductName { get; set; }
        public string BCProduct { get; set; }
        public string ConstructionType { get; set; }
        public string UnitType { get; set; }
        public string StageOfWork { get; set; }
        public double ReconstructionCostValue { get; set; }
        public double EstrimatedSalesPrice { get; set; }
        public double  SqMValue { get; set; }
    }
}
