﻿using ExcelDataReader;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.PageObjects;
using RegressionPacks.Support;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RegressionPacks.Pages
{
    public class CommercialBCPremiums : Support.Pages
    {
        public IWebDriver wdriver;
        double vatCaluclationValue = 0.20;
        double BCTAFee = 0;
        double BCAdminFee = 0;
        DataTable excelTable;
        double getBanding;
        double Banding;
        int RatingColumn;
        double MinimumBCTAFee = 0;
        public CommercialBCPremiums(ISearchContext driver)
        {
            PageFactory.InitElements(driver, this);
            wdriver = (RemoteWebDriver)driver;
        }
        [FindsBy(How = How.XPath, Using = "//crisp-wizard-step[@step-title='Fees']//div//table//thead//tr//th[@class='product-title'][contains(text(),'Building Control')]//parent::tr//following-sibling::tr[1]//td//crisp-display-value[@value.bind='product.unitCount']//span")]
        public IWebElement BCPlotsCount;

        [FindsBy(How = How.XPath, Using = "//crisp-wizard-step[@step-title='Fees']//div//table//thead//tr//th[@class='product-title'][contains(text(),'Building Control')]")]
        public IList<IWebElement> BCProduct;
        public void CoversOnCommercialBCProduct()
        {

            if (Premiums.eachplotrow.Any(o => o.BCProduct.Contains("Commercial Building Control")))
            {

                Premiums.ProductServices = PremiumCovers.ProductServicesList.Select(i => i.Text.ToString()).ToList();               
                //Products Services 
                if (Premiums.ProductServices.Any(x => x.Contains("Technical Fee")))
                {
                    Premiums.ProductTechnicalAuditFeeService = PremiumCovers.ProductTechnicalAuditFee.Count > 0 ? "Yes" : "No";
                }
                if (Premiums.ProductServices.Any(x => x.Contains("Administration Fee")))
                {
                    Premiums.ProductAdminFeeService = PremiumCovers.ProductAdministrationFee.Count > 0 ? "Yes" : "No";
                }
                if (Premiums.ProductServices.Any(x => x.Contains("Cancellation Fee")))
                {
                    Premiums.ProductCancellationFeeService = PremiumCovers.ProductCancellationFee.Count > 0 ? "Yes" : "No";
                }
            }
        }
        //CommercialBC Premium Caluclation On It's Own
        public void CommercialBCPremiumCalcs()
        {
            Premiums.BCTechnicalFee = 0;
            Premiums.BCAdministrationFee = 0;
            Premiums.BCCancellationFee = 0;

            if (BCProduct.Count > 0)
            {
                var aggregatedPremium = new CommercialBCPremium();

                foreach (var plot in Premiums.eachplotrow)
                {
                    //Actual Premiums Caluclaions for Each Plot Row 
                    if(plot.BCProduct == "Commercial Building Control")
                    {
                        var calculatedPremiums = CommercialBCPremiumCaluclationsForEachPlotRow(plot);
                        Premiums.BCTechnicalFee += calculatedPremiums.bcTechnicalFee;
                        Premiums.BCAdministrationFee = calculatedPremiums.bcAdministrationFee;
                        Premiums.BCCancellationFee = calculatedPremiums.bcCancellationFee;
                    }                    
                }
                //VAT Calucaltions 
                Premiums.BCTechnicalVATFee = Math.Round(Convert.ToDouble(Premiums.BCTechnicalFee * vatCaluclationValue), 2);
                Premiums.BCAdministrationVATFee = Convert.ToDouble(0.00);
                Premiums.BCCancellationVATFee = Convert.ToDouble(0.00);
                VerfiyCommercialBCUIValues();

            }
        }
   
        public class CommercialBCPremium
        {
            public double bcTechnicalFee;

            public double bcAdministrationFee;

            public double bcCancellationFee;

        }
        

        public CommercialBCPremium CommercialBCPremiumCaluclationsForEachPlotRow(plotrows plot)
        {
            var calculatedPremiums = new CommercialBCPremium();

            var fileInfo = new FileInfo(ExtensionMethods.GetAbsolutePath($@"TestData\Premiums\ProductPricingSetup\Product Pricing Setup - Building Control.xlsx"));
            //Filter Premium Data for Commercials 
            string premiumsdata = fileInfo.ToString();
            excelTable = ReadCommercialBCMasterPremiumsData(premiumsdata);
            //Caluclate Premium Data for Completed Housing 
            //Structural Premium Values Based On Reconstruction Cost

            BCTAFeeCaluclations(plot);
            calculatedPremiums.bcTechnicalFee = Convert.ToDouble(BCTAFee);
            calculatedPremiums.bcAdministrationFee = Convert.ToDouble(0.00);
            calculatedPremiums.bcCancellationFee = Convert.ToDouble(0.00);
            return calculatedPremiums;
        }
        public void BCTAFeeCaluclations(plotrows plot)
        {
            CoversOnCommercialBCProduct();

            DateTime date1 = new DateTime(2019, 11, 10);
            DateTime date2 = new DateTime(2019, 11, 28);
            TimeSpan tt = date2 - date1;
            int totalWeeks = tt.Days / 7;
            //Techinical Audit Fee Table
            //int Units = Convert.ToInt32(Premiums.BCPlots);
            int BuildPeriod = totalWeeks; 
            var technicalFeeTable = -1;
            var technicalFeeRow = -1;
            double technicalAuditFeePerUnit = 0;

            for (int i = 0; i < excelTable.Rows.Count; i++)
            {
                if (excelTable.Rows[i][1].ToString().Contains("Build Period (Weeks)"))
                {
                    technicalFeeTable = i;
                    break;
                }
            }
            if (technicalFeeTable == -1)
                throw new Exception("Technical Fee Table Not Found");

            for (int i = technicalFeeTable; i < excelTable.Rows.Count; i++)
            {
                if (excelTable.Rows[i][1].ToString().Contains(BuildPeriod.ToString()))
                {
                    technicalFeeRow = i;
                    break;
                }
            }
            if (technicalFeeRow == -1)
                throw new Exception("Technical Fee Row Not Found");
            technicalAuditFeePerUnit = Convert.ToDouble(excelTable.Rows[technicalFeeRow][3]);
            int plotCount = Convert.ToInt32(Premiums.BCPlots);
            //Technical Audit Fee
            BCTAFee = Convert.ToDouble(technicalAuditFeePerUnit/plotCount);
            BCTaFeeBasedOnUnitType(plot);
        }
        public void BCTaFeeBasedOnUnitType(plotrows plot)
        {
            
            var taFeeUnitTypeTable = -1;
            var taFeeUnitTypeRow = -1;
            for (int i = 0; i < excelTable.Rows.Count; i++)
            {
                if (excelTable.Rows[i][0].ToString().Contains("Unit Type Loading"))
                {
                    taFeeUnitTypeTable = i;
                    break;
                }
            }
            if (taFeeUnitTypeTable == -1)
                throw new Exception("Technical Fee Unit Type Table Not Found");
            if (plot.UnitType.Contains("Shopping Centre"))
            {
                string tempUnitType = "Other";
                for (int i = taFeeUnitTypeTable; i < excelTable.Rows.Count; i++)
                {
                    if (excelTable.Rows[i][1].ToString().Contains(tempUnitType))
                    {
                        taFeeUnitTypeRow = i;
                        break;
                    }
                }
                if (taFeeUnitTypeRow == -1)
                    throw new Exception("Technical Fee Unit Type Row Not Found");
            }
            else
            {
                for (int i = taFeeUnitTypeTable; i < excelTable.Rows.Count; i++)
                {
                    if (excelTable.Rows[i][1].ToString().Contains(plot.UnitType))
                    {
                        taFeeUnitTypeRow = i;
                        break;
                    }
                }
                if (taFeeUnitTypeRow == -1)
                    throw new Exception("Technical Fee Unit Type Row Not Found");
            }

                        
          
            double taFeeBasedOnUnitType = Convert.ToDouble(excelTable.Rows[taFeeUnitTypeRow][2]);
            BCTAFee = Convert.ToDouble(BCTAFee * taFeeBasedOnUnitType);
        }


        public DataTable ReadCommercialBCMasterPremiumsData(string fileName)
        {

            var result = new DataSet();                                                                             //Set the First Row as Column Name
            using (var stream = File.Open(fileName, FileMode.Open, FileAccess.Read))
            using (var excelReader = ExcelReaderFactory.CreateOpenXmlReader(stream)) //.xlsx
            {
                //Return as DataSet
                result = excelReader.AsDataSet(new ExcelDataSetConfiguration()
                {
                    ConfigureDataTable = (_) => new ExcelDataTableConfiguration()
                    {
                        UseHeaderRow = true
                    }
                });
            }
            //Get all the Tables
            DataTableCollection table = result.Tables;
            DataTable resultTable = table["BC - Commercial (1)"];
            return resultTable;
        }

       


        public void VerfiyCommercialBCUIValues()
        {
            var productName = Premiums.eachplotrow.FirstOrDefault().BCProduct;
           

            //Services Elements  and  Fee values  
            //Technical Audit Fee values  
            if (Premiums.ProductServices.Any(x => x.Contains("Technical Fee")))
            {


                var uiTechnicalFee = $"//th[@class='product-title'][text()='{productName}']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[text()='Technical Fee']//following-sibling::td[@class='right-align'][1]";
                var TechnicalFee = Driver.FindElement(By.XPath(uiTechnicalFee)).Text.Replace("£", "");
                double UITechnicalFee = Math.Round(Convert.ToDouble(TechnicalFee), 2);
                double TechnicalFeeDiff;

                var uiTechnicalVATFee = $"//th[@class='product-title'][text()='{productName}']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[text()='Technical Fee']//following-sibling::td[@class='right-align'][2]";
                var TechnicalVATFee = Driver.FindElement(By.XPath(uiTechnicalVATFee)).Text.Replace("£", "");
                double UITechnicalVATFee = Math.Round(Convert.ToDouble(TechnicalVATFee), 2);
                double TechnicalVATDiff;

                var uiTechnicalTotalFee = $"//th[@class='product-title'][text()='{productName}']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[text()='Technical Fee']//following-sibling::th[@class='right-align'][1]";
                var TechnicalTotalFee = Driver.FindElement(By.XPath(uiTechnicalTotalFee)).Text.Replace("£", "");
                double UITechnicalTotalFee = Math.Round(Convert.ToDouble(TechnicalTotalFee), 2);
                double TechnicalTotalValue = Math.Round(Convert.ToDouble(UITechnicalFee + UITechnicalVATFee), 2);
                double TechnicalTotalDiff;

                if (UITechnicalFee != Premiums.BCTechnicalFee || UITechnicalVATFee != Premiums.BCTechnicalVATFee || UITechnicalTotalFee != TechnicalTotalValue)
                {
                    TechnicalFeeDiff = Convert.ToDouble(Premiums.BCTechnicalFee - UITechnicalFee);
                    TechnicalVATDiff = Convert.ToDouble(Premiums.BCTechnicalVATFee - UITechnicalVATFee);
                    TechnicalTotalDiff = Convert.ToDouble(TechnicalTotalValue - UITechnicalTotalFee);
                    if (TechnicalFeeDiff >= -1.01 && TechnicalFeeDiff <= 1.01)
                    {
                        Premiums.BCTechnicalFee = UITechnicalFee;
                    }

                    if (TechnicalVATDiff >= -1.01 && TechnicalVATDiff <= 1.01)
                    {
                        Premiums.BCTechnicalVATFee = UITechnicalVATFee;
                    }
                    if (TechnicalTotalDiff >= 1.01 && TechnicalTotalDiff <= 1.01)
                    {
                        TechnicalTotalValue = UITechnicalTotalFee;
                    }

                    Assert.AreEqual(UITechnicalFee, Premiums.BCTechnicalFee, $"TechnicalFee Services Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UITechnicalVATFee, Premiums.BCTechnicalVATFee, $"TechnicalVATFee Services Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UITechnicalTotalFee, TechnicalTotalValue, $"TechnicalTotalValue Services Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                }
                else
                {
                    Assert.AreEqual(UITechnicalFee, Premiums.BCTechnicalFee, $"TechnicalFee Services Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UITechnicalVATFee, Premiums.BCTechnicalVATFee, $"TechnicalVATFee Services Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UITechnicalTotalFee, TechnicalTotalValue, $"TechnicalTotalValue Services Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");

                }
            }


            //Administrator Fee values    
            if (Premiums.ProductServices.Any(x => x.Contains("Administration Fee")))
            {

                var uiBCAdministrationFee = $"//th[@class='product-title'][text()='{productName}']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[text()='Administration Fee']//following-sibling::td[@class='right-align'][1]";
                var BCAdministrationFee = Driver.FindElement(By.XPath(uiBCAdministrationFee)).Text.Replace("£", "");
                double UIBCAdministrationFee = Math.Round(Convert.ToDouble(BCAdministrationFee), 2);
                double BCAdministrationFeeDiff;

                var uiBCAdministrationVATFee = $"//th[@class='product-title'][text()='{productName}']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[text()='Administration Fee']//following-sibling::td[@class='right-align'][2]";
                var BCAdministrationVATFee = Driver.FindElement(By.XPath(uiBCAdministrationVATFee)).Text.Replace("£", "");
                double UIBCAdministrationVATFee = Math.Round(Convert.ToDouble(BCAdministrationVATFee), 2);
                double BCAdministrationVATDiff;

                var uiBCAdministrationTotalFee = $"//th[@class='product-title'][text()='{productName}']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[text()='Administration Fee']//following-sibling::th[@class='right-align'][1]";
                var BCAdministrationTotalFee = Driver.FindElement(By.XPath(uiBCAdministrationTotalFee)).Text.Replace("£", "");
                double UIBCAdministrationTotalFee = Math.Round(Convert.ToDouble(BCAdministrationTotalFee), 2);
                double BCAdministrationTotalValue = Math.Round(Convert.ToDouble(UIBCAdministrationFee + UIBCAdministrationVATFee), 2);
                double BCAdministrationTotalDiff;

                if (UIBCAdministrationFee != Premiums.BCAdministrationFee || UIBCAdministrationVATFee != Premiums.BCAdministrationVATFee || UIBCAdministrationTotalFee != BCAdministrationTotalValue)
                {
                    BCAdministrationFeeDiff = Convert.ToDouble(Premiums.BCAdministrationFee - UIBCAdministrationFee);
                    BCAdministrationVATDiff = Convert.ToDouble(Premiums.BCAdministrationVATFee - UIBCAdministrationVATFee);
                    BCAdministrationTotalDiff = Convert.ToDouble(BCAdministrationTotalValue - UIBCAdministrationTotalFee);
                    if (BCAdministrationFeeDiff >= -1.01 && BCAdministrationFeeDiff <= 1.01)
                    {
                        Premiums.BCAdministrationFee = UIBCAdministrationFee;
                    }

                    if (BCAdministrationVATDiff >= -1.01 && BCAdministrationVATDiff <= 1.01)
                    {
                        Premiums.BCAdministrationVATFee = UIBCAdministrationVATFee;
                    }
                    if (BCAdministrationTotalDiff >= -1.01 && BCAdministrationTotalDiff <= 1.01)
                    {
                        BCAdministrationTotalValue = UIBCAdministrationTotalFee;
                    }

                    Assert.AreEqual(UIBCAdministrationFee, Premiums.BCAdministrationFee, $"BCAdministrationFee Services Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UIBCAdministrationVATFee, Premiums.BCAdministrationVATFee, $"BCAdministrationVATFee Services Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UIBCAdministrationTotalFee, BCAdministrationTotalValue, $"BCAdministrationTotalValue Services Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                }
                else
                {
                    Assert.AreEqual(UIBCAdministrationFee, Premiums.BCAdministrationFee, $"BCAdministrationFee Services Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UIBCAdministrationVATFee, Premiums.BCAdministrationVATFee, $"BCAdministrationVATFee Services Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UIBCAdministrationTotalFee, BCAdministrationTotalValue, $"BCAdministrationTotalValue Services Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                }
            }
            //Cancellation Fee values  
            if (Premiums.ProductServices.Any(x => x.Contains("Cancellation Fee")))
            {


                var uiBCCancellationFee = $"//th[@class='product-title'][text()='{productName}']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[text()='Cancellation Fee']//following-sibling::td[@class='right-align'][2]";
                var BCCancellationFee = Driver.FindElement(By.XPath(uiBCCancellationFee)).Text.Replace("£", "");
                double UIBCCancellationFee = Math.Round(Convert.ToDouble(BCCancellationFee), 2);
                double BCCancellationFeeDiff;

                var uiBCCancellationVATFee = $"//th[@class='product-title'][text()='{productName}']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[text()='Cancellation Fee']//following-sibling::td[@class='right-align'][2]";
                var BCCancellationVATFee = Driver.FindElement(By.XPath(uiBCCancellationVATFee)).Text.Replace("£", "");
                double UIBCCancellationVATFee = Math.Round(Convert.ToDouble(BCCancellationVATFee), 2);
                double BCCancellationVATDiff;

                var uiBCCancellationTotalFee = $"//th[@class='product-title'][text()='{productName}']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[text()='Cancellation Fee']//following-sibling::td[@class='right-align'][2]";
                var BCCancellationTotalFee = Driver.FindElement(By.XPath(uiBCCancellationTotalFee)).Text.Replace("£", "");
                double UIBCCancellationTotalFee = Math.Round(Convert.ToDouble(BCCancellationTotalFee), 2);
                double BCCancellationTotalValue = Math.Round(Convert.ToDouble(UIBCCancellationFee + UIBCCancellationVATFee), 2);
                double BCCancellationTotalDiff;


                if (UIBCCancellationFee != Premiums.BCCancellationFee || UIBCCancellationVATFee != Premiums.BCCancellationVATFee || UIBCCancellationTotalFee != BCCancellationTotalValue)
                {
                    BCCancellationFeeDiff = Convert.ToDouble(Premiums.BCCancellationFee - UIBCCancellationFee);
                    BCCancellationVATDiff = Convert.ToDouble(Premiums.BCCancellationVATFee - UIBCCancellationVATFee);
                    BCCancellationTotalDiff = Convert.ToDouble(BCCancellationTotalValue - UIBCCancellationTotalFee);
                    if (BCCancellationFeeDiff >= -1.01 && BCCancellationFeeDiff <= 1.01)
                    {
                        Premiums.BCCancellationFee = UIBCCancellationFee;
                    }

                    if (BCCancellationVATDiff >= -1.01 && BCCancellationVATDiff <= 1.01)
                    {
                        Premiums.BCCancellationVATFee = UIBCCancellationVATFee;
                    }
                    if (BCCancellationTotalDiff >= -1.01 && BCCancellationTotalDiff <= 1.01)
                    {
                        BCCancellationTotalValue = UIBCCancellationTotalFee;
                    }

                    Assert.AreEqual(UIBCCancellationFee, Premiums.BCCancellationFee, $"BCCancellationFee Services Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UIBCCancellationVATFee, Premiums.BCCancellationVATFee, $"BCCancellationVATFee Services Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UIBCCancellationTotalFee, BCCancellationTotalValue, $"BCCancellationTotalValue Services Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                }
                else
                {
                    Assert.AreEqual(UIBCCancellationFee, Premiums.BCCancellationFee, $"BCCancellationFee Services Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UIBCCancellationVATFee, Premiums.BCCancellationVATFee, $"BCCancellationVATFee Services Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UIBCCancellationTotalFee, BCCancellationTotalValue, $"BCCancellationTotalValue Services Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                }
            }

        }


    }
}
