﻿using ExcelDataReader;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.PageObjects;
using RegressionPacks.Support;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RegressionPacks.Pages
{
    public class ChannelIslandsCommercialPremiums : Support.Pages
    {
        public IWebDriver wdriver;
        DataTable excelTable;
        double BasePremium = 0;
        double SeepageFee = 0;
        double WaviersOfSubrogationRightsBuilderFee = 0;
        double WaviersOfSubrogationRightsEngineerFee = 0;
        double TAFee = 0;
        double RefurbishmentFee = 0;
        double MinimumTAFee = 0;
        double iptCaluclationValue = 0.00;
       
        int RatingColumn;
        public ChannelIslandsCommercialPremiums(ISearchContext driver)
        {
            PageFactory.InitElements(driver, this);
            wdriver = (RemoteWebDriver)driver;
        }
        

        public void CoversOnChannelIslandsCommercialProduct()
        {
            if (Premiums.eachplotrow.Any(o => o.ProductName.Contains("Channel Islands Commercial")))
            {
                Premiums.ProductsCovers = PremiumCovers.ProductCoversList.Select(i => i.Text.ToString()).ToList();
                Premiums.ProductServices = PremiumCovers.ProductServicesList.Select(i => i.Text.ToString()).ToList();

                if (Premiums.ProductsCovers.Any(x => x.Contains("Seepage")))
                {
                    Premiums.SeepageCover = PremiumCovers.SeeapageCover.Count > 0 ? "Yes" : "No";
                }
                if (Premiums.ProductsCovers.Any(x => x.Contains("Waivers of Subrogation Rights - Builder")))
                {
                    Premiums.WaiversOfSubrogationRightsBuilderCover = PremiumCovers.WaiversBuilderCover.Count > 0 ? "Yes" : "No";
                }

                if (Premiums.ProductsCovers.Any(x => x.Contains("Waivers of Subrogation Rights - Engineer")))
                {
                    Premiums.WaiversOfSubrogationRightsEngineerCover = PremiumCovers.WaiversEngineerCover.Count > 0 ? "Yes" : "No";
                }
                if (Premiums.ProductServices.Any(x => x.Contains("Technical Audit Fee")))
                {
                    Premiums.ProductTechnicalAuditFeeService = PremiumCovers.ProductTechnicalAuditFee.Count > 0 ? "Yes" : "No";
                }
                if (Premiums.ProductServices.Any(x => x.Contains("Refurbishment Assessment Fee")))
                {
                    Premiums.ProductRefurbishmentAssessmentService = PremiumCovers.ProductRefurbishmentAssessmentFee.Count > 0 ? "Yes" : "No";
                }
                if (Premiums.ProductServices.Any(x => x.Contains("Administration Fee")))
                {
                    Premiums.ProductAdminFeeService = PremiumCovers.ProductAdministrationFee.Count > 0 ? "Yes" : "No";
                }
                if (Premiums.ProductServices.Any(x => x.Contains("Cancellation Fee")))
                {
                    Premiums.ProductCancellationFeeService = PremiumCovers.ProductCancellationFee.Count > 0 ? "Yes" : "No";
                }
            }
        }

        public void ChannelIslandsCommercialPremiumsCaluclations()
        {
            Premiums.StructuralFee = 0;
            Premiums.SeepageFee = 0;
            Premiums.WaiversOfSubrogationBuilderFee = 0;
            Premiums.WaiversOfSubrogationEngineerFee = 0;
            Premiums.TechnicalAuditFee = 0;
            Premiums.AdministrationFee = 0;
            Premiums.RefurbishmentAssessmentFee = 0;
            Premiums.CancellationFee = 0;

            var aggregatedPremium = new ChannelIslandsCommercialPremium();

            foreach (var plot in Premiums.eachplotrow)
            {
                //Actual Premiums Caluclaions for Each Plot Row 
                var calculatedPremiums = ChannelIslandsCommercialCaluclationsForEachPlotRow(plot);
                //Cover Premiums              
                Premiums.StructuralFee += calculatedPremiums.structuralFee;
                Premiums.SeepageFee += calculatedPremiums.seepageFee;
                Premiums.WaiversOfSubrogationBuilderFee += calculatedPremiums.waviersOfSubrogationRightsBuilderFee;
                Premiums.WaiversOfSubrogationEngineerFee += calculatedPremiums.waviersOfSubrogationRightsEngineerFee;

                //Services 
                Premiums.TechnicalAuditFee += calculatedPremiums.technicalFee;
                Premiums.RefurbishmentAssessmentFee += calculatedPremiums.refurbishmentAssessmentFee;
                Premiums.AdministrationFee += calculatedPremiums.administrationFee;
                Premiums.CancellationFee += calculatedPremiums.cancellationFee;
            }       
            //Caluclate Minimum TA Fee       
            MinimumTAFeeCaluclations();
            //Apply Minimum TAFee Value
            if (Premiums.TechnicalAuditFee <= MinimumTAFee)
            {
                Premiums.TechnicalAuditFee = Convert.ToDouble(MinimumTAFee);
            }

            //IPT Caluclations 

            Premiums.StructuralIPTFee = Math.Round(Convert.ToDouble(Premiums.StructuralFee * iptCaluclationValue), 2);
            Premiums.SeepageIPTFee = Convert.ToDouble(Premiums.SeepageFee * iptCaluclationValue);
            Premiums.WaiversOfSubrogationBuilderIPTFee = Convert.ToDouble(Premiums.WaiversOfSubrogationBuilderFee * iptCaluclationValue);
            Premiums.WaiversOfSubrogationEngineerIPTFee = Convert.ToDouble(Premiums.WaiversOfSubrogationEngineerFee * iptCaluclationValue);

            //VAT Caluclations
            Premiums.TechnicalAuditVATFee = Math.Round(0.00);
            Premiums.AdministrationVATFee = Math.Round(0.00);
            Premiums.RefurbishmentAssessmentVATFee = Math.Round(0.00);
            Premiums.CancellationVATFee = Math.Round(0.00);
            VerfiyChannelIslandsCommercialUIValues();
            if (Premiums.eachplotrow.Any(o => o.BCProduct.Contains("Commercial Building Control")))
            {
                CommercialBCPremiums.CommercialBCPremiumCalcs();
            }
        }
        public class ChannelIslandsCommercialPremium
        {
            public double structuralFee;
           
            public double seepageFee;

            public double waviersOfSubrogationRightsBuilderFee;          

            public double waviersOfSubrogationRightsEngineerFee;

            public double technicalFee;

            public double refurbishmentAssessmentFee;

            public double administrationFee;

            public double cancellationFee;

        }

        public ChannelIslandsCommercialPremium ChannelIslandsCommercialCaluclationsForEachPlotRow(plotrows plot)
        {
            var calculatedPremiums = new ChannelIslandsCommercialPremium();
            //Retrieve all the covers and Services from Fees Detail Page 
            CoversOnChannelIslandsCommercialProduct();
            //Reading Pricing Setup Book 
            var fileInfo = new FileInfo(ExtensionMethods.GetAbsolutePath($@"TestData\Premiums\ProductPricingSetup\Product Pricing Setup - Channel Islands.xlsx"));
            string premiumsdata = fileInfo.ToString();
            excelTable = ReadChannelIslandsCommercialMasterPremiumsPriceData(premiumsdata);
            //Structural Fee Caluclations Based on Banding           
            StuctralPremiumCaluclation(plot);

            calculatedPremiums.structuralFee = BasePremium;
            if (Premiums.ProductsCovers.Count > 0)
            {
                if (Premiums.SeepageCover == "Yes")
                {
                    SeepagePremiumCaluclations(plot);
                    calculatedPremiums.seepageFee = Convert.ToDouble(SeepageFee);
                }
                if (Premiums.WaiversOfSubrogationRightsBuilderCover == "Yes")
                {
                    WaiversOfSubrogationPremiumCaluclations(plot);
                    calculatedPremiums.waviersOfSubrogationRightsBuilderFee = Convert.ToDouble(WaviersOfSubrogationRightsBuilderFee);
                }
                if (Premiums.WaiversOfSubrogationRightsEngineerCover == "Yes")
                {
                    WaiversOfSubrogationPremiumCaluclations(plot);
                    calculatedPremiums.waviersOfSubrogationRightsEngineerFee = Convert.ToDouble(WaviersOfSubrogationRightsEngineerFee);
                }
            }

            //Product Service Fee Values 
            if (Premiums.ProductServices.Count > 0)
            {

                if (Premiums.ProductTechnicalAuditFeeService == "Yes")
                {
                    TAFeeCaluclations(plot);                   
                    calculatedPremiums.technicalFee = Math.Round((TAFee), 2);
                }
                if (Premiums.ProductRefurbishmentAssessmentService == "Yes")
                {                   
                    calculatedPremiums.refurbishmentAssessmentFee = Convert.ToDouble(0.00);
                }
                if (Premiums.ProductAdminFeeService == "Yes")
                {
                    calculatedPremiums.administrationFee = Convert.ToDouble(0.00);
                }
                if (Premiums.ProductCancellationFeeService == "Yes")
                {
                    calculatedPremiums.cancellationFee = Convert.ToDouble(0.00);
                }
            }
            return calculatedPremiums;
        }


        //Reading Strucural Premium Banding Table  and Caluclate Base Premium
        public void StuctralPremiumCaluclation(plotrows plot)
        {          
        
            //Caluclations Based on Reconstruction Cost 
            // Reading Structural Premium Table From CICommercial Pricing Setup Book
            double structuralPremium = Convert.ToDouble(excelTable.Rows[1][1]);
            BasePremium = Convert.ToDouble(structuralPremium * plot.ReconstructionCostValue);
            //Caluclations Based on Cover Loading 
            PeriodOfCoverLoadingCaluclation(plot);
            //Caluclate Premiums Based on Unit type 
            BuilderExperienceLoadingCaluclations(plot);
        }

        //Reading Period Of Cover Loading Table 
        public void PeriodOfCoverLoadingCaluclation(plotrows plot)
        {
            var periodOfCoverLoadingTableRow = -1;
            var periodOfCoverLengthRow = -1;
            //Get Unit Type Loading Table from Structural Premium Table
            for (int i = 0; i < excelTable.Rows.Count; i++)
            {
                if (excelTable.Rows[i][0].ToString().Contains("Period of Cover Loading"))
                {
                    periodOfCoverLoadingTableRow = i;
                    break;
                }
            }
            if (periodOfCoverLoadingTableRow == -1)
                throw new Exception("Unit Type Loading Table Not Found");

            //Get UnitType Row from Unit Type Loading Table
            for (int i = periodOfCoverLoadingTableRow; i < excelTable.Rows.Count; i++)
            {
                if (excelTable.Rows[i][1].ToString() == Premiums.CoverLengthYears.ToString())
                {
                    periodOfCoverLengthRow = i;
                    break;
                }
            }
            if (periodOfCoverLengthRow == -1)
                throw new Exception("Period Of Cover Loading Row Not Found");
            double premiumValueBasedonPeriodOfCoverLoading = Convert.ToDouble(excelTable.Rows[periodOfCoverLengthRow][2]);
            BasePremium = Convert.ToDouble(BasePremium * premiumValueBasedonPeriodOfCoverLoading);
        }



        //Reading Unit Type Loading Table and Caluclate Base Premium
        public void BuilderExperienceLoadingCaluclations(plotrows plot)
        {
            double builderExperianceBelow4Years = Convert.ToDouble(excelTable.Rows[15][2]);
            double builderExperianceAbove4Years = Convert.ToDouble(excelTable.Rows[20][2]);

            //Coverpremium value Based on Builder Experience 
            if (Premiums.BuilderExperiance == "1" || Premiums.BuilderExperiance == "2" || Premiums.BuilderExperiance == "3" || Premiums.BuilderExperiance == "4")
            {
                BasePremium = Convert.ToDouble(BasePremium * builderExperianceBelow4Years);
            }
            else
            {
                BasePremium = Convert.ToDouble(BasePremium * builderExperianceAbove4Years);
            }
        }
        //Reading Seepage Loading Table and Caluclate Base Premium
        public void SeepagePremiumCaluclations(plotrows plot)
        {
            var seepagePremiumTable = -1;
         
            //Get Unit Type Loading Table from Structural Premium Table
            for (int i = 0; i < excelTable.Rows.Count; i++)
            {
                if (excelTable.Rows[i][0].ToString().Contains("Seepage Premium"))
                {
                    seepagePremiumTable = i;
                    break;
                }
            }
            if (seepagePremiumTable == -1)
                throw new Exception("Seepage Premium Table Not Found");
            double seepagePremiumValue = Convert.ToDouble(excelTable.Rows[seepagePremiumTable+2][1]);
            SeepageFee = Convert.ToDouble(seepagePremiumValue * plot.ReconstructionCostValue);
        }
        //Reading Seepage Loading Table and Caluclate Base Premium
        public void WaiversOfSubrogationPremiumCaluclations(plotrows plot)
        {
            var waviersOfSubrogationRightsPremiumTable = -1;
            
            //Get Unit Type Loading Table from Structural Premium Table
            for (int i = 0; i < excelTable.Rows.Count; i++)
            {
                if (excelTable.Rows[i][0].ToString().Contains("Waivers of Subrogation Rights Premium"))
                {
                    waviersOfSubrogationRightsPremiumTable = i;
                    break;
                }
            }
            if (waviersOfSubrogationRightsPremiumTable == -1)
                throw new Exception("Seepage Premium Table Not Found");
            double waviersOfSubrogationRightsPremiumValue = Convert.ToDouble(excelTable.Rows[waviersOfSubrogationRightsPremiumTable + 2][1]);
            WaviersOfSubrogationRightsBuilderFee = Convert.ToDouble(waviersOfSubrogationRightsPremiumValue * plot.ReconstructionCostValue);
            WaviersOfSubrogationRightsEngineerFee = Convert.ToDouble(waviersOfSubrogationRightsPremiumValue * plot.ReconstructionCostValue);

        }

        //Services Caluclations 
        //TA Fee Caluclations   
        //Basic TAFee Caluclations     
        public void TAFeeCaluclations(plotrows plot)
        {
            // Reading TA Fee Table From Newhomes Pricing Setup Book           
            var TAFeeTableRow = -1;
            

            //Get TA Fee Table from Structural Premium Table
            for (int i = 0; i < excelTable.Rows.Count; i++)
            {
                if (excelTable.Rows[i][0].ToString() == "Technical Audit Fee")
                {
                    TAFeeTableRow = i;
                    break;
                }
            }
            if (TAFeeTableRow == -1)
                throw new Exception("TAFee Table Not Found");

          
            //Get TA Fee Value From TAFee Table 
            double taFeeValue = Convert.ToDouble(excelTable.Rows[TAFeeTableRow+2][1]);
            TAFee = Convert.ToDouble(plot.ReconstructionCostValue * taFeeValue);
        }
      

        //Minimum TAFee Caluclations Based On Conversion SOW 
        public void MinimumTAFeeCaluclations()
        {
            // Reading Minimum TA Fee Table From Newhomes Pricing Setup Book            
            var MinimumTAFeeTableRow = -1;
          

            //Get TA Fee New Build SOW Table from Structural Premium Table
            for (int i = 0; i < excelTable.Rows.Count; i++)
            {
                if (excelTable.Rows[i][0].ToString().Contains("Technical Audit Minimum Fee"))
                {
                    MinimumTAFeeTableRow = i;
                    break;
                }
            }
            if (MinimumTAFeeTableRow == -1)
                throw new Exception("Minimum TA Fee Table Not Found");
          
         
            MinimumTAFee = Convert.ToDouble(excelTable.Rows[MinimumTAFeeTableRow+2][1]);

            //Apply Minimum TAFee Value
            if (TAFee <= MinimumTAFee)
            {
                TAFee = Convert.ToDouble(MinimumTAFee);
            }

        }


        public void VerfiyChannelIslandsCommercialUIValues()
        {
            var productName = Premiums.eachplotrow.FirstOrDefault().ProductName;


            //Covers Elements 
            //Veridy UI Structural Values
            var uiStructuralFee = $"//th[@class='product-title'][text()='{productName}']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[text()='Structural']//following-sibling::td[@class='right-align'][1]";
            var StructuralFee = Driver.FindElement(By.XPath(uiStructuralFee)).Text.Replace("£", "");
            double UIStructuralFee = Math.Round(Convert.ToDouble(StructuralFee), 2);
            double StructuralFeeDiff;
            var uiStructuralIPTFee = $"//th[@class='product-title'][text()='{productName}']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[text()='Structural']//following-sibling::td[@class='right-align'][2]";
            var StructuralIPTFee = Driver.FindElement(By.XPath(uiStructuralIPTFee)).Text.Replace("£", "");
            double UIStructuralIPTFee = Math.Round(Convert.ToDouble(StructuralIPTFee), 2);
            double StructuralIPTDiff;
            var uiStructuralTotalFee = $"//th[@class='product-title'][text()='{productName}']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[text()='Structural']//following-sibling::th[@class='right-align'][1]";
            var StructuralTotalFee = Driver.FindElement(By.XPath(uiStructuralTotalFee)).Text.Replace("£", "");
            double UIStructuralTotalFee = Math.Round(Convert.ToDouble(StructuralTotalFee), 2);
            double StructuralTotalValue = Math.Round(Convert.ToDouble(UIStructuralFee + UIStructuralIPTFee), 2);
            double StructuralTotalDiff;

            if (UIStructuralFee != Premiums.StructuralFee || UIStructuralIPTFee != Premiums.StructuralIPTFee || UIStructuralTotalFee != StructuralTotalValue)
            {
                StructuralFeeDiff = Convert.ToDouble(UIStructuralFee - Premiums.StructuralFee);
                StructuralIPTDiff = Convert.ToDouble(UIStructuralIPTFee - Premiums.StructuralIPTFee);
                StructuralTotalDiff = Convert.ToDouble(UIStructuralTotalFee - StructuralTotalValue);
                if (StructuralFeeDiff >= -1.01 && StructuralFeeDiff <= 1.01)
                {
                    Premiums.StructuralFee = UIStructuralFee;
                }
                if (StructuralIPTDiff >= -1.01 && StructuralIPTDiff <= 1.01)
                {
                    Premiums.StructuralIPTFee = UIStructuralIPTFee;
                }
                if (StructuralTotalDiff >= -1.01 && StructuralTotalDiff <= 1.01)
                {
                    StructuralTotalValue = UIStructuralTotalFee;
                }

                Assert.AreEqual(UIStructuralFee, Premiums.StructuralFee, $"StructuralFee Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.EstimatedSalePrice}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                Assert.AreEqual(UIStructuralIPTFee, Premiums.StructuralIPTFee, $"StructuralIPTFee Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.EstimatedSalePrice}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                Assert.AreEqual(UIStructuralTotalFee, StructuralTotalValue, $"StructuralTotalFee Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.EstimatedSalePrice}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
            }
            else
            {

                Assert.AreEqual(UIStructuralFee, Premiums.StructuralFee, $"StructuralFee Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.EstimatedSalePrice}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                Assert.AreEqual(UIStructuralIPTFee, Premiums.StructuralIPTFee, $"StructuralIPTFee Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.EstimatedSalePrice}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                Assert.AreEqual(UIStructuralTotalFee, StructuralTotalValue, $"StructuralTotalFee Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.EstimatedSalePrice}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
            }
            if (Premiums.ProductsCovers.Any(x => x.Contains("Seepage")))
            {
                //UI Seepage Values
                var uiSeepageFee = $"//th[@class='product-title'][text()='{productName}']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[text()='Seepage']//following-sibling::td[@class='right-align'][1]";
                var SeepageFee = Driver.FindElement(By.XPath(uiSeepageFee)).Text.Replace("£", "");
                double UISeepageFee = Math.Round(Convert.ToDouble(SeepageFee), 2);
                double SeepageFeeDiff;

                var uiSeepageIPTFee = $"//th[@class='product-title'][text()='{productName}']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[text()='Seepage']//following-sibling::td[@class='right-align'][2]";
                var SeepageIPTFee = Driver.FindElement(By.XPath(uiSeepageIPTFee)).Text.Replace("£", "");
                double UISeepageIPTFee = Math.Round(Convert.ToDouble(SeepageIPTFee), 2);
                double SeepageIPTDiff;

                var uiSeepageTotalFee = $"//th[@class='product-title'][text()='{productName}']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[text()='Seepage']//following-sibling::th[@class='right-align'][1]";
                var SeepageTotalFee = Driver.FindElement(By.XPath(uiSeepageTotalFee)).Text.Replace("£", "");
                double UISeepageTotalFee = Math.Round(Convert.ToDouble(SeepageTotalFee), 2);
                double SeepageTotalValue = Math.Round(Convert.ToDouble(UISeepageFee + UISeepageIPTFee), 2);
                double SeepageTotalDiff;

                if (UISeepageFee != Premiums.SeepageFee || UISeepageIPTFee != Premiums.SeepageIPTFee || UISeepageTotalFee != SeepageTotalValue)
                {
                    SeepageFeeDiff = Convert.ToDouble(Premiums.SeepageFee - UISeepageFee);
                    SeepageIPTDiff = Convert.ToDouble(Premiums.SeepageIPTFee - UISeepageIPTFee);
                    SeepageTotalDiff = Convert.ToDouble(SeepageTotalValue - UISeepageTotalFee);
                    if (SeepageFeeDiff >= -1.01 && SeepageFeeDiff <= 1.01)
                    {
                        Premiums.SeepageFee = UISeepageFee;
                    }
                    if (SeepageIPTDiff >= -1.01 && SeepageIPTDiff <= 1.01)
                    {
                        Premiums.SeepageIPTFee = UISeepageIPTFee;
                    }
                    if (SeepageTotalDiff >= -1.01 && SeepageTotalDiff <= 1.01)
                    {
                        SeepageTotalValue = UISeepageTotalFee;
                    }

                    Assert.AreEqual(UISeepageFee, Premiums.SeepageFee, $"SeepageFee Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UISeepageIPTFee, Premiums.SeepageIPTFee, $"SeepageIPTFee Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UISeepageTotalFee, SeepageTotalValue, $"SeepageTotalValue Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                }
                else
                {
                    Assert.AreEqual(UISeepageFee, Premiums.SeepageFee, $"SeepageFee Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UISeepageIPTFee, Premiums.SeepageIPTFee, $"SeepageIPTFee Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UISeepageTotalFee, SeepageTotalValue, $"SeepageTotalValue Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                }
            }
            if (Premiums.ProductsCovers.Any(x => x.Contains("Waivers of Subrogation Rights - Engineer")))
            {

                //Verify UI Waivers of Subrogation Rights - Builder values
                var uiWaiversOfSubrogationRightsEngineerFee = $"//th[@class='product-title'][text()='{productName}']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[text()='Waivers of Subrogation Rights - Engineer']//following-sibling::td[@class='right-align'][1]";
                var WaiversOfSubrogationRightsEngineerFee = Driver.FindElement(By.XPath(uiWaiversOfSubrogationRightsEngineerFee)).Text.Replace("£", "");
                double UIWaiversOfSubrogationRightsEngineerFee = Math.Round(Convert.ToDouble(WaiversOfSubrogationRightsEngineerFee), 2);
                double WaiversOfSubrogationRightsEngineerFeeDiff;

                var uiWaiversOfSubrogationRightsEngineerIPTFee = $"//th[@class='product-title'][text()='{productName}']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[text()='Waivers of Subrogation Rights - Engineer']//following-sibling::td[@class='right-align'][2]";
                var WaiversOfSubrogationRightsEngineerIPTFee = Driver.FindElement(By.XPath(uiWaiversOfSubrogationRightsEngineerIPTFee)).Text.Replace("£", "");
                double UIWaiversOfSubrogationRightsEngineerIPTFee = Math.Round(Convert.ToDouble(WaiversOfSubrogationRightsEngineerIPTFee), 2);
                double WaiversOfSubrogationRightsEngineerIPTDiff;

                var uiWaiversOfSubrogationRightsEngineerTotalFee = $"//th[@class='product-title'][text()='{productName}']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[text()='Waivers of Subrogation Rights - Engineer']//following-sibling::th[@class='right-align'][1]";
                var WaiversOfSubrogationRightsEngineerTotalFee = Driver.FindElement(By.XPath(uiWaiversOfSubrogationRightsEngineerTotalFee)).Text.Replace("£", "");
                double UIWaiversOfSubrogationRightsEngineerTotalFee = Math.Round(Convert.ToDouble(WaiversOfSubrogationRightsEngineerTotalFee), 2);
                double WaiversOfSubrogationRightsEngineerTotalValue = Math.Round(Convert.ToDouble(UIWaiversOfSubrogationRightsEngineerFee + UIWaiversOfSubrogationRightsEngineerIPTFee), 2);
                double WaiversOfSubrogationRightsEngineerTotalDiff;

                if (UIWaiversOfSubrogationRightsEngineerFee != Premiums.WaiversOfSubrogationEngineerFee || UIWaiversOfSubrogationRightsEngineerIPTFee != Premiums.WaiversOfSubrogationEngineerIPTFee || UIWaiversOfSubrogationRightsEngineerTotalFee != WaiversOfSubrogationRightsEngineerTotalValue)
                {
                    WaiversOfSubrogationRightsEngineerFeeDiff = Convert.ToDouble(Premiums.WaiversOfSubrogationEngineerFee - UIWaiversOfSubrogationRightsEngineerFee);
                    WaiversOfSubrogationRightsEngineerIPTDiff = Convert.ToDouble(Premiums.WaiversOfSubrogationEngineerIPTFee - UIWaiversOfSubrogationRightsEngineerIPTFee);
                    WaiversOfSubrogationRightsEngineerTotalDiff = Convert.ToDouble(WaiversOfSubrogationRightsEngineerTotalValue - UIWaiversOfSubrogationRightsEngineerTotalFee);
                    if (WaiversOfSubrogationRightsEngineerFeeDiff >= -1.01 && WaiversOfSubrogationRightsEngineerFeeDiff <= 1.01)
                    {
                        Premiums.WaiversOfSubrogationEngineerFee = UIWaiversOfSubrogationRightsEngineerFee;
                    }

                    if (WaiversOfSubrogationRightsEngineerIPTDiff >= -1.01 && WaiversOfSubrogationRightsEngineerIPTDiff <= 1.01)
                    {
                        Premiums.WaiversOfSubrogationEngineerIPTFee = UIWaiversOfSubrogationRightsEngineerIPTFee;
                    }
                    if (WaiversOfSubrogationRightsEngineerTotalDiff >= -1.01 && WaiversOfSubrogationRightsEngineerTotalDiff <= 1.01)
                    {
                        WaiversOfSubrogationRightsEngineerTotalValue = UIWaiversOfSubrogationRightsEngineerTotalFee;
                    }

                    Assert.AreEqual(UIWaiversOfSubrogationRightsEngineerFee, Premiums.WaiversOfSubrogationEngineerFee, $"WaiversOfSubrogationEngineerFee Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UIWaiversOfSubrogationRightsEngineerIPTFee, Premiums.WaiversOfSubrogationEngineerIPTFee, $"WaiversOfSubrogationEngineerIPTFee Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UIWaiversOfSubrogationRightsEngineerTotalFee, WaiversOfSubrogationRightsEngineerTotalValue, $"WaiversOfSubrogationRightsEngineerTotalValue Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                }
                else
                {
                    Assert.AreEqual(UIWaiversOfSubrogationRightsEngineerFee, Premiums.WaiversOfSubrogationEngineerFee, $"WaiversOfSubrogationEngineerFee Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UIWaiversOfSubrogationRightsEngineerIPTFee, Premiums.WaiversOfSubrogationEngineerIPTFee, $"WaiversOfSubrogationEngineerIPTFee Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UIWaiversOfSubrogationRightsEngineerTotalFee, WaiversOfSubrogationRightsEngineerTotalValue, $"WaiversOfSubrogationRightsEngineerTotalValue Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                }
            }
            if (Premiums.ProductsCovers.Any(x => x.Contains("Waivers of Subrogation Rights - Builder")))
            {

                //ui Waivers of Subrogation Rights - Builder values
                var uiWaiversOfSubrogationRightsBuilderFee = $"//th[@class='product-title'][text()='{productName}']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[text()='Waivers of Subrogation Rights - Builder']//following-sibling::td[@class='right-align'][1]";
                var WaiversOfSubrogationRightsBuilderFee = Driver.FindElement(By.XPath(uiWaiversOfSubrogationRightsBuilderFee)).Text.Replace("£", "");
                double UIWaiversOfSubrogationRightsBuilderFee = Math.Round(Convert.ToDouble(WaiversOfSubrogationRightsBuilderFee), 2);
                double WaiversOfSubrogationRightsBuilderFeeDiff;

                var uiWaiversOfSubrogationRightsBuilderIPTFee = $"//th[@class='product-title'][text()='{productName}']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[text()='Waivers of Subrogation Rights - Builder']//following-sibling::td[@class='right-align'][2]";
                var WaiversOfSubrogationRightsBuilderIPTFee = Driver.FindElement(By.XPath(uiWaiversOfSubrogationRightsBuilderIPTFee)).Text.Replace("£", "");
                double UIWaiversOfSubrogationRightsBuilderIPTFee = Math.Round(Convert.ToDouble(WaiversOfSubrogationRightsBuilderIPTFee), 2);
                double WaiversOfSubrogationRightsBuilderIPTDiff;

                var uiWaiversOfSubrogationRightsBuilderTotalFee = $"//th[@class='product-title'][text()='{productName}']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[text()='Waivers of Subrogation Rights - Builder']//following-sibling::th[@class='right-align'][1]";
                var WaiversOfSubrogationRightsBuilderTotalFee = Driver.FindElement(By.XPath(uiWaiversOfSubrogationRightsBuilderTotalFee)).Text.Replace("£", "");
                double UIWaiversOfSubrogationRightsBuilderTotalFee = Math.Round(Convert.ToDouble(WaiversOfSubrogationRightsBuilderTotalFee), 2);
                double WaiversOfSubrogationRightsBuilderTotalValue = Math.Round(Convert.ToDouble(UIWaiversOfSubrogationRightsBuilderFee + UIWaiversOfSubrogationRightsBuilderIPTFee), 2);
                double WaiversOfSubrogationRightsBuilderTotalDiff;

                if (UIWaiversOfSubrogationRightsBuilderFee != Premiums.WaiversOfSubrogationBuilderFee || UIWaiversOfSubrogationRightsBuilderIPTFee != Premiums.WaiversOfSubrogationBuilderIPTFee || UIWaiversOfSubrogationRightsBuilderTotalFee != WaiversOfSubrogationRightsBuilderTotalValue)
                {
                    WaiversOfSubrogationRightsBuilderFeeDiff = Convert.ToDouble(Premiums.WaiversOfSubrogationBuilderFee - UIWaiversOfSubrogationRightsBuilderFee);
                    WaiversOfSubrogationRightsBuilderIPTDiff = Convert.ToDouble(Premiums.WaiversOfSubrogationBuilderIPTFee - UIWaiversOfSubrogationRightsBuilderIPTFee);
                    WaiversOfSubrogationRightsBuilderTotalDiff = Convert.ToDouble(WaiversOfSubrogationRightsBuilderTotalValue - UIWaiversOfSubrogationRightsBuilderTotalFee);
                    if (WaiversOfSubrogationRightsBuilderFeeDiff >= -1.01 && WaiversOfSubrogationRightsBuilderFeeDiff <= 1.01)
                    {
                        Premiums.WaiversOfSubrogationBuilderFee = UIWaiversOfSubrogationRightsBuilderFee;
                    }
                    if (WaiversOfSubrogationRightsBuilderIPTDiff >= -1.01 && WaiversOfSubrogationRightsBuilderIPTDiff <= 1.01)
                    {
                        Premiums.WaiversOfSubrogationBuilderIPTFee = UIWaiversOfSubrogationRightsBuilderIPTFee;
                    }
                    if (WaiversOfSubrogationRightsBuilderTotalDiff >= -1.01 && WaiversOfSubrogationRightsBuilderTotalDiff <= 1.01)
                    {
                        WaiversOfSubrogationRightsBuilderTotalValue = UIWaiversOfSubrogationRightsBuilderTotalFee;
                    }

                    Assert.AreEqual(UIWaiversOfSubrogationRightsBuilderFee, Premiums.WaiversOfSubrogationBuilderFee, $"WaiversOfSubrogationBuilderFee Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UIWaiversOfSubrogationRightsBuilderIPTFee, Premiums.WaiversOfSubrogationBuilderIPTFee, $"WaiversOfSubrogationBuilderIPTFee Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UIWaiversOfSubrogationRightsBuilderTotalFee, WaiversOfSubrogationRightsBuilderTotalValue, $"WaiversOfSubrogationRightsBuilderTotalValue Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                }
                else
                {
                    Assert.AreEqual(UIWaiversOfSubrogationRightsBuilderFee, Premiums.WaiversOfSubrogationBuilderFee, $"WaiversOfSubrogationBuilderFee Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UIWaiversOfSubrogationRightsBuilderIPTFee, Premiums.WaiversOfSubrogationBuilderIPTFee, $"WaiversOfSubrogationBuilderIPTFee Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UIWaiversOfSubrogationRightsBuilderTotalFee, WaiversOfSubrogationRightsBuilderTotalValue, $"WaiversOfSubrogationRightsBuilderTotalValue Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                }
            }

            //Services Elements  and  Fee values  
            //Technical Audit Fee values  
            if (Premiums.ProductServices.Any(x => x.Contains("Technical Audit Fee")))
            {
                var uiTechnicalAuditFee = $"//th[@class='product-title'][text()='{productName}']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[text()='Technical Audit Fee']//following-sibling::td[@class='right-align'][1]";
                var TechnicalAuditFee = Driver.FindElement(By.XPath(uiTechnicalAuditFee)).Text.Replace("£", "");
                double UITechnicalAuditFee = Math.Round(Convert.ToDouble(TechnicalAuditFee), 2);
                double TechnicalAuditFeeDiff;

                var uiTechnicalAuditVATFee = $"//th[@class='product-title'][text()='{productName}']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[text()='Technical Audit Fee']//following-sibling::td[@class='right-align'][2]";
                var TechnicalAuditVATFee = Driver.FindElement(By.XPath(uiTechnicalAuditVATFee)).Text.Replace("£", "");
                double UITechnicalAuditVATFee = Math.Round(Convert.ToDouble(TechnicalAuditVATFee), 2);
                double TechnicalAuditVATDiff;

                var uiTechnicalAuditTotalFee = $"//th[@class='product-title'][text()='{productName}']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[text()='Technical Audit Fee']//following-sibling::th[@class='right-align'][1]";
                var TechnicalAuditTotalFee = Driver.FindElement(By.XPath(uiTechnicalAuditTotalFee)).Text.Replace("£", "");
                double UITechnicalAuditTotalFee = Math.Round(Convert.ToDouble(TechnicalAuditTotalFee), 2);
                double TechnicalAuditTotalValue = Math.Round(Convert.ToDouble(UITechnicalAuditFee + UITechnicalAuditVATFee), 2);
                double TechnicalAuditTotalDiff;

                if (UITechnicalAuditFee != Premiums.TechnicalAuditFee || UITechnicalAuditVATFee != Premiums.TechnicalAuditVATFee || UITechnicalAuditTotalFee != TechnicalAuditTotalValue)
                {
                    TechnicalAuditFeeDiff = Convert.ToDouble(UITechnicalAuditFee - Premiums.TechnicalAuditFee);
                    TechnicalAuditVATDiff = Convert.ToDouble(UITechnicalAuditVATFee - Premiums.TechnicalAuditVATFee);
                    TechnicalAuditTotalDiff = Convert.ToDouble(UITechnicalAuditTotalFee - TechnicalAuditTotalValue);
                    if (TechnicalAuditFeeDiff >= -1.01 && TechnicalAuditFeeDiff <= 1.01)
                    {
                        Premiums.TechnicalAuditFee = UITechnicalAuditFee;
                    }

                    if (TechnicalAuditVATDiff >= -1.01 && TechnicalAuditVATDiff <= 1.01)
                    {
                        Premiums.TechnicalAuditVATFee = UITechnicalAuditVATFee;
                    }
                    if (TechnicalAuditTotalDiff >= -1.01 && TechnicalAuditTotalDiff <= 1.01)
                    {
                        TechnicalAuditTotalValue = UITechnicalAuditTotalFee;
                    }

                    Assert.AreEqual(UITechnicalAuditFee, Premiums.TechnicalAuditFee, $"TechnicalAuditFee Services Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.EstimatedSalePrice}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UITechnicalAuditVATFee, Premiums.TechnicalAuditVATFee, $"TechnicalAuditVATFee Services Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.EstimatedSalePrice}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UITechnicalAuditTotalFee, TechnicalAuditTotalValue, $"TechnicalAuditTotalValue Services Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.EstimatedSalePrice}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                }
                else
                {
                    Assert.AreEqual(UITechnicalAuditFee, Premiums.TechnicalAuditFee, $"TechnicalAuditFee Services Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.EstimatedSalePrice}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UITechnicalAuditVATFee, Premiums.TechnicalAuditVATFee, $"TechnicalAuditVATFee Services Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.EstimatedSalePrice}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UITechnicalAuditTotalFee, TechnicalAuditTotalValue, $"TechnicalAuditTotalValue Services Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.EstimatedSalePrice}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");

                }
            }

            //Refurbishment Assessment Fee values  
            if (Premiums.ProductServices.Any(x => x.Contains("Refurbishment Assessment Fee")))
            {
                var uiRefurbishmentFee = $"//th[@class='product-title'][text()='{productName}']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[text()='Refurbishment Assessment Fee']//following-sibling::td[@class='right-align'][1]";
                var RefurbishmentFee = Driver.FindElement(By.XPath(uiRefurbishmentFee)).Text.Replace("£", "");
                double UIRefurbishmentFee = Math.Round(Convert.ToDouble(RefurbishmentFee), 2);
                double RefurbishmentFeeDiff;

                var uiRefurbishmentVATFee = $"//th[@class='product-title'][text()='{productName}']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[text()='Refurbishment Assessment Fee']//following-sibling::td[@class='right-align'][2]";
                var RefurbishmentVATFee = Driver.FindElement(By.XPath(uiRefurbishmentVATFee)).Text.Replace("£", "");
                double UIRefurbishmentVATFee = Math.Round(Convert.ToDouble(RefurbishmentVATFee), 2);
                double RefurbishmentVATDiff;

                var uiRefurbishmentTotalFee = $"//th[@class='product-title'][text()='{productName}']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[text()='Refurbishment Assessment Fee']//following-sibling::th[@class='right-align'][1]";
                var RefurbishmentTotalFee = Driver.FindElement(By.XPath(uiRefurbishmentTotalFee)).Text.Replace("£", "");
                double UIRefurbishmentTotalFee = Math.Round(Convert.ToDouble(RefurbishmentTotalFee), 2);
                double RefurbishmentTotalValue = Math.Round(Convert.ToDouble(UIRefurbishmentFee + UIRefurbishmentVATFee), 2);
                double RefurbishmentTotalDiff;


                if (UIRefurbishmentFee != Premiums.RefurbishmentAssessmentFee || UIRefurbishmentVATFee != Premiums.RefurbishmentAssessmentVATFee || UIRefurbishmentTotalFee != RefurbishmentTotalValue)
                {
                    RefurbishmentFeeDiff = Convert.ToDouble(Premiums.RefurbishmentAssessmentFee - UIRefurbishmentFee);
                    RefurbishmentVATDiff = Convert.ToDouble(Premiums.RefurbishmentAssessmentVATFee - UIRefurbishmentVATFee);
                    RefurbishmentTotalDiff = Convert.ToDouble(RefurbishmentTotalValue - UIRefurbishmentTotalFee);
                    if (RefurbishmentFeeDiff >= -1.01 && RefurbishmentFeeDiff <= 1.01)
                    {
                        Premiums.RefurbishmentAssessmentFee = UIRefurbishmentFee;
                    }

                    if (RefurbishmentVATDiff >= -1.01 && RefurbishmentVATDiff <= 1.01)
                    {
                        Premiums.RefurbishmentAssessmentVATFee = UIRefurbishmentVATFee;
                    }
                    if (RefurbishmentTotalDiff >= -1.01 && RefurbishmentTotalDiff <= 1.01)
                    {
                        RefurbishmentTotalValue = UIRefurbishmentTotalFee;
                    }

                    Assert.AreEqual(UIRefurbishmentFee, Premiums.RefurbishmentAssessmentFee, $"RefurbishmentFee Services Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.EstimatedSalePrice}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UIRefurbishmentVATFee, Premiums.RefurbishmentAssessmentVATFee, $"RefurbishmentVATFee Services Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.EstimatedSalePrice}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UIRefurbishmentTotalFee, RefurbishmentTotalValue, $"RefurbishmentTotalValue Services Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.EstimatedSalePrice}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                }
                else
                {
                    Assert.AreEqual(UIRefurbishmentFee, Premiums.RefurbishmentAssessmentFee, $"RefurbishmentFee Services Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.EstimatedSalePrice}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UIRefurbishmentVATFee, Premiums.RefurbishmentAssessmentVATFee, $"RefurbishmentVATFee Services Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.EstimatedSalePrice}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UIRefurbishmentTotalFee, RefurbishmentTotalValue, $"RefurbishmentTotalValue Services Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.EstimatedSalePrice}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                }
            }

            //Administrator Fee values    
            if (Premiums.ProductServices.Any(x => x.Contains("Administration Fee")))
            {

                var uiAdministrationFee = $"//th[@class='product-title'][text()='{productName}']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[text()='Administration Fee']//following-sibling::td[@class='right-align'][1]";
                var AdministrationFee = Driver.FindElement(By.XPath(uiAdministrationFee)).Text.Replace("£", "");
                double UIAdministrationFee = Math.Round(Convert.ToDouble(AdministrationFee), 2);
                double AdministrationFeeDiff;

                var uiAdministrationVATFee = $"//th[@class='product-title'][text()='{productName}']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[text()='Administration Fee']//following-sibling::td[@class='right-align'][2]";
                var AdministrationVATFee = Driver.FindElement(By.XPath(uiAdministrationVATFee)).Text.Replace("£", "");
                double UIAdministrationVATFee = Math.Round(Convert.ToDouble(AdministrationVATFee), 2);
                double AdministrationVATDiff;

                var uiAdministrationTotalFee = $"//th[@class='product-title'][text()='{productName}']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[text()='Administration Fee']//following-sibling::th[@class='right-align'][1]";
                var AdministrationTotalFee = Driver.FindElement(By.XPath(uiAdministrationTotalFee)).Text.Replace("£", "");
                double UIAdministrationTotalFee = Math.Round(Convert.ToDouble(AdministrationTotalFee), 2);
                double AdministrationTotalValue = Math.Round(Convert.ToDouble(UIAdministrationFee + UIAdministrationVATFee), 2);
                double AdministrationTotalDiff;

                if (UIAdministrationFee != Premiums.AdministrationFee || UIAdministrationVATFee != Premiums.AdministrationVATFee || UIAdministrationTotalFee != AdministrationTotalValue)
                {
                    AdministrationFeeDiff = Convert.ToDouble(UIAdministrationFee - Premiums.AdministrationFee);
                    AdministrationVATDiff = Convert.ToDouble(UIAdministrationVATFee - Premiums.AdministrationVATFee);
                    AdministrationTotalDiff = Convert.ToDouble(UIAdministrationTotalFee - AdministrationTotalValue);
                    if (AdministrationFeeDiff >= -1.01 && AdministrationFeeDiff <= 1.01)
                    {
                        Premiums.AdministrationFee = UIAdministrationFee;
                    }

                    if (AdministrationVATDiff >= -1.01 && AdministrationVATDiff <= 1.01)
                    {
                        Premiums.AdministrationVATFee = UIAdministrationVATFee;
                    }
                    if (AdministrationTotalDiff >= 1.01 && AdministrationTotalDiff <= 1.01)
                    {
                        AdministrationTotalValue = UIAdministrationTotalFee;
                    }

                    Assert.AreEqual(UIAdministrationFee, Premiums.AdministrationFee, $"AdministrationFee Services Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.EstimatedSalePrice}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UIAdministrationVATFee, Premiums.AdministrationVATFee, $"AdministrationVATFee Services Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.EstimatedSalePrice}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UIAdministrationTotalFee, AdministrationTotalValue, $"AdministrationTotalValue Services Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.EstimatedSalePrice}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                }
                else
                {
                    Assert.AreEqual(UIAdministrationFee, Premiums.AdministrationFee, $"AdministrationFee Services Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.EstimatedSalePrice}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UIAdministrationVATFee, Premiums.AdministrationVATFee, $"AdministrationVATFee Services Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.EstimatedSalePrice}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UIAdministrationTotalFee, AdministrationTotalValue, $"AdministrationTotalValue Services Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.EstimatedSalePrice}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                }
            }
            //Cancellation Fee values  
            if (Premiums.ProductServices.Any(x => x.Contains("Cancellation Fee")))
            {


                var uiCancellationFee = $"//th[@class='product-title'][text()='{productName}']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[text()='Cancellation Fee']//following-sibling::td[@class='right-align'][2]";
                var CancellationFee = Driver.FindElement(By.XPath(uiCancellationFee)).Text.Replace("£", "");
                double UICancellationFee = Math.Round(Convert.ToDouble(CancellationFee), 2);
                double CancellationFeeDiff;

                var uiCancellationVATFee = $"//th[@class='product-title'][text()='{productName}']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[text()='Cancellation Fee']//following-sibling::td[@class='right-align'][2]";
                var CancellationVATFee = Driver.FindElement(By.XPath(uiCancellationVATFee)).Text.Replace("£", "");
                double UICancellationVATFee = Math.Round(Convert.ToDouble(CancellationVATFee), 2);
                double CancellationVATDiff;

                var uiCancellationTotalFee = $"//th[@class='product-title'][text()='{productName}']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[text()='Cancellation Fee']//following-sibling::td[@class='right-align'][2]";
                var CancellationTotalFee = Driver.FindElement(By.XPath(uiCancellationTotalFee)).Text.Replace("£", "");
                double UICancellationTotalFee = Math.Round(Convert.ToDouble(CancellationTotalFee), 2);
                double CancellationTotalValue = Math.Round(Convert.ToDouble(UICancellationFee + UICancellationVATFee), 2);
                double CancellationTotalDiff;


                if (UICancellationFee != Premiums.CancellationFee || UICancellationVATFee != Premiums.CancellationVATFee || UICancellationTotalFee != CancellationTotalValue)
                {
                    CancellationFeeDiff = Convert.ToDouble(UICancellationFee - Premiums.CancellationFee);
                    CancellationVATDiff = Convert.ToDouble(UICancellationVATFee - Premiums.CancellationVATFee);
                    CancellationTotalDiff = Convert.ToDouble(UICancellationTotalFee - CancellationTotalValue);
                    if (CancellationFeeDiff >= -1.01 && CancellationFeeDiff <= 1.01)
                    {
                        Premiums.CancellationFee = UICancellationFee;
                    }

                    if (CancellationVATDiff >= -1.01 && CancellationVATDiff <= 1.01)
                    {
                        Premiums.CancellationVATFee = UICancellationVATFee;
                    }
                    if (CancellationTotalDiff >= -1.01 && CancellationTotalDiff <= 1.01)
                    {
                        CancellationTotalValue = UICancellationTotalFee;
                    }

                    Assert.AreEqual(UICancellationFee, Premiums.CancellationFee, $"CancellationFee Services Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.EstimatedSalePrice}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UICancellationVATFee, Premiums.CancellationVATFee, $"CancellationVATFee Services Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.EstimatedSalePrice}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UICancellationTotalFee, CancellationTotalValue, $"CancellationTotalValue Services Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.EstimatedSalePrice}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                }
                else
                {
                    Assert.AreEqual(UICancellationFee, Premiums.CancellationFee, $"CancellationFee Services Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.EstimatedSalePrice}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UICancellationVATFee, Premiums.CancellationVATFee, $"CancellationVATFee Services Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.EstimatedSalePrice}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UICancellationTotalFee, CancellationTotalValue, $"CancellationTotalValue Services Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.EstimatedSalePrice}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                }
            }
        }

        public DataTable ReadChannelIslandsCommercialMasterPremiumsPriceData(string fileName)
        {

            var result = new DataSet();                                                                             //Set the First Row as Column Name
            using (var stream = File.Open(fileName, FileMode.Open, FileAccess.Read))
            using (var excelReader = ExcelReaderFactory.CreateOpenXmlReader(stream)) //.xlsx
            {
                //Return as DataSet
                result = excelReader.AsDataSet(new ExcelDataSetConfiguration()
                {
                    ConfigureDataTable = (_) => new ExcelDataTableConfiguration()
                    {
                        UseHeaderRow = true
                    }
                });
            }
            //Get all the Tables
            DataTableCollection table = result.Tables;
            //Reading Last Sheet From Product Pricing Setup 
            DataTable resultTable = table["CI - Commercial (3)"];
            return resultTable;
        }
    }
}