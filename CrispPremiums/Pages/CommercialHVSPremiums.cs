﻿using OpenQA.Selenium;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.PageObjects;
using RegressionPacks.Support;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using ExcelDataReader;

namespace RegressionPacks.Pages
{
    public class CommercialHVSPremiums : Support.Pages
    {
        public string RatingValue;
        public IWebDriver wdriver;
        double iptCaluclationValue = 0.12;
        double vatCaluclationValue = 0.20;
        double BasePremium = 0;      
        double LABuildingControlFunctionFee = 0;
        double SeepageFee = 0;
        double LossOfGrossProfitPremium = 0;
        double LossOfRentPayablePremium = 0;
        double LossOfRentReceivablePremium = 0;
        double WaviersArchitect = 0;
        double WaviersEngineer = 0;
        double WaviersBuilder = 0;
        double WaterproofBasement = 0;
        double TAFee = 0;
        int RatingColumn;

        double AtkinsPremium = 0;       
        DataTable excelTable;      

        public CommercialHVSPremiums(ISearchContext driver)
        {
            PageFactory.InitElements(driver, this);
            wdriver = (RemoteWebDriver)driver;
        }

        public void CoversOnCommercialHVSProduct()
        {
            if (Premiums.eachplotrow.Any(o => o.ProductName.Contains("Commercial - High Value")))
            {
                Premiums.ProductsCovers = PremiumCovers.ProductCoversList.Select(i => i.Text.ToString()).ToList();
                Premiums.ProductServices = PremiumCovers.ProductServicesList.Select(i => i.Text.ToString()).ToList();

                if (Premiums.ProductsCovers.Any(x => x.Contains("Contaminated Land")))
                {
                    Premiums.ContaminatedLandCover = PremiumCovers.ContaminatedCover.Count > 0 ? "Yes" : "No";
                }
                if (Premiums.ProductsCovers.Any(x => x.Contains("Additional Cover for Local Authority Building Control Function")))
                {
                    Premiums.LocalAuthorityBuildingControlFunctionCover = PremiumCovers.LocalAuthorityBuildingControlFunctionCover.Count > 0 ? "Yes" : "No";
                }
                if (Premiums.ProductsCovers.Any(x => x.Contains("Seepage")))
                {
                    Premiums.SeepageCover = PremiumCovers.SeeapageCover.Count > 0 ? "Yes" : "No";
                }
                if (Premiums.ProductsCovers.Any(x => x.Contains("Waivers of Subrogation Rights - Builder")))
                {
                    Premiums.WaiversOfSubrogationRightsBuilderCover = PremiumCovers.WaiversBuilderCover.Count > 0 ? "Yes" : "No";
                }
                if (Premiums.ProductsCovers.Any(x => x.Contains("Loss of Gross Profit")))
                {
                    Premiums.LossOfGrossProfitCover = PremiumCovers.LossOfGrossProfitCover.Count > 0 ? "Yes" : "No";
                }
                if (Premiums.ProductsCovers.Any(x => x.Contains("Loss of Rent Payable")))
                {
                    Premiums.LossOfRentPayableCover = PremiumCovers.LossOfRentPayableCover.Count > 0 ? "Yes" : "No";
                }
                if (Premiums.ProductsCovers.Any(x => x.Contains("Loss of Rent Receivable")))
                {
                    Premiums.LossOfRentReceivableCover = PremiumCovers.LossOfRentReceivableCover.Count > 0 ? "Yes" : "No";
                }
                if (Premiums.ProductsCovers.Any(x => x.Contains("Waivers of Subrogation Rights - Engineer")))
                {
                    Premiums.WaiversOfSubrogationRightsEngineerCover = PremiumCovers.WaiversEngineerCover.Count > 0 ? "Yes" : "No";
                }
                if (Premiums.ProductsCovers.Any(x => x.Contains("Waivers of Subrogation Rights - Architect")))
                {
                    Premiums.WaiversOfSubrogationRightsArchitectCover = PremiumCovers.WaiversArchitectCover.Count > 0 ? "Yes" : "No";
                }
                if (Premiums.ProductServices.Any(x => x.Contains("Technical Audit Fee")))
                {
                    Premiums.ProductTechnicalAuditFeeService = PremiumCovers.ProductTechnicalAuditFee.Count > 0 ? "Yes" : "No";
                }
                if (Premiums.ProductServices.Any(x => x.Contains("Administration Fee")))
                {
                    Premiums.ProductAdminFeeService = PremiumCovers.ProductAdministrationFee.Count > 0 ? "Yes" : "No";
                }
                if (Premiums.ProductServices.Any(x => x.Contains("Cancellation Fee")))
                {
                    Premiums.ProductCancellationFeeService = PremiumCovers.ProductCancellationFee.Count > 0 ? "Yes" : "No";
                }
                if (Premiums.ProductServices.Any(x => x.Contains("Atkins Fee")))
                {
                    Premiums.ProductAtkinsFeeService = PremiumCovers.ProductAtkinsFee.Count > 0 ? "Yes" : "No";
                }
            }
        }
        //Caluclating Premiums For Each Row 
        public void CommercialHVSPremiumsCaluclations()
        {
            Premiums.StructuralFee = 0;
            Premiums.ContaminatedLandFee = 0;
            Premiums.LocalAuthorityBuildingControlFee = 0;
            Premiums.SeepageFee = 0;
            Premiums.WaiversOfSubrogationBuilderFee = 0;
            Premiums.LossOfGrossProfitFee = 0;
            Premiums.LossOfRentPayableFee = 0;
            Premiums.LossOfRentReceivableFee = 0;
            Premiums.WaiversOfSubrogationArchitectFee = 0;
            Premiums.WaiversOfSubrogationEngineerFee = 0;
            Premiums.TechnicalAuditFee = 0;
            Premiums.AdministrationFee = 0;
            Premiums.CancellationFee = 0;
            Premiums.AtkinsFee = 0;
            var aggregatedPremium = new CommercialHVSPremium();
            foreach (var plot in Premiums.eachplotrow)
            {
                var calculatedPremiums = CommercialHVSPremiumCaluclationsForEachPlotRow(plot);
                Premiums.StructuralFee += calculatedPremiums.structuralFee;
                Premiums.ContaminatedLandFee += calculatedPremiums.contaminatedLandFee;
                Premiums.LocalAuthorityBuildingControlFee += calculatedPremiums.lABuildingControlFuntionFee;              
                Premiums.SeepageFee += calculatedPremiums.seepageFee;
                Premiums.WaiversOfSubrogationBuilderFee += calculatedPremiums.waviersOfSubrogationRightsBuilderFee;
                Premiums.LossOfGrossProfitFee += calculatedPremiums.lossOfGrossProfitFee;
                Premiums.LossOfRentPayableFee += calculatedPremiums.lossOfRentPayableFee;
                Premiums.LossOfRentReceivableFee += calculatedPremiums.lossOfRentReceivableFee;
                Premiums.WaiversOfSubrogationEngineerFee += calculatedPremiums.waviersOfSubrogationRightsEngineerFee;
                Premiums.WaiversOfSubrogationArchitectFee += calculatedPremiums.waviersOfSubrogationRightsArchitectFee;
                //Services 
                Premiums.TechnicalAuditFee += calculatedPremiums.technicalFee;
                Premiums.AdministrationFee += calculatedPremiums.administrationFee;
                Premiums.CancellationFee += calculatedPremiums.cancellationFee;
                Premiums.AtkinsFee = calculatedPremiums.atkinsFee;              
            }          
          
            //IPT caluclations 
            Premiums.StructuralIPTFee = Convert.ToDouble(Premiums.StructuralFee * iptCaluclationValue);
            Premiums.ContaminatedLandIPTFee = Convert.ToDouble(Premiums.ContaminatedLandFee * iptCaluclationValue);
            Premiums.LocalAuthorityBuildingControlIPTFee = Convert.ToDouble(Premiums.LocalAuthorityBuildingControlFee * iptCaluclationValue);
            Premiums.SeepageIPTFee = Convert.ToDouble(Premiums.SeepageFee * iptCaluclationValue);
            Premiums.WaiversOfSubrogationBuilderIPTFee = Convert.ToDouble(Premiums.WaiversOfSubrogationBuilderFee * iptCaluclationValue);
            Premiums.LossOfGrossProfitIPTFee = Convert.ToDouble(Premiums.LossOfGrossProfitFee * iptCaluclationValue);
            Premiums.LossOfRentPayableIPTFee = Convert.ToDouble(Premiums.LossOfRentPayableFee * iptCaluclationValue);
            Premiums.LossOfRentReceivableIPTFee = Convert.ToDouble(Premiums.LossOfRentReceivableFee * iptCaluclationValue);
            Premiums.WaiversOfSubrogationEngineerIPTFee = Convert.ToDouble(Premiums.WaiversOfSubrogationEngineerFee * iptCaluclationValue);
            Premiums.WaiversOfSubrogationArchitectIPTFee = Convert.ToDouble(Premiums.WaiversOfSubrogationArchitectFee * iptCaluclationValue);
            //VAT Calucaltions 
            Premiums.TechnicalAuditVATFee = Convert.ToDouble(0.00);
            Premiums.AdministrationVATFee = Convert.ToDouble(0.00);
            Premiums.CancellationVATFee = Convert.ToDouble(0.00);
            Premiums.AtkinsVATFee = Convert.ToDouble(Premiums.AtkinsFee * iptCaluclationValue);
            VerfiyCommercialHVSUIValues();
            if (Premiums.eachplotrow.Any(o => o.BCProduct.Contains("Commercial Building Control")))
            {
                CommercialBCPremiums.CommercialBCPremiumCalcs();
            }
        }

        public class CommercialHVSPremium
        {
            public double structuralFee;

            public double contaminatedLandFee;

            public double lABuildingControlFuntionFee;          

            public double seepageFee;

            public double waviersOfSubrogationRightsBuilderFee;

            public double lossOfGrossProfitFee;

            public double lossOfRentPayableFee;

            public double lossOfRentReceivableFee;

            public double waviersOfSubrogationRightsEngineerFee;

            public double waviersOfSubrogationRightsArchitectFee;

            public double technicalFee;

            public double administrationFee;

            public double cancellationFee;

            public double atkinsFee;

        }

        public CommercialHVSPremium CommercialHVSPremiumCaluclationsForEachPlotRow(plotrows plot)
        {
            var calculatedPremiums = new CommercialHVSPremium();
            //Retrive all the covers and Services from Fees Detaila Page 
            CoversOnCommercialHVSProduct();
            //Reading Pricing Setup Book 
            var fileInfo = new FileInfo(ExtensionMethods.GetAbsolutePath($@"TestData\Premiums\ProductPricingSetup\Product Pricing Setup - HVS.xlsx"));
            string premiumsdata = fileInfo.ToString();
            excelTable = ReadCommercialHVSMasterPremiumsPriceData(premiumsdata);
            //Structural Fee Caluclations Based on Banding           
            StuctralPremiumCaluclations(plot);
            SecondaryLayerPremiums.AddSecondaryLayerPremium(BasePremium, plot);
            BasePremium = Premiums.SecondaryLayerPremium;
            calculatedPremiums.structuralFee = Math.Round(BasePremium, 2);

            AtkinsFeeCaluclations();
            calculatedPremiums.atkinsFee = Math.Round(Convert.ToDouble(AtkinsPremium), 2);

            if (Premiums.ProductsCovers.Count > 0)
            {
                if (Premiums.ContaminatedLandCover == "Yes")
                {
                    calculatedPremiums.contaminatedLandFee = Convert.ToDouble(0.00);
                }
                if (Premiums.LocalAuthorityBuildingControlFunctionCover == "Yes")
                {
                    calculatedPremiums.lABuildingControlFuntionFee = Math.Round(Convert.ToDouble(LABuildingControlFunctionFee), 2);                    
                }
                if (Premiums.SeepageCover == "Yes")
                {
                    calculatedPremiums.seepageFee = Math.Round((SeepageFee), 2);
                }
                if (Premiums.WaiversOfSubrogationRightsBuilderCover == "Yes")
                {
                    WaiversBuilderPremiumCaluclations(plot);
                    calculatedPremiums.waviersOfSubrogationRightsBuilderFee = Math.Round((WaviersBuilder), 2);
                }
                if (Premiums.LossOfGrossProfitCover == "Yes")
                {
                    LossOfRentCaluclations(plot);
                    calculatedPremiums.lossOfGrossProfitFee = Math.Round((LossOfGrossProfitPremium), 2);
                }
                if (Premiums.LossOfRentPayableCover == "Yes")
                {
                    LossOfRentCaluclations(plot);
                    calculatedPremiums.lossOfRentPayableFee = Math.Round((LossOfRentPayablePremium), 2);
                }
                if (Premiums.LossOfRentReceivableCover == "Yes")
                {
                    LossOfRentCaluclations(plot);
                    calculatedPremiums.lossOfRentReceivableFee = Math.Round((LossOfRentReceivablePremium), 2);
                }
                if (Premiums.WaiversOfSubrogationRightsEngineerCover == "Yes")
                {
                    WaiversEngineerPremiumCaluclations(plot);
                    calculatedPremiums.waviersOfSubrogationRightsEngineerFee = Math.Round((WaviersEngineer), 2);
                }
                if (Premiums.WaiversOfSubrogationRightsArchitectCover == "Yes")
                {
                    WaiversArchitectPremiumCaluclations(plot);
                    calculatedPremiums.waviersOfSubrogationRightsArchitectFee = Math.Round((WaviersArchitect), 2);
                }
            }

            //Product Service Fee Values 
            if (Premiums.ProductServices.Count > 0)
            {

                if (Premiums.ProductTechnicalAuditFeeService == "Yes")
                {
                    //Cover Services Caluclations 
                    TAFeeCaluclations(plot);
                    calculatedPremiums.technicalFee = Math.Round((TAFee), 2);
                }
                if (Premiums.ProductAdminFeeService == "Yes")
                {
                    calculatedPremiums.administrationFee = Convert.ToDouble(0.00);
                }
                if (Premiums.ProductCancellationFeeService == "Yes")
                {
                    calculatedPremiums.cancellationFee = Convert.ToDouble(0.00);
                }
            }
       
            return calculatedPremiums;
        }



        //Reading Strucural Premium Banding Table  and Caluclate Base Premium
        public void StuctralPremiumCaluclations(plotrows plot)
        {          
            // Reading Structural Premium Table From CommercialHVS Pricing Setup Book
            var rating = Premiums.Rating;
            var ratingTableRow = -1;
            var ratingColumn = -1;
            //Get Rating Table from Structural Premium Primary Layer Table
            for (int i = 0; i < excelTable.Rows.Count; i++)
            {
                if (excelTable.Rows[i][1].ToString().Contains("Rating"))
                {
                    ratingTableRow = i;
                    break;
                }
            }
            if (ratingTableRow == -1)
                throw new Exception("Rating Table Row Not Found");


            //Get Rating Columns from Structural Premium Table

            for (int i = ratingTableRow; i < excelTable.Rows[ratingTableRow].ItemArray.Length; i++)
            {
                if (excelTable.Rows[ratingTableRow].ItemArray[i].ToString() == Premiums.Rating.ToString())
                {
                    ratingColumn = i;
                    RatingColumn = ratingColumn;
                }
            }

            if (ratingColumn == -1)
                throw new Exception("Rating Column Not Found");

            //Get Premium Value From structural Premium Table 
            double basicpremiumValue = Convert.ToDouble(excelTable.Rows[ratingTableRow + 1][RatingColumn]);
            BasePremium = Convert.ToDouble(plot.ReconstructionCostValue * basicpremiumValue);

            //Cover Loading Premiums 
            PeriodOfCoverLoadingCaluclation(plot);

            //Caluclate Premiums Based on Unit type 
            UnitTypeCaluclation(plot);
            //Caluclate Premiums Based on Construction type 
            if (Premiums.eachplotrow.Any(o => o.ConstructionType.Contains("New Build")) && Premiums.eachplotrow.Any(o => o.ConstructionType.Contains("Conversion")))
            {
                if (plot.ConstructionType.Contains("New Build"))
                {
                    ConstructionTypeCaluclationNewbuildandConversion(plot);
                }
                else
                {
                    ConstructionTypeCaluclation(plot);
                }
            }
            else
            {
                ConstructionTypeCaluclation(plot);
            }

            //Multi Project Loading Caluclation              

            if (SendQuotePage.MultiProjectDiscount.Count > 0)
            {
                Premiums.MultiProjectLoading = SendQuotePage.MultiProjectDiscount[0].Text;
                if (Premiums.MultiProjectLoading == "Yes")
                {
                    MultiProjectLoadngDiscountCaluclations(plot);
                }

            }
            if (SendQuotePage.DiscretionaryDiscount.Count > 0)
            {
                DiscretionaryDiscountCaluclations(plot);
            }
            if (SendQuotePage.StageOfWorksPercentage.Count > 0)
            {
                StageOfWorksLoadingPercentageCaluclations(plot);
            }

        }
        //Reading Period Of Cover Loading Table 
        public void PeriodOfCoverLoadingCaluclation(plotrows plot)
        {
            var periodOfCoverLoadingTableRow = -1;
            var periodOfCoverRow = -1;
            //Get Unit Type Loading Table from Structural Premium Table
            for (int i = 0; i < excelTable.Rows.Count; i++)
            {
                if (excelTable.Rows[i][0].ToString().Contains("Period of Cover Loading"))
                {
                    periodOfCoverLoadingTableRow = i;
                    break;
                }
            }
            if (periodOfCoverLoadingTableRow == -1)
                throw new Exception("Unit Type Loading Table Not Found");

            //Get UnitType Row from Unit Type Loading Table
            for (int i = periodOfCoverLoadingTableRow; i < excelTable.Rows.Count; i++)
            {
                if (excelTable.Rows[i][1].ToString() == Premiums.CoverLengthYears.ToString())
                {
                    periodOfCoverRow = i;
                    break;
                }
            }
            if (periodOfCoverRow == -1)
                throw new Exception("Period Of Cover Loading Row Not Found");
            double premiumValueBasedonPeriodOfCoverLoading = Convert.ToDouble(excelTable.Rows[periodOfCoverRow][2]);
            BasePremium = Convert.ToDouble(BasePremium * premiumValueBasedonPeriodOfCoverLoading);
        }
        //Reading Unit Type Loading Table and Caluclate Base Premium
        public void UnitTypeCaluclation(plotrows plot)
        {
            var unitTypeTableRow = -1;
            var unitTypeRow = -1;
            //Get Unit Type Loading Table from Structural Premium Table
            for (int i = 0; i < excelTable.Rows.Count; i++)
            {
                if (excelTable.Rows[i][0].ToString().Contains("Unit Type Loading"))
                {
                    unitTypeTableRow = i;
                    break;
                }
            }
            if (unitTypeTableRow == -1)
                throw new Exception("Unit Type Loading Table Not Found");

            //Get UnitType Row from Unit Type Loading Table
            for (int i = unitTypeTableRow; i < excelTable.Rows.Count; i++)
            {
                if (excelTable.Rows[i][1].ToString() == plot.UnitType.ToString())
                {
                    unitTypeRow = i;
                    break;
                }
            }
            if (unitTypeRow == -1)
                throw new Exception("Unit Type Row Not Found");
            double premiumValueBasedonUnitTypeLoading = Convert.ToDouble(excelTable.Rows[unitTypeRow][RatingColumn]);
            BasePremium = Convert.ToDouble(BasePremium * premiumValueBasedonUnitTypeLoading);
        }
        //Reading Construction Type Loading Table and Caluclate Base Premium
        public void ConstructionTypeCaluclation(plotrows plot)
        {
            var constructionTypeTableRow = -1;
            var constructionTypeRow = -1;
            //Get Construction Type Loading Table from Structural Premium Table
            for (int i = 0; i < excelTable.Rows.Count; i++)
            {
                if (excelTable.Rows[i][0].ToString().Contains("Construction Type Loading"))
                {
                    constructionTypeTableRow = i;
                    break;
                }
            }
            if (constructionTypeTableRow == -1)
                throw new Exception("Construction Type Loading Table Not Found");

            //Get Construction Type  Row from Construction Type Loading Table 
            for (int i = constructionTypeTableRow; i < excelTable.Rows.Count; i++)
            {
                if (excelTable.Rows[i][1].ToString() == plot.ConstructionType.ToString())
                {
                    constructionTypeRow = i;
                    break;
                }
            }
            if (constructionTypeRow == -1)
                throw new Exception("Construction Type Row Not Found");
            double premiumValueBasedOnConstructionType = Convert.ToDouble(excelTable.Rows[constructionTypeRow][RatingColumn]);
            BasePremium = Convert.ToDouble(BasePremium * premiumValueBasedOnConstructionType);
            if (plot.ConstructionType == "New Build")
            {
                NewBuildStagesOfWorkCaluclation(plot);
            }
            if (plot.ConstructionType == "Conversion")
            {
                ConversionStagesOfWorkCaluclation(plot);
            }
        }
        //Reading Construction Type Loading Table and Caluclate Base Premium
        public void ConstructionTypeCaluclationNewbuildandConversion(plotrows plot)
        {
            var constructionTypeTableRow = -1;
            var constructionTypeRow = -1;
            //Get Construction Type Loading Table from Structural Premium Table
            for (int i = 0; i < excelTable.Rows.Count; i++)
            {
                if (excelTable.Rows[i][0].ToString().Contains("Construction Type Loading"))
                {
                    constructionTypeTableRow = i;
                    break;
                }
            }
            if (constructionTypeTableRow == -1)
                throw new Exception("Construction Type Loading Table Not Found");

            //Get Construction Type  Row from Construction Type Loading Table 
            for (int i = constructionTypeTableRow; i < excelTable.Rows.Count; i++)
            {
                if (excelTable.Rows[i][1].ToString().Contains("New Build (with Conversion)"))
                {
                    constructionTypeRow = i;
                    break;
                }
            }
            if (constructionTypeRow == -1)
                throw new Exception("Construction Type Row Not Found");
            double premiumValueBasedOnConstructionType = Convert.ToDouble(excelTable.Rows[constructionTypeRow][RatingColumn]);
            BasePremium = Convert.ToDouble(BasePremium * premiumValueBasedOnConstructionType);
            if (plot.ConstructionType == "New Build")
            {
                NewBuildStagesOfWorkCaluclation(plot);
            }
            if (plot.ConstructionType == "Conversion")
            {
                ConversionStagesOfWorkCaluclation(plot);
            }
        }


        //Reading NewBuild Stages Of Work Loading Table and Caluclate Base Premium
        public void NewBuildStagesOfWorkCaluclation(plotrows plot)
        {
            double premiumValueBasedOnNewBuildSOW = 0.00;
            if (plot.StageOfWork == "No Works Started")
            {
                premiumValueBasedOnNewBuildSOW = Convert.ToDouble(1.00);
            }
            if (plot.StageOfWork == "Foundation/DPC")
            {
                premiumValueBasedOnNewBuildSOW = Convert.ToDouble(1.25);
            }
            if (plot.StageOfWork == "1st Floor")
            {
                premiumValueBasedOnNewBuildSOW = Convert.ToDouble(1.50);
            }
            if (plot.StageOfWork == "Wall Plate Level")
            {
                premiumValueBasedOnNewBuildSOW = Convert.ToDouble(1.75);
            }
            if (plot.StageOfWork == "Roof/Watertight")
            {
                premiumValueBasedOnNewBuildSOW = Convert.ToDouble(2.00);
            }
            BasePremium = Convert.ToDouble(BasePremium * premiumValueBasedOnNewBuildSOW);
        }

        //Reading Conversion Stages Of Work Loading Table and Caluclate Base Premium
        public void ConversionStagesOfWorkCaluclation(plotrows plot)
        {
            double premiumValueBasedOnConversionSOW = 0.00;
            if (plot.StageOfWork == "No Works Started")
            {
                premiumValueBasedOnConversionSOW = Convert.ToDouble(1.00);
            }
            if (plot.StageOfWork == "First Fix")
            {
                premiumValueBasedOnConversionSOW = Convert.ToDouble(1.25);
            }
            if (plot.StageOfWork == "Second Fix")
            {
                premiumValueBasedOnConversionSOW = Convert.ToDouble(1.75);
            }
            BasePremium = Convert.ToDouble(BasePremium * premiumValueBasedOnConversionSOW);
        }
        public void MultiProjectLoadngDiscountCaluclations(plotrows plot)
        {
            var multiProjectLoadngTableRow = -1;
            var multiProjectLoadingRow = -1;
            //Get Basment Loading (Site wide for all products) Table from Structural Premium Table
            for (int i = 0; i < excelTable.Rows.Count; i++)
            {
                if (excelTable.Rows[i][0].ToString().Contains("Multi Project Loading (Site wide for all products)"))
                {
                    multiProjectLoadngTableRow = i;
                    break;
                }
            }
            if (multiProjectLoadngTableRow == -1)
                throw new Exception("Multi Project Loading (Site wide for all products) Table Not Found");

            //Multi Basment Loading (Site wide for all products) Table Row from Multi Project Loading Table 
            for (int i = multiProjectLoadngTableRow; i < excelTable.Rows.Count; i++)
            {
                if (excelTable.Rows[i][1].ToString() == "Yes")
                {
                    multiProjectLoadingRow = i;
                    break;
                }
            }
            if (multiProjectLoadingRow == -1)
                throw new Exception("Multi Project Loading (Site wide for all products) Table Row Not Found");
            double premiumValueBasedOnMultiProjectLoadingValue = Convert.ToDouble(excelTable.Rows[multiProjectLoadingRow][2]);
            Premiums.MultiProjectDiscountInput = SendQuotePage.MultiProjectDiscount[0].Text;
            if (Premiums.MultiProjectDiscountInput == "Yes")
            {
                double multiProjectDiscount = Convert.ToDouble(BasePremium * premiumValueBasedOnMultiProjectLoadingValue);
                BasePremium = Convert.ToDouble(multiProjectDiscount);
            }
        }
        //Discretionary Discount Caluclations
        public void DiscretionaryDiscountCaluclations(plotrows plot)
        {
            string discretionaryDiscountText = SendQuotePage.DiscretionaryDiscount[0].Text;
            string discretionaryDiscountTextValue = discretionaryDiscountText.Replace("%", "");
            Premiums.DiscretionaryDiscountInput = Convert.ToDouble(discretionaryDiscountTextValue);
            if (Premiums.DiscretionaryDiscountInput > 0.00)
            {
                double discretionaryDiscount = Convert.ToDouble(BasePremium * Premiums.DiscretionaryDiscountInput);
                BasePremium = Convert.ToDouble(BasePremium - discretionaryDiscount);
            }
        }

        //Stages oF works Loading Caluclations
        public void StageOfWorksLoadingPercentageCaluclations(plotrows plot)
        {
            string sowPercentageText = SendQuotePage.StageOfWorksPercentage[0].Text;
            string sowPercentageTextValue = sowPercentageText.Replace("%", "");
            Premiums.StageOfWorksLoadingPercentageInput = Convert.ToDouble(sowPercentageTextValue);
            if (Premiums.StageOfWorksLoadingPercentageInput > 0.00)
            {
                double sowPercentage = Convert.ToDouble(BasePremium * Premiums.StageOfWorksLoadingPercentageInput);
                BasePremium = Convert.ToDouble(BasePremium + sowPercentage);
            }
        }

        //Reading Loss Of Rent Premiums Factor Table 
        public void LossOfRentCaluclations(plotrows plot)
        {
            var lossOfRentTable = -1;
            var lossOfRentRatingRow = -1;
            //Get Conversion Stages Of Works Table from Structural Premium Table
            for (int i = 0; i < excelTable.Rows.Count; i++)
            {
                if (excelTable.Rows[i][0].ToString().Contains("Loss of Rent Premium"))
                {
                    lossOfRentTable = i;
                    break;
                }
            }
            if (lossOfRentTable == -1)
                throw new Exception("Loss Of Rent Table Not Found");
            //Get Conversion Stages Of Works Row 
            for (int i = lossOfRentTable; i < excelTable.Rows.Count; i++)
            {
                if (excelTable.Rows[i][1].ToString().Contains("Rating"))
                {
                    lossOfRentRatingRow = i;
                    break;
                }
            }
            if (lossOfRentRatingRow == -1)
                throw new Exception("Loss Of Rent Banding Row Not Found");

            //Get Premium Value From structural Premium Table 
            double lossofrentpremiumValue = Convert.ToDouble(excelTable.Rows[lossOfRentRatingRow + 1][RatingColumn]);
            int plotCount = Convert.ToInt32(Premiums.Plots);
            var annualAmountPerPlot = Convert.ToDouble(Premiums.LossOfGrossProfitInput / plotCount);
            var lossOfRentPremiumValue = (annualAmountPerPlot * Premiums.LossOfGrossProfitNumberOfYearsInput) * lossofrentpremiumValue;
            LossOfRentPayablePremium = Convert.ToDouble(lossOfRentPremiumValue);
            LossOfRentReceivablePremium = Convert.ToDouble(lossOfRentPremiumValue);
            LossOfGrossProfitPremium = Convert.ToDouble(lossOfRentPremiumValue);

        }

        //Reading Waviers Architect Premiums Factor Table 
        public void WaiversArchitectPremiumCaluclations(plotrows plot)
        {
            var waiversArchitectTable = -1;

            //Get Subrogation Waivers - Architect Premium Table from Structural Premium Table
            for (int i = 0; i < excelTable.Rows.Count; i++)
            {
                if (excelTable.Rows[i][0].ToString().Contains("Subrogation Waivers - Architect Premium"))
                {
                    waiversArchitectTable = i;
                    break;
                }
            }
            if (waiversArchitectTable == -1)
                throw new Exception("Subrogation Waivers - Architect Premium Table Not Found");


            //Get CSubrogation Waivers - Architect Premium Row 
            double waiversArchitectPremium = Convert.ToDouble(excelTable.Rows[waiversArchitectTable+2][1]);
            WaviersArchitect = Convert.ToDouble(plot.ReconstructionCostValue * waiversArchitectPremium);
        }
        //Reading Waviers Builder Premiums Factor Table 
        public void WaiversBuilderPremiumCaluclations(plotrows plot)
        {
            var waiversBuilderTable = -1;

            //Get Subrogation Waivers - Builder Premium Table from Structural Premium Table
            for (int i = 0; i < excelTable.Rows.Count; i++)
            {
                if (excelTable.Rows[i][0].ToString().Contains("Subrogation Waivers - Builder Premium"))
                {
                    waiversBuilderTable = i;
                    break;
                }
            }
            if (waiversBuilderTable == -1)
                throw new Exception("Subrogation Waivers - Builder Premium Table Not Found");


            //Get Subrogation Waivers - Builder PremiumRow 
            double waiversBuilderPremium = Convert.ToDouble(excelTable.Rows[waiversBuilderTable + 2][1]);
            WaviersBuilder= Convert.ToDouble(plot.ReconstructionCostValue * waiversBuilderPremium);
        }
        //Reading Waviers Engineer Premiums Factor Table 
        public void WaiversEngineerPremiumCaluclations(plotrows plot)
        {
            var waiversEngineerTable = -1;

            //GetSubrogation Waivers - Engineer Premium Table from Structural Premium Table
            for (int i = 0; i < excelTable.Rows.Count; i++)
            {
                if (excelTable.Rows[i][0].ToString().Contains("Subrogation Waivers - Engineer Premium"))
                {
                    waiversEngineerTable = i;
                    break;
                }
            }
            if (waiversEngineerTable == -1)
                throw new Exception("Subrogation Waivers - Engineer Premium Table Not Found");


            //Get Subrogation Waivers - Builder PremiumRow 
            double waiversEngineerPremium = Convert.ToDouble(excelTable.Rows[waiversEngineerTable + 2][1]);
            WaviersEngineer = Convert.ToDouble(plot.ReconstructionCostValue * waiversEngineerPremium);
        }
        //Reading Waterproof Basement Premiums Factor Table 
        public void WaterProofBasementPremiumCaluclations(plotrows plot)
        {
            var waterProofBasementTable = -1;

            //Get Subrogation Waivers - Builder Premium Table from Structural Premium Table
            for (int i = 0; i < excelTable.Rows.Count; i++)
            {
                if (excelTable.Rows[i][0].ToString().Contains("Waterproofing Basements Premium"))
                {
                    waterProofBasementTable = i;
                    break;
                }
            }
            if (waterProofBasementTable == -1)
                throw new Exception("Waterproofing Basements Premium Table Not Found");


            //Get Subrogation Waivers - Builder PremiumRow 
            double waterProofBasementPremium = Convert.ToDouble(excelTable.Rows[waterProofBasementTable + 2][1]);
            WaterproofBasement = Convert.ToDouble(plot.ReconstructionCostValue * waterProofBasementPremium);
        }



        //Services Caluclations 
        //TA Fee Caluclations   
        //Basic TAFee Caluclations     
        public void TAFeeCaluclations(plotrows plot)
        {
            // Reading TA Fee Table From CommercialHVS Pricing Setup Book           
            var TAFeeTableRow = -1;
            var bandingRow = -1;

            //Get TA Fee Table from Structural Premium Table
            for (int i = 0; i < excelTable.Rows.Count; i++)
            {
                if (excelTable.Rows[i][0].ToString() == "Technical Audit Fee")
                {
                    TAFeeTableRow = i;
                    break;
                }
            }
            if (TAFeeTableRow == -1)
                throw new Exception("TAFee Table Not Found");


            //Get TA FEE Banding Row from TA Fee Table
            for (int i = TAFeeTableRow; i < excelTable.Rows.Count; i++)
            {
                if (excelTable.Rows[i][1].ToString() == "Rate")
                {
                    bandingRow = i;
                    break;
                }
            }
            if (bandingRow == -1)
                throw new Exception("TA Fee Banding Row Not Found");

            //Get TA Fee Value From TAFee Table 
            double taFeeValue = Convert.ToDouble(excelTable.Rows[bandingRow][RatingColumn]);
            TAFee = Convert.ToDouble(plot.ReconstructionCostValue * taFeeValue);
        }
  
       
        public void AtkinsFeeCaluclations()
        {
            // Reading Consumer Code Fee Table From CommercialHVS Pricing Setup Book            
            var atkinsPremiumTableRow = -1;


            //Get Consumer Code Fee per Plot Table from Structural Premium Table
            for (int i = 0; i < excelTable.Rows.Count; i++)
            {
                if (excelTable.Rows[i][0].ToString().Contains("Atkins Premium"))
                {
                    atkinsPremiumTableRow = i;
                    break;
                }
            }
            if (atkinsPremiumTableRow == -1)
                throw new Exception("Consumer Code Fee per Plot Table Not Found");

            double atkinsFeeValue = Convert.ToDouble(excelTable.Rows[atkinsPremiumTableRow + 2][2]);
            AtkinsPremium = Convert.ToDouble(atkinsFeeValue * 12);

        }
    
        public void VerfiyCommercialHVSUIValues()
        {
            var productName = Premiums.eachplotrow.FirstOrDefault().ProductName;
            //Covers Elements 
            //Veridy UI Structural Values
            var uiStructuralFee = $"//th[@class='product-title'][text()='{productName}']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[text()='Structural']//following-sibling::td[@class='right-align'][1]";
            var StructuralFee = Driver.FindElement(By.XPath(uiStructuralFee)).Text.Replace("£", "");
            double UIStructuralFee = Math.Round(Convert.ToDouble(StructuralFee), 2);
            double StructuralFeeDiff;
            var uiStructuralIPTFee = $"//th[@class='product-title'][text()='{productName}']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[text()='Structural']//following-sibling::td[@class='right-align'][2]";
            var StructuralIPTFee = Driver.FindElement(By.XPath(uiStructuralIPTFee)).Text.Replace("£", "");
            double UIStructuralIPTFee = Math.Round(Convert.ToDouble(StructuralIPTFee), 2);
            double StructuralIPTDiff;
            var uiStructuralTotalFee = $"//th[@class='product-title'][text()='{productName}']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[text()='Structural']//following-sibling::th[@class='right-align'][1]";
            var StructuralTotalFee = Driver.FindElement(By.XPath(uiStructuralTotalFee)).Text.Replace("£", "");
            double UIStructuralTotalFee = Math.Round(Convert.ToDouble(StructuralTotalFee), 2);
            double StructuralTotalValue = Math.Round(Convert.ToDouble(UIStructuralFee + UIStructuralIPTFee), 2);
            double StructuralTotalDiff;

            if (UIStructuralFee != Premiums.StructuralFee || UIStructuralIPTFee != Premiums.StructuralIPTFee || UIStructuralTotalFee != StructuralTotalValue)
            {
                StructuralFeeDiff = Convert.ToDouble(Premiums.StructuralFee - UIStructuralFee);
                StructuralIPTDiff = Convert.ToDouble(Premiums.StructuralIPTFee - UIStructuralIPTFee);
                StructuralTotalDiff = Convert.ToDouble(StructuralTotalValue - UIStructuralTotalFee);
                if (StructuralFeeDiff >= -1.01 && StructuralFeeDiff <= 1.01)
                {
                    Premiums.StructuralFee = UIStructuralFee;
                }
                if (StructuralIPTDiff >= -1.01 && StructuralIPTDiff <= 1.01)
                {
                    Premiums.StructuralIPTFee = UIStructuralIPTFee;
                }
                if (StructuralTotalDiff >= -1.01 && StructuralTotalDiff <= 1.01)
                {
                    StructuralTotalValue = UIStructuralTotalFee;
                }

                Assert.AreEqual(UIStructuralFee, Premiums.StructuralFee, $"StructuralFee Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.EstimatedSalePrice}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                Assert.AreEqual(UIStructuralIPTFee, Premiums.StructuralIPTFee, $"StructuralIPTFee Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.EstimatedSalePrice}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                Assert.AreEqual(UIStructuralTotalFee, StructuralTotalValue, $"StructuralTotalFee Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.EstimatedSalePrice}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
            }
            else
            {

                Assert.AreEqual(UIStructuralFee, Premiums.StructuralFee, $"StructuralFee Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.EstimatedSalePrice}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                Assert.AreEqual(UIStructuralIPTFee, Premiums.StructuralIPTFee, $"StructuralIPTFee Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.EstimatedSalePrice}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                Assert.AreEqual(UIStructuralTotalFee, StructuralTotalValue, $"StructuralTotalFee Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.EstimatedSalePrice}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
            }
            if (Premiums.ProductsCovers.Any(x => x.Contains("Contaminated Land")))
            {
                //Veirfy UI Contaminated Land values
                var uiContaminatedLandFee = $"//th[@class='product-title'][text()='{productName}']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[text()='Contaminated Land']//following-sibling::td[@class='right-align'][1]";
                var ContaminatedLandFee = Driver.FindElement(By.XPath(uiContaminatedLandFee)).Text.Replace("£", "");
                double UIContaminatedLandFee = Math.Round(Convert.ToDouble(ContaminatedLandFee), 2);
                double ContaminatedLandFeeDiff;
                var uiContaminatedLandIPTFee = $"//th[@class='product-title'][text()='{productName}']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[text()='Contaminated Land']//following-sibling::td[@class='right-align'][2]";
                var ContaminatedLandIPTFee = Driver.FindElement(By.XPath(uiContaminatedLandIPTFee)).Text.Replace("£", "");
                double UIContaminatedLandIPTFee = Math.Round(Convert.ToDouble(ContaminatedLandIPTFee), 2);
                double ContaminatedLandIPTDiff;
                var uiContaminatedLandTotalFee = $"//th[@class='product-title'][text()='{productName}']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[text()='Contaminated Land']//following-sibling::th[@class='right-align'][1]";
                var ContaminatedLandTotalFee = Driver.FindElement(By.XPath(uiContaminatedLandTotalFee)).Text.Replace("£", "");
                double UIContaminatedLandTotalFee = Math.Round(Convert.ToDouble(ContaminatedLandTotalFee), 2);
                double ContaminatedLandTotalValue = Math.Round(Convert.ToDouble(UIContaminatedLandFee + UIContaminatedLandIPTFee), 2);
                double ContaminatedLandTotalDiff;

                if (UIContaminatedLandFee != Premiums.ContaminatedLandFee || UIContaminatedLandIPTFee != Premiums.ContaminatedLandIPTFee || UIContaminatedLandTotalFee != ContaminatedLandTotalValue)
                {
                    ContaminatedLandFeeDiff = Convert.ToDouble(Premiums.ContaminatedLandFee - UIContaminatedLandFee);
                    ContaminatedLandIPTDiff = Convert.ToDouble(Premiums.ContaminatedLandIPTFee - UIContaminatedLandIPTFee);
                    ContaminatedLandTotalDiff = Convert.ToDouble(ContaminatedLandTotalValue - UIContaminatedLandTotalFee);
                    if (ContaminatedLandFeeDiff >= -1.01 && ContaminatedLandFeeDiff <= 1.01)
                    {
                        Premiums.ContaminatedLandFee = UIContaminatedLandFee;
                    }
                    if (ContaminatedLandIPTDiff >= -1.01 && ContaminatedLandIPTDiff <= 1.01)
                    {
                        Premiums.ContaminatedLandIPTFee = UIContaminatedLandIPTFee;
                    }
                    if (ContaminatedLandTotalDiff >= -1.01 && ContaminatedLandTotalDiff <= 1.01)
                    {
                        ContaminatedLandTotalValue = UIContaminatedLandTotalFee;
                    }

                    Assert.AreEqual(UIContaminatedLandFee, Premiums.ContaminatedLandFee, $"ContaminatedLandFee Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UIContaminatedLandIPTFee, Premiums.ContaminatedLandIPTFee, $"ContaminatedLandIPTFee Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UIContaminatedLandTotalFee, ContaminatedLandTotalValue, $"ContaminatedLandTotalFee Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                }
                else
                {
                    Assert.AreEqual(UIContaminatedLandFee, Premiums.ContaminatedLandFee, $"ContaminatedLandFee Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UIContaminatedLandIPTFee, Premiums.ContaminatedLandIPTFee, $"ContaminatedLandIPTFee Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UIContaminatedLandTotalFee, ContaminatedLandTotalValue, $"ContaminatedLandTotalFee Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                }
            }
            if (Premiums.ProductsCovers.Any(x => x.Contains("Additional Cover for Local Authority Building Control Function")))
            {

                //Verify UI Local Authority Building Control Values
                var uiLocalAuthorityBuildingControlFee = $"//th[@class='product-title'][text()='{productName}']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[text()='Additional Cover for Local Authority Building Control Function']//following-sibling::td[@class='right-align'][1]";
                var LocalAuthorityBuildingControlFee = Driver.FindElement(By.XPath(uiLocalAuthorityBuildingControlFee)).Text.Replace("£", "");
                double UILocalAuthorityBuildingControlFee = Math.Round(Convert.ToDouble(LocalAuthorityBuildingControlFee), 2);
                double LocalAuthorityBuildingControlFeeDiff;

                var uiLocalAuthorityBuildingControlIPTFee = $"//th[@class='product-title'][text()='{productName}']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[text()='Additional Cover for Local Authority Building Control Function']//following-sibling::td[@class='right-align'][2]";
                var LocalAuthorityBuildingControlIPTFee = Driver.FindElement(By.XPath(uiLocalAuthorityBuildingControlIPTFee)).Text.Replace("£", "");
                double UILocalAuthorityBuildingControlIPTFee = Math.Round(Convert.ToDouble(LocalAuthorityBuildingControlIPTFee), 2);
                double LocalAuthorityBuildingControlIPTDiff;

                var uiLocalAuthorityBuildingControlTotalFee = $"//th[@class='product-title'][text()='{productName}']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[text()='Additional Cover for Local Authority Building Control Function']//following-sibling::th[@class='right-align'][1]";
                var LocalAuthorityBuildingControlTotalFee = Driver.FindElement(By.XPath(uiLocalAuthorityBuildingControlTotalFee)).Text.Replace("£", "");
                double UILocalAuthorityBuildingControlTotalFee = Math.Round(Convert.ToDouble(LocalAuthorityBuildingControlTotalFee), 2);
                double LocalAuthorityBuildingControlTotalValue = Math.Round(Convert.ToDouble(UILocalAuthorityBuildingControlFee + UILocalAuthorityBuildingControlIPTFee), 2);
                double LocalAuthorityBuildingControlTotalDiff;

                if (UILocalAuthorityBuildingControlFee != Premiums.LocalAuthorityBuildingControlFee || UILocalAuthorityBuildingControlIPTFee != Premiums.LocalAuthorityBuildingControlIPTFee || UILocalAuthorityBuildingControlTotalFee != LocalAuthorityBuildingControlTotalValue)
                {
                    LocalAuthorityBuildingControlFeeDiff = Convert.ToDouble(Premiums.LocalAuthorityBuildingControlFee - UILocalAuthorityBuildingControlFee);
                    LocalAuthorityBuildingControlIPTDiff = Convert.ToDouble(Premiums.LocalAuthorityBuildingControlIPTFee - UILocalAuthorityBuildingControlIPTFee);
                    LocalAuthorityBuildingControlTotalDiff = Convert.ToDouble(LocalAuthorityBuildingControlTotalValue - UILocalAuthorityBuildingControlTotalFee);
                    if (LocalAuthorityBuildingControlFeeDiff >= -1.01 && LocalAuthorityBuildingControlFeeDiff <= 1.01)
                    {
                        Premiums.LocalAuthorityBuildingControlFee = UILocalAuthorityBuildingControlFee;
                    }
                    if (LocalAuthorityBuildingControlIPTDiff >= -1.01 && LocalAuthorityBuildingControlIPTDiff <= 1.01)
                    {
                        Premiums.LocalAuthorityBuildingControlIPTFee = UILocalAuthorityBuildingControlIPTFee;
                    }
                    if (LocalAuthorityBuildingControlTotalDiff >= -1.01 && LocalAuthorityBuildingControlTotalDiff <= 1.01)
                    {
                        LocalAuthorityBuildingControlTotalValue = UILocalAuthorityBuildingControlTotalFee;
                    }

                    Assert.AreEqual(UILocalAuthorityBuildingControlFee, Premiums.LocalAuthorityBuildingControlFee, $"LocalAuthorityBuildingControlFee Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UILocalAuthorityBuildingControlIPTFee, Premiums.LocalAuthorityBuildingControlIPTFee, $"LocalAuthorityBuildingControlIPTFee Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UILocalAuthorityBuildingControlTotalFee, LocalAuthorityBuildingControlTotalValue, $"LocalAuthorityBuildingControlTotalFee Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                }
                else
                {
                    Assert.AreEqual(UILocalAuthorityBuildingControlFee, Premiums.LocalAuthorityBuildingControlFee, $"LocalAuthorityBuildingControlFee Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UILocalAuthorityBuildingControlIPTFee, Premiums.LocalAuthorityBuildingControlIPTFee, $"LocalAuthorityBuildingControlIPTFee Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UILocalAuthorityBuildingControlTotalFee, LocalAuthorityBuildingControlTotalValue, $"LocalAuthorityBuildingControlTotalFee Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                }

            }
            if (Premiums.ProductsCovers.Any(x => x.Contains("Seepage")))
            {
                //UI Seepage Values
                var uiSeepageFee = $"//th[@class='product-title'][text()='{productName}']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[text()='Seepage']//following-sibling::td[@class='right-align'][1]";
                var SeepageFee = Driver.FindElement(By.XPath(uiSeepageFee)).Text.Replace("£", "");
                double UISeepageFee = Math.Round(Convert.ToDouble(SeepageFee), 2);
                double SeepageFeeDiff;

                var uiSeepageIPTFee = $"//th[@class='product-title'][text()='{productName}']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[text()='Seepage']//following-sibling::td[@class='right-align'][2]";
                var SeepageIPTFee = Driver.FindElement(By.XPath(uiSeepageIPTFee)).Text.Replace("£", "");
                double UISeepageIPTFee = Math.Round(Convert.ToDouble(SeepageIPTFee), 2);
                double SeepageIPTDiff;

                var uiSeepageTotalFee = $"//th[@class='product-title'][text()='{productName}']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[text()='Seepage']//following-sibling::th[@class='right-align'][1]";
                var SeepageTotalFee = Driver.FindElement(By.XPath(uiSeepageTotalFee)).Text.Replace("£", "");
                double UISeepageTotalFee = Math.Round(Convert.ToDouble(SeepageTotalFee), 2);
                double SeepageTotalValue = Math.Round(Convert.ToDouble(UISeepageFee + UISeepageIPTFee), 2);
                double SeepageTotalDiff;

                if (UISeepageFee != Premiums.SeepageFee || UISeepageIPTFee != Premiums.SeepageIPTFee || UISeepageTotalFee != SeepageTotalValue)
                {
                    SeepageFeeDiff = Convert.ToDouble(Premiums.SeepageFee - UISeepageFee);
                    SeepageIPTDiff = Convert.ToDouble(Premiums.SeepageIPTFee - UISeepageIPTFee);
                    SeepageTotalDiff = Convert.ToDouble(SeepageTotalValue - UISeepageTotalFee);
                    if (SeepageFeeDiff >= -1.01 && SeepageFeeDiff <= 1.01)
                    {
                        Premiums.SeepageFee = UISeepageFee;
                    }
                    if (SeepageIPTDiff >= -1.01 && SeepageIPTDiff <= 1.01)
                    {
                        Premiums.SeepageIPTFee = UISeepageIPTFee;
                    }
                    if (SeepageTotalDiff >= -1.01 && SeepageTotalDiff <= 1.01)
                    {
                        SeepageTotalValue = UISeepageTotalFee;
                    }

                    Assert.AreEqual(UISeepageFee, Premiums.SeepageFee, $"SeepageFee Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UISeepageIPTFee, Premiums.SeepageIPTFee, $"SeepageIPTFee Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UISeepageTotalFee, SeepageTotalValue, $"SeepageTotalValue Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                }
                else
                {
                    Assert.AreEqual(UISeepageFee, Premiums.SeepageFee, $"SeepageFee Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UISeepageIPTFee, Premiums.SeepageIPTFee, $"SeepageIPTFee Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UISeepageTotalFee, SeepageTotalValue, $"SeepageTotalValue Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                }
            }
            if (Premiums.ProductsCovers.Any(x => x.Contains("Waivers of Subrogation Rights - Builder")))
            {

                //ui Waivers of Subrogation Rights - Builder values
                var uiWaiversOfSubrogationRightsBuilderFee = $"//th[@class='product-title'][text()='{productName}']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[text()='Waivers of Subrogation Rights - Builder']//following-sibling::td[@class='right-align'][1]";
                var WaiversOfSubrogationRightsBuilderFee = Driver.FindElement(By.XPath(uiWaiversOfSubrogationRightsBuilderFee)).Text.Replace("£", "");
                double UIWaiversOfSubrogationRightsBuilderFee = Math.Round(Convert.ToDouble(WaiversOfSubrogationRightsBuilderFee), 2);
                double WaiversOfSubrogationRightsBuilderFeeDiff;

                var uiWaiversOfSubrogationRightsBuilderIPTFee = $"//th[@class='product-title'][text()='{productName}']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[text()='Waivers of Subrogation Rights - Builder']//following-sibling::td[@class='right-align'][2]";
                var WaiversOfSubrogationRightsBuilderIPTFee = Driver.FindElement(By.XPath(uiWaiversOfSubrogationRightsBuilderIPTFee)).Text.Replace("£", "");
                double UIWaiversOfSubrogationRightsBuilderIPTFee = Math.Round(Convert.ToDouble(WaiversOfSubrogationRightsBuilderIPTFee), 2);
                double WaiversOfSubrogationRightsBuilderIPTDiff;

                var uiWaiversOfSubrogationRightsBuilderTotalFee = $"//th[@class='product-title'][text()='{productName}']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[text()='Waivers of Subrogation Rights - Builder']//following-sibling::th[@class='right-align'][1]";
                var WaiversOfSubrogationRightsBuilderTotalFee = Driver.FindElement(By.XPath(uiWaiversOfSubrogationRightsBuilderTotalFee)).Text.Replace("£", "");
                double UIWaiversOfSubrogationRightsBuilderTotalFee = Math.Round(Convert.ToDouble(WaiversOfSubrogationRightsBuilderTotalFee), 2);
                double WaiversOfSubrogationRightsBuilderTotalValue = Math.Round(Convert.ToDouble(UIWaiversOfSubrogationRightsBuilderFee + UIWaiversOfSubrogationRightsBuilderIPTFee), 2);
                double WaiversOfSubrogationRightsBuilderTotalDiff;

                if (UIWaiversOfSubrogationRightsBuilderFee != Premiums.WaiversOfSubrogationBuilderFee || UIWaiversOfSubrogationRightsBuilderIPTFee != Premiums.WaiversOfSubrogationBuilderIPTFee || UIWaiversOfSubrogationRightsBuilderTotalFee != WaiversOfSubrogationRightsBuilderTotalValue)
                {
                    WaiversOfSubrogationRightsBuilderFeeDiff = Convert.ToDouble(Premiums.WaiversOfSubrogationBuilderFee - UIWaiversOfSubrogationRightsBuilderFee);
                    WaiversOfSubrogationRightsBuilderIPTDiff = Convert.ToDouble(Premiums.WaiversOfSubrogationBuilderIPTFee - UIWaiversOfSubrogationRightsBuilderIPTFee);
                    WaiversOfSubrogationRightsBuilderTotalDiff = Convert.ToDouble(WaiversOfSubrogationRightsBuilderTotalValue - UIWaiversOfSubrogationRightsBuilderTotalFee);
                    if (WaiversOfSubrogationRightsBuilderFeeDiff >= -1.01 && WaiversOfSubrogationRightsBuilderFeeDiff <= 1.01)
                    {
                        Premiums.WaiversOfSubrogationBuilderFee = UIWaiversOfSubrogationRightsBuilderFee;
                    }
                    if (WaiversOfSubrogationRightsBuilderIPTDiff >= -1.01 && WaiversOfSubrogationRightsBuilderIPTDiff <= 1.01)
                    {
                        Premiums.WaiversOfSubrogationBuilderIPTFee = UIWaiversOfSubrogationRightsBuilderIPTFee;
                    }
                    if (WaiversOfSubrogationRightsBuilderTotalDiff >= -1.01 && WaiversOfSubrogationRightsBuilderTotalDiff <= 1.01)
                    {
                        WaiversOfSubrogationRightsBuilderTotalValue = UIWaiversOfSubrogationRightsBuilderTotalFee;
                    }

                    Assert.AreEqual(UIWaiversOfSubrogationRightsBuilderFee, Premiums.WaiversOfSubrogationBuilderFee, $"WaiversOfSubrogationBuilderFee Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UIWaiversOfSubrogationRightsBuilderIPTFee, Premiums.WaiversOfSubrogationBuilderIPTFee, $"WaiversOfSubrogationBuilderIPTFee Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UIWaiversOfSubrogationRightsBuilderTotalFee, WaiversOfSubrogationRightsBuilderTotalValue, $"WaiversOfSubrogationRightsBuilderTotalValue Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                }
                else
                {
                    Assert.AreEqual(UIWaiversOfSubrogationRightsBuilderFee, Premiums.WaiversOfSubrogationBuilderFee, $"WaiversOfSubrogationBuilderFee Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UIWaiversOfSubrogationRightsBuilderIPTFee, Premiums.WaiversOfSubrogationBuilderIPTFee, $"WaiversOfSubrogationBuilderIPTFee Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UIWaiversOfSubrogationRightsBuilderTotalFee, WaiversOfSubrogationRightsBuilderTotalValue, $"WaiversOfSubrogationRightsBuilderTotalValue Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                }
            }
            if (Premiums.ProductsCovers.Any(x => x.Contains("Loss of Gross Profit")))
            {

                //ui Loss of Gross Profit Values
                var uiLossOfGrossProfitFee = $"//th[@class='product-title'][text()='{productName}']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[text()='Loss of Gross Profit']//following-sibling::td[@class='right-align'][1]";
                var LossOfGrossProfitFee = Driver.FindElement(By.XPath(uiLossOfGrossProfitFee)).Text.Replace("£", "");
                double UILossOfGrossProfitFee = Math.Round(Convert.ToDouble(LossOfGrossProfitFee), 2);
                double LossOfGrossProfitFeeDiff;

                var uiLossOfGrossProfitIPTFee = $"//th[@class='product-title'][text()='{productName}']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[text()='Loss of Gross Profit']//following-sibling::td[@class='right-align'][2]";
                var LossOfGrossProfitIPTFee = Driver.FindElement(By.XPath(uiLossOfGrossProfitIPTFee)).Text.Replace("£", "");
                double UILossOfGrossProfitIPTFee = Math.Round(Convert.ToDouble(LossOfGrossProfitIPTFee), 2);
                double LossOfGrossProfitIPTDiff;

                var uiLossOfGrossProfitTotalFee = $"//th[@class='product-title'][text()='{productName}']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[text()='Loss of Gross Profit']//following-sibling::th[@class='right-align'][1]";
                var LossOfGrossProfitTotalFee = Driver.FindElement(By.XPath(uiLossOfGrossProfitTotalFee)).Text.Replace("£", "");
                double UILossOfGrossProfitTotalFee = Math.Round(Convert.ToDouble(LossOfGrossProfitTotalFee), 2);
                double LossOfGrossProfitTotalValue = Math.Round(Convert.ToDouble(UILossOfGrossProfitFee + UILossOfGrossProfitIPTFee), 2);
                double LossOfGrossProfitTotalDiff;

                if (UILossOfGrossProfitFee != Premiums.LossOfGrossProfitFee || UILossOfGrossProfitIPTFee != Premiums.LossOfGrossProfitIPTFee || UILossOfGrossProfitTotalFee != LossOfGrossProfitTotalValue)
                {
                    LossOfGrossProfitFeeDiff = Convert.ToDouble(Premiums.LossOfGrossProfitFee - UILossOfGrossProfitFee);
                    LossOfGrossProfitIPTDiff = Convert.ToDouble(Premiums.LossOfGrossProfitIPTFee - UILossOfGrossProfitIPTFee);
                    LossOfGrossProfitTotalDiff = Convert.ToDouble(LossOfGrossProfitTotalValue - UILossOfGrossProfitTotalFee);
                    if (LossOfGrossProfitFeeDiff >= -1.01 && LossOfGrossProfitFeeDiff <= 1.01)
                    {
                        Premiums.LossOfGrossProfitFee = UILossOfGrossProfitFee;
                    }
                    if (LossOfGrossProfitIPTDiff >= -1.01 && LossOfGrossProfitIPTDiff <= 1.01)
                    {
                        Premiums.LossOfGrossProfitIPTFee = UILossOfGrossProfitIPTFee;
                    }
                    if (LossOfGrossProfitTotalDiff >= -1.01 && LossOfGrossProfitTotalDiff <= 1.01)
                    {
                        LossOfGrossProfitTotalValue = UILossOfGrossProfitTotalFee;
                    }

                    Assert.AreEqual(UILossOfGrossProfitFee, Premiums.LossOfGrossProfitFee, $"LossOfGrossProfitFee Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UILossOfGrossProfitIPTFee, Premiums.LossOfGrossProfitIPTFee, $"LossOfGrossProfitIPTFee Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UILossOfGrossProfitTotalFee, LossOfGrossProfitTotalValue, $"LossOfGrossProfitTotalValue Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                }
                else
                {
                    Assert.AreEqual(UILossOfGrossProfitFee, Premiums.LossOfGrossProfitFee, $"LossOfGrossProfitFee Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UILossOfGrossProfitIPTFee, Premiums.LossOfGrossProfitIPTFee, $"LossOfGrossProfitIPTFee Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UILossOfGrossProfitTotalFee, LossOfGrossProfitTotalValue, $"LossOfGrossProfitTotalValue Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                }
            }
            if (Premiums.ProductsCovers.Any(x => x.Contains("Loss of Rent Payable")))
            {

                //ui Loss of Rent Payable Values
                var uiLossOfRentPayableFee = $"//th[@class='product-title'][text()='{productName}']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[text()='Loss of Rent Payable']//following-sibling::td[@class='right-align'][1]";
                var LossOfRentPayableFee = Driver.FindElement(By.XPath(uiLossOfRentPayableFee)).Text.Replace("£", "");
                double UILossOfRentPayableFee = Math.Round(Convert.ToDouble(LossOfRentPayableFee), 2);
                double LossOfRentPayableFeeDiff;

                var uiLossOfRentPayableIPTFee = $"//th[@class='product-title'][text()='{productName}']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[text()='Loss of Rent Payable']//following-sibling::td[@class='right-align'][2]";
                var LossOfRentPayableIPTFee = Driver.FindElement(By.XPath(uiLossOfRentPayableIPTFee)).Text.Replace("£", "");
                double UILossOfRentPayableIPTFee = Math.Round(Convert.ToDouble(LossOfRentPayableIPTFee), 2);
                double LossOfRentPayableIPTDiff;

                var uiLossOfRentPayableTotalFee = $"//th[@class='product-title'][text()='{productName}']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[text()='Loss of Rent Payable']//following-sibling::th[@class='right-align'][1]";
                var LossOfRentPayableTotalFee = Driver.FindElement(By.XPath(uiLossOfRentPayableTotalFee)).Text.Replace("£", "");
                double UILossOfRentPayableTotalFee = Math.Round(Convert.ToDouble(LossOfRentPayableTotalFee), 2);
                double LossOfRentPayableTotalValue = Math.Round(Convert.ToDouble(UILossOfRentPayableFee + UILossOfRentPayableIPTFee), 2);
                double LossOfRentPayableTotalDiff;

                if (UILossOfRentPayableFee != Premiums.LossOfRentPayableFee || UILossOfRentPayableIPTFee != Premiums.LossOfRentPayableIPTFee || UILossOfRentPayableTotalFee != LossOfRentPayableTotalValue)
                {
                    LossOfRentPayableFeeDiff = Convert.ToDouble(Premiums.LossOfRentPayableFee - UILossOfRentPayableFee);
                    LossOfRentPayableIPTDiff = Convert.ToDouble(Premiums.LossOfRentPayableIPTFee - UILossOfRentPayableIPTFee);
                    LossOfRentPayableTotalDiff = Convert.ToDouble(LossOfRentPayableTotalValue - UILossOfRentPayableTotalFee);
                    if (LossOfRentPayableFeeDiff >= -1.01 && LossOfRentPayableFeeDiff <= 1.01)
                    {
                        Premiums.LossOfRentPayableFee = UILossOfRentPayableFee;
                    }
                    if (LossOfRentPayableIPTDiff >= -1.01 && LossOfRentPayableIPTDiff <= 1.01)
                    {
                        Premiums.LossOfRentPayableIPTFee = UILossOfRentPayableIPTFee;
                    }
                    if (LossOfRentPayableTotalDiff >= -1.01 && LossOfRentPayableTotalDiff <= 1.01)
                    {
                        LossOfRentPayableTotalValue = UILossOfRentPayableTotalFee;
                    }

                    Assert.AreEqual(UILossOfRentPayableFee, Premiums.LossOfRentPayableFee, $"LossOfRentPayableFee Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UILossOfRentPayableIPTFee, Premiums.LossOfRentPayableIPTFee, $"LossOfRentPayableIPTFee Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UILossOfRentPayableTotalFee, LossOfRentPayableTotalValue, $"LossOfRentPayableTotalValue Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                }
                else
                {
                    Assert.AreEqual(UILossOfRentPayableFee, Premiums.LossOfRentPayableFee, $"LossOfRentPayableFee Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UILossOfRentPayableIPTFee, Premiums.LossOfRentPayableIPTFee, $"LossOfRentPayableIPTFee Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UILossOfRentPayableTotalFee, LossOfRentPayableTotalValue, $"LossOfRentPayableTotalValue Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                }
            }

            if (Premiums.ProductsCovers.Any(x => x.Contains("Loss of Rent Receivable")))
            {

                //ui Loss of Rent Receivable Values
                var uiLossofRentReceivableFee = $"//th[@class='product-title'][text()='{productName}']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[text()='Loss of Rent Receivable']//following-sibling::td[@class='right-align'][1]";
                var LossofRentReceivableFee = Driver.FindElement(By.XPath(uiLossofRentReceivableFee)).Text.Replace("£", "");
                double UILossofRentReceivableFee = Math.Round(Convert.ToDouble(LossofRentReceivableFee), 2);
                double LossofRentReceivableFeeDiff;

                var uiLossofRentReceivableIPTFee = $"//th[@class='product-title'][text()='{productName}']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[text()='Loss of Rent Receivable']//following-sibling::td[@class='right-align'][2]";
                var LossofRentReceivableIPTFee = Driver.FindElement(By.XPath(uiLossofRentReceivableIPTFee)).Text.Replace("£", "");
                double UILossofRentReceivableIPTFee = Math.Round(Convert.ToDouble(LossofRentReceivableIPTFee), 2);
                double LossofRentReceivableIPTDiff;

                var uiLossofRentReceivableTotalFee = $"//th[@class='product-title'][text()='{productName}']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[text()='Loss of Rent Receivable']//following-sibling::th[@class='right-align'][1]";
                var LossofRentReceivableTotalFee = Driver.FindElement(By.XPath(uiLossofRentReceivableTotalFee)).Text.Replace("£", "");
                double UILossofRentReceivableTotalFee = Math.Round(Convert.ToDouble(LossofRentReceivableTotalFee), 2);
                double LossofRentReceivableTotalValue = Math.Round(Convert.ToDouble(UILossofRentReceivableFee + UILossofRentReceivableIPTFee), 2);
                double LossofRentReceivableTotalDiff;

                if (UILossofRentReceivableFee != Premiums.LossOfRentReceivableFee || UILossofRentReceivableIPTFee != Premiums.LossOfRentReceivableIPTFee || UILossofRentReceivableTotalFee != LossofRentReceivableTotalValue)
                {
                    LossofRentReceivableFeeDiff = Convert.ToDouble(Premiums.LossOfRentReceivableFee - UILossofRentReceivableFee);
                    LossofRentReceivableIPTDiff = Convert.ToDouble(Premiums.LossOfRentReceivableIPTFee - UILossofRentReceivableIPTFee);
                    LossofRentReceivableTotalDiff = Convert.ToDouble(LossofRentReceivableTotalValue - UILossofRentReceivableTotalFee);

                    if (LossofRentReceivableFeeDiff >= -1.01 && LossofRentReceivableFeeDiff <= 1.01)
                    {
                        Premiums.LossOfRentReceivableFee = UILossofRentReceivableFee;
                    }
                    if (LossofRentReceivableIPTDiff >= -1.01 && LossofRentReceivableIPTDiff <= 1.01)
                    {
                        Premiums.LossOfRentReceivableIPTFee = UILossofRentReceivableIPTFee;
                    }
                    if (LossofRentReceivableTotalDiff >= -1.01 && LossofRentReceivableTotalDiff <= 1.01)
                    {
                        LossofRentReceivableTotalValue = UILossofRentReceivableTotalFee;
                    }

                    Assert.AreEqual(UILossofRentReceivableFee, Premiums.LossOfRentReceivableFee, $"LossOfRentReceivableFee Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UILossofRentReceivableIPTFee, Premiums.LossOfRentReceivableIPTFee, $"LossOfRentReceivableIPTFee Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UILossofRentReceivableTotalFee, LossofRentReceivableTotalValue, $"LossofRentReceivableTotalValue Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                }
                else
                {
                    Assert.AreEqual(UILossofRentReceivableFee, Premiums.LossOfRentReceivableFee, $"LossOfRentReceivableFee Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UILossofRentReceivableIPTFee, Premiums.LossOfRentReceivableIPTFee, $"LossOfRentReceivableIPTFee Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UILossofRentReceivableTotalFee, LossofRentReceivableTotalValue, $"LossofRentReceivableTotalValue Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                }
            }
            if (Premiums.ProductsCovers.Any(x => x.Contains("Waivers of Subrogation Rights - Engineer")))
            {

                //Verify UI Waivers of Subrogation Rights - Builder values
                var uiWaiversOfSubrogationRightsEngineerFee = $"//th[@class='product-title'][text()='{productName}']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[text()='Waivers of Subrogation Rights - Engineer']//following-sibling::td[@class='right-align'][1]";
                var WaiversOfSubrogationRightsEngineerFee = Driver.FindElement(By.XPath(uiWaiversOfSubrogationRightsEngineerFee)).Text.Replace("£", "");
                double UIWaiversOfSubrogationRightsEngineerFee = Math.Round(Convert.ToDouble(WaiversOfSubrogationRightsEngineerFee), 2);
                double WaiversOfSubrogationRightsEngineerFeeDiff;

                var uiWaiversOfSubrogationRightsEngineerIPTFee = $"//th[@class='product-title'][text()='{productName}']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[text()='Waivers of Subrogation Rights - Engineer']//following-sibling::td[@class='right-align'][2]";
                var WaiversOfSubrogationRightsEngineerIPTFee = Driver.FindElement(By.XPath(uiWaiversOfSubrogationRightsEngineerIPTFee)).Text.Replace("£", "");
                double UIWaiversOfSubrogationRightsEngineerIPTFee = Math.Round(Convert.ToDouble(WaiversOfSubrogationRightsEngineerIPTFee), 2);
                double WaiversOfSubrogationRightsEngineerIPTDiff;

                var uiWaiversOfSubrogationRightsEngineerTotalFee = $"//th[@class='product-title'][text()='{productName}']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[text()='Waivers of Subrogation Rights - Engineer']//following-sibling::th[@class='right-align'][1]";
                var WaiversOfSubrogationRightsEngineerTotalFee = Driver.FindElement(By.XPath(uiWaiversOfSubrogationRightsEngineerTotalFee)).Text.Replace("£", "");
                double UIWaiversOfSubrogationRightsEngineerTotalFee = Math.Round(Convert.ToDouble(WaiversOfSubrogationRightsEngineerTotalFee), 2);
                double WaiversOfSubrogationRightsEngineerTotalValue = Math.Round(Convert.ToDouble(UIWaiversOfSubrogationRightsEngineerFee + UIWaiversOfSubrogationRightsEngineerIPTFee), 2);
                double WaiversOfSubrogationRightsEngineerTotalDiff;

                if (UIWaiversOfSubrogationRightsEngineerFee != Premiums.WaiversOfSubrogationEngineerFee || UIWaiversOfSubrogationRightsEngineerIPTFee != Premiums.WaiversOfSubrogationEngineerIPTFee || UIWaiversOfSubrogationRightsEngineerTotalFee != WaiversOfSubrogationRightsEngineerTotalValue)
                {
                    WaiversOfSubrogationRightsEngineerFeeDiff = Convert.ToDouble(Premiums.WaiversOfSubrogationEngineerFee - UIWaiversOfSubrogationRightsEngineerFee);
                    WaiversOfSubrogationRightsEngineerIPTDiff = Convert.ToDouble(Premiums.WaiversOfSubrogationEngineerIPTFee - UIWaiversOfSubrogationRightsEngineerIPTFee);
                    WaiversOfSubrogationRightsEngineerTotalDiff = Convert.ToDouble(WaiversOfSubrogationRightsEngineerTotalValue - UIWaiversOfSubrogationRightsEngineerTotalFee);
                    if (WaiversOfSubrogationRightsEngineerFeeDiff >= -1.01 && WaiversOfSubrogationRightsEngineerFeeDiff <= 1.01)
                    {
                        Premiums.WaiversOfSubrogationEngineerFee = UIWaiversOfSubrogationRightsEngineerFee;
                    }

                    if (WaiversOfSubrogationRightsEngineerIPTDiff >= -1.01 && WaiversOfSubrogationRightsEngineerIPTDiff <= 1.01)
                    {
                        Premiums.WaiversOfSubrogationEngineerIPTFee = UIWaiversOfSubrogationRightsEngineerIPTFee;
                    }
                    if (WaiversOfSubrogationRightsEngineerTotalDiff >= -1.01 && WaiversOfSubrogationRightsEngineerTotalDiff <= 1.01)
                    {
                        WaiversOfSubrogationRightsEngineerTotalValue = UIWaiversOfSubrogationRightsEngineerTotalFee;
                    }

                    Assert.AreEqual(UIWaiversOfSubrogationRightsEngineerFee, Premiums.WaiversOfSubrogationEngineerFee, $"WaiversOfSubrogationEngineerFee Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UIWaiversOfSubrogationRightsEngineerIPTFee, Premiums.WaiversOfSubrogationEngineerIPTFee, $"WaiversOfSubrogationEngineerIPTFee Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UIWaiversOfSubrogationRightsEngineerTotalFee, WaiversOfSubrogationRightsEngineerTotalValue, $"WaiversOfSubrogationRightsEngineerTotalValue Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                }
                else
                {
                    Assert.AreEqual(UIWaiversOfSubrogationRightsEngineerFee, Premiums.WaiversOfSubrogationEngineerFee, $"WaiversOfSubrogationEngineerFee Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UIWaiversOfSubrogationRightsEngineerIPTFee, Premiums.WaiversOfSubrogationEngineerIPTFee, $"WaiversOfSubrogationEngineerIPTFee Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UIWaiversOfSubrogationRightsEngineerTotalFee, WaiversOfSubrogationRightsEngineerTotalValue, $"WaiversOfSubrogationRightsEngineerTotalValue Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                }
            }
            if (Premiums.ProductsCovers.Any(x => x.Contains("Waivers of Subrogation Rights - Architect")))
            {

                //Verify UI Waivers of Subrogation Rights - Builder values
                var uiWaiversOfSubrogationRightsArchitectFee = $"//th[@class='product-title'][text()='{productName}']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[text()='Waivers of Subrogation Rights - Architect']//following-sibling::td[@class='right-align'][1]";
                var WaiversOfSubrogationRightsArchitectFee = Driver.FindElement(By.XPath(uiWaiversOfSubrogationRightsArchitectFee)).Text.Replace("£", "");
                double UIWaiversOfSubrogationRightsArchitectFee = Math.Round(Convert.ToDouble(WaiversOfSubrogationRightsArchitectFee), 2);
                double WaiversOfSubrogationRightsArchitectFeeDiff;

                var uiWaiversOfSubrogationRightsArchitectIPTFee = $"//th[@class='product-title'][text()='{productName}']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[text()='Waivers of Subrogation Rights - Architect']//following-sibling::td[@class='right-align'][2]";
                var WaiversOfSubrogationRightsArchitectIPTFee = Driver.FindElement(By.XPath(uiWaiversOfSubrogationRightsArchitectIPTFee)).Text.Replace("£", "");
                double UIWaiversOfSubrogationRightsArchitectIPTFee = Math.Round(Convert.ToDouble(WaiversOfSubrogationRightsArchitectIPTFee), 2);
                double WaiversOfSubrogationRightsArchitectIPTDiff;

                var uiWaiversOfSubrogationRightsArchitectTotalFee = $"//th[@class='product-title'][text()='{productName}']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[text()='Waivers of Subrogation Rights - Architect']//following-sibling::th[@class='right-align'][1]";
                var WaiversOfSubrogationRightsArchitectTotalFee = Driver.FindElement(By.XPath(uiWaiversOfSubrogationRightsArchitectTotalFee)).Text.Replace("£", "");
                double UIWaiversOfSubrogationRightsArchitectTotalFee = Math.Round(Convert.ToDouble(WaiversOfSubrogationRightsArchitectTotalFee), 2);
                double WaiversOfSubrogationRightsArchitectTotalValue = Math.Round(Convert.ToDouble(UIWaiversOfSubrogationRightsArchitectFee + UIWaiversOfSubrogationRightsArchitectIPTFee), 2);
                double WaiversOfSubrogationRightsArchitectTotalDiff;

                if (UIWaiversOfSubrogationRightsArchitectFee != Premiums.WaiversOfSubrogationArchitectFee || UIWaiversOfSubrogationRightsArchitectIPTFee != Premiums.WaiversOfSubrogationArchitectIPTFee || UIWaiversOfSubrogationRightsArchitectTotalFee != WaiversOfSubrogationRightsArchitectTotalValue)
                {
                    WaiversOfSubrogationRightsArchitectFeeDiff = Convert.ToDouble(Premiums.WaiversOfSubrogationArchitectFee - UIWaiversOfSubrogationRightsArchitectFee);
                    WaiversOfSubrogationRightsArchitectIPTDiff = Convert.ToDouble(Premiums.WaiversOfSubrogationArchitectIPTFee - UIWaiversOfSubrogationRightsArchitectIPTFee);
                    WaiversOfSubrogationRightsArchitectTotalDiff = Convert.ToDouble(WaiversOfSubrogationRightsArchitectTotalValue - UIWaiversOfSubrogationRightsArchitectTotalFee);
                    if (WaiversOfSubrogationRightsArchitectFeeDiff >= -1.01 && WaiversOfSubrogationRightsArchitectFeeDiff <= 1.01)
                    {
                        Premiums.WaiversOfSubrogationArchitectFee = UIWaiversOfSubrogationRightsArchitectFee;
                    }

                    if (WaiversOfSubrogationRightsArchitectIPTDiff >= -1.01 && WaiversOfSubrogationRightsArchitectIPTDiff <= 1.01)
                    {
                        Premiums.WaiversOfSubrogationArchitectIPTFee = UIWaiversOfSubrogationRightsArchitectIPTFee;
                    }
                    if (WaiversOfSubrogationRightsArchitectTotalDiff >= -1.01 && WaiversOfSubrogationRightsArchitectTotalDiff <= 1.01)
                    {
                        WaiversOfSubrogationRightsArchitectTotalValue = UIWaiversOfSubrogationRightsArchitectTotalFee;
                    }

                    Assert.AreEqual(UIWaiversOfSubrogationRightsArchitectFee, Premiums.WaiversOfSubrogationArchitectFee, $"WaiversOfSubrogationArchitectFee Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UIWaiversOfSubrogationRightsArchitectIPTFee, Premiums.WaiversOfSubrogationArchitectIPTFee, $"WaiversOfSubrogationArchitectIPTFee Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UIWaiversOfSubrogationRightsArchitectTotalFee, WaiversOfSubrogationRightsArchitectTotalValue, $"WaiversOfSubrogationRightsArchitectTotalValue Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                }
                else
                {
                    Assert.AreEqual(UIWaiversOfSubrogationRightsArchitectFee, Premiums.WaiversOfSubrogationArchitectFee, $"WaiversOfSubrogationArchitectFee Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UIWaiversOfSubrogationRightsArchitectIPTFee, Premiums.WaiversOfSubrogationArchitectIPTFee, $"WaiversOfSubrogationArchitectIPTFee Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UIWaiversOfSubrogationRightsArchitectTotalFee, WaiversOfSubrogationRightsArchitectTotalValue, $"WaiversOfSubrogationRightsArchitectTotalValue Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                }
            }

            //Services Elements  and  Fee values  
            //Technical Audit Fee values  
            if (Premiums.ProductServices.Any(x => x.Contains("Technical Audit Fee")))
            {


                var uiTechnicalAuditFee = $"//th[@class='product-title'][text()='{productName}']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[text()='Technical Audit Fee']//following-sibling::td[@class='right-align'][1]";
                var TechnicalAuditFee = Driver.FindElement(By.XPath(uiTechnicalAuditFee)).Text.Replace("£", "");
                double UITechnicalAuditFee = Math.Round(Convert.ToDouble(TechnicalAuditFee), 2);
                double TechnicalAuditFeeDiff;

                var uiTechnicalAuditVATFee = $"//th[@class='product-title'][text()='{productName}']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[text()='Technical Audit Fee']//following-sibling::td[@class='right-align'][2]";
                var TechnicalAuditVATFee = Driver.FindElement(By.XPath(uiTechnicalAuditVATFee)).Text.Replace("£", "");
                double UITechnicalAuditVATFee = Math.Round(Convert.ToDouble(TechnicalAuditVATFee), 2);
                double TechnicalAuditVATDiff;

                var uiTechnicalAuditTotalFee = $"//th[@class='product-title'][text()='{productName}']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[text()='Technical Audit Fee']//following-sibling::th[@class='right-align'][1]";
                var TechnicalAuditTotalFee = Driver.FindElement(By.XPath(uiTechnicalAuditTotalFee)).Text.Replace("£", "");
                double UITechnicalAuditTotalFee = Math.Round(Convert.ToDouble(TechnicalAuditTotalFee), 2);
                double TechnicalAuditTotalValue = Math.Round(Convert.ToDouble(UITechnicalAuditFee + UITechnicalAuditVATFee), 2);
                double TechnicalAuditTotalDiff;

                if (UITechnicalAuditFee != Premiums.TechnicalAuditFee || UITechnicalAuditVATFee != Premiums.TechnicalAuditVATFee || UITechnicalAuditTotalFee != TechnicalAuditTotalValue)
                {
                    TechnicalAuditFeeDiff = Convert.ToDouble(Premiums.TechnicalAuditFee - UITechnicalAuditFee);
                    TechnicalAuditVATDiff = Convert.ToDouble(Premiums.TechnicalAuditVATFee - UITechnicalAuditVATFee);
                    TechnicalAuditTotalDiff = Convert.ToDouble(TechnicalAuditTotalValue - UITechnicalAuditTotalFee);
                    if (TechnicalAuditFeeDiff >= -1.01 && TechnicalAuditFeeDiff <= 1.01)
                    {
                        Premiums.TechnicalAuditFee = UITechnicalAuditFee;
                    }

                    if (TechnicalAuditVATDiff >= -1.01 && TechnicalAuditVATDiff <= 1.01)
                    {
                        Premiums.TechnicalAuditVATFee = UITechnicalAuditVATFee;
                    }
                    if (TechnicalAuditTotalDiff >= -1.01 && TechnicalAuditTotalDiff <= 1.01)
                    {
                        TechnicalAuditTotalValue = UITechnicalAuditTotalFee;
                    }

                    Assert.AreEqual(UITechnicalAuditFee, Premiums.TechnicalAuditFee, $"TechnicalAuditFee Services Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UITechnicalAuditVATFee, Premiums.TechnicalAuditVATFee, $"TechnicalAuditVATFee Services Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UITechnicalAuditTotalFee, TechnicalAuditTotalValue, $"TechnicalAuditTotalValue Services Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                }
                else
                {
                    Assert.AreEqual(UITechnicalAuditFee, Premiums.TechnicalAuditFee, $"TechnicalAuditFee Services Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UITechnicalAuditVATFee, Premiums.TechnicalAuditVATFee, $"TechnicalAuditVATFee Services Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UITechnicalAuditTotalFee, TechnicalAuditTotalValue, $"TechnicalAuditTotalValue Services Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");

                }
            }


            //Administrator Fee values    
            if (Premiums.ProductServices.Any(x => x.Contains("Administration Fee")))
            {

                var uiAdministrationFee = $"//th[@class='product-title'][text()='{productName}']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[text()='Administration Fee']//following-sibling::td[@class='right-align'][1]";
                var AdministrationFee = Driver.FindElement(By.XPath(uiAdministrationFee)).Text.Replace("£", "");
                double UIAdministrationFee = Math.Round(Convert.ToDouble(AdministrationFee), 2);
                double AdministrationFeeDiff;

                var uiAdministrationVATFee = $"//th[@class='product-title'][text()='{productName}']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[text()='Administration Fee']//following-sibling::td[@class='right-align'][2]";
                var AdministrationVATFee = Driver.FindElement(By.XPath(uiAdministrationVATFee)).Text.Replace("£", "");
                double UIAdministrationVATFee = Math.Round(Convert.ToDouble(AdministrationVATFee), 2);
                double AdministrationVATDiff;

                var uiAdministrationTotalFee = $"//th[@class='product-title'][text()='{productName}']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[text()='Administration Fee']//following-sibling::th[@class='right-align'][1]";
                var AdministrationTotalFee = Driver.FindElement(By.XPath(uiAdministrationTotalFee)).Text.Replace("£", "");
                double UIAdministrationTotalFee = Math.Round(Convert.ToDouble(AdministrationTotalFee), 2);
                double AdministrationTotalValue = Math.Round(Convert.ToDouble(UIAdministrationFee + UIAdministrationVATFee), 2);
                double AdministrationTotalDiff;

                if (UIAdministrationFee != Premiums.AdministrationFee || UIAdministrationVATFee != Premiums.AdministrationVATFee || UIAdministrationTotalFee != AdministrationTotalValue)
                {
                    AdministrationFeeDiff = Convert.ToDouble(Premiums.AdministrationFee - UIAdministrationFee);
                    AdministrationVATDiff = Convert.ToDouble(Premiums.AdministrationVATFee - UIAdministrationVATFee);
                    AdministrationTotalDiff = Convert.ToDouble(AdministrationTotalValue - UIAdministrationTotalFee);
                    if (AdministrationFeeDiff >= -1.01 && AdministrationFeeDiff <= 1.01)
                    {
                        Premiums.AdministrationFee = UIAdministrationFee;
                    }

                    if (AdministrationVATDiff >= -1.01 && AdministrationVATDiff <= 1.01)
                    {
                        Premiums.AdministrationVATFee = UIAdministrationVATFee;
                    }
                    if (AdministrationTotalDiff >= 1.01 && AdministrationTotalDiff <= 1.01)
                    {
                        AdministrationTotalValue = UIAdministrationTotalFee;
                    }

                    Assert.AreEqual(UIAdministrationFee, Premiums.AdministrationFee, $"AdministrationFee Services Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UIAdministrationVATFee, Premiums.AdministrationVATFee, $"AdministrationVATFee Services Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UIAdministrationTotalFee, AdministrationTotalValue, $"AdministrationTotalValue Services Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                }
                else
                {
                    Assert.AreEqual(UIAdministrationFee, Premiums.AdministrationFee, $"AdministrationFee Services Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UIAdministrationVATFee, Premiums.AdministrationVATFee, $"AdministrationVATFee Services Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UIAdministrationTotalFee, AdministrationTotalValue, $"AdministrationTotalValue Services Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                }
            }
            //Atkins Fee values    
            if (Premiums.ProductServices.Any(x => x.Contains("Atkins Fee")))
            {

                var uiAtkinsFee = $"//th[@class='product-title'][text()='{productName}']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[text()='Atkins Fee']//following-sibling::td[@class='right-align'][1]";
                var AtkinsFee = Driver.FindElement(By.XPath(uiAtkinsFee)).Text.Replace("£", "");
                double UIAtkinsFee = Math.Round(Convert.ToDouble(AtkinsFee), 2);
                double AtkinsFeeDiff;

                var uiAtkinsVATFee = $"//th[@class='product-title'][text()='{productName}']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[text()='Atkins Fee']//following-sibling::td[@class='right-align'][2]";
                var AtkinsVATFee = Driver.FindElement(By.XPath(uiAtkinsVATFee)).Text.Replace("£", "");
                double UIAtkinsVATFee = Math.Round(Convert.ToDouble(AtkinsVATFee), 2);
                double AtkinsVATDiff;

                var uiAtkinsTotalFee = $"//th[@class='product-title'][text()='{productName}']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[text()='Atkins Fee']//following-sibling::th[@class='right-align'][1]";
                var AtkinsTotalFee = Driver.FindElement(By.XPath(uiAtkinsTotalFee)).Text.Replace("£", "");
                double UIAtkinsTotalFee = Math.Round(Convert.ToDouble(AtkinsTotalFee), 2);
                double AtkinsTotalValue = Math.Round(Convert.ToDouble(UIAtkinsFee + UIAtkinsVATFee), 2);
                double AtkinsTotalDiff;

                if (UIAtkinsFee != Premiums.AtkinsFee || UIAtkinsVATFee != Premiums.AtkinsVATFee || UIAtkinsTotalFee != AtkinsTotalValue)
                {
                    AtkinsFeeDiff = Convert.ToDouble(Premiums.AtkinsFee - UIAtkinsFee);
                    AtkinsVATDiff = Convert.ToDouble(Premiums.AtkinsVATFee - UIAtkinsVATFee);
                    AtkinsTotalDiff = Convert.ToDouble(AtkinsTotalValue - UIAtkinsTotalFee);
                    if (AtkinsFeeDiff >= -1.01 && AtkinsFeeDiff <= 1.01)
                    {
                        Premiums.AtkinsFee = UIAtkinsFee;
                    }

                    if (AtkinsVATDiff >= -1.01 && AtkinsVATDiff <= 1.01)
                    {
                        Premiums.AtkinsVATFee = UIAtkinsVATFee;
                    }
                    if (AtkinsTotalDiff >= -1.01 && AtkinsTotalDiff <= 1.01)
                    {
                        AtkinsTotalValue = UIAtkinsTotalFee;
                    }

                    Assert.AreEqual(UIAtkinsFee, Premiums.AtkinsFee, $"AtkinsFee Services Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.EstimatedSalePrice}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UIAtkinsVATFee, Premiums.AtkinsVATFee, $"AtkinsVATFee Services Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.EstimatedSalePrice}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UIAtkinsTotalFee, AtkinsTotalValue, $"AtkinsTotalValue Services Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.EstimatedSalePrice}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                }
                else
                {
                    Assert.AreEqual(UIAtkinsFee, Premiums.AtkinsFee, $"AtkinsFee Services Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.EstimatedSalePrice}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UIAtkinsVATFee, Premiums.AtkinsVATFee, $"AtkinsVATFee Services Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.EstimatedSalePrice}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UIAtkinsTotalFee, AtkinsTotalValue, $"AtkinsTotalValue Services Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.EstimatedSalePrice}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                }
            }
            //Cancellation Fee values  
            if (Premiums.ProductServices.Any(x => x.Contains("Cancellation Fee")))
            {


                var uiCancellationFee = $"//th[@class='product-title'][text()='{productName}']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[text()='Cancellation Fee']//following-sibling::td[@class='right-align'][1]";
                var CancellationFee = Driver.FindElement(By.XPath(uiCancellationFee)).Text.Replace("£", "");
                double UICancellationFee = Math.Round(Convert.ToDouble(CancellationFee), 2);
                double CancellationFeeDiff;

                var uiCancellationVATFee = $"//th[@class='product-title'][text()='{productName}']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[text()='Cancellation Fee']//following-sibling::td[@class='right-align'][2]";
                var CancellationVATFee = Driver.FindElement(By.XPath(uiCancellationVATFee)).Text.Replace("£", "");
                double UICancellationVATFee = Math.Round(Convert.ToDouble(CancellationVATFee), 2);
                double CancellationVATDiff;

                var uiCancellationTotalFee = $"//th[@class='product-title'][text()='{productName}']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[text()='Cancellation Fee']//following-sibling::td[@class='right-align'][1]";
                var CancellationTotalFee = Driver.FindElement(By.XPath(uiCancellationTotalFee)).Text.Replace("£", "");
                double UICancellationTotalFee = Math.Round(Convert.ToDouble(CancellationTotalFee), 2);
                double CancellationTotalValue = Math.Round(Convert.ToDouble(UICancellationFee + UICancellationVATFee), 2);
                double CancellationTotalDiff;


                if (UICancellationFee != Premiums.CancellationFee || UICancellationVATFee != Premiums.CancellationVATFee || UICancellationTotalFee != CancellationTotalValue)
                {
                    CancellationFeeDiff = Convert.ToDouble(Premiums.CancellationFee - UICancellationFee);
                    CancellationVATDiff = Convert.ToDouble(Premiums.CancellationVATFee - UICancellationVATFee);
                    CancellationTotalDiff = Convert.ToDouble(CancellationTotalValue - UICancellationTotalFee);
                    if (CancellationFeeDiff >= -1.01 && CancellationFeeDiff <= 1.01)
                    {
                        Premiums.CancellationFee = UICancellationFee;
                    }

                    if (CancellationVATDiff >= -1.01 && CancellationVATDiff <= 1.01)
                    {
                        Premiums.CancellationVATFee = UICancellationVATFee;
                    }
                    if (CancellationTotalDiff >= -1.01 && CancellationTotalDiff <= 1.01)
                    {
                        CancellationTotalValue = UICancellationTotalFee;
                    }

                    Assert.AreEqual(UICancellationFee, Premiums.CancellationFee, $"CancellationFee Services Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UICancellationVATFee, Premiums.CancellationVATFee, $"CancellationVATFee Services Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UICancellationTotalFee, CancellationTotalValue, $"CancellationTotalValue Services Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                }
                else
                {
                    Assert.AreEqual(UICancellationFee, Premiums.CancellationFee, $"CancellationFee Services Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UICancellationVATFee, Premiums.CancellationVATFee, $"CancellationVATFee Services Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UICancellationTotalFee, CancellationTotalValue, $"CancellationTotalValue Services Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                }
            }        
        }

        public DataTable ReadCommercialHVSMasterPremiumsPriceData(string fileName)
        {

            var result = new DataSet();                                                                             //Set the First Row as Column Name
            using (var stream = File.Open(fileName, FileMode.Open, FileAccess.Read))
            using (var excelReader = ExcelReaderFactory.CreateOpenXmlReader(stream)) //.xlsx
            {
                //Return as DataSet
                result = excelReader.AsDataSet(new ExcelDataSetConfiguration()
                {
                    ConfigureDataTable = (_) => new ExcelDataTableConfiguration()
                    {
                        UseHeaderRow = true
                    }
                });
            }
            //Get all the Tables
            DataTableCollection table = result.Tables;
            //Reading Last Sheet From Product Pricing Setup 
            DataTable resultTable = table["Commercial - HVS (1)"];
            return resultTable;
        }
    }
}