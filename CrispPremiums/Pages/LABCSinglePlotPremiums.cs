﻿using OpenQA.Selenium;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.PageObjects;
using RegressionPacks.Support;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RegressionPacks.Pages
{
    public class LABCSinglePlotPremiums : Support.Pages
    {
        public IWebDriver wdriver;
        public string RatingValue;
        string masterplotdata;

        public LABCSinglePlotPremiums(ISearchContext driver)
        {
            PageFactory.InitElements(driver, this);
            wdriver = (RemoteWebDriver)driver;
        }
        public void LABCNewhomesPremiums()
        {
            masterplotdata = @"\\tpgfile\NewCompany\IT\Department\QA Team\Current Projects\CRISP\CrispAutomation\CrispData\Premiums\NHPremiums\NH-MasterPlotData.xlsx";
           var dtExcel =  ExcelPlay.ExcelToDataTable(masterplotdata);
            for (int i = 0; i < dtExcel.Rows.Count; i++)
            {
                ExcelPlay.CreateSpreadsheet(dtExcel.Rows[i]);
                CreateAndSendQuoteToVerifyPremiums();
            }
        }
        public void LABCSocialHousingPremiums()
        {
            masterplotdata = @"\\tpgfile\NewCompany\IT\Department\QA Team\Current Projects\CRISP\CrispAutomation\CrispData\Premiums\SHPremiums\SH-MasterPlotData.xlsx";
            var dtExcel = ExcelPlay.ExcelToDataTable(masterplotdata);
            for (int i = 0; i < dtExcel.Rows.Count; i++)
            {
                ExcelPlay.CreateSpreadsheet(dtExcel.Rows[i]);
                CreateAndSendQuoteToVerifyPremiums();
            }
        }
        public void LABCPrivateRentalPremiums()
        {
            masterplotdata = @"\\tpgfile\NewCompany\IT\Department\QA Team\Current Projects\CRISP\CrispAutomation\CrispData\Premiums\PRSPremiums\PRS-MasterPlotData.xlsx";
            var dtExcel = ExcelPlay.ExcelToDataTable(masterplotdata);
            for (int i = 0; i < dtExcel.Rows.Count; i++)
            {
                ExcelPlay.CreateSpreadsheet(dtExcel.Rows[i]);
                CreateAndSendQuoteToVerifyPremiums();
            }
        }
        public void LABCCommercialPremiums()
        {
            masterplotdata = @"\\tpgfile\NewCompany\IT\Department\QA Team\Current Projects\CRISP\CrispAutomation\CrispData\Premiums\CommPremiums\Comm-MasterPlotData.xlsx";
            var dtExcel = ExcelPlay.ExcelToDataTable(masterplotdata);
            for (int i = 0; i < dtExcel.Rows.Count; i++)
            {
                ExcelPlay.CreateSpreadsheet(dtExcel.Rows[i]);
                CreateAndSendQuoteToVerifyPremiums();
            }
        }
        public void LABCCompletedHousingPremiums()
        {
            masterplotdata = @"\\tpgfile\NewCompany\IT\Department\QA Team\Current Projects\CRISP\CrispAutomation\CrispData\Premiums\CompPremiums\Comp-MasterPlotData.xlsx";
            var dtExcel = ExcelPlay.ExcelToDataTable(masterplotdata);
            for (int i = 0; i < dtExcel.Rows.Count; i++)
            {
                ExcelPlay.CreateSpreadsheet(dtExcel.Rows[i]);
                CreateAndSendQuoteToVerifyPremiums();
            }
        }
        public void LABCSelfBuildPremiums()
        {
            masterplotdata = @"\\tpgfile\NewCompany\IT\Department\QA Team\Current Projects\CRISP\CrispAutomation\CrispData\Premiums\SBPremiums\SB-MasterPlotData.xlsx";
            var dtExcel = ExcelPlay.ExcelToDataTable(masterplotdata);
            for (int i = 0; i < dtExcel.Rows.Count; i++)
            {
                ExcelPlay.CreateSpreadsheet(dtExcel.Rows[i]);
                CreateAndSendQuoteToVerifyPremiums();
            }
        }

        public void LABCSocialHousingPremiumsFor12YearsCoverLength()
        {
            masterplotdata = @"\\tpgfile\NewCompany\IT\Department\QA Team\Current Projects\CRISP\CrispAutomation\CrispData\Premiums\SHPremiums\SH-MasterPlotData.xlsx";
            var dtExcel = ExcelPlay.ExcelToDataTable(masterplotdata);
            for (int i = 0; i < dtExcel.Rows.Count; i++)
            {
                ExcelPlay.CreateSpreadsheet(dtExcel.Rows[i]);
                CreateAndSendQuoteToVerifyPremiumsFor12YearsCoverLength();
            }
        }
        public void LABCPrivateRentalPremiumsFor12YearsCoverLength()
        {
            masterplotdata = @"\\tpgfile\NewCompany\IT\Department\QA Team\Current Projects\CRISP\CrispAutomation\CrispData\Premiums\PRSPremiums\PRS-MasterPlotData.xlsx";
            var dtExcel = ExcelPlay.ExcelToDataTable(masterplotdata);
            for (int i = 0; i < dtExcel.Rows.Count; i++)
            {
                ExcelPlay.CreateSpreadsheet(dtExcel.Rows[i]);
                CreateAndSendQuoteToVerifyPremiumsFor12YearsCoverLength();
            }
        }
        public void LABCCommercialPremiumsFor12YearsCoverLength()
        {
            masterplotdata = @"\\tpgfile\NewCompany\IT\Department\QA Team\Current Projects\CRISP\CrispAutomation\CrispData\Premiums\CommPremiums\Comm-MasterPlotData.xlsx";
            var dtExcel = ExcelPlay.ExcelToDataTable(masterplotdata);
            for (int i = 0; i < dtExcel.Rows.Count; i++)
            {
                ExcelPlay.CreateSpreadsheet(dtExcel.Rows[i]);
                CreateAndSendQuoteToVerifyPremiumsFor12YearsCoverLength();
            }
        }

        //Create a Quote From Master Plot Schedule Data 
        public void CreateAndSendQuoteToVerifyPremiums()
        {
            // step3: create quote               
            WaitForElement(Dashboardpage.PlusButton);
            AddLABCQuotePage.SubmitQuoteOnSinglePlot();
            // step4: send quote and Iterate for ratings               
            SendQuotePage.ClickOnSendQuoteButton();

            if (Premiums.eachplotrow.Any(o => o.ProductName.Contains("New Homes")) ||
                Premiums.eachplotrow.Any(o => o.ProductName.Contains("Social Housing")) ||
                Premiums.eachplotrow.Any(o => o.ProductName.Contains("Private Rental")))
            {
                LABCPremiums.SendQuoteRatingPage();
            }
            if (Premiums.eachplotrow.Any(o => o.ProductName.Contains("Commercial")))
            {
                LABCPremiums.SendQuotePageBuilderExpPage();
            }
            if (Premiums.eachplotrow.Any(o => o.ProductName.Contains("Completed Housing")) || Premiums.eachplotrow.Any(o => o.ProductName.Contains("Self Build")))
            {
                SendQuotePage.SendQuoteSecuritiesPage();
                SendQuotePage.SendQuoteConditionsPage();
                SendQuotePage.SendQuotepecialTermsPage();
                LABCPremiums.SendQuoteFeesDetailsPage();
            }
        }
        //Create a Quote From Master Plot Schedule Data 
        public void CreateAndSendQuoteToVerifyPremiumsFor12YearsCoverLength()
        {
            // step3: create quote               
            WaitForElement(Dashboardpage.PlusButton);
            AddLABCQuotePage.SubmitQuoteOnSinglePlotFor12YearsCoverLength();
            // step4: send quote and Iterate for ratings               
            SendQuotePage.ClickOnSendQuoteButton();

            if (Premiums.eachplotrow.Any(o => o.ProductName.Contains("New Homes")) ||
                Premiums.eachplotrow.Any(o => o.ProductName.Contains("Social Housing")) ||
                Premiums.eachplotrow.Any(o => o.ProductName.Contains("Private Rental")))
            {
                LABCPremiums.SendQuoteRatingPage();
            }
            if (Premiums.eachplotrow.Any(o => o.ProductName.Contains("Commercial")))
            {
                LABCPremiums.SendQuotePageBuilderExpPage();
            }
            if (Premiums.eachplotrow.Any(o => o.ProductName.Contains("Completed Housing")) || Premiums.eachplotrow.Any(o => o.ProductName.Contains("Self Build")))
            {
                SendQuotePage.SendQuoteSecuritiesPage();
                SendQuotePage.SendQuoteConditionsPage();
                SendQuotePage.SendQuotepecialTermsPage();
                LABCPremiums.SendQuoteFeesDetailsPage();
            }
        }
    }
}
