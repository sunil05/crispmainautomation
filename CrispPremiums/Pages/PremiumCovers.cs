﻿using ExcelDataReader;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.PageObjects;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace RegressionPacks.Pages
{
    public class PremiumCovers:Support.Pages
    {
        public IWebDriver wdriver;
       // public string RatingValue;    

        public PremiumCovers(ISearchContext driver)
        {
            PageFactory.InitElements(driver, this);
            wdriver = (RemoteWebDriver)driver;
        }
        //Fee Table Elements 

        [FindsBy(How = How.XPath, Using = "//crisp-wizard-step[@step-title='Fees']//fees//fee-container//table//thead//tr//th[text()='Service']//parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[1]")]
        public IList<IWebElement> ProductServicesList;

        [FindsBy(How = How.XPath, Using = "//crisp-wizard-step[@step-title='Fees']//fees//fee-container//table//thead//tr//th[text()='Cover']//parent::tr//parent::thead//parent::table//tbody//tr[@class='item']//td[1]")]
        public IList<IWebElement> ProductCoversList;

        [FindsBy(How = How.XPath, Using = "//crisp-wizard-step[@step-title='Fees']//fees//fee-container//table//thead//tr//th[text()='Service']//parent::tr//parent::thead//parent::table//tbody//tr[@class='item']//td[text()='Contaminated Land']//parent::tr")]
        public IList<IWebElement> ContaminatedCover;

        [FindsBy(How = How.XPath, Using = "//crisp-wizard-step[@step-title='Fees']//fees//fee-container//table//thead//tr//th[text()='Service']//parent::tr//parent::thead//parent::table//tbody//tr[@class='item']//td[text()='Additional Cover for Local Authority Building Control Function']//parent::tr")]
        public IList<IWebElement> LocalAuthorityBuildingControlFunctionCover;

        [FindsBy(How = How.XPath, Using = "//crisp-wizard-step[@step-title='Fees']//fees//fee-container//table//thead//tr//th[text()='Service']//parent::tr//parent::thead//parent::table//tbody//tr[@class='item']//td[text()='Additional Cover for Approved Inspector Building Control Function']//parent::tr")]
        public IList<IWebElement> ApprovedInspectorBuildingControlFunctionCover;     

        [FindsBy(How = How.XPath, Using = "//crisp-wizard-step[@step-title='Fees']//fees//fee-container//table//thead//tr//th[text()='Service']//parent::tr//parent::thead//parent::table//tbody//tr[@class='item']//td[text()='Seepage']//parent::tr")]
        public IList<IWebElement> SeeapageCover;

        [FindsBy(How = How.XPath, Using = "//crisp-wizard-step[@step-title='Fees']//fees//fee-container//table//thead//tr//th[text()='Service']//parent::tr//parent::thead//parent::table//tbody//tr[@class='item']//td[text()='Waivers of Subrogation Rights - Builder']//parent::tr")]
        public IList<IWebElement> WaiversBuilderCover;

        [FindsBy(How = How.XPath, Using = "//crisp-wizard-step[@step-title='Fees']//fees//fee-container//table//thead//tr//th[text()='Service']//parent::tr//parent::thead//parent::table//tbody//tr[@class='item']//td[text()='Waivers of Subrogation Rights - Engineer']//parent::tr")]
        public IList<IWebElement> WaiversEngineerCover;

        [FindsBy(How = How.XPath, Using = "//crisp-wizard-step[@step-title='Fees']//fees//fee-container//table//thead//tr//th[text()='Service']//parent::tr//parent::thead//parent::table//tbody//tr[@class='item']//td[text()='Waivers of Subrogation Rights - Architect']//parent::tr")]
        public IList<IWebElement> WaiversArchitectCover;

        [FindsBy(How = How.XPath, Using = "//crisp-wizard-step[@step-title='Fees']//fees//fee-container//table//thead//tr//th[text()='Service']//parent::tr//parent::thead//parent::table//tbody//tr[@class='item']//td[text()='Loss of Gross Profit']//parent::tr")]
        public IList<IWebElement> LossOfGrossProfitCover;

        [FindsBy(How = How.XPath, Using = "//crisp-wizard-step[@step-title='Fees']//fees//fee-container//table//thead//tr//th[text()='Service']//parent::tr//parent::thead//parent::table//tbody//tr[@class='item']//td[text()='Loss of Rent Payable']//parent::tr")]
        public IList<IWebElement> LossOfRentPayableCover;

        [FindsBy(How = How.XPath, Using = "//crisp-wizard-step[@step-title='Fees']//fees//fee-container//table//thead//tr//th[text()='Service']//parent::tr//parent::thead//parent::table//tbody//tr[@class='item']//td[text()='Loss of Rent Receivable']//parent::tr")]
        public IList<IWebElement> LossOfRentReceivableCover;

        [FindsBy(How = How.XPath, Using = "//crisp-wizard-step[@step-title='Fees']//fees//fee-container//table//thead//tr//th[text()='Service']//parent::tr//parent::thead//parent::table//tbody//tr[@class='item']//td[text()='Insolvency of the Developer']//parent::tr")]
        public IList<IWebElement> InsolvencyOfDeveloperCover;

        [FindsBy(How = How.XPath, Using = "//crisp-wizard-step[@step-title='Fees']//fees//fee-container//table//thead//tr//th[text()='Service']//parent::tr//parent::thead//parent::table//tbody//tr[@class='item']//td[text()='Insolvency of the Builder']//parent::tr")]
        public IList<IWebElement> InsolvencyOfBuilderCover;

        [FindsBy(How = How.XPath, Using = "//crisp-wizard-step[@step-title='Fees']//fees//fee-container//table//thead//tr//th[text()='Service']//parent::tr//parent::thead//parent::table//tbody//tr[@class='item']//td[text()='Defects']//parent::tr")]
        public IList<IWebElement> DefectsCover;

        [FindsBy(How = How.XPath, Using = "//crisp-wizard-step[@step-title='Fees']//fees//fee-container//table//thead//tr//th[text()='Service']//parent::tr//parent::thead//parent::table//tbody//tr[@class='item']//td[text()='Contaminated Land']//parent::tr")]
        public IList<IWebElement> ContaminatedLandList;

        [FindsBy(How = How.XPath, Using = "//crisp-wizard-step[@step-title='Fees']//fees//fee-container//table//thead//tr//th[text()='Service']//parent::tr//parent::thead//parent::table//tbody//tr[@class='item']//td[text()='Consumer Code Fee']//parent::tr")]
        public IList<IWebElement> ConsumerCodeFeeList;

        [FindsBy(How = How.XPath, Using = "//crisp-wizard-step[@step-title='Fees']//fees//fee-container//table//thead//tr//th[text()='Service']//parent::tr//parent::thead//parent::table//tbody//tr[@class='item']//td[text()='Loss of Rent']//parent::tr")]
        public IList<IWebElement> LossOfRentCover;


        [FindsBy(How = How.XPath, Using = "//crisp-wizard-step[@step-title='Fees']//fees//fee-container//table//thead//tr//th[text()='Service']//parent::tr//parent::thead//parent::table//tbody//tr[@class='item']//td[text()='Plastering']//parent::tr")]
        public IList<IWebElement> PlasteringCover;

        [FindsBy(How = How.XPath, Using = "//crisp-wizard-step[@step-title='Fees']//fees//fee-container//table//thead//tr//th[text()='Service']//parent::tr//parent::thead//parent::table//tbody//tr[@class='item']//td[text()='Sound Transmission']//parent::tr")]
        public IList<IWebElement> SoundTransmissionCover;

        //services Elements 

        [FindsBy(How = How.XPath, Using = "//crisp-wizard-step[@step-title='Fees']//fees//fee-container//table//thead//tr//th[text()='Service']//parent::tr//parent::thead//parent::table//tbody//tr[@class='item']//td[text()='Technical Audit Fee']//parent::tr")]
        public IList<IWebElement> ProductTechnicalAuditFee;

        [FindsBy(How = How.XPath, Using = "//crisp-wizard-step[@step-title='Fees']//fees//fee-container//table//thead//tr//th[text()='Service']//parent::tr//parent::thead//parent::table//tbody//tr[@class='item']//td[text()='Consumer Code Fee']//parent::tr")]
        public IList<IWebElement> ProductConsumerCodeFee;

        [FindsBy(How = How.XPath, Using = "//crisp-wizard-step[@step-title='Fees']//fees//fee-container//table//thead//tr//th[text()='Service']//parent::tr//parent::thead//parent::table//tbody//tr[@class='item']//td[text()='Refurbishment Assessment Fee']//parent::tr")]
        public IList<IWebElement> ProductRefurbishmentAssessmentFee;

        [FindsBy(How = How.XPath, Using = "//crisp-wizard-step[@step-title='Fees']//fees//fee-container//table//thead//tr//th[text()='Service']//parent::tr//parent::thead//parent::table//tbody//tr[@class='item']//td[text()='Administration Fee']//parent::tr")]
        public IList<IWebElement> ProductAdministrationFee;

        [FindsBy(How = How.XPath, Using = "//crisp-wizard-step[@step-title='Fees']//fees//fee-container//table//thead//tr//th[text()='Service']//parent::tr//parent::thead//parent::table//tbody//tr[@class='item']//td[text()='Cancellation Fee']//parent::tr")]
        public IList<IWebElement> ProductCancellationFee;

        [FindsBy(How = How.XPath, Using = "//crisp-wizard-step[@step-title='Fees']//fees//fee-container//table//thead//tr//th[text()='Service']//parent::tr//parent::thead//parent::table//tbody//tr[@class='item']//td[text()='Atkins Fee']//parent::tr")]
        public IList<IWebElement> ProductAtkinsFee;

        //Fees Table Elements from UI 

        [FindsBy(How = How.XPath, Using = "//crisp-wizard-step[@step-title='Fees']//fees//fee-container//div[@class='crisp-row']//table//thead[2]//tr//th[@class='product-title']")]
        public IWebElement ProductNameOnFeesPage;

        [FindsBy(How = How.XPath, Using = "//th[@class='product-title']/parent::tr/following-sibling::tr//crisp-display-value[contains(@value.bind,'product.totalEstimatedSalePrice')]//span")]
        public IWebElement EstimatedSalesPriceOnFeesPage;

        [FindsBy(How = How.XPath, Using = "//th[@class='product-title']/parent::tr/following-sibling::tr//crisp-display-value[contains(@value.bind,'product.totalReconstructionCost')]//span")]
        public IWebElement ReconstructionCostOnFeesPage;

        [FindsBy(How = How.XPath, Using = "//th[@class='product-title']/parent::tr/following-sibling::tr//crisp-display-value[@value.bind='product.unitCount']//label/following-sibling::span[1]")]
        public IWebElement NumberOfPlotsCount;

        [FindsBy(How = How.XPath, Using = "//th[@class='product-title']/parent::tr/following-sibling::tr//crisp-display-value[contains(@value.bind,'product.lengthOfCover')]//span")]
        public IWebElement CoverLengthYears;

        [FindsBy(How = How.XPath, Using = "//th[@class='product-title']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[contains(text(),'Insolvency')]//following-sibling::td[@class='right-align'][1]")]
        public IWebElement InsolvancyStructuralFee;

        [FindsBy(How = How.XPath, Using = "//th[@class='product-title']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[contains(text(),'Insolvency')]//following-sibling::td[@class='right-align'][2]")]
        public IWebElement ActualInsolvancyStructuralIPTFee;

        [FindsBy(How = How.XPath, Using = "//th[@class='product-title']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[text()='Consumer Code Fee']//following-sibling::td[@class='right-align'][1]")]
        public IList<IWebElement> ConsumerCodeList;

        [FindsBy(How = How.XPath, Using = "//th[@class='product-title']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[text()='Consumer Code Fee']//following-sibling::td[@class='right-align'][1]")]
        public IWebElement ConsumerCodeValue;

        [FindsBy(How = How.XPath, Using = "//th[@class='product-title']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[text()='Technical Audit Fee']//following-sibling::td[@class='right-align'][1]")]
        public IWebElement TechnicalAuditValue;

        [FindsBy(How = How.XPath, Using = "//th[@class='product-title']//parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[text()='Refurbishment Assessment Fee']//following-sibling::td[@class='right-align'][1]")]
        public IWebElement RefurbishmentFee;

        [FindsBy(How = How.XPath, Using = "//th[@class='product-title']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[text()='Administration Fee']//following-sibling::td[@class='right-align'][1]")]
        public IWebElement AdminiStrationFee;

    }
}


