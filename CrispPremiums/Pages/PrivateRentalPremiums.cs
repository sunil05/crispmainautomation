﻿using OpenQA.Selenium;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.PageObjects;
using RegressionPacks.Support;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using ExcelDataReader;

namespace RegressionPacks.Pages
{
    public class PrivateRentalPremiums : Support.Pages
    {
        public IWebDriver wdriver;
        public PrivateRentalPremiums(ISearchContext driver)
        {           
            PageFactory.InitElements(driver, this);
            wdriver = (RemoteWebDriver)driver;
        }
        DataTable excelTable;
        double BasePremium = 0;
        double TAFee = 0;
        double RefurbishmentFee = 0;
        double MinimumTAFee = 0;
        double iptCaluclationValue = 0.12;
        double InsolvencyPremium = 0;
        double LossOfRentPremium = 0;
        double PlasteringPremium = 0;
        double SoundTransmissionPremium = 0;
        double getBanding;
        double Banding;
        int RatingColumn;
        double CoverLoadingPremium;

        public void CoversOnPrivateRentalProduct()
        {
            if (Premiums.eachplotrow.Any(o => o.ProductName.Contains("Private Rental")))
            {
                Premiums.ProductsCovers = PremiumCovers.ProductCoversList.Select(i => i.Text.ToString()).ToList();
                Premiums.ProductServices = PremiumCovers.ProductServicesList.Select(i => i.Text.ToString()).ToList();
                //Products Covers

                if (Premiums.ProductsCovers.Any(x => x.Contains("Defects")))
                {
                    Premiums.DefectsCover = PremiumCovers.DefectsCover.Count > 0 ? "Yes" : "No";
                }
                if (Premiums.ProductsCovers.Any(x => x.Contains("Contaminated Land")))
                {
                    Premiums.ContaminatedLandCover = PremiumCovers.ContaminatedLandList.Count > 0 ? "Yes" : "No";
                }
                if (Premiums.ProductsCovers.Any(x => x.Contains("Additional Cover for Local Authority Building Control Function")))
                {
                    Premiums.LocalAuthorityBuildingControlFunctionCover = PremiumCovers.LocalAuthorityBuildingControlFunctionCover.Count > 0 ? "Yes" : "No";
                }
                if (Premiums.ProductsCovers.Any(x => x.Contains("Insolvency of the Builder")))
                {
                    Premiums.InsolvencyCover = PremiumCovers.InsolvencyOfBuilderCover.Count > 0 ? "Yes" : "No";
                }
                if (Premiums.ProductsCovers.Any(x => x.Contains("Loss of Rent")))
                {
                    Premiums.LossOfRentCover = PremiumCovers.LossOfRentCover.Count > 0 ? "Yes" : "No";
                }
                if (Premiums.ProductsCovers.Any(x => x.Contains("Plastering")))
                {
                    Premiums.PlasteringCover = PremiumCovers.PlasteringCover.Count > 0 ? "Yes" : "No";
                }
                if (Premiums.ProductsCovers.Any(x => x.Contains("Sound Transmission")))
                {
                    Premiums.SoundTransmissionCover = PremiumCovers.SoundTransmissionCover.Count > 0 ? "Yes" : "No";
                }

                //Products Services 
                if (Premiums.ProductServices.Any(x => x.Contains("Technical Audit Fee")))
                {
                    Premiums.ProductTechnicalAuditFeeService = PremiumCovers.ProductTechnicalAuditFee.Count > 0 ? "Yes" : "No";
                }
                if (Premiums.ProductServices.Any(x => x.Contains("Refurbishment Assessment Fee")))
                {
                    Premiums.ProductRefurbishmentAssessmentService = PremiumCovers.ProductRefurbishmentAssessmentFee.Count > 0 ? "Yes" : "No";
                }
                if (Premiums.ProductServices.Any(x => x.Contains("Administration Fee")))
                {
                    Premiums.ProductAdminFeeService = PremiumCovers.ProductAdministrationFee.Count > 0 ? "Yes" : "No";
                }
                if (Premiums.ProductServices.Any(x => x.Contains("Cancellation Fee")))
                {
                    Premiums.ProductCancellationFeeService = PremiumCovers.ProductCancellationFee.Count > 0 ? "Yes" : "No";
                }
            }
        }

        public void PrivateRentalPremiumsCaluclations()
        {
            Premiums.StructuralFee = 0;
            Premiums.InsolvencyFee = 0;
            Premiums.DefectsFee = 0;
            Premiums.LocalAuthorityBuildingControlFee = 0;
            Premiums.LossOfRentFee = 0;
            Premiums.PlasteringFee = 0;
            Premiums.SoundTransmissionFee = 0;
            Premiums.ContaminatedLandFee = 0;
            Premiums.TechnicalAuditFee = 0;
            Premiums.AdministrationFee = 0;
            Premiums.RefurbishmentAssessmentFee = 0;
            Premiums.CancellationFee = 0;

            var aggregatedPremium = new PrivateRentalPremium();
            //Caluclating Each Plot Row Premiums Values 
            foreach (var plot in Premiums.eachplotrow)
            {
                //Actual Premiums Caluclaions for Each Plot Row 
                var calculatedPremiums = PrivateRentalCaluclationsForEachPlotRow(plot);
                //Cover Premiums 
                Premiums.StructuralFee += calculatedPremiums.structuralFee;
                Premiums.DefectsFee += calculatedPremiums.defectsFee;
                Premiums.ContaminatedLandFee += calculatedPremiums.contaminatedLandFee;
                Premiums.LocalAuthorityBuildingControlFee += calculatedPremiums.laBuldingControlFunctionFee;
                Premiums.LossOfRentFee += calculatedPremiums.lossOfRentFee;   
                Premiums.PlasteringFee += calculatedPremiums.plasteringFee;
                Premiums.SoundTransmissionFee += calculatedPremiums.soundTransmissionFee;
                //Services 
                Premiums.TechnicalAuditFee += calculatedPremiums.technicalFee;            
                Premiums.AdministrationFee += calculatedPremiums.administrationFee;
                Premiums.CancellationFee += calculatedPremiums.cancellationFee;
            }
            //Caluclating Insolvancy Builder Premium Values on Quote Level 
            if (Premiums.InsolvencyCover == "Yes")
            {
                InsolvencyBuilderPremiumFatcor();
                Premiums.InsolvencyFee = Math.Round((InsolvencyPremium), 2);
            }
            //Caluclating Refrubishment Cover Premium Values on Quote Level 
            if (Premiums.ProductRefurbishmentAssessmentService == "Yes")
            {
                RefurbishmentFeeCaluclations();
                Premiums.RefurbishmentAssessmentFee = Math.Round((RefurbishmentFee), 2);
            }
            //Caluclate Minimum TA Fee    
            if(Premiums.eachplotrow.Count <=14)
            {
                MinimumTAFeeCaluclations();
                //Apply Minimum TAFee Value              
                if (Premiums.TechnicalAuditFee <= MinimumTAFee)
                {
                    Premiums.TechnicalAuditFee = Math.Round(Convert.ToDouble(MinimumTAFee), 2);
                }
            }
            //IPT Caluclations 
            Premiums.DefectsIPTFee = Math.Round(Convert.ToDouble(Premiums.DefectsFee * iptCaluclationValue), 2);
            Premiums.StructuralIPTFee = Math.Round(Convert.ToDouble(Premiums.StructuralFee * iptCaluclationValue), 2);
            Premiums.InsolvencyIPTFee = Math.Round(Convert.ToDouble(Premiums.InsolvencyFee * iptCaluclationValue), 2);
            Premiums.LossOfRentIPTFee = Math.Round(Convert.ToDouble(Premiums.LossOfRentFee * iptCaluclationValue), 2);
            Premiums.ContaminatedLandIPTFee = Math.Round(Convert.ToDouble(Premiums.ContaminatedLandFee * iptCaluclationValue), 2);
            Premiums.LocalAuthorityBuildingControlIPTFee = Math.Round(Convert.ToDouble(Premiums.LocalAuthorityBuildingControlFee * iptCaluclationValue), 2);
            Premiums.PlasteringIPTFee = Math.Round(Convert.ToDouble(Premiums.PlasteringFee * iptCaluclationValue), 2);
            Premiums.SoundTransmissionIPTFee = Math.Round(Convert.ToDouble(Premiums.SoundTransmissionFee * iptCaluclationValue), 2);
            //VAT Caluclations
            Premiums.TechnicalAuditVATFee = Math.Round(0.00);
            Premiums.AdministrationVATFee = Math.Round(0.00);
            Premiums.RefurbishmentAssessmentVATFee = Math.Round(0.00);
            Premiums.CancellationVATFee = Math.Round(0.00);
            VerfiyPrivateRentalUIValues();
            if (Premiums.eachplotrow.Any(o => o.BCProduct.Contains("Residential Building Control")))
            {
                ResidentialBCPremiums.ResidentialBCPremiumNewPricesCalcs();
            }
        }
        public class PrivateRentalPremium
        {
            public double defectsFee;

            public double structuralFee;

            public double contaminatedLandFee;

            public double laBuldingControlFunctionFee;

            public double plasteringFee;

            public double soundTransmissionFee;

            public double insolvencyFee;

            public double lossOfRentFee;

            public double technicalFee;

            public double administrationFee;

            public double refurbishmentAssessmentFee;

            public double cancellationFee;

        }

        public PrivateRentalPremium PrivateRentalCaluclationsForEachPlotRow(plotrows plot)
        {
            var calculatedPremiums = new PrivateRentalPremium();
            //Retrieve all the covers and Services from Fees Detail Page 
            CoversOnPrivateRentalProduct();
            //Reading Pricing Setup Book 
            var fileInfo = new FileInfo(ExtensionMethods.GetAbsolutePath($@"TestData\Premiums\ProductPricingSetup\Product Pricing Setup - {plot.ProductName}.xlsx"));
            string premiumsdata = fileInfo.ToString();
            excelTable = ReadPrivateRentalMasterPremiumsPriceData(premiumsdata);
            //Structural Fee Caluclations Based on Banding           
            StuctralPremiumCaluclation(plot);
            calculatedPremiums.structuralFee = BasePremium;
            if (Premiums.ProductsCovers.Count > 0)
            {
                if (Premiums.DefectsCover == "Yes")
                {
                    calculatedPremiums.defectsFee = Convert.ToDouble(0.00);
                }
                if (Premiums.ContaminatedLandCover == "Yes")
                {
                    calculatedPremiums.contaminatedLandFee = Convert.ToDouble(0.00);
                }
                if (Premiums.LocalAuthorityBuildingControlFunctionCover == "Yes")
                {
                    calculatedPremiums.laBuldingControlFunctionFee = Convert.ToDouble(0.00);
                }

                if (Premiums.LossOfRentCover == "Yes")
                {
                    LossOfRentCaluclations(plot);
                    calculatedPremiums.lossOfRentFee = Math.Round((LossOfRentPremium), 2);
                }

                if (Premiums.PlasteringCover == "Yes")
                {
                    PlasteringPremiumsCaluclations(plot);
                    calculatedPremiums.plasteringFee = Math.Round((PlasteringPremium), 2);
                }
                if (Premiums.SoundTransmissionCover == "Yes")
                {
                    SoundTransmissionPremiumsCaluclations(plot);
                    calculatedPremiums.soundTransmissionFee = Math.Round((SoundTransmissionPremium), 2);
                }
            }

            //Product Service Fee Values 
            if (Premiums.ProductServices.Count > 0)
            {

                if (Premiums.ProductTechnicalAuditFeeService == "Yes")
                {
                    TAFeeCaluclations(plot);
                    if (plot.ConstructionType == "New Build")
                    {
                        TAFeeNewBuildSOWCaluclations(plot);
                    }
                    if (plot.ConstructionType == "Conversion")
                    {
                        TAFeeConversionSOWCaluclations(plot);
                    }
                    calculatedPremiums.technicalFee = Math.Round((TAFee), 2);
                }
                if (Premiums.ProductAdminFeeService == "Yes")
                {
                    calculatedPremiums.administrationFee = Convert.ToDouble(0.00);
                }
                if (Premiums.ProductCancellationFeeService == "Yes")
                {
                    calculatedPremiums.cancellationFee = Convert.ToDouble(0.00);
                }
            }

            return calculatedPremiums;
        }


        //Reading Strucural Premium Banding Table  and Caluclate Base Premium
        public void StuctralPremiumCaluclation(plotrows plot)
        {
            //Caluclations Based on Banding 
            if (plot.ReconstructionCostValue <= 80000)
            {
                getBanding = 80000;
            }
            if (plot.ReconstructionCostValue > 80000 && plot.ReconstructionCostValue <= 90000)
            {
                getBanding = 90000;
            }
            if (plot.ReconstructionCostValue > 90000 && plot.ReconstructionCostValue <= 100000)
            {
                getBanding = 100000;
            }
            if (plot.ReconstructionCostValue > 100000 && plot.ReconstructionCostValue <= 110000)
            {
                getBanding = 110000;
            }
            if (plot.ReconstructionCostValue > 110000 && plot.ReconstructionCostValue <= 120000)
            {
                getBanding = 120000;
            }
            if (plot.ReconstructionCostValue > 120000 && plot.ReconstructionCostValue <= 140000)
            {
                getBanding = 140000;
            }
            if (plot.ReconstructionCostValue > 140000 && plot.ReconstructionCostValue <= 160000)
            {
                getBanding = 160000;
            }
            if (plot.ReconstructionCostValue > 160000 && plot.ReconstructionCostValue <= 180000)
            {
                getBanding = 180000;
            }
            if (plot.ReconstructionCostValue > 180000 && plot.ReconstructionCostValue <= 200000)
            {
                getBanding = 200000;
            }
            if (plot.ReconstructionCostValue > 200000 && plot.ReconstructionCostValue <= 250000)
            {
                getBanding = 250000;
            }
            if (plot.ReconstructionCostValue > 250000 && plot.ReconstructionCostValue <= 300000)
            {
                getBanding = 300000;
            }
            if (plot.ReconstructionCostValue > 300000 && plot.ReconstructionCostValue <= 350000)
            {
                getBanding = 350000;
            }
            if (plot.ReconstructionCostValue > 350000 && plot.ReconstructionCostValue <= 400000)
            {
                getBanding = 400000;
            }
            if (plot.ReconstructionCostValue > 400000 && plot.ReconstructionCostValue <= 500000)
            {
                getBanding = 500000;
            }
            if (plot.ReconstructionCostValue > 500000 && plot.ReconstructionCostValue <= 600000)
            {
                getBanding = 600000;
            }

            if (plot.ReconstructionCostValue > 600000 && plot.ReconstructionCostValue <= 750000)
            {
                getBanding = 750000;
            }

            if (plot.ReconstructionCostValue > 750000 && plot.ReconstructionCostValue <= 1000000)
            {
                getBanding = 1000000;
            }
            if (plot.ReconstructionCostValue >= 1000000)
            {
                getBanding = 1000000;
            }
            Banding = Convert.ToDouble(getBanding);
            // Reading Structural Premium Table From Newhomes Pricing Setup Book
            var rating = Premiums.Rating;
            var bandingTableRow = -1;
            var bandingRow = -1;
            var ratingColumn = -1;
            //Get Banding Table from Structural Premium Table
            for (int i = 0; i < excelTable.Rows.Count; i++)
            {
                if (excelTable.Rows[i][1].ToString().Contains("Banding "))
                {
                    bandingTableRow = i;
                    break;
                }
            }
            if (bandingTableRow == -1)
                throw new Exception("Banding Table Not Found");


            //Get Banding Row from Structural Premium Table
            for (int i = bandingTableRow; i < excelTable.Rows.Count; i++)
            {
                if (excelTable.Rows[i][1].ToString() == getBanding.ToString())
                {
                    bandingRow = i;
                    break;
                }
            }
            if (bandingRow == -1)
                throw new Exception("Banding Table Starting Row Not Found");

            //Get Rating Columns from Structural Premium Table

            for (int i = bandingTableRow; i < excelTable.Rows[bandingTableRow].ItemArray.Length; i++)
            {
                if (excelTable.Rows[bandingTableRow].ItemArray[i].ToString() == Premiums.Rating.ToString())
                {
                    ratingColumn = i;
                    RatingColumn = ratingColumn;
                }
            }

            if (ratingColumn == -1)
                throw new Exception("Rating Column Not Found");

            //Get Premium Value From structural Premium Table 
            double basicpremiumValue = Convert.ToDouble(excelTable.Rows[bandingRow][RatingColumn]);
            BasePremium = Convert.ToDouble(getBanding * basicpremiumValue);
            if (plot.ReconstructionCostValue > 1000000)
            {
                StuctralPremiumBandHighValueCaluclations(plot);
            }

            //Caluclate Premiums Based on Unit type 
            UnitTypeCaluclation(plot);
            //Caluclate Premiums Based on Construction type 
            ConstructionTypeCaluclation(plot);
            //Caluclations Based on Cover Loading 
            PeriodOfCoverLoadingCaluclation(plot);

        }
        public void StuctralPremiumBandHighValueCaluclations(plotrows plot)
        {
            //Caluclate Structural Premium High Value

            double bandHighValueDiffrence = Convert.ToDouble(plot.EstrimatedSalesPrice - 1000000);

            var rating = Premiums.Rating;
            var highValueBandingTableRow = -1;
            var highValueBandingRow = -1;
            var ratingColumn = -1;
            //Get Banding Table from Structural Premium Table
            for (int i = 0; i < excelTable.Rows.Count; i++)
            {
                if (excelTable.Rows[i][0].ToString().Contains("Structural Premium High Value"))
                {
                    highValueBandingTableRow = i;
                    break;
                }
            }
            if (highValueBandingTableRow == -1)
                throw new Exception("Structural Premium High Value Banding Table Not Found");


            //Get Banding Row from Structural Premium Table
            for (int i = highValueBandingTableRow; i < excelTable.Rows.Count; i++)
            {
                if (excelTable.Rows[i][1].ToString().Contains("Fee per 100,000"))
                {
                    highValueBandingRow = i;
                    break;
                }
            }
            if (highValueBandingRow == -1)
                throw new Exception("High Value Banding Row Not Found");

            //Get Premium Value From structural Premium Table 
            double basicpremiumValue = Convert.ToDouble(excelTable.Rows[highValueBandingRow][RatingColumn]);
            BasePremium = Convert.ToDouble((bandHighValueDiffrence * basicpremiumValue) + BasePremium);

        }
        //Reading Period Of Cover Loading Table 
        public void PeriodOfCoverLoadingCaluclation(plotrows plot)
        {
            var periodOfCoverLoadingTableRow = -1;
            var periodOfCoverRow = -1;
            //Get Unit Type Loading Table from Structural Premium Table
            for (int i = 0; i < excelTable.Rows.Count; i++)
            {
                if (excelTable.Rows[i][0].ToString().Contains("Period of Cover Loading"))
                {
                    periodOfCoverLoadingTableRow = i;
                    break;
                }
            }
            if (periodOfCoverLoadingTableRow == -1)
                throw new Exception("Unit Type Loading Table Not Found");

            //Get UnitType Row from Unit Type Loading Table
            for (int i = periodOfCoverLoadingTableRow; i < excelTable.Rows.Count; i++)
            {
                if (excelTable.Rows[i][1].ToString() == Premiums.CoverLengthYears.ToString())
                {
                    periodOfCoverRow = i;
                    break;
                }
            }
            if (periodOfCoverRow == -1)
                throw new Exception("Period Of Cover Loading Row Not Found");
            double premiumValueBasedonPeriodOfCoverLoading = Convert.ToDouble(excelTable.Rows[periodOfCoverRow][RatingColumn]);
            CoverLoadingPremium = Convert.ToDouble(premiumValueBasedonPeriodOfCoverLoading);
            BasePremium = Convert.ToDouble(BasePremium * premiumValueBasedonPeriodOfCoverLoading);
        }



        //Reading Unit Type Loading Table and Caluclate Base Premium
        public void UnitTypeCaluclation(plotrows plot)
        {
            var unitTypeTableRow = -1;
            var unitTypeRow = -1;
            //Get Unit Type Loading Table from Structural Premium Table
            for (int i = 0; i < excelTable.Rows.Count; i++)
            {
                if (excelTable.Rows[i][0].ToString().Contains("Unit Type Loading"))
                {
                    unitTypeTableRow = i;
                    break;
                }
            }
            if (unitTypeTableRow == -1)
                throw new Exception("Unit Type Loading Table Not Found");

            //Get UnitType Row from Unit Type Loading Table
            for (int i = unitTypeTableRow; i < excelTable.Rows.Count; i++)
            {
                if (excelTable.Rows[i][1].ToString() == plot.UnitType.ToString())
                {
                    unitTypeRow = i;
                    break;
                }
            }
            if (unitTypeRow == -1)
                throw new Exception("Unit Type Row Not Found");
            double premiumValueBasedonUnitTypeLoading = Convert.ToDouble(excelTable.Rows[unitTypeRow][RatingColumn]);
            BasePremium = Convert.ToDouble(BasePremium * premiumValueBasedonUnitTypeLoading);
        }
        //Reading Construction Type Loading Table and Caluclate Base Premium
        public void ConstructionTypeCaluclation(plotrows plot)
        {
            var constructionTypeTableRow = -1;
            var constructionTypeRow = -1;
            //Get Construction Type Loading Table from Structural Premium Table
            for (int i = 0; i < excelTable.Rows.Count; i++)
            {
                if (excelTable.Rows[i][0].ToString().Contains("Construction Type Loading"))
                {
                    constructionTypeTableRow = i;
                    break;
                }
            }
            if (constructionTypeTableRow == -1)
                throw new Exception("Construction Type Loading Table Not Found");

            //Get Construction Type  Row from Construction Type Loading Table 
            for (int i = constructionTypeTableRow; i < excelTable.Rows.Count; i++)
            {
                if (excelTable.Rows[i][1].ToString() == plot.ConstructionType.ToString())
                {
                    constructionTypeRow = i;
                    break;
                }
            }
            if (constructionTypeRow == -1)
                throw new Exception("Construction Type Row Not Found");
            double premiumValueBasedOnConstructionType = Convert.ToDouble(excelTable.Rows[constructionTypeRow][RatingColumn]);
            BasePremium = Convert.ToDouble(BasePremium * premiumValueBasedOnConstructionType);
            if (plot.ConstructionType == "New Build")
            {
                NewBuildStagesOfWorkCaluclation(plot);
            }
            if (plot.ConstructionType == "Conversion")
            {
                ConversionStagesOfWorkCaluclation(plot);
            }
        }

        //Reading NewBuild Stages Of Work Loading Table and Caluclate Base Premium
        public void NewBuildStagesOfWorkCaluclation(plotrows plot)
        {
            var newBuildSOWTableRow = -1;
            var sowRow = -1;
            //Get Newbuild Stages Of Works Table from Structural Premium Table
            for (int i = 0; i < excelTable.Rows.Count; i++)
            {
                if (excelTable.Rows[i][0].ToString().Contains("Structural Premium New Build SoW Loading"))
                {
                    newBuildSOWTableRow = i;
                    break;
                }
            }
            if (newBuildSOWTableRow == -1)
                throw new Exception("New Build SOW Table Not Found");

            //Get Newbuild Stages Of Works Row 
            for (int i = newBuildSOWTableRow; i < excelTable.Rows.Count; i++)
            {
                var stageOfWorks = plot.StageOfWork == "No Works Started" ? "No Works" : plot.StageOfWork;
                if (excelTable.Rows[i][1].ToString().Contains(stageOfWorks.ToString()))
                {
                    sowRow = i;
                    break;
                }
            }
            if (sowRow == -1)
                throw new Exception("Stages Of Works  Type Row Not Found");
            double premiumValueBasedOnNewBuildSOW = Convert.ToDouble(excelTable.Rows[sowRow][RatingColumn]);
            BasePremium = Convert.ToDouble(BasePremium * premiumValueBasedOnNewBuildSOW);
        }

        //Reading Conversion Stages Of Work Loading Table and Caluclate Base Premium
        public void ConversionStagesOfWorkCaluclation(plotrows plot)
        {
            var conversionSOWTableRow = -1;
            var sowRow = -1;
            //Get Conversion Stages Of Works Table from Structural Premium Table
            for (int i = 0; i < excelTable.Rows.Count; i++)
            {
                if (excelTable.Rows[i][0].ToString().Contains("Structural Premium Conversion SoW Loading"))
                {
                    conversionSOWTableRow = i;
                    break;
                }
            }
            if (conversionSOWTableRow == -1)
                throw new Exception("Conversion SOW Loading Table Not Found");

            //Get Conversion Stages Of Works Row 
            for (int i = conversionSOWTableRow; i < excelTable.Rows.Count; i++)
            {
                var stageOfWorks = plot.StageOfWork == "No Works Started" ? "No Works" : plot.StageOfWork;
                if (excelTable.Rows[i][1].ToString().Contains(stageOfWorks.ToString()))
                {
                    sowRow = i;
                    break;
                }
            }
            if (sowRow == -1)
                throw new Exception("Stages Of Works Type Row Not Found");
            double premiumValueBasedOnConversionSOW = Convert.ToDouble(excelTable.Rows[sowRow][RatingColumn]);
            BasePremium = Convert.ToDouble(BasePremium * premiumValueBasedOnConversionSOW);
        }

        //Reading Insolvancy Premium Factor Table 
        public void InsolvencyBuilderPremiumFatcor()
        {
            var insolvencyPremiumFactor = -1;
            var contractValueRow = -1;
            //Get Conversion Stages Of Works Table from Structural Premium Table
            for (int i = 0; i < excelTable.Rows.Count; i++)
            {
                if (excelTable.Rows[i][0].ToString().Contains("Insolvency of Builder Premium"))
                {
                    insolvencyPremiumFactor = i;
                    break;
                }
            }
            if (insolvencyPremiumFactor == -1)
                throw new Exception("Insolvency of Builder Premium Factor Table Not Found");
            //Get Conversion Stages Of Works Row 
            for (int i = insolvencyPremiumFactor; i < excelTable.Rows.Count; i++)
            {
                if (excelTable.Rows[i][1].ToString().Contains("Contract Value"))
                {
                    contractValueRow = i;
                    break;
                }
            }
            if (contractValueRow == -1)
                throw new Exception("Insolvency of Builder Contact Value Row Not Found");
            //Get Conversion Stages Of Works Row 
            double insolvencyContractValue = Convert.ToDouble(excelTable.Rows[contractValueRow][RatingColumn]);
            InsolvencyPremium = Convert.ToDouble(Premiums.ContractValue * insolvencyContractValue);

        }

        //Reading Loss Of Rent Premiums Factor Table 
        public void LossOfRentCaluclations(plotrows plot)
        {
            var lossOfRentTable = -1;
            var lossOfRentBandingRow = -1;
            //Get Conversion Stages Of Works Table from Structural Premium Table
            for (int i = 0; i < excelTable.Rows.Count; i++)
            {
                if (excelTable.Rows[i][0].ToString().Contains("Loss of Rent Premium"))
                {
                    lossOfRentTable = i;
                    break;
                }
            }
            if (lossOfRentTable == -1)
                throw new Exception("Loss Of Rent Table Not Found");
            //Get Conversion Stages Of Works Row 
            for (int i = lossOfRentTable; i < excelTable.Rows.Count; i++)
            {
                if (excelTable.Rows[i][1].ToString() == getBanding.ToString())
                {
                    lossOfRentBandingRow = i;
                    break;
                }
            }
            if (lossOfRentBandingRow == -1)
                throw new Exception("Loss Of Rent Banding Row Not Found");

            //Get Conversion Stages Of Works Row 
            double lossOfRentPremium = Convert.ToDouble(excelTable.Rows[lossOfRentBandingRow][RatingColumn]);
            LossOfRentPremium = Convert.ToDouble(Banding * lossOfRentPremium);
            LossOfRentPremium = Convert.ToDouble(LossOfRentPremium * CoverLoadingPremium);

        }
        //Reading Plastering Premiums Factor Table 
        public void PlasteringPremiumsCaluclations(plotrows plot)
        {
            var plasteringPremiumTable = -1;
            var plasteringPremiumsBandingRow = -1;
            //Get Conversion Stages Of Works Table from Structural Premium Table
            for (int i = 0; i < excelTable.Rows.Count; i++)
            {
                if (excelTable.Rows[i][0].ToString().Contains("Plastering Premium"))
                {
                    plasteringPremiumTable = i;
                    break;
                }
            }
            if (plasteringPremiumTable == -1)
                throw new Exception("Plastering Premium Table Not Found");
            //Get Conversion Stages Of Works Row 
            for (int i = plasteringPremiumTable; i < excelTable.Rows.Count; i++)
            {
                if (excelTable.Rows[i][1].ToString() == getBanding.ToString())
                {
                    plasteringPremiumsBandingRow = i;
                    break;
                }
            }
            if (plasteringPremiumsBandingRow == -1)
                throw new Exception("Plastering Premium Table Row Not Found");

            //Get Conversion Stages Of Works Row 
            double plasteringPremium = Convert.ToDouble(excelTable.Rows[plasteringPremiumsBandingRow][RatingColumn]);
            PlasteringPremium = Convert.ToDouble(Banding * plasteringPremium);
            PlasteringPremium = Convert.ToDouble(PlasteringPremium * CoverLoadingPremium);
        }

        //Reading Plastering Premiums Factor Table 
        public void SoundTransmissionPremiumsCaluclations(plotrows plot)
        {
            var soundTransmissionPremiumTable = -1;
            var soundTransmissionRow = -1;
            //Get Conversion Stages Of Works Table from Structural Premium Table
            for (int i = 0; i < excelTable.Rows.Count; i++)
            {
                if (excelTable.Rows[i][0].ToString().Contains("Sound Transmission Premium"))
                {
                    soundTransmissionPremiumTable = i;
                    break;
                }
            }
            if (soundTransmissionPremiumTable == -1)
                throw new Exception("Sound Transmission Premium Table Not Found");
            //Get Conversion Stages Of Works Row 
            for (int i = soundTransmissionPremiumTable; i < excelTable.Rows.Count; i++)
            {
                if (excelTable.Rows[i][1].ToString().Contains("Per Plot"))
                {
                    soundTransmissionRow = i;
                    break;
                }
            }
            if (soundTransmissionRow == -1)
                throw new Exception("Sound Transmission Row Not Found");

            //Get Conversion Stages Of Works Row 
            double SoundTransmissionRowValue = Convert.ToDouble(excelTable.Rows[soundTransmissionRow][2]);
            SoundTransmissionPremium = Convert.ToDouble(SoundTransmissionRowValue);
        }


        //Services Caluclations 
        //TA Fee Caluclations   
        //Basic TAFee Caluclations     
        public void TAFeeCaluclations(plotrows plot)
        {
            // Reading TA Fee Table From Newhomes Pricing Setup Book           
            var TAFeeTableRow = -1;
            var bandingRow = -1;

            //Get TA Fee Table from Structural Premium Table
            for (int i = 0; i < excelTable.Rows.Count; i++)
            {
                if (excelTable.Rows[i][0].ToString() == "TA Fee")
                {
                    TAFeeTableRow = i;
                    break;
                }
            }
            if (TAFeeTableRow == -1)
                throw new Exception("TAFee Table Not Found");


            //Get TA FEE Banding Row from TA Fee Table
            for (int i = TAFeeTableRow; i < excelTable.Rows.Count; i++)
            {
                if (excelTable.Rows[i][1].ToString() == getBanding.ToString())
                {
                    bandingRow = i;
                    break;
                }
            }
            if (bandingRow == -1)
                throw new Exception("TA Fee Banding Row Not Found");

            //Get TA Fee Value From TAFee Table 
            double taFeeValue = Convert.ToDouble(excelTable.Rows[bandingRow][RatingColumn]);
            TAFee = Convert.ToDouble(Banding * taFeeValue);
        }
        //TAFee Caluclations Based On NewBuild SOW 
        public void TAFeeNewBuildSOWCaluclations(plotrows plot)
        {
            // Reading TA Fee Table From Newhomes Pricing Setup Book            
            var TAFeeNewBuildSOWTableRow = -1;
            var TAFeeStagesOfWorksRow = -1;

            //Get TA Fee New Build SOW Table from Structural Premium Table
            for (int i = 0; i < excelTable.Rows.Count; i++)
            {
                if (excelTable.Rows[i][0].ToString() == "TA Fee New Build SoW Loading")
                {
                    TAFeeNewBuildSOWTableRow = i;
                    break;
                }
            }
            if (TAFeeNewBuildSOWTableRow == -1)
                throw new Exception("TA Fee New Build SoW Loading Table Not Found");


            //Get TA Fee New Build SoW Row from Structural Premium Table
            for (int i = TAFeeNewBuildSOWTableRow; i < excelTable.Rows.Count; i++)
            {
                var stageOfWorks = plot.StageOfWork == "No Works Started" ? "No Works" : plot.StageOfWork;
                if (excelTable.Rows[i][1].ToString().Contains(stageOfWorks.ToString()))
                {
                    TAFeeStagesOfWorksRow = i;
                    break;
                }

            }
            if (TAFeeStagesOfWorksRow == -1)
                throw new Exception("TA Fee New Build SoW Loading Row Not Found");

            //Get TA Fee Value From TAFee Table 
            double taFeeNewBuildSOWValue = Convert.ToDouble(excelTable.Rows[TAFeeStagesOfWorksRow][RatingColumn]);
            TAFee = Convert.ToDouble(TAFee * taFeeNewBuildSOWValue);
        }
        //TAFee Caluclations Based On Conversion SOW 
        public void TAFeeConversionSOWCaluclations(plotrows plot)
        {
            // Reading TA Fee Table From Newhomes Pricing Setup Book            
            var TAFeeConversionSOWTableRow = -1;
            var TAFeeStagesOfWorksRow = -1;

            //Get TA Fee New Build SOW Table from Structural Premium Table
            for (int i = 0; i < excelTable.Rows.Count; i++)
            {
                if (excelTable.Rows[i][0].ToString() == "TA Fee Conversion SoW Loading")
                {
                    TAFeeConversionSOWTableRow = i;
                    break;
                }
            }
            if (TAFeeConversionSOWTableRow == -1)
                throw new Exception("TA Fee Conversion SoW Loading Table Not Found");


            //Get TA Fee New Build SoW Row from Structural Premium Table
            for (int i = TAFeeConversionSOWTableRow; i < excelTable.Rows.Count; i++)
            {
                var stageOfWorks = plot.StageOfWork == "No Works Started" ? "No Works" : plot.StageOfWork;
                if (excelTable.Rows[i][1].ToString().Contains(stageOfWorks.ToString()))
                {
                    TAFeeStagesOfWorksRow = i;
                    break;
                }
            }
            if (TAFeeStagesOfWorksRow == -1)
                throw new Exception("TA Fee Conversion SoW Row Not Found");

            //Get TA Fee Value From TAFee Table 
            double taFeeConversionSOWValue = Convert.ToDouble(excelTable.Rows[TAFeeStagesOfWorksRow][RatingColumn].ToString());
            TAFee = Convert.ToDouble(TAFee * taFeeConversionSOWValue);           

        }

        //Minimum TAFee Caluclations Based On Conversion SOW 
        public void MinimumTAFeeCaluclations()
        {
            // Reading Minimum TA Fee Table From Newhomes Pricing Setup Book            
            var MinimumTAFeeTableRow = -1;
            var MinimumTAFeeRowBasedOnPlots = -1;

            //Get TA Fee New Build SOW Table from Structural Premium Table
            for (int i = 0; i < excelTable.Rows.Count; i++)
            {
                if (excelTable.Rows[i][0].ToString().Contains("Minimum Tafee"))
                {
                    MinimumTAFeeTableRow = i;
                    break;
                }
            }
            if (MinimumTAFeeTableRow == -1)
                throw new Exception("Minimum TA Fee Table Not Found");

            //Get TA FEE Banding Row from TA Fee Table
            for (int i = MinimumTAFeeTableRow; i < excelTable.Rows.Count; i++)
            {
                if (excelTable.Rows[i][1].ToString().Contains(Premiums.Plots))
                {
                    MinimumTAFeeRowBasedOnPlots = i;
                    break;
                }
            }
            if (MinimumTAFeeRowBasedOnPlots == -1)
                throw new Exception("Minimum TAFee Row Based On Plots Row Not Found");
            MinimumTAFee = Convert.ToDouble(excelTable.Rows[MinimumTAFeeRowBasedOnPlots][RatingColumn]);

            //Apply Minimum TAFee Value
            if (TAFee <= MinimumTAFee)
            {
                TAFee = Convert.ToDouble(MinimumTAFee);
            }

        }

        //Refurbishment Assessment Fee Caluclations  
        public void RefurbishmentFeeCaluclations()
        {
            // Reading Refurbishment Assessment Fee Table From Newhomes Pricing Setup Book            
            var refurbishmentTableRow = -1;
            var refurbishmentFeeRow = -1;

            //Get Refurbishment Assessment Fee Table from Structural Premium Table
            for (int i = 0; i < excelTable.Rows.Count; i++)
            {
                if (excelTable.Rows[i][0].ToString().Contains("Refurbishment Assessment Fee"))
                {
                    refurbishmentTableRow = i;
                    break;
                }
            }
            if (refurbishmentTableRow == -1)
                throw new Exception("Refurbishment Assessment Fee Table Not Found");

            //Get Refurbishment FEE  Row from
            for (int i = refurbishmentTableRow; i < excelTable.Rows.Count; i++)
            {
                if (excelTable.Rows[i][1].ToString().Contains("Fee"))
                {
                    refurbishmentFeeRow = i;
                    break;
                }
            }
            if (refurbishmentFeeRow == -1)
                throw new Exception("Refurbishment Fee Row Not Found");
            double refurbishmentFeeValue = Convert.ToDouble(excelTable.Rows[refurbishmentFeeRow][RatingColumn]);
            RefurbishmentFee = Convert.ToDouble(refurbishmentFeeValue);
            //efurbishmentFee = Convert.ToDouble(RefurbishmentFee * Premiums.Plots.Count());
        }

        public void VerfiyPrivateRentalUIValues()
        {
            var productName = Premiums.eachplotrow.FirstOrDefault().ProductName;


            //Covers Elements 
            //Veridy UI Structural Values
            var uiStructuralFee = $"//th[@class='product-title'][text()='{productName}']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[text()='Structural']//following-sibling::td[@class='right-align'][1]";
            var StructuralFee = Driver.FindElement(By.XPath(uiStructuralFee)).Text.Replace("£", "");
            double UIStructuralFee = Math.Round(Convert.ToDouble(StructuralFee), 2);
            double StructuralFeeDiff;
            var uiStructuralIPTFee = $"//th[@class='product-title'][text()='{productName}']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[text()='Structural']//following-sibling::td[@class='right-align'][2]";
            var StructuralIPTFee = Driver.FindElement(By.XPath(uiStructuralIPTFee)).Text.Replace("£", "");
            double UIStructuralIPTFee = Math.Round(Convert.ToDouble(StructuralIPTFee), 2);
            double StructuralIPTDiff;
            var uiStructuralTotalFee = $"//th[@class='product-title'][text()='{productName}']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[text()='Structural']//following-sibling::th[@class='right-align'][1]";
            var StructuralTotalFee = Driver.FindElement(By.XPath(uiStructuralTotalFee)).Text.Replace("£", "");
            double UIStructuralTotalFee = Math.Round(Convert.ToDouble(StructuralTotalFee), 2);
            double StructuralTotalValue = Math.Round(Convert.ToDouble(UIStructuralFee + UIStructuralIPTFee), 2);
            double StructuralTotalDiff;

            if (UIStructuralFee != Premiums.StructuralFee || UIStructuralIPTFee != Premiums.StructuralIPTFee || UIStructuralTotalFee != StructuralTotalValue)
            {
                StructuralFeeDiff = Convert.ToDouble(UIStructuralFee - Premiums.StructuralFee);
                StructuralIPTDiff = Convert.ToDouble(UIStructuralIPTFee - Premiums.StructuralIPTFee);
                StructuralTotalDiff = Convert.ToDouble(UIStructuralTotalFee - StructuralTotalValue);
                if (StructuralFeeDiff >= -1.01 && StructuralFeeDiff <= 1.01)
                {
                    Premiums.StructuralFee = UIStructuralFee;
                }
                if (StructuralIPTDiff >= -1.01 && StructuralIPTDiff <= 1.01)
                {
                    Premiums.StructuralIPTFee = UIStructuralIPTFee;
                }
                if (StructuralTotalDiff >= -1.01 && StructuralTotalDiff <= 1.01)
                {
                    StructuralTotalValue = UIStructuralTotalFee;
                }

                Assert.AreEqual(UIStructuralFee, Premiums.StructuralFee, $"StructuralFee Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.EstimatedSalePrice}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                Assert.AreEqual(UIStructuralIPTFee, Premiums.StructuralIPTFee, $"StructuralIPTFee Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.EstimatedSalePrice}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                Assert.AreEqual(UIStructuralTotalFee, StructuralTotalValue, $"StructuralTotalFee Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.EstimatedSalePrice}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
            }
            else
            {

                Assert.AreEqual(UIStructuralFee, Premiums.StructuralFee, $"StructuralFee Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.EstimatedSalePrice}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                Assert.AreEqual(UIStructuralIPTFee, Premiums.StructuralIPTFee, $"StructuralIPTFee Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.EstimatedSalePrice}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                Assert.AreEqual(UIStructuralTotalFee, StructuralTotalValue, $"StructuralTotalFee Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.EstimatedSalePrice}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
            }
           
            if (Premiums.ProductsCovers.Any(x => x.Contains("Defects")))
            {
                //Veirfy UI Contaminated Land values
                var uiDefectsFee = $"//th[@class='product-title'][text()='{productName}']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[text()='Defects']//following-sibling::td[@class='right-align'][1]";
                var DefectsFee = Driver.FindElement(By.XPath(uiDefectsFee)).Text.Replace("£", "");
                double UIDefectsFee = Math.Round(Convert.ToDouble(DefectsFee), 2);
                double DefectsFeeDiff;
                var uiDefectsIPTFee = $"//th[@class='product-title'][text()='{productName}']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[text()='Defects']//following-sibling::td[@class='right-align'][2]";
                var DefectsIPTFee = Driver.FindElement(By.XPath(uiDefectsIPTFee)).Text.Replace("£", "");
                double UIDefectsIPTFee = Math.Round(Convert.ToDouble(DefectsIPTFee), 2);
                double DefectsIPTDiff;
                var uiDefectsTotalFee = $"//th[@class='product-title'][text()='{productName}']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[text()='Defects']//following-sibling::th[@class='right-align'][1]";
                var DefectsTotalFee = Driver.FindElement(By.XPath(uiDefectsTotalFee)).Text.Replace("£", "");
                double UIDefectsTotalFee = Math.Round(Convert.ToDouble(DefectsTotalFee), 2);
                double DefectsTotalValue = Math.Round(Convert.ToDouble(UIDefectsFee + UIDefectsIPTFee), 2);
                double DefectsTotalDiff;

                if (UIDefectsFee != Premiums.DefectsFee || UIDefectsIPTFee != Premiums.DefectsIPTFee || UIDefectsTotalFee != DefectsTotalValue)
                {
                    DefectsFeeDiff = Convert.ToDouble(UIDefectsFee - Premiums.DefectsFee);
                    DefectsIPTDiff = Convert.ToDouble(UIDefectsIPTFee - Premiums.DefectsIPTFee);
                    DefectsTotalDiff = Convert.ToDouble(UIDefectsTotalFee - DefectsTotalValue);
                    if (DefectsFeeDiff >= -1.01 && DefectsFeeDiff <= 1.01)
                    {
                        Premiums.DefectsFee = UIDefectsFee;
                    }
                    if (DefectsIPTDiff >= -1.01 && DefectsIPTDiff <= 1.01)
                    {
                        Premiums.DefectsIPTFee = UIDefectsIPTFee;
                    }
                    if (DefectsTotalDiff >= -1.01 && DefectsTotalDiff <= 1.01)
                    {
                        DefectsTotalValue = UIDefectsTotalFee;
                    }

                    Assert.AreEqual(UIDefectsFee, Premiums.DefectsFee, $"DefectsFee Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.EstimatedSalePrice}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UIDefectsIPTFee, Premiums.DefectsIPTFee, $"DefectsIPTFee Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.EstimatedSalePrice}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UIDefectsTotalFee, DefectsTotalValue, $"DefectsTotalFee Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.EstimatedSalePrice}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                }
                else
                {
                    Assert.AreEqual(UIDefectsFee, Premiums.DefectsFee, $"DefectsFee Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.EstimatedSalePrice}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UIDefectsIPTFee, Premiums.DefectsIPTFee, $"DefectsIPTFee Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.EstimatedSalePrice}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UIDefectsTotalFee, DefectsTotalValue, $"DefectsTotalFee Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.EstimatedSalePrice}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                }
            }
            if (Premiums.ProductsCovers.Any(x => x.Contains("Contaminated Land")))
            {
                //Veirfy UI Contaminated Land values
                var uiContaminatedLandFee = $"//th[@class='product-title'][text()='{productName}']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[text()='Contaminated Land']//following-sibling::td[@class='right-align'][1]";
                var ContaminatedLandFee = Driver.FindElement(By.XPath(uiContaminatedLandFee)).Text.Replace("£", "");
                double UIContaminatedLandFee = Math.Round(Convert.ToDouble(ContaminatedLandFee), 2);
                double ContaminatedLandFeeDiff;
                var uiContaminatedLandIPTFee = $"//th[@class='product-title'][text()='{productName}']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[text()='Contaminated Land']//following-sibling::td[@class='right-align'][2]";
                var ContaminatedLandIPTFee = Driver.FindElement(By.XPath(uiContaminatedLandIPTFee)).Text.Replace("£", "");
                double UIContaminatedLandIPTFee = Math.Round(Convert.ToDouble(ContaminatedLandIPTFee), 2);
                double ContaminatedLandIPTDiff;
                var uiContaminatedLandTotalFee = $"//th[@class='product-title'][text()='{productName}']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[text()='Contaminated Land']//following-sibling::th[@class='right-align'][1]";
                var ContaminatedLandTotalFee = Driver.FindElement(By.XPath(uiContaminatedLandTotalFee)).Text.Replace("£", "");
                double UIContaminatedLandTotalFee = Math.Round(Convert.ToDouble(ContaminatedLandTotalFee), 2);
                double ContaminatedLandTotalValue = Math.Round(Convert.ToDouble(UIContaminatedLandFee + UIContaminatedLandIPTFee), 2);
                double ContaminatedLandTotalDiff;

                if (UIContaminatedLandFee != Premiums.ContaminatedLandFee || UIContaminatedLandIPTFee != Premiums.ContaminatedLandIPTFee || UIContaminatedLandTotalFee != ContaminatedLandTotalValue)
                {
                    ContaminatedLandFeeDiff = Convert.ToDouble(UIContaminatedLandFee - Premiums.ContaminatedLandFee);
                    ContaminatedLandIPTDiff = Convert.ToDouble(UIContaminatedLandIPTFee - Premiums.ContaminatedLandIPTFee);
                    ContaminatedLandTotalDiff = Convert.ToDouble(UIContaminatedLandTotalFee - ContaminatedLandTotalValue);
                    if (ContaminatedLandFeeDiff >= -1.01 && ContaminatedLandFeeDiff <= 1.01)
                    {
                        Premiums.ContaminatedLandFee = UIContaminatedLandFee;
                    }
                    if (ContaminatedLandIPTDiff >= -1.01 && ContaminatedLandIPTDiff <= 1.01)
                    {
                        Premiums.ContaminatedLandIPTFee = UIContaminatedLandIPTFee;
                    }
                    if (ContaminatedLandTotalDiff >= -1.01 && ContaminatedLandTotalDiff <= 1.01)
                    {
                        ContaminatedLandTotalValue = UIContaminatedLandTotalFee;
                    }

                    Assert.AreEqual(UIContaminatedLandFee, Premiums.ContaminatedLandFee, $"ContaminatedLandFee Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.EstimatedSalePrice}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UIContaminatedLandIPTFee, Premiums.ContaminatedLandIPTFee, $"ContaminatedLandIPTFee Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.EstimatedSalePrice}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UIContaminatedLandTotalFee, ContaminatedLandTotalValue, $"ContaminatedLandTotalFee Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.EstimatedSalePrice}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                }
                else
                {
                    Assert.AreEqual(UIContaminatedLandFee, Premiums.ContaminatedLandFee, $"ContaminatedLandFee Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.EstimatedSalePrice}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UIContaminatedLandIPTFee, Premiums.ContaminatedLandIPTFee, $"ContaminatedLandIPTFee Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.EstimatedSalePrice}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UIContaminatedLandTotalFee, ContaminatedLandTotalValue, $"ContaminatedLandTotalFee Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.EstimatedSalePrice}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                }
            }
            if (Premiums.ProductsCovers.Any(x => x.Contains("Additional Cover for Local Authority Building Control Function")))
            {

                //Verify UI Local Authority Building Control Values
                var uiLocalAuthorityBuildingControlFee = $"//th[@class='product-title'][text()='{productName}']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[text()='Additional Cover for Local Authority Building Control Function']//following-sibling::td[@class='right-align'][1]";
                var LocalAuthorityBuildingControlFee = Driver.FindElement(By.XPath(uiLocalAuthorityBuildingControlFee)).Text.Replace("£", "");
                double UILocalAuthorityBuildingControlFee = Math.Round(Convert.ToDouble(LocalAuthorityBuildingControlFee), 2);
                double LocalAuthorityBuildingControlFeeDiff;

                var uiLocalAuthorityBuildingControlIPTFee = $"//th[@class='product-title'][text()='{productName}']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[text()='Additional Cover for Local Authority Building Control Function']//following-sibling::td[@class='right-align'][2]";
                var LocalAuthorityBuildingControlIPTFee = Driver.FindElement(By.XPath(uiLocalAuthorityBuildingControlIPTFee)).Text.Replace("£", "");
                double UILocalAuthorityBuildingControlIPTFee = Math.Round(Convert.ToDouble(LocalAuthorityBuildingControlIPTFee), 2);
                double LocalAuthorityBuildingControlIPTDiff;

                var uiLocalAuthorityBuildingControlTotalFee = $"//th[@class='product-title'][text()='{productName}']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[text()='Additional Cover for Local Authority Building Control Function']//following-sibling::th[@class='right-align'][1]";
                var LocalAuthorityBuildingControlTotalFee = Driver.FindElement(By.XPath(uiLocalAuthorityBuildingControlTotalFee)).Text.Replace("£", "");
                double UILocalAuthorityBuildingControlTotalFee = Math.Round(Convert.ToDouble(LocalAuthorityBuildingControlTotalFee), 2);
                double LocalAuthorityBuildingControlTotalValue = Math.Round(Convert.ToDouble(UILocalAuthorityBuildingControlFee + UILocalAuthorityBuildingControlIPTFee), 2);
                double LocalAuthorityBuildingControlTotalDiff;

                if (UILocalAuthorityBuildingControlFee != Premiums.LocalAuthorityBuildingControlFee || UILocalAuthorityBuildingControlIPTFee != Premiums.LocalAuthorityBuildingControlIPTFee || UILocalAuthorityBuildingControlTotalFee != LocalAuthorityBuildingControlTotalValue)
                {
                    LocalAuthorityBuildingControlFeeDiff = Convert.ToDouble(Premiums.LocalAuthorityBuildingControlFee - UILocalAuthorityBuildingControlFee);
                    LocalAuthorityBuildingControlIPTDiff = Convert.ToDouble(Premiums.LocalAuthorityBuildingControlIPTFee - UILocalAuthorityBuildingControlIPTFee);
                    LocalAuthorityBuildingControlTotalDiff = Convert.ToDouble(LocalAuthorityBuildingControlTotalValue - UILocalAuthorityBuildingControlTotalFee);
                    if (LocalAuthorityBuildingControlFeeDiff >= -1.01 && LocalAuthorityBuildingControlFeeDiff <= 1.01)
                    {
                        Premiums.LocalAuthorityBuildingControlFee = UILocalAuthorityBuildingControlFee;
                    }
                    if (LocalAuthorityBuildingControlIPTDiff >= -1.01 && LocalAuthorityBuildingControlIPTDiff <= 1.01)
                    {
                        Premiums.LocalAuthorityBuildingControlIPTFee = UILocalAuthorityBuildingControlIPTFee;
                    }
                    if (LocalAuthorityBuildingControlTotalDiff >= -1.01 && LocalAuthorityBuildingControlTotalDiff <= 1.01)
                    {
                        LocalAuthorityBuildingControlTotalValue = UILocalAuthorityBuildingControlTotalFee;
                    }

                    Assert.AreEqual(UILocalAuthorityBuildingControlFee, Premiums.LocalAuthorityBuildingControlFee, $"LocalAuthorityBuildingControlFee Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UILocalAuthorityBuildingControlIPTFee, Premiums.LocalAuthorityBuildingControlIPTFee, $"LocalAuthorityBuildingControlIPTFee Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UILocalAuthorityBuildingControlTotalFee, LocalAuthorityBuildingControlTotalValue, $"LocalAuthorityBuildingControlTotalFee Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                }
                else
                {
                    Assert.AreEqual(UILocalAuthorityBuildingControlFee, Premiums.LocalAuthorityBuildingControlFee, $"LocalAuthorityBuildingControlFee Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UILocalAuthorityBuildingControlIPTFee, Premiums.LocalAuthorityBuildingControlIPTFee, $"LocalAuthorityBuildingControlIPTFee Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UILocalAuthorityBuildingControlTotalFee, LocalAuthorityBuildingControlTotalValue, $"LocalAuthorityBuildingControlTotalFee Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                }

            }
            if (Premiums.ProductsCovers.Any(x => x.Contains("Insolvency of the Builder")))
            {
                //Veirfy UI Contaminated Land values
                var uiInsolvencyFee = $"//th[@class='product-title'][text()='{productName}']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[text()='Insolvency of the Builder']//following-sibling::td[@class='right-align'][1]";
                var InsolvencyFee = Driver.FindElement(By.XPath(uiInsolvencyFee)).Text.Replace("£", "");
                double UIInsolvencyFee = Math.Round(Convert.ToDouble(InsolvencyFee), 2);
                double InsolvencyFeeDiff;
                var uiInsolvencyIPTFee = $"//th[@class='product-title'][text()='{productName}']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[text()='Insolvency of the Builder']//following-sibling::td[@class='right-align'][2]";
                var InsolvencyIPTFee = Driver.FindElement(By.XPath(uiInsolvencyIPTFee)).Text.Replace("£", "");
                double UIInsolvencyIPTFee = Math.Round(Convert.ToDouble(InsolvencyIPTFee), 2);
                double InsolvencyIPTDiff;
                var uiInsolvencyTotalFee = $"//th[@class='product-title'][text()='{productName}']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[text()='Insolvency of the Builder']//following-sibling::th[@class='right-align'][1]";
                var InsolvencyTotalFee = Driver.FindElement(By.XPath(uiInsolvencyTotalFee)).Text.Replace("£", "");
                double UIInsolvencyTotalFee = Math.Round(Convert.ToDouble(InsolvencyTotalFee), 2);
                double InsolvencyTotalValue = Math.Round(Convert.ToDouble(UIInsolvencyFee + UIInsolvencyIPTFee), 2);
                double InsolvencyTotalDiff;

                if (UIInsolvencyFee != Premiums.InsolvencyFee || UIInsolvencyIPTFee != Premiums.InsolvencyIPTFee || UIInsolvencyTotalFee != InsolvencyTotalValue)
                {
                    InsolvencyFeeDiff = Convert.ToDouble(Premiums.InsolvencyFee - UIInsolvencyFee);
                    InsolvencyIPTDiff = Convert.ToDouble(Premiums.InsolvencyIPTFee - UIInsolvencyIPTFee);
                    InsolvencyTotalDiff = Convert.ToDouble(InsolvencyTotalValue - UIInsolvencyTotalFee);
                    if (InsolvencyFeeDiff >= -1.01 && InsolvencyFeeDiff <= 1.01)
                    {
                        Premiums.InsolvencyFee = UIInsolvencyFee;
                    }
                    if (InsolvencyIPTDiff >= -1.01 && InsolvencyIPTDiff <= 1.01)
                    {
                        Premiums.InsolvencyIPTFee = UIInsolvencyIPTFee;
                    }
                    if (InsolvencyTotalDiff >= -1.01 && InsolvencyTotalDiff <= 1.01)
                    {
                        InsolvencyTotalValue = UIInsolvencyTotalFee;
                    }

                    Assert.AreEqual(UIInsolvencyFee, Premiums.InsolvencyFee, $"InsolvencyFee Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.EstimatedSalePrice}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UIInsolvencyIPTFee, Premiums.InsolvencyIPTFee, $"InsolvencyIPTFee Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.EstimatedSalePrice}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UIInsolvencyTotalFee, InsolvencyTotalValue, $"InsolvencyTotalFee Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.EstimatedSalePrice}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                }
                else
                {
                    Assert.AreEqual(UIInsolvencyFee, Premiums.InsolvencyFee, $"InsolvencyFee Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.EstimatedSalePrice}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UIInsolvencyIPTFee, Premiums.InsolvencyIPTFee, $"InsolvencyIPTFee Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.EstimatedSalePrice}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UIInsolvencyTotalFee, InsolvencyTotalValue, $"InsolvencyTotalFee Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.EstimatedSalePrice}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                }
            }
            if (Premiums.ProductsCovers.Any(x => x.Contains("Loss of Rent")))
            {

                //ui Loss of Rent  Values
                var uiLossOfRentFee = $"//th[@class='product-title'][text()='{productName}']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[text()='Loss of Rent']//following-sibling::td[@class='right-align'][1]";
                var LossOfRentFee = Driver.FindElement(By.XPath(uiLossOfRentFee)).Text.Replace("£", "");
                double UILossOfRentFee = Math.Round(Convert.ToDouble(LossOfRentFee), 2);
                double LossOfRentFeeDiff;

                var uiLossOfRentIPTFee = $"//th[@class='product-title'][text()='{productName}']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[text()='Loss of Rent']//following-sibling::td[@class='right-align'][2]";
                var LossOfRentIPTFee = Driver.FindElement(By.XPath(uiLossOfRentIPTFee)).Text.Replace("£", "");
                double UILossOfRentIPTFee = Math.Round(Convert.ToDouble(LossOfRentIPTFee), 2);
                double LossOfRentIPTDiff;

                var uiLossOfRentTotalFee = $"//th[@class='product-title'][text()='{productName}']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[text()='Loss of Rent']//following-sibling::th[@class='right-align'][1]";
                var LossOfRentTotalFee = Driver.FindElement(By.XPath(uiLossOfRentTotalFee)).Text.Replace("£", "");
                double UILossOfRentTotalFee = Math.Round(Convert.ToDouble(LossOfRentTotalFee), 2);
                double LossOfRentTotalValue = Math.Round(Convert.ToDouble(UILossOfRentFee + UILossOfRentIPTFee), 2);
                double LossOfRentTotalDiff;

                if (UILossOfRentFee != Premiums.LossOfRentFee || UILossOfRentIPTFee != Premiums.LossOfRentIPTFee || UILossOfRentTotalFee != LossOfRentTotalValue)
                {
                    LossOfRentFeeDiff = Convert.ToDouble(UILossOfRentFee - Premiums.LossOfRentFee);
                    LossOfRentIPTDiff = Convert.ToDouble(UILossOfRentIPTFee - Premiums.LossOfRentIPTFee);
                    LossOfRentTotalDiff = Convert.ToDouble(UILossOfRentTotalFee - LossOfRentTotalValue);
                    if (LossOfRentFeeDiff >= -1.01 && LossOfRentFeeDiff <= 1.01)
                    {
                        Premiums.LossOfRentFee = UILossOfRentFee;
                    }
                    if (LossOfRentIPTDiff >= -1.01 && LossOfRentIPTDiff <= 1.01)
                    {
                        Premiums.LossOfRentIPTFee = UILossOfRentIPTFee;
                    }
                    if (LossOfRentTotalDiff >= -1.01 && LossOfRentTotalDiff <= 1.01)
                    {
                        LossOfRentTotalValue = UILossOfRentTotalFee;
                    }

                    Assert.AreEqual(UILossOfRentFee, Premiums.LossOfRentFee, $"LossOfRentFee Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UILossOfRentIPTFee, Premiums.LossOfRentIPTFee, $"LossOfRentIPTFee Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UILossOfRentTotalFee, LossOfRentTotalValue, $"LossOfRentTotalValue Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                }
                else
                {
                    Assert.AreEqual(UILossOfRentFee, Premiums.LossOfRentFee, $"LossOfRentFee Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UILossOfRentIPTFee, Premiums.LossOfRentIPTFee, $"LossOfRentIPTFee Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UILossOfRentTotalFee, LossOfRentTotalValue, $"LossOfRentTotalValue Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                }
            }
            if (Premiums.ProductsCovers.Any(x => x.Contains("Plastering")))
            {

                //ui Loss of Rent Payable Values
                var uiPlasteringFee = $"//th[@class='product-title'][text()='{productName}']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[text()='Plastering']//following-sibling::td[@class='right-align'][1]";
                var PlasteringFee = Driver.FindElement(By.XPath(uiPlasteringFee)).Text.Replace("£", "");
                double UIPlasteringFee = Math.Round(Convert.ToDouble(PlasteringFee), 2);
                double PlasteringFeeDiff;

                var uiPlasteringIPTFee = $"//th[@class='product-title'][text()='{productName}']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[text()='Plastering']//following-sibling::td[@class='right-align'][2]";
                var PlasteringIPTFee = Driver.FindElement(By.XPath(uiPlasteringIPTFee)).Text.Replace("£", "");
                double UIPlasteringIPTFee = Math.Round(Convert.ToDouble(PlasteringIPTFee), 2);
                double PlasteringIPTDiff;

                var uiPlasteringTotalFee = $"//th[@class='product-title'][text()='{productName}']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[text()='Plastering']//following-sibling::th[@class='right-align'][1]";
                var PlasteringTotalFee = Driver.FindElement(By.XPath(uiPlasteringTotalFee)).Text.Replace("£", "");
                double UIPlasteringTotalFee = Math.Round(Convert.ToDouble(PlasteringTotalFee), 2);
                double PlasteringTotalValue = Math.Round(Convert.ToDouble(UIPlasteringFee + UIPlasteringIPTFee), 2);
                double PlasteringTotalDiff;

                if (UIPlasteringFee != Premiums.PlasteringFee || UIPlasteringIPTFee != Premiums.PlasteringIPTFee || UIPlasteringTotalFee != PlasteringTotalValue)
                {
                    PlasteringFeeDiff = Convert.ToDouble(UIPlasteringFee - Premiums.PlasteringFee);
                    PlasteringIPTDiff = Convert.ToDouble(UIPlasteringIPTFee - Premiums.PlasteringIPTFee);
                    PlasteringTotalDiff = Convert.ToDouble(UIPlasteringTotalFee - PlasteringTotalValue);
                    if (PlasteringFeeDiff >= -1.01 && PlasteringFeeDiff <= 1.01)
                    {
                        Premiums.PlasteringFee = UIPlasteringFee;
                    }
                    if (PlasteringIPTDiff >= -1.01 && PlasteringIPTDiff <= 1.01)
                    {
                        Premiums.PlasteringIPTFee = UIPlasteringIPTFee;
                    }
                    if (PlasteringTotalDiff >= -1.01 && PlasteringTotalDiff <= 1.01)
                    {
                        PlasteringTotalValue = UIPlasteringTotalFee;
                    }

                    Assert.AreEqual(UIPlasteringFee, Premiums.PlasteringFee, $"PlasteringFee Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UIPlasteringIPTFee, Premiums.PlasteringIPTFee, $"PlasteringIPTFee Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UIPlasteringTotalFee, PlasteringTotalValue, $"PlasteringTotalValue Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                }
                else
                {
                    Assert.AreEqual(UIPlasteringFee, Premiums.PlasteringFee, $"PlasteringFee Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UIPlasteringIPTFee, Premiums.PlasteringIPTFee, $"PlasteringIPTFee Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UIPlasteringTotalFee, PlasteringTotalValue, $"PlasteringTotalValue Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                }
            }
            if (Premiums.ProductsCovers.Any(x => x.Contains("Sound Transmission")))
            {

                //ui Loss of Rent Payable Values
                var uiSoundOfTransmissionFee = $"//th[@class='product-title'][text()='{productName}']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[text()='Sound Transmission']//following-sibling::td[@class='right-align'][1]";
                var SoundOfTransmissionFee = Driver.FindElement(By.XPath(uiSoundOfTransmissionFee)).Text.Replace("£", "");
                double UISoundOfTransmissionFee = Math.Round(Convert.ToDouble(SoundOfTransmissionFee), 2);
                double SoundOfTransmissionFeeDiff;

                var uiSoundOfTransmissionIPTFee = $"//th[@class='product-title'][text()='{productName}']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[text()='Sound Transmission']//following-sibling::td[@class='right-align'][2]";
                var SoundOfTransmissionIPTFee = Driver.FindElement(By.XPath(uiSoundOfTransmissionIPTFee)).Text.Replace("£", "");
                double UISoundOfTransmissionIPTFee = Math.Round(Convert.ToDouble(SoundOfTransmissionIPTFee), 2);
                double SoundOfTransmissionIPTDiff;

                var uiSoundOfTransmissionTotalFee = $"//th[@class='product-title'][text()='{productName}']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[text()='Sound Transmission']//following-sibling::th[@class='right-align'][1]";
                var SoundOfTransmissionTotalFee = Driver.FindElement(By.XPath(uiSoundOfTransmissionTotalFee)).Text.Replace("£", "");
                double UISoundOfTransmissionTotalFee = Math.Round(Convert.ToDouble(SoundOfTransmissionTotalFee), 2);
                double SoundOfTransmissionTotalValue = Math.Round(Convert.ToDouble(UISoundOfTransmissionFee + UISoundOfTransmissionIPTFee), 2);
                double SoundOfTransmissionTotalDiff;

                if (UISoundOfTransmissionFee != Premiums.SoundTransmissionFee || UISoundOfTransmissionIPTFee != Premiums.SoundTransmissionIPTFee || UISoundOfTransmissionTotalFee != SoundOfTransmissionTotalValue)
                {
                    SoundOfTransmissionFeeDiff = Convert.ToDouble(UISoundOfTransmissionFee - Premiums.SoundTransmissionFee);
                    SoundOfTransmissionIPTDiff = Convert.ToDouble(UISoundOfTransmissionIPTFee - Premiums.SoundTransmissionIPTFee);
                    SoundOfTransmissionTotalDiff = Convert.ToDouble(UISoundOfTransmissionTotalFee - SoundOfTransmissionTotalValue);
                    if (SoundOfTransmissionFeeDiff >= -1.01 && SoundOfTransmissionFeeDiff <= 1.01)
                    {
                        Premiums.SoundTransmissionFee = UISoundOfTransmissionFee;
                    }
                    if (SoundOfTransmissionIPTDiff >= -1.01 && SoundOfTransmissionIPTDiff <= 1.01)
                    {
                        Premiums.SoundTransmissionIPTFee = UISoundOfTransmissionIPTFee;
                    }
                    if (SoundOfTransmissionTotalDiff >= -1.01 && SoundOfTransmissionTotalDiff <= 1.01)
                    {
                        SoundOfTransmissionTotalValue = UISoundOfTransmissionTotalFee;
                    }

                    Assert.AreEqual(UISoundOfTransmissionFee, Premiums.SoundTransmissionFee, $"SoundOfTransmissionFee Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UISoundOfTransmissionIPTFee, Premiums.SoundTransmissionIPTFee, $"SoundOfTransmissionIPTFee Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UISoundOfTransmissionTotalFee, SoundOfTransmissionTotalValue, $"SoundOfTransmissionTotalValue Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                }
                else
                {
                    Assert.AreEqual(UISoundOfTransmissionFee, Premiums.SoundTransmissionFee, $"SoundOfTransmissionFee Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UISoundOfTransmissionIPTFee, Premiums.SoundTransmissionIPTFee, $"SoundOfTransmissionIPTFee Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UISoundOfTransmissionTotalFee, SoundOfTransmissionTotalValue, $"SoundOfTransmissionTotalValue Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.ReconstructionCost}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                }
            }

            //Services Elements  and  Fee values  
            //Technical Audit Fee values  
            if (Premiums.ProductServices.Any(x => x.Contains("Technical Audit Fee")))
            {
                var uiTechnicalAuditFee = $"//th[@class='product-title'][text()='{productName}']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[text()='Technical Audit Fee']//following-sibling::td[@class='right-align'][1]";
                var TechnicalAuditFee = Driver.FindElement(By.XPath(uiTechnicalAuditFee)).Text.Replace("£", "");
                double UITechnicalAuditFee = Math.Round(Convert.ToDouble(TechnicalAuditFee), 2);
                double TechnicalAuditFeeDiff;

                var uiTechnicalAuditVATFee = $"//th[@class='product-title'][text()='{productName}']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[text()='Technical Audit Fee']//following-sibling::td[@class='right-align'][2]";
                var TechnicalAuditVATFee = Driver.FindElement(By.XPath(uiTechnicalAuditVATFee)).Text.Replace("£", "");
                double UITechnicalAuditVATFee = Math.Round(Convert.ToDouble(TechnicalAuditVATFee), 2);
                double TechnicalAuditVATDiff;

                var uiTechnicalAuditTotalFee = $"//th[@class='product-title'][text()='{productName}']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[text()='Technical Audit Fee']//following-sibling::th[@class='right-align'][1]";
                var TechnicalAuditTotalFee = Driver.FindElement(By.XPath(uiTechnicalAuditTotalFee)).Text.Replace("£", "");
                double UITechnicalAuditTotalFee = Math.Round(Convert.ToDouble(TechnicalAuditTotalFee), 2);
                double TechnicalAuditTotalValue = Math.Round(Convert.ToDouble(UITechnicalAuditFee + UITechnicalAuditVATFee), 2);
                double TechnicalAuditTotalDiff;

                if (UITechnicalAuditFee != Premiums.TechnicalAuditFee || UITechnicalAuditVATFee != Premiums.TechnicalAuditVATFee || UITechnicalAuditTotalFee != TechnicalAuditTotalValue)
                {
                    TechnicalAuditFeeDiff = Convert.ToDouble(UITechnicalAuditFee - Premiums.TechnicalAuditFee);
                    TechnicalAuditVATDiff = Convert.ToDouble(UITechnicalAuditVATFee - Premiums.TechnicalAuditVATFee);
                    TechnicalAuditTotalDiff = Convert.ToDouble(UITechnicalAuditTotalFee - TechnicalAuditTotalValue);
                    if (TechnicalAuditFeeDiff >= -1.01 && TechnicalAuditFeeDiff <= 1.01)
                    {
                        Premiums.TechnicalAuditFee = UITechnicalAuditFee;
                    }

                    if (TechnicalAuditVATDiff >= -1.01 && TechnicalAuditVATDiff <= 1.01)
                    {
                        Premiums.TechnicalAuditVATFee = UITechnicalAuditVATFee;
                    }
                    if (TechnicalAuditTotalDiff >= -1.01 && TechnicalAuditTotalDiff <= 1.01)
                    {
                        TechnicalAuditTotalValue = UITechnicalAuditTotalFee;
                    }

                    Assert.AreEqual(UITechnicalAuditFee, Premiums.TechnicalAuditFee, $"TechnicalAuditFee Services Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.EstimatedSalePrice}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UITechnicalAuditVATFee, Premiums.TechnicalAuditVATFee, $"TechnicalAuditVATFee Services Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.EstimatedSalePrice}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UITechnicalAuditTotalFee, TechnicalAuditTotalValue, $"TechnicalAuditTotalValue Services Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.EstimatedSalePrice}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                }
                else
                {
                    Assert.AreEqual(UITechnicalAuditFee, Premiums.TechnicalAuditFee, $"TechnicalAuditFee Services Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.EstimatedSalePrice}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UITechnicalAuditVATFee, Premiums.TechnicalAuditVATFee, $"TechnicalAuditVATFee Services Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.EstimatedSalePrice}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UITechnicalAuditTotalFee, TechnicalAuditTotalValue, $"TechnicalAuditTotalValue Services Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.EstimatedSalePrice}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");

                }
            }

            //Refurbishment Assessment Fee values  
            if (Premiums.ProductServices.Any(x => x.Contains("Refurbishment Assessment Fee")))
            {
                var uiRefurbishmentFee = $"//th[@class='product-title'][text()='{productName}']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[text()='Refurbishment Assessment Fee']//following-sibling::td[@class='right-align'][1]";
                var RefurbishmentFee = Driver.FindElement(By.XPath(uiRefurbishmentFee)).Text.Replace("£", "");
                double UIRefurbishmentFee = Math.Round(Convert.ToDouble(RefurbishmentFee), 2);
                double RefurbishmentFeeDiff;

                var uiRefurbishmentVATFee = $"//th[@class='product-title'][text()='{productName}']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[text()='Refurbishment Assessment Fee']//following-sibling::td[@class='right-align'][2]";
                var RefurbishmentVATFee = Driver.FindElement(By.XPath(uiRefurbishmentVATFee)).Text.Replace("£", "");
                double UIRefurbishmentVATFee = Math.Round(Convert.ToDouble(RefurbishmentVATFee), 2);
                double RefurbishmentVATDiff;

                var uiRefurbishmentTotalFee = $"//th[@class='product-title'][text()='{productName}']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[text()='Refurbishment Assessment Fee']//following-sibling::th[@class='right-align'][1]";
                var RefurbishmentTotalFee = Driver.FindElement(By.XPath(uiRefurbishmentTotalFee)).Text.Replace("£", "");
                double UIRefurbishmentTotalFee = Math.Round(Convert.ToDouble(RefurbishmentTotalFee), 2);
                double RefurbishmentTotalValue = Math.Round(Convert.ToDouble(UIRefurbishmentFee + UIRefurbishmentVATFee), 2);
                double RefurbishmentTotalDiff;


                if (UIRefurbishmentFee != Premiums.RefurbishmentAssessmentFee || UIRefurbishmentVATFee != Premiums.RefurbishmentAssessmentVATFee || UIRefurbishmentTotalFee != RefurbishmentTotalValue)
                {
                    RefurbishmentFeeDiff = Convert.ToDouble(Premiums.RefurbishmentAssessmentFee - UIRefurbishmentFee);
                    RefurbishmentVATDiff = Convert.ToDouble(Premiums.RefurbishmentAssessmentVATFee - UIRefurbishmentVATFee);
                    RefurbishmentTotalDiff = Convert.ToDouble(RefurbishmentTotalValue - UIRefurbishmentTotalFee);
                    if (RefurbishmentFeeDiff >= -1.01 && RefurbishmentFeeDiff <= 1.01)
                    {
                        Premiums.RefurbishmentAssessmentFee = UIRefurbishmentFee;
                    }

                    if (RefurbishmentVATDiff >= -1.01 && RefurbishmentVATDiff <= 1.01)
                    {
                        Premiums.RefurbishmentAssessmentVATFee = UIRefurbishmentVATFee;
                    }
                    if (RefurbishmentTotalDiff >= -1.01 && RefurbishmentTotalDiff <= 1.01)
                    {
                        RefurbishmentTotalValue = UIRefurbishmentTotalFee;
                    }

                    Assert.AreEqual(UIRefurbishmentFee, Premiums.RefurbishmentAssessmentFee, $"RefurbishmentFee Services Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.EstimatedSalePrice}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UIRefurbishmentVATFee, Premiums.RefurbishmentAssessmentVATFee, $"RefurbishmentVATFee Services Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.EstimatedSalePrice}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UIRefurbishmentTotalFee, RefurbishmentTotalValue, $"RefurbishmentTotalValue Services Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.EstimatedSalePrice}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                }
                else
                {
                    Assert.AreEqual(UIRefurbishmentFee, Premiums.RefurbishmentAssessmentFee, $"RefurbishmentFee Services Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.EstimatedSalePrice}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UIRefurbishmentVATFee, Premiums.RefurbishmentAssessmentVATFee, $"RefurbishmentVATFee Services Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.EstimatedSalePrice}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UIRefurbishmentTotalFee, RefurbishmentTotalValue, $"RefurbishmentTotalValue Services Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.EstimatedSalePrice}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                }
            }

            //Administrator Fee values    
            if (Premiums.ProductServices.Any(x => x.Contains("Administration Fee")))
            {

                var uiAdministrationFee = $"//th[@class='product-title'][text()='{productName}']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[text()='Administration Fee']//following-sibling::td[@class='right-align'][1]";
                var AdministrationFee = Driver.FindElement(By.XPath(uiAdministrationFee)).Text.Replace("£", "");
                double UIAdministrationFee = Math.Round(Convert.ToDouble(AdministrationFee), 2);
                double AdministrationFeeDiff;

                var uiAdministrationVATFee = $"//th[@class='product-title'][text()='{productName}']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[text()='Administration Fee']//following-sibling::td[@class='right-align'][2]";
                var AdministrationVATFee = Driver.FindElement(By.XPath(uiAdministrationVATFee)).Text.Replace("£", "");
                double UIAdministrationVATFee = Math.Round(Convert.ToDouble(AdministrationVATFee), 2);
                double AdministrationVATDiff;

                var uiAdministrationTotalFee = $"//th[@class='product-title'][text()='{productName}']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[text()='Administration Fee']//following-sibling::th[@class='right-align'][1]";
                var AdministrationTotalFee = Driver.FindElement(By.XPath(uiAdministrationTotalFee)).Text.Replace("£", "");
                double UIAdministrationTotalFee = Math.Round(Convert.ToDouble(AdministrationTotalFee), 2);
                double AdministrationTotalValue = Math.Round(Convert.ToDouble(UIAdministrationFee + UIAdministrationVATFee), 2);
                double AdministrationTotalDiff;

                if (UIAdministrationFee != Premiums.AdministrationFee || UIAdministrationVATFee != Premiums.AdministrationVATFee || UIAdministrationTotalFee != AdministrationTotalValue)
                {
                    AdministrationFeeDiff = Convert.ToDouble(UIAdministrationFee - Premiums.AdministrationFee);
                    AdministrationVATDiff = Convert.ToDouble(UIAdministrationVATFee - Premiums.AdministrationVATFee);
                    AdministrationTotalDiff = Convert.ToDouble(UIAdministrationTotalFee - AdministrationTotalValue);
                    if (AdministrationFeeDiff >= -1.01 && AdministrationFeeDiff <= 1.01)
                    {
                        Premiums.AdministrationFee = UIAdministrationFee;
                    }

                    if (AdministrationVATDiff >= -1.01 && AdministrationVATDiff <= 1.01)
                    {
                        Premiums.AdministrationVATFee = UIAdministrationVATFee;
                    }
                    if (AdministrationTotalDiff >= 1.01 && AdministrationTotalDiff <= 1.01)
                    {
                        AdministrationTotalValue = UIAdministrationTotalFee;
                    }

                    Assert.AreEqual(UIAdministrationFee, Premiums.AdministrationFee, $"AdministrationFee Services Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.EstimatedSalePrice}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UIAdministrationVATFee, Premiums.AdministrationVATFee, $"AdministrationVATFee Services Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.EstimatedSalePrice}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UIAdministrationTotalFee, AdministrationTotalValue, $"AdministrationTotalValue Services Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.EstimatedSalePrice}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                }
                else
                {
                    Assert.AreEqual(UIAdministrationFee, Premiums.AdministrationFee, $"AdministrationFee Services Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.EstimatedSalePrice}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UIAdministrationVATFee, Premiums.AdministrationVATFee, $"AdministrationVATFee Services Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.EstimatedSalePrice}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UIAdministrationTotalFee, AdministrationTotalValue, $"AdministrationTotalValue Services Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.EstimatedSalePrice}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                }
            }
            //Cancellation Fee values  
            if (Premiums.ProductServices.Any(x => x.Contains("Cancellation Fee")))
            {


                var uiCancellationFee = $"//th[@class='product-title'][text()='{productName}']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[text()='Cancellation Fee']//following-sibling::td[@class='right-align'][2]";
                var CancellationFee = Driver.FindElement(By.XPath(uiCancellationFee)).Text.Replace("£", "");
                double UICancellationFee = Math.Round(Convert.ToDouble(CancellationFee), 2);
                double CancellationFeeDiff;

                var uiCancellationVATFee = $"//th[@class='product-title'][text()='{productName}']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[text()='Cancellation Fee']//following-sibling::td[@class='right-align'][2]";
                var CancellationVATFee = Driver.FindElement(By.XPath(uiCancellationVATFee)).Text.Replace("£", "");
                double UICancellationVATFee = Math.Round(Convert.ToDouble(CancellationVATFee), 2);
                double CancellationVATDiff;

                var uiCancellationTotalFee = $"//th[@class='product-title'][text()='{productName}']/parent::tr//parent::thead//following-sibling::tbody//tr[@class='item']//td[text()='Cancellation Fee']//following-sibling::td[@class='right-align'][2]";
                var CancellationTotalFee = Driver.FindElement(By.XPath(uiCancellationTotalFee)).Text.Replace("£", "");
                double UICancellationTotalFee = Math.Round(Convert.ToDouble(CancellationTotalFee), 2);
                double CancellationTotalValue = Math.Round(Convert.ToDouble(UICancellationFee + UICancellationVATFee), 2);
                double CancellationTotalDiff;


                if (UICancellationFee != Premiums.CancellationFee || UICancellationVATFee != Premiums.CancellationVATFee || UICancellationTotalFee != CancellationTotalValue)
                {
                    CancellationFeeDiff = Convert.ToDouble(UICancellationFee - Premiums.CancellationFee);
                    CancellationVATDiff = Convert.ToDouble(UICancellationVATFee - Premiums.CancellationVATFee);
                    CancellationTotalDiff = Convert.ToDouble(UICancellationTotalFee - CancellationTotalValue);
                    if (CancellationFeeDiff >= -1.01 && CancellationFeeDiff <= 1.01)
                    {
                        Premiums.CancellationFee = UICancellationFee;
                    }

                    if (CancellationVATDiff >= -1.01 && CancellationVATDiff <= 1.01)
                    {
                        Premiums.CancellationVATFee = UICancellationVATFee;
                    }
                    if (CancellationTotalDiff >= -1.01 && CancellationTotalDiff <= 1.01)
                    {
                        CancellationTotalValue = UICancellationTotalFee;
                    }

                    Assert.AreEqual(UICancellationFee, Premiums.CancellationFee, $"CancellationFee Services Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.EstimatedSalePrice}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UICancellationVATFee, Premiums.CancellationVATFee, $"CancellationVATFee Services Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.EstimatedSalePrice}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UICancellationTotalFee, CancellationTotalValue, $"CancellationTotalValue Services Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.EstimatedSalePrice}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                }
                else
                {
                    Assert.AreEqual(UICancellationFee, Premiums.CancellationFee, $"CancellationFee Services Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.EstimatedSalePrice}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UICancellationVATFee, Premiums.CancellationVATFee, $"CancellationVATFee Services Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.EstimatedSalePrice}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                    Assert.AreEqual(UICancellationTotalFee, CancellationTotalValue, $"CancellationTotalValue Services Details Are Not Matching on Product : {Premiums.ProductName},ReconstructionCost : {Premiums.EstimatedSalePrice}, Construction : {Premiums.ConstructionType}, UnitType : {Premiums.UnitType}, StagesOfWork : {Premiums.ProductName}");
                }
            }
        }

        public DataTable ReadPrivateRentalMasterPremiumsPriceData(string fileName)
        {

            var result = new DataSet();                                                                             //Set the First Row as Column Name
            using (var stream = File.Open(fileName, FileMode.Open, FileAccess.Read))
            using (var excelReader = ExcelReaderFactory.CreateOpenXmlReader(stream)) //.xlsx
            {
                //Return as DataSet
                result = excelReader.AsDataSet(new ExcelDataSetConfiguration()
                {
                    ConfigureDataTable = (_) => new ExcelDataTableConfiguration()
                    {
                        UseHeaderRow = true
                    }
                });
            }
            //Get all the Tables
            DataTableCollection table = result.Tables;
            //Reading Last Sheet From Product Pricing Setup 
            DataTable resultTable = table[table.Count - 1];
            return resultTable;
        }
    }
}