﻿Feature: LABCLVSSinglePlotsPremiums
Background: 
	Given I am on login page
	When I enter crisp username 
	And I enter crisp password
	And I click on login button
	Then I should see all items displayed on dashboard page
	
@RegressionTest
Scenario:Verify Premiums on LABC NewHomes single product 
When I Create a NewhomesQuote and Verify Premiums On Single Plot
Then I Should Successfully Verify the Premiums 


@RegressionTest
Scenario:Verify Premiums on LABC Commercial single product
When I Create a CommercialQuote and Verify Premiums On Single Plot
Then I Should Successfully Verify the Premiums 


@RegressionTest
Scenario:Verify Premiums on LABC SocialHousing single product
When I Create a SocialHousingQuote and Verify Premiums On Single Plot
Then I Should Successfully Verify the Premiums 


@RegressionTest
Scenario:Verify Premiums on LABC PrivateRental single product
When I Create a PrivateRentalQuote and Verify Premiums On Single Plot
Then I Should Successfully Verify the Premiums 


@RegressionTest
Scenario:Verify Premiums on LABC CompletedHousing single product
When I Create a CompletedHousingQuote and Verify Premiums On Single Plot
Then I Should Successfully Verify the Premiums 


Scenario:Verify Premiums on LABC SelfBuild single product
When I Create a SelfBuildQuote and Verify Premiums On Single Plot
Then I Should Successfully Verify the Premiums 

@RegressionTest
Scenario:Verify Premiums on LABC Commercial single product For Twelve Years Cover Length
When I Create a CommercialQuote For Twelve Years Cover Length and Verify Premiums On Single Plot
Then I Should Successfully Verify the Premiums 


@RegressionTest
Scenario:Verify Premiums on LABC SocialHousing single product For Twelve Years Cover Length
When I Create a SocialHousingQuote For Twelve Years Cover Length and Verify Premiums On Single Plot
Then I Should Successfully Verify the Premiums 


@RegressionTest
Scenario:Verify Premiums on LABC PrivateRental single product For Twelve Years Cover Length
When I Create a PrivateRentalQuote For Twelve Years Cover Length and Verify Premiums On Single Plot
Then I Should Successfully Verify the Premiums 