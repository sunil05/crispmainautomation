﻿// ------------------------------------------------------------------------------
//  <auto-generated>
//      This code was generated by SpecFlow (http://www.specflow.org/).
//      SpecFlow Version:2.4.0.0
//      SpecFlow Generator Version:2.4.0.0
// 
//      Changes to this file may cause incorrect behavior and will be lost if
//      the code is regenerated.
//  </auto-generated>
// ------------------------------------------------------------------------------
#region Designer generated code
#pragma warning disable
namespace CrispPremiums.Features
{
    using TechTalk.SpecFlow;
    
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("TechTalk.SpecFlow", "2.4.0.0")]
    [System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    [NUnit.Framework.TestFixtureAttribute()]
    [NUnit.Framework.DescriptionAttribute("PGLVSSinglePlotsPremiums")]
    public partial class PGLVSSinglePlotsPremiumsFeature
    {
        
        private TechTalk.SpecFlow.ITestRunner testRunner;
        
#line 1 "PGLVSSinglePlotPremiums.feature"
#line hidden
        
        [NUnit.Framework.OneTimeSetUpAttribute()]
        public virtual void FeatureSetup()
        {
            testRunner = TechTalk.SpecFlow.TestRunnerManager.GetTestRunner();
            TechTalk.SpecFlow.FeatureInfo featureInfo = new TechTalk.SpecFlow.FeatureInfo(new System.Globalization.CultureInfo("en-US"), "PGLVSSinglePlotsPremiums", null, ProgrammingLanguage.CSharp, ((string[])(null)));
            testRunner.OnFeatureStart(featureInfo);
        }
        
        [NUnit.Framework.OneTimeTearDownAttribute()]
        public virtual void FeatureTearDown()
        {
            testRunner.OnFeatureEnd();
            testRunner = null;
        }
        
        [NUnit.Framework.SetUpAttribute()]
        public virtual void TestInitialize()
        {
        }
        
        [NUnit.Framework.TearDownAttribute()]
        public virtual void ScenarioTearDown()
        {
            testRunner.OnScenarioEnd();
        }
        
        public virtual void ScenarioInitialize(TechTalk.SpecFlow.ScenarioInfo scenarioInfo)
        {
            testRunner.OnScenarioInitialize(scenarioInfo);
            testRunner.ScenarioContext.ScenarioContainer.RegisterInstanceAs<NUnit.Framework.TestContext>(NUnit.Framework.TestContext.CurrentContext);
        }
        
        public virtual void ScenarioStart()
        {
            testRunner.OnScenarioStart();
        }
        
        public virtual void ScenarioCleanup()
        {
            testRunner.CollectScenarioErrors();
        }
        
        public virtual void FeatureBackground()
        {
#line 2
#line 3
 testRunner.Given("I am on login page", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line 4
 testRunner.When("I enter crisp username", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line 5
 testRunner.And("I enter crisp password", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 6
 testRunner.And("I click on login button", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 7
 testRunner.Then("I should see all items displayed on dashboard page", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line hidden
        }
        
        [NUnit.Framework.TestAttribute()]
        [NUnit.Framework.DescriptionAttribute("Verify Premiums on PG  NewHomes single product")]
        [NUnit.Framework.CategoryAttribute("RegressionTest")]
        public virtual void VerifyPremiumsOnPGNewHomesSingleProduct()
        {
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("Verify Premiums on PG  NewHomes single product", null, new string[] {
                        "RegressionTest"});
#line 10
this.ScenarioInitialize(scenarioInfo);
            this.ScenarioStart();
#line 2
this.FeatureBackground();
#line 11
testRunner.When("I Create a PG NewhomesQuote and Verify Premiums On Single Plot", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line 12
testRunner.Then("I Should Successfully Verify the Premiums", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line hidden
            this.ScenarioCleanup();
        }
        
        [NUnit.Framework.TestAttribute()]
        [NUnit.Framework.DescriptionAttribute("Verify Premiums on PG Commercial single product")]
        [NUnit.Framework.CategoryAttribute("RegressionTest")]
        public virtual void VerifyPremiumsOnPGCommercialSingleProduct()
        {
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("Verify Premiums on PG Commercial single product", null, new string[] {
                        "RegressionTest"});
#line 16
this.ScenarioInitialize(scenarioInfo);
            this.ScenarioStart();
#line 2
this.FeatureBackground();
#line 17
testRunner.When("I Create a PG CommercialQuote and Verify Premiums On Single Plot", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line 18
testRunner.Then("I Should Successfully Verify the Premiums", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line hidden
            this.ScenarioCleanup();
        }
        
        [NUnit.Framework.TestAttribute()]
        [NUnit.Framework.DescriptionAttribute("Verify Premiums on PG SocialHousing single product")]
        [NUnit.Framework.CategoryAttribute("RegressionTest")]
        public virtual void VerifyPremiumsOnPGSocialHousingSingleProduct()
        {
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("Verify Premiums on PG SocialHousing single product", null, new string[] {
                        "RegressionTest"});
#line 22
this.ScenarioInitialize(scenarioInfo);
            this.ScenarioStart();
#line 2
this.FeatureBackground();
#line 23
testRunner.When("I Create a PG SocialHousingQuote and Verify Premiums On Single Plot", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line 24
testRunner.Then("I Should Successfully Verify the Premiums", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line hidden
            this.ScenarioCleanup();
        }
        
        [NUnit.Framework.TestAttribute()]
        [NUnit.Framework.DescriptionAttribute("Verify Premiums on PG PrivateRental  single product")]
        [NUnit.Framework.CategoryAttribute("RegressionTest")]
        public virtual void VerifyPremiumsOnPGPrivateRentalSingleProduct()
        {
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("Verify Premiums on PG PrivateRental  single product", null, new string[] {
                        "RegressionTest"});
#line 28
this.ScenarioInitialize(scenarioInfo);
            this.ScenarioStart();
#line 2
this.FeatureBackground();
#line 29
testRunner.When("I Create a PG PrivateRentalQuote and Verify Premiums On Single Plot", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line 30
testRunner.Then("I Should Successfully Verify the Premiums", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line hidden
            this.ScenarioCleanup();
        }
        
        [NUnit.Framework.TestAttribute()]
        [NUnit.Framework.DescriptionAttribute("Verify Premiums on PG CompletedHousing single product")]
        [NUnit.Framework.CategoryAttribute("RegressionTest")]
        public virtual void VerifyPremiumsOnPGCompletedHousingSingleProduct()
        {
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("Verify Premiums on PG CompletedHousing single product", null, new string[] {
                        "RegressionTest"});
#line 34
this.ScenarioInitialize(scenarioInfo);
            this.ScenarioStart();
#line 2
this.FeatureBackground();
#line 35
testRunner.When("I Create a PG CompletedHousingQuote and Verify Premiums On Single Plot", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line 36
testRunner.Then("I Should Successfully Verify the Premiums", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line hidden
            this.ScenarioCleanup();
        }
        
        [NUnit.Framework.TestAttribute()]
        [NUnit.Framework.DescriptionAttribute("Verify Premiums on PG SelfBuild single product")]
        [NUnit.Framework.CategoryAttribute("RegressionTest")]
        public virtual void VerifyPremiumsOnPGSelfBuildSingleProduct()
        {
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("Verify Premiums on PG SelfBuild single product", null, new string[] {
                        "RegressionTest"});
#line 39
this.ScenarioInitialize(scenarioInfo);
            this.ScenarioStart();
#line 2
this.FeatureBackground();
#line 40
testRunner.When("I Create a PG SelfBuildQuote and Verify Premiums On Single Plot", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line 41
testRunner.Then("I Should Successfully Verify the Premiums", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line hidden
            this.ScenarioCleanup();
        }
        
        [NUnit.Framework.TestAttribute()]
        [NUnit.Framework.DescriptionAttribute("Verify Premiums on CI Residential single product")]
        [NUnit.Framework.CategoryAttribute("RegressionTest")]
        public virtual void VerifyPremiumsOnCIResidentialSingleProduct()
        {
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("Verify Premiums on CI Residential single product", null, new string[] {
                        "RegressionTest"});
#line 44
this.ScenarioInitialize(scenarioInfo);
            this.ScenarioStart();
#line 2
this.FeatureBackground();
#line 45
testRunner.When("I Create a PG CI Residential Quote and Verify Premiums On Single Plot", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line 46
testRunner.Then("I Should Successfully Verify the Premiums", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line hidden
            this.ScenarioCleanup();
        }
        
        [NUnit.Framework.TestAttribute()]
        [NUnit.Framework.DescriptionAttribute("Verify Premiums on CICommercial single product")]
        [NUnit.Framework.CategoryAttribute("RegressionTest")]
        public virtual void VerifyPremiumsOnCICommercialSingleProduct()
        {
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("Verify Premiums on CICommercial single product", null, new string[] {
                        "RegressionTest"});
#line 49
this.ScenarioInitialize(scenarioInfo);
            this.ScenarioStart();
#line 2
this.FeatureBackground();
#line 50
testRunner.When("I Create a PG CI Commercial and Verify Premiums On Single Plot", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line 51
testRunner.Then("I Should Successfully Verify the Premiums", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line hidden
            this.ScenarioCleanup();
        }
        
        [NUnit.Framework.TestAttribute()]
        [NUnit.Framework.DescriptionAttribute("Verify Premiums on Residential Building Control single products")]
        [NUnit.Framework.CategoryAttribute("RegressionTest")]
        public virtual void VerifyPremiumsOnResidentialBuildingControlSingleProducts()
        {
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("Verify Premiums on Residential Building Control single products", null, new string[] {
                        "RegressionTest"});
#line 54
this.ScenarioInitialize(scenarioInfo);
            this.ScenarioStart();
#line 2
this.FeatureBackground();
#line 55
testRunner.When("I Create a Quote with Residential BC Products and Verify Premiums On Single Plot", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line 56
testRunner.Then("I Should Successfully Verify the Premiums", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line hidden
            this.ScenarioCleanup();
        }
        
        [NUnit.Framework.TestAttribute()]
        [NUnit.Framework.DescriptionAttribute("Verify Premiums on Commercial Building Control single products")]
        [NUnit.Framework.CategoryAttribute("RegressionTest")]
        public virtual void VerifyPremiumsOnCommercialBuildingControlSingleProducts()
        {
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("Verify Premiums on Commercial Building Control single products", null, new string[] {
                        "RegressionTest"});
#line 59
this.ScenarioInitialize(scenarioInfo);
            this.ScenarioStart();
#line 2
this.FeatureBackground();
#line 60
testRunner.When("I Create a Quote with Commercial BC Products and Verify Premiums On Single Plot", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line 61
testRunner.Then("I Should Successfully Verify the Premiums", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line hidden
            this.ScenarioCleanup();
        }
        
        [NUnit.Framework.TestAttribute()]
        [NUnit.Framework.DescriptionAttribute("Verify Premiums on PG Commercial single product For Twelve Years Cover Length")]
        [NUnit.Framework.CategoryAttribute("RegressionTest")]
        public virtual void VerifyPremiumsOnPGCommercialSingleProductForTwelveYearsCoverLength()
        {
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("Verify Premiums on PG Commercial single product For Twelve Years Cover Length", null, new string[] {
                        "RegressionTest"});
#line 66
this.ScenarioInitialize(scenarioInfo);
            this.ScenarioStart();
#line 2
this.FeatureBackground();
#line 67
testRunner.When("I Create a PG CommercialQuote For Twelve Years Cover Length and Verify Premiums O" +
                    "n Single Plot", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line 68
testRunner.Then("I Should Successfully Verify the Premiums", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line hidden
            this.ScenarioCleanup();
        }
        
        [NUnit.Framework.TestAttribute()]
        [NUnit.Framework.DescriptionAttribute("Verify Premiums on PG SocialHousing single product For Twelve Years Cover Length")]
        [NUnit.Framework.CategoryAttribute("RegressionTest")]
        public virtual void VerifyPremiumsOnPGSocialHousingSingleProductForTwelveYearsCoverLength()
        {
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("Verify Premiums on PG SocialHousing single product For Twelve Years Cover Length", null, new string[] {
                        "RegressionTest"});
#line 72
this.ScenarioInitialize(scenarioInfo);
            this.ScenarioStart();
#line 2
this.FeatureBackground();
#line 73
testRunner.When("I Create a PG SocialHousingQuote For Twelve Years Cover Length and Verify Premium" +
                    "s On Single Plot", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line 74
testRunner.Then("I Should Successfully Verify the Premiums", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line hidden
            this.ScenarioCleanup();
        }
        
        [NUnit.Framework.TestAttribute()]
        [NUnit.Framework.DescriptionAttribute("Verify Premiums on PG PrivateRental single product For Twelve Years Cover Length")]
        [NUnit.Framework.CategoryAttribute("RegressionTest")]
        public virtual void VerifyPremiumsOnPGPrivateRentalSingleProductForTwelveYearsCoverLength()
        {
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("Verify Premiums on PG PrivateRental single product For Twelve Years Cover Length", null, new string[] {
                        "RegressionTest"});
#line 78
this.ScenarioInitialize(scenarioInfo);
            this.ScenarioStart();
#line 2
this.FeatureBackground();
#line 79
testRunner.When("I Create a PG PrivateRentalQuote For Twelve Years Cover Length and Verify Premium" +
                    "s On Single Plot", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line 80
testRunner.Then("I Should Successfully Verify the Premiums", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line hidden
            this.ScenarioCleanup();
        }
        
        [NUnit.Framework.TestAttribute()]
        [NUnit.Framework.DescriptionAttribute("Verify Premiums on CI Residential single product For Twelve Years Cover Length")]
        [NUnit.Framework.CategoryAttribute("RegressionTest")]
        public virtual void VerifyPremiumsOnCIResidentialSingleProductForTwelveYearsCoverLength()
        {
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("Verify Premiums on CI Residential single product For Twelve Years Cover Length", null, new string[] {
                        "RegressionTest"});
#line 83
this.ScenarioInitialize(scenarioInfo);
            this.ScenarioStart();
#line 2
this.FeatureBackground();
#line 84
testRunner.When("I Create a PG CI Residential Quote For Twelve Years Cover Length and Verify Premi" +
                    "ums On Single Plot", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line 85
testRunner.Then("I Should Successfully Verify the Premiums", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line hidden
            this.ScenarioCleanup();
        }
        
        [NUnit.Framework.TestAttribute()]
        [NUnit.Framework.DescriptionAttribute("Verify Premiums on CICommercial single product For Twelve Years Cover Length")]
        [NUnit.Framework.CategoryAttribute("RegressionTest")]
        public virtual void VerifyPremiumsOnCICommercialSingleProductForTwelveYearsCoverLength()
        {
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("Verify Premiums on CICommercial single product For Twelve Years Cover Length", null, new string[] {
                        "RegressionTest"});
#line 88
this.ScenarioInitialize(scenarioInfo);
            this.ScenarioStart();
#line 2
this.FeatureBackground();
#line 89
testRunner.When("I Create a PG CI Commercial For Twelve Years Cover Length and Verify Premiums On " +
                    "Single Plot", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line 90
testRunner.Then("I Should Successfully Verify the Premiums", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line hidden
            this.ScenarioCleanup();
        }
    }
}
#pragma warning restore
#endregion
