﻿Feature: PGAvivaHVSPremiums
Background: 
	Given I am on login page
	When I enter crisp username 
	And I enter crisp password
	And I click on login button
	Then I should see all items displayed on dashboard page

@RegressionTest
Scenario:Verify Premiums on PG NewHomes HVS Aviva product
When I Create a PG Newhomes HVS Aviva Quote and Verify Premiums
Then I Should Successfully Verify the HVS Premiums 


@RegressionTest
Scenario:Verify Premiums on PG Commercial HVS Aviva product
When I Create a PG Commercial HVS Aviva Quote and Verify HVS Premiums
Then I Should Successfully Verify the HVS Premiums 


@RegressionTest
Scenario:Verify Premiums on PG SocialHousing HVS Aviva product
When I Create a PG SocialHousing HVS Aviva Quote and Verify HVS Premiums
Then I Should Successfully Verify the HVS Premiums 


@RegressionTest
Scenario:Verify Premiums on PG PrivateRental HVS Aviva product
When I Create a PG PrivateRental HVS Aviva Quote and Verify Premiums
Then I Should Successfully Verify the HVS Premiums 

@RegressionTest
Scenario:Verify Premiums on PG CI Residential HVS Aviva product
When I Create a PG CI Residential Aviva Quote and Verify Premiums
Then I Should Successfully Verify the HVS Premiums

@RegressionTest
Scenario:Verify Premiums on PG CICommercial HVS Aviva product
When I Create a PG CI Commercial Aviva Quote and Verify Premiums
Then I Should Successfully Verify the HVS Premiums


@RegressionTest
Scenario:Verify Premiums on PG Commercial HVS Aviva product For Twelve Years Cover Length
When I Create a PG Commercial HVS Aviva Quote For Twelve Years Cover Length and Verify HVS Premiums
Then I Should Successfully Verify the HVS Premiums 


@RegressionTest
Scenario:Verify Premiums on PG SocialHousing HVS Aviva product For Twelve Years Cover Length
When I Create a PG SocialHousing HVS Aviva Quote For Twelve Years Cover Length and Verify HVS Premiums
Then I Should Successfully Verify the HVS Premiums 

@RegressionTest
Scenario:Verify Premiums on PG PrivateRental HVS Aviva product For Twelve Years Cover Length
When I Create a PG PrivateRental HVS Aviva Quote For Twelve Years Cover Length and Verify Premiums
Then I Should Successfully Verify the HVS Premiums 

@RegressionTest
Scenario:Verify Premiums on PG CI Residential HVS Aviva product For Twelve Years Cover Length
When I Create a PG CI Residential Aviva Quote For Twelve Years Cover Length and Verify Premiums
Then I Should Successfully Verify the HVS Premiums

@RegressionTest
Scenario:Verify Premiums on PG CICommercial HVS Aviva product For Twelve Years Cover Length
When I Create a PG CI Commercial Aviva Quote For Twelve Years Cover Length and Verify Premiums
Then I Should Successfully Verify the HVS Premiums