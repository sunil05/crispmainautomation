﻿Feature: CreateLABCOrder	
Background: 
	Given I am on login page
	When I enter crisp username 
	And I enter crisp password
	And I click on login button
	Then I should see all items displayed on dashboard page

#LABC Order for NewHomes Product 
@Smoketest
Scenario Outline: Verify Creating LABC Order 
When I click on plus button and select  quote option
And I read following '<plotdata>' details
And I provide details on key site details page 
And I provide details on other site details page 
And I provide following '<plotdata>' data on plot schedule page
And I provide details on the product details page
And I provide details on additional questions page
And I select relavant roles on roles page
When I provide the details on declaration page
Then I click on complete button to see the application submitted details on the dashboard
#Send Quote 
When I click on sendquote button 
And I verify the details on send quote rating page
And I verify the details on send quote securities page
And I verify the details on send quote conditions page
And I verify the details on send quote special terms page
And I verify the details on send quote fees page
And I verify the details on send quote file review page
And I verify the details on send quote endorsements page
And I verify the details on send quote confirm details page
And I verify the details on send quote correspondence page
Then I click on send quote button to see quoted details on the dashboard
#Accept Quote
When I click on acceptquote button 
And I verify the details on accept quote products page
And I verify the details on accept quote conditions page
And I verify the details on accept quote file review page 
And I verify the details on accept quote confirm page 
And I verify the details on accept quote internal roles page 
And I verify the details on accept quote correspondence page 
Then I click on accept quote button to see order details on the dashboard
Examples: 
| plotdata                                                                                                                |
|\\\tpgfile\NewCompany\IT\Department\QA Team\Current Projects\CRISP\CrispAutomation\CrispData\LABC-Products\NewHomes.xlsx |

#LABC Send Quote 
@Smoketest
Scenario Outline: Verify Sending LABC Quote 
When I click on plus button and select  quote option
And I read following '<plotdata>' details
And I provide details on key site details page 
And I provide details on other site details page 
And I provide following '<plotdata>' data on plot schedule page
And I provide details on the product details page
And I provide details on additional questions page
And I select relavant roles on roles page
When I provide the details on declaration page
Then I click on complete button to see the application submitted details on the dashboard
#Send Quote 
When I click on sendquote button 
And I verify the details on send quote rating page
And I verify the details on send quote securities page
And I verify the details on send quote conditions page
And I verify the details on send quote special terms page
And I verify the details on send quote fees page
And I verify the details on send quote file review page
And I verify the details on send quote endorsements page
And I verify the details on send quote confirm details page
And I verify the details on send quote correspondence page
Then I click on send quote button to see quoted details on the dashboard
Examples: 
| plotdata                                                                                                                                              |
|\\\tpgfile\NewCompany\IT\Department\QA Team\Current Projects\CRISP\CrispAutomation\CrispData\LABC-Products\NewHomes.xlsx                              |

#Submitting LABC  Quote 
@Smoketest
Scenario Outline: Verify Submittng LABC Quote 
When I click on plus button and select  quote option
And I read following '<plotdata>' details
And I provide details on key site details page 
And I provide details on other site details page 
And I provide following '<plotdata>' data on plot schedule page
And I provide details on the product details page
And I provide details on additional questions page
And I select relavant roles on roles page
When I provide the details on declaration page
Then I click on complete button to see the application submitted details on the dashboard
Examples: 
| plotdata                                                                                                                                              |
|\\\tpgfile\NewCompany\IT\Department\QA Team\Current Projects\CRISP\CrispAutomation\CrispData\LABC-Products\NewHomes.xlsx                              |
