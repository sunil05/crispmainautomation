﻿Feature: LABCLVSPremiums
Background: 
	Given I am on login page
	When I enter crisp username 
	And I enter crisp password
	And I click on login button
	Then I should see all items displayed on dashboard page

@RegressionTest
Scenario:Verify Premiums on LABC NewHomes product
When I Create a Newhomes Quote and Verify Premiums
Then I Should Successfully Verify the Premiums 


@RegressionTest
Scenario:Verify Premiums on LABC Commercial product
When I Create a CommercialQuote and Verify Premiums
Then I Should Successfully Verify the Premiums 


@RegressionTest
Scenario:Verify Premiums on LABC SocialHousing product
When I Create a SocialHousingQuote and Verify Premiums
Then I Should Successfully Verify the Premiums 


@RegressionTest
Scenario:Verify Premiums on LABC PrivateRental product
When I Create a PrivateRentalQuote and Verify Premiums
Then I Should Successfully Verify the Premiums 


@RegressionTest
Scenario:Verify Premiums on LABC CompletedHousing product
When I Create a CompletedHousingQuote and Verify Premiums
Then I Should Successfully Verify the Premiums 

@RegressionTest
Scenario:Verify Premiums on LABC SelfBuild product
When I Create a SelfBuildQuote and Verify Premiums
Then I Should Successfully Verify the Premiums 

@RegressionTest
Scenario:Verify Premiums on LABC Commercial product for twelve years cover length
When I Create a CommercialQuote  for twelve years cover length and Verify Premiums
Then I Should Successfully Verify the Premiums 


@RegressionTest
Scenario:Verify Premiums on LABC SocialHousing product for twelve years cover length
When I Create a SocialHousingQuote for twelve years cover length and Verify Premiums
Then I Should Successfully Verify the Premiums 


@RegressionTest
Scenario:Verify Premiums on LABC PrivateRental product for twelve years cover length
When I Create a PrivateRentalQuote for twelve years cover length and Verify Premiums
Then I Should Successfully Verify the Premiums 
