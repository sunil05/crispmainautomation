﻿Feature: PGLVSPremiums
Background: 
	Given I am on login page
	When I enter crisp username 
	And I enter crisp password
	And I click on login button
	Then I should see all items displayed on dashboard page

@RegressionTest
Scenario:Verify Premiums on PG NewHomes product
When I Create a PG NewhomesQuote and Verify Premiums
Then I Should Successfully Verify the Premiums 


@RegressionTest
Scenario:Verify Premiums on PG Commercial product
When I Create a PG CommercialQuote and Verify Premiums
Then I Should Successfully Verify the Premiums 


@RegressionTest
Scenario:Verify Premiums on PG SocialHousing product
When I Create a PG SocialHousingQuote and Verify Premiums
Then I Should Successfully Verify the Premiums 


@RegressionTest
Scenario:Verify Premiums on PG PrivateRental product
When I Create a PG PrivateRentalQuote and Verify Premiums
Then I Should Successfully Verify the Premiums 


@RegressionTest
Scenario:Verify Premiums on PG CompletedHousing product
When I Create a PG CompletedHousingQuote and Verify Premiums
Then I Should Successfully Verify the Premiums 

@RegressionTest
Scenario:Verify Premiums on PG SelfBuild product
When I Create a PG SelfBuildQuote and Verify Premiums
Then I Should Successfully Verify the Premiums 

@RegressionTest
Scenario:Verify Premiums on CI Residential product
When I Create a PG CI Residential Quote and Verify Premiums
Then I Should Successfully Verify the Premiums

@RegressionTest
Scenario:Verify Premiums on CICommercial product
When I Create a PG CI Commercial and Verify Premiums
Then I Should Successfully Verify the Premiums

@RegressionTest
Scenario:Verify Premiums on Residential Building Control products
When I Create a Quote with Residential BC Products and Verify Premiums
Then I Should Successfully Verify the Premiums 

@RegressionTest
Scenario:Verify Premiums on Commercial Building Control products
When I Create a Quote with Commercial BC Products and Verify Premiums
Then I Should Successfully Verify the Premiums 

#12 Years Cover Length
@RegressionTest
Scenario:Verify Premiums on PG Commercial product For Twelve Years Cover Length
When I Create a PG CommercialQuote For Twelve Years Cover Length and Verify Premiums
Then I Should Successfully Verify the Premiums 


@RegressionTest
Scenario:Verify Premiums on PG SocialHousing product For Twelve Years Cover Length
When I Create a PG SocialHousingQuote For Twelve Years Cover Length and Verify Premiums
Then I Should Successfully Verify the Premiums 


@RegressionTest
Scenario:Verify Premiums on PG PrivateRental product For Twelve Years Cover Length
When I Create a PG PrivateRentalQuote For Twelve Years Cover Length and Verify Premiums
Then I Should Successfully Verify the Premiums 

@RegressionTest
Scenario:Verify Premiums on CI Residential product For Twelve Years Cover Length
When I Create a PG CI Residential For Twelve Years Cover Length Quote and Verify Premiums
Then I Should Successfully Verify the Premiums

@RegressionTest
Scenario:Verify Premiums on CICommercial product For Twelve Years Cover Length
When I Create a PG CI Commercial For Twelve Years Cover Length and Verify Premiums
Then I Should Successfully Verify the Premiums