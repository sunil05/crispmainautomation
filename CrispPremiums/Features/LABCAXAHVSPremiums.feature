﻿Feature: LABCAXAHVSPremiums
Background: 
	Given I am on login page
	When I enter crisp username 
	And I enter crisp password
	And I click on login button
	Then I should see all items displayed on dashboard page

@RegressionTest
Scenario:Verify Premiums on LABC NewHomes AXA HVS product
When I Create a Newhomes HVS Quote and Verify Premiums
Then I Should Successfully Verify the HVS Premiums 


@RegressionTest
Scenario:Verify Premiums on LABC Commercial AXA HVS product
When I Create a Commercial HVS Quote and Verify HVS Premiums
Then I Should Successfully Verify the HVS Premiums 

@RegressionTest
Scenario:Verify Premiums on LABC Commercial AXA HVS product for twelve years cover length
When I Create a Commercial HVS Quote for twelve years cover length and Verify HVS Premiums
Then I Should Successfully Verify the HVS Premiums 




