﻿Feature: LABCAvivaHVSPremiums
Background: 
	Given I am on login page
	When I enter crisp username 
	And I enter crisp password
	And I click on login button
	Then I should see all items displayed on dashboard page

@RegressionTest
Scenario:Verify Premiums on LABC NewHomes HVS Aviva product
When I Create a Newhomes HVS Aviva Quote and Verify Premiums
Then I Should Successfully Verify the HVS Premiums 


@RegressionTest
Scenario:Verify Premiums on LABC Commercial HVS Aviva product
When I Create a Commercial HVS Aviva Quote and Verify HVS Premiums
Then I Should Successfully Verify the HVS Premiums 


@RegressionTest
Scenario:Verify Premiums on LABC SocialHousing HVS Aviva product
When I Create a SocialHousing HVS Aviva Quote and Verify HVS Premiums
Then I Should Successfully Verify the HVS Premiums 


@RegressionTest
Scenario:Verify Premiums on LABC PrivateRental HVS Aviva product
When I Create a PrivateRental HVS Aviva Quote and Verify Premiums
Then I Should Successfully Verify the HVS Premiums 


@RegressionTest
Scenario:Verify Premiums on LABC Commercial HVS Aviva product for Twelve years cover length
When I Create a Commercial HVS Aviva Quote for Twelve years cover length and Verify HVS Premiums
Then I Should Successfully Verify the HVS Premiums 


@RegressionTest
Scenario:Verify Premiums on LABC SocialHousing HVS Aviva product for Twelve years cover length
When I Create a SocialHousing HVS Aviva Quote for Twelve years cover length and Verify HVS Premiums
Then I Should Successfully Verify the HVS Premiums 


@RegressionTest
Scenario:Verify Premiums on LABC PrivateRental HVS Aviva product for Twelve years cover length
When I Create a PrivateRental HVS Aviva Quote for Twelve years cover length and Verify Premiums
Then I Should Successfully Verify the HVS Premiums