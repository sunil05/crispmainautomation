﻿// ------------------------------------------------------------------------------
//  <auto-generated>
//      This code was generated by SpecFlow (http://www.specflow.org/).
//      SpecFlow Version:2.4.0.0
//      SpecFlow Generator Version:2.4.0.0
// 
//      Changes to this file may cause incorrect behavior and will be lost if
//      the code is regenerated.
//  </auto-generated>
// ------------------------------------------------------------------------------
#region Designer generated code
#pragma warning disable
namespace CrispPremiums.Features
{
    using TechTalk.SpecFlow;
    
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("TechTalk.SpecFlow", "2.4.0.0")]
    [System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    [NUnit.Framework.TestFixtureAttribute()]
    [NUnit.Framework.DescriptionAttribute("LABCLVSSinglePlotsPremiums")]
    public partial class LABCLVSSinglePlotsPremiumsFeature
    {
        
        private TechTalk.SpecFlow.ITestRunner testRunner;
        
#line 1 "LABCLVSSinglePlotPremiums.feature"
#line hidden
        
        [NUnit.Framework.OneTimeSetUpAttribute()]
        public virtual void FeatureSetup()
        {
            testRunner = TechTalk.SpecFlow.TestRunnerManager.GetTestRunner();
            TechTalk.SpecFlow.FeatureInfo featureInfo = new TechTalk.SpecFlow.FeatureInfo(new System.Globalization.CultureInfo("en-US"), "LABCLVSSinglePlotsPremiums", null, ProgrammingLanguage.CSharp, ((string[])(null)));
            testRunner.OnFeatureStart(featureInfo);
        }
        
        [NUnit.Framework.OneTimeTearDownAttribute()]
        public virtual void FeatureTearDown()
        {
            testRunner.OnFeatureEnd();
            testRunner = null;
        }
        
        [NUnit.Framework.SetUpAttribute()]
        public virtual void TestInitialize()
        {
        }
        
        [NUnit.Framework.TearDownAttribute()]
        public virtual void ScenarioTearDown()
        {
            testRunner.OnScenarioEnd();
        }
        
        public virtual void ScenarioInitialize(TechTalk.SpecFlow.ScenarioInfo scenarioInfo)
        {
            testRunner.OnScenarioInitialize(scenarioInfo);
            testRunner.ScenarioContext.ScenarioContainer.RegisterInstanceAs<NUnit.Framework.TestContext>(NUnit.Framework.TestContext.CurrentContext);
        }
        
        public virtual void ScenarioStart()
        {
            testRunner.OnScenarioStart();
        }
        
        public virtual void ScenarioCleanup()
        {
            testRunner.CollectScenarioErrors();
        }
        
        public virtual void FeatureBackground()
        {
#line 2
#line 3
 testRunner.Given("I am on login page", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line 4
 testRunner.When("I enter crisp username", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line 5
 testRunner.And("I enter crisp password", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 6
 testRunner.And("I click on login button", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 7
 testRunner.Then("I should see all items displayed on dashboard page", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line hidden
        }
        
        [NUnit.Framework.TestAttribute()]
        [NUnit.Framework.DescriptionAttribute("Verify Premiums on LABC NewHomes single product")]
        [NUnit.Framework.CategoryAttribute("RegressionTest")]
        public virtual void VerifyPremiumsOnLABCNewHomesSingleProduct()
        {
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("Verify Premiums on LABC NewHomes single product", null, new string[] {
                        "RegressionTest"});
#line 10
this.ScenarioInitialize(scenarioInfo);
            this.ScenarioStart();
#line 2
this.FeatureBackground();
#line 11
testRunner.When("I Create a NewhomesQuote and Verify Premiums On Single Plot", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line 12
testRunner.Then("I Should Successfully Verify the Premiums", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line hidden
            this.ScenarioCleanup();
        }
        
        [NUnit.Framework.TestAttribute()]
        [NUnit.Framework.DescriptionAttribute("Verify Premiums on LABC Commercial single product")]
        [NUnit.Framework.CategoryAttribute("RegressionTest")]
        public virtual void VerifyPremiumsOnLABCCommercialSingleProduct()
        {
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("Verify Premiums on LABC Commercial single product", null, new string[] {
                        "RegressionTest"});
#line 16
this.ScenarioInitialize(scenarioInfo);
            this.ScenarioStart();
#line 2
this.FeatureBackground();
#line 17
testRunner.When("I Create a CommercialQuote and Verify Premiums On Single Plot", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line 18
testRunner.Then("I Should Successfully Verify the Premiums", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line hidden
            this.ScenarioCleanup();
        }
        
        [NUnit.Framework.TestAttribute()]
        [NUnit.Framework.DescriptionAttribute("Verify Premiums on LABC SocialHousing single product")]
        [NUnit.Framework.CategoryAttribute("RegressionTest")]
        public virtual void VerifyPremiumsOnLABCSocialHousingSingleProduct()
        {
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("Verify Premiums on LABC SocialHousing single product", null, new string[] {
                        "RegressionTest"});
#line 22
this.ScenarioInitialize(scenarioInfo);
            this.ScenarioStart();
#line 2
this.FeatureBackground();
#line 23
testRunner.When("I Create a SocialHousingQuote and Verify Premiums On Single Plot", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line 24
testRunner.Then("I Should Successfully Verify the Premiums", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line hidden
            this.ScenarioCleanup();
        }
        
        [NUnit.Framework.TestAttribute()]
        [NUnit.Framework.DescriptionAttribute("Verify Premiums on LABC PrivateRental single product")]
        [NUnit.Framework.CategoryAttribute("RegressionTest")]
        public virtual void VerifyPremiumsOnLABCPrivateRentalSingleProduct()
        {
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("Verify Premiums on LABC PrivateRental single product", null, new string[] {
                        "RegressionTest"});
#line 28
this.ScenarioInitialize(scenarioInfo);
            this.ScenarioStart();
#line 2
this.FeatureBackground();
#line 29
testRunner.When("I Create a PrivateRentalQuote and Verify Premiums On Single Plot", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line 30
testRunner.Then("I Should Successfully Verify the Premiums", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line hidden
            this.ScenarioCleanup();
        }
        
        [NUnit.Framework.TestAttribute()]
        [NUnit.Framework.DescriptionAttribute("Verify Premiums on LABC CompletedHousing single product")]
        [NUnit.Framework.CategoryAttribute("RegressionTest")]
        public virtual void VerifyPremiumsOnLABCCompletedHousingSingleProduct()
        {
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("Verify Premiums on LABC CompletedHousing single product", null, new string[] {
                        "RegressionTest"});
#line 34
this.ScenarioInitialize(scenarioInfo);
            this.ScenarioStart();
#line 2
this.FeatureBackground();
#line 35
testRunner.When("I Create a CompletedHousingQuote and Verify Premiums On Single Plot", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line 36
testRunner.Then("I Should Successfully Verify the Premiums", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line hidden
            this.ScenarioCleanup();
        }
        
        [NUnit.Framework.TestAttribute()]
        [NUnit.Framework.DescriptionAttribute("Verify Premiums on LABC SelfBuild single product")]
        public virtual void VerifyPremiumsOnLABCSelfBuildSingleProduct()
        {
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("Verify Premiums on LABC SelfBuild single product", null, ((string[])(null)));
#line 39
this.ScenarioInitialize(scenarioInfo);
            this.ScenarioStart();
#line 2
this.FeatureBackground();
#line 40
testRunner.When("I Create a SelfBuildQuote and Verify Premiums On Single Plot", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line 41
testRunner.Then("I Should Successfully Verify the Premiums", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line hidden
            this.ScenarioCleanup();
        }
        
        [NUnit.Framework.TestAttribute()]
        [NUnit.Framework.DescriptionAttribute("Verify Premiums on LABC Commercial single product For Twelve Years Cover Length")]
        [NUnit.Framework.CategoryAttribute("RegressionTest")]
        public virtual void VerifyPremiumsOnLABCCommercialSingleProductForTwelveYearsCoverLength()
        {
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("Verify Premiums on LABC Commercial single product For Twelve Years Cover Length", null, new string[] {
                        "RegressionTest"});
#line 44
this.ScenarioInitialize(scenarioInfo);
            this.ScenarioStart();
#line 2
this.FeatureBackground();
#line 45
testRunner.When("I Create a CommercialQuote For Twelve Years Cover Length and Verify Premiums On S" +
                    "ingle Plot", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line 46
testRunner.Then("I Should Successfully Verify the Premiums", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line hidden
            this.ScenarioCleanup();
        }
        
        [NUnit.Framework.TestAttribute()]
        [NUnit.Framework.DescriptionAttribute("Verify Premiums on LABC SocialHousing single product For Twelve Years Cover Lengt" +
            "h")]
        [NUnit.Framework.CategoryAttribute("RegressionTest")]
        public virtual void VerifyPremiumsOnLABCSocialHousingSingleProductForTwelveYearsCoverLength()
        {
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("Verify Premiums on LABC SocialHousing single product For Twelve Years Cover Lengt" +
                    "h", null, new string[] {
                        "RegressionTest"});
#line 50
this.ScenarioInitialize(scenarioInfo);
            this.ScenarioStart();
#line 2
this.FeatureBackground();
#line 51
testRunner.When("I Create a SocialHousingQuote For Twelve Years Cover Length and Verify Premiums O" +
                    "n Single Plot", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line 52
testRunner.Then("I Should Successfully Verify the Premiums", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line hidden
            this.ScenarioCleanup();
        }
        
        [NUnit.Framework.TestAttribute()]
        [NUnit.Framework.DescriptionAttribute("Verify Premiums on LABC PrivateRental single product For Twelve Years Cover Lengt" +
            "h")]
        [NUnit.Framework.CategoryAttribute("RegressionTest")]
        public virtual void VerifyPremiumsOnLABCPrivateRentalSingleProductForTwelveYearsCoverLength()
        {
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("Verify Premiums on LABC PrivateRental single product For Twelve Years Cover Lengt" +
                    "h", null, new string[] {
                        "RegressionTest"});
#line 56
this.ScenarioInitialize(scenarioInfo);
            this.ScenarioStart();
#line 2
this.FeatureBackground();
#line 57
testRunner.When("I Create a PrivateRentalQuote For Twelve Years Cover Length and Verify Premiums O" +
                    "n Single Plot", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line 58
testRunner.Then("I Should Successfully Verify the Premiums", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line hidden
            this.ScenarioCleanup();
        }
    }
}
#pragma warning restore
#endregion
