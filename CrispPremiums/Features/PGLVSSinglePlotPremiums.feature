﻿Feature: PGLVSSinglePlotsPremiums
Background: 
	Given I am on login page
	When I enter crisp username 
	And I enter crisp password
	And I click on login button
	Then I should see all items displayed on dashboard page

@RegressionTest
Scenario:Verify Premiums on PG  NewHomes single product
When I Create a PG NewhomesQuote and Verify Premiums On Single Plot
Then I Should Successfully Verify the Premiums 


@RegressionTest
Scenario:Verify Premiums on PG Commercial single product
When I Create a PG CommercialQuote and Verify Premiums On Single Plot
Then I Should Successfully Verify the Premiums 


@RegressionTest
Scenario:Verify Premiums on PG SocialHousing single product
When I Create a PG SocialHousingQuote and Verify Premiums On Single Plot
Then I Should Successfully Verify the Premiums 


@RegressionTest
Scenario:Verify Premiums on PG PrivateRental  single product
When I Create a PG PrivateRentalQuote and Verify Premiums On Single Plot
Then I Should Successfully Verify the Premiums 


@RegressionTest
Scenario:Verify Premiums on PG CompletedHousing single product
When I Create a PG CompletedHousingQuote and Verify Premiums On Single Plot
Then I Should Successfully Verify the Premiums 

@RegressionTest
Scenario:Verify Premiums on PG SelfBuild single product
When I Create a PG SelfBuildQuote and Verify Premiums On Single Plot
Then I Should Successfully Verify the Premiums 

@RegressionTest
Scenario:Verify Premiums on CI Residential single product
When I Create a PG CI Residential Quote and Verify Premiums On Single Plot
Then I Should Successfully Verify the Premiums

@RegressionTest
Scenario:Verify Premiums on CICommercial single product
When I Create a PG CI Commercial and Verify Premiums On Single Plot
Then I Should Successfully Verify the Premiums

@RegressionTest
Scenario:Verify Premiums on Residential Building Control single products
When I Create a Quote with Residential BC Products and Verify Premiums On Single Plot
Then I Should Successfully Verify the Premiums 

@RegressionTest
Scenario:Verify Premiums on Commercial Building Control single products
When I Create a Quote with Commercial BC Products and Verify Premiums On Single Plot
Then I Should Successfully Verify the Premiums 

#12Years Cover Length Scenarios 

@RegressionTest
Scenario:Verify Premiums on PG Commercial single product For Twelve Years Cover Length
When I Create a PG CommercialQuote For Twelve Years Cover Length and Verify Premiums On Single Plot
Then I Should Successfully Verify the Premiums 


@RegressionTest
Scenario:Verify Premiums on PG SocialHousing single product For Twelve Years Cover Length
When I Create a PG SocialHousingQuote For Twelve Years Cover Length and Verify Premiums On Single Plot
Then I Should Successfully Verify the Premiums 


@RegressionTest
Scenario:Verify Premiums on PG PrivateRental single product For Twelve Years Cover Length
When I Create a PG PrivateRentalQuote For Twelve Years Cover Length and Verify Premiums On Single Plot
Then I Should Successfully Verify the Premiums 

@RegressionTest
Scenario:Verify Premiums on CI Residential single product For Twelve Years Cover Length
When I Create a PG CI Residential Quote For Twelve Years Cover Length and Verify Premiums On Single Plot
Then I Should Successfully Verify the Premiums

@RegressionTest
Scenario:Verify Premiums on CICommercial single product For Twelve Years Cover Length
When I Create a PG CI Commercial For Twelve Years Cover Length and Verify Premiums On Single Plot
Then I Should Successfully Verify the Premiums

