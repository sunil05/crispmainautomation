﻿Feature: PGAXAHVSPremiums
Background:
	Given I am on login page
	When I enter crisp username 
	And I enter crisp password
	And I click on login button
	Then I should see all items displayed on dashboard page


@RegressionTest
Scenario:Verify Premiums on PG NewHomes AXA HVS product
When I Create a PG Newhomes HVS Quote and Verify Premiums
Then I Should Successfully Verify the HVS Premiums 


@RegressionTest
Scenario:Verify Premiums on PG Commercial AXA HVS product
When I Create a PG Commercial HVS Quote and Verify HVS Premiums
Then I Should Successfully Verify the HVS Premiums 


@RegressionTest
Scenario:Verify Premiums on PG SocialHousing AXA HVS product
When I Create a PG SocialHousing HVS Quote and Verify HVS Premiums
Then I Should Successfully Verify the HVS Premiums

@RegressionTest
Scenario:Verify Premiums on CI Residential AXA HVS product
When I Create a PG CI Residential HVS Quote and Verify Premiums
Then I Should Successfully Verify the HVS Premiums

@RegressionTest
Scenario:Verify Premiums on PG Commercial AXA HVS product For Twelve Years Cover Length
When I Create a PG Commercial HVS Quote For Twelve Years Cover Length and Verify HVS Premiums
Then I Should Successfully Verify the HVS Premiums 


@RegressionTest
Scenario:Verify Premiums on PG SocialHousing AXA HVS product For Twelve Years Cover Length
When I Create a PG SocialHousing HVS Quote For Twelve Years Cover Length and Verify HVS Premiums
Then I Should Successfully Verify the HVS Premiums

@RegressionTest
Scenario:Verify Premiums on CI Residential AXA HVS product For Twelve Years Cover Length
When I Create a PG CI Residential HVS Quote For Twelve Years Cover Length and Verify Premiums
Then I Should Successfully Verify the HVS Premiums


