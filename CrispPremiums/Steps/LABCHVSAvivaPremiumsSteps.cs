﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TechTalk.SpecFlow;

namespace RegressionPacks.Steps
{
    [Binding]
    public class LABCHVSAvivaPremiumsSteps : Support.Pages
    {
        [When(@"I Create a Newhomes HVS Aviva Quote and Verify Premiums")]
        public void WhenICreateANewhomesHVSAvivaQuoteAndVerifyPremiums()
        {
           LABCHVSAvivaPremiums.LABCNewhomesHVSAvivaPremiums();
        }

        [When(@"I Create a Commercial HVS Aviva Quote and Verify HVS Premiums")]
        public void WhenICreateACommercialHVSAvivaQuoteAndVerifyHVSPremiums()
        {
            LABCHVSAvivaPremiums.LABCCommercialHVSAvivaPremiums();
        }

        [When(@"I Create a SocialHousing HVS Aviva Quote and Verify HVS Premiums")]
        public void WhenICreateASocialHousingHVSAvivaQuoteAndVerifyHVSPremiums()
        {
            LABCHVSAvivaPremiums.LABCSocialHousingHVSAvivaPremiums();
        }

        [When(@"I Create a PrivateRental HVS Aviva Quote and Verify Premiums")]
        public void WhenICreateAPrivateRentalHVSAvivaQuoteAndVerifyPremiums()
        {
            LABCHVSAvivaPremiums.LABCPrivateRentalHVSAvivaPremiums();
        }

        [When(@"I Create a Commercial HVS Aviva Quote for Twelve years cover length and Verify HVS Premiums")]
        public void WhenICreateACommercialHVSAvivaQuoteForTwelveYearsCoverLengthAndVerifyHVSPremiums()
        {
            LABCHVSAvivaPremiums.LABCCommercialHVSAvivaPremiumsFor12YearsCoverLength();
        }

        [When(@"I Create a SocialHousing HVS Aviva Quote for Twelve years cover length and Verify HVS Premiums")]
        public void WhenICreateASocialHousingHVSAvivaQuoteForTwelveYearsCoverLengthAndVerifyHVSPremiums()
        {
            LABCHVSAvivaPremiums.LABCSocialHousingHVSAvivaPremiumsFor12YearsCoverLength();
        }

        [When(@"I Create a PrivateRental HVS Aviva Quote for Twelve years cover length and Verify Premiums")]
        public void WhenICreateAPrivateRentalHVSAvivaQuoteForTwelveYearsCoverLengthAndVerifyPremiums()
        {
            LABCHVSAvivaPremiums.LABCPrivateRentalHVSAvivaPremiumsFor12YearsCoverLength();
        }



    }
}
