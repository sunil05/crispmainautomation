﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TechTalk.SpecFlow;

namespace RegressionPacks.Steps
{
    [Binding]
    public sealed class PGHVSPremiumsSteps : Support.Pages
    {
        [When(@"I Create a PG Newhomes HVS Quote and Verify Premiums")]
        public void WhenICreateAPGNewhomesHVSQuoteAndVerifyPremiums()
        {
            PGHVSPremiums.PGNewhomesHVSPremiums();
        }

        [When(@"I Create a PG Commercial HVS Quote and Verify HVS Premiums")]
        public void WhenICreateAPGCommercialHVSQuoteAndVerifyHVSPremiums()
        {
            PGHVSPremiums.PGCommercialHVSPremiums();
        }

        [When(@"I Create a PG SocialHousing HVS Quote and Verify HVS Premiums")]
        public void WhenICreateAPGSocialHousingHVSQuoteAndVerifyHVSPremiums()
        {
            PGHVSPremiums.PGSocialHousingHVSPremiums();
        }
        [When(@"I Create a PG CI Residential HVS Quote and Verify Premiums")]
        public void WhenICreateAPGCIResidentialHVSQuoteAndVerifyPremiums()
        {
            PGHVSPremiums.PGCIResidentialHVSPremiums();
        }

        [When(@"I Create a PG CI Commercial HVS Quote and Verify Premiums")]
        public void WhenICreateAPGCICommercialHVSQuoteAndVerifyPremiums()
        {
            PGHVSPremiums.PGCICommercialHVSPremiums();
        }
        [When(@"I Create a PG Commercial HVS Quote For Twelve Years Cover Length and Verify HVS Premiums")]
        public void WhenICreateAPGCommercialHVSQuoteForTwelveYearsCoverLengthAndVerifyHVSPremiums()
        {
            PGHVSPremiums.PGCommercialHVSPremiumsFor12YearsCoverLength();
        }

        [When(@"I Create a PG SocialHousing HVS Quote For Twelve Years Cover Length and Verify HVS Premiums")]
        public void WhenICreateAPGSocialHousingHVSQuoteForTwelveYearsCoverLengthAndVerifyHVSPremiums()
        {
            PGHVSPremiums.PGSocialHousingHVSPremiumsFor12YearsCoverLength();
        }

        [When(@"I Create a PG CI Residential HVS Quote For Twelve Years Cover Length and Verify Premiums")]
        public void WhenICreateAPGCIResidentialHVSQuoteForTwelveYearsCoverLengthAndVerifyPremiums()
        {
            PGHVSPremiums.PGCIResidentialHVSPremiumsFor12YearsCoverLength();
        }

    }
}
