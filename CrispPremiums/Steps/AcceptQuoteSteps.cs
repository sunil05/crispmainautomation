﻿using NUnit.Framework;
using RegressionPacks.Pages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using TechTalk.SpecFlow;

namespace RegressionPacks.Steps
{
    [Binding]
    public class AcceptQuoteSteps : Support.Pages
    {
        // Accept QUote 
        [When(@"I click on acceptquote button")]
        public void WhenIClickOnAcceptquoteButton()
        {
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(1000);
            WaitForElement(Dashboardpage.QuoteDecisionButton);
            Thread.Sleep(500);
            Dashboardpage.QuoteDecisionButton.Click();
            Thread.Sleep(500);
            WaitForElement(Dashboardpage.AcceptQuoteButton);
            Thread.Sleep(500);
            Dashboardpage.AcceptQuoteButton.Click();
            Thread.Sleep(1000);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(1000);
            WaitForElement(AcceptQuotePage.LabcAcceptQuoteDialogueWindow);
            Assert.IsTrue(AcceptQuotePage.LabcAcceptQuoteDialogueWindow.Displayed, "Failed to display Accept Quote Wizard");
            Thread.Sleep(1000);
            CloseSpinneronDiv();
        }
        [When(@"I click on the PG Product acceptquote button")]
        public void WhenIClickOnThePGProductAcceptquoteButton()
        {
            Thread.Sleep(500);
            WaitForElement(Dashboardpage.QuoteDecisionButton);
            Thread.Sleep(500);
            Dashboardpage.QuoteDecisionButton.Click();
            Thread.Sleep(500);
            WaitForElement(Dashboardpage.AcceptQuoteButton);
            Thread.Sleep(500);
            Dashboardpage.AcceptQuoteButton.Click();
            Thread.Sleep(1000);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(1000);
            WaitForElement(AcceptQuotePage.AcceptQuoteDialogueWindow);
            Assert.IsTrue(AcceptQuotePage.AcceptQuoteDialogueWindow.Displayed, "Failed to display Accept Quote Wizard");
            Thread.Sleep(1000);
            CloseSpinneronDiv();
        }


        [When(@"I verify the details on accept quote products page")]
        public void WhenIVerifyTheDetailsOnAcceptQuoteProductsPage()
        {
            AcceptQuotePage.AcceptQuoteProductsPage();
        }

        [When(@"I verify the details on accept quote conditions page")]
        public void WhenIVerifyTheDetailsOnAcceptQuoteConditionsPage()
        {
            AcceptQuotePage.AcceptQuoteConditionsPage();

        }

        [When(@"I verify the details on accept quote file review page")]
        public void WhenIVerifyTheDetailsOnAcceptQuoteFileReviewPage()
        {
            AcceptQuotePage.AcceptQuoteFileReviewPage();

        }

        [When(@"I verify the details on accept quote confirm page")]
        public void WhenIVerifyTheDetailsOnAcceptQuoteConfirmPage()
        {
            AcceptQuotePage.AcceptQuoteConfirmPage();

        }

        [When(@"I verify the details on accept quote internal roles page")]
        public void WhenIVerifyTheDetailsOnAcceptQuoteInternalRolesPage()
        {
            AcceptQuotePage.AcceptQuoteInternalRolesPage();

        }

        [When(@"I verify the details on accept quote correspondence page")]
        public void WhenIVerifyTheDetailsOnAcceptQuoteCorrespondencePage()
        {

            AcceptQuotePage.AcceptQuoteCorrespondencePage();

        }

        [Then(@"I click on accept quote button to see order details on the dashboard")]
        public void ThenIClickOnAcceptQuoteButtonToSeeOrderDetailsOnTheDashboard()
        {
            AcceptQuotePage.CompleteButtonAcceptquoteWindow();
            AcceptQuotePage.OrderDetailsOnTheDashboard();

        }

    }
}
