﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TechTalk.SpecFlow;

namespace RegressionPacks.Steps
{
    [Binding]
    public class PGPremiumSteps : Support.Pages
    {
        [When(@"I Create a PG NewhomesQuote and Verify Premiums")]
        public void WhenICreateAPGNewhomesQuoteAndVerifyPremiums()
        {
            PGPremiums.PGNewhomesPremiums();
        }
        [When(@"I Create a PG CommercialQuote and Verify Premiums")]
        public void WhenICreateAPGCommercialQuoteAndVerifyPremiums()
        {
            PGPremiums.PGCommercialPremiums();
        }
        [When(@"I Create a PG SocialHousingQuote and Verify Premiums")]
        public void WhenICreateAPGSocialHousingQuoteAndVerifyPremiums()
        {
            PGPremiums.PGSocialHousingPremiums();
        }
        [When(@"I Create a PG PrivateRentalQuote and Verify Premiums")]
        public void WhenICreateAPGPrivateRentalQuoteAndVerifyPremiums()
        {
            PGPremiums.PGPrivateRentalPremiums();
        }

        [When(@"I Create a PG CompletedHousingQuote and Verify Premiums")]
        public void WhenICreateAPGCompletedHousingQuoteAndVerifyPremiums()
        {
            PGPremiums.PGCompletedHousingPremiums();
        }

        [When(@"I Create a PG SelfBuildQuote and Verify Premiums")]
        public void WhenICreateAPGSelfBuildQuoteAndVerifyPremiums()
        {
            PGPremiums.PGSelfBuildPremiums();
        }
        [When(@"I Create a PG CI Residential Quote and Verify Premiums")]
        public void WhenICreateAPGCIResidentialQuoteAndVerifyPremiums()
        {
            PGPremiums.PGCIResidentialPremiums();
        }
        [When(@"I Create a PG CI Commercial and Verify Premiums")]
        public void WhenICreateAPGCICommercialAndVerifyPremiums()
        {
            PGPremiums.PGCICommercialPremiums();
        }
        
        [When(@"I Create a Quote with Residential BC Products and Verify Premiums")]
        public void WhenICreateAQuoteWithResidentialBCProductsAndVerifyPremiums()
        {
            PGPremiums.ResidentialBCPremium();
        }

        [When(@"I Create a Quote with Commercial BC Products and Verify Premiums")]
        public void WhenICreateAQuoteWithCommercialBCProductsAndVerifyPremiums()
        {
            PGPremiums.CommercialBCPremium();
        }
        [When(@"I Create a PG CommercialQuote For Twelve Years Cover Length and Verify Premiums")]
        public void WhenICreateAPGCommercialQuoteForTwelveYearsCoverLengthAndVerifyPremiums()
        {
            PGPremiums.PGCommercialPremiumsFor12YearsCoverLength();
        }

        [When(@"I Create a PG SocialHousingQuote For Twelve Years Cover Length and Verify Premiums")]
        public void WhenICreateAPGSocialHousingQuoteForTwelveYearsCoverLengthAndVerifyPremiums()
        {
            PGPremiums.PGSocialHousingPremiumsFor12YearsCoverLength();
        }

        [When(@"I Create a PG PrivateRentalQuote For Twelve Years Cover Length and Verify Premiums")]
        public void WhenICreateAPGPrivateRentalQuoteForTwelveYearsCoverLengthAndVerifyPremiums()
        {
            PGPremiums.PGPrivateRentalPremiumsFor12YearsCoverLength();
        }

        [When(@"I Create a PG CI Residential For Twelve Years Cover Length Quote and Verify Premiums")]
        public void WhenICreateAPGCIResidentialForTwelveYearsCoverLengthQuoteAndVerifyPremiums()
        {
            PGPremiums.PGCIResidentialPremiumsFor12YearsCoverLength();
        }

        [When(@"I Create a PG CI Commercial For Twelve Years Cover Length and Verify Premiums")]
        public void WhenICreateAPGCICommercialForTwelveYearsCoverLengthAndVerifyPremiums()
        {
            PGPremiums.PGCICommercialPremiumsFor12YearsCoverLength();
        }


    }
}
