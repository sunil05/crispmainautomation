﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TechTalk.SpecFlow;

namespace RegressionPacks.Steps
{
    [Binding]
    public class LABCSinglePlotPremiumsSteps : Support.Pages
    {
        [When(@"I Create a NewhomesQuote and Verify Premiums On Single Plot")]
        public void WhenICreateANewhomesQuoteAndVerifyPremiumsOnSinglePlot()
        {
            LABCSinglePlotPremiums.LABCNewhomesPremiums();
        }

        [When(@"I Create a CommercialQuote and Verify Premiums On Single Plot")]
        public void WhenICreateACommercialQuoteAndVerifyPremiumsOnSinglePlot()
        {
            LABCSinglePlotPremiums.LABCCommercialPremiums();
        }

        [When(@"I Create a SocialHousingQuote and Verify Premiums On Single Plot")]
        public void WhenICreateASocialHousingQuoteAndVerifyPremiumsOnSinglePlot()
        {
            LABCSinglePlotPremiums.LABCSocialHousingPremiums();
        }

        [When(@"I Create a PrivateRentalQuote and Verify Premiums On Single Plot")]
        public void WhenICreateAPrivateRentalQuoteAndVerifyPremiumsOnSinglePlot()
        {
            LABCSinglePlotPremiums.LABCPrivateRentalPremiums();
        }

        [When(@"I Create a CompletedHousingQuote and Verify Premiums On Single Plot")]
        public void WhenICreateACompletedHousingQuoteAndVerifyPremiumsOnSinglePlot()
        {
            LABCSinglePlotPremiums.LABCCompletedHousingPremiums();
        }

        [When(@"I Create a SelfBuildQuote and Verify Premiums On Single Plot")]
        public void WhenICreateASelfBuildQuoteAndVerifyPremiumsOnSinglePlot()
        {
            LABCSinglePlotPremiums.LABCSelfBuildPremiums();
        }

        [When(@"I Create a CommercialQuote For Twelve Years Cover Length and Verify Premiums On Single Plot")]
        public void WhenICreateACommercialQuoteForTwelveYearsCoverLengthAndVerifyPremiumsOnSinglePlot()
        {
            LABCSinglePlotPremiums.LABCCommercialPremiumsFor12YearsCoverLength();
        }

        [When(@"I Create a SocialHousingQuote For Twelve Years Cover Length and Verify Premiums On Single Plot")]
        public void WhenICreateASocialHousingQuoteForTwelveYearsCoverLengthAndVerifyPremiumsOnSinglePlot()
        {
            LABCSinglePlotPremiums.LABCSocialHousingPremiumsFor12YearsCoverLength();
        }

        [When(@"I Create a PrivateRentalQuote For Twelve Years Cover Length and Verify Premiums On Single Plot")]
        public void WhenICreateAPrivateRentalQuoteForTwelveYearsCoverLengthAndVerifyPremiumsOnSinglePlot()
        {
            LABCSinglePlotPremiums.LABCPrivateRentalPremiumsFor12YearsCoverLength();
        }

    }
}
