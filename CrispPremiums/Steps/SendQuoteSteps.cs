﻿using NUnit.Framework;
using RegressionPacks.Pages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using TechTalk.SpecFlow;

namespace RegressionPacks.Steps
{
    [Binding]
    public class SendQuoteSteps : Support.Pages
    {    


        //Sending Quote
        [When(@"I click on sendquote button")]
        public void WhenIClickOnSendquoteButton()
        {

            SendQuotePage.ClickOnSendQuoteButton();
          
        }
        [When(@"I verify the details on send quote rating page")]
        public void WhenIVerifyTheDetailsOnSendQuoteRatingPage()
        {
            SendQuotePage.SendQuoteRatingPage();
        }

        [When(@"I verify the details on send quote securities page")]
        public void WhenIVerifyTheDetailsOnSendQuoteSecuritiesPage()
        {
            SendQuotePage.SendQuoteSecuritiesPage();
        }

        [When(@"I verify the details on send quote conditions page")]
        public void WhenIVerifyTheDetailsOnSendQuoteConditionsPage()
        {
            SendQuotePage.SendQuoteConditionsPage();
        }
        [When(@"I verify the details on send quote special terms page")]
        public void WhenIVerifyTheDetailsOnSendQuotespecialTermsPage()
        {
            SendQuotePage.SendQuotepecialTermsPage();
        }

        [When(@"I verify the details on send quote fees page")]
        public void WhenIVerifyTheDetailsOnSendQuoteFeesPage()
        {
            SendQuotePage.SendQuoteFeesPage();
        }

        [When(@"I verify the details on send quote file review page")]
        public void WhenIVerifyTheDetailsOnSendQuoteFileReviewPage()
        {
            SendQuotePage.SendQuoteFileReviewPage();
        }

        [When(@"I verify the details on send quote endorsements page")]
        public void WhenIVerifyTheDetailsOnSendQuoteEndorsementsPage()
        {
            SendQuotePage.SendQuoteEndorsementPage();
        }

        [When(@"I verify the details on send quote confirm details page")]
        public void WhenIVerifyTheDetailsOnSendQuoteConfirmDetailsPage()
        {
            SendQuotePage.SendQuoteConfirmDetailsPage();
        }

        [When(@"I verify the details on send quote correspondence page")]
        public void WhenIVerifyTheDetailsOnSendQuoteCorrespondencePage()
        {
            SendQuotePage.SendQuoteCorrespondencePage();
        }
        [Then(@"I click on send quote button to see quoted details on the dashboard")]
        public void ThenIClickOnSendQuoteButtonToSeeQuotedDetailsOnTheDashboard()
        {
            SendQuotePage.SendQuoteDetailsOnTheDashboard();
        }

    }
}
