﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TechTalk.SpecFlow;

namespace RegressionPacks.Steps
{
    [Binding]
    public class PGSinglePlotPremiumsSteps : Support.Pages
    {
        [When(@"I Create a PG NewhomesQuote and Verify Premiums On Single Plot")]
        public void WhenICreateAPGNewhomesQuoteAndVerifyPremiumsOnSinglePlot()
        {
            PGSinglePlotPremiums.PGNewhomesPremiums();
        }

        [When(@"I Create a PG CommercialQuote and Verify Premiums On Single Plot")]
        public void WhenICreateAPGCommercialQuoteAndVerifyPremiumsOnSinglePlot()
        {
            PGSinglePlotPremiums.PGCommercialPremiums();
        }

        [When(@"I Create a PG SocialHousingQuote and Verify Premiums On Single Plot")]
        public void WhenICreateAPGSocialHousingQuoteAndVerifyPremiumsOnSinglePlot()
        {
            PGSinglePlotPremiums.PGSocialHousingPremiums();
        }

        [When(@"I Create a PG PrivateRentalQuote and Verify Premiums On Single Plot")]
        public void WhenICreateAPGPrivateRentalQuoteAndVerifyPremiumsOnSinglePlot()
        {
            PGSinglePlotPremiums.PGPrivateRentalPremiums();
        }

        [When(@"I Create a PG CompletedHousingQuote and Verify Premiums On Single Plot")]
        public void WhenICreateAPGCompletedHousingQuoteAndVerifyPremiumsOnSinglePlot()
        {
            PGSinglePlotPremiums.PGCompletedHousingPremiums();
        }

        [When(@"I Create a PG SelfBuildQuote and Verify Premiums On Single Plot")]
        public void WhenICreateAPGSelfBuildQuoteAndVerifyPremiumsOnSinglePlot()
        {
            PGSinglePlotPremiums.PGSelfBuildPremiums();
        }

        [When(@"I Create a PG CI Residential Quote and Verify Premiums On Single Plot")]
        public void WhenICreateAPGCIResidentialQuoteAndVerifyPremiumsOnSinglePlot()
        {
            PGSinglePlotPremiums.PGCIResidentialPremiums();
        }

        [When(@"I Create a PG CI Commercial and Verify Premiums On Single Plot")]
        public void WhenICreateAPGCICommercialAndVerifyPremiumsOnSinglePlot()
        {
            PGSinglePlotPremiums.PGCICommercialPremiums();
        }

        [When(@"I Create a Quote with Residential BC Products and Verify Premiums On Single Plot")]
        public void WhenICreateAQuoteWithResidentialBCProductsAndVerifyPremiumsOnSinglePlot()
        {
            PGSinglePlotPremiums.ResidentialBCPremium();
        }

        [When(@"I Create a Quote with Commercial BC Products and Verify Premiums On Single Plot")]
        public void WhenICreateAQuoteWithCommercialBCProductsAndVerifyPremiumsOnSinglePlot()
        {
            PGSinglePlotPremiums.CommercialBCPremium();
        }


        //12 Years Cover Length For Single Products 
        [When(@"I Create a PG CommercialQuote For Twelve Years Cover Length and Verify Premiums On Single Plot")]
        public void WhenICreateAPGCommercialQuoteForTwelveYearsCoverLengthAndVerifyPremiumsOnSinglePlot()
        {
          
        }

        [When(@"I Create a PG SocialHousingQuote For Twelve Years Cover Length and Verify Premiums On Single Plot")]
        public void WhenICreateAPGSocialHousingQuoteForTwelveYearsCoverLengthAndVerifyPremiumsOnSinglePlot()
        {
            
        }

        [When(@"I Create a PG PrivateRentalQuote For Twelve Years Cover Length and Verify Premiums On Single Plot")]
        public void WhenICreateAPGPrivateRentalQuoteForTwelveYearsCoverLengthAndVerifyPremiumsOnSinglePlot()
        {
          
        }

        [When(@"I Create a PG CI Residential Quote For Twelve Years Cover Length and Verify Premiums On Single Plot")]
        public void WhenICreateAPGCIResidentialQuoteForTwelveYearsCoverLengthAndVerifyPremiumsOnSinglePlot()
        {
            
        }

        [When(@"I Create a PG CI Commercial For Twelve Years Cover Length and Verify Premiums On Single Plot")]
        public void WhenICreateAPGCICommercialForTwelveYearsCoverLengthAndVerifyPremiumsOnSinglePlot()
        {
            
        }







    }
}
