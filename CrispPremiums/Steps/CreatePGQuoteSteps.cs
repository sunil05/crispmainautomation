﻿using RegressionPacks.Pages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TechTalk.SpecFlow;

namespace RegressionPacks.Steps
{
    [Binding]
    public class CreatePGQuoteSteps : Support.Pages
    {
        [When(@"I provide details on key site details page for PG Brand")]
        public void WhenIProvideDetailsOnKeySiteDetailsPageForPGBrand()
        {
            AddPGQuotePage.PGKeySiteDetailsForAviva();
        }
        [When(@"I select relavant PG roles on roles page")]
        public void WhenISelectRelavantPGRolesOnRolesPage()
        {
            AddLABCQuotePage.RolesDetails();
        }

    }
}
