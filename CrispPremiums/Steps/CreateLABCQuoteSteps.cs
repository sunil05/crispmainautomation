﻿using NUnit.Framework;
using RegressionPacks.Pages;
using RegressionPacks.Support;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using TechTalk.SpecFlow;

namespace RegressionPacks.Steps
{
    [Binding]
    public class CreateLABCQuoteSteps : Support.Pages
    {
        //Application Submit Stage
        [When(@"I click on plus button and select  quote option")]
        public void WhenIClickOnPlusButtonAndSelectQuoteOption()
        {
            WaitForElement(Dashboardpage.PlusButton);
            Dashboardpage.PlusButton.Click();
            WaitForElement(Dashboardpage.QuoteOption);
            Dashboardpage.QuoteOption.Click();
            WaitForElement(AddLABCQuotePage.AddNewQuoteDiv);
            Assert.IsTrue(AddLABCQuotePage.AddNewQuoteDiv.Displayed, "Failed on display the Quote Wizard");
        }
        [When(@"I read following '(.*)' details")]
        public void WhenIReadFollowingDetails(string plotdata)
        {
            ExtensionMethods.ReadExcelPlotData(plotdata);
        }

        [When(@"I provide details on key site details page")]
        public void WhenIProvideDetailsOnKeySiteDetailsPage()
        {
            AddLABCQuotePage.KeySiteDetailsPage();
        }

        [When(@"I provide details on other site details page")]
        public void WhenIProvideDetailsOnOtherSiteDetailsPage()
        {
            AddLABCQuotePage.OtherSiteDetailsPage();
        }
        [When(@"I provide following '(.*)' data on plot schedule page")]
        public void WhenIProvideFollowingDataOnPlotSchedulePage(string plotdata)
        {

            AddLABCQuotePage.PlotSchedulePage(plotdata);
        }

        [When(@"I provide details on the product details page")]
        public void WhenIProvideDetailsOnTheProductDetailsPage()
        {
            AddLABCQuotePage.ProductsDetailsPage();
        }

        [When(@"I provide details on additional questions page")]
        public void WhenIProvideDetailsOnAdditionalQuestionsPage()
        {

            AddLABCQuotePage.AdditionalQuestionsPage();
        }

        [When(@"I select relavant roles on roles page")]
        public void WhenISelectRelavantRolesOnRolesPage()
        {
            AddLABCQuotePage.RolesDetails();
        }

        [When(@"I provide the details on declaration page")]
        public void WhenIProvideTheDetailsOnDeclarationPage()
        {
            AddLABCQuotePage.DeclarationDetailsPage();
        }
        [Then(@"I click on complete button to see the application submitted details on the dashboard")]
        public void ThenIClickOnCompleteButtonToSeeTheApplicationSubmittedDetailsOnTheDashboard()
        {
            Thread.Sleep(1000);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(1000);
            WaitForElement(AddLABCQuotePage.SaveButton);
            WaitForElementToClick(AddLABCQuotePage.SaveButton);
            AddLABCQuotePage.SaveButton.Click();
            Thread.Sleep(1000);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(1000);
            WaitForElement(AddLABCQuotePage.SubmitButton);
            WaitForElementToClick(AddLABCQuotePage.SubmitButton);
            AddLABCQuotePage.SubmitButton.Click();
            Thread.Sleep(1000);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(1000);
            CloseCrispDiv(1);
            WaitForElement(Dashboardpage.QuoteApplicationStatusSubmitted);
            Assert.IsTrue(Dashboardpage.QuoteApplicationStatusSubmitted.Displayed, "Failed on Submit Quote Application");
            var QuoteRef = AddLABCQuotePage.SiteRefDetails.Text;
            Statics.QuoteRef = QuoteRef;
            ExtensionMethods.CreateSpreadsheet();           
           // AddLABCQuotePage.SetupEmailonLocalAuthority();
        }

    }
}
