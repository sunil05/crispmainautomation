﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TechTalk.SpecFlow;

namespace RegressionPacks.Steps
{
    [Binding]
    public sealed class PGHVSAvivaPremiumsSteps : Support.Pages
    {
        [When(@"I Create a PG Newhomes HVS Aviva Quote and Verify Premiums")]
        public void WhenICreateAPGNewhomesHVSAvivaQuoteAndVerifyPremiums()
        {
            PGHVSAvivaPremiums.PGNewhomesHVSAvivaPremiums();
        }

        [When(@"I Create a PG Commercial HVS Aviva Quote and Verify HVS Premiums")]
        public void WhenICreateAPGCommercialHVSAvivaQuoteAndVerifyHVSPremiums()
        {
            PGHVSAvivaPremiums.PGCommercialHVSAvivaPremiums();
        }

        [When(@"I Create a PG SocialHousing HVS Aviva Quote and Verify HVS Premiums")]
        public void WhenICreateAPGSocialHousingHVSAvivaQuoteAndVerifyHVSPremiums()
        {
            PGHVSAvivaPremiums.PGSocialHousingHVSAvivaPremiums();
        }

        [When(@"I Create a PG PrivateRental HVS Aviva Quote and Verify Premiums")]
        public void WhenICreateAPGPrivateRentalHVSAvivaQuoteAndVerifyPremiums()
        {
            PGHVSAvivaPremiums.PGPrivateRentalHVSAvivaPremiums();
        }
        [When(@"I Create a PG CI Residential Aviva Quote and Verify Premiums")]
        public void WhenICreateAPGCIResidentialAvivaQuoteAndVerifyPremiums()
        {
            PGHVSAvivaPremiums.PGCIResidentialHVSAvivaPremiums();
        }

        [When(@"I Create a PG CI Commercial Aviva Quote and Verify Premiums")]
        public void WhenICreateAPGCICommercialAvivaQuoteAndVerifyPremiums()
        {
            PGHVSAvivaPremiums.PGCICommercialHVSAvivaPremiums(); 
        }

        [When(@"I Create a PG Commercial HVS Aviva Quote For Twelve Years Cover Length and Verify HVS Premiums")]
        public void WhenICreateAPGCommercialHVSAvivaQuoteForTwelveYearsCoverLengthAndVerifyHVSPremiums()
        {
            PGHVSAvivaPremiums.PGCommercialHVSAvivaPremiumsFor12YearsCoverLength();
        }

        [When(@"I Create a PG SocialHousing HVS Aviva Quote For Twelve Years Cover Length and Verify HVS Premiums")]
        public void WhenICreateAPGSocialHousingHVSAvivaQuoteForTwelveYearsCoverLengthAndVerifyHVSPremiums()
        {
            PGHVSAvivaPremiums.PGSocialHousingHVSAvivaPremiumsFor12YearsCoverLength();
        }

        [When(@"I Create a PG PrivateRental HVS Aviva Quote For Twelve Years Cover Length and Verify Premiums")]
        public void WhenICreateAPGPrivateRentalHVSAvivaQuoteForTwelveYearsCoverLengthAndVerifyPremiums()
        {
            PGHVSAvivaPremiums.PGPrivateRentalHVSAvivaPremiumsFor12YearsCoverLength();
        }

        [When(@"I Create a PG CI Residential Aviva Quote For Twelve Years Cover Length and Verify Premiums")]
        public void WhenICreateAPGCIResidentialAvivaQuoteForTwelveYearsCoverLengthAndVerifyPremiums()
        {
            PGHVSAvivaPremiums.PGCIResidentialHVSAvivaPremiumsFor12YearsCoverLength();
        }

        [When(@"I Create a PG CI Commercial Aviva Quote For Twelve Years Cover Length and Verify Premiums")]
        public void WhenICreateAPGCICommercialAvivaQuoteForTwelveYearsCoverLengthAndVerifyPremiums()
        {
            PGHVSAvivaPremiums.PGCICommercialHVSAvivaPremiumsFor12YearsCoverLength();
        }

    }
}
