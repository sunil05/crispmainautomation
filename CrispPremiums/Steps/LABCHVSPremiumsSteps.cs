﻿using RegressionPacks.Pages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TechTalk.SpecFlow;

namespace RegressionPacks.Steps
{
    [Binding]
    public sealed class LABCHVSPremiumsSteps : Support.Pages
    {
        [When(@"I Create a Newhomes HVS Quote and Verify Premiums")]
        public void WhenICreateANewhomesHVSQuoteAndVerifyPremiums()
        {
            LABCHVSPremiums.LABCNewhomesHVSPremiums();
        }        

        [When(@"I Create a Commercial HVS Quote and Verify HVS Premiums")]
        public void WhenICreateACommercialHVSQuoteAndVerifyHVSPremiums()
        {
            LABCHVSPremiums.LABCCommercialHVSPremiums();
        }

        [When(@"I Create a SocialHousing HVS Quote and Verify HVS Premiums")]
        public void WhenICreateASocialHousingHVSQuoteAndVerifyHVSPremiums()
        {
            LABCHVSPremiums.LABCSocialHousingHVSPremiums();
        }

        [When(@"I Create a PrivateRental HVSQuote and Verify Premiums")]
        public void WhenICreateAPrivateRentalHVSQuoteAndVerifyPremiums()
        {
            LABCHVSPremiums.LABCPrivateRentalHVSPremiums();
        }

        [When(@"I Create a Commercial HVS Quote for twelve years cover length and Verify HVS Premiums")]
        public void WhenICreateACommercialHVSQuoteForTwelveYearsCoverLengthAndVerifyHVSPremiums()
        {
            LABCHVSPremiums.LABCCommercialHVSPremiumsFor12YearsCoverLength();
        }

        [When(@"I Create a SocialHousing HVS Quote for twelve years cover length and Verify HVS Premiums")]
        public void WhenICreateASocialHousingHVSQuoteForTwelveYearsCoverLengthAndVerifyHVSPremiums()
        {
            LABCHVSPremiums.LABCSocialHousingHVSPremiumsFor12YearsCoverLength();
        }


        [Then(@"I Should Successfully Verify the HVS Premiums")]
        public void ThenIShouldSuccessfullyVerifyTheHVSPremiums()
        {
            foreach (var plot in Premiums.eachplotrow)
            {
                Console.WriteLine($"Premiums Details Are Verified for Following Plots Details : {plot.ProductName},  {plot.EstrimatedSalesPrice},  {plot.UnitType}, {plot.ConstructionType},  {plot.StageOfWork}");
                Console.WriteLine($"Premim Details are Verified at {Premiums.Rating} : Structural Premium =  {Premiums.StructuralFee} , Insolvancy Premium = {Premiums.InsolvencyFee}, TA Fee = {Premiums.TechnicalAuditFee}");
            }

        }

    }
}
