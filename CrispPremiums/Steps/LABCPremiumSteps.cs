﻿using RegressionPacks.Pages;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using TechTalk.SpecFlow;

namespace RegressionPacks.Steps
{
    [Binding]
    public  class LABCPremiumSteps : Support.Pages
    {
        [When(@"I Create a Newhomes Quote and Verify Premiums")]
        public void WhenICreateANewhomesQuoteAndVerifyPremiums()
        {
            LABCPremiums.LABCNewhomesPremiums();
        }
        [When(@"I Create a CommercialQuote and Verify Premiums")]
        public void WhenICreateACommercialQuoteAndVerifyPremiums()
        {            
           LABCPremiums.LABCCommercialPremiums();
        }
        [When(@"I Create a SocialHousingQuote and Verify Premiums")]
        public void WhenICreateASocialHousingQuoteAndVerifyPremiums()
        {
            LABCPremiums.LABCSocialHousingPremiums();
        }
        [When(@"I Create a PrivateRentalQuote and Verify Premiums")]
        public void WhenICreateAPrivateRentalQuoteAndVerifyPremiums()
        {
            LABCPremiums.LABCPrivateRentalPremiums();
        }
        [When(@"I Create a CompletedHousingQuote and Verify Premiums")]
        public void WhenICreateACompletedHousingQuoteAndVerifyPremiums()
        {            
            LABCPremiums.LABCCompletedHousingPremiums();
        }

        [When(@"I Create a SelfBuildQuote and Verify Premiums")]
        public void WhenICreateASelfBuildQuoteAndVerifyPremiums()
        {
            LABCPremiums.LABCSelfBuildPremiums();
        }

        [When(@"I Create a CommercialQuote  for twelve years cover length and Verify Premiums")]
        public void WhenICreateACommercialQuoteForTwelveYearsCoverLengthAndVerifyPremiums()
        {
            LABCPremiums.LABCCommercialPremiumsFor12YearsCoverLength();
        }

        [When(@"I Create a SocialHousingQuote for twelve years cover length and Verify Premiums")]
        public void WhenICreateASocialHousingQuoteForTwelveYearsCoverLengthAndVerifyPremiums()
        {
            LABCPremiums.LABCSocialHousingPremiumsFor12YearsCoverLength();
        }

        [When(@"I Create a PrivateRentalQuote for twelve years cover length and Verify Premiums")]
        public void WhenICreateAPrivateRentalQuoteForTwelveYearsCoverLengthAndVerifyPremiums()
        {
            LABCPremiums.LABCPrivateRentalPremiumsFor12YearsCoverLength();
        }


        [Then(@"I Should Successfully Verify the Premiums")]
        public void ThenIShouldSuccessfullyVerifyThePremiums()
        {
            foreach (var plot in Premiums.eachplotrow)
            {
                Console.WriteLine($"Premiums Details Are Verified for Following Plots Details : {plot.ProductName},  {plot.EstrimatedSalesPrice},  {plot.UnitType}, {plot.ConstructionType},  {plot.StageOfWork}");
                Console.WriteLine($"Premim Details are Verified at {Premiums.Rating} : Structural Premium =  {Premiums.StructuralFee} , Insolvancy Premium = {Premiums.InsolvencyFee}, TA Fee = {Premiums.TechnicalAuditFee}");
            }
        }
      
    }
}
