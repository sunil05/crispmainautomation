﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CrispAutomation.ExtranetPages;
using CrispAutomation.Features;
using CrispAutomation.Pages;
using CrispAutomation.Permissions;
using log4net;
using Protractor;

namespace CrispAutomation.Support
{
    public class Pages : Base
    {
        public static LoginPage Loginpage
        {
            get
            {
                var login = new LoginPage(Driver);
                return login;
            }
        }
        public static DashboardPage Dashboardpage
        {
            get
            {
                var dashboard = new DashboardPage(Driver);
                return dashboard;
            }
        }

        public static AddPersonPage AddPersonpage
        {
            get
            {
                var addperson = new AddPersonPage(Driver);
                return addperson;
            }
        }
        public static EditPersonDetailsPage EditPersonDetailsPage
        {
            get
            {
                var editpersondetails = new EditPersonDetailsPage(Driver);
                return editpersondetails;
            }
        }
        public static AddRatingToPersonPage AddRatingToPersonPage
        {
            get
            {
                var addratingtoperson = new AddRatingToPersonPage(Driver);
                return addratingtoperson;
            }
        }
        public static EditRatingToPersonPage EditRatingToPersonPage
        {
            get
            {
                var editratingtoperson = new EditRatingToPersonPage(Driver);
                return editratingtoperson;
            }
        }
        public static ContactSearchPage Contactsearchpage
        {
            get
            {
                var contact = new ContactSearchPage(Driver);
                return contact;
            }
        }
        public static AddCompanyPage AddCompanyPage
        {
            get
            {
                var addcompany = new AddCompanyPage(Driver);
                return addcompany;
            }
        }
        public static EditCompanyDetailsPage EditCompanyDetailsPage
        {
            get
            {
                var editcompanydetails = new EditCompanyDetailsPage(Driver);
                return editcompanydetails;
            }
        }
        public static AddRatingToCompanyPage AddRatingToCompanyPage
        {
            get
            {
                var addratingtocompany = new AddRatingToCompanyPage(Driver);
                return addratingtocompany;
            }
        }
        public static EditRatingToCompanyPage EditRatingToCompanyPage
        {
            get
            {
                var editratingtocompany = new EditRatingToCompanyPage(Driver);
                return editratingtocompany;
            }
        }
        public static AddCompanyGroupPage AddCompanyGroupPage
        {
            get
            {
                var addcompanygroup = new AddCompanyGroupPage(Driver);
                return addcompanygroup;
            }
        }
        public static ManageCompanyListPage ManageCompanyListPage
        {
            get
            {
                var managecompanylist = new ManageCompanyListPage(Driver);
                return managecompanylist;
            }
        }        
        public static NoPermissionsPage NoPermissionsPage
        {
            get
            {
                var noPermissionsPage = new NoPermissionsPage(Driver);
                return noPermissionsPage;
            }
        }
        public static ManagePermissionsPage ManagePermissionsPage
        {
            get
            {
                var managepermissionspage = new ManagePermissionsPage(Driver);
                return managepermissionspage;
            }
        }
        public static ContactsPermissionsPage ContactsPermissionsPage
        {
            get
            {
                var contactPermissionsPage = new ContactsPermissionsPage(Driver);
                return contactPermissionsPage;
            }
        }
        public static LeadPermissionsPage LeadPermissionsPage
        {
            get
            {
                var leadPermissionsPage = new LeadPermissionsPage(Driver);
                return leadPermissionsPage;
            }
        }

        public static QuotePermissionsPage QuotePermissionsPage
        {
            get
            {
                var quotePermissionsPage = new QuotePermissionsPage(Driver);
                return quotePermissionsPage;
            }
        }
        public static OrderPermissionsPage OrderPermissionsPage
        {
            get
            {
                var orderPermissionsPage = new OrderPermissionsPage(Driver);
                return orderPermissionsPage;
            }
        }
       
        public static ReconstructionCostLimitPermissionsPage ReconstructionCostLimitPermissionsPage
        {
            get
            {
                var reconstructionCostLimitPermissionsPage = new ReconstructionCostLimitPermissionsPage(Driver);
                return reconstructionCostLimitPermissionsPage;
            }
        }
        public static QuotableProductsPermissionsPage QuotableProductsPermissionsPage
        {
            get
            {
                var quotableProductsPermissionsPage = new QuotableProductsPermissionsPage(Driver);
                return quotableProductsPermissionsPage;
            }
        }

        public static InternalUserPermissionsPage InternalUserPermissionsPage
        {
            get
            {
                var internalUserPermissionsPage = new InternalUserPermissionsPage(Driver);
                return internalUserPermissionsPage;
            }
        }
        public static ManageCompanyListsPermissionsPage ManageCompanyListsPermissionsPage
        {
            get
            {
                var manageCompanyListsPermissionsPage = new ManageCompanyListsPermissionsPage(Driver);
                return manageCompanyListsPermissionsPage;
            }
        }

        public static ManageCompanyGroupPermissionsPage ManageCompanGroupPermissions
        {
            get
            {
                var manageCompanGroupPermissions = new ManageCompanyGroupPermissionsPage(Driver);
                return manageCompanGroupPermissions;
            }
        }
       
        public static OverridableProcessesPage OverridableProcessesPage
        {
            get
            {
                var overridableProcessesPage = new OverridableProcessesPage(Driver);
                return overridableProcessesPage;
            }
        }

        public static OverridablePaymentValidationPermissionsPage OverridablePaymentValidationPermissions
        {
            get
            {
                var OverridablePaymentValidationPermissions = new OverridablePaymentValidationPermissionsPage(Driver);
                return OverridablePaymentValidationPermissions;
            }
        }
        public static RefurbAssessmentPermissionsPage RefurbAssessmentPermissionsPage
        {
            get
            {
                var RefurbAssessmentPermissionsPage = new RefurbAssessmentPermissionsPage(Driver);
                return RefurbAssessmentPermissionsPage;
            }
        }
        public static BrandPermissionsPage BrandPermissionsPage
        {
            get
            {
                var brandPermissionsPage = new BrandPermissionsPage(Driver);
                return brandPermissionsPage;
            }
        }
        public static CompanyBlockingPermissionsPage CompanyBlockingPermissionsPage
        {
            get
            {
                var companyBlockingPermissionsPage = new CompanyBlockingPermissionsPage(Driver);
                return companyBlockingPermissionsPage;
            }
        }
        public static CoverNotesPermissionsPage CoverNotesPermissionsPage
        {
            get
            {
                var coverNotesPermissionsPage = new CoverNotesPermissionsPage(Driver);
                return coverNotesPermissionsPage;
            }
        }
        public static DesignReviewPermissionsPage DesignReviewPermissionsPage
        {
            get
            {
                var designReviewPermissionsPage = new DesignReviewPermissionsPage(Driver);
                return designReviewPermissionsPage;
            }
        }
        public static EndorsementsPermissionPage EndorsementsPermissionPage
        {
            get
            {
                var endorsementsPermissionPage = new EndorsementsPermissionPage(Driver);
                return endorsementsPermissionPage;
            }
        }
        public static ExtranetPermissionsPage ExtranetPermissionsPage
        {
            get
            {
                var extranetPermissionsPage = new ExtranetPermissionsPage(Driver);
                return extranetPermissionsPage;
            }
        }
        public static FinancialAccountsPermissionsPage FinancialAccountsPermissionsPage
        {
            get
            {
                var financialAccountsPermissionsPage = new FinancialAccountsPermissionsPage(Driver);
                return financialAccountsPermissionsPage;
            }
        }
        public static InspectionPermissionsPage InspectionPermissionsPage
        {
            get
            {
                var inspectionPermissionsPage = new InspectionPermissionsPage(Driver);
                return inspectionPermissionsPage;
            }
        }
        public static LocalAuthoritiesPermissionsPage LocalAuthoritiesPermissionsPage
        {
            get
            {
                var localAuthoritiesPermissionsPage = new LocalAuthoritiesPermissionsPage(Driver);
                return localAuthoritiesPermissionsPage;
            }
        }
        public static RatingPermissionsPage RatingPermissionsPage
        {
            get
            {
                var ratingPermissionsPage = new RatingPermissionsPage(Driver);
                return ratingPermissionsPage;
            }
        }
        public static RegistrationPermissionsPage RegistrationPermissionsPage
        {
            get
            {
                var registrationPermissionsPage = new RegistrationPermissionsPage(Driver);
                return registrationPermissionsPage;
            }
        }
        public static SalesAppointmentsPermissionsPage SalesAppointmentsPermissionsPage
        {
            get
            {
                var salesAppointmentsPermissionsPage = new SalesAppointmentsPermissionsPage(Driver);
                return salesAppointmentsPermissionsPage;
            }
        }
        public static SiteRiskAssessmentPermissionsPage SiteRiskAssessmentPermissionsPage
        {
            get
            {
                var siteRiskAssessmentPermissionsPage = new SiteRiskAssessmentPermissionsPage(Driver);
                return siteRiskAssessmentPermissionsPage;
            }
        }
        public static StructuralReviewPermissionsPage StructuralReviewPermissionsPage
        {
            get
            {
                var structuralReviewPermissionsPage = new StructuralReviewPermissionsPage(Driver);
                return structuralReviewPermissionsPage;
            }
        }
        public static AddLeadPage AddLeadPage
        {
            get
            {
                var addleadpage = new AddLeadPage(Driver);
                return addleadpage;
            }
        }
        public static AddLABCQuotePage AddLABCQuotePage
        {
            get
            {
                var addlabcquotepage = new AddLABCQuotePage(Driver);
                return addlabcquotepage;
            }
        }
        public static AddPGQuotePage AddPGQuotePage
        {
            get
            {
                var addpgquotepage = new AddPGQuotePage(Driver);
                return addpgquotepage;
            }
        }

        public static SendQuotePage SendQuotePage
        {
            get
            {
                var sendquotepage = new SendQuotePage(Driver);
                return sendquotepage;
            }
        }
        public static AcceptQuotePage AcceptQuotePage
        {
            get
            {
                var acceptquotepage = new AcceptQuotePage(Driver);
                return acceptquotepage;
            }
        }

        public static SendInitialNotice SendIntialNotice
        {
            get
            {
                var sendintialnoticepage = new SendInitialNotice(Driver);
                return sendintialnoticepage;
            }
        }
        public static RespondIntialNotice RespondIntialNotice
        {
            get
            {
                var respondintialnoticepage = new RespondIntialNotice(Driver);
                return respondintialnoticepage;
            }
        }

        public static RejectLeadPage RejectLeadPage
        {
            get
            {
                var rejectleadpage = new RejectLeadPage(Driver);
                return rejectleadpage;
            }
        }

        public static ReInstateLeadPage ReInstateLeadPage
        {
            get
            {
                var reinstateleadpage = new ReInstateLeadPage(Driver);
                return reinstateleadpage;
            }
        }
        public static FinancesPage FinancesPage
        {
            get
            {
                var financesPage = new FinancesPage(Driver);
                return financesPage;
            }
        }
        public static ManageConditionsPage ManageConditionsPage
        {
            get
            {
                var manageConditionsPage = new ManageConditionsPage(Driver);
                return manageConditionsPage;
            }
        }
        public static AdditionalMethodsPage AdditionalMethodsPage
        {
            get
            {
                var addtionalMethods = new AdditionalMethodsPage(Driver);
                return addtionalMethods;
            }
        }
        public static SurveyorLoginPage SurveyorLoginPage
        {
            get
            {
                var surveyorLoginPage = new SurveyorLoginPage(Driver);
                return surveyorLoginPage;
            }
        }

        public static FinalNoticePage FinalNoticePage
        {
            get
            {
                var finalNoticePage = new FinalNoticePage(Driver);
                return finalNoticePage;
            }
        }
        public static ActivitiesPage ActivitiesPage
        {
            get
            {
                var activitiesPage = new ActivitiesPage(Driver);
                return activitiesPage;
            }
        }
        public static ShortListLeadPage ShortListLeadPage
        {
            get
            {
                var shortListLeadPage = new ShortListLeadPage(Driver);
                return shortListLeadPage;
            }
        }
        public static SiteRiskAssessmentPage SiteRiskAssessmentPage
        {
            get
            {
                var siteRiskAssessmentPage = new SiteRiskAssessmentPage(Driver);
                return siteRiskAssessmentPage;

            }
        }
        public static RefurbishmentAssessmentPage RefurbishmentAssessmentPage
        {
            get
            {
                var refurbishmentAssessmentPage = new RefurbishmentAssessmentPage(Driver);
                return refurbishmentAssessmentPage;
            }
        }       
        public static DocumentComparisionPage DocumentComparisionPage
        {
            get
            {
                var documentComparisionPage = new DocumentComparisionPage(Driver);
                return documentComparisionPage;
            }
        }
        public static  HTTPClient HTTPClient
        {
            get
            {
                var httpclient = new HTTPClient(Driver);
                return httpclient;
            }
        }
        public static ILog log = LogManager.GetLogger(typeof(Pages));

        public static SiteInspectionPage SiteInspectionPage
        {
            get
            {
                var siteInspectionPage = new SiteInspectionPage(Driver);
                return siteInspectionPage;
            }
        }

        public static IssueCertificatePage IssueCertificatePage
        {
            get
            {
                var issueCertificatePage = new IssueCertificatePage(Driver);
                return issueCertificatePage;
            }
        }
        public static PGRegistration PGRegistration
        {
            get
            {
                var pgRegistration = new PGRegistration(Driver);
                return pgRegistration;
            }
        }
        public static LABCRegistrations LABCRegistrations
        {
            get
            {
                var lABCRegistrations = new LABCRegistrations(Driver);
                return lABCRegistrations;
            }
        }
        public static DesignReviewPage DesignReviewPage
        {
            get
            {
                var designReviewPage = new DesignReviewPage(Driver);
                return designReviewPage;
            }
        }
        public static EngineerReviewPage EngineerReviewPage
        {
            get
            {
                var engineerReviewPage = new EngineerReviewPage(Driver);
                return engineerReviewPage;
            }
        }
        public static CoverNotesPage CoverNotesPage
        {
            get
            {
                var coverNotesPage = new CoverNotesPage(Driver);
                return coverNotesPage;
            }
        }
        public static SurveyorAssessments SurveyorAssessments
        {
            get
            {
                var surveyorAssessments = new SurveyorAssessments(Driver);
                return surveyorAssessments;
            }
        }
        public static SurveyorDocsSRA SurveyorDocsSRA
        {
            get
            {
                var surveyorDocsSRA = new SurveyorDocsSRA(Driver);
                return surveyorDocsSRA;
            }
        }
        public static SurveyorDocsRA SurveyorDocsRA
        {
            get
            {
                var surveyorDocsRA = new SurveyorDocsRA(Driver);
                return surveyorDocsRA;
            }
        }
        public static SurveyorDocsDR SurveyorDocsDR
        {
            get
            {
                var surveyorDocsDR = new SurveyorDocsDR(Driver);
                return surveyorDocsDR;
            }
        }
        public static SurveyorDocsER SurveyorDocsER
        {
            get
            {
                var surveyorDocsER = new SurveyorDocsER(Driver);
                return surveyorDocsER;
            }
        }       
        public static SurveyorDocsSiteInspection SurveyorDocsSiteInspection
        {
            get
            {
                var surveyorDocsSiteInspection = new SurveyorDocsSiteInspection(Driver);
                return surveyorDocsSiteInspection;
            }
        }

        public static SurveyorDocs SurveyorDocs
        {
            get
            {
                var surveyorDocs = new SurveyorDocs(Driver);
                return surveyorDocs;
            }
        }
        public static PageUtils PageUtils
        {
            get
            {
                var pageUtils = new PageUtils(Driver);
                return pageUtils;
            }
        }
        public static BondsPage BondsPage
        {
            get
            {
                var bondsPage = new BondsPage(Driver);
                return bondsPage;
            }
        }

        //Extranet Pages
        public static ExtranetLoginPage ExtranetLoginPage
        {
            get
            {
                var extranetLoginPage = new ExtranetLoginPage(Driver);
                return extranetLoginPage;
            }
        }
        public static ExtranetHomePage ExtranetHomePage
        {
            get
            {
                var extranetHomePage = new ExtranetHomePage(Driver);
                return extranetHomePage;
            }
        }
        public static ExtranetQuotesPage ExtranetQuotesPage
        {
            get
            {
                var extranetQuotesPage = new ExtranetQuotesPage(Driver);
                return extranetQuotesPage;
            }
        }
        public static ExtranetOrdersPage ExtranetOrdersPage
        {
            get
            {
                var extranetOrdersPage = new ExtranetOrdersPage(Driver);
                return extranetOrdersPage;
            }
        }

        public static ExtranetCompanyPage ExtranetCompanyPage
        {
            get
            {
                var extranetCompanyPage = new ExtranetCompanyPage(Driver);
                return extranetCompanyPage;
            }
        }
        public static ExtranetRequestIssueCOIPage ExtranetRequestIssueCOIPage
        {
            get
            {
                var extranetRequestIssueCOIPage = new ExtranetRequestIssueCOIPage(Driver);
                return extranetRequestIssueCOIPage;
            }
        }
        public static ExtranetRegistrationsPage ExtranetRegistrationsPage
        {
            get
            {
                var extranetRegistrationsPage = new ExtranetRegistrationsPage(Driver);
                return extranetRegistrationsPage;
            }
        }
        public static ExtranetFeePage ExtranetFeePage
        {
            get
            {
                var extranetFeePage = new ExtranetFeePage(Driver);
                return extranetFeePage;
            }
        }

        public static ExtranetTasksPage ExtranetTasksPage
        {
            get
            {
                var extranetTasksPage = new ExtranetTasksPage(Driver);
                return extranetTasksPage;
            }
        }

        public static DBConnection DBConnection
        {
            get
            {
                var dBConnection = new DBConnection();
                return dBConnection;
            }
        }
        public static RegistrationDocs RegistrationDocs
        {
            get
            {
                var registrationDocs = new RegistrationDocs(Driver);
                return registrationDocs;
            }
        }
      
        public static SurveyorInformationPage SurveyorInformationPage
        {
            get
            {
                var surveyorInformationPage = new SurveyorInformationPage(Driver);
                return surveyorInformationPage;
            }
        }
        public static TasksPage TasksPage
        {
            get
            {
                var tasksPage = new TasksPage(Driver);
                return tasksPage;
            }
        }
        public static ConditionsPage ConditionsPage
        {
            get
            {
                var conditionsPage = new ConditionsPage(Driver);
                return conditionsPage;
            }
        }        
        public static RescindProcessPage RescindProcessPage
        {
            get
            {
                var rescindProcessPage = new RescindProcessPage(Driver);
                return rescindProcessPage;
            }
        }
        public static ReferralsPage ReferralsPage
        {
            get
            {
                var referralsPage = new ReferralsPage(Driver);
                return referralsPage;
            }
        }       
    }
}
