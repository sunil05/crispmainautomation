﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Threading;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.UI;
using TechTalk.SpecFlow;
using Protractor;

namespace CrispAutomation.Support
{
    public class Base
    {
        protected static RemoteWebDriver Driver => ScenarioContext.Current.Get<RemoteWebDriver>("currentDriver");
        public bool WaitForElement(IWebElement elementLocator)
        {           
            bool result = false;
            bool enabled = false;         
            int attempts = 0;
            do
            {
                attempts++;
                try
                {                  
                    GenerateWebDriverWait().IgnoreExceptionTypes(typeof(NoSuchElementException));
                    GenerateWebDriverWait().IgnoreExceptionTypes(typeof(ElementNotVisibleException));
                    GenerateWebDriverWait().IgnoreExceptionTypes(typeof(ElementNotInteractableException));
                    GenerateWebDriverWait().IgnoreExceptionTypes(typeof(ElementNotSelectableException));
                    GenerateWebDriverWait().IgnoreExceptionTypes(typeof(StaleElementReferenceException));
                    GenerateWebDriverWait().Until(d => ((IJavaScriptExecutor)Driver).ExecuteScript("return document.readyState").ToString() == "complete");
                   //GenerateWebDriverWait().Until(d => ((IJavaScriptExecutor)Driver).ExecuteScript("return document.readyState").Equals("complete"));
                    result = GenerateWebDriverWait().Until(d => elementLocator.Displayed);
                    enabled = GenerateWebDriverWait().Until(d => elementLocator.Enabled);  
                    IJavaScriptExecutor ex = Driver;
                    ex.ExecuteScript("arguments[0].scrollIntoView()", elementLocator);
                    Actions action = new Actions(Driver);
                    action.MoveToElement(elementLocator);
                    //new Actions(Driver).MoveToElement(elementLocator).Perform();
                    GenerateWebDriverWait().Until(d => ((IJavaScriptExecutor)Driver).ExecuteScript("return jQuery.active == 0"));                    
                    GenerateWebDriverWait().Until(ExpectedConditions.VisibilityOfAllElementsLocatedBy(
                    new ReadOnlyCollection<IWebElement>(new List<IWebElement>
                    {
                        elementLocator
                    })));
                }
                catch (Exception lastError)
                {                      
                    if (lastError != null)
                    {
                        string title = ScenarioContext.Current.ScenarioInfo.Title;
                        var stepType = ScenarioStepContext.Current.StepInfo.StepDefinitionType.ToString();
                        var stepInfo = ScenarioContext.Current.StepContext.StepInfo;
                        var stepDescription = stepInfo.StepDefinitionType + stepInfo.Text;

                        if (lastError is NoSuchElementException || lastError is TimeoutException)
                        {
                           
                            Debug.WriteLine("Element with locator: '" + stepDescription +  elementLocator +
                                          "' was not found in current context page.");
                        }
                        if(lastError is ElementNotVisibleException)
                        {
                            Debug.WriteLine("Element with locator: '" + stepDescription + elementLocator +
                                          "' was not visible in current context page.");
                        }                      
                    }
                    throw lastError;

                }
                if (result)
                    break;
                if (enabled)
                    break;
                Thread.Sleep(100);
            } while (attempts < 200);
            Thread.Sleep(100);
            return result&&enabled;
        }
        public void StaleElement(IWebElement element)
        {
            bool staleElement = true;
            while (staleElement)
            {
                try
                {
                    element.Click();
                    staleElement = false;

                }
                catch (StaleElementReferenceException e)
                {
                    staleElement = true;
                }
            }
        }

        public bool WaitForElementToClick(IWebElement elementLocator)
        {
           
            try
            {
                GenerateWebDriverWait().Until(ExpectedConditions.ElementToBeClickable(elementLocator));
                //Thread.Sleep(500);
            }
            catch(Exception)
            {
                Exception lastError = ScenarioContext.Current.TestError;
                if (lastError != null)
                {
                    if (lastError is ElementNotInteractableException)
                    {
                        string title = ScenarioContext.Current.ScenarioInfo.Title;
                        Debug.WriteLine("Element with locator: '" + title + elementLocator +
                                      "' was not clickable in current context page.");
                    }
                    
                }
                Debug.WriteLine("Element with locator: '" + elementLocator +
                                      "' was not clickable in current context page.");
            }
            //Thread.Sleep(100);
            return true;
        }


        public bool WaitForElementonSurveyor(IWebElement elementLocator)
        {
            bool result = false;
            bool enabled = false;
            int attempts = 0;

            do
            {
                attempts++;

                try
                {
                    
                    result = GenerateWebDriverWait().Until(d => elementLocator.Displayed);
                                      
                }
                catch (Exception)
                {

                    Exception lastError = ScenarioContext.Current.TestError;
                    if (lastError != null)
                    {
                        if (lastError is NoSuchElementException)
                        {
                            string title = ScenarioContext.Current.ScenarioInfo.Title;
                            Debug.WriteLine("Element with locator: '" + title + elementLocator +
                                          "' was not found in current context page.");
                        }
                        if (lastError is ElementNotVisibleException)
                        {
                            string title = ScenarioContext.Current.ScenarioInfo.Title;
                            Debug.WriteLine("Element with locator: '" + title + elementLocator +
                                          "' was not visible in current context page.");
                        }
                    }
                    Debug.WriteLine("Element with locator: '" + elementLocator +
                                          "' was not found in current context page.");

                }
                if (result)
                    break;
                if (enabled)
                    break;
                //Thread.Sleep(100);
            } while (attempts < 100);
            //Thread.Sleep(100);
            return result;
        }
        public bool WaitForElementOnExtranet(IWebElement elementLocator)
        {
            bool result = false;
            bool enabled = false;
            int attempts = 0;

            do
            {
                attempts++;

                try
                {

                    result = GenerateWebDriverWait().Until(d => elementLocator.Displayed);

                }
                catch (Exception)
                {

                    Exception lastError = ScenarioContext.Current.TestError;
                    if (lastError != null)
                    {
                        if (lastError is NoSuchElementException)
                        {
                            string title = ScenarioContext.Current.ScenarioInfo.Title;
                            Debug.WriteLine("Element with locator: '" + title + elementLocator +
                                          "' was not found in current context page.");
                        }
                        if (lastError is ElementNotVisibleException)
                        {
                            string title = ScenarioContext.Current.ScenarioInfo.Title;
                            Debug.WriteLine("Element with locator: '" + title + elementLocator +
                                          "' was not visible in current context page.");
                        }
                    }
                    Debug.WriteLine("Element with locator: '" + elementLocator +
                                          "' was not found in current context page.");

                }
                if (result)
                    break;
                if (enabled)
                    break;
                //Thread.Sleep(100);
            } while (attempts < 100);
            //Thread.Sleep(100);
            return result;
        }
        public void CloseSpinneronDiv()
        {
            var wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(180));
            var SpinnerText = By.XPath("//ai-dialog-container[@class='active']//div[@class='modal-content']//crisp-loader[@class='au-target']//parent::div//h4[@class='heading']");
            var Crispspinner = By.XPath("//ai-dialog-container[@class='active']//div[@class='modal-content']//crisp-loader[@class='au-target']//div[contains(@class,'crisp-spinner')]");
            var toast = By.XPath("//div[@id='toast-container']//div[@class='toast']");
            var element = By.XPath("//crisp-loader[@show.bind='isLoading'][@class='au-target']//div[contains(@class,'crisp-spinner')]");
            var pageLoading = By.XPath("//ai-dialog-container//div[@class='au-target modal loader']//div[@class='modal-content']//crisp-loader[@class='au-target']//parent::div//h4[@class='heading'][contains(text(),'Loading')]");
            var validatePlotData = By.XPath("//ai-dialog-container//div[@class='au-target modal loader']//div[@class='modal-content']//crisp-loader[@class='au-target']//parent::div//h4[@class='heading'][contains(text(),'Validating Excel Spreadsheet')]");
            var pageLoadingonCaluclateFees = By.XPath("//ai-dialog-container//div[@class='au-target modal loader']//div[@class='modal-content']//crisp-loader[@class='au-target']//parent::div//h4[@class='heading'][text()='Calculating Fees']");

            wait.Until(ExpectedConditions.InvisibilityOfElementLocated(SpinnerText));
            wait.Until(ExpectedConditions.InvisibilityOfElementLocated(Crispspinner));
            wait.Until(ExpectedConditions.InvisibilityOfElementLocated(element));
            wait.Until(ExpectedConditions.InvisibilityOfElementLocated(toast));
            wait.Until(ExpectedConditions.InvisibilityOfElementLocated(pageLoading));
            wait.Until(ExpectedConditions.InvisibilityOfElementLocated(validatePlotData));
            wait.Until(ExpectedConditions.InvisibilityOfElementLocated(pageLoadingonCaluclateFees));
            Thread.Sleep(100);
        }
        public static void CloseSpinneronRegPage()
        {          
            var wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(120));
            var element = By.XPath("//ai-dialog-overlay[@class='active']//following-sibling::ai-dialog-container[@class='active']");
            var pageLoading = By.XPath("//ai-dialog-container//div[@class='au-target modal loader']//div[@class='modal-content']//crisp-loader[@class='au-target']//parent::div//h4[@class='heading'][contains(text(),'Loading')]");
            var refreshingPage = By.XPath("//ai-dialog-container[2]//div[@class='au-target modal loader']//div[@class='modal-content']//crisp-loader[@class='au-target']//parent::div//h4[@class='heading'][contains(text(),'Refreshing Registration Details')]");
           wait.Until(ExpectedConditions.InvisibilityOfElementLocated(element));
            wait.Until(ExpectedConditions.InvisibilityOfElementLocated(pageLoading));
            wait.Until(ExpectedConditions.InvisibilityOfElementLocated(refreshingPage));
            Thread.Sleep(100);
        }

        public void CloseSpinneronPage()
        {
            try
            {
                var wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(180));
                var element = By.XPath("//crisp-loader[@class='au-target with-message message-position-right']/div[@class='content-container']//div[@class='center crisp-spinner']//parent::div//div[@class='loader-message au-target']");
                var SpinnerMessage = By.XPath("//div[@class='au-target modal loader']//div[@class='modal-content']//crisp-loader[@class='au-target']//parent::div//h4[@class='heading']//parent::div//parent::div[@class='au-target modal loader']");
                var toast = By.XPath("//div[@id='toast-container']//div[@class='toast']");
                wait.Until(ExpectedConditions.InvisibilityOfElementLocated(element));
                wait.Until(ExpectedConditions.InvisibilityOfElementLocated(SpinnerMessage));
                wait.Until(ExpectedConditions.InvisibilityOfElementLocated(toast));
                //Thread.Sleep(100);
            }
            catch(Exception)
            {

            }
        }
        public void CloseSpinnerOnSurveyorPage()
        {
            var element = By.XPath("//div[@class='page-loader-wrapper'][contains(@style,'block')]");
            var wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(180));
            wait.Until(ExpectedConditions.InvisibilityOfElementLocated(element));
            //Thread.Sleep(100);
        }
        public void CloseCrispCard()
        {
            var element = By.XPath("//ai-dialog-container[@class='active']//crisp-drawer//crisp-card[@class='au-target']");
            var wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(180));
            wait.Until(ExpectedConditions.InvisibilityOfElementLocated(element));

            //Thread.Sleep(500);
        }
        public void CloseChooseRolesCard()
        {
            var rolecard = By.XPath("//ai-dialog-container[@class='active'][2]");
            var wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(180));
            wait.Until(ExpectedConditions.InvisibilityOfElementLocated(rolecard));
        }


        public void CloseCrispDiv(int i)
        {                        
            var element = By.XPath($"//ai-dialog-container[{i}]//crisp-dialog//div[contains(@class,'au-target modal dialog')]");
            var wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(180));
            wait.Until(ExpectedConditions.InvisibilityOfElementLocated(element));
            //Thread.Sleep(100);
        }

        public void CloseSpinnerOnSurveyorDiv()
        {
            var element = By.XPath("//div[@class='dataTables_processing'][contains(@style,'block')]//div[@class='preloader']");
            var wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(180));
            wait.Until(ExpectedConditions.InvisibilityOfElementLocated(element));
            //Thread.Sleep(100);

        }
        public void CloseSpinnerOnExtranet()
        {
            var loader = By.XPath("//div[@class='page-loader-wrapper'][contains(@style,'opacity')]//div[@class='loader']//div[@class='preloader']");
            var pleaseWait = By.XPath("//div[@class='page-loader-wrapper'][contains(@style,'opacity')]//div[@class='loader']//div[@class='preloader']//following-sibling::p[contains(text(),'Please wait')]");
            var processing = By.XPath("//div[text()='Processing...'][@style='display: block;'][contains(text(),'Processing')]");
            var spinner = By.XPath("//div[@id='loadingContainer'][contains(@style,'block')]");           
            var wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(180));
            wait.Until(ExpectedConditions.InvisibilityOfElementLocated(loader));
            wait.Until(ExpectedConditions.InvisibilityOfElementLocated(pleaseWait));
            wait.Until(ExpectedConditions.InvisibilityOfElementLocated(processing));
            wait.Until(ExpectedConditions.InvisibilityOfElementLocated(spinner));
            Thread.Sleep(500);
        }
        
        
        public void WaitForElements(IList<IWebElement> elements)
        {
            CloseSpinneronPage();
            CloseSpinneronDiv();
            GenerateWebDriverWait().Until(d => elements.Count > 0);
            CloseSpinneronPage();
            CloseSpinneronDiv();
            CloseSpinnerOnExtranet();
            //Thread.Sleep(500);
        }
        public IWebElement GetElement(IWebDriver driver, By selector, int tries = 120)
        {
            for (int i = 1; i <= tries; i++)
            {
                try
                {
                    return driver.FindElement(selector);
                }
                catch (WebDriverException)
                {
                    Thread.Sleep(200);
                }
            }
            return null;
        }
        
        private WebDriverWait GenerateWebDriverWait(int seconds = 120)
        {
            return new WebDriverWait(Driver, TimeSpan.FromSeconds(seconds));
        }

        public void WaitForLoadElements(IList<IWebElement> elements)
        {
            IJavaScriptExecutor js = (IJavaScriptExecutor)Driver;
            int timeoutSec = 120;
            WebDriverWait wait = new WebDriverWait(Driver, new TimeSpan(0, 0, timeoutSec));
            wait.Until(wd => js.ExecuteScript("return document.readyState").ToString() == "complete");
            WaitForElements(elements);
        }
        public void WaitForLoadElement(IWebElement element)
        {
            IJavaScriptExecutor js = (IJavaScriptExecutor)Driver;
            int timeoutSec = 120;
            WebDriverWait wait = new WebDriverWait(Driver, new TimeSpan(0, 0, timeoutSec));
            wait.Until(wd => js.ExecuteScript("return document.readyState").ToString() == "complete");
            WaitForElement(element);
        }
        public void WaitForLoadElementtobeclickable(IWebElement element)
        {
            IJavaScriptExecutor js = (IJavaScriptExecutor)Driver;
            int timeoutSec = 120;
            WebDriverWait wait = new WebDriverWait(Driver, new TimeSpan(0, 0, timeoutSec));
            wait.Until(wd => js.ExecuteScript("return document.readyState").ToString() == "complete");
            WaitForElement(element);
            wait.Until(wd => ExpectedConditions.ElementToBeClickable(element));
        }
        public void ScrollIntoView(IWebElement element)
        {
            IJavaScriptExecutor js = (IJavaScriptExecutor)Driver;
            js.ExecuteScript("arguments[0].scrollIntoView();", element);
        }

        public void Scrollup()
        {
            IJavaScriptExecutor js = (IJavaScriptExecutor)Driver;
            js.ExecuteScript("scroll(0, -250);");
            //js.ExecuteScript("scrollTop: element.offset().top - $('#navbar-collapse').height(), 2000);");

        }

        public IWebElement RandomElement(IList<IWebElement> element)
        {
            Random rnd = new Random();
            int randomValue = rnd.Next(1, (element.Count) - 1);
            IWebElement OurElement = element[randomValue];
            Console.WriteLine(OurElement.Text + " is selected material");
            return OurElement;
        }
    }
}
