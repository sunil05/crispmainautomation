﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Remote;
using TechTalk.SpecFlow;
using OpenQA.Selenium.IE;
using log4net.Config;
using log4net;
using TechTalk.SpecFlow.Tracing;
using OpenQA.Selenium.Support.Extensions;
using System.Drawing.Imaging;
using System.IO;
using System.Text;
using System.Diagnostics;
using CrispAutomation.Features;
using AventStack.ExtentReports.Reporter;
using AventStack.ExtentReports;
using AventStack.ExtentReports.Gherkin.Model;
using System.Reflection;
using NUnit.Framework.Interfaces;

namespace CrispAutomation.Support
{
    [Binding]
    public class Hooks
    {
        public static RemoteWebDriver Driver = null;
        private IWebElement elementLocator;
        private static ExtentReports extent;
        private static ExtentTest featureName;
        private static ExtentTest scenario;
        private static ExtentTest test;
        // private static readonly ILog log = LogManager.GetLogger(typeof(Hooks));
        private static readonly log4net.ILog log
            = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        [BeforeScenario]
        public void Initialisedriver()
        {
            Driver = Driver ?? StartDriver(ConfigurationManager.AppSettings["Browser"]);
            ScenarioContext.Current.Set(Driver, "currentDriver");
            //  XmlConfigurator.Configure();
            log.Info($"Test Scenario Started For : {ScenarioContext.Current.ScenarioInfo.Title}");
            // scenario = extent.CreateTest<Scenario>(ScenarioContext.Current.ScenarioInfo.Title);
            scenario = featureName.CreateNode<Scenario>(ScenarioContext.Current.ScenarioInfo.Title);
        }
        [AfterScenario]
        public void AfterScenario()
        {
            try
            {
                Exception lastError = ScenarioContext.Current.TestError;

                if (lastError != null)
                {

                    if (lastError is NoSuchElementException)
                    {
                        Debug.WriteLine("Element with locator: '" + elementLocator +
                                      "' was not found in current context page.");
                    }
                }
                if (lastError != null)
                {
                    // CaptureScreenShot();
                    Screenshot ss = ((ITakesScreenshot)Driver).GetScreenshot();
                    string title = ScenarioContext.Current.ScenarioInfo.Title;
                    string Runname = title + DateTime.Now.ToString("yyyy-MM-dd-HH_mm_ss");
                    string sceenshotPath = new FileInfo(ExtensionMethods.GetAbsolutePath(@"TestData\ErrorScreenshot\")).ToString(); ;
                    //string sceenshotPath = @"\\tpgfile\NewCompany\IT\Department\QA Team\Current Projects\CRISP\CrispAutomation\Screenshots\";

                    string drive = Path.GetPathRoot(sceenshotPath);
                    if (!Directory.Exists(drive))
                    {
                        string localPath = @"\\tpgfile\NewCompany\IT\Department\QA Team\Current Projects\CRISP\CrispAutomation\Screenshots\";
                        ss.SaveAsFile(localPath + Runname + ".jpg", ScreenshotImageFormat.Jpeg);
                        //return localPath;
                    }
                    else
                    {
                        string screenshotfilename = sceenshotPath + Runname + ".jpg";
                        ss.SaveAsFile(screenshotfilename, ScreenshotImageFormat.Jpeg);
                        string urlfile = "http://storage/screenshots/" + Runname + ".jpg";
                        Console.WriteLine("" + urlfile);

                        //return screenshotfilename;
                        //Console.WriteLine(" ");
                    }
                }
            }
            catch
            {
                //Console.WriteLine("catch");   
            }
            finally
            {
                ScenarioContext.Current.Get<RemoteWebDriver>("currentDriver").Quit();
                Driver = null;
                ScenarioContext.Current.Clear();
                log.Info($"Test Scenario Started For : {ScenarioContext.Current.ScenarioInfo}");
                if (Driver != null)
                {
                    Driver?.Quit();
                    Driver?.Close();
                    Driver?.Dispose();
                    Driver = null;
                    extent.Flush();
                }
            }

        }

        [AfterTestRun]
        public static void StopSeleniumAfterAllTests()
        {
            Driver?.Close();
            Driver?.Quit();
            Driver?.Dispose();
            extent.Flush();
        }

        [BeforeTestRun]
        public static void IntialiseReport()
        {
            //var htmlReporter = new ExtentHtmlReporter(@"C:\Crisp\crisp.automation\CrispAutomation\Reports\ExtentReports.html");
            var fileInfo = new FileInfo(ExtensionMethods.GetAbsolutePath(@"TestData\ExtentReports\ExtentReports.html"));
            var htmlReporter = new ExtentHtmlReporter(fileInfo.ToString());
            htmlReporter.Configuration().Theme = AventStack.ExtentReports.Reporter.Configuration.Theme.Dark;
            extent = new ExtentReports();
            extent.AttachReporter(htmlReporter);
        }
        
        [BeforeFeature]
        public static void BeforeFeature()
        {
            featureName = extent.CreateTest<Feature>(FeatureContext.Current.FeatureInfo.Title);
        }
        [AfterStep]
        public static void InsertReportingSteps()
        {
            Exception lastError = ScenarioContext.Current.TestError;    
            var stepType = ScenarioStepContext.Current.StepInfo.StepDefinitionType.ToString();
            PropertyInfo pInfo = typeof(ScenarioContext).GetProperty("TestStatus", BindingFlags.Instance | BindingFlags.NonPublic);
            //MethodInfo getter = pInfo.GetGetMethod(nonPublic: true);
            //object TestResult = getter.Invoke(ScenarioContext.Current, null);

            if (lastError == null)
            {
             
                if (stepType == "Given") 
                scenario.CreateNode<Given>(ScenarioStepContext.Current.StepInfo.Text);
                if (stepType == "When") 
                scenario.CreateNode<When>(ScenarioStepContext.Current.StepInfo.Text);
                if (stepType == "And") 
                scenario.CreateNode<And>(ScenarioStepContext.Current.StepInfo.Text);
                if (stepType == "Then") 
                scenario.CreateNode<Then>(ScenarioStepContext.Current.StepInfo.Text);
                
            }
            else
            if (lastError != null)
            {
                var stacktrase = "<pre>" + lastError.StackTrace + "<pre>";
              //  string screenshotPath = CaptureScreenShot.Capture(Driver, "ScreenshotName");
                var stepInfo = ScenarioContext.Current.StepContext.StepInfo;
                var stepDescription = stepInfo.StepDefinitionType + stepInfo.Text;
                
                //if (stepType == "Given")
                //    scenario.CreateNode<Given>(ScenarioStepContext.Current.StepInfo.Text).Fail(ScenarioContext.Current.TestError.InnerException);
                //if (stepType == "When")
                //    scenario.CreateNode<When>(ScenarioStepContext.Current.StepInfo.Text).Fail(ScenarioContext.Current.TestError.InnerException);
                //if (stepType == "And")
                //    scenario.CreateNode<And>(ScenarioStepContext.Current.StepInfo.Text).Fail(ScenarioContext.Current.TestError.InnerException);
                //if (stepType == "Then")
                //    scenario.CreateNode<Then>(ScenarioStepContext.Current.StepInfo.Text).Fail(ScenarioContext.Current.TestError.InnerException);
                if (stepType == "Given")
                    scenario.CreateNode<Given>(ScenarioStepContext.Current.StepInfo.Text).Fail(stepDescription);
                if (stepType == "When")
                    scenario.CreateNode<When>(ScenarioStepContext.Current.StepInfo.Text).Fail(stepDescription);
                if (stepType == "And")
                    scenario.CreateNode<And>(ScenarioStepContext.Current.StepInfo.Text).Fail(stepDescription);
                if (stepType == "Then")
                    scenario.CreateNode<Then>(ScenarioStepContext.Current.StepInfo.Text).Fail(stepDescription);

              //  test.AddScreenCaptureFromPath((screenshotfilename));
                
                // CaptureScreenShot();
            }
            ////Pending Status
            //if(TestResult.ToString()=="StepDefinitionPending")
            //{
            //    if (stepType == "Given")
            //        scenario.CreateNode<Given>(ScenarioStepContext.Current.StepInfo.Text).Skip("Step Definition Pending");
            //    if (stepType == "When")
            //        scenario.CreateNode<When>(ScenarioStepContext.Current.StepInfo.Text).Skip("Step Definition Pending");
            //    if (stepType == "And")
            //        scenario.CreateNode<And>(ScenarioStepContext.Current.StepInfo.Text).Skip("Step Definition Pending");
            //    if (stepType == "Then")
            //        scenario.CreateNode<Then>(ScenarioStepContext.Current.StepInfo.Text).Skip("Step Definition Pending");
            //}      
        }
        //[AfterStep]
        //public void AfterStepOnError()
        //{
        //    Exception lastError = ScenarioContext.Current.TestError;
        //    if (lastError != null)
        //    {
        //        var stepInfo = ScenarioContext.Current.StepContext.StepInfo;
        //        var stepDescription = stepInfo.StepDefinitionType + stepInfo.Text;
        //        Debug.WriteLine(stepDescription);
        //    }
        //}
        private RemoteWebDriver StartDriver(string browser)
        {
            RemoteWebDriver driver = null; 

            switch (browser)
            {
                case "chrome":
                    //var fileInfo = new FileInfo(ExtensionMethods.GetAbsolutePath(@"TestData"));
                    var fileInfo = new FileInfo(ExtensionMethods.GetAbsolutePath(@"TestData\Downloads"));
                    DirectoryInfo downloadFolder = new DirectoryInfo($"{fileInfo}");                   
                    String DownloadfilePath = downloadFolder.ToString();
                    Statics.DownloadFolder = DownloadfilePath;
                    var chromeOptions = new ChromeOptions();
                    chromeOptions.AddUserProfilePreference("download.default_directory", DownloadfilePath);
                    chromeOptions.AddUserProfilePreference("intl.accept_languages", "nl");
                    chromeOptions.AddUserProfilePreference("disable-popup-blocking", "true");
                    driver = new ChromeDriver(chromeOptions);                   
                    driver.Navigate().GoToUrl(ConfigurationManager.AppSettings["CrispUrl"]);
                    driver.Manage().Window.Maximize();                 
                    
                    break;

                case "ie":
                    driver = new InternetExplorerDriver();
                    driver.Navigate().GoToUrl("http://crisp-dev-vip-a.mdisdev.local:65035/");
                    driver.Manage().Window.Maximize();
                   
                    break;

                default:
                    driver = new ChromeDriver();
                    driver.Navigate().GoToUrl(ConfigurationManager.AppSettings["CrispUrl"]);
                    driver.Manage().Window.Maximize();
                    break;

                    //case "chrome":
                    //    ChromeOptions electronOptions = new ChromeOptions();
                    //    electronOptions.BinaryLocation = @"C:\Users\KumarS\webdriverio-test\Crisp.Desktop\Crisp.Desktop.exe";
                    //    electronOptions.AddArgument("--env " + "C" + " --multi-instance");
                    //   // electronOptions.AddArgument(@"C:\Users\KumarS\webdriverio-test\Crisp.Desktop");
                    //    DesiredCapabilities capability = new DesiredCapabilities();
                    //    capability.SetCapability(CapabilityType.BrowserName, "Chrome");
                    //    capability.SetCapability("ChromeOptions", electronOptions); 
                    //    driver = new ChromeDriver(electronOptions);
                    //    break;

                    //case "ie":
                    //  //var test = "no need to implement";
                    //    break;

                    //default:
                    //    throw new Exception("browser not found!");

            }
            return driver;
        }      
    }
}
