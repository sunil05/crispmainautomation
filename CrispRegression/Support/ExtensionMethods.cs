﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Security.Policy;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using System.IO;
using CrispAutomation.Features;
using ExcelDataReader;
using iTextSharp.text.pdf;
using iTextSharp.text.pdf.parser;
using OfficeOpenXml;
using System.Collections.Generic;
using System.Reflection;

namespace CrispAutomation.Support
{
    public static class ExtensionMethods 
    {
        public static void EmptyFolder(DirectoryInfo directoryInfo)
        {
            foreach (FileInfo file in directoryInfo.GetFiles())
            {
                GC.Collect();
                GC.WaitForPendingFinalizers();
                file.IsReadOnly = false;
                file.Delete();
            }
            foreach (DirectoryInfo subfolder in directoryInfo.GetDirectories())
            {
                EmptyFolder(subfolder);
            }
        }

        public static void  MoveToActiveAndSendKeys(this IWebDriver dr, string keys, bool tab)
        {
            if (tab)
            {
               var el =  dr.SwitchTo().ActiveElement();
                el.SendKeys(keys);
                el.SendKeys(Keys.Tab);                  

            }
            else
            {
                dr.SwitchTo().ActiveElement().SendKeys(keys);
            }
              
        }

        public static void SendKeysAndtab(this IWebElement ele,string keys)
        {
            ele.SendKeys(keys);
            ele.SendKeys(Keys.Tab);
        }
       

        public static string GetAbsolutePath(string fileLocation)
        {
            var relativeDir = System.IO.Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);

            return System.IO.Path.Combine(relativeDir, fileLocation);
        }

        public static ExcelPackage CreateSpreadsheet(string orderRef)
        {
            if (Statics.Excel == null)
            {
                var excel = new ExcelPackage();
                var worksheet = excel.Workbook.Worksheets.Add("QuoteReferences");
                worksheet.Protection.IsProtected = true;
                Statics.Excel = excel;
            }
            
            //worksheet.Cells["A1"].Value = "Order Reference Number";
            Statics.CurrentExcelRow++;
            Statics.Excel.Workbook.Worksheets[1].Cells[$"A{Statics.CurrentExcelRow}"].Value = Statics.SiteRefNumber;
            Statics.Excel.Workbook.Worksheets[1].Cells[$"A{Statics.CurrentExcelRow}"].Value = orderRef;
           // Statics.Excel.SaveAs(new FileInfo(@"\\tpgfile\NewCompany\IT\Department\QA Team\Current Projects\CRISP\CrispAutomation\CrispData\OrderRef\OrderRef.xlsx"));
                       
            var fileInfo = new FileInfo(GetAbsolutePath(@"TestData\OrderRef\OrderRef.xlsx"));
            Statics.Excel.SaveAs(fileInfo);
            //C:\Crisp\crisp.automation\CrispAutomation\TestData\OrderRef\OrderRef.xlsx
            return Statics.Excel;
        }

        //Reading the Data from master plot schedule sheet using Excel Data Reader 
        public static void ReadExcelPlotData(string masterplotdata)
        {

            //string masterfilepath = @"C:\Users\KumarS\Desktop\Temp\NewHomes-Plot Schedule.xlsx";
            //open file and returns as Stream
            var result = new DataSet();
            using (var stream = File.Open(masterplotdata, FileMode.Open, FileAccess.Read))
            using (var excelReader = ExcelReaderFactory.CreateOpenXmlReader(stream)) //.xlsx
            {
                //Createopenxmlreader via ExcelReaderFactory

                //Set the First Row as Column Name

                //Return as DataSet
                result = excelReader.AsDataSet(new ExcelDataSetConfiguration()
                {
                    ConfigureDataTable = (_) => new ExcelDataTableConfiguration()
                    {
                        UseHeaderRow = true
                    }
                });
            }
            //Get all the Tables
            DataTableCollection table = result.Tables;
            //Store it in DataTable
            DataTable resultTable = table["Plots"];

            //Get Plot List Count

            var PlotList = resultTable.DefaultView
    .ToTable(true, "Plot Name")
    .Rows
    .Cast<DataRow>()
    .Select(row => row["Plot Name"])
    .ToList();
            Statics.Plots = PlotList.Select(i => i.ToString()).ToList();


            //Get The Warrenty Product List 

            var Productlist = resultTable.DefaultView
     .ToTable(true, "Warranty Product")
     .Rows
     .Cast<DataRow>()
     .Select(row => row["Warranty Product"])
     .ToList();
            Statics.ProductNameList = Productlist.Select(i => i.ToString()).ToList();

            //Get The Construction Type 
            var Construction = resultTable.DefaultView
   .ToTable(true, "New Build/Conversion")
   .Rows
   .Cast<DataRow>()
   .Select(row => row["New Build/Conversion"])
   .ToList();

            Statics.ConstructionType = Construction.Select(i => i.ToString()).ToList();

            //Get the BC Poducts

            var BCProductList = resultTable.DefaultView
 .ToTable(true, "Building Control Product")
 .Rows
 .Cast<DataRow>()
 .Select(row => row["Building Control Product"])
 .ToList();

            Statics.BCProducts = BCProductList.Select(i => i.ToString()).ToList();
        }
        public static class RunId
        {
            public static readonly string MainId = Guid.NewGuid().ToString();

            public static readonly string Id = MainId.TrimEnd();
        }

        public static string GetMainSolutionFolderPath(string currentFolderPath)
        {
            var folder = Directory.GetParent(currentFolderPath);

            var foundFile = folder.GetFiles().SingleOrDefault(x => x.Name == "CrispAutomation.sln");

            if (foundFile != null)
            {
                return foundFile.DirectoryName;
            }

            var folderPath = GetMainSolutionFolderPath(folder.ToString());
            return folderPath;
        }

        
    }
}
