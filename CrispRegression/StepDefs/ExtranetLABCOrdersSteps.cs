﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TechTalk.SpecFlow;

namespace CrispAutomation.ExtranetFeatures
{
    [Binding]
    public sealed class ExtranetOrdersSteps : Support.Pages
    {
        [Then(@"I should click and verfy orders from homepage")]
        public void ThenIShouldClickAndVerfyOrdersFromHomepage()
        {
            ExtranetOrdersPage.VerifyOrderRecordsFromReferenceCoulmn();
            ExtranetOrdersPage.VerifyOrderRecordsFromActionsCoulmn();
            ExtranetOrdersPage.VerifyOrderRecordsFromUrgentActionsCoulmn();
        }
        [Then(@"I should click and verfy orders from orderpage")]
        public void ThenIShouldClickAndVerfyOrdersFromOrderpage()
        {
            ExtranetOrdersPage.VerifyOrdersPage();
        }
        [Then(@"I should verfy orders count from homepage")]
        public void ThenIShouldVerfyOrdersCountFromHomepage()
        {
            ExtranetHomePage.VerifyOrdersCountOnHomePage();
        }

    }
}
