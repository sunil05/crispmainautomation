﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using TechTalk.SpecFlow;

namespace CrispAutomation.StepDefs
{
    [Binding]
    public class PGRegistrationSteps : Support.Pages
    {
        
        [Given(@"I have created company name as (.*)")]
        public void GivenIHaveCreatedCompanyNameAs(string companyname)
        {          
            AddCompanyPage.CreateCompany(companyname);
        }

        [When(@"I select PG registration tab")]
        public void WhenISelectPGRegistrationTab()
        {
            //Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(500);
            WaitForElement(PGRegistration.PGRegistrationTab);
            //Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(500);
            PGRegistration.PGRegistrationTab.Click();
            //Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(500);
            if(PGRegistration.CreateRegistrationButton.Count> 0)
            {
                WaitForElement(PGRegistration.CreateRegistrationButton[0]);
                PGRegistration.CreateRegistrationButton[0].Click();
                //Thread.Sleep(500);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                //Thread.Sleep(500);
            }
             
            WaitForElement(PGRegistration.RegistrationReference);
            var RegReference = PGRegistration.RegistrationReference.Text;
            var RegRefNumber = RegReference.Replace("Reference", "");
            //Thread.Sleep(1000);
        }

        [When(@"I verify the operations on overview section")]
        public void WhenIVerifyTheOperationsOnOverviewSection()
        {
            PGRegistration.OverviewSection();
        }

        [When(@"I verify the operations on reg documents section")]
        public void WhenIVerifyTheOperationsOnRegDocumentsSection()
        {
            PGRegistration.RegDocumentsSection();
        }

        [When(@"I verfiy the operations on conditions section")]
        public void WhenIVerfiyTheOperationsOnConditionsSection()
        {
            PGRegistration.ConditionSection();
        }

        [When(@"I verify the operations on terms section")]
        public void WhenIVerifyTheOperationsOnTermsSection()
        {
            PGRegistration.TermsSection();
        }

        [When(@"I verify the operations on roles section")]
        public void WhenIVerifyTheOperationsOnRolesSection()
        {
            PGRegistration.RolesSection();
          
        }

        [When(@"I verify the operations on account section")]
        public void WhenIVerifyTheOperationsOnAccountSection()
        {
            PGRegistration.AccountSection();
        }

        [When(@"I verify the operations on tasks section")]
        public void WhenIVerifyTheOperationsOnTasksSection()
        {
            PGRegistration.TaskSection();
        }

        [When(@"I verify the operations on correspondence section")]
        public void WhenIVerifyTheOperationsOnCorrespondenceSection()
        {
            PGRegistration.CorrespondenceSection();
        }

        [When(@"I verify the operations on files&documents section")]
        public void WhenIVerifyTheOperationsOnFilesDocumentsSection()
        {
            PGRegistration.FilesDocsSection();
        }

        [When(@"I verify the operations on notes section")]
        public void WhenIVerifyTheOperationsOnNotesSection()
        {
            PGRegistration.NotesSection();
        }

        [Then(@"I registaions details has been verified")]
        public void ThenIRegistaionsDetailsHasBeenVerified()
        {
            Console.WriteLine("Documents has been downloaded");
        }

        //Registration Doc Comparision on Person 
        [Given(@"I have created a person")]
        public void GivenIHaveCreatedAPerson()
        {
            AddPersonpage.AddPersonMainMethod();
        }
        //Registration Doc Comparision on Company 
        [Given(@"I have created a company")]
        public void GivenIHaveCreatedACompany()
        {
            AddCompanyPage.CreateCompany("TestRegDocs");
        }
        [When(@"I add a role on Roles page")]
        public void WhenIAddARoleOnRolesPage()
        {
            PGRegistration.CreateRole();
        }

        //Document Comparision 
        [When(@"I add all type of security documents")]
        public void WhenIAddAllTypeOfSecurityDocuments()
        {
            RegistrationDocs.AddSecurityDocsForVerification();         
        }

        [When(@"I verify site specific security document conditions")]
        public void WhenIVerifySiteSpecificSecurityDocumentConditions()
        {
            //RegistrationDocs.SiteSpecificRegDocs();
        }

        [Then(@"I should downloand the registration documents and compare it\.")]
        public void ThenIShouldDownloandTheRegistrationDocumentsAndCompareIt_()
        {
           // RegistrationDocs.DownloadDocsOnFileReview();
        }
    }
}
