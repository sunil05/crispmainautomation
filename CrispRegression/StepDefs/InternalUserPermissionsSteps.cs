﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TechTalk.SpecFlow;

namespace CrispAutomation.StepDefs
{
    [Binding]
    public class InternalUserPermissionsSteps : Support.Pages
    {
        [When(@"I remove all permissions on internal users operations")]
        public void WhenIRemoveAllPermissionsOnInternalUsersOperations()
        {
            InternalUserPermissionsPage.RemoveAllPermissionsToUser();
        }   

        [When(@"I apply permissions on creating internal users")]
        public void WhenIApplyPermissionsOnCreatingInternalUsers()
        {
            InternalUserPermissionsPage.ApplyCreatingInternalUserPermissions();
        }
        

        [Then(@"I verify that internal users can be created not amended")]
        public void ThenIVerifyThatInternalUsersCanBeCreatedNotAmended()
        {
            InternalUserPermissionsPage.VerifyCreatingInternaluserOperationWhenPermissionsApplied();
        }

        [When(@"I apply permissions on amending internal users")]
        public void WhenIApplyPermissionsOnAmendingInternalUsers()
        {
            InternalUserPermissionsPage.ApplyAmendingInternalUserPermissions();
        }

        [Then(@"I verify that internal users can be amended not created")]
        public void ThenIVerifyThatInternalUsersCanBeAmendedNotCreated()
        {
            InternalUserPermissionsPage.VerifyAmendingInternaluserOperationWhenPermissionsApplied();
        }

        [When(@"I apply permissions on view internal users")]
        public void WhenIApplyPermissionsOnViewInternalUsers()
        {
            InternalUserPermissionsPage.ApplyViewingInternalUserPermissions();
        }

        [Then(@"I verify that internal users can be search and viewed")]
        public void ThenIVerifyThatInternalUsersCanBeSearchAndViewed()
        {
           
        }

    }
}
