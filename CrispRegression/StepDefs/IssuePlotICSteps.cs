﻿using CrispAutomation.Features;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using TechTalk.SpecFlow;

namespace CrispAutomation.StepDefs
{
    [Binding]
    public class IssuePlotICSteps : Support.Pages
    {

        //Issue PlotIC

        [Then(@"I login back to crisp application to issue the plotIC")]
        public void ThenILoginBackToCrispApplicationToIssueThePlotIC()
        {
            if (Statics.ProductNameList.Any(o => o.Equals("New Homes")) || Statics.ProductNameList.Any(o => o.Equals("New Homes - High Value")))
            {
                IssueCertificatePage.IssuePlotIC();
            }
            //Thread.Sleep(1500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            WaitForElement(Dashboardpage.QuoteApplication);
            Assert.IsTrue(Dashboardpage.QuoteApplication.Displayed);
        }
        //Comparing PlotIC Document
        [Then(@"I should download plotIC and compare the document")]
        public void ThenIShouldDownloadPlotICAndCompareTheDocument()
        {

            if (Statics.ProductNameList.Any(o => o.Equals("New Homes")) || Statics.ProductNameList.Any(o => o.Equals("New Homes - High Value")))
            {
                DocumentComparisionPage.PlotICDocComparision();
            }          

        }
        
    }
}
