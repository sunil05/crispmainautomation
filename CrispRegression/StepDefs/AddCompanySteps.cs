﻿using System.Linq;
using System.Threading;
using NUnit.Framework;
using TechTalk.SpecFlow;
using CrispAutomation.Pages;
using System;

namespace CrispAutomation.StepDefs
{
    [Binding]
    public class AddCompanySteps : Support.Pages
    {
        //Adding CompanY Details 
        [When(@"I click on add button to add company")]
        public void WhenIClickOnAddButtonToAddCompany()
        {
            Dashboardpage.PlusButton.Click();
        }
        [When(@"select Company Option")]
        public void WhenSelectCompanyOption()
        {
            Dashboardpage.CompanyButton.Click();
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(1000);
            WaitForElement(AddCompanyPage.ContactDiv);
            Assert.IsTrue(AddCompanyPage.ContactDiv.Displayed);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(1000);

        }
        [When(@"I enter a '(.*)' to create a company on search existing company page")]
        public void WhenIEnterAToCreateACompanyOnSearchExistingCompanyPage(string companyname)
        {
            WaitForElement(AddCompanyPage.CompanyNameToCreate);
            AddCompanyPage.EnterCompanyName.SendKeys(companyname);
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
        }
        [When(@"I click on No Existing Match Found button")]
        public void WhenIClickOnNoExistingMatchFoundButton()
        {
            WaitForElement(AddCompanyPage.NoExistingMatchFound);
            WaitForElementToClick(AddCompanyPage.NoExistingMatchFound);
            AddCompanyPage.NoExistingMatchFound.Click();
            //Thread.Sleep(500);
        }

        [When(@"I click on No Match Found on companies house page")]
        public void WhenIClickOnNoMatchFoundOnCompaniesHousePage()
        {
            WaitForElement(AddCompanyPage.NoMatchFound);
            WaitForElementToClick(AddCompanyPage.NoMatchFound);
            AddCompanyPage.NoMatchFound.Click();
            //Thread.Sleep(500);
        }
        [When(@"I enter company details and social media details manually")]
        public void WhenIEnterCompanyDetailsAndSocialMediaDetailsManually()
        {
            WaitForElement(AddCompanyPage.CompanyName);
            AddCompanyPage.FillCompanyDetails();
        }  

        [When(@"I add the office information on Offices Page")]
        public void WhenIAddTheOfficeInformationOnOfficesPage()
        {
            WaitForElement(AddCompanyPage.AddOfficeButton);
            AddCompanyPage.AddOfficeButton.Click();
            WaitForElement(AddCompanyPage.OfficeNameInput);
            AddCompanyPage.AddOfficeDetails();
        }
        [When(@"I add employee on employees page")]
        public void WhenIAddEmployeeOnEmployeesPage()
        {
            WaitForElement(AddCompanyPage.AddEmployeeButton);
            AddCompanyPage.AddEmployeeButton.Click();
            //Thread.Sleep(1000);
            WaitForElement(AddCompanyPage.EmpTitleLabel);
            AddCompanyPage.AddEmpDetails();

        }
        [When(@"I click on Save button to save above created company")]
        public void WhenIClickOnSaveButtonToSaveAboveCreatedCompany()
        {
            WaitForElement(AddCompanyPage.SaveButton);
            AddCompanyPage.SaveButton.Click();
            //Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(500);
        }
        [Then(@"I should see company details on the dashborad")]
        public void ThenIShouldSeeCompanyDetailsOnTheDashborad()
        {
            WaitForElement(Dashboardpage.CompanyImage);
            Assert.True(Dashboardpage.CompanyImage.Displayed);
        }

        // Adding Company From Crisp Database
        [When(@"I select on existing company from the crisp database list")]
        public void WhenISelectOnExistingCompanyFromTheCrispDatabaseList()
        {
            WaitForElement(AddCompanyPage.ExistingCompaniesFromCrispDB);
            AddCompanyPage.AddExistingCompanyFromCrispDatabase();
        }

        // Adding Company From Companies House
        [When(@"I select on existing company from the companieshouse database list")]
        public void WhenISelectOnExistingCompanyFromTheCompanieshouseDatabaseList()
        {          
            AddCompanyPage.AddExistingCompanyFromCompaniesHouse();

        }
        // Edit company details Scenario Steps 
        [When(@"I select edit button to edit company details")]
        public void WhenISelectEditButtonToEditCompanyDetails()
        {            
            WaitForElement(EditCompanyDetailsPage.EditCompany);
            EditCompanyDetailsPage.EditCompany.Click();
            WaitForElement(AddCompanyPage.ContactDiv);
            Assert.IsTrue(AddCompanyPage.ContactDiv.Displayed);
        }

        [When(@"I edit company details on details page '(.*)'")]
        public void WhenIEditCompanyDetailsOnDetailsPage(string p0)
        {
            WaitForElement(EditCompanyDetailsPage.CompanyNameInput);
            EditCompanyDetailsPage.FillCompanyDetails();
        }      

        [When(@"I edit existing office details and add new office details")]
        public void WhenIEditExistingOfficeDetailsAndAddNewOfficeDetails()
        {
            WaitForElement(EditCompanyDetailsPage.EditOfficeAddress);
            EditCompanyDetailsPage.EditOfficeAddress.Click();
            WaitForElement(EditCompanyDetailsPage.EditOfficeAddressButton);
            EditCompanyDetailsPage.EditOfficeAddressButton.Click();
            WaitForElement(EditCompanyDetailsPage.OfficeNameInput);
            EditCompanyDetailsPage.EditOfficeDetails();
            WaitForElement(EditCompanyDetailsPage.AddOfficeButton);
            EditCompanyDetailsPage.AddOfficeButton.Click();
            WaitForElement(AddCompanyPage.OfficeName);
            EditCompanyDetailsPage.AddNewOfficeDetails();

        }
        [When(@"I click on Save button to update the company details")]
        public void WhenIClickOnSaveButtonToUpdateTheCompanyDetails()
        {
            WaitForElement(EditCompanyDetailsPage.SaveButton);
            EditCompanyDetailsPage.SaveButton.Click();
            //Thread.Sleep(1000);
        }
        [Then(@"I should see updated company details on the dashboard")]
        public void ThenIShouldSeeUpdatedCompanyDetailsOnTheDashboard()
        {
            WaitForElement(Dashboardpage.CompanyImage);
            Assert.True(Dashboardpage.CompanyImage.Displayed);
            //Thread.Sleep(500);
        }
        //Edit Employee details  and Add new Employee
        [When(@"I select employees option")]
        public void WhenISelectEmployeesOption()
        {
            WaitForElement(EditCompanyDetailsPage.EmployeesOption);
            EditCompanyDetailsPage.EmployeesOption.Click();
            //Thread.Sleep(500);
        }
        [When(@"I edit existing employee details and add new employee details")]
        public void WhenIEditExistingEmployeeDetailsAndAddNewEmployeeDetails()
        {
            WaitForElement(EditCompanyDetailsPage.EditEmployees);
            EditCompanyDetailsPage.EditEmpDetails();
            WaitForElement(EditCompanyDetailsPage.AddEmployeeButton);            
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            WaitForElementToClick(EditCompanyDetailsPage.AddEmployeeButton);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            EditCompanyDetailsPage.AddEmployeeButton.Click();
            WaitForElement(EditCompanyDetailsPage.Title);
            EditCompanyDetailsPage.NewEmpDetails();
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
        }

        [Then(@"I should see new employee details on the company dashboard")]
        public void ThenIShouldSeeNewEmployeeDetailsOnTheCompanyDashboard()
        {
            Console.WriteLine("Employees added");
        }


        // Add Rating to the company

        [When(@"I click on rating option")]
        public void WhenIClickOnRatingOption()
        {
            WaitForElement(AddRatingToCompanyPage.RatingOption);
            AddRatingToCompanyPage.RatingOption.Click();
            //Thread.Sleep(500);
        }

        [When(@"I select add rating button on companypage from top right corner")]
        public void WhenISelectAddRatingButtonOnCompanypageFromTopRightCorner()
        {
            WaitForElement(AddRatingToCompanyPage.AddRatingButton);
            AddRatingToCompanyPage.AddRatingButton.Click();
            WaitForElement(AddCompanyPage.ContactDiv);
            Assert.IsTrue(AddCompanyPage.ContactDiv.Displayed);
            //Thread.Sleep(500);
        }

        [When(@"I provide rating details on details page")]
        public void WhenIProvideRatingDetailsOnDetailsPage()
        {
            WaitForElement(AddRatingToCompanyPage.DetailsOption);
            AddRatingToCompanyPage.DetailsPage();
        }

        [When(@"I provide details on warranty providers page")]
        public void WhenIProvideDetailsOnWarrantyProvidersPage()
        {
            WaitForElement(AddRatingToCompanyPage.WarrentyProvider);
            AddRatingToCompanyPage.WarrantyDetails();
        }

        [When(@"I provide details on directors page")]
        public void WhenIProvideDetailsOnDirectorsPage()
        {
            WaitForElement(AddRatingToCompanyPage.DirectorsOption);
            AddRatingToCompanyPage.DirectorDetails();
        }

        [When(@"I provide details on developments page")]
        public void WhenIProvideDetailsOnDevelopmentsPage()
        {
            WaitForElement(AddRatingToCompanyPage.Developments);
            AddRatingToCompanyPage.DevelopmentDetails();
        }
        [When(@"I provide details on prospective business page")]
        public void WhenIProvideDetailsOnProspectiveBusinessPage()
        {
            WaitForElement(AddRatingToCompanyPage.ProspectiveBusiness);
            AddRatingToCompanyPage.ProspectiveBusinessDetails();
        }
        [When(@"I provide details on claims history page")]
        public void WhenIProvideDetailsOnClaimsHistoryPage()
        {
            WaitForElement(AddRatingToCompanyPage.ClaimHistory);
            AddRatingToCompanyPage.ClaimsHistoryDetails();
        }
        [When(@"I provide details on group page")]
        public void WhenIProvideDetailsOnGroupPage()
        {
            WaitForElement(AddRatingToCompanyPage.GroupOption);
            AddRatingToCompanyPage.GroupDetails();
        }

        [When(@"I provide details on scores details page")]
        public void WhenIProvideDetailsOnScoresDetailsPage()
        {
            WaitForElement(AddRatingToCompanyPage.ScoresOption);
            AddRatingToCompanyPage.ScoresDetails();
        }

        [When(@"I click on confirm rating button on the wizard")]
        public void WhenIClickOnConfirmRatingButtonOnTheWizard()
        {
            WaitForElement(AddRatingToCompanyPage.ConfirmRatingButton);
            //Thread.Sleep(500);
            AddRatingToCompanyPage.ConfirmRatingButton.Click();
            //Thread.Sleep(1000);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            CloseCrispDiv(1);
            //Thread.Sleep(1500);        
            // Driver.Navigate().Refresh();
            WaitForElement(AddRatingToCompanyPage.RatingOption);
            WaitForElementToClick(AddRatingToCompanyPage.RatingOption);
            AddRatingToCompanyPage.RatingOption.Click();
        }

        [Then(@"I should see the rating details for company")]
        public void ThenIShouldSeeTheRatingDetailsForCompany()
        {
            WaitForElement(AddRatingToCompanyPage.RatingSummaryDisplayed);
            Assert.IsTrue(AddRatingToCompanyPage.RatingSummaryDisplayed.Displayed);
        }


        //Edit Rating To the company 
        [When(@"I click on rating option to edit rating")]
        public void WhenIClickOnRatingOptionToEditRating()
        {
            WaitForElement(EditRatingToCompanyPage.RatingOption);
            EditRatingToCompanyPage.RatingOption.Click();
            //Thread.Sleep(1000);
        }
        
        [When(@"I select edit rating button")]
        public void WhenISelectEditRatingButton()
        {
            WaitForElement(EditRatingToCompanyPage.EditRating);
            //Thread.Sleep(1000);
            EditRatingToCompanyPage.EditRating.Click();
            WaitForElement(AddCompanyPage.ContactDiv);
            Assert.IsTrue(AddCompanyPage.ContactDiv.Displayed);
            //Thread.Sleep(1000);
        }

        [When(@"I edit rating details on details page")]
        public void WhenIEditRatingDetailsOnDetailsPage()
        {
            WaitForElement(EditRatingToCompanyPage.DetailsOption);
            EditRatingToCompanyPage.EditDetailsPage();
        }

        [When(@"I edit details on warranty providers page")]
        public void WhenIEditDetailsOnWarrantyProvidersPage()
        {
            WaitForElement(EditRatingToCompanyPage.WarrentyProvider);
            EditRatingToCompanyPage.WarrantyDetails("2018");
        }
        [When(@"I edit details on directors page")]
        public void WhenIEditDetailsOnDirectorsPage()
        {
            WaitForElement(EditRatingToCompanyPage.DirectorsOption);
            EditRatingToCompanyPage.EditDirectorDetails();
        }
        [When(@"I edit details on developments page")]
        public void WhenIEditDetailsOnDevelopmentsPage()
        {
            WaitForElement(EditRatingToCompanyPage.Developments);
            EditRatingToCompanyPage.EditDevelopmentDetails();
            WaitForElement(EditRatingToCompanyPage.Developments);
            EditRatingToCompanyPage.NewDevelopmentDetails();
        }
        [When(@"I edit details on prospective business page")]
        public void WhenIEditDetailsOnProspectiveBusinessPage()
        {
            WaitForElement(EditRatingToCompanyPage.ProspectiveBusiness);
            EditRatingToCompanyPage.EditProspectiveBusinessDetails();
            WaitForElement(EditRatingToCompanyPage.ProspectiveBusiness);
            EditRatingToCompanyPage.NewProspectiveBusinessDetails();
        }
        [When(@"I edit details on claims history page")]
        public void WhenIEditDetailsOnClaimsHistoryPage()
        {          

            WaitForElement(AddRatingToCompanyPage.ClaimHistory);
            EditRatingToCompanyPage.EditClaimsHistoryDetails();
            WaitForElement(EditRatingToCompanyPage.ClaimHistory);
            EditRatingToCompanyPage.NewClaimsHistoryDetails();
        }
        [When(@"I edit details on group page")]
        public void WhenIEditDetailsOnGroupPage()
        {
            WaitForElement(EditRatingToCompanyPage.NextButton);
            EditRatingToCompanyPage.NextButton.Click();
        }
        [When(@"I edit details on scores details page")]
        public void WhenIEditDetailsOnScoresDetailsPage()
        {
            WaitForElement(EditRatingToCompanyPage.ScoresOption);
            EditRatingToCompanyPage.EditScoresDetails();
        }


        [When(@"I click on confirm rating button to update amended rating details")]
        public void WhenIClickOnConfirmRatingButtonToUpdateAmendedRatingDetails()
        {
            WaitForElement(EditRatingToCompanyPage.ConfirmRatingButton);
            EditRatingToCompanyPage.ConfirmRatingButton.Click();
            //Thread.Sleep(1000);
        }

        [Then(@"I should see updated rating details for the company")]
        public void ThenIShouldSeeUpdatedRatingDetailsForTheCompany()
        {
            CloseSpinneronDiv();
            CloseSpinneronPage();
            WaitForElement(EditRatingToCompanyPage.RatingSummaryDisplayed);
            Assert.IsTrue(EditRatingToCompanyPage.RatingSummaryDisplayed.Displayed);
            //Thread.Sleep(1000);
        }    
       
    }
}

