﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using NUnit.Framework;
using TechTalk.SpecFlow;
using OpenQA.Selenium;

namespace CrispAutomation.StepDefs
{
    [Binding]
    public class DocumentVerificationSteps :Support.Pages

    {
        //Comparing Quote Document
        [Then(@"I should download the quote document and compare it")]
        public void ThenIShouldDownloadTheQuoteDocumentAndCompareIt()
        {
            DocumentComparisionPage.QuoteDocComparision();
          
        }

       //Comparing Intial Notice Document
        [Then(@"I should download the intialnotice document and compare it")]
        public void ThenIShouldDownloadTheIntialnoticeDocumentAndCompareIt()
        {
            DocumentComparisionPage.IntialNoticeDocComparision();
        }
        //Comparing Final Notice Document
        [Then(@"I should download the final notice document and compare it")]
        public void ThenIShouldDownloadTheFinalNoticeDocumentAndCompareIt()
        {
            DocumentComparisionPage.FinalNoticeDocComparision();
        }
        //Comparing Partial Final Notice Document
        [Then(@"I should download the partial final notice document and compare it")]
        public void ThenIShouldDownloadThePartialFinalNoticeDocumentAndCompareIt()
        {
            DocumentComparisionPage.PartialFinalNoticeDocComparision();
        }

    }
}
