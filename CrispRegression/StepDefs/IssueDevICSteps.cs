﻿using CrispAutomation.Features;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using TechTalk.SpecFlow;

namespace CrispAutomation.StepDefs
{
    [Binding]
    public class IssueDevICSteps : Support.Pages
    {
        // Issue Dev IC 

        [Then(@"I login back to crisp application to issue the DevIC")]
        public void ThenILoginBackToCrispApplicationToIssueTheDevIC()
        {
            if (Statics.ProductNameList.Any(o => o.Equals("Social Housing")) || Statics.ProductNameList.Any(o => o.Equals("Social Housing - High Value")) || Statics.ProductNameList.Any(o => o.Equals("Private Rental")) || Statics.ProductNameList.Any(o => o.Equals("Private Rental - High Value")))
            {
                IssueCertificatePage.IssueDevIC();
            }
            //Thread.Sleep(1500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            WaitForElement(Dashboardpage.QuoteApplication);
            Assert.IsTrue(Dashboardpage.QuoteApplication.Displayed);

        }

        //Comparing DevIC Document
        [Then(@"I should download DevIC and compare the document")]
        public void ThenIShouldDownloadDevICAndCompareTheDocument()
        {
            if (Statics.ProductNameList.Any(o => o.Equals("Social Housing")) || Statics.ProductNameList.Any(o => o.Equals("Social Housing - High Value")) || Statics.ProductNameList.Any(o => o.Equals("Private Rental")) || Statics.ProductNameList.Any(o => o.Equals("Private Rental - High Value")))
            {
                DocumentComparisionPage.DevICDocComparision();
            }
        }
    }
}
