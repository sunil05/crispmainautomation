﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TechTalk.SpecFlow;

namespace CrispAutomation.StepDefs
{
    [Binding]
    public class CreatePGOrderSteps : Support.Pages
    {
        [When(@"I provide details on key site details page for PG Brand")]
        public void WhenIProvideDetailsOnKeySiteDetailsPageForPGBrand()
        {
            AddPGQuotePage.PGKeySiteDetailsPage();
        }
        [When(@"I select relavant PG roles on roles page")]
        public void WhenISelectRelavantPGRolesOnRolesPage()
        {
            AddLABCQuotePage.RolesDetails();
        }
        [When(@"I provide migrated company details on key site details page for PG Brand")]
        public void WhenIProvideMigratedCompanyDetailsOnKeySiteDetailsPageForPGBrand()
        {
            AddPGQuotePage.PGKeySiteDetailsWithMigratedData();
        }

        [When(@"I select roles with migrated company on roles page for PG Brand")]
        public void WhenISelectRolesWithMigratedCompanyOnRolesPageForPGBrand()
        {
            AddLABCQuotePage.RolesDetailsWithMigratedData();
        }      

    }
}
