﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TechTalk.SpecFlow;

namespace CrispAutomation.StepDefs
{
    [Binding]
    public sealed class ExtranetFeeDetailsSteps : Support.Pages
    {
        [When(@"I store fee details from crisp")]
        public void WhenIStoreFeeDetailsFromCrisp()
        {
            ExtranetFeePage.GetFeeDetailsFromCrisp();
        }

        [Then(@"I should verify fee details on extranet")]
        public void ThenIShouldVerifyFeeDetailsOnExtranet()
        {
            ExtranetFeePage.GetandVerifyFeeDetailsOnExtranet();   
        }
    }
}
