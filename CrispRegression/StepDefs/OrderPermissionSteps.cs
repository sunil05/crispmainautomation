﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TechTalk.SpecFlow;

namespace CrispAutomation.StepDefs
{
    [Binding]
    public sealed class OrderPermissionSteps : Support.Pages
    {
        [When(@"I remove all permissions on order operations")]
        public void WhenIRemoveAllPermissionsOnOrderOperations()
        {
            OrderPermissionsPage.RemoveAllPermissionsToUser();
        }

        [Then(@"I verify that user can not perform order operations")]
        public void ThenIVerifyThatUserCanNotPerformOrderOperations()
        {
            OrderPermissionsPage.VerifyOrderOperationWhenPermissionsRemoved();
        }

        [When(@"I apply permissions on creating order")]
        public void WhenIApplyPermissionsOnCreatingOrder()
        {
            OrderPermissionsPage.ApplyCreatingOrderPermissions();
        }

        [Then(@"I verify that order can be created not amended")]
        public void ThenIVerifyThatOrderCanBeCreatedNotAmended()
        {
            OrderPermissionsPage.VerifyCreatingOrderWhenPermissionsApplied();
        }

        [When(@"I apply permissions on amending order")]
        public void WhenIApplyPermissionsOnAmendingOrder()
        {
            OrderPermissionsPage.ApplyAmendingOrderPermissions();
        }

        [Then(@"I verify that order can be amended not created")]
        public void ThenIVerifyThatOrderCanBeAmendedNotCreated()
        {
            OrderPermissionsPage.VerifyAmendingOrderWhenPermissionsApplied();
        }

        [When(@"I apply permissions on view order")]
        public void WhenIApplyPermissionsOnViewOrder()
        {
            OrderPermissionsPage.ApplyViewingOrderPermissions();
        }

        [Then(@"I verify that users can view order")]
        public void ThenIVerifyThatUsersCanViewOrder()
        {
            OrderPermissionsPage.VerifyViewingOrderOperationWhenPermissionsApplied();
        }

    }
}
