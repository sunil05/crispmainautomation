﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TechTalk.SpecFlow;

namespace CrispAutomation.StepDefs
{
    [Binding]
    public sealed class QuotePermissionSteps : Support.Pages
    {
        [When(@"I remove all permissions on quote operations")]
        public void WhenIRemoveAllPermissionsOnQuoteOperations()
        {
            QuotePermissionsPage.RemoveAllPermissionsToUser();
        }
        [Then(@"I verify that user can not perform quote operations")]
        public void ThenIVerifyThatUserCanNotPerformQuoteOperations()
        {
            QuotePermissionsPage.VerifyQuoteOperationWhenPermissionsRemoved();
        }
        [When(@"I apply permissions on creating quote")]
        public void WhenIApplyPermissionsOnCreatingQuote()
        {
            QuotePermissionsPage.ApplyCreatingQuotePermissions();
        }
        [Then(@"I verify that quote can be created not amended")]
        public void ThenIVerifyThatQuoteCanBeCreatedNotAmended()
        {
            QuotePermissionsPage.VerifyCreatingQuoteWhenPermissionsApplied();
        }
        [When(@"I apply permissions on amending quote")]
        public void WhenIApplyPermissionsOnAmendingQuote()
        {
            QuotePermissionsPage.ApplyAmendingQuotePermissions();
        }
        [Then(@"I verify that quote can be amended not created")]
        public void ThenIVerifyThatQuoteCanBeAmendedNotCreated()
        {
            QuotePermissionsPage.VerifyAmendingQuoteWhenPermissionsApplied();
        }
        [When(@"I apply permissions on view quote")]
        public void WhenIApplyPermissionsOnViewQuote()
        {
            QuotePermissionsPage.ApplyViewingQuotePermissions();
        }
        [Then(@"I verify that users can view quote")]
        public void ThenIVerifyThatUsersCanViewQuote()
        {
            QuotePermissionsPage.VerifyViewingQuoteOperationWhenPermissionsApplied();
            QuotePermissionsPage.ApplySendQuotePermissions();
            QuotePermissionsPage.VerifySendQuoteOperationWhenPermissionsApplied();
        }      
    }
}
