﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TechTalk.SpecFlow;

namespace CrispAutomation.StepDefs
{
    [Binding]
    public class ExtranetCompanySteps : Support.Pages
    {
        [Then(@"I should verify the office details on company page")]
        public void ThenIShouldVerifyTheOfficeDetailsOnCompanyPage()
        {
            ExtranetCompanyPage.VerifCompanySection();
        }
        [Then(@"I should add new office from lookup address on company page")]
        public void ThenIShouldAddNewOfficeFromLookupAddressOnCompanyPage()
        {
            ExtranetCompanyPage.AddNewOfficeDetailsFromLookup();
        }

        [Then(@"I should add new office manually on company page")]
        public void ThenIShouldAddNewOfficeManuallyOnCompanyPage()
        {
            ExtranetCompanyPage.AddNewOfficeDetailsManually();
        }

       
        [Then(@"I should edit office from lookup address on company page")]
        public void ThenIShouldEditOfficeFromLookupAddressOnCompanyPage()
        {
            ExtranetCompanyPage.EditOfficeDetailsFromLookup();
        }

        [Then(@"I should edit office manually on company page")]
        public void ThenIShouldEditOfficeManuallyOnCompanyPage()
        {
            ExtranetCompanyPage.EditOfficeDetailsManually();
        }
        [Then(@"I should verify the employee details on company page")]
        public void ThenIShouldVerifyTheEmployeeDetailsOnCompanyPage()
        {
            ExtranetCompanyPage.VerifyEmployeesSection();
        }
        [Then(@"I should add new employee on company page")]
        public void ThenIShouldAddNewEmployeeOnCompanyPage()
        {
            ExtranetCompanyPage.AddNewEmployeesection();
        }

        [Then(@"I should edit employee on company page")]
        public void ThenIShouldEditEmployeeOnCompanyPage()
        {
            ExtranetCompanyPage.EditEmployeesection();
        }

    }
}
