﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CrispAutomation.Pages;
using NUnit.Framework;
using TechTalk.SpecFlow;
using System.Threading;
using OpenQA.Selenium;

namespace CrispAutomation.StepDefs
{
    [Binding]
    public class IntialNoticeSteps : Support.Pages
    {
        //Sending Intial Notice Steps 
        [When(@"I select files and documents page")]
        public void WhenISelectFilesAndDocumentsPage()
        {
            WaitForElement(SendIntialNotice.FilesandDocsTab);
            SendIntialNotice.FilesandDocsTab.Click();
        }

        [When(@"I upload sitelocation plan file type and surface storm and foul drainage layout  file type documents")]
        public void WhenIUploadSitelocationPlanFileTypeAndSurfaceStormAndFoulDrainageLayoutFileTypeDocuments()
        {
            //Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(500);
            WaitForElement(SendIntialNotice.UploadButton);
            SendIntialNotice.UploadButton.Click();
            //Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(500);
            WaitForElement(SendIntialNotice.FileButton);           
            SendIntialNotice.FilesandDocumentsDetails();
            //Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(500);
            log.Info($"Required Document are uploaded Successfully to sent Intial Notice");
        }
      
        [When(@"I click on send intial notice button on newhomes product")]
        public void WhenIClickOnSendIntialNoticeButtonOnNewhomesProduct()
        {
            log.Info("Sending Intial Notice");
            //Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(500);
            if (SendIntialNotice.ReloadButton.Count > 0)
            {
                WaitForElements(SendIntialNotice.ReloadButton);
                SendIntialNotice.ReloadButton[0].Click();
            }
            else
            {
                Driver.Navigate().Refresh();
            }
            SendIntialNotice.SelectIntialNotice();
            //Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            WaitForElement(SendIntialNotice.SendIntialNoticeDialogueWizard[0]);
            Assert.IsTrue(SendIntialNotice.SendIntialNoticeDialogueWizard[0].Displayed);
            log.Info($"Sending Intial Notice");
        }

        [When(@"I click on send intial notice button")]
        public void WhenIClickOnSendIntialNoticeButton()
        {
            log.Info("Sending Intial Notice");
            //Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(500);
            if (SendIntialNotice.ReloadButton.Count > 0)
            {
                WaitForElements(SendIntialNotice.ReloadButton);
                SendIntialNotice.ReloadButton[0].Click();
            }
            else
            {
                Driver.Navigate().Refresh();
            }
            //Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(500);
            WaitForElement(Dashboardpage.ActionsButton);
            //Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(500);
            Dashboardpage.ActionsButton.Click();
            //Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(500);
            WaitForElement(SendIntialNotice.SendInitialNoticeButton);
            //Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(500);
            SendIntialNotice.SendInitialNoticeButton.Click();
            //Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(500);
            if(SendIntialNotice.SendIntialNoticeDialogueWizard.Count>0)
            {
                WaitForElement(SendIntialNotice.SendIntialNoticeDialogueWizard[0]);
                Assert.IsTrue(SendIntialNotice.SendIntialNoticeDialogueWizard[0].Displayed);
                log.Info($"Sending Intial Notice");
            }else
            {
                Driver.Navigate().Refresh();
                //Thread.Sleep(500);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                //Thread.Sleep(500);
                WaitForElement(Dashboardpage.ActionsButton);
                //Thread.Sleep(500);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                //Thread.Sleep(500);
                Dashboardpage.ActionsButton.Click();
                //Thread.Sleep(500);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                //Thread.Sleep(500);
                WaitForElement(SendIntialNotice.SendInitialNoticeButton);
                //Thread.Sleep(500);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                //Thread.Sleep(500);
                SendIntialNotice.SendInitialNoticeButton.Click();
                //Thread.Sleep(500);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                //Thread.Sleep(500);
            }
        }

        [When(@"I provide the details on initial notice qs page")]
        public void WhenIProvideTheDetailsOnInitialNoticeQsPage()
        {
            WaitForElement(SendIntialNotice.IsConnectiontotheSewer);
            //Thread.Sleep(500);
            SendIntialNotice.IntialNoticeQS();
            log.Info($"Details are provided on IntialNoticeQSPage");
        }

        [When(@"I verify the details on file review page")]
        public void WhenIVerifyTheDetailsOnFileReviewPage()
        {
            WaitForElement(SendIntialNotice.InitialNoticeNextButton);
            SendIntialNotice.InitialNoticeNextButton.Click();
            WaitForElement(SendIntialNotice.Blockers);
            SendIntialNotice.FileReviewDetails();
            log.Info($"Details are Verified on FileReview Page on IntialNotice Wizard");

        }

        [When(@"I verify the details on confirm page")]
        public void WhenIVerifyTheDetailsOnConfirmPage()
        {
            WaitForElement(SendIntialNotice.InitialNoticeNextButton);
            SendIntialNotice.InitialNoticeNextButton.Click();
            WaitForElement(SendIntialNotice.ConfirmDetailsPage);
            SendIntialNotice.ConfirmDetails();
            log.Info($"Details are Verified on Confirm Page on IntialNotice Wizard");

        }

        [When(@"I verify the details on intoclient page")]
        public void WhenIVerifyTheDetailsOnIntoclientPage()
        {
            WaitForElement(SendIntialNotice.InitialNoticeNextButton);
            SendIntialNotice.InitialNoticeNextButton.Click();
            WaitForElements(SendIntialNotice.PGGenerateQuote);
            SendIntialNotice.InToClientDetails();
            log.Info($"Details are Verified on IntoClient Page on IntialNotice Wizard");

        }

        [When(@"I Verify the details on intola page")]
        public void WhenIVerifyTheDetailsOnIntolaPage()
        {
            WaitForElement(SendIntialNotice.InitialNoticeNextButton);
            SendIntialNotice.InitialNoticeNextButton.Click();
            WaitForElements(SendIntialNotice.PGGenerateQuote);
            SendIntialNotice.InToLADetails();
            log.Info($"Details are Verified on IntoLA Page on IntialNotice Wizard");

        }

        [When(@"I click on send initial notice button to complete sending intial notice")]
        public void WhenIClickOnSendInitialNoticeButtonToCompleteSendingIntialNotice()
        {
            WaitForElement(SendIntialNotice.CompleteSednIntialNoticeButton);
            SendIntialNotice.CompleteSednIntialNoticeButton.Click();
            //Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(500);
        }

        [Then(@"I should see the intial notice status has been sent")]
        public void ThenIShouldSeeTheIntialNoticeStatusHasBeenSent()
        {
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(500);
            WaitForElement(RespondIntialNotice.BuildingControlTab);
            //Thread.Sleep(500);
            RespondIntialNotice.BuildingControlTab.Click();
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(500);
            WaitForElement(RespondIntialNotice.Processes);
            RespondIntialNotice.Processes.Click();
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(500);
            RespondIntialNotice.IntialNoticeStatusSent();
            log.Info($"Intial Notice Sent SuccessFully");

        }


        //Accepting Intial notice Steps 

        [When(@"I Select respond to initial notice button")]
        public void WhenISelectRespondToInitialNoticeButton()
        {
            //Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(500);
            WaitForElement(Dashboardpage.ActionsButton);
            WaitForElementToClick(Dashboardpage.ActionsButton);
            Dashboardpage.ActionsButton.Click();
            //Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(500);
            WaitForElement(SendIntialNotice.RespondToInitialNotice);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            WaitForElementToClick(SendIntialNotice.RespondToInitialNotice);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            SendIntialNotice.RespondToInitialNotice.Click();
            //Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(500);
        }
        //Respond to intial notice on warrenty product

        [When(@"I Select respond to initial notice button on warrenty products")]
        public void WhenISelectRespondToInitialNoticeButtonOnWarrentyProducts()
        {
            //Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(500);
            WaitForElement(Dashboardpage.ActionsButton);
            WaitForElementToClick(Dashboardpage.ActionsButton);
            Dashboardpage.ActionsButton.Click();
            //Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(500);
            WaitForElement(SendIntialNotice.RespondToInitialNotice);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            WaitForElementToClick(SendIntialNotice.RespondToInitialNotice);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            SendIntialNotice.RespondToInitialNotice.Click();  
            CloseSpinneronDiv();
            CloseSpinneronPage();
        }


        [When(@"I select the response type as accepted")]
        public void WhenISelectTheResponseTypeAsAccepted()
        {
            WaitForElement(SendIntialNotice.ResponseTypeAccepted);
            SendIntialNotice.ResponseTypeAccepted.Click();
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(500);
        }
        [When(@"I verify the details on acceptance page")]
        public void WhenIVerifyTheDetailsOnAcceptancePage()
        {
            WaitForElement(RespondIntialNotice.FormalAcceptanceButton);
            RespondIntialNotice.FormalAcceptanceButton.Click();
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(500);

        }

        [When(@"I verify the details on fliereview page")]
        public void WhenIVerifyTheDetailsOnFliereviewPage()
        {
            WaitForElement(RespondIntialNotice.AcceptanceIntialNoticeResponseNextButton);
            RespondIntialNotice.AcceptanceIntialNoticeResponseNextButton.Click();
            RespondIntialNotice.FileReviewDetails();
        }

        [When(@"I verify details on confirm page")]
        public void WhenIVerifyDetailsOnConfirmPage()
        {
            WaitForElement(RespondIntialNotice.AcceptanceIntialNoticeResponseNextButton);
            RespondIntialNotice.AcceptanceIntialNoticeResponseNextButton.Click();
            RespondIntialNotice.ConfirmDetails();
        }

        [When(@"I verify the initial notice acceptance details")]
        public void WhenIVerifyTheInitialNoticeAcceptanceDetails()
        {
            WaitForElement(RespondIntialNotice.AcceptanceIntialNoticeResponseNextButton);
            RespondIntialNotice.AcceptanceIntialNoticeResponseNextButton.Click();
            //Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(500);
            RespondIntialNotice.IntialNoticeAcceptanceDetails();
            //Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(500);
        }

        [When(@"I click on send initial notice response -acceptance button")]
        public void WhenIClickOnSendInitialNoticeResponse_AcceptanceButton()
        {
            WaitForElement(RespondIntialNotice.SendIntialNoticeResponseAcceptanceButton);          
            RespondIntialNotice.SendIntialNoticeResponseAcceptanceButton.Click();
            //Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(500);
        }

        [Then(@"I should view the resposne status on building control page\.")]
        public void ThenIShouldViewTheResposneStatusOnBuildingControlPage_()
        {
            //Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(500);
            WaitForElement(RespondIntialNotice.BuildingControlTab);
            //Thread.Sleep(500);
            RespondIntialNotice.BuildingControlTab.Click();
            //Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(500);
            WaitForElement(RespondIntialNotice.Processes);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            WaitForElementToClick(RespondIntialNotice.Processes);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            RespondIntialNotice.Processes.Click();
            //Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(500);
            RespondIntialNotice.IntialNoticeStatusDetails();

        }

        //Rejecting Intial Notice Steps 

        [When(@"I select the response type as rejected")]
        public void WhenISelectTheResponseTypeAsRejected()
        {
            WaitForElement(RespondIntialNotice.RejectIntialNotice);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            WaitForElementToClick(RespondIntialNotice.RejectIntialNotice);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            RespondIntialNotice.RejectIntialNotice.Click();
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(500);
        }

        [When(@"I verify the details on rejection page")]
        public void WhenIVerifyTheDetailsOnRejectionPage()
        {
            WaitForElement(RespondIntialNotice.InitialNoticeResponseRejectWizard);
            Assert.IsTrue(RespondIntialNotice.InitialNoticeResponseRejectWizard.Displayed);
            //Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            WaitForElement(RespondIntialNotice.RejectionReason);
            RespondIntialNotice.RejectionDetails();

        }

        [When(@"I verify details on rejection confirm page")]
        public void WhenIVerifyDetailsOnRejectionConfirmPage()
        {
            WaitForElement(RespondIntialNotice.AcceptanceIntialNoticeResponseNextButton);
            RespondIntialNotice.AcceptanceIntialNoticeResponseNextButton.Click();
            RespondIntialNotice.RejectionConfirmDetails();
        }


        [When(@"I verify the initial notice rejection details")]
        public void WhenIVerifyTheInitialNoticeRejectionDetails()
        {
            WaitForElement(RespondIntialNotice.AcceptanceIntialNoticeResponseNextButton);
            RespondIntialNotice.AcceptanceIntialNoticeResponseNextButton.Click();
            RespondIntialNotice.IntialNoticeAcceptanceDetails();
        }

        [When(@"I click on send initial notice response -reject button")]
        public void WhenIClickOnSendInitialNoticeResponse_RejectButton()
        {
            WaitForElement(RespondIntialNotice.SendIntialNoticeResponseRejectButton);
            RespondIntialNotice.SendIntialNoticeResponseRejectButton.Click();
            //Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(500); ;
        }

        [Then(@"I should view the resposne status as rejected on building control page\.")]
        public void ThenIShouldViewTheResposneStatusAsRejectedOnBuildingControlPage_()
        {
            //Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(500);
            WaitForElement(RespondIntialNotice.BuildingControlTab);
            RespondIntialNotice.BuildingControlTab.Click();
            //Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(500);
            WaitForElement(RespondIntialNotice.Processes);
            RespondIntialNotice.Processes.Click();
            //Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(500);
            RespondIntialNotice.IntialNoticeStatusDetailsonRejection();
        }
    }
}
