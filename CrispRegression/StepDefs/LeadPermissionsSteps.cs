﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TechTalk.SpecFlow;

namespace CrispAutomation.StepDefs
{
    [Binding]
    public sealed class LeadPermissionsSteps : Support.Pages
    {
        [When(@"I remove all permissions on lead operations")]
        public void WhenIRemoveAllPermissionsOnLeadOperations()
        {
            LeadPermissionsPage.RemoveAllPermissionsToUser();
        }
       
        [When(@"I apply permissions on creating lead")]
        public void WhenIApplyPermissionsOnCreatingLead()
        {
            LeadPermissionsPage.ApplyCreatingLeadPermissions();
        }

        [Then(@"I verify that lead can be created not amended")]
        public void ThenIVerifyThatLeadCanBeCreatedNotAmended()
        {
            LeadPermissionsPage.VerifyCreatingLeadWhenPermissionsApplied();
        }

        [When(@"I apply permissions on amending lead")]
        public void WhenIApplyPermissionsOnAmendingLead()
        {
            LeadPermissionsPage.ApplyAmendingLeadPermissions();
        }

        [Then(@"I verify that lead can be amended not created")]
        public void ThenIVerifyThatLeadCanBeAmendedNotCreated()
        {
            LeadPermissionsPage.VerifyAmendingLeadWhenPermissionsApplied();
        }

        [When(@"I apply permissions on view lead")]
        public void WhenIApplyPermissionsOnViewLead()
        {
            LeadPermissionsPage.ApplyViewingLeadPermissions();
        }

        [Then(@"I verify that users can view lead")]
        public void ThenIVerifyThatUsersCanViewLead()
        {
            LeadPermissionsPage.VerifyViewingLeadOperationWhenPermissionsApplied();
        }

    }
}
