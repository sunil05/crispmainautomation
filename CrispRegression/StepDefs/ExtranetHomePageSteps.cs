﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TechTalk.SpecFlow;

namespace CrispAutomation.StepDefs
{
    [Binding]
    public class ExtranetHomePageSteps : Support.Pages
    {
        [When(@"I create LABC quotes on crisp using '(.*)'")]
        public void WhenICreateLABCQuotesOnCrispUsing(string plotdata)
        {
            AddLABCQuotePage.SubmitLABCQuoteForExtranet(plotdata);
            SendQuotePage.SendquoteMainMethod();          
        }
        [When(@"I create PG quotes on crisp using '(.*)'")]
        public void WhenICreatePGQuotesOnCrispUsing(string plotdata)
        {
            AddPGQuotePage.SubmitPGQuoteForExtranet(plotdata);
            SendQuotePage.SendquoteMainMethod();          
        }
        [Then(@"I should verify the quotes on homepage")]
        public void ThenIShouldVerifyTheQuotesOnHomepage()
        {
            ExtranetHomePage.VerifyQuotesOnHomePage();
        }
        [When(@"I create LABC orders on crisp using '(.*)'")]
        public void WhenICreateLABCOrdersOnCrispUsing(string plotdata)
        {
            AddLABCQuotePage.SubmitLABCQuoteForExtranet(plotdata);
            SendQuotePage.SendquoteMainMethod();
            AcceptQuotePage.AcceptQuoteMain();
        }       

        [When(@"I create PG orders on crisp using '(.*)'")]
        public void WhenICreatePGOrdersOnCrispUsing(string plotdata)
        {
            AddPGQuotePage.SubmitPGQuoteForExtranet(plotdata);
            SendQuotePage.SendquoteMainMethod();
            AcceptQuotePage.AcceptQuoteMain();
        }
        [Then(@"I should verify the orders on homepage")]
        public void ThenIShouldVerifyTheOrdersOnHomepage()
        {
            ExtranetHomePage.VerifyOrdersOnHomePage();
        }
        [Then(@"I should verify incomplete applications on homepage")]
        public void ThenIShouldVerifyIncompleteApplicationsOnHomepage()
        {
            ExtranetHomePage.VerifyIncompleteApplicationsOnHomePage();
        }
        [When(@"I click on registration status div on homepage")]
        public void WhenIClickOnRegistrationStatusDivOnHomepage()
        {
            ExtranetHomePage.VerifyRegistrationStatusOnHomePage();
        }
        [Then(@"I should navigate to registration page")]
        public void ThenIShouldNavigateToRegistrationPage()
        {
           // Assert.IsTrue(ExtranetHomePage.OutstandingRegDocuments.Displayed,"Failed to Navigate Registration Page");
           // Assert.IsTrue(ExtranetHomePage.ConfirmedRegDocuments.Displayed,"Failed to Navigate Registration Page");
           // Assert.IsTrue(ExtranetHomePage.TermDetailsOnRegistration.Displayed,"Failed to Navigate Registration Page");
        }
        [When(@"I click on manage company details div on homepage")]
        public void WhenIClickOnManageCompanyDetailsDivOnHomepage()
        {
            ExtranetHomePage.VerifyManageCompanyDetailsOnHomePage();
        }
        [Then(@"I should navigate to company page")]
        public void ThenIShouldNavigateToCompanyPage()
        {
            Assert.IsTrue(ExtranetHomePage.OfficeDetails.Displayed, "Failed to Navigate Company Page");
        }      
        [When(@"I click on extranet LABC training guide on homepage")]
        public void WhenIClickOnExtranetLABCTrainingGuideOnHomepage()
        {
            ExtranetHomePage.VerifyDownloadLABCTrainingGuideOnHomePage();
        }
        [Then(@"I should see training guide is downloaded")]
        public void ThenIShouldSeeTrainingGuideIsDownloaded()
        {
            
        }
        [When(@"I click on health and safety div on homepage")]
        public void WhenIClickOnHealthAndSafetyDivOnHomepage()
        {
            ExtranetHomePage.VerifyHealthAndSafetyDivOnHomePage();
        }

        [Then(@"I should see health and safety site is opened")]
        public void ThenIShouldSeeHealthAndSafetySiteIsOpened()
        {
            
        }
    }
}
