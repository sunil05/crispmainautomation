﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TechTalk.SpecFlow;

namespace CrispAutomation.StepDefs
{
    [Binding]
    public class NoPermissionsSteps : Support.Pages
    {
        //Contact Permissions Steps 

        [Given(@"I have created auto user to check permissions")]
        public void GivenIHaveCreatedAutoUserToCheckPermissions()
        {
            DBConnection.RemovePermissionsOnUser();
        }
        [When(@"I remove all permissions on auto user")]
        public void WhenIRemoveAllPermissionsOnAutoUser()
        {
           
        }

        [Then(@"I verify that not perform create operations")]
        public void ThenIVerifyThatNotPerformCreateOperations()
        {
            
        }

        [Then(@"I verify that not perform amend operations")]
        public void ThenIVerifyThatNotPerformAmendOperations()
        {
           
        }

        [Then(@"I verify that can not perform search operations")]
        public void ThenIVerifyThatCanNotPerformSearchOperations()
        {
           
        }
    }
}
