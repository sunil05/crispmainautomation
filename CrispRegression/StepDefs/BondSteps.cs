﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TechTalk.SpecFlow;

namespace CrispAutomation.StepDefs
{
    [Binding]
    public class BondSteps : Support.Pages
    {
        [When(@"I select site details on quote")]
        public void WhenISelectSiteDetailsOnQuote()
        {
            WaitForElement(Dashboardpage.ActionsButton);
            Dashboardpage.ActionsButton.Click();
            WaitForElement(Dashboardpage.SiteDetails);
            Dashboardpage.SiteDetails.Click();
        }
        [Then(@"I can create road bond")]
        public void ThenICanCreateRoads()
        {
            BondsPage.CreateRoadBond();
        }
        [Then(@"I can create sewer bond")]
        public void ThenICanCreateSewerBond()
        {
            BondsPage.CreateSewerBond();
        }
        [When(@"I create conditions on bond")]
        public void WhenICreateConditionsOnBond()
        {           
            BondsPage.CreateConditionsOnRoadBond();
        }

        [Then(@"I should verify conditions on bond")]
        public void ThenIShouldVerifyConditionsOnBond()
        {
            BondsPage.VerifyConditionsOnBond();
            BondsPage.ChaseBondConditions();
            BondsPage.CloseBondConditions();
        }


        [When(@"I create single bond")]
        public void WhenICreateSingleBond()
        {
            BondsPage.CreateSingleRoadBond();
        }

        [Then(@"I should accept the bond")]
        public void ThenIShouldAcceptTheBond()
        {
            BondsPage.AcceptBond();
        }

        [Then(@"I should reject the bond")]
        public void ThenIShouldRejectTheBond()
        {
            BondsPage.RejectingBond();
        }

    }
}
