﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TechTalk.SpecFlow;
using CrispAutomation.Pages;
using NUnit.Framework;
using System.Threading;

namespace CrispAutomation.StepDefs
{
    [Binding]
    public class VerifyManageCompanyListSteps : Support.Pages
    {
        [When(@"I click the on manage company group and lists icon")]
        public void WhenIClickTheOnManageCompanyGroupAndListsIcon()
        {
            WaitForElement(Dashboardpage.AdministrationButton);
            Dashboardpage.AdministrationButton.Click();
            //Thread.Sleep(1000);
        }

        [When(@"I select manage company lists option")]
        public void WhenISelectManageCompanyListsOption()
        {
            WaitForElement(Dashboardpage.ManageCompanyList);
            Dashboardpage.ManageCompanyList.Click();
            //Thread.Sleep(1000);
        }

        [When(@"I Verfy the company role list")]
        public void WhenIVerfyTheCompanyRoleList()
        {
            WaitForElements(ManageCompanyListPage.TypeofCompanyRoleList);
            ManageCompanyListPage.VerifyDefaultCompanyList();
        }

        [Then(@"I should see the populated companies")]
        public void ThenIShouldSeeThePopulatedCompanies()
        {
            Console.WriteLine("Companies are dsiplayed");
        }

        [When(@"I edit the populated companies on company role")]
        public void WhenIEditThePopulatedCompaniesOnCompanyRole()
        {
            WaitForElements(ManageCompanyListPage.SelectedCompanyRoleList);
            if (ManageCompanyListPage.SelectedCompanyRoleList.Count > 0)
            {
                WaitForElements(ManageCompanyListPage.SelectButton);
                //Thread.Sleep(500);
                if (ManageCompanyListPage.SelectButton.Count > 0)
                {
                    for (int i = 1; i < 5; i++)
                    {
                        WaitForElement(ManageCompanyListPage.SelectButton[i]);
                        ManageCompanyListPage.SelectButton[i].Click();
                        //Thread.Sleep(500);
                    }
                    WaitForElement(ManageCompanyListPage.SaveButton);
                    //Thread.Sleep(500);
                    ManageCompanyListPage.EditSelectedCompanyRole();
                }
            }
        }

        [Then(@"I should see the saved results")]
        public void ThenIShouldSeeTheSavedResults()
        {
            Console.WriteLine("Results has been saved");
        }

    }
}


