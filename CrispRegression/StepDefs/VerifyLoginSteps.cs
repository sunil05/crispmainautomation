﻿using System;
using System.Configuration;
using System.Threading;
using NUnit.Framework;
using TechTalk.SpecFlow;


namespace CrispAutomation.StepDefs
{
    [Binding]
    public class VerifyLoginSteps:Support.Pages
    {
        [Given(@"I am on login page")]
        public void GivenIAmOnLoginPage()
        {
              WaitForElement(Loginpage.LoginButton);          
        }
        [When(@"I enter crisp username")]
        public void WhenIEnterCrispUsername()
        {
       
            WaitForElement(Loginpage.Username);
            Loginpage.Username.Click();
            Loginpage.Username.Clear();
            Loginpage.Username.SendKeys(ConfigurationManager.AppSettings["CrispUsername"]);
        }

        [When(@"I enter crisp password")]
        public void WhenIEnterCrispPassword()
        {
            WaitForElement(Loginpage.Password);
            Loginpage.Password.Click();
            Loginpage.Password.Clear();
            Loginpage.Password.SendKeys(ConfigurationManager.AppSettings["CrispPassword"]);
        }      

        [When(@"I click on login button")]
        public void WhenIClickOnLoginButton()
        {
           Loginpage.LoginButton.Click();         
        }

        [Then(@"I should see all items displayed on dashboard page")]
        public void ThenIShouldSeeAllItemsDisplayedOnDashboardPage()
        {
            WaitForElements(Dashboardpage.NavItems);
            Assert.True(Dashboardpage.NavItems.Count > 0,"User Failed to login to Crisp Application");
            foreach (var item in Dashboardpage.NavItems)
            {
                Assert.True(item.Displayed);
            }           
          // DBConnection.ApplyPermissionsOnCrispUser();
        }

        [When(@"I log into surveyor site")]
        public void WhenILogIntoSurveyorSite()
        {
            SurveyorLoginPage.SurveyorLoginMethod();
          
        }

        [Then(@"I should see surveyor portal on the dashboard")]
        public void ThenIShouldSeeSurveyorPortalOnTheDashboard()
        {
            Assert.IsTrue(SurveyorLoginPage.SurveyorPortal.Displayed,"Failed to logon to Surveyor Application");
            Console.WriteLine("Surveyor Portal  has been logged in ");
        }

    }
}
