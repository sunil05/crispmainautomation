﻿using CrispAutomation.Features;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using TechTalk.SpecFlow;

namespace CrispAutomation.StepDefs
{
    [Binding]
    public class FinalNoticeSteps : Support.Pages
    {
        [When(@"I login to surveyor application to signoff building control")]
        public void WhenILoginToSurveyorApplicationToSignoffBuildingControl()
        {            
            FinalNoticePage.SignoffBuildingControl(Statics.OrderNumber);
        }

        [When(@"I loginback to crisp application to send final notice")]
        public void WhenILoginbackToCrispApplicationToSendFinalNotice()
        {
            FinalNoticePage.SendFinalNotice();
        }

        [Then(@"I should see final notice has been sent successfully\.")]
        public void ThenIShouldSeeFinaNoticeHasBeenSentSuccessfully_()
        {
            //Thread.Sleep(1500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(500);
            if (SendIntialNotice.ReloadButton.Count > 0)
            {
                WaitForElements(SendIntialNotice.ReloadButton);
                SendIntialNotice.ReloadButton[0].Click();
            }
            else
            {
                Driver.Navigate().Refresh();
            }
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(1000);
            WaitForElement(RespondIntialNotice.BuildingControlTab);
            //Thread.Sleep(1000);
            RespondIntialNotice.BuildingControlTab.Click();
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            FinalNoticePage.FinalNoticeStatus();
        }

        [Then(@"I should see partial final notice has been sent successfully\.")]
        public void ThenIShouldSeePartialFinalNoticeHasBeenSentSuccessfully_()
        {
            //Thread.Sleep(1500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(500);
            if (SendIntialNotice.ReloadButton.Count > 0)
            {
                WaitForElements(SendIntialNotice.ReloadButton);
                SendIntialNotice.ReloadButton[0].Click();
            }
            else
            {
                Driver.Navigate().Refresh();
            }
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(1000);
            WaitForElement(RespondIntialNotice.BuildingControlTab);
            //Thread.Sleep(1000);
            RespondIntialNotice.BuildingControlTab.Click();
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            FinalNoticePage.PartialFinalNoticeStatus();
        }


    }
}
