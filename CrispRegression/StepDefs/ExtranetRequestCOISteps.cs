﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TechTalk.SpecFlow;

namespace CrispAutomation.StepDefs
{
    [Binding]
    public class ExtranetRequestCOISteps :Support.Pages
    {
        [Then(@"I should request coi from homepage")]
        public void ThenIShouldRequestCoiFromHomepage()
        {
            ExtranetRequestIssueCOIPage.RequestCOIfromHomePage();
        }
        [Then(@"I should request coi from orderspage")]
        public void ThenIShouldRequestCoiFromOrderspage()
        {
            ExtranetRequestIssueCOIPage.RequestCOIfromOrdersPage();
        }
        [Then(@"I should request coi from plotdata")]
        public void ThenIShouldRequestCoiFromPlotdata()
        {
            ExtranetRequestIssueCOIPage.RequestCOIfromOrdersPage();
        }
        [When(@"I verify coi task and issue the COI from Crisp")]
        public void WhenIVerifyCoiTaskAndIssueTheCOIFromCrisp()
        {
            ExtranetRequestIssueCOIPage.VerifyTasksInTheCrisp();
            IssueCertificatePage.COIForExtranet();
        }

        [Then(@"I sould verify coi certifcate on extranet")]
        public void ThenISouldVerifyCoiCertifcateOnExtranet()
        {
            ExtranetRequestIssueCOIPage.VerifyCOIDocOnExtranet();
        }
    }
}
