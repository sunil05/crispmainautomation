﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading;
using NUnit.Framework;
using TechTalk.SpecFlow;

namespace CrispAutomation.StepDefs
{
    [Binding]
    public class ManageConditionsSteps : Support.Pages
    {
        [When(@"I select on conditions option")]
        public void WhenISelectOnConditionsOption()
        {
            WaitForElement(ManageConditionsPage.ConditionsOption);
            ManageConditionsPage.ConditionsOption.Click();
            WaitForElement(ManageConditionsPage.ConditionsTitle);
            Assert.IsTrue(ManageConditionsPage.ConditionsTitle.Displayed);
            //Thread.Sleep(1000);
        }

        [When(@"I create conditions and verify them")]
        public void WhenICreateConditionsAndVerifyThem()
        {
            ManageConditionsPage.AddAndVerifySiteCondition();
            ManageConditionsPage.AddAndVerifyProductsCondition();
            ManageConditionsPage.AddAndVerifyPlotsCondition();

        }

        [Then(@"conditions should be displayed in the conditions page")]
        public void ThenConditionsShouldBeDisplayedInTheConditionsPage()
        {
            ManageConditionsPage.VerifyConditionsList();
        }


        // Send Quote Steps When Conditions are Opened
        [When(@"I verify above manual conditions details on conditions page")]
        public void WhenIVerifyAboveManualConditionsDetailsOnConditionsPage()
        {
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            WaitForElements(ManageConditionsPage.SendQuoteConditionsList);
            Assert.IsTrue(ManageConditionsPage.SendQuoteConditionsList.Count >= 3);
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            WaitForElement(SendQuotePage.SendQuoteNextButton);           
            SendQuotePage.SendQuoteNextButton.Click();
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
        }

        [When(@"I verify above manual conditions on file review details page")]
        public void WhenIVerifyAboveManualConditionsOnFileReviewDetailsPage()
        {
            WaitForElement(ManageConditionsPage.SendQuoteFileReviewConditionsList);
            Assert.IsTrue(ManageConditionsPage.SendQuoteFileReviewConditionsList.Displayed);
            //Thread.Sleep(1500);
            //Thread.Sleep(500);
            WaitForElement(SendQuotePage.SendQuoteNextButton);
            //Thread.Sleep(500);
            SendQuotePage.SendQuoteNextButton.Click();
            //Thread.Sleep(500);
        }

        [When(@"I verify the confirm chase on confirm details page")]
        public void WhenIVerifyTheConfirmChaseOnConfirmDetailsPage()
        {
            
            if (SendQuotePage.ConfirmPageEle.Count > 0)
            {
                WaitForElements(SendQuotePage.ConfirmPageEle);
                if (SendQuotePage.ConfirmExpiry.Count > 0)
                {
                    WaitForElements(SendQuotePage.ConfirmExpiry);
                    //Thread.Sleep(500);
                    Assert.IsTrue(SendQuotePage.ConfirmExpiry[0].Displayed);
                    Console.WriteLine("confirm details verified");
                    //Thread.Sleep(500);
                    CloseSpinneronDiv();
                }
                //Thread.Sleep(500);
                if (SendQuotePage.ChaseOutstandingInformation.Count > 0)
                {
                    WaitForElement(SendQuotePage.ChaseOutstandingInformation[0]);
                    SendQuotePage.ChaseOutstandingInformation[0].Click();
                    //Thread.Sleep(500);
                    CloseSpinneronDiv();
                }
                //Thread.Sleep(500);
                CloseSpinneronDiv();
                //Thread.Sleep(1000);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                WaitForElement(SendQuotePage.SendQuoteNextButton);
                //Thread.Sleep(500);
                SendQuotePage.SendQuoteNextButton.Click();
                //Thread.Sleep(500);
            }
        }

        [Then(@"I should see quote is not changed into quoted")]
        public void ThenIShouldSeeQuoteIsNotChangedIntoQuoted()
        {
            WaitForElement(Dashboardpage.QuoteApplication);
            Assert.IsTrue(Dashboardpage.QuoteApplication.Displayed);
            Assert.IsTrue(Dashboardpage.QuoteApplicationStatusSubmitted.Displayed);
        }

        //Send Quote steps after closing conditions 
        [When(@"I click on  manual conditions")]
        public void WhenIClickOnManualConditions()
        {
            CloseSpinneronDiv();
            WaitForElements(ManageConditionsPage.ManualConditionsList);
        }

        [When(@"I perform the operation to close all conditions")]
        public void WhenIPerformTheOperationToCloseAllConditions()
        {
            CloseSpinneronDiv();
            ManageConditionsPage.ClosingCondition();
            //Thread.Sleep(1000);
        }

        [When(@"I try to send the quote")]
        public void WhenITryToSendTheQuote()
        {
            SendQuotePage.SendquoteMainMethod();

        }
        [Then(@"I should see quoted details on the dashboard")]
        public void ThenIShouldSeeQuotedDetailsOnTheDashboard()
        {
            Console.WriteLine("Quote has been sent");
        }


        // Clone Condition Steps 
        [When(@"I create site condition and verify them")]
        public void WhenICreateSiteConditionAndVerifyThem()
        {
            ManageConditionsPage.AddAndVerifySiteCondition();

        }

        [When(@"I perform the operation to clone all conditions")]
        public void WhenIPerformTheOperationToCloneAllConditions()
        {
            ManageConditionsPage.CloneManualConditions();
        }

        [Then(@"I should see each condition has been cloned")]
        public void ThenIShouldSeeEachConditionHasBeenCloned()
        {
            Assert.IsTrue(ManageConditionsPage.ManualConditionsList.Count == 2);
        }
        //Clone Automatic Condition Steps 
        [When(@"I select automatic condition")]
        public void WhenISelectAutomaticCondition()
        {
            WaitForElements(ManageConditionsPage.AutomaticConditionsList);
        }

        [When(@"I perform the operation to clone condition")]
        public void WhenIPerformTheOperationToCloneCondition()
        {
           ManageConditionsPage.CloneAutomaticConditions();
        }

        [Then(@"I should see the condition has been cloned")]
        public void ThenIShouldSeeTheConditionHasBeenCloned()
        {
            Assert.IsTrue(ManageConditionsPage.ManualConditionsList.Count == 3);
        }
        //Edit Codnition 

        [When(@"I perform the operation to edit condition")]
        public void WhenIPerformTheOperationToEditCondition()
        {
            ManageConditionsPage.EditCondition();
        }

        [Then(@"I should see the condition has been edited")]
        public void ThenIShouldSeeTheConditionHasBeenEdited()
        {
            Assert.IsTrue(ManageConditionsPage.ManualConditionsList.Count == 3);
        }
    }
}
