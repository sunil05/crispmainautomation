﻿using CrispAutomation.Features;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using TechTalk.SpecFlow;

namespace CrispAutomation.StepDefs
{

    [Binding]
    public class SurveyorAssessmentSteps : Support.Pages
    {
        //Site Risk Assessment Section 
       
        [When(@"I login to surveyor site")]
        public void WhenILoginToSurveyorSite()
        {
            SurveyorLoginPage.SurveyorLoginMethod();
        }
        
        [When(@"I select the one of the site from the list")]
        public void WhenISelectTheOneOfTheSiteFromTheList()
        {
            SurveyorLoginPage.SelectSite();
        }

        [When(@"I click on site risk assessment option")]
        public void WhenIClickOnSiteRiskAssessmentOption()
        {
            Thread.Sleep(500);
            CloseSpinnerOnSurveyorPage();
            Thread.Sleep(500);
            WaitForElementonSurveyor(SiteRiskAssessmentPage.SiteRiskAssessmentOption);
            Thread.Sleep(500);
            CloseSpinnerOnSurveyorPage();
            Thread.Sleep(500);
            SiteRiskAssessmentPage.SiteRiskAssessmentOption.Click();
            Thread.Sleep(500);
            CloseSpinnerOnSurveyorPage();
            Thread.Sleep(500);
            WaitForElementonSurveyor(SiteRiskAssessmentPage.SiteRiskSection);
            Assert.IsTrue(SiteRiskAssessmentPage.SiteRiskSection.Displayed);
            Assert.IsTrue(SiteRiskAssessmentPage.EditButton.Displayed, "No permissions applied to the user to submit SiteRisk Assessment");
        }


        [When(@"I Click on edit button to submit the site risk assessment")]
        public void WhenIClickOnEditButtonToSubmitTheSiteRiskAssessment()
        {
            //Thread.Sleep(1000);
            WaitForElementonSurveyor(SiteRiskAssessmentPage.EditButton);
            SiteRiskAssessmentPage.EditButton.Click();
            //Thread.Sleep(500);
            CloseSpinnerOnSurveyorPage();
            //Thread.Sleep(500);
            WaitForElementonSurveyor(SiteRiskAssessmentPage.AlertInfoOnEdit);
            Assert.IsTrue(SiteRiskAssessmentPage.AlertInfoOnEdit.Displayed);
            SiteRiskAssessmentPage.EditSiteRiSkAssessmentSection();
            //Thread.Sleep(500);
            CloseSpinnerOnSurveyorPage();
            //Thread.Sleep(500);
            WaitForElementonSurveyor(SiteRiskAssessmentPage.AcceptableRiskRadioButton);
            SiteRiskAssessmentPage.AcceptableRiskRadioButton.Click();
            //Thread.Sleep(500);
            CloseSpinnerOnSurveyorPage();
            //Thread.Sleep(500);
            WaitForElementonSurveyor(SiteRiskAssessmentPage.SubmitButton);
            SiteRiskAssessmentPage.SubmitButton.Click();
            //Thread.Sleep(500);
            CloseSpinnerOnSurveyorPage();
            //Thread.Sleep(500);
            WaitForElementonSurveyor(SiteRiskAssessmentPage.SendButton);
            SiteRiskAssessmentPage.SendButton.Click();
            //Thread.Sleep(500);
            CloseSpinnerOnSurveyorPage();
            //Thread.Sleep(500);
        }
        [When(@"I Click on edit button to submit the site risk assessment with no options")]
        public void WhenIClickOnEditButtonToSubmitTheSiteRiskAssessmentWithNoOptions()
        {

            //Thread.Sleep(1000);
            WaitForElementonSurveyor(SiteRiskAssessmentPage.EditButton);
            SiteRiskAssessmentPage.EditButton.Click();
            //Thread.Sleep(500);
            CloseSpinnerOnSurveyorPage();
            //Thread.Sleep(500);
            WaitForElementonSurveyor(SiteRiskAssessmentPage.AlertInfoOnEdit);
            Assert.IsTrue(SiteRiskAssessmentPage.AlertInfoOnEdit.Displayed);
            SiteRiskAssessmentPage.EditSRAWhenNoSelected();
            //Thread.Sleep(500);
            CloseSpinnerOnSurveyorPage();
            //Thread.Sleep(500);
            WaitForElementonSurveyor(SiteRiskAssessmentPage.AcceptableRiskRadioButton);
            SiteRiskAssessmentPage.AcceptableRiskRadioButton.Click();
            //Thread.Sleep(500);
            CloseSpinnerOnSurveyorPage();
            //Thread.Sleep(500);
            WaitForElementonSurveyor(SiteRiskAssessmentPage.SubmitButton);
            SiteRiskAssessmentPage.SubmitButton.Click();
            //Thread.Sleep(500);
            CloseSpinnerOnSurveyorPage();
            //Thread.Sleep(500);
            WaitForElementonSurveyor(SiteRiskAssessmentPage.SendButton);
            SiteRiskAssessmentPage.SendButton.Click();
            //Thread.Sleep(500);
            CloseSpinnerOnSurveyorPage();
            //Thread.Sleep(500);
        }


        [Then(@"Siterisk assessment should be submitted successfully")]
        public void ThenSiteriskAssessmentShouldBeSubmittedSuccessfully()
        {
            WaitForElementonSurveyor(SiteRiskAssessmentPage.SiteRiskSection);
            Assert.IsTrue(SiteRiskAssessmentPage.SiteRiskSection.Displayed, "Failed to send correspondence on Site Risk Assessment");
        }

        //Refurbishment Assessment Section
       
    
        [When(@"I click on the refurbishment risk assessment option")]
        public void WhenIClickOnTheRefurbishmentRiskAssessmentOption()
        {
            WaitForElementonSurveyor(RefurbishmentAssessmentPage.RefurbishmentAssessmentOption);
            RefurbishmentAssessmentPage.RefurbishmentAssessmentOption.Click();  
        }
        [When(@"I click on edit button to submit the refurbishment risk assessment")]
        public void WhenIClickOnEditButtonToSubmitTheRefurbishmentRiskAssessment()
        {
            WaitForElementonSurveyor(RefurbishmentAssessmentPage.EditButton);
            RefurbishmentAssessmentPage.EditButton.Click();
            //Thread.Sleep(500);
            CloseSpinnerOnSurveyorPage();
            //Thread.Sleep(500);
            WaitForElementonSurveyor(RefurbishmentAssessmentPage.AlertStatusonEdit);
            Assert.IsTrue(RefurbishmentAssessmentPage.AlertStatusonEdit.Displayed);
            RefurbishmentAssessmentPage.EditRefurbishmentAssessmentSection();
            WaitForElementonSurveyor(RefurbishmentAssessmentPage.SubmitButton);
            RefurbishmentAssessmentPage.SubmitButton.Click();
            //Thread.Sleep(500);
            CloseSpinnerOnSurveyorPage();
            //Thread.Sleep(500);
            WaitForElementonSurveyor(SiteRiskAssessmentPage.SendButton);
            SiteRiskAssessmentPage.SendButton.Click();
            //Thread.Sleep(500);
            CloseSpinnerOnSurveyorPage();
            //Thread.Sleep(500);
        }

        [Then(@"Refurbishment assessment should be submitted successfully")]
        public void ThenRefurbishmentAssessmentShouldBeSubmittedSuccessfully()
        {
            WaitForElementonSurveyor(SiteRiskAssessmentPage.RefurbishmentSection);
            Assert.IsTrue(SiteRiskAssessmentPage.RefurbishmentSection.Displayed, "Failed to send correspondence on Refurbishment Assessment");

        }

        [When(@"I click on edit button to submit the refurbishment risk assessment with no options")]
        public void WhenIClickOnEditButtonToSubmitTheRefurbishmentRiskAssessmentWithNoOptions()
        {
            WaitForElementonSurveyor(RefurbishmentAssessmentPage.EditButton);
            RefurbishmentAssessmentPage.EditButton.Click();
            //Thread.Sleep(500);
            CloseSpinnerOnSurveyorPage();
            //Thread.Sleep(500);
            WaitForElementonSurveyor(RefurbishmentAssessmentPage.AlertStatusonEdit);
            Assert.IsTrue(RefurbishmentAssessmentPage.AlertStatusonEdit.Displayed);
            RefurbishmentAssessmentPage.EditRefurbishmentAssessmentWhenNoSelected();
            WaitForElementonSurveyor(RefurbishmentAssessmentPage.SubmitButton);
            RefurbishmentAssessmentPage.SubmitButton.Click();
            //Thread.Sleep(500);
            CloseSpinnerOnSurveyorPage();
            //Thread.Sleep(500);
            WaitForElementonSurveyor(SiteRiskAssessmentPage.SendButton);
            SiteRiskAssessmentPage.SendButton.Click();
            //Thread.Sleep(500);
            CloseSpinnerOnSurveyorPage();
            //Thread.Sleep(500);
        }
             

        // Techinical inspection Page
        [When(@"I click on site inspection option to add inspections")]
        public void WhenIClickOnSiteInspectionOptionToAddInspections()
        {

            SiteInspectionPage.InspectionUpdateMain();
        }

        [Then(@"site inspection should be added successfully")]
        public void ThenSiteInspectionShouldBeAddedSuccessfully()
        {
            //WaitForElementonSurveyor(SiteInspectionPage.AlertStatus);
            //Assert.IsTrue(SiteInspectionPage.AlertStatus.Displayed);
            Debug.WriteLine("Site Inspections Submitted Successfully");
        }

        //Design Review Page 
        [When(@"I click on Design Review option")]
        public void WhenIClickOnDesignReviewOption()
        {
            WaitForElementonSurveyor(DesignReviewPage.DesignReviewOption);
            DesignReviewPage.DesignReviewOption.Click();
            //Thread.Sleep(500);
            CloseSpinnerOnSurveyorPage();
            //Thread.Sleep(500);
        }
        [When(@"I Click on edit button to submit design review")]
        public void WhenIClickOnEditButtonToSubmitDesignReview()
        {
            WaitForElementonSurveyor(DesignReviewPage.EditButton);
            //Thread.Sleep(500);
            CloseSpinnerOnSurveyorPage();
            //Thread.Sleep(500);
            DesignReviewPage.EditButton.Click();
            //Thread.Sleep(500);
            CloseSpinnerOnSurveyorPage();
            //Thread.Sleep(500);
            DesignReviewPage.DesignReviewAssessment();
            //Thread.Sleep(500);
            CloseSpinnerOnSurveyorPage();
            //Thread.Sleep(500);

        }
        [Then(@"design review assessment should be submitted successfully")]
        public void ThenDesignReviewAssessmentShouldBeSubmittedSuccessfully()
        {
            DesignReviewPage.SubmitDR();
            //Thread.Sleep(500);
            CloseSpinnerOnSurveyorPage();
            //Thread.Sleep(500);
            WaitForElementonSurveyor(DesignReviewPage.DesignReviewMainPage);
            Assert.IsTrue(DesignReviewPage.DesignReviewMainPage.Displayed);
        }
        //Engineer Review 
        [When(@"I click on Engineer Review option")]
        public void WhenIClickOnEngineerReviewOption()
        {
            WaitForElementonSurveyor(EngineerReviewPage.EngineerReviewOption);
            EngineerReviewPage.EngineerReviewOption.Click();
            //Thread.Sleep(500);
            CloseSpinnerOnSurveyorPage();
            //Thread.Sleep(500);
        }
        [When(@"I Click on edit button to engineer review")]
        public void WhenIClickOnEditButtonToEngineerReview()
        {
            WaitForElementonSurveyor(EngineerReviewPage.EditButton);
            //Thread.Sleep(500);
            CloseSpinnerOnSurveyorPage();
            //Thread.Sleep(500);
            EngineerReviewPage.EditButton.Click();
            //Thread.Sleep(500);
            CloseSpinnerOnSurveyorPage();
            //Thread.Sleep(500);
            EngineerReviewPage.EngineerReviewAssessment();
        }
        [Then(@"engineer review assessment should be submitted successfully")]
        public void ThenEngineerReviewAssessmentShouldBeSubmittedSuccessfully()
        {
            EngineerReviewPage.SubmitER();
            //Thread.Sleep(500);
            CloseSpinnerOnSurveyorPage();
            //Thread.Sleep(500);
            WaitForElementonSurveyor(EngineerReviewPage.EngineerReviewMainPage);
            Assert.IsTrue(EngineerReviewPage.EngineerReviewMainPage.Displayed);
        }
        [When(@"I create new covernote")]
        public void WhenICreateNewCovernote()
        {
            CoverNotesPage.VerifyCoverNoteErrors();
            CoverNotesPage.CreatingCoverNote();
        }
        [Then(@"the covernote should be added successfully")]
        public void ThenTheCovernoteShouldBeAddedSuccessfully()
        {
          
        }
        //Submit Surveyor Assessments 
        [When(@"I login to surveyor site to submit surveyor assessments")]
        public void WhenILoginToSurveyorSiteToSubmitSurveyorAssessments()
        {
         
            SurveyorAssessments.SurveyorAssessment();
        }

        [Then(@"surveyor assessment should be submitted successfully")]
        public void ThenSurveyorAssessmentShouldBeSubmittedSuccessfully()
        {
            Debug.WriteLine("Surveyor Assessments Are Submitted Successfully");
        }
        //Open Documents on SRA 
        [When(@"I upload docs on SRA assessments")]
        public void WhenIUploadDocsOnSRAAssessments()
        {
            SurveyorDocsSRA.OpenSurveyorDocsOnSRA();
        }        

        // Close Documents on SRA 
        [Then(@"I close docs on SRA assessments")]
        public void WhenICloseDocsOnSRAAssessments()
        {
            SurveyorDocsSRA.CloseSurveyorDocsOnSRA();
        }
        // Can Not Close Documents on SRA 
        [Then(@"I can not close docs on SRA assessments")]
        public void ThenICanNotCloseDocsOnSRAAssessments()
        {
            SurveyorDocsSRA.NotClosingDocsonSRA();
        }

        //Verify Closed Docs on SRA
        [Then(@"I should verify the closed SRA documents")]
        public void ThenIShouldVerifyTheClosedSRADocuments()
        {
            SurveyorDocsSRA.VerifyClosedSurveyorDocsOnSRA();
        }

       //Open Documents on RA 

        [When(@"I upload docs on RA assessments")]
        public void WhenIUploadDocsOnRAAssessments()
        {
            SurveyorDocsRA.OpenSurveyorDocsRA();
        }
        //Close Documents on RA
        [Then(@"I close docs on RA assessments")]
        public void WhenICloseDocsOnRAAssessments()
        {
            SurveyorDocsRA.CloseSurveyorDocsRA();
        }
        //Verify Closed Documents on RA
        [Then(@"I should verify the closed RA documents")]
        public void ThenIShouldVerifyTheClosedRADocuments()
        {
            SurveyorDocsRA.VerifyClosedSurveyorDocsOnRA();
        }
        //Can Not Close Documents on RA
        [Then(@"I can not close docs on RA assessments")]
        public void WhenICanNotCloseDocsOnRAAssessments()
        {
            SurveyorDocsRA.NotClosingSurveyorDocsRA();
        }


        //Open Documents on DR 
        [When(@"I upload docs on DR assessments")]
        public void WhenIUploadDocsOnDRAssessments()
        {
            SurveyorDocsDR.OpenSurveyorDocsDR();
        }
        //Close Documents on DR
        [Then(@"I close docs on DR assessments")]
        public void WhenICloseDocsOnDRAssessments()
        {
            SurveyorDocsDR.CloseSurveyorDocsDR();
        }
        //Can Not Close Documents on DR
        [Then(@"I can not close docs on DR assessments")]
        public void ThenICanNotCloseDocsOnDRAssessments()
        {
            SurveyorDocsDR.NotClosingSurveyorDocsDR();
        }

        //Verify Closed Documents on DR
        [Then(@"I should verify the closed DR documents")]
        public void ThenIShouldVerifyTheClosedDRDocuments()
        {
            SurveyorDocsDR.VerifyClosedSurveyorDocsOnDR();
        }

        //Open Documents on ER 
        [When(@"I upload docs on ER assessments")]
        public void WhenIUploadDocsOnERAssessments()
        {
            SurveyorDocsER.OpenSurveyorDocsER();
        }
        //Close Documents on ER
        [Then(@"I close docs on ER assessments")]
        public void WhenICloseDocsOnERAssessments()
        {
            SurveyorDocsER.CloseSurveyorDocsER();
        }
        //Not Closing Documents on ER
        [Then(@"I can not close docs on ER assessments")]
        public void WhenICanNotCloseDocsOnERAssessments()
        {
            SurveyorDocsER.NotClosingSurveyorDocsER();
        }
        //Verify Closed Documents on ER
        [Then(@"I should verify the closed ER documents")]
        public void ThenIShouldVerifyTheClosedERDocuments()
        {
            SurveyorDocsER.VerifyClosedSurveyorDocsOnER();
        }
        //Verify Open Documents on Site Inspection
        [When(@"I upload docs on SiteInspection assessments")]
        public void WhenIUploadDocsOnSiteInspectionAssessments()
        {
            SurveyorDocsSiteInspection.OpenSurveyorDocsOnSiteInspection();
        }
        //Verify Closed Documents on Site Inspection
        [Then(@"I close docs on SiteInspection assessments")]
        public void ThenICloseDocsOnSiteInspectionAssessments()
        {
            SurveyorDocsSiteInspection.CloseSurveyorDocsOnSiteInspection();
        }
        //Verify Verify Closed Documents Documents on Site Inspection
        [Then(@"I should verify the closed SiteInspection documents")]
        public void ThenIShouldVerifyTheClosedSiteInspectionDocuments()
        {
            SurveyorDocsSiteInspection.VerifyClosedSurveyorDocsOnSiteInspection();
        }       
        [Then(@"I should complete the surveyor assessments")]
        public void ThenIShouldCompleteTheSurveyorAssessments()
        {
            Debug.WriteLine("Surveyor Assessments Are Submitted Successfully");
        }
        [When(@"I login to surveyor and close all documents if they open already")]
        public void WhenILoginToSurveyorAndCloseAllDocumentsIfTheyOpenAlready()
        {
            SurveyorAssessments.SubmitSurveyorAssessments();
        }
    }
}

