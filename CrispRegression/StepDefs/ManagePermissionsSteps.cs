﻿using CrispAutomation.Pages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using TechTalk.SpecFlow;
using System.Threading;
using OpenQA.Selenium;

namespace CrispAutomation.StepDefs
{
    [Binding]
    public class ManagePermissionsSteps : Support.Pages
    {     

        [Given(@"as a admin remove all permissions on user")]
        public void GivenAsAAdminRemoveAllPermissionsOnUser()
        {
            DBConnection.RemovePermissionsOnUser();
        }

        [Then(@"I should verify user not able to perform any operations")]
        public void ThenIShouldVerifyUserNotAbleToPerformAnyOperations()
        {
            
           
        }

    }
}
