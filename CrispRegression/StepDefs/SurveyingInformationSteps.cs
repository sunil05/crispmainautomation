﻿using CrispAutomation.Pages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TechTalk.SpecFlow;

namespace CrispAutomation.StepDefs
{
    [Binding]
    public class SurveyingInformationSteps : Support.Pages
    {
        [When(@"I set not required to all the assessment")]
        public void WhenISetNotRequiredToAllTheAssessment()
        {
            SurveyorInformationPage.SetNotRequiredOnAssessments();
        }
        [When(@"I verify all the assessments set to not required on surveyor page")]
        public void WhenIVerifyAllTheAssessmentsSetToNotRequiredOnSurveyorPage()
        {
            SurveyorInformationPage.VerifySiteAssessmentStatus();
        }

    }
}
