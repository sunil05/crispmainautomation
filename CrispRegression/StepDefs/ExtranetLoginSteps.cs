﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TechTalk.SpecFlow;

namespace CrispAutomation.StepDefs
{
    [Binding]
    public class ExtranetLoginSteps : Support.Pages
    {
        [Given(@"I have extranet admin permissions")]
        public void GivenIHaveExtranetAdminPermissions()
        {
           //DBConnection.ExtranetAdminPermissions();
        }
        [Given(@"I Login to LABC Extranet Site")]
        public void GivenILoginToLABCExtranetSite()
        {
            ExtranetLoginPage.ExtranetLABCLogin();
        }
        [Given(@"I Login to PG Extranet Site")]
        public void GivenILoginToPGExtranetSite()
        {
            ExtranetLoginPage.ExtranetPGLogin();
        }
        [When(@"I click on PG extranet training guide on homepage")]
        public void WhenIClickOnPGExtranetTrainingGuideOnHomepage()
        {
            ExtranetHomePage.VerifyDownloadPGTrainingGuideOnHomePage();
        }
        [When(@"Verify all the links")]
        public void WhenVerifyAllTheLinks()
        {
            ExtranetHomePage.VerifyMainPageLinks();
        }
        [Then(@"I Should Logout From Extranet Site")]
        public void ThenIShouldLogoutFromExtranetSite()
        {
            ExtranetLoginPage.ExtranetLogOut();
        }

        [When(@"I select extranet site")]
        public void WhenISelectExtranetSite()
        {
            ExtranetLoginPage.SelectExtranetSite();
        }


    }
}
