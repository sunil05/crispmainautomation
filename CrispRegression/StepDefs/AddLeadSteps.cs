﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using CrispAutomation.Features;
using CrispAutomation.Pages;
using CrispAutomation.Support;
using NUnit.Framework;
using TechTalk.SpecFlow;

namespace CrispAutomation.StepDefs
{
    [Binding]
    public class AddLeadSteps : Support.Pages
    {

        [When(@"I click on the plus button")]
        public void WhenIClickOnThePlusButton()
        {
            WaitForElement(Dashboardpage.PlusButton);
            Dashboardpage.PlusButton.Click();

        }

        [When(@"I select lead option to create lead")]
        public void WhenISelectLeadOptionToCreateLead()
        {
            WaitForElement(Dashboardpage.LeadOption);
            Dashboardpage.LeadOption.Click();
            CloseSpinneronDiv();
            CloseSpinneronPage();
        }
        [When(@"I provide pg details on the development information page")]
        public void WhenIProvidePgDetailsOnTheDevelopmentInformationPage()
        {
            AddLeadPage.PGDevelopmentInformationDetails();
        }
        [When(@"I provide labc details on the development information page")]
        public void WhenIProvideLabcDetailsOnTheDevelopmentInformationPage()
        {
            AddLeadPage.LABCDevelopmentInformationDetails();
        }
        [When(@"I provide details on products details page")]
        public void WhenIProvideDetailsOnProductsDetailsPage()
        {
            AddLeadPage.LVSProductsDetails();
        }
        [When(@"I provide high value details on products details page")]
        public void WhenIProvideHighValueDetailsOnProductsDetailsPage()
        {
            AddLeadPage.HVSProductsDetails();
        }
        [When(@"I provide details on the roles page")]
        public void WhenIProvideDetailsOnTheRolesPage()
        {
            AddLeadPage.RolesDetails();
        }
        [When(@"I provide details on other details page")]
        public void WhenIProvideDetailsOnOtherDetailsPage()
        {
            AddLeadPage.OtherDetails();
        }
        [When(@"I save the lead details")]
        public void WhenISaveTheLeadDetails()
        {
            WaitForElement(AddLeadPage.SaveButton);
            AddLeadPage.SaveButton.Click();
        }   
        [Then(@"I should see PG Lead details on dashboard page")]
        public void ThenIShouldSeePGLeadDetailsOnDashboardPage()
        {
            WaitForElement(Dashboardpage.LeadImage);
            Assert.True(Dashboardpage.LeadImage.Displayed);
            Console.Write("PGLead is added and displayed on the dashboard");
            //Thread.Sleep(500);
            var LeadRef = AddLABCQuotePage.SiteRefDetails.Text.Replace(" - Unqualified", "");
            ExtensionMethods.CreateSpreadsheet(LeadRef);
            Statics.SiteRefNumber = LeadRef;
            Dashboardpage.LeadGuidId();
        }   
       
        [Then(@"I should see LABC lead details on the dashboard")]
        public void ThenIShouldSeeLABCLeadDetailsOnTheDashboard()
        {
            WaitForElement(Dashboardpage.LeadImage);
            Assert.True(Dashboardpage.LeadImage.Displayed);
            //Thread.Sleep(1500);
            Assert.True(Dashboardpage.LeadUnqualifiedState.Displayed);
            Console.Write("LABC Lead is added and displayed on the dashboard");
        }

        //Rejecting Lead 
        [When(@"I click on reject button on dashboard")]
        public void WhenIClickOnRejectButtonOnDashboard()
        {
            //Thread.Sleep(1000);
            WaitForElement(Dashboardpage.ActionsButton);
            //Thread.Sleep(500);
            Dashboardpage.ActionsButton.Click();
            //Thread.Sleep(500);
            WaitForElement(RejectLeadPage.RejectButton);
            RejectLeadPage.RejectButton.Click();
            //Thread.Sleep(1000);

        }

        [When(@"I enter the details for rejection on rejection dialogue window")]
        public void WhenIEnterTheDetailsForRejectionOnRejectionDialogueWindow()
        {
            WaitForElement(RejectLeadPage.RejectionReasonLabel);
            RejectLeadPage.RejectingLead();
        }

        [When(@"I select reject option on rejection dialogue window")]
        public void WhenISelectRejectOptionOnRejectionDialogueWindow()
        {
            //Thread.Sleep(500);
            WaitForElement(RejectLeadPage.RejectOptionButton);
            RejectLeadPage.RejectOptionButton.Click();
            //Thread.Sleep(1000);
            CloseSpinneronDiv();
            CloseSpinneronPage();
        }

        [Then(@"I should see rejected lead on the dashboard")]
        public void ThenIShouldSeeRejectedLeadOnTheDashboard()
        {
            WaitForElement(Dashboardpage.LeadImage);
            Assert.True(Dashboardpage.LeadImage.Displayed);
            //Thread.Sleep(1000);
            CloseSpinneronDiv();
            Assert.True(Dashboardpage.LeadRejectedState.Displayed);
            Console.Write("LABC Lead is added and displayed on the dashboard");
        }

        //ReInstate the lead
        [When(@"I click on reinstate button on dashboard")]

        public void WhenIClickOnReinstateButtonOnDashboard()
        {
            //Thread.Sleep(1000);
            WaitForElement(Dashboardpage.ActionsButton);
            //Thread.Sleep(500);
            Dashboardpage.ActionsButton.Click();
            //Thread.Sleep(500);
            WaitForElement(ReInstateLeadPage.ReInstateButton);
            //Thread.Sleep(500);
            ReInstateLeadPage.ReInstateButton.Click();
            //Thread.Sleep(1000);

        }      

        [When(@"I enter the notes details on reinstate dialogue window")]
        public void WhenIEnterTheNotesDetailsOnReinstateDialogueWindow()
        {
            WaitForElement(ReInstateLeadPage.NotesLabel);
            ReInstateLeadPage.ReInstateLead();
        }
        [When(@"I click on reinstate option on reinstate dialogue window")]
        public void WhenIClickOnReinstateOptionOnReinstateDialogueWindow()
        {
            //Thread.Sleep(500);
            WaitForElement(ReInstateLeadPage.ReInstateOptionButton);
            ReInstateLeadPage.ReInstateOptionButton.Click();
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(1000);
        }

        [Then(@"I should see unqualified lead on the dashboard")]
        public void ThenIShouldSeeUnqualifiedLeadOnTheDashboard()
        {
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(1000);
            WaitForElement(Dashboardpage.LeadImage);
            Assert.True(Dashboardpage.LeadImage.Displayed);
            //Thread.Sleep(1000);
            CloseSpinneronDiv();
            Assert.True(Dashboardpage.LeadUnqualifiedState.Displayed);
            Console.Write("LABC Lead is added and displayed on the dashboard");
        }
        //Convert Lead into quote
        [When(@"I click on actions button to select quote application")]
        public void WhenIClickOnActionsButtonToSelectQuoteApplication()
        {
            //Thread.Sleep(1000);
            WaitForElement(Dashboardpage.ActionsButton);
            //Thread.Sleep(500);
            Dashboardpage.ActionsButton.Click();
            //Thread.Sleep(500);
            WaitForElement(Dashboardpage.QuoteApplicationButton);
            //Thread.Sleep(500);
            Dashboardpage.QuoteApplicationButton.Click();
            //Thread.Sleep(1000);
            WaitForElement(AddLABCQuotePage.AddNewQuoteDiv);
            //Thread.Sleep(500);
            Assert.IsTrue(AddLABCQuotePage.AddNewQuoteDiv.Displayed);
        }

        //Shortlisting Lead 
        [When(@"I perform operation to shortlist the lead")]
        public void WhenIPerformOperationToShortlistTheLead()
        {
            ShortListLeadPage.ShortListMethod();
        }
    }
}
