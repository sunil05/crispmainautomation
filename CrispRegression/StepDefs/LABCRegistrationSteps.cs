﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using TechTalk.SpecFlow;

namespace CrispAutomation.StepDefs
{
    [Binding]
    public class LABCRegistrationSteps:Support.Pages
    {
        [When(@"I select LABC registration tab")]
        public void WhenISelectLABCRegistrationTab()
        {
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(500);
            WaitForElement(LABCRegistrations.LABCRegistrationTab);
            LABCRegistrations.LABCRegistrationTab.Click();
            //Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(500);
            if (LABCRegistrations.CreateRegistrationButton.Count > 0)
            {
                WaitForElement(LABCRegistrations.CreateRegistrationButton[0]);
                LABCRegistrations.CreateRegistrationButton[0].Click();
                //Thread.Sleep(500);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                //Thread.Sleep(500);
            }
            WaitForElement(LABCRegistrations.RegistrationReference);
            var RegReference = LABCRegistrations.RegistrationReference.Text;
            var RegRefNumber = RegReference.Replace("Reference", "");
            //Thread.Sleep(1000);
        }

    }
}
