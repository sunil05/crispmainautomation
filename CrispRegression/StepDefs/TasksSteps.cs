﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TechTalk.SpecFlow;

namespace CrispAutomation.StepDefs
{
    [Binding]
    public sealed class TasksSteps : Support.Pages
    {
      
        [Given(@"I have submit a quote using following '(.*)'")]
        public void GivenIHaveSubmitAQuoteUsingFollowing(string Plotdata)
        {
            AddLABCQuotePage.SubmitLABCQuote(Plotdata);
        }
        [Given(@"I have submit a PG quote using following '(.*)'")]
        public void GivenIHaveSubmitAPGQuoteUsingFollowing(string Plotdata)
        {
            AddPGQuotePage.SubmitPGQuote(Plotdata);
        }
        [Then(@"I verify the tasks are created in submitted stage")]
        public void ThenIVerifyTheTasksAreCreatedInSubmittedStage()
        {
            TasksPage.TasksCreatedWhenSubmitAQuote();
        }
        [When(@"I convert the application into quoted state")]
        public void WhenIConvertTheApplicationIntoQuotedState()
        {
            SendQuotePage.SendquoteMainMethod();
        }
        [Then(@"I should verify the created tasks in quoted stage")]
        public void ThenIShouldVerifyTheCreatedTasksInQuotedStage()
        {
            TasksPage.TasksCreatedWhenSendAQuote();
        }
        [Then(@"I should  verify the closed tasks in quoted stage")]
        public void ThenIShouldVerifyTheClosedTasksInQuotedStage()
        {
            TasksPage.TasksClosedWhenSendAQuote();
        }
        [When(@"I convert the quote into order")]
        public void WhenIConvertTheQuoteIntoOrder()
        {
            AcceptQuotePage.AcceptQuoteMain();
        }
        [Then(@"I should verify the created tasks in order stage")]
        public void ThenIShouldVerifyTheCreatedTasksInOrderStage()
        {
            TasksPage.TasksCreatedWhenAcceptAQuote();
        }
        [Then(@"I should verify the closed tasks in order stage")]
        public void ThenIShouldVerifyTheClosedTasksInOrderStage()
        {
            TasksPage.TasksClosedWhenAcceptAQuote();
        }
        [When(@"I send intial notice")]
        public void WhenISendIntialNotice()
        {
            SendIntialNotice.SendIntialNoticeMethod();
        }       
        [Then(@"I should verify the created tasks on intial notice stage")]
        public void ThenIShouldVerifyTheCreatedTasksOnIntialNoticeStage()
        {
            TasksPage.TasksCreatedWhenIntialNoticeSent();
        }
        [Then(@"I should verify the closed taks  on intial notice stage")]
        public void ThenIShouldVerifyTheClosedTaksOnIntialNoticeStage()
        {
            TasksPage.TasksClosedWhenIntialNoticeSent();
        }
        [When(@"I select respond to initial notice")]
        public void WhenISelectRespondToInitialNotice()
        {
            SendIntialNotice.RespondtoIntialNoticeMethod();
        }
        [Then(@"I should verify the created tasks on intial notice response stage")]
        public void ThenIShouldVerifyTheCreatedTasksOnIntialNoticeResponseStage()
        {
            TasksPage.TasksCreatedWhenIntialNoticeAccepted();
        }
        [Then(@"I should verify the closed taks  on intial notice response stage")]
        public void ThenIShouldVerifyTheClosedTaksOnIntialNoticeResponseStage()
        {
            TasksPage.TasksClosedWhenIntialNoticeAccepted();
        }
        [When(@"I submit site risk assessment")]
        public void WhenISubmitSiteRiskAssessment()
        {
            SiteRiskAssessmentPage.SubmitSiteRiskAssessmentMethod();
        }
        [Then(@"I should verify the created tasks when site risk assessment submit")]
        public void ThenIShouldVerifyTheCreatedTasksWhenSiteRiskAssessmentSubmit()
        {
            if (Dashboardpage.DecisionButton.Count <= 0)
            {
                Dashboardpage.SelectOrder();
            }

            TasksPage.TasksCreatedWhenSRASubmit();
        }
        [Then(@"I should verify the closed taks  when site risk assessment submit")]
        public void ThenIShouldVerifyTheClosedTaksWhenSiteRiskAssessmentSubmit()
        {
            if (Dashboardpage.DecisionButton.Count <= 0)
            {
                Dashboardpage.SelectOrder();
            }
            TasksPage.TasksClosedWhenSRASubmitted();
        }
        [When(@"I submit design review assessment")]
        public void WhenISubmitDesignReviewAssessment()
        {
            DesignReviewPage.SubmitDesignReviewAssessmentMethod();
        }
        [Then(@"I should verify the created tasks when design review assessment submit")]
        public void ThenIShouldVerifyTheCreatedTasksWhenDesignReviewAssessmentSubmit()
        {
            if (Dashboardpage.DecisionButton.Count <= 0)
            {
                Dashboardpage.SelectOrder();
            }
            TasksPage.TasksCreatedWhenDRSubmit();
        }

        [Then(@"I should verify the closed taks  when design review assessment submit")]
        public void ThenIShouldVerifyTheClosedTaksWhenDesignReviewAssessmentSubmit()
        {
            if (Dashboardpage.DecisionButton.Count <= 0)
            {
                Dashboardpage.SelectOrder();
            }
            TasksPage.TasksClosedWhenDRSubmitted();
        }
        [When(@"I submit engineer review assessment")]
        public void WhenISubmitEngineerReviewAssessment()
        {
            EngineerReviewPage.SubmitEngineerReviewAssessmentMethod();
        }

        [Then(@"I should verify the created tasks when engineer review assessment submit")]
        public void ThenIShouldVerifyTheCreatedTasksWhenEngineerReviewAssessmentSubmit()
        {
            if (Dashboardpage.DecisionButton.Count <= 0)
            {
                Dashboardpage.SelectOrder();
            }
            TasksPage.TasksCreatedWhenERSubmit();
        }

        [Then(@"I should verify the closed taks  when engineer review assessment submit")]
        public void ThenIShouldVerifyTheClosedTaksWhenEngineerReviewAssessmentSubmit()
        {
            if (Dashboardpage.DecisionButton.Count <= 0)
            {
                Dashboardpage.SelectOrder();
            }
            TasksPage.TasksClosedWhenERSubmitted();
        }
        [When(@"I submit site inspection assessment")]
        public void WhenISubmitSiteInspectionAssessment()
        {
            SiteInspectionPage.SubmitSiteInspectionMethod();
        }
        [Then(@"I should verify the created tasks when site inspection assessment submit")]
        public void ThenIShouldVerifyTheCreatedTasksWhenSiteInspectionAssessmentSubmit()
        {
            if (Dashboardpage.DecisionButton.Count <= 0)
            {
                Dashboardpage.SelectOrder();
            }
            TasksPage.TasksCreatedWhenInspectionSubmit();
        }
        [Then(@"I should verify the closed taks  when site inspection assessment submit")]
        public void ThenIShouldVerifyTheClosedTaksWhenSiteInspectionAssessmentSubmit()
        {
            if (Dashboardpage.DecisionButton.Count <= 0)
            {
                Dashboardpage.SelectOrder();
            }
            TasksPage.TasksClosedWhenInspectionSubmitted();
        }


        [When(@"I send final notice")]
        public void WhenISendFinalNotice()
        {
            ConditionsPage.MakeOrderPayment();
            FinalNoticePage.SendFinalNotice();
        }

        [Then(@"I should verify the created tasks on final notice stage")]
        public void ThenIShouldVerifyTheCreatedTasksOnFinalNoticeStage()
        {           
            TasksPage.TasksCreatedWhenFinalNoticeSent();
        }

        [Then(@"I should verify the closed taks  on final notice stage")]
        public void ThenIShouldVerifyTheClosedTaksOnFinalNoticeStage()
        {
            TasksPage.TasksClosedWhenFinalNoticeSent();
        }      
     
        [Then(@"I should verify the created tasks on issue COI")]
        public void ThenIShouldVerifyTheCreatedTasksOnIssueCOI()
        {
            TasksPage.TasksCreatedWhenCOISent();
        }

        [Then(@"I should verify the closed taks  on issue COI")]
        public void ThenIShouldVerifyTheClosedTaksOnIssueCOI()
        {
            TasksPage.TasksClosedWhenCOISent();
        }

        [When(@"I rescind the COI")]
        public void WhenIRescindTheCOI()
        {
            RescindProcessPage.RescindCOI();
        }

        [Then(@"I should verify the created tasks on Rescind COI")]
        public void ThenIShouldVerifyTheCreatedTasksOnRescindCOI()
        {
            TasksPage.TasksCreatedWhenRescindCOI();
        }

        [When(@"I rescind the other activites")]
        public void WhenIRescindTheOtherActivites()
        {
            RescindProcessPage.RescindPlotIC();
            RescindProcessPage.RescindBCSignOff();
            RescindProcessPage.RescindFinalNotice();
            RescindProcessPage.RescindIntialNotice();
            RescindProcessPage.RescindCOA();
        }

        [Then(@"I should verify created tasks on rescind process")]
        public void ThenIShouldVerifyCreatedTasksOnRescindProcess()
        {
            TasksPage.TasksCreatedWhenRescindActivities();
        }
        [When(@"I create the referrals")]
        public void WhenICreateTheReferrals()
        {
            ReferralsPage.CreateReferrals();
        }
        [Then(@"I should verify tasks created on referral")]
        public void ThenIShouldVerifyTasksCreatedOnReferral()
        {
            TasksPage.TasksCreatedWhenReferralsCreated();
        }

        [When(@"I close the referrals")]
        public void WhenICloseTheReferrals()
        {
            ReferralsPage.CloseAllReferrals();
        }

        [Then(@"I should verify tasks closed on referral")]
        public void ThenIShouldVerifyTasksClosedOnReferral()
        {
            TasksPage.TasksClosedWhenReferralClosed();
        }
    }
}
