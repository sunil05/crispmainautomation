﻿using OpenQA.Selenium.Chrome;
using Protractor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TechTalk.SpecFlow;

namespace CrispAutomation.StepDefs
{
    [Binding]
    public sealed class ExtranetLABCQuotesSteps : Support.Pages
    {

        [When(@"I click on quote records on home page")]
        public void WhenIClickOnQuoteRecordsOnHomePage()
        {
            ExtranetQuotesPage.VerifyQuotesRecordsFromHomePage();
        }

        [When(@"I should navigate to quotes page")]
        public void WhenIShouldNavigateToQuotesPage()
        {
            ExtranetQuotesPage.VerifyQuoteTable();
        }

        [When(@"I click on quote records on quotes page")]
        public void WhenIClickOnQuoteRecordsOnQuotesPage()
        {
            ExtranetQuotesPage.VerifyQuotesRecordsFromQuotesPage();
        }


    }
}
