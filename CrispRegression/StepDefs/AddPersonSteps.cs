﻿using System;
using System.Threading;
using CrispAutomation.Pages;
using NUnit.Framework;
using OpenQA.Selenium;
using TechTalk.SpecFlow;

namespace CrispAutomation.StepDefs
{
    [Binding]
    public class AddPersonSteps:Support.Pages
    {
        [When(@"I click on add button to create a person")]
        public void WhenIClickOnAddButtonToCreateAPerson()
        {
            WaitForElement(Dashboardpage.PlusButton);
            Dashboardpage.PlusButton.Click();
        }
        [When(@"Select Person Option")]
        public void WhenSelectPersonOption()
        {
            WaitForElement(Dashboardpage.PersonButton);
            Dashboardpage.PersonButton.Click();
            //Thread.Sleep(500);
        }
        [When(@"I enter person details on details page")]
        public void WhenIEnterPersonDetailsOnDetailsPage()
        {
            WaitForElement(AddPersonpage.Salutation);
            AddPersonpage.FillDetails();
        }
        [When(@"I enter address details on addresses page")]
        public void WhenIEnterAddressDetailsOnAddressesPage()
        {
            WaitForElement(AddPersonpage.AddressButton);
            AddPersonpage.AddAddress();
        }
        [When(@"I enter socialmedia details on social page")]
        public void WhenIEnterSocialmediaDetailsOnSocialPage()
        {
            WaitForElement(AddPersonpage.FaceBookId);
            AddPersonpage.AddSocialInfo();
        }
        [When(@"I click on save button to save person details")]
        public void WhenIClickOnSaveButtonToSavePersonDetails()
        {
            WaitForElement(AddPersonpage.SaveButton);
            AddPersonpage.SaveButton.Click();
            //Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(500);
        }
        [Then(@"I should see person details on the dashboard")]
        public void ThenIShouldSeePersonDetailsOnTheDashboard()
        {
           
            WaitForElement(AddPersonpage.Imageperson);
            //Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(500);
            Assert.IsTrue(AddPersonpage.Imageperson.Displayed);
        }
        [When(@"I click on edit button to edit person details")]
        public void WhenIClickOnEditButtonToEditPersonDetails()
        {

            WaitForElement(EditPersonDetailsPage.EditPersonButton);
            EditPersonDetailsPage.EditPersonButton.Click();

        }
        [When(@"I edit the person details on details page")]
        public void WhenIEditThePersonDetailsOnDetailsPage()
        {
            WaitForElement(EditPersonDetailsPage.SalutationFieldEdit);
            EditPersonDetailsPage.EditPersonetails();
        }
        [When(@"I edit the existing address details and add new address details")]
        public void WhenIEditTheExistingAddressDetailsAndAddNewAddressDetails()
        {
            WaitForElement(EditPersonDetailsPage.DefaultAddress);
            EditPersonDetailsPage.EditAddressDetails();
            WaitForElement(EditPersonDetailsPage.AddressButton);
            EditPersonDetailsPage.AddNewAddressDetails();
        }
        [When(@"I edit the socialmedia details on social page")]
        public void WhenIEditTheSocialmediaDetailsOnSocialPage()
        {
            WaitForElement(EditPersonDetailsPage.NextButton);
            EditPersonDetailsPage.NextButton.Click();
            //Thread.Sleep(500);
            WaitForElement(EditPersonDetailsPage.EditFacebookInput);
            //Thread.Sleep(500);
            EditPersonDetailsPage.EditSocialInfo();
        }

        [When(@"I click on save button to update the person details")]
        public void WhenIClickOnSaveButtonToUpdateThePersonDetails()
        {
            WaitForElement(EditPersonDetailsPage.SaveButton);
            WaitForElement(EditPersonDetailsPage.SaveButton);
            EditPersonDetailsPage.SaveButton.Click();
            //Thread.Sleep(1000);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(500);
        }
        [Then(@"I should see updated contact details on the dashboard")]
        public void ThenIShouldSeeUpdatedContactDetailsOnTheDashboard()
        {
            WaitForElement(EditPersonDetailsPage.Imageperson);
            //Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(500);   
            Assert.IsTrue(EditPersonDetailsPage.Imageperson.Displayed);
        }


        [When(@"I click on rating option to provide rating to a person")]
        public void WhenIClickOnRatingOptionToProvideRatingToAPerson()
        {
            WaitForElement(AddRatingToPersonPage.RatingOption);
            AddRatingToPersonPage.RatingOption.Click();
            //Thread.Sleep(500);
        }

        [When(@"I select add rating button on person page from top right corner")]
        public void WhenISelectAddRatingButtonOnPersonPageFromTopRightCorner()
        {
            WaitForElement(AddRatingToPersonPage.AddRatingButton);
            AddRatingToPersonPage.AddRatingButton.Click();
            //Thread.Sleep(500);
        }
        [When(@"I provide the details on details page")]
        public void WhenIProvideTheDetailsOnDetailsPage()
        {
            WaitForElement(AddRatingToPersonPage.RatingDialogue);
            WaitForElement(AddRatingToPersonPage.DetailsOption);
            AddRatingToPersonPage.DetailsPage();
        }
        [When(@"I provide the details on warranty providers page")]
        public void WhenIProvideTheDetailsOnWarrantyProvidersPage()
        {
            WaitForElement(AddRatingToPersonPage.WarrentyProvider);
            AddRatingToPersonPage.WarrantyDetails();
        }
        [When(@"I provide the details on developments page")]
        public void WhenIProvideTheDetailsOnDevelopmentsPage()
        {
            WaitForElement(AddRatingToPersonPage.Developments);
            AddRatingToPersonPage.DevelopmentDetails();
        }
        [When(@"I provide the details on prospective business page")]
        public void WhenIProvideTheDetailsOnProspectiveBusinessPage()
        {
            WaitForElement(AddRatingToPersonPage.ProspectiveBusiness);
            AddRatingToPersonPage.ProspectiveBusinessDetails();           
        }
        [When(@"I provide the details on claims history page")]
        public void WhenIProvideTheDetailsOnClaimsHistoryPage()
        {
            WaitForElement(AddRatingToPersonPage.ClaimHistory);
            AddRatingToPersonPage.ClaimsHistoryDetails();          
        }
        [When(@"I provide the details on scores details page")]
        public void WhenIProvideTheDetailsOnScoresDetailsPage()
        {
            WaitForElement(AddRatingToPersonPage.ScoresOption);
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            AddRatingToPersonPage.ScoresDetails();
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
        }
        [When(@"I click on the confirm rating button on the wizard")]
        public void WhenIClickOnTheConfirmRatingButtonOnTheWizard()
        {
            WaitForElement(AddRatingToPersonPage.ConfirmRatingButton);
            AddRatingToPersonPage.ConfirmRatingButton.Click();           
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(1000);
        }
        [Then(@"I should see the rating details for person")]
        public void ThenIShouldSeeTheRatingDetailsForPerson()
        {
            WaitForElement(AddRatingToPersonPage.Imageperson);
            Assert.IsTrue(AddRatingToPersonPage.Imageperson.Displayed);
            try
            {
                if (!AddRatingToPersonPage.RatingSummaryDisplayed.Displayed)
                {
                    // Driver.Navigate().Refresh();
                    WaitForElement(AddRatingToPersonPage.RatingOption);
                    AddRatingToPersonPage.RatingOption.Click();
                    WaitForElement(AddRatingToPersonPage.RatingSummaryDisplayed);
                    Assert.IsTrue(AddRatingToPersonPage.RatingSummaryDisplayed.Displayed);
                }
                else
                {
                    WaitForElement(AddRatingToPersonPage.RatingSummaryDisplayed);
                    Assert.IsTrue(AddRatingToPersonPage.RatingSummaryDisplayed.Displayed);

                }
            }
            catch (NoSuchElementException)
            {
                // Driver.Navigate().Refresh();
                WaitForElement(AddRatingToPersonPage.RatingOption);
                AddRatingToPersonPage.RatingOption.Click();
                WaitForElement(AddRatingToPersonPage.RatingSummaryDisplayed);
                Assert.IsTrue(AddRatingToPersonPage.RatingSummaryDisplayed.Displayed);
            }
        }

        //Edit Rating To A Person

        [When(@"I click on rating option to edit rating details to person")]
        public void WhenIClickOnRatingOptionToEditRatingDetailsToPerson()
        {
            WaitForElement(EditRatingToPersonPage.RatingOption);
            EditRatingToPersonPage.RatingOption.Click();
            //Thread.Sleep(500);
        }

        [When(@"I select edit rating button on person rating page")]
        public void WhenISelectEditRatingButtonOnPersonRatingPage()
        {
            WaitForElement(EditRatingToPersonPage.EditRating);
            //Thread.Sleep(500);
            EditRatingToPersonPage.EditRating.Click();
            WaitForElement(EditRatingToPersonPage.RatingDialogue);
            Assert.IsTrue(EditRatingToPersonPage.RatingDialogue.Displayed);
            //Thread.Sleep(500);
        }

        [When(@"I edit the details on rating details page")]
        public void WhenIEditTheDetailsOnRatingDetailsPage()
        {
            WaitForElement(EditRatingToPersonPage.DetailsOption);
            EditRatingToPersonPage.EditDetailsPage();
        }
        [When(@"I edit the details on warranty providers page")]
        public void WhenIEditTheDetailsOnWarrantyProvidersPage()
        {
            WaitForElement(EditRatingToPersonPage.WarrentyProvider);
            EditRatingToPersonPage.WarrantyDetails();         
        }

        [When(@"I edit the details on developments page")]
        public void WhenIEditTheDetailsOnDevelopmentsPage()
        {
            WaitForElement(EditRatingToPersonPage.Developments);
            EditRatingToPersonPage.EditDevelopmentDetails();
            WaitForElement(EditRatingToPersonPage.Developments);
            EditRatingToPersonPage.NewDevelopmentDetails();           
        }
        [When(@"I edit the details on prospective business page")]
        public void WhenIEditTheDetailsOnProspectiveBusinessPage()
        {
            WaitForElement(EditRatingToPersonPage.ProspectiveBusiness);
            EditRatingToPersonPage.EditProspectiveBusinessDetails();
            WaitForElement(EditRatingToPersonPage.ProspectiveBusiness);
            EditRatingToPersonPage.NewProspectiveBusinessDetails();           
        }
        [When(@"I edit the details on claims history page")]
        public void WhenIEditTheDetailsOnClaimsHistoryPage()
        {
            WaitForElement(EditRatingToPersonPage.ClaimHistory);
            EditRatingToPersonPage.EditClaimsHistoryDetails();
            WaitForElement(EditRatingToPersonPage.AddHistoricClaimsButton);
            EditRatingToPersonPage.NewClaimsHistoryDetails();

        }
        [When(@"I edit the details on scores details page")]
        public void WhenIEditTheDetailsOnScoresDetailsPage()
        {

            WaitForElement(EditRatingToPersonPage.ScoresOption);
            EditRatingToPersonPage.EditScoresDetails();
        }
        [When(@"I click on the confirm rating button to update amended rating details")]
        public void WhenIClickOnTheConfirmRatingButtonToUpdateAmendedRatingDetails()
        {
            WaitForElement(AddRatingToPersonPage.ConfirmRatingButton);
            AddRatingToPersonPage.ConfirmRatingButton.Click();
            //Thread.Sleep(1000);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(500);
            WaitForElement(AddRatingToPersonPage.Imageperson);
            Assert.IsTrue(AddRatingToPersonPage.Imageperson.Displayed);
            //Thread.Sleep(1000);
            WaitForElement(AddRatingToPersonPage.RatingOption);
            AddRatingToPersonPage.RatingOption.Click();
        }
        [Then(@"I should see updated rating details for the person")]
        public void ThenIShouldSeeUpdatedRatingDetailsForThePerson()
        {
            WaitForElement(EditRatingToPersonPage.RatingSummaryDisplayed);
            Assert.IsTrue(EditRatingToPersonPage.RatingSummaryDisplayed.Displayed);
            //Thread.Sleep(500);
        }
        //Searching for Contact            
        [When(@"I search for contacts")]
        public void WhenISearchForContacts()
        {           
            Dashboardpage.SearchButton.Click();
            WaitForElement(Dashboardpage.ContactOption);
            Dashboardpage.ContactOption.Click();
            WaitForElement(ManagePermissionsPage.IncludeCompaniesCheckbox);
            ManagePermissionsPage.IncludeCompaniesCheckbox.Click();
            WaitForElement(ManagePermissionsPage.IncludeEmployeesCheckbox);
            ManagePermissionsPage.IncludeEmployeesCheckbox.Click();
            WaitForElement(Contactsearchpage.NameField);
            Contactsearchpage.NameField.Click();
            Contactsearchpage.EnterName.Click();
            Contactsearchpage.EnterName.SendKeys("Sunil Sunkishala");
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
        }
        [Then(@"I should see contact details")]
        public void ThenIShouldSeeContactDetails()
        {
          
        }
    }
}
