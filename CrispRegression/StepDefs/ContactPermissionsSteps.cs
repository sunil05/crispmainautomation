﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TechTalk.SpecFlow;

namespace CrispAutomation.StepDefs
{
    [Binding]
    public class ContactPermissionsSteps : Support.Pages
    {       

        // Apply only Create Contact Permissions 
        [When(@"I apply permissions on creating contact")]
        public void WhenIApplyPermissionsOnCreatingContact()
        {
            ContactsPermissionsPage.ApplyCreatePersonPermissionsToUser();
        }

        [Then(@"I verify that contacts can be created not amended")]
        public void ThenIVerifyThatContactsCanBeCreatedNotAmended()
        {
            ContactsPermissionsPage.VerifyCreatingPersonWhenUserApplyPermissions();
        }
        // Apply only Amend Contact Permissions 
        [When(@"I apply permissions on amending contact")]
        public void WhenIApplyPermissionsOnAmendingContact()
        {
            ContactsPermissionsPage.ApplyAmendPersonPermissionsToUser();
        }

        [Then(@"I verify that contacts can be amended not created")]
        public void ThenIVerifyThatContactsCanBeAmendedNotCreated()
        {
            ContactsPermissionsPage.VerifyAmendingPersonWhenUserApplyPermissions();
        }
        // Apply only View Contact Permissions 
        [When(@"I apply permissions on view contact")]
        public void WhenIApplyPermissionsOnViewContact()
        {
            ContactsPermissionsPage.ApplySearchContactPermissionsToUser();
        }
        [Then(@"I verify that contacts can be search and viewed")]
        public void ThenIVerifyThatContactsCanBeSearchAndViewed()
        {
            ContactsPermissionsPage.VerifySearchContactWhenUserApplyPermissions();
        }
        // Apply amend company name Permissions 
        [When(@"I apply permissions on amend companyname")]
        public void WhenIApplyPermissionsOnAmendCompanyname()
        {
            ContactsPermissionsPage.ApplyAmendCompanyNamePermissionsToUser();
        }       

        [Then(@"I verify that companyname can be amended")]
        public void ThenIVerifyThatCompanynameCanBeAmended()
        {
            ContactsPermissionsPage.VerifyAmendCompanyNameWhenUserApplyPermissions();
        }
        // Apply set-non standard payment terms Permissions 
        [When(@"I apply permissions on set-non standard payment termms")]
        public void WhenIApplyPermissionsOnSet_NonStandardPaymentTermms()
        {
            
        }

        [Then(@"I verify that set-non standard payment terms can be applied")]
        public void ThenIVerifyThatSet_NonStandardPaymentTermsCanBeApplied()
        {
            ContactsPermissionsPage.VerifyContactOperations();
        }

    }
}
