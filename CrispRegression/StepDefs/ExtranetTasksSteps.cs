﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TechTalk.SpecFlow;

namespace CrispAutomation.StepDefs
{
    [Binding]
    public sealed class ExtranetTasksSteps :Support.Pages
    {
        [When(@"I respond to the documents")]
        public void WhenIRespondToTheDocuments()
        {
            
        }

        [Then(@"I should see the tasks on Crisp")]
        public void ThenIShouldSeeTheTasksOnCrisp()
        {
           
        }

    }
}
