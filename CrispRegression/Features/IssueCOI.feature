﻿Feature: IssueCOI
Background: 
    Given I am on login page
	When I enter crisp username 
	And I enter crisp password
	And I click on login button
	Then I should see all items displayed on dashboard page

#Issue COI for LABC NewHomes Product 
@Smoketest
Scenario Outline: Verify Issue COI on LABC NewHomes Product
When I click on plus button and select  quote option
And I read following '<plotdata>' details
And I provide details on key site details page
And I provide details on other site details page 
And I provide following '<plotdata>' data on plot schedule page
And I provide details on the product details page
And I provide details on additional questions page
And I select relavant roles on roles page
When I provide the details on declaration page
Then I click on complete button to see the application submitted details on the dashboard
#Send Quote 
When I click on sendquote button 
And I verify the details on send quote rating page
And I verify the details on send quote securities page
And I verify the details on send quote conditions page
And I verify the details on send quote special terms page
And I verify the details on send quote fees page
And I verify the details on send quote file review page
And I verify the details on send quote endorsements page
And I verify the details on send quote confirm details page
And I verify the details on send quote correspondence page
Then I click on send quote button to see quoted details on the dashboard
#Accept Quote
When I click on acceptquote button 
And I verify the details on accept quote products page
And I verify the details on accept quote conditions page
And I verify the details on accept quote file review page 
And I verify the details on accept quote confirm page 
And I verify the details on accept quote internal roles page 
And I verify the details on accept quote correspondence page 
Then I click on accept quote button to see order details on the dashboard
#Pay the Fee balance condition
When I verify account and make the balance payments
#Security Document condition and Registration fee condition
When I clear security document conditions 
#Registration fee condition
#When I clear registration fee conditions
#Manual Conditions created automatically on High Value product
When I clear manual conditions
#Clear additional Conditions
When I clear LABC additional conditions
#Surveyor Assessments 
When I login to surveyor site to submit surveyor assessments
#Issue The COI
When I login back to crisp application to issue the COI
Then I should download COI document and compare the document 


Examples: 
| plotdata                                                                                                                        |
| \\\tpgfile\NewCompany\IT\Department\QA Team\Current Projects\CRISP\CrispAutomation\CrispData\LABC-Products\NewHomes.xlsx        |

#Issue COI for LABC NewHomes HVS Product 
@Smoketest
Scenario Outline: Verify Issue COI on LABC NewHomes HVS Product
When I click on plus button and select  quote option
And I read following '<plotdata>' details
And I provide details on key site details page
And I provide details on other site details page 
And I provide following '<plotdata>' data on plot schedule page
And I provide details on the product details page
And I provide details on additional questions page
And I select relavant roles on roles page
When I provide the details on declaration page
Then I click on complete button to see the application submitted details on the dashboard
#Send Quote 
When I click on sendquote button 
And I verify the details on send quote rating page
And I verify the details on send quote securities page
And I verify the details on send quote conditions page
And I verify the details on send quote special terms page
And I verify the details on send quote fees page
And I verify the details on send quote file review page
And I verify the details on send quote endorsements page
And I verify the details on send quote confirm details page
And I verify the details on send quote correspondence page
Then I click on send quote button to see quoted details on the dashboard
#Accept Quote
When I click on acceptquote button 
And I verify the details on accept quote products page
And I verify the details on accept quote conditions page
And I verify the details on accept quote file review page 
And I verify the details on accept quote confirm page 
And I verify the details on accept quote internal roles page 
And I verify the details on accept quote correspondence page 
Then I click on accept quote button to see order details on the dashboard
#Pay the Fee balance condition
When I verify account and make the balance payments
#Security Document condition and Registration fee condition
When I clear security document conditions 
#Registration fee condition
#When I clear registration fee conditions
#Manual Conditions created automatically on High Value product
When I clear manual conditions
#Clear additional Conditions
When I clear LABC additional conditions
#Surveyor Assessments 
When I login to surveyor site to submit surveyor assessments
#Issue The COI
When I login back to crisp application to issue the COI
Then I should download COI document and compare the document 


Examples: 
| plotdata                                                                                                                        |
| \\\tpgfile\NewCompany\IT\Department\QA Team\Current Projects\CRISP\CrispAutomation\CrispData\LABC-Products\NewHomesHV.xlsx      |

#Issue COI for LABC Commercial Product 
@Smoketest
Scenario Outline: Verify Issue COI on LABC Commercial Product
When I click on plus button and select  quote option
And I read following '<plotdata>' details
And I provide details on key site details page
And I provide details on other site details page 
And I provide following '<plotdata>' data on plot schedule page
And I provide details on the product details page
And I provide details on additional questions page
And I select relavant roles on roles page
When I provide the details on declaration page
Then I click on complete button to see the application submitted details on the dashboard
#Send Quote 
When I click on sendquote button 
And I verify the details on send quote rating page
And I verify the details on send quote securities page
And I verify the details on send quote conditions page
And I verify the details on send quote special terms page
And I verify the details on send quote fees page
And I verify the details on send quote file review page
And I verify the details on send quote endorsements page
And I verify the details on send quote confirm details page
And I verify the details on send quote correspondence page
Then I click on send quote button to see quoted details on the dashboard
#Accept Quote
When I click on acceptquote button 
And I verify the details on accept quote products page
And I verify the details on accept quote conditions page
And I verify the details on accept quote file review page 
And I verify the details on accept quote confirm page 
And I verify the details on accept quote internal roles page 
And I verify the details on accept quote correspondence page 
Then I click on accept quote button to see order details on the dashboard
#Pay the Fee balance condition
When I verify account and make the balance payments
#Security Document condition and Registration fee condition
When I clear security document conditions 
#Registration fee condition
#When I clear registration fee conditions
#Manual Conditions created automatically on High Value product
When I clear manual conditions
#Clear additional Conditions
When I clear LABC additional conditions
#Surveyor Assessments 
When I login to surveyor site to submit surveyor assessments
#Issue The COI
When I login back to crisp application to issue the COI
Then I should download COI document and compare the document 


Examples: 
| plotdata                                                                                                                        |
| \\\tpgfile\NewCompany\IT\Department\QA Team\Current Projects\CRISP\CrispAutomation\CrispData\LABC-Products\Commercials.xlsx     |


#Issue COI for LABC Commercial HVS Product 
@Smoketest
Scenario Outline: Verify Issue COI on LABC Commercial HVS Product
When I click on plus button and select  quote option
And I read following '<plotdata>' details
And I provide details on key site details page
And I provide details on other site details page 
And I provide following '<plotdata>' data on plot schedule page
And I provide details on the product details page
And I provide details on additional questions page
And I select relavant roles on roles page
When I provide the details on declaration page
Then I click on complete button to see the application submitted details on the dashboard
#Send Quote 
When I click on sendquote button 
And I verify the details on send quote rating page
And I verify the details on send quote securities page
And I verify the details on send quote conditions page
And I verify the details on send quote special terms page
And I verify the details on send quote fees page
And I verify the details on send quote file review page
And I verify the details on send quote endorsements page
And I verify the details on send quote confirm details page
And I verify the details on send quote correspondence page
Then I click on send quote button to see quoted details on the dashboard
#Accept Quote
When I click on acceptquote button 
And I verify the details on accept quote products page
And I verify the details on accept quote conditions page
And I verify the details on accept quote file review page 
And I verify the details on accept quote confirm page 
And I verify the details on accept quote internal roles page 
And I verify the details on accept quote correspondence page 
Then I click on accept quote button to see order details on the dashboard
#Pay the Fee balance condition
When I verify account and make the balance payments
#Security Document condition and Registration fee condition
When I clear security document conditions 
#Registration fee condition
#When I clear registration fee conditions
#Manual Conditions created automatically on High Value product
When I clear manual conditions
#Clear additional Conditions
When I clear LABC additional conditions
#Surveyor Assessments 
When I login to surveyor site to submit surveyor assessments
#Issue The COI
When I login back to crisp application to issue the COI
Then I should download COI document and compare the document 


Examples: 
| plotdata                                                                                                                        |
| \\\tpgfile\NewCompany\IT\Department\QA Team\Current Projects\CRISP\CrispAutomation\CrispData\LABC-Products\CommercialsHV.xlsx   |

#Issue COI for LABC Private Rental Product 
@Smoketest
Scenario Outline: Verify Issue COI on LABC Private Rental Product
When I click on plus button and select  quote option
And I read following '<plotdata>' details
And I provide details on key site details page
And I provide details on other site details page 
And I provide following '<plotdata>' data on plot schedule page
And I provide details on the product details page
And I provide details on additional questions page
And I select relavant roles on roles page
When I provide the details on declaration page
Then I click on complete button to see the application submitted details on the dashboard
#Send Quote 
When I click on sendquote button 
And I verify the details on send quote rating page
And I verify the details on send quote securities page
And I verify the details on send quote conditions page
And I verify the details on send quote special terms page
And I verify the details on send quote fees page
And I verify the details on send quote file review page
And I verify the details on send quote endorsements page
And I verify the details on send quote confirm details page
And I verify the details on send quote correspondence page
Then I click on send quote button to see quoted details on the dashboard
#Accept Quote
When I click on acceptquote button 
And I verify the details on accept quote products page
And I verify the details on accept quote conditions page
And I verify the details on accept quote file review page 
And I verify the details on accept quote confirm page 
And I verify the details on accept quote internal roles page 
And I verify the details on accept quote correspondence page 
Then I click on accept quote button to see order details on the dashboard
#Pay the Fee balance condition
When I verify account and make the balance payments
#Security Document condition and Registration fee condition
When I clear security document conditions 
#Registration fee condition
#When I clear registration fee conditions
#Manual Conditions created automatically on High Value product
When I clear manual conditions
#Clear additional Conditions
When I clear LABC additional conditions
#Surveyor Assessments 
When I login to surveyor site to submit surveyor assessments
#Issue The COI
When I login back to crisp application to issue the COI
Then I should download COI document and compare the document 


Examples: 
| plotdata                                                                                                                        |
| \\\tpgfile\NewCompany\IT\Department\QA Team\Current Projects\CRISP\CrispAutomation\CrispData\LABC-Products\PrivateRental.xlsx   |


#Issue COI for LABC Private Rental HVS Product 
@Smoketest
Scenario Outline: Verify Issue COI on LABC Private Rental HVS Product
When I click on plus button and select  quote option
And I read following '<plotdata>' details
And I provide details on key site details page
And I provide details on other site details page 
And I provide following '<plotdata>' data on plot schedule page
And I provide details on the product details page
And I provide details on additional questions page
And I select relavant roles on roles page
When I provide the details on declaration page
Then I click on complete button to see the application submitted details on the dashboard
#Send Quote 
When I click on sendquote button 
And I verify the details on send quote rating page
And I verify the details on send quote securities page
And I verify the details on send quote conditions page
And I verify the details on send quote special terms page
And I verify the details on send quote fees page
And I verify the details on send quote file review page
And I verify the details on send quote endorsements page
And I verify the details on send quote confirm details page
And I verify the details on send quote correspondence page
Then I click on send quote button to see quoted details on the dashboard
#Accept Quote
When I click on acceptquote button 
And I verify the details on accept quote products page
And I verify the details on accept quote conditions page
And I verify the details on accept quote file review page 
And I verify the details on accept quote confirm page 
And I verify the details on accept quote internal roles page 
And I verify the details on accept quote correspondence page 
Then I click on accept quote button to see order details on the dashboard
#Pay the Fee balance condition
When I verify account and make the balance payments
#Security Document condition and Registration fee condition
When I clear security document conditions 
#Registration fee condition
#When I clear registration fee conditions
#Manual Conditions created automatically on High Value product
When I clear manual conditions
#Clear additional Conditions
When I clear LABC additional conditions
#Surveyor Assessments 
When I login to surveyor site to submit surveyor assessments
#Issue The COI
When I login back to crisp application to issue the COI
Then I should download COI document and compare the document 


Examples: 
| plotdata                                                                                                                        |
| \\\tpgfile\NewCompany\IT\Department\QA Team\Current Projects\CRISP\CrispAutomation\CrispData\LABC-Products\PrivateRentalHV.xlsx |


#Issue COI for LABC SelfBuild Product 
@Smoketest
Scenario Outline: Verify Issue COI on LABC SelfBuild Product
When I click on plus button and select  quote option
And I read following '<plotdata>' details
And I provide details on key site details page
And I provide details on other site details page 
And I provide following '<plotdata>' data on plot schedule page
And I provide details on the product details page
And I provide details on additional questions page
And I select relavant roles on roles page
When I provide the details on declaration page
Then I click on complete button to see the application submitted details on the dashboard
#Send Quote 
When I click on sendquote button 
And I verify the details on send quote rating page
And I verify the details on send quote securities page
And I verify the details on send quote conditions page
And I verify the details on send quote special terms page
And I verify the details on send quote fees page
And I verify the details on send quote file review page
And I verify the details on send quote endorsements page
And I verify the details on send quote confirm details page
And I verify the details on send quote correspondence page
Then I click on send quote button to see quoted details on the dashboard
#Accept Quote
When I click on acceptquote button 
And I verify the details on accept quote products page
And I verify the details on accept quote conditions page
And I verify the details on accept quote file review page 
And I verify the details on accept quote confirm page 
And I verify the details on accept quote internal roles page 
And I verify the details on accept quote correspondence page 
Then I click on accept quote button to see order details on the dashboard
#Pay the Fee balance condition
When I verify account and make the balance payments
#Security Document condition and Registration fee condition
When I clear security document conditions 
#Registration fee condition
#When I clear registration fee conditions
#Manual Conditions created automatically on High Value product
When I clear manual conditions
#Clear additional Conditions
When I clear LABC additional conditions
#Surveyor Assessments 
When I login to surveyor site to submit surveyor assessments
#Issue The COI
When I login back to crisp application to issue the COI
Then I should download COI document and compare the document 


Examples: 
| plotdata                                                                                                                        |
| \\\tpgfile\NewCompany\IT\Department\QA Team\Current Projects\CRISP\CrispAutomation\CrispData\LABC-Products\SelfBuild.xlsx       |


#Issue COI for LABC Completed Housing Product 
@Smoketest
Scenario Outline: Verify Issue COI on LABC Completed Housing Product
When I click on plus button and select  quote option
And I read following '<plotdata>' details
And I provide details on key site details page
And I provide details on other site details page 
And I provide following '<plotdata>' data on plot schedule page
And I provide details on the product details page
And I provide details on additional questions page
And I select relavant roles on roles page
When I provide the details on declaration page
Then I click on complete button to see the application submitted details on the dashboard
#Send Quote 
When I click on sendquote button 
And I verify the details on send quote rating page
And I verify the details on send quote securities page
And I verify the details on send quote conditions page
And I verify the details on send quote special terms page
And I verify the details on send quote fees page
And I verify the details on send quote file review page
And I verify the details on send quote endorsements page
And I verify the details on send quote confirm details page
And I verify the details on send quote correspondence page
Then I click on send quote button to see quoted details on the dashboard
#Accept Quote
When I click on acceptquote button 
And I verify the details on accept quote products page
And I verify the details on accept quote conditions page
And I verify the details on accept quote file review page 
And I verify the details on accept quote confirm page 
And I verify the details on accept quote internal roles page 
And I verify the details on accept quote correspondence page 
Then I click on accept quote button to see order details on the dashboard
#Pay the Fee balance condition
When I verify account and make the balance payments
#Security Document condition and Registration fee condition
When I clear security document conditions 
#Registration fee condition
#When I clear registration fee conditions
#Manual Conditions created automatically on High Value product
When I clear manual conditions
#Clear additional Conditions
When I clear LABC additional conditions
#Surveyor Assessments 
When I login to surveyor site to submit surveyor assessments
#Issue The COI
When I login back to crisp application to issue the COI
Then I should download COI document and compare the document 


Examples: 
| plotdata                                                                                                                        |
| \\\tpgfile\NewCompany\IT\Department\QA Team\Current Projects\CRISP\CrispAutomation\CrispData\LABC-Products\CompletedHousing.xlsx |



#Issue COI for LABC Social Housing Product 
@Smoketest
Scenario Outline: Verify Issue COI on LABC Social Housing Product
When I click on plus button and select  quote option
And I read following '<plotdata>' details
And I provide details on key site details page
And I provide details on other site details page 
And I provide following '<plotdata>' data on plot schedule page
And I provide details on the product details page
And I provide details on additional questions page
And I select relavant roles on roles page
When I provide the details on declaration page
Then I click on complete button to see the application submitted details on the dashboard
#Send Quote 
When I click on sendquote button 
And I verify the details on send quote rating page
And I verify the details on send quote securities page
And I verify the details on send quote conditions page
And I verify the details on send quote special terms page
And I verify the details on send quote fees page
And I verify the details on send quote file review page
And I verify the details on send quote endorsements page
And I verify the details on send quote confirm details page
And I verify the details on send quote correspondence page
Then I click on send quote button to see quoted details on the dashboard
#Accept Quote
When I click on acceptquote button 
And I verify the details on accept quote products page
And I verify the details on accept quote conditions page
And I verify the details on accept quote file review page 
And I verify the details on accept quote confirm page 
And I verify the details on accept quote internal roles page 
And I verify the details on accept quote correspondence page 
Then I click on accept quote button to see order details on the dashboard
#Pay the Fee balance condition
When I verify account and make the balance payments
#Security Document condition and Registration fee condition
When I clear security document conditions 
#Registration fee condition
#When I clear registration fee conditions
#Manual Conditions created automatically on High Value product
When I clear manual conditions
#Clear additional Conditions
When I clear LABC additional conditions
#Surveyor Assessments 
When I login to surveyor site to submit surveyor assessments
#Issue The COI
When I login back to crisp application to issue the COI
Then I should download COI document and compare the document 


Examples: 
| plotdata                                                                                                                        |
| \\\tpgfile\NewCompany\IT\Department\QA Team\Current Projects\CRISP\CrispAutomation\CrispData\LABC-Products\SocialHousing.xlsx  |



#Issue COI for LABC Social Housing HVS Product 
@Smoketest
Scenario Outline: Verify Issue COI on LABC Social Housing HVS Product
When I click on plus button and select  quote option
And I read following '<plotdata>' details
And I provide details on key site details page
And I provide details on other site details page 
And I provide following '<plotdata>' data on plot schedule page
And I provide details on the product details page
And I provide details on additional questions page
And I select relavant roles on roles page
When I provide the details on declaration page
Then I click on complete button to see the application submitted details on the dashboard
#Send Quote 
When I click on sendquote button 
And I verify the details on send quote rating page
And I verify the details on send quote securities page
And I verify the details on send quote conditions page
And I verify the details on send quote special terms page
And I verify the details on send quote fees page
And I verify the details on send quote file review page
And I verify the details on send quote endorsements page
And I verify the details on send quote confirm details page
And I verify the details on send quote correspondence page
Then I click on send quote button to see quoted details on the dashboard
#Accept Quote
When I click on acceptquote button 
And I verify the details on accept quote products page
And I verify the details on accept quote conditions page
And I verify the details on accept quote file review page 
And I verify the details on accept quote confirm page 
And I verify the details on accept quote internal roles page 
And I verify the details on accept quote correspondence page 
Then I click on accept quote button to see order details on the dashboard
#Pay the Fee balance condition
When I verify account and make the balance payments
#Security Document condition and Registration fee condition
When I clear security document conditions 
#Registration fee condition
#When I clear registration fee conditions
#Manual Conditions created automatically on High Value product
When I clear manual conditions
#Clear additional Conditions
When I clear LABC additional conditions
#Surveyor Assessments 
When I login to surveyor site to submit surveyor assessments
#Issue The COI
When I login back to crisp application to issue the COI
Then I should download COI document and compare the document 


Examples: 
| plotdata                                                                                                                        |
| \\\tpgfile\NewCompany\IT\Department\QA Team\Current Projects\CRISP\CrispAutomation\CrispData\LABC-Products\SocialHousingHV.xlsx |



#Issue COI for PG Newhomes Product
@Smoketest
Scenario Outline: Verify Issue COI on PG Newhomes Product
When I click on plus button and select  quote option
And I read following '<plotdata>' details
And I provide details on key site details page for PG Brand
And I provide details on other site details page 
And I provide following '<plotdata>' data on plot schedule page
And I provide details on the product details page
And I provide details on additional questions page
And I select relavant PG roles on roles page
When I provide the details on declaration page
Then I click on complete button to see the application submitted details on the dashboard
#Send Quote 
When I click on the send quote button 
And I verify the details on send quote rating page
And I verify the details on send quote securities page
And I verify the details on send quote conditions page
And I verify the details on send quote special terms page
And I verify the details on send quote fees page
And I verify the details on send quote file review page
And I verify the details on send quote endorsements page
And I verify the details on send quote confirm details page
And I verify the details on send quote correspondence page
Then I click on send quote button to see quoted details on the dashboard
#Accept Quote
When I click on acceptquote button 
And I verify the details on accept quote products page
And I verify the details on accept quote conditions page
And I verify the details on accept quote file review page 
And I verify the details on accept quote confirm page 
And I verify the details on accept quote internal roles page 
And I verify the details on accept quote correspondence page 
Then I click on accept quote button to see order details on the dashboard
#Pay the Fee balance condition
When I verify account and make the balance payments
#Security Document condition and Registration fee condition
When I clear security document conditions 
#Registration fee condition
#When I clear registration fee conditions
#Manual Conditions created automatically on High Value product
When I clear manual conditions
#Clear additional Conditions
When I clear PG additional conditions
#Surveyor Assessments 
When I login to surveyor site to submit surveyor assessments
#Send Final Notice and Issue COI
When I login back to crisp application to issue the COI
#Issue The COI
Then I should download COI document and compare the document 


Examples: 
| plotdata                                                                                                                |
| \\\tpgfile\NewCompany\IT\Department\QA Team\Current Projects\CRISP\CrispAutomation\CrispData\PG-Products\NewHomes.xlsx  |


#Issue COI for PG Newhomes HVS Product
@Smoketest
Scenario Outline: Verify Issue COI on PG Newhomes HVS Product
When I click on plus button and select  quote option
And I read following '<plotdata>' details
And I provide details on key site details page for PG Brand
And I provide details on other site details page 
And I provide following '<plotdata>' data on plot schedule page
And I provide details on the product details page
And I provide details on additional questions page
And I select relavant PG roles on roles page
When I provide the details on declaration page
Then I click on complete button to see the application submitted details on the dashboard
#Send Quote 
When I click on the send quote button 
And I verify the details on send quote rating page
And I verify the details on send quote securities page
And I verify the details on send quote conditions page
And I verify the details on send quote special terms page
And I verify the details on send quote fees page
And I verify the details on send quote file review page
And I verify the details on send quote endorsements page
And I verify the details on send quote confirm details page
And I verify the details on send quote correspondence page
Then I click on send quote button to see quoted details on the dashboard
#Accept Quote
When I click on acceptquote button 
And I verify the details on accept quote products page
And I verify the details on accept quote conditions page
And I verify the details on accept quote file review page 
And I verify the details on accept quote confirm page 
And I verify the details on accept quote internal roles page 
And I verify the details on accept quote correspondence page 
Then I click on accept quote button to see order details on the dashboard
#Pay the Fee balance condition
When I verify account and make the balance payments
#Security Document condition and Registration fee condition
When I clear security document conditions 
#Registration fee condition
#When I clear registration fee conditions
#Manual Conditions created automatically on High Value product
When I clear manual conditions
#Clear additional Conditions
When I clear PG additional conditions
#Surveyor Assessments 
When I login to surveyor site to submit surveyor assessments
#Send Final Notice and Issue COI
When I login back to crisp application to issue the COI
#Issue The COI
Then I should download COI document and compare the document 


Examples: 
| plotdata                                                                                                                   |
| \\\tpgfile\NewCompany\IT\Department\QA Team\Current Projects\CRISP\CrispAutomation\CrispData\PG-Products\NewHomes-HV.xlsx  |


#Issue COI for PG Private Rental Product
@Smoketest
Scenario Outline: Verify Issue COI on PG Private Rental Product
When I click on plus button and select  quote option
And I read following '<plotdata>' details
And I provide details on key site details page for PG Brand
And I provide details on other site details page 
And I provide following '<plotdata>' data on plot schedule page
And I provide details on the product details page
And I provide details on additional questions page
And I select relavant PG roles on roles page
When I provide the details on declaration page
Then I click on complete button to see the application submitted details on the dashboard
#Send Quote 
When I click on the send quote button 
And I verify the details on send quote rating page
And I verify the details on send quote securities page
And I verify the details on send quote conditions page
And I verify the details on send quote special terms page
And I verify the details on send quote fees page
And I verify the details on send quote file review page
And I verify the details on send quote endorsements page
And I verify the details on send quote confirm details page
And I verify the details on send quote correspondence page
Then I click on send quote button to see quoted details on the dashboard
#Accept Quote
When I click on acceptquote button 
And I verify the details on accept quote products page
And I verify the details on accept quote conditions page
And I verify the details on accept quote file review page 
And I verify the details on accept quote confirm page 
And I verify the details on accept quote internal roles page 
And I verify the details on accept quote correspondence page 
Then I click on accept quote button to see order details on the dashboard
#Pay the Fee balance condition
When I verify account and make the balance payments
#Security Document condition and Registration fee condition
When I clear security document conditions 
#Registration fee condition
#When I clear registration fee conditions
#Manual Conditions created automatically on High Value product
When I clear manual conditions
#Clear additional Conditions
When I clear PG additional conditions
#Surveyor Assessments 
When I login to surveyor site to submit surveyor assessments
#Send Final Notice and Issue COI
When I login back to crisp application to issue the COI
#Issue The COI
Then I should download COI document and compare the document 


Examples: 
| plotdata                                                                                                                     |
| \\\tpgfile\NewCompany\IT\Department\QA Team\Current Projects\CRISP\CrispAutomation\CrispData\PG-Products\PrivateRental.xlsx  |


#Issue COI for PG Private Rental HVS Product
@Smoketest
Scenario Outline: Verify Issue COI on PG Private Rental HVS Product
When I click on plus button and select  quote option
And I read following '<plotdata>' details
And I provide details on key site details page for PG Brand
And I provide details on other site details page 
And I provide following '<plotdata>' data on plot schedule page
And I provide details on the product details page
And I provide details on additional questions page
And I select relavant PG roles on roles page
When I provide the details on declaration page
Then I click on complete button to see the application submitted details on the dashboard
#Send Quote 
When I click on the send quote button 
And I verify the details on send quote rating page
And I verify the details on send quote securities page
And I verify the details on send quote conditions page
And I verify the details on send quote special terms page
And I verify the details on send quote fees page
And I verify the details on send quote file review page
And I verify the details on send quote endorsements page
And I verify the details on send quote confirm details page
And I verify the details on send quote correspondence page
Then I click on send quote button to see quoted details on the dashboard
#Accept Quote
When I click on acceptquote button 
And I verify the details on accept quote products page
And I verify the details on accept quote conditions page
And I verify the details on accept quote file review page 
And I verify the details on accept quote confirm page 
And I verify the details on accept quote internal roles page 
And I verify the details on accept quote correspondence page 
Then I click on accept quote button to see order details on the dashboard
#Pay the Fee balance condition
When I verify account and make the balance payments
#Security Document condition and Registration fee condition
When I clear security document conditions 
#Registration fee condition
#When I clear registration fee conditions
#Manual Conditions created automatically on High Value product
When I clear manual conditions
#Clear additional Conditions
When I clear PG additional conditions
#Surveyor Assessments 
When I login to surveyor site to submit surveyor assessments
#Send Final Notice and Issue COI
When I login back to crisp application to issue the COI
#Issue The COI
Then I should download COI document and compare the document 


Examples: 
| plotdata                                                                                                                      |
| \\\tpgfile\NewCompany\IT\Department\QA Team\Current Projects\CRISP\CrispAutomation\CrispData\PG-Products\PrivateRentalHV.xlsx |


#Issue COI for PG Commercials Product
@Smoketest
Scenario Outline: Verify Issue COI on PG Commercials Product
When I click on plus button and select  quote option
And I read following '<plotdata>' details
And I provide details on key site details page for PG Brand
And I provide details on other site details page 
And I provide following '<plotdata>' data on plot schedule page
And I provide details on the product details page
And I provide details on additional questions page
And I select relavant PG roles on roles page
When I provide the details on declaration page
Then I click on complete button to see the application submitted details on the dashboard
#Send Quote 
When I click on the send quote button 
And I verify the details on send quote rating page
And I verify the details on send quote securities page
And I verify the details on send quote conditions page
And I verify the details on send quote special terms page
And I verify the details on send quote fees page
And I verify the details on send quote file review page
And I verify the details on send quote endorsements page
And I verify the details on send quote confirm details page
And I verify the details on send quote correspondence page
Then I click on send quote button to see quoted details on the dashboard
#Accept Quote
When I click on acceptquote button 
And I verify the details on accept quote products page
And I verify the details on accept quote conditions page
And I verify the details on accept quote file review page 
And I verify the details on accept quote confirm page 
And I verify the details on accept quote internal roles page 
And I verify the details on accept quote correspondence page 
Then I click on accept quote button to see order details on the dashboard
#Pay the Fee balance condition
When I verify account and make the balance payments
#Security Document condition and Registration fee condition
When I clear security document conditions 
#Registration fee condition
#When I clear registration fee conditions
#Manual Conditions created automatically on High Value product
When I clear manual conditions
#Clear additional Conditions
When I clear PG additional conditions
#Surveyor Assessments 
When I login to surveyor site to submit surveyor assessments
#Send Final Notice and Issue COI
When I login back to crisp application to issue the COI
#Issue The COI
Then I should download COI document and compare the document 


Examples: 
| plotdata                                                                                                                      |
| \\\tpgfile\NewCompany\IT\Department\QA Team\Current Projects\CRISP\CrispAutomation\CrispData\PG-Products\Commercials.xlsx     |

#Issue COI for PG Commercials HVS  Product
@Smoketest
Scenario Outline: Verify Issue COI on PG Commercials HVS Product
When I click on plus button and select  quote option
And I read following '<plotdata>' details
And I provide details on key site details page for PG Brand
And I provide details on other site details page 
And I provide following '<plotdata>' data on plot schedule page
And I provide details on the product details page
And I provide details on additional questions page
And I select relavant PG roles on roles page
When I provide the details on declaration page
Then I click on complete button to see the application submitted details on the dashboard
#Send Quote 
When I click on the send quote button 
And I verify the details on send quote rating page
And I verify the details on send quote securities page
And I verify the details on send quote conditions page
And I verify the details on send quote special terms page
And I verify the details on send quote fees page
And I verify the details on send quote file review page
And I verify the details on send quote endorsements page
And I verify the details on send quote confirm details page
And I verify the details on send quote correspondence page
Then I click on send quote button to see quoted details on the dashboard
#Accept Quote
When I click on acceptquote button 
And I verify the details on accept quote products page
And I verify the details on accept quote conditions page
And I verify the details on accept quote file review page 
And I verify the details on accept quote confirm page 
And I verify the details on accept quote internal roles page 
And I verify the details on accept quote correspondence page 
Then I click on accept quote button to see order details on the dashboard
#Pay the Fee balance condition
When I verify account and make the balance payments
#Security Document condition and Registration fee condition
When I clear security document conditions 
#Registration fee condition
#When I clear registration fee conditions
#Manual Conditions created automatically on High Value product
When I clear manual conditions
#Clear additional Conditions
When I clear PG additional conditions
#Surveyor Assessments 
When I login to surveyor site to submit surveyor assessments
#Send Final Notice and Issue COI
When I login back to crisp application to issue the COI
#Issue The COI
Then I should download COI document and compare the document 


Examples: 
| plotdata                                                                                                                       |
| \\\tpgfile\NewCompany\IT\Department\QA Team\Current Projects\CRISP\CrispAutomation\CrispData\PG-Products\Commercials -HV.xlsx  |

#Issue COI for PG Social Housing Product
@Smoketest
Scenario Outline: Verify Issue COI on PG Social Housing Product
When I click on plus button and select  quote option
And I read following '<plotdata>' details
And I provide details on key site details page for PG Brand
And I provide details on other site details page 
And I provide following '<plotdata>' data on plot schedule page
And I provide details on the product details page
And I provide details on additional questions page
And I select relavant PG roles on roles page
When I provide the details on declaration page
Then I click on complete button to see the application submitted details on the dashboard
#Send Quote 
When I click on the send quote button 
And I verify the details on send quote rating page
And I verify the details on send quote securities page
And I verify the details on send quote conditions page
And I verify the details on send quote special terms page
And I verify the details on send quote fees page
And I verify the details on send quote file review page
And I verify the details on send quote endorsements page
And I verify the details on send quote confirm details page
And I verify the details on send quote correspondence page
Then I click on send quote button to see quoted details on the dashboard
#Accept Quote
When I click on acceptquote button 
And I verify the details on accept quote products page
And I verify the details on accept quote conditions page
And I verify the details on accept quote file review page 
And I verify the details on accept quote confirm page 
And I verify the details on accept quote internal roles page 
And I verify the details on accept quote correspondence page 
Then I click on accept quote button to see order details on the dashboard
#Pay the Fee balance condition
When I verify account and make the balance payments
#Security Document condition and Registration fee condition
When I clear security document conditions 
#Registration fee condition
#When I clear registration fee conditions
#Manual Conditions created automatically on High Value product
When I clear manual conditions
#Clear additional Conditions
When I clear PG additional conditions
#Surveyor Assessments 
When I login to surveyor site to submit surveyor assessments
#Send Final Notice and Issue COI
When I login back to crisp application to issue the COI
#Issue The COI
Then I should download COI document and compare the document 


Examples: 
| plotdata                                                                                                                     |
| \\\tpgfile\NewCompany\IT\Department\QA Team\Current Projects\CRISP\CrispAutomation\CrispData\PG-Products\SocialHousing.xlsx  |


#Issue COI for PG Social Housing HVS Product
@Smoketest
Scenario Outline: Verify Issue COI on PG Social Housing HVS Product
When I click on plus button and select  quote option
And I read following '<plotdata>' details
And I provide details on key site details page for PG Brand
And I provide details on other site details page 
And I provide following '<plotdata>' data on plot schedule page
And I provide details on the product details page
And I provide details on additional questions page
And I select relavant PG roles on roles page
When I provide the details on declaration page
Then I click on complete button to see the application submitted details on the dashboard
#Send Quote 
When I click on the send quote button 
And I verify the details on send quote rating page
And I verify the details on send quote securities page
And I verify the details on send quote conditions page
And I verify the details on send quote special terms page
And I verify the details on send quote fees page
And I verify the details on send quote file review page
And I verify the details on send quote endorsements page
And I verify the details on send quote confirm details page
And I verify the details on send quote correspondence page
Then I click on send quote button to see quoted details on the dashboard
#Accept Quote
When I click on acceptquote button 
And I verify the details on accept quote products page
And I verify the details on accept quote conditions page
And I verify the details on accept quote file review page 
And I verify the details on accept quote confirm page 
And I verify the details on accept quote internal roles page 
And I verify the details on accept quote correspondence page 
Then I click on accept quote button to see order details on the dashboard
#Pay the Fee balance condition
When I verify account and make the balance payments
#Security Document condition and Registration fee condition
When I clear security document conditions 
#Registration fee condition
#When I clear registration fee conditions
#Manual Conditions created automatically on High Value product
When I clear manual conditions
#Clear additional Conditions
When I clear PG additional conditions
#Surveyor Assessments 
When I login to surveyor site to submit surveyor assessments
#Send Final Notice and Issue COI
When I login back to crisp application to issue the COI
#Issue The COI
Then I should download COI document and compare the document 


Examples: 
| plotdata                                                                                                                        |
| \\\tpgfile\NewCompany\IT\Department\QA Team\Current Projects\CRISP\CrispAutomation\CrispData\PG-Products\SocialHousing-HV.xlsx  |


#Issue COI for PG SelfBuild Product
@Smoketest
Scenario Outline: Verify Issue COI on PG SelfBuild Product
When I click on plus button and select  quote option
And I read following '<plotdata>' details
And I provide details on key site details page for PG Brand
And I provide details on other site details page 
And I provide following '<plotdata>' data on plot schedule page
And I provide details on the product details page
And I provide details on additional questions page
And I select relavant PG roles on roles page
When I provide the details on declaration page
Then I click on complete button to see the application submitted details on the dashboard
#Send Quote 
When I click on the send quote button 
And I verify the details on send quote rating page
And I verify the details on send quote securities page
And I verify the details on send quote conditions page
And I verify the details on send quote special terms page
And I verify the details on send quote fees page
And I verify the details on send quote file review page
And I verify the details on send quote endorsements page
And I verify the details on send quote confirm details page
And I verify the details on send quote correspondence page
Then I click on send quote button to see quoted details on the dashboard
#Accept Quote
When I click on acceptquote button 
And I verify the details on accept quote products page
And I verify the details on accept quote conditions page
And I verify the details on accept quote file review page 
And I verify the details on accept quote confirm page 
And I verify the details on accept quote internal roles page 
And I verify the details on accept quote correspondence page 
Then I click on accept quote button to see order details on the dashboard
#Pay the Fee balance condition
When I verify account and make the balance payments
#Security Document condition and Registration fee condition
When I clear security document conditions 
#Registration fee condition
#When I clear registration fee conditions
#Manual Conditions created automatically on High Value product
When I clear manual conditions
#Clear additional Conditions
When I clear PG additional conditions
#Surveyor Assessments 
When I login to surveyor site to submit surveyor assessments
#Send Final Notice and Issue COI
When I login back to crisp application to issue the COI
#Issue The COI
Then I should download COI document and compare the document 


Examples: 
| plotdata                                                                                                                 |
| \\\tpgfile\NewCompany\IT\Department\QA Team\Current Projects\CRISP\CrispAutomation\CrispData\PG-Products\SelfBuild.xlsx  |


#Issue COI for PG Completed Housing Product
@Smoketest
Scenario Outline: Verify Issue COI on PG Completed Housing Product
When I click on plus button and select  quote option
And I read following '<plotdata>' details
And I provide details on key site details page for PG Brand
And I provide details on other site details page 
And I provide following '<plotdata>' data on plot schedule page
And I provide details on the product details page
And I provide details on additional questions page
And I select relavant PG roles on roles page
When I provide the details on declaration page
Then I click on complete button to see the application submitted details on the dashboard
#Send Quote 
When I click on the send quote button 
And I verify the details on send quote rating page
And I verify the details on send quote securities page
And I verify the details on send quote conditions page
And I verify the details on send quote special terms page
And I verify the details on send quote fees page
And I verify the details on send quote file review page
And I verify the details on send quote endorsements page
And I verify the details on send quote confirm details page
And I verify the details on send quote correspondence page
Then I click on send quote button to see quoted details on the dashboard
#Accept Quote
When I click on acceptquote button 
And I verify the details on accept quote products page
And I verify the details on accept quote conditions page
And I verify the details on accept quote file review page 
And I verify the details on accept quote confirm page 
And I verify the details on accept quote internal roles page 
And I verify the details on accept quote correspondence page 
Then I click on accept quote button to see order details on the dashboard
#Pay the Fee balance condition
When I verify account and make the balance payments
#Security Document condition and Registration fee condition
When I clear security document conditions 
#Registration fee condition
#When I clear registration fee conditions
#Manual Conditions created automatically on High Value product
When I clear manual conditions
#Clear additional Conditions
When I clear PG additional conditions
#Surveyor Assessments 
When I login to surveyor site to submit surveyor assessments
#Send Final Notice and Issue COI
When I login back to crisp application to issue the COI
#Issue The COI
Then I should download COI document and compare the document 


Examples: 
| plotdata                                                                                                                       |
| \\\tpgfile\NewCompany\IT\Department\QA Team\Current Projects\CRISP\CrispAutomation\CrispData\PG-Products\CompletedHousing.xlsx |


#Issue COI for PG CI Residential Product
@Smoketest
Scenario Outline: Verify Issue COI on PG CI Residential Product
When I click on plus button and select  quote option
And I read following '<plotdata>' details
And I provide details on key site details page for PG Brand
And I provide details on other site details page 
And I provide following '<plotdata>' data on plot schedule page
And I provide details on the product details page
And I provide details on additional questions page
And I select relavant PG roles on roles page
When I provide the details on declaration page
Then I click on complete button to see the application submitted details on the dashboard
#Send Quote 
When I click on the send quote button 
And I verify the details on send quote rating page
And I verify the details on send quote securities page
And I verify the details on send quote conditions page
And I verify the details on send quote special terms page
And I verify the details on send quote fees page
And I verify the details on send quote file review page
And I verify the details on send quote endorsements page
And I verify the details on send quote confirm details page
And I verify the details on send quote correspondence page
Then I click on send quote button to see quoted details on the dashboard
#Accept Quote
When I click on acceptquote button 
And I verify the details on accept quote products page
And I verify the details on accept quote conditions page
And I verify the details on accept quote file review page 
And I verify the details on accept quote confirm page 
And I verify the details on accept quote internal roles page 
And I verify the details on accept quote correspondence page 
Then I click on accept quote button to see order details on the dashboard
#Pay the Fee balance condition
When I verify account and make the balance payments
#Security Document condition and Registration fee condition
When I clear security document conditions 
#Registration fee condition
#When I clear registration fee conditions
#Manual Conditions created automatically on High Value product
When I clear manual conditions
#Clear additional Conditions
When I clear PG additional conditions
#Surveyor Assessments 
When I login to surveyor site to submit surveyor assessments
#Send Final Notice and Issue COI
When I login back to crisp application to issue the COI
#Issue The COI
Then I should download COI document and compare the document 


Examples: 
| plotdata                                                                                                                       |
| \\\tpgfile\NewCompany\IT\Department\QA Team\Current Projects\CRISP\CrispAutomation\CrispData\PG-Products\CI Residential.xlsx   |


#Issue COI for PG CI Residential HVS Product
@Smoketest
Scenario Outline: Verify Issue COI on PG CI Residential HVS Product
When I click on plus button and select  quote option
And I read following '<plotdata>' details
And I provide details on key site details page for PG Brand
And I provide details on other site details page 
And I provide following '<plotdata>' data on plot schedule page
And I provide details on the product details page
And I provide details on additional questions page
And I select relavant PG roles on roles page
When I provide the details on declaration page
Then I click on complete button to see the application submitted details on the dashboard
#Send Quote 
When I click on the send quote button 
And I verify the details on send quote rating page
And I verify the details on send quote securities page
And I verify the details on send quote conditions page
And I verify the details on send quote special terms page
And I verify the details on send quote fees page
And I verify the details on send quote file review page
And I verify the details on send quote endorsements page
And I verify the details on send quote confirm details page
And I verify the details on send quote correspondence page
Then I click on send quote button to see quoted details on the dashboard
#Accept Quote
When I click on acceptquote button 
And I verify the details on accept quote products page
And I verify the details on accept quote conditions page
And I verify the details on accept quote file review page 
And I verify the details on accept quote confirm page 
And I verify the details on accept quote internal roles page 
And I verify the details on accept quote correspondence page 
Then I click on accept quote button to see order details on the dashboard
#Pay the Fee balance condition
When I verify account and make the balance payments
#Security Document condition and Registration fee condition
When I clear security document conditions 
#Registration fee condition
#When I clear registration fee conditions
#Manual Conditions created automatically on High Value product
When I clear manual conditions
#Clear additional Conditions
When I clear PG additional conditions
#Surveyor Assessments 
When I login to surveyor site to submit surveyor assessments
#Send Final Notice and Issue COI
When I login back to crisp application to issue the COI
#Issue The COI
Then I should download COI document and compare the document 


Examples: 
| plotdata                                                                                                                        |
| \\\tpgfile\NewCompany\IT\Department\QA Team\Current Projects\CRISP\CrispAutomation\CrispData\PG-Products\CI Residential-HV.xlsx |

#Issue COI for PG CI Commercials Product
@Smoketest
Scenario Outline: Verify Issue COI on PG CI Commercials Product
When I click on plus button and select  quote option
And I read following '<plotdata>' details
And I provide details on key site details page for PG Brand
And I provide details on other site details page 
And I provide following '<plotdata>' data on plot schedule page
And I provide details on the product details page
And I provide details on additional questions page
And I select relavant PG roles on roles page
When I provide the details on declaration page
Then I click on complete button to see the application submitted details on the dashboard
#Send Quote 
When I click on the send quote button 
And I verify the details on send quote rating page
And I verify the details on send quote securities page
And I verify the details on send quote conditions page
And I verify the details on send quote special terms page
And I verify the details on send quote fees page
And I verify the details on send quote file review page
And I verify the details on send quote endorsements page
And I verify the details on send quote confirm details page
And I verify the details on send quote correspondence page
Then I click on send quote button to see quoted details on the dashboard
#Accept Quote
When I click on acceptquote button 
And I verify the details on accept quote products page
And I verify the details on accept quote conditions page
And I verify the details on accept quote file review page 
And I verify the details on accept quote confirm page 
And I verify the details on accept quote internal roles page 
And I verify the details on accept quote correspondence page 
Then I click on accept quote button to see order details on the dashboard
#Pay the Fee balance condition
When I verify account and make the balance payments
#Security Document condition and Registration fee condition
When I clear security document conditions 
#Registration fee condition
#When I clear registration fee conditions
#Manual Conditions created automatically on High Value product
When I clear manual conditions
#Clear additional Conditions
When I clear PG additional conditions
#Surveyor Assessments 
When I login to surveyor site to submit surveyor assessments
#Send Final Notice and Issue COI
When I login back to crisp application to issue the COI
#Issue The COI
Then I should download COI document and compare the document 


Examples: 
| plotdata                                                                                                                      |
| \\\tpgfile\NewCompany\IT\Department\QA Team\Current Projects\CRISP\CrispAutomation\CrispData\PG-Products\CI Commercial.xlsx   |

#Issue COI for PG CI Commercials HVS Product
@Smoketest
Scenario Outline: Verify Issue COI on PG CI Commercials HVS Product
When I click on plus button and select  quote option
And I read following '<plotdata>' details
And I provide details on key site details page for PG Brand
And I provide details on other site details page 
And I provide following '<plotdata>' data on plot schedule page
And I provide details on the product details page
And I provide details on additional questions page
And I select relavant PG roles on roles page
When I provide the details on declaration page
Then I click on complete button to see the application submitted details on the dashboard
#Send Quote 
When I click on the send quote button 
And I verify the details on send quote rating page
And I verify the details on send quote securities page
And I verify the details on send quote conditions page
And I verify the details on send quote special terms page
And I verify the details on send quote fees page
And I verify the details on send quote file review page
And I verify the details on send quote endorsements page
And I verify the details on send quote confirm details page
And I verify the details on send quote correspondence page
Then I click on send quote button to see quoted details on the dashboard
#Accept Quote
When I click on acceptquote button 
And I verify the details on accept quote products page
And I verify the details on accept quote conditions page
And I verify the details on accept quote file review page 
And I verify the details on accept quote confirm page 
And I verify the details on accept quote internal roles page 
And I verify the details on accept quote correspondence page 
Then I click on accept quote button to see order details on the dashboard
#Pay the Fee balance condition
When I verify account and make the balance payments
#Security Document condition and Registration fee condition
When I clear security document conditions 
#Registration fee condition
#When I clear registration fee conditions
#Manual Conditions created automatically on High Value product
When I clear manual conditions
#Clear additional Conditions
When I clear PG additional conditions
#Surveyor Assessments 
When I login to surveyor site to submit surveyor assessments
#Send Final Notice and Issue COI
When I login back to crisp application to issue the COI
#Issue The COI
Then I should download COI document and compare the document 


Examples: 
| plotdata                                                                                                                      |
|\\\tpgfile\NewCompany\IT\Department\QA Team\Current Projects\CRISP\CrispAutomation\CrispData\PG-Products\CI Commercial-HV.xlsx |

