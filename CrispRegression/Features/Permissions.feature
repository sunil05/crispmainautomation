﻿Feature: Permissions

Background: 
	Given I am on login page
	When I enter crisp username 
	And I enter crisp password
	And I click on login button
	Then I should see all items displayed on dashboard page

@SmokeTest
Scenario: Verify No Permissions
Given I have created auto user to check permissions 
When I remove all permissions on auto user
Then I verify that not perform create operations
Then I verify that not perform amend operations
Then I verify that can not perform search operations
