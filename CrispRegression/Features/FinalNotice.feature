﻿Feature: FinalNotice		
Background: 
	Given I am on login page
	When I enter crisp username 
	And I enter crisp password
	And I click on login button
	Then I should see all items displayed on dashboard page

@Smoketest
Scenario Outline: Verify Sending Full Final Notice for PG Quote Residential building control Product
When I click on plus button and select  quote option
And I read following '<plotdata>' details
And I provide details on key site details page for PG Brand
And I provide details on other site details page 
And I provide following '<plotdata>' data on plot schedule page
And I provide details on the product details page
And I provide details on additional questions page
And I select relavant PG roles on roles page
When I provide the details on declaration page
Then I click on complete button to see the application submitted details on the dashboard
#Send Quote 
When I click on the send quote button 
And I verify the details on send quote rating page
And I verify the details on send quote securities page
And I verify the details on send quote conditions page
And I verify the details on send quote special terms page
And I verify the details on send quote fees page
And I verify the details on send quote file review page
And I verify the details on send quote endorsements page
And I verify the details on send quote confirm details page
And I verify the details on send quote correspondence page
Then I click on send quote button to see quoted details on the dashboard
#Accept Quote
When I click on acceptquote button  
And I verify the details on accept quote products page
And I verify the details on accept quote conditions page
And I verify the details on accept quote file review page 
And I verify the details on accept quote confirm page 
And I verify the details on accept quote internal roles page 
And I verify the details on accept quote correspondence page 
Then I click on accept quote button to see order details on the dashboard
#Sending Intial Notice
When I select files and documents page
And I upload sitelocation plan file type and surface storm and foul drainage layout  file type documents 
And I click on send intial notice button 
And I provide the details on initial notice qs page
And I verify the details on file review page 
And I verify the details on confirm page 
And I verify the details on intoclient page 
And I Verify the details on intola page
When I click on send initial notice button to complete sending intial notice
Then I should see the intial notice status has been sent
#Respond To Accept Intial Notice 
When I Select respond to initial notice button 
And I select the response type as accepted
And I verify the details on acceptance page 
And I verify the details on fliereview page 
And I verify details on confirm page
And I verify the initial notice acceptance details
When I click on send initial notice response -acceptance button 
Then I should view the resposne status on building control page.
#Pay the Fee balance
When I verify account and make the balance payments
#Manual Conditions created automatically on High Value product
When I clear manual conditions
#Surveyor Assessments 
When I login to surveyor site to submit surveyor assessments
Then surveyor assessment should be submitted successfully
#Sending Final Notice
#When I login to surveyor application to signoff building control 
When I loginback to crisp application to send final notice 
Then I should see final notice has been sent successfully. 
Then I should download the final notice document and compare it

Examples: 
 | plotdata                                                                                                                        |
 |\\\tpgfile\NewCompany\IT\Department\QA Team\Current Projects\CRISP\CrispAutomation\CrispData\FinalNoticeData\NewHomes.xlsx |

@Smoketest
Scenario Outline: Verify Sending Full Final Notice for PG Quote commercial building control Product
When I click on plus button and select  quote option
And I read following '<plotdata>' details
And I provide details on key site details page for PG Brand
And I provide details on other site details page 
And I provide following '<plotdata>' data on plot schedule page
And I provide details on the product details page
And I provide details on additional questions page
And I select relavant PG roles on roles page
When I provide the details on declaration page
Then I click on complete button to see the application submitted details on the dashboard
#Send Quote 
When I click on the send quote button 
And I verify the details on send quote rating page
And I verify the details on send quote securities page
And I verify the details on send quote conditions page
And I verify the details on send quote special terms page
And I verify the details on send quote fees page
And I verify the details on send quote file review page
And I verify the details on send quote endorsements page
And I verify the details on send quote confirm details page
And I verify the details on send quote correspondence page
Then I click on send quote button to see quoted details on the dashboard
#Accept Quote
When I click on acceptquote button  
And I verify the details on accept quote products page
And I verify the details on accept quote conditions page
And I verify the details on accept quote file review page 
And I verify the details on accept quote confirm page 
And I verify the details on accept quote internal roles page 
And I verify the details on accept quote correspondence page 
Then I click on accept quote button to see order details on the dashboard
#Sending Intial Notice
When I select files and documents page
And I upload sitelocation plan file type and surface storm and foul drainage layout  file type documents 
And I click on send intial notice button 
And I provide the details on initial notice qs page
And I verify the details on file review page 
And I verify the details on confirm page 
And I verify the details on intoclient page 
And I Verify the details on intola page
When I click on send initial notice button to complete sending intial notice
Then I should see the intial notice status has been sent
#Respond To Accept Intial Notice 
When I Select respond to initial notice button 
And I select the response type as accepted
And I verify the details on acceptance page 
And I verify the details on fliereview page 
And I verify details on confirm page
And I verify the initial notice acceptance details
When I click on send initial notice response -acceptance button 
Then I should view the resposne status on building control page.
#Pay the Fee balance
When I verify account and make the balance payments
#Manual Conditions created automatically on High Value product
When I clear manual conditions
#Surveyor Assessments 
When I login to surveyor site to submit surveyor assessments
Then surveyor assessment should be submitted successfully
#Sending Final Notice
#When I login to surveyor application to signoff building control 
When I loginback to crisp application to send final notice 
Then I should see final notice has been sent successfully. 
Then I should download the final notice document and compare it

Examples: 
 | plotdata                                                                                                                         | 
 |\\\tpgfile\NewCompany\IT\Department\QA Team\Current Projects\CRISP\CrispAutomation\CrispData\FinalNoticeData\Commercials.xlsx |

@Smoketest
Scenario Outline: Verify Sending Partial Final Notice for PG Quote commercial building control Product
When I click on plus button and select  quote option
And I read following '<plotdata>' details
And I provide details on key site details page for PG Brand
And I provide details on other site details page 
And I provide following '<plotdata>' data on plot schedule page
And I provide details on the product details page
And I provide details on additional questions page
And I select relavant PG roles on roles page
When I provide the details on declaration page
Then I click on complete button to see the application submitted details on the dashboard
#Send Quote 
When I click on the send quote button 
And I verify the details on send quote rating page
And I verify the details on send quote securities page
And I verify the details on send quote conditions page
And I verify the details on send quote special terms page
And I verify the details on send quote fees page
And I verify the details on send quote file review page
And I verify the details on send quote endorsements page
And I verify the details on send quote confirm details page
And I verify the details on send quote correspondence page
Then I click on send quote button to see quoted details on the dashboard
#Accept Quote
When I click on acceptquote button 
And I verify the details on accept quote products page
And I verify the details on accept quote conditions page
And I verify the details on accept quote file review page 
And I verify the details on accept quote confirm page 
And I verify the details on accept quote internal roles page 
And I verify the details on accept quote correspondence page 
Then I click on accept quote button to see order details on the dashboard
#Sending Intial Notice
When I select files and documents page
And I upload sitelocation plan file type and surface storm and foul drainage layout  file type documents 
And I click on send intial notice button 
And I provide the details on initial notice qs page
And I verify the details on file review page 
And I verify the details on confirm page 
And I verify the details on intoclient page 
And I Verify the details on intola page
When I click on send initial notice button to complete sending intial notice
Then I should see the intial notice status has been sent
#Respond To Accept Intial Notice 
When I Select respond to initial notice button 
And I select the response type as accepted
And I verify the details on acceptance page 
And I verify the details on fliereview page 
And I verify details on confirm page
And I verify the initial notice acceptance details
When I click on send initial notice response -acceptance button 
Then I should view the resposne status on building control page.
#Pay the Fee balance
When I verify account and make the balance payments
#Manual Conditions created automatically on High Value product
When I clear manual conditions
#Surveyor Assessments 
When I login to surveyor site to submit surveyor assessments
Then surveyor assessment should be submitted successfully
#Sending Final Notice
#When I login to surveyor application to signoff building control 
When I loginback to crisp application to send final notice 
Then I should see partial final notice has been sent successfully. 
Then I should download the partial final notice document and compare it

Examples: 
| plotdata                                                        | 
|\\\tpgfile\NewCompany\IT\Department\QA Team\Current Projects\CRISP\CrispAutomation\CrispData\FinalNoticeData\Commercials - PartialFN.xlsx| 


@Smoketest
Scenario Outline: Verify Sending Partial Final Notice for PG Quote Residential building control Product
When I click on plus button and select  quote option
And I read following '<plotdata>' details
And I provide details on key site details page for PG Brand
And I provide details on other site details page 
And I provide following '<plotdata>' data on plot schedule page
And I provide details on the product details page
And I provide details on additional questions page
And I select relavant PG roles on roles page
When I provide the details on declaration page
Then I click on complete button to see the application submitted details on the dashboard
#Send Quote 
When I click on the send quote button 
And I verify the details on send quote rating page
And I verify the details on send quote securities page
And I verify the details on send quote conditions page
And I verify the details on send quote special terms page
And I verify the details on send quote fees page
And I verify the details on send quote file review page
And I verify the details on send quote endorsements page
And I verify the details on send quote confirm details page
And I verify the details on send quote correspondence page
Then I click on send quote button to see quoted details on the dashboard
#Accept Quote
When I click on acceptquote button  
And I verify the details on accept quote products page
And I verify the details on accept quote conditions page
And I verify the details on accept quote file review page 
And I verify the details on accept quote confirm page 
And I verify the details on accept quote internal roles page 
And I verify the details on accept quote correspondence page 
Then I click on accept quote button to see order details on the dashboard
#Sending Intial Notice
When I select files and documents page
And I upload sitelocation plan file type and surface storm and foul drainage layout  file type documents 
And I click on send intial notice button 
And I provide the details on initial notice qs page
And I verify the details on file review page 
And I verify the details on confirm page 
And I verify the details on intoclient page 
And I Verify the details on intola page
When I click on send initial notice button to complete sending intial notice
Then I should see the intial notice status has been sent
#Respond To Accept Intial Notice 
When I Select respond to initial notice button 
And I select the response type as accepted
And I verify the details on acceptance page 
And I verify the details on fliereview page 
And I verify details on confirm page
And I verify the initial notice acceptance details
When I click on send initial notice response -acceptance button 
Then I should view the resposne status on building control page.
#Pay the Fee balance
When I verify account and make the balance payments
#Manual Conditions created automatically on High Value product
When I clear manual conditions
#Surveyor Assessments 
When I login to surveyor site to submit surveyor assessments
Then surveyor assessment should be submitted successfully
#Sending Final Notice
#When I login to surveyor application to signoff building control 
When I loginback to crisp application to send final notice 
Then I should see partial final notice has been sent successfully. 
Then I should download the partial final notice document and compare it

Examples: 
| plotdata                                                        | 
|\\\tpgfile\NewCompany\IT\Department\QA Team\Current Projects\CRISP\CrispAutomation\CrispData\FinalNoticeData\NewHomes - PartialFN.xlsx|

