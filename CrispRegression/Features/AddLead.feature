﻿Feature:AddLead

Background: 
	Given I am on login page
	When I enter crisp username 
	And I enter crisp password
	And I click on login button
	Then I should see all items displayed on dashboard page

@SmokeTest
Scenario: Verify adding PG Lead for standard products 
When I click on the plus button 
And I select lead option to create lead
And I provide pg details on the development information page
And I provide details on products details page
And I provide details on the roles page
And I provide details on other details page
When I save the lead details 
Then I should see PG Lead details on dashboard page

@SmokeTest
Scenario: Verify adding PG Lead for high value products 
When I click on the plus button 
And I select lead option to create lead
And I provide pg details on the development information page
And I provide high value details on products details page
And I provide details on the roles page
And I provide details on other details page
When I save the lead details 
Then I should see PG Lead details on dashboard page

@SmokeTest
Scenario: Verify adding LABC Lead for standard products 	
When I click on the plus button 
And I select lead option to create lead
And I provide labc details on the development information page
And I provide details on products details page
And I provide details on the roles page
And I provide details on other details page
When I save the lead details 
Then I should see LABC lead details on the dashboard

@SmokeTest
Scenario: Verify adding LABC Lead for high value products 
When I click on the plus button 
And I select lead option to create lead
And I provide labc details on the development information page
And I provide high value details on products details page
And I provide details on the roles page
And I provide details on other details page
When I save the lead details 
Then I should see LABC lead details on the dashboard

@SmokeTest
Scenario: Verify rejecting and reinstating LABC Lead for high value products 	
When I click on the plus button 
And I select lead option to create lead
And I provide labc details on the development information page
And I provide high value details on products details page
And I provide details on the roles page
And I provide details on other details page
When I save the lead details 
Then I should see PG Lead details on dashboard page 
#Rejecting the Lead 
When I click on reject button on dashboard
And I enter the details for rejection on rejection dialogue window 
And I select reject option on rejection dialogue window
Then I should see rejected lead on the dashboard
#ReInstate the Lead
When I click on reinstate button on dashboard 
And I enter the notes details on reinstate dialogue window 
And I click on reinstate option on reinstate dialogue window 
Then I should see LABC lead details on the dashboard

@SmokeTest
Scenario: Verify ShortList a Lead 
When I click on the plus button 
And I select lead option to create lead
And I provide pg details on the development information page
And I provide details on products details page
And I provide details on the roles page
And I provide details on other details page
When I save the lead details 
Then I should see PG Lead details on dashboard page
Then I should see lead the activity has been recorded
#Shortlisting the Lead
When I perform operation to shortlist the lead


@SmokeTest
Scenario: Verify Allocating Email To a Lead 
When I click on the plus button 
And I select lead option to create lead
And I provide pg details on the development information page
And I provide details on products details page
And I provide details on the roles page
And I provide details on other details page
When I save the lead details 
Then I should see PG Lead details on dashboard page
#Then I should see lead the activity has been recorded
#Allocating Email To The Lead
#When I perform operation email to the the lead



@SmokeTest
Scenario Outline: Verify additional buttons on LABC Lead for standard products 	
When I click on the plus button 
And I select lead option to create lead
And I provide labc details on the development information page
And I provide details on products details page
And I provide details on the roles page
And I provide details on other details page
When I save the lead details 
Then I should see LABC lead details on the dashboard
#Create a quote
When I click on actions button to select quote application
And I read following '<plotdata>' details
And I provide details on key site details page
And I provide details on other site details page 
And I provide following '<plotdata>' data on plot schedule page
And I provide details on the product details page
And I provide details on additional questions page
And I select relavant roles on roles page
When I provide the details on declaration page
Then I click on complete button to see the application submitted details on the dashboard
#Send Quote 
When I click on sendquote button 
And I verify the details on send quote rating page
And I verify the details on send quote securities page
And I verify the details on send quote conditions page
And I verify the details on send quote special terms page
And I verify the details on send quote fees page
And I verify the details on send quote file review page
And I verify the details on send quote endorsements page
And I verify the details on send quote confirm details page
And I verify the details on send quote correspondence page
Then I click on send quote button to see quoted details on the dashboard
#Accept Quote
When I click on acceptquote button 
And I verify the details on accept quote products page
And I verify the details on accept quote conditions page
And I verify the details on accept quote file review page 
And I verify the details on accept quote confirm page 
And I verify the details on accept quote internal roles page 
And I verify the details on accept quote correspondence page 
Then I click on accept quote button to see order details on the dashboard


Examples: 

| plotdata |
|\\\tpgfile\NewCompany\IT\Department\QA Team\Current Projects\CRISP\CrispAutomation\CrispData\LABC-Products\NewHomes.xlsx |
