﻿Feature: SiteTasks
Background: 
	Given I am on login page
	When I enter crisp username 
	And I enter crisp password
	And I click on login button
	Then I should see all items displayed on dashboard page  

@SmokeTest
Scenario Outline: Verify the creation and closing of tasks on PG
    Given I have submit a PG quote using following '<plotdata>'
	Then I verify the tasks are created in submitted stage
	When I convert the application into quoted state
	Then I should verify the created tasks in quoted stage
	Then I should  verify the closed tasks in quoted stage
	When I convert the quote into order
	Then I should verify the created tasks in order stage
	Then I should verify the closed tasks in order stage
	When I send intial notice 
	Then I should verify the created tasks on intial notice stage
	Then I should verify the closed taks  on intial notice stage
	When I select respond to initial notice 
	Then I should verify the created tasks on intial notice response stage
	Then I should verify the closed taks  on intial notice response stage
	When I submit site risk assessment
	Then I should verify the created tasks when site risk assessment submit
	Then I should verify the closed taks  when site risk assessment submit
	When I submit design review assessment
    Then I should verify the created tasks when design review assessment submit
	Then I should verify the closed taks  when design review assessment submit
	When I submit engineer review assessment
    Then I should verify the created tasks when engineer review assessment submit
	Then I should verify the closed taks  when engineer review assessment submit
	When I submit site inspection assessment
    Then I should verify the created tasks when site inspection assessment submit
	Then I should verify the closed taks  when site inspection assessment submit
    When I send final notice 
	Then I should verify the created tasks on final notice stage
	Then I should verify the closed taks  on final notice stage
	When I clear security document conditions
    When I clear manual conditions
    When I clear PG additional conditions 
	#Pay the Fee balance condition
    When I verify account and make the balance payments
    #Issue The DevelopmentIC
    Then I login back to crisp application to issue the DevIC 	
    #Issue The PlotIC
    Then I login back to crisp application to issue the plotIC	
	#Send Final Notice and Issue COI
    When I login back to crisp application to issue the COI
	Then I should verify the created tasks on issue COI
	Then I should verify the closed taks  on issue COI
	#Verify Task On RescindProcess Activity
	When I rescind the COI 
	Then I should verify the created tasks on Rescind COI
	When I rescind the other activites 
	Then I should verify created tasks on rescind process
Examples: 
| plotdata                                                                                                                                              |
|\\\tpgfile\NewCompany\IT\Department\QA Team\Current Projects\CRISP\CrispAutomation\CrispData\IntialNoticeData\NewHomes.xlsx| 

@mytag
Scenario Outline: Verify the creation and closing of tasks on PG referrals
    Given I have submit a PG quote using following '<plotdata>'
	Then I verify the tasks are created in submitted stage
	When I convert the application into quoted state
	Then I should verify the created tasks in quoted stage
	Then I should  verify the closed tasks in quoted stage
	When I convert the quote into order
	Then I should verify the created tasks in order stage
	Then I should verify the closed tasks in order stage
    When I create the referrals 
	Then I should verify tasks created on referral 
	When I close the referrals 
	Then I should verify tasks closed on referral 
Examples: 
| plotdata                                                                                                                                              |
|\\\tpgfile\NewCompany\IT\Department\QA Team\Current Projects\CRISP\CrispAutomation\CrispData\IntialNoticeData\NewHomes.xlsx| 

@mytag
Scenario Outline: Verify the creation and closing of tasks on LABC
    Given I have submit a quote using following '<plotdata>'
	Then I verify the tasks are created in submitted stage
	When I convert the application into quoted state
	Then I should verify the created tasks in quoted stage
	Then I should  verify the closed tasks in quoted stage
	When I convert the quote into order
	Then I should verify the created tasks in order stage
	Then I should verify the closed tasks in order stage  
	When I submit site risk assessment
	Then I should verify the created tasks when site risk assessment submit
	Then I should verify the closed taks  when site risk assessment submit
	When I submit design review assessment
    Then I should verify the created tasks when design review assessment submit
	Then I should verify the closed taks  when design review assessment submit
	When I submit engineer review assessment
    Then I should verify the created tasks when engineer review assessment submit
	Then I should verify the closed taks  when engineer review assessment submit
	When I submit site inspection assessment
    Then I should verify the created tasks when site inspection assessment submit
	Then I should verify the closed taks  when site inspection assessment submit 
	When I clear security document conditions
    When I clear manual conditions
    When I clear LABC additional conditions 
    #Pay the Fee balance condition
    When I verify account and make the balance payments
    #Issue The DevelopmentIC
    Then I login back to crisp application to issue the DevIC 	
    #Issue The PlotIC
    Then I login back to crisp application to issue the plotIC	
	#Send Final Notice and Issue COI
    When I login back to crisp application to issue the COI
	Then I should verify the created tasks on issue COI
	Then I should verify the closed taks  on issue COI

Examples: 
| plotdata                                                                                                                                              |
|\\\tpgfile\NewCompany\IT\Department\QA Team\Current Projects\CRISP\CrispAutomation\CrispData\LABC-Products\NewHomes.xlsx | 