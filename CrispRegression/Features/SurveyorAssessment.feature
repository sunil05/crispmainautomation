﻿Feature: SurveyorAssessment
Background: 
	Given I am on login page
	When I enter crisp username 
	And I enter crisp password
	And I click on login button
	Then I should see all items displayed on dashboard page
	
#Verify SiteRisk Assessment on all prdocuts when construction type is selected "NewBuild"
@Smoketest
Scenario Outline: Verify Submit Site Risk Assessment
When I click on plus button and select  quote option
And I read following '<plotdata>' details
And I provide details on key site details page for PG Brand
And I provide details on other site details page 
And I provide following '<plotdata>' data on plot schedule page
And I provide details on the product details page
And I provide details on additional questions page
And I select relavant PG roles on roles page
When I provide the details on declaration page
Then I click on complete button to see the application submitted details on the dashboard
#Send Quote 
When I click on the send quote button 
And I verify the details on send quote rating page
And I verify the details on send quote securities page
And I verify the details on send quote conditions page
And I verify the details on send quote special terms page
And I verify the details on send quote fees page
And I verify the details on send quote file review page
And I verify the details on send quote endorsements page
And I verify the details on send quote confirm details page
And I verify the details on send quote correspondence page
Then I click on send quote button to see quoted details on the dashboard
#Accept Quote
When I click on acceptquote button 
And I verify the details on accept quote products page
And I verify the details on accept quote conditions page
And I verify the details on accept quote file review page 
And I verify the details on accept quote confirm page 
And I verify the details on accept quote internal roles page 
And I verify the details on accept quote correspondence page 
Then I click on accept quote button to see order details on the dashboard
#SiteRiskAssessment 
When I login to surveyor site  
And I select the one of the site from the list 
And I click on site risk assessment option 
And I Click on edit button to submit the site risk assessment
Then Siterisk assessment should be submitted successfully


Examples: 
| plotdata                                                    |
|\\\tpgfile\NewCompany\IT\Department\QA Team\Current Projects\CRISP\CrispAutomation\CrispData\Assessment\NewHomes-SRA.xlsx| 

#Verify SiteRisk Assessment on all prdocuts when construction type is selected "NewBuild"
@Smoketest
Scenario Outline: Verify Submit Site Risk Assessment when no selected
When I click on plus button and select  quote option
And I read following '<plotdata>' details
And I provide details on key site details page for PG Brand
And I provide details on other site details page 
And I provide following '<plotdata>' data on plot schedule page
And I provide details on the product details page
And I provide details on additional questions page
And I select relavant PG roles on roles page
When I provide the details on declaration page
Then I click on complete button to see the application submitted details on the dashboard
#Send Quote 
When I click on the send quote button 
And I verify the details on send quote rating page
And I verify the details on send quote securities page
And I verify the details on send quote conditions page
And I verify the details on send quote special terms page
And I verify the details on send quote fees page
And I verify the details on send quote file review page
And I verify the details on send quote endorsements page
And I verify the details on send quote confirm details page
And I verify the details on send quote correspondence page
Then I click on send quote button to see quoted details on the dashboard
#Accept Quote
When I click on acceptquote button 
And I verify the details on accept quote products page
And I verify the details on accept quote conditions page
And I verify the details on accept quote file review page 
And I verify the details on accept quote confirm page 
And I verify the details on accept quote internal roles page 
And I verify the details on accept quote correspondence page 
Then I click on accept quote button to see order details on the dashboard
#SiteRiskAssessment 
When I login to surveyor site  
When I select the one of the site from the list 
And I click on site risk assessment option 
And I Click on edit button to submit the site risk assessment with no options
Then Siterisk assessment should be submitted successfully

Examples: 
| plotdata                                                    |
|\\\tpgfile\NewCompany\IT\Department\QA Team\Current Projects\CRISP\CrispAutomation\CrispData\Assessment\NewHomes-SRA.xlsx| 

#Verify Refurbishment Assessment on all products when construction type is selected "Conversion"
@Smoketest
Scenario Outline: Verify Submit Refurbishment Assessment
When I click on plus button and select  quote option
And I read following '<plotdata>' details
And I provide details on key site details page for PG Brand
And I provide details on other site details page 
And I provide following '<plotdata>' data on plot schedule page
And I provide details on the product details page
And I provide details on additional questions page
And I select relavant PG roles on roles page
When I provide the details on declaration page
Then I click on complete button to see the application submitted details on the dashboard
#Send Quote 
When I click on the send quote button 
And I verify the details on send quote rating page
And I verify the details on send quote securities page
And I verify the details on send quote conditions page
And I verify the details on send quote special terms page
And I verify the details on send quote fees page
And I verify the details on send quote file review page
And I verify the details on send quote endorsements page
And I verify the details on send quote confirm details page
And I verify the details on send quote correspondence page
Then I click on send quote button to see quoted details on the dashboard
#Accept Quote
When I click on acceptquote button 
And I verify the details on accept quote products page
And I verify the details on accept quote conditions page
And I verify the details on accept quote file review page 
And I verify the details on accept quote confirm page 
And I verify the details on accept quote internal roles page 
And I verify the details on accept quote correspondence page 
Then I click on accept quote button to see order details on the dashboard
#RefurbishmentAssessment
When I login to surveyor site  
And I select the one of the site from the list 
And I click on the refurbishment risk assessment option 
When I click on edit button to submit the refurbishment risk assessment 
Then Refurbishment assessment should be submitted successfully


Examples: 
| plotdata                                                                                              | 
|\\\tpgfile\NewCompany\IT\Department\QA Team\Current Projects\CRISP\CrispAutomation\CrispData\Assessment\NewHomes-Refurbishment.xlsx| 

#Verify Refurbishment Assessment on all products when construction type is selected "Conversion"
@Smoketest
Scenario Outline: Verify Submit Refurbishment Assessment when no selected
When I click on plus button and select  quote option
And I read following '<plotdata>' details
And I provide details on key site details page for PG Brand
And I provide details on other site details page 
And I provide following '<plotdata>' data on plot schedule page
And I provide details on the product details page
And I provide details on additional questions page
And I select relavant PG roles on roles page
When I provide the details on declaration page
Then I click on complete button to see the application submitted details on the dashboard
#Send Quote 
When I click on the send quote button 
And I verify the details on send quote rating page
And I verify the details on send quote securities page
And I verify the details on send quote conditions page
And I verify the details on send quote special terms page
And I verify the details on send quote fees page
And I verify the details on send quote file review page
And I verify the details on send quote endorsements page
And I verify the details on send quote confirm details page
And I verify the details on send quote correspondence page
Then I click on send quote button to see quoted details on the dashboard
#Accept Quote
When I click on acceptquote button 
And I verify the details on accept quote products page
And I verify the details on accept quote conditions page
And I verify the details on accept quote file review page 
And I verify the details on accept quote confirm page 
And I verify the details on accept quote internal roles page 
And I verify the details on accept quote correspondence page 
Then I click on accept quote button to see order details on the dashboard
#RefurbishmentAssessment
When I login to surveyor site  
And I select the one of the site from the list  
And I click on the refurbishment risk assessment option 
When I click on edit button to submit the refurbishment risk assessment with no options
Then Refurbishment assessment should be submitted successfully

Examples: 
| plotdata                                                                                              | 
|\\\tpgfile\NewCompany\IT\Department\QA Team\Current Projects\CRISP\CrispAutomation\CrispData\Assessment\NewHomes-Refurbishment.xlsx| 


#Verify Site inspection Feature at last stage(After complete rest of the assessments for all prodcuts)
@Smoketest
Scenario Outline: Verify Site Inspection
When I click on plus button and select  quote option
And I read following '<plotdata>' details
And I provide details on key site details page for PG Brand
And I provide details on other site details page 
And I provide following '<plotdata>' data on plot schedule page
And I provide details on the product details page
And I provide details on additional questions page
And I select relavant PG roles on roles page
When I provide the details on declaration page
Then I click on complete button to see the application submitted details on the dashboard
#Send Quote 
When I click on the send quote button 
And I verify the details on send quote rating page
And I verify the details on send quote securities page
And I verify the details on send quote conditions page
And I verify the details on send quote special terms page
And I verify the details on send quote fees page
And I verify the details on send quote file review page
And I verify the details on send quote endorsements page
And I verify the details on send quote confirm details page
And I verify the details on send quote correspondence page
Then I click on send quote button to see quoted details on the dashboard
#Accept Quote
When I click on acceptquote button 
And I verify the details on accept quote products page
And I verify the details on accept quote conditions page
And I verify the details on accept quote file review page 
And I verify the details on accept quote confirm page 
And I verify the details on accept quote internal roles page 
And I verify the details on accept quote correspondence page 
Then I click on accept quote button to see order details on the dashboard
#Site Inspection Update
When I login to surveyor site  
When I select the one of the site from the list 
And I click on site inspection option to add inspections
Then site inspection should be added successfully

Examples: 
| plotdata                                                        | 
|\\\tpgfile\NewCompany\IT\Department\QA Team\Current Projects\CRISP\CrispAutomation\CrispData\Assessment\NewHomes-SRA.xlsx| 


#Verify Design Review (Should be submitted for over 100+ or  All HVS plots or  All BC plots)
@Smoketest
Scenario Outline: Verify Design Review
When I click on plus button and select  quote option
And I read following '<plotdata>' details
And I provide details on key site details page for PG Brand
And I provide details on other site details page 
And I provide following '<plotdata>' data on plot schedule page
And I provide details on the product details page
And I provide details on additional questions page
And I select relavant PG roles on roles page
When I provide the details on declaration page
Then I click on complete button to see the application submitted details on the dashboard
#Send Quote 
When I click on the send quote button 
And I verify the details on send quote rating page
And I verify the details on send quote securities page
And I verify the details on send quote conditions page
And I verify the details on send quote special terms page
And I verify the details on send quote fees page
And I verify the details on send quote file review page
And I verify the details on send quote endorsements page
And I verify the details on send quote confirm details page
And I verify the details on send quote correspondence page
Then I click on send quote button to see quoted details on the dashboard
#Accept Quote
When I click on acceptquote button 
And I verify the details on accept quote products page
And I verify the details on accept quote conditions page
And I verify the details on accept quote file review page 
And I verify the details on accept quote confirm page 
And I verify the details on accept quote internal roles page 
And I verify the details on accept quote correspondence page 
Then I click on accept quote button to see order details on the dashboard
#Design Review Update
When I login to surveyor site  
And I select the one of the site from the list 
And I click on Design Review option
And I Click on edit button to submit design review  
Then design review assessment should be submitted successfully

Examples: 
| plotdata                                                        | 
|\\\tpgfile\NewCompany\IT\Department\QA Team\Current Projects\CRISP\CrispAutomation\CrispData\Assessment\NewHomes-SRA.xlsx| 

#Verify Engineer Review (Should be submitted for  All HVS plots or  over 7 plots  or Ground Type is 'Yes' on SRA/RA or (Foundation Pile or Foundation Raft on SRA/RA))
@Smoketest
Scenario Outline: Verify Engineer Review
When I click on plus button and select  quote option
And I read following '<plotdata>' details
And I provide details on key site details page for PG Brand
And I provide details on other site details page 
And I provide following '<plotdata>' data on plot schedule page
And I provide details on the product details page
And I provide details on additional questions page
And I select relavant PG roles on roles page
When I provide the details on declaration page
Then I click on complete button to see the application submitted details on the dashboard
#Send Quote 
When I click on the send quote button 
And I verify the details on send quote rating page
And I verify the details on send quote securities page
And I verify the details on send quote conditions page
And I verify the details on send quote special terms page
And I verify the details on send quote fees page
And I verify the details on send quote file review page
And I verify the details on send quote endorsements page
And I verify the details on send quote confirm details page
And I verify the details on send quote correspondence page
Then I click on send quote button to see quoted details on the dashboard
#Accept Quote
When I click on acceptquote button 
And I verify the details on accept quote products page
And I verify the details on accept quote conditions page
And I verify the details on accept quote file review page 
And I verify the details on accept quote confirm page 
And I verify the details on accept quote internal roles page 
And I verify the details on accept quote correspondence page 
Then I click on accept quote button to see order details on the dashboard
#Engineer Review Update
When I login to surveyor site  
And I select the one of the site from the list 
And I click on Engineer Review option 
And I Click on edit button to engineer review  
Then engineer review assessment should be submitted successfully

Examples: 
| plotdata                                                        | 
|\\\tpgfile\NewCompany\IT\Department\QA Team\Current Projects\CRISP\CrispAutomation\CrispData\Assessment\NewHomes-SRA.xlsx| 

#Cover Notes
@Smoketest
Scenario Outline: Cover Notes on Surveyor
When I click on plus button and select  quote option
And I read following '<plotdata>' details
And I provide details on key site details page for PG Brand
And I provide details on other site details page 
And I provide following '<plotdata>' data on plot schedule page
And I provide details on the product details page
And I provide details on additional questions page
And I select relavant PG roles on roles page
When I provide the details on declaration page
Then I click on complete button to see the application submitted details on the dashboard
#Send Quote 
When I click on the send quote button 
And I verify the details on send quote rating page
And I verify the details on send quote securities page
And I verify the details on send quote conditions page
And I verify the details on send quote special terms page
And I verify the details on send quote fees page
And I verify the details on send quote file review page
And I verify the details on send quote endorsements page
And I verify the details on send quote confirm details page
And I verify the details on send quote correspondence page
Then I click on send quote button to see quoted details on the dashboard
#Accept Quote
When I click on acceptquote button 
And I verify the details on accept quote products page
And I verify the details on accept quote conditions page
And I verify the details on accept quote file review page 
And I verify the details on accept quote confirm page 
And I verify the details on accept quote internal roles page 
And I verify the details on accept quote correspondence page 
Then I click on accept quote button to see order details on the dashboard
#CoverNotes
When I login to surveyor site  
And I select the one of the site from the list 
And I click on site risk assessment option 
And I Click on edit button to submit the site risk assessment
Then Siterisk assessment should be submitted successfully
When I create new covernote 
Then the covernote should be added successfully

Examples: 
| plotdata                                                        | 
|\\\tpgfile\NewCompany\IT\Department\QA Team\Current Projects\CRISP\CrispAutomation\CrispData\Assessment\NewHomes-SRA.xlsx| 

#Verify all Surveyor Assessments
@Smoketest
Scenario Outline: Verify Submit Surveyor Assessment
When I click on plus button and select  quote option
And I read following '<plotdata>' details
And I provide details on key site details page for PG Brand
And I provide details on other site details page 
And I provide following '<plotdata>' data on plot schedule page
And I provide details on the product details page
And I provide details on additional questions page
And I select relavant PG roles on roles page
When I provide the details on declaration page
Then I click on complete button to see the application submitted details on the dashboard
#Send Quote 
When I click on the send quote button 
And I verify the details on send quote rating page
And I verify the details on send quote securities page
And I verify the details on send quote conditions page
And I verify the details on send quote special terms page
And I verify the details on send quote fees page
And I verify the details on send quote file review page
And I verify the details on send quote endorsements page
And I verify the details on send quote confirm details page
And I verify the details on send quote correspondence page
Then I click on send quote button to see quoted details on the dashboard
#Accept Quote
When I click on acceptquote button 
And I verify the details on accept quote products page
And I verify the details on accept quote conditions page
And I verify the details on accept quote file review page 
And I verify the details on accept quote confirm page 
And I verify the details on accept quote internal roles page 
And I verify the details on accept quote correspondence page 
Then I click on accept quote button to see order details on the dashboard
#Surveyor Assessments 
When I login to surveyor site to submit surveyor assessments
Then surveyor assessment should be submitted successfully

Examples: 
| plotdata                                                    |
|\\\tpgfile\NewCompany\IT\Department\QA Team\Current Projects\CRISP\CrispAutomation\CrispData\Assessment\NewHomes-SRA.xlsx| 

