﻿Feature: IntialNotice	
Background: 
	Given I am on login page
	When I enter crisp username 
	And I enter crisp password
	And I click on login button
	Then I should see all items displayed on dashboard page


#Verify Sending Intial Notice and Accepting Intial Notice PGQuote Brand For Residential Building control Product
@Smoketest
Scenario Outline: Verify Accepting Intial Notice for PG Quote Newhomes Residential building control Product
When I click on plus button and select  quote option
And I read following '<plotdata>' details
And I provide details on key site details page for PG Brand
And I provide details on other site details page 
And I provide following '<plotdata>' data on plot schedule page
And I provide details on the product details page
And I provide details on additional questions page
And I select relavant PG roles on roles page
When I provide the details on declaration page
Then I click on complete button to see the application submitted details on the dashboard
#Send Quote 
When I click on the send quote button 
And I verify the details on send quote rating page
And I verify the details on send quote securities page
And I verify the details on send quote conditions page
And I verify the details on send quote special terms page
And I verify the details on send quote fees page
And I verify the details on send quote file review page
And I verify the details on send quote endorsements page
And I verify the details on send quote confirm details page
And I verify the details on send quote correspondence page
Then I click on send quote button to see quoted details on the dashboard
#Accept Quote
When I click on acceptquote button  
And I verify the details on accept quote products page
And I verify the details on accept quote conditions page
And I verify the details on accept quote file review page 
And I verify the details on accept quote confirm page 
And I verify the details on accept quote internal roles page 
And I verify the details on accept quote correspondence page 
Then I click on accept quote button to see order details on the dashboard
#Sending Intial Notice
When I select files and documents page
And I upload sitelocation plan file type and surface storm and foul drainage layout  file type documents 
And I click on send intial notice button 
And I provide the details on initial notice qs page
And I verify the details on file review page 
And I verify the details on confirm page 
And I verify the details on intoclient page 
And I Verify the details on intola page
When I click on send initial notice button to complete sending intial notice
Then I should see the intial notice status has been sent
#Respond To Accept Intial Notice 
When I Select respond to initial notice button 
And I select the response type as accepted
And I verify the details on acceptance page 
And I verify the details on fliereview page 
And I verify details on confirm page
And I verify the initial notice acceptance details
When I click on send initial notice response -acceptance button 
Then I should view the resposne status on building control page.

Examples: 
 | plotdata                                                    | 
 |\\\tpgfile\NewCompany\IT\Department\QA Team\Current Projects\CRISP\CrispAutomation\CrispData\IntialNoticeData\NewHomes.xlsx|


#Verify Sending Intial Notice and Accepting Intial Notice on PGQuote Brand For Commercial Product

@Smoketest
Scenario Outline: Verify Accepting Intial Notice for PG Quote commercial building control Product
When I click on plus button and select  quote option
And I read following '<plotdata>' details
And I provide details on key site details page for PG Brand
And I provide details on other site details page 
And I provide following '<plotdata>' data on plot schedule page
And I provide details on the product details page
And I provide details on additional questions page
And I select relavant PG roles on roles page
When I provide the details on declaration page
Then I click on complete button to see the application submitted details on the dashboard
#Send Quote 
When I click on the send quote button 
And I verify the details on send quote rating page
And I verify the details on send quote securities page
And I verify the details on send quote conditions page
And I verify the details on send quote special terms page
And I verify the details on send quote fees page
And I verify the details on send quote file review page
And I verify the details on send quote endorsements page
And I verify the details on send quote confirm details page
And I verify the details on send quote correspondence page
Then I click on send quote button to see quoted details on the dashboard
#Accept Quote
When I click on acceptquote button 
And I verify the details on accept quote products page
And I verify the details on accept quote conditions page
And I verify the details on accept quote file review page 
And I verify the details on accept quote confirm page 
And I verify the details on accept quote internal roles page 
And I verify the details on accept quote correspondence page 
Then I click on accept quote button to see order details on the dashboard
#Sending Intial Notice
When I select files and documents page
And I upload sitelocation plan file type and surface storm and foul drainage layout  file type documents 
And I click on send intial notice button 
And I provide the details on initial notice qs page
And I verify the details on file review page 
And I verify the details on confirm page 
And I verify the details on intoclient page 
And I Verify the details on intola page
When I click on send initial notice button to complete sending intial notice
Then I should see the intial notice status has been sent
#Respond To Accept Intial Notice 
When I Select respond to initial notice button 
And I select the response type as accepted
And I verify the details on acceptance page 
And I verify the details on fliereview page 
And I verify details on confirm page
And I verify the initial notice acceptance details
When I click on send initial notice response -acceptance button 
Then I should view the resposne status on building control page.

Examples: 
 | plotdata                                                        |
 |\\\tpgfile\NewCompany\IT\Department\QA Team\Current Projects\CRISP\CrispAutomation\CrispData\IntialNoticeData\Commercials.xlsx| 



#Verify Sending Intial Notice and Rejecting Intial Notice PGQuote Brand For Residential Building control Product
@Smoketest
Scenario Outline: Verify Rejecting Intial Notice for PG Quote Newhomes Residential building control Product
When I click on plus button and select  quote option
And I read following '<plotdata>' details
And I provide details on key site details page for PG Brand
And I provide details on other site details page 
And I provide following '<plotdata>' data on plot schedule page
And I provide details on the product details page
And I provide details on additional questions page
And I select relavant PG roles on roles page
When I provide the details on declaration page
Then I click on complete button to see the application submitted details on the dashboard
#Send Quote 
When I click on the send quote button 
And I verify the details on send quote rating page
And I verify the details on send quote securities page
And I verify the details on send quote conditions page
And I verify the details on send quote special terms page
And I verify the details on send quote fees page
And I verify the details on send quote file review page
And I verify the details on send quote endorsements page
And I verify the details on send quote confirm details page
And I verify the details on send quote correspondence page
Then I click on send quote button to see quoted details on the dashboard
#Accept Quote
When I click on acceptquote button 
And I verify the details on accept quote products page
And I verify the details on accept quote conditions page
And I verify the details on accept quote file review page 
And I verify the details on accept quote confirm page 
And I verify the details on accept quote internal roles page 
And I verify the details on accept quote correspondence page 
Then I click on accept quote button to see order details on the dashboard
#Sending Intial Notice
When I select files and documents page
And I upload sitelocation plan file type and surface storm and foul drainage layout  file type documents
And I click on send intial notice button 
And I provide the details on initial notice qs page
And I verify the details on file review page 
And I verify the details on confirm page 
And I verify the details on intoclient page 
And I Verify the details on intola page
When I click on send initial notice button to complete sending intial notice
Then I should see the intial notice status has been sent 
#Respond To  Reject Intial Notice 
When I Select respond to initial notice button 
And I select the response type as rejected
And I verify the details on rejection page 
And I verify the details on fliereview page 
And I verify details on rejection confirm page
And I verify the initial notice rejection details
When I click on send initial notice response -reject button 
Then I should view the resposne status as rejected on building control page.

Examples: 
|plotdata                                                    | 
|\\\tpgfile\NewCompany\IT\Department\QA Team\Current Projects\CRISP\CrispAutomation\CrispData\IntialNoticeData\NewHomes.xlsx| 

