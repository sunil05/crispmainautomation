﻿// ------------------------------------------------------------------------------
//  <auto-generated>
//      This code was generated by SpecFlow (http://www.specflow.org/).
//      SpecFlow Version:1.9.0.77
//      SpecFlow Generator Version:1.9.0.0
//      Runtime Version:4.0.30319.42000
// 
//      Changes to this file may cause incorrect behavior and will be lost if
//      the code is regenerated.
//  </auto-generated>
// ------------------------------------------------------------------------------
#region Designer generated code
#pragma warning disable
namespace CrispRegression.Features
{
    using TechTalk.SpecFlow;
    
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("TechTalk.SpecFlow", "1.9.0.77")]
    [System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    [NUnit.Framework.TestFixtureAttribute()]
    [NUnit.Framework.DescriptionAttribute("InsuranceGroups")]
    public partial class InsuranceGroupsFeature
    {
        
        private static TechTalk.SpecFlow.ITestRunner testRunner;
        
#line 1 "InsuranceGroups.feature"
#line hidden
        
        [NUnit.Framework.TestFixtureSetUpAttribute()]
        public virtual void FeatureSetup()
        {
            testRunner = TechTalk.SpecFlow.TestRunnerManager.GetTestRunner();
            TechTalk.SpecFlow.FeatureInfo featureInfo = new TechTalk.SpecFlow.FeatureInfo(new System.Globalization.CultureInfo("en-US"), "InsuranceGroups", "", ProgrammingLanguage.CSharp, ((string[])(null)));
            testRunner.OnFeatureStart(featureInfo);
        }
        
        [NUnit.Framework.TestFixtureTearDownAttribute()]
        public virtual void FeatureTearDown()
        {
            testRunner.OnFeatureEnd();
            testRunner = null;
        }
        
        [NUnit.Framework.SetUpAttribute()]
        public virtual void TestInitialize()
        {
        }
        
        [NUnit.Framework.TearDownAttribute()]
        public virtual void ScenarioTearDown()
        {
            testRunner.OnScenarioEnd();
        }
        
        public virtual void ScenarioSetup(TechTalk.SpecFlow.ScenarioInfo scenarioInfo)
        {
            testRunner.OnScenarioStart(scenarioInfo);
        }
        
        public virtual void ScenarioCleanup()
        {
            testRunner.CollectScenarioErrors();
        }
        
        public virtual void FeatureBackground()
        {
#line 2
 #line 3
    testRunner.Given("I am on login page", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Given ");
#line 4
 testRunner.When("I enter crisp username", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line 5
 testRunner.And("I enter crisp password", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 6
 testRunner.And("I click on login button", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 7
 testRunner.Then("I should see all items displayed on dashboard page", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line hidden
        }
        
        [NUnit.Framework.TestAttribute()]
        [NUnit.Framework.DescriptionAttribute("Verify Issue All Certificates on AXA HVS Insurance PG HVS Products")]
        [NUnit.Framework.CategoryAttribute("Smoketest")]
        [NUnit.Framework.TestCaseAttribute("\\\\tpgfile\\NewCompany\\IT\\Department\\QA Team\\Current Projects\\CRISP\\CrispAutomation" +
            "\\CrispData\\PG-Products\\PG-AXA-HVS.xlsx", null)]
        public virtual void VerifyIssueAllCertificatesOnAXAHVSInsurancePGHVSProducts(string plotdata, string[] exampleTags)
        {
            string[] @__tags = new string[] {
                    "Smoketest"};
            if ((exampleTags != null))
            {
                @__tags = System.Linq.Enumerable.ToArray(System.Linq.Enumerable.Concat(@__tags, exampleTags));
            }
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("Verify Issue All Certificates on AXA HVS Insurance PG HVS Products", @__tags);
#line 10
this.ScenarioSetup(scenarioInfo);
#line 2
 this.FeatureBackground();
#line 11
testRunner.When("I click on plus button and select  quote option", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line 12
testRunner.And(string.Format("I read following \'{0}\' details", plotdata), ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 13
testRunner.And("I provide AXA HVS insurer on key site details page for PG Brand", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 14
testRunner.And("I provide details on other site details page", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 15
testRunner.And(string.Format("I provide following \'{0}\' data on plot schedule page", plotdata), ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 16
testRunner.And("I provide details on the product details page", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 17
testRunner.And("I provide details on additional questions page", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 18
testRunner.And("I select relavant PG roles on roles page", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 19
testRunner.When("I provide the details on declaration page", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line 20
testRunner.Then("I click on complete button to see the application submitted details on the dashbo" +
                    "ard", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 22
testRunner.When("I click on the send quote button", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line 23
testRunner.And("I verify the details on send quote rating page", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 24
testRunner.And("I verify the details on send quote securities page", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 25
testRunner.And("I verify the details on send quote conditions page", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 26
testRunner.And("I verify the details on send quote special terms page", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 27
testRunner.And("I verify the details on send quote fees page", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 28
testRunner.And("I verify the details on send quote file review page", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 29
testRunner.And("I verify the details on send quote endorsements page", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 30
testRunner.And("I verify the details on send quote confirm details page", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 31
testRunner.And("I verify the details on send quote correspondence page", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 32
testRunner.Then("I click on send quote button to see quoted details on the dashboard", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 34
testRunner.When("I click on acceptquote button", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line 35
testRunner.And("I verify the details on accept quote products page", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 36
testRunner.And("I verify the details on accept quote conditions page", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 37
testRunner.And("I verify the details on accept quote file review page", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 38
testRunner.And("I verify the details on accept quote confirm page", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 39
testRunner.And("I verify the details on accept quote internal roles page", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 40
testRunner.And("I verify the details on accept quote correspondence page", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 41
testRunner.Then("I click on accept quote button to see order details on the dashboard", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 43
testRunner.When("I verify account and make the balance payments", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line 45
testRunner.When("I clear security document conditions", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line 49
testRunner.When("I clear manual conditions", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line 51
testRunner.When("I clear PG additional conditions", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line 53
testRunner.When("I login to surveyor site to submit surveyor assessments", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line 55
testRunner.When("I login back to crisp application to issue the COI", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line 59
testRunner.Then("I login back to crisp application to issue the plotIC", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line hidden
            this.ScenarioCleanup();
        }
        
        [NUnit.Framework.TestAttribute()]
        [NUnit.Framework.DescriptionAttribute("Verify Issue All Certificates on Sompo Canopius Insurance PG HVS Products")]
        [NUnit.Framework.CategoryAttribute("Smoketest")]
        [NUnit.Framework.TestCaseAttribute("\\\\tpgfile\\NewCompany\\IT\\Department\\QA Team\\Current Projects\\CRISP\\CrispAutomation" +
            "\\CrispData\\PG-Products\\PG-SompoCanopius-HVS.xlsx", null)]
        public virtual void VerifyIssueAllCertificatesOnSompoCanopiusInsurancePGHVSProducts(string plotdata, string[] exampleTags)
        {
            string[] @__tags = new string[] {
                    "Smoketest"};
            if ((exampleTags != null))
            {
                @__tags = System.Linq.Enumerable.ToArray(System.Linq.Enumerable.Concat(@__tags, exampleTags));
            }
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("Verify Issue All Certificates on Sompo Canopius Insurance PG HVS Products", @__tags);
#line 68
this.ScenarioSetup(scenarioInfo);
#line 2
 this.FeatureBackground();
#line 69
testRunner.When("I click on plus button and select  quote option", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line 70
testRunner.And(string.Format("I read following \'{0}\' details", plotdata), ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 71
testRunner.And("I provide Sompo Canopius Insurer on key site details page for PG Brand", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 72
testRunner.And("I provide details on other site details page", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 73
testRunner.And(string.Format("I provide following \'{0}\' data on plot schedule page", plotdata), ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 74
testRunner.And("I provide details on the product details page", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 75
testRunner.And("I provide details on additional questions page", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 76
testRunner.And("I select relavant PG roles on roles page", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 77
testRunner.When("I provide the details on declaration page", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line 78
testRunner.Then("I click on complete button to see the application submitted details on the dashbo" +
                    "ard", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 80
testRunner.When("I click on the send quote button", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line 81
testRunner.And("I verify the details on send quote rating page", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 82
testRunner.And("I verify the details on send quote securities page", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 83
testRunner.And("I verify the details on send quote conditions page", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 84
testRunner.And("I verify the details on send quote special terms page", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 85
testRunner.And("I verify the details on send quote fees page", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 86
testRunner.And("I verify the details on send quote file review page", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 87
testRunner.And("I verify the details on send quote endorsements page", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 88
testRunner.And("I verify the details on send quote confirm details page", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 89
testRunner.And("I verify the details on send quote correspondence page", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 90
testRunner.Then("I click on send quote button to see quoted details on the dashboard", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 92
testRunner.When("I click on acceptquote button", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line 93
testRunner.And("I verify the details on accept quote products page", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 94
testRunner.And("I verify the details on accept quote conditions page", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 95
testRunner.And("I verify the details on accept quote file review page", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 96
testRunner.And("I verify the details on accept quote confirm page", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 97
testRunner.And("I verify the details on accept quote internal roles page", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 98
testRunner.And("I verify the details on accept quote correspondence page", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 99
testRunner.Then("I click on accept quote button to see order details on the dashboard", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 101
testRunner.When("I verify account and make the balance payments", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line 103
testRunner.When("I clear security document conditions", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line 107
testRunner.When("I clear manual conditions", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line 109
testRunner.When("I clear PG additional conditions", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line 111
testRunner.When("I login to surveyor site to submit surveyor assessments", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line 113
testRunner.When("I login back to crisp application to issue the COI", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line 117
testRunner.Then("I login back to crisp application to issue the plotIC", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line hidden
            this.ScenarioCleanup();
        }
        
        [NUnit.Framework.TestAttribute()]
        [NUnit.Framework.DescriptionAttribute("Verify Issue All Certificates on Sompo Canopius Insurance LABC HVS Products")]
        [NUnit.Framework.CategoryAttribute("Smoketest")]
        [NUnit.Framework.TestCaseAttribute("\\\\tpgfile\\NewCompany\\IT\\Department\\QA Team\\Current Projects\\CRISP\\CrispAutomation" +
            "\\CrispData\\LABC-Products\\LABC-SompoCanopius-HVS.xlsx", null)]
        public virtual void VerifyIssueAllCertificatesOnSompoCanopiusInsuranceLABCHVSProducts(string plotdata, string[] exampleTags)
        {
            string[] @__tags = new string[] {
                    "Smoketest"};
            if ((exampleTags != null))
            {
                @__tags = System.Linq.Enumerable.ToArray(System.Linq.Enumerable.Concat(@__tags, exampleTags));
            }
            TechTalk.SpecFlow.ScenarioInfo scenarioInfo = new TechTalk.SpecFlow.ScenarioInfo("Verify Issue All Certificates on Sompo Canopius Insurance LABC HVS Products", @__tags);
#line 125
this.ScenarioSetup(scenarioInfo);
#line 2
 this.FeatureBackground();
#line 126
testRunner.When("I click on plus button and select  quote option", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line 127
testRunner.And(string.Format("I read following \'{0}\' details", plotdata), ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 128
testRunner.And("I provide Sompo Canopius Insurer on key site details page", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 129
testRunner.And("I provide details on other site details page", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 130
testRunner.And(string.Format("I provide following \'{0}\' data on plot schedule page", plotdata), ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 131
testRunner.And("I provide details on the product details page", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 132
testRunner.And("I provide details on additional questions page", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 133
testRunner.And("I select relavant roles on roles page", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 134
testRunner.When("I provide the details on declaration page", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line 135
testRunner.Then("I click on complete button to see the application submitted details on the dashbo" +
                    "ard", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 137
testRunner.When("I click on sendquote button", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line 138
testRunner.And("I verify the details on send quote rating page", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 139
testRunner.And("I verify the details on send quote securities page", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 140
testRunner.And("I verify the details on send quote conditions page", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 141
testRunner.And("I verify the details on send quote special terms page", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 142
testRunner.And("I verify the details on send quote fees page", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 143
testRunner.And("I verify the details on send quote file review page", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 144
testRunner.And("I verify the details on send quote endorsements page", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 145
testRunner.And("I verify the details on send quote confirm details page", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 146
testRunner.And("I verify the details on send quote correspondence page", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 147
testRunner.Then("I click on send quote button to see quoted details on the dashboard", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 149
testRunner.When("I click on acceptquote button", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line 150
testRunner.And("I verify the details on accept quote products page", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 151
testRunner.And("I verify the details on accept quote conditions page", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 152
testRunner.And("I verify the details on accept quote file review page", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 153
testRunner.And("I verify the details on accept quote confirm page", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 154
testRunner.And("I verify the details on accept quote internal roles page", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 155
testRunner.And("I verify the details on accept quote correspondence page", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "And ");
#line 156
testRunner.Then("I click on accept quote button to see order details on the dashboard", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 158
testRunner.When("I verify account and make the balance payments", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line 160
testRunner.When("I clear security document conditions", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line 164
testRunner.When("I clear manual conditions", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line 166
testRunner.When("I clear LABC additional conditions", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line 168
testRunner.When("I login to surveyor site to submit surveyor assessments", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line 170
testRunner.When("I login back to crisp application to issue the COI", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "When ");
#line 172
testRunner.Then("I login back to crisp application to issue the DevIC", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line 174
testRunner.Then("I login back to crisp application to issue the plotIC", ((string)(null)), ((TechTalk.SpecFlow.Table)(null)), "Then ");
#line hidden
            this.ScenarioCleanup();
        }
    }
}
#pragma warning restore
#endregion
