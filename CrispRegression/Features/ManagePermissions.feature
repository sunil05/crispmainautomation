﻿Feature: ManagePermissions
Background: 
	Given I am on login page
	When I enter crisp username 
	And I enter crisp password
	And I click on login button
	Then I should see all items displayed on dashboard page	

@Smoketest
Scenario: Remove All Permissions 
	Given as a admin remove all permissions on user
	Then I should verify user not able to perform any operations

