﻿Feature: RoadAndSewerBonds
Background: 
	Given I am on login page
	When I enter crisp username 
	And I enter crisp password
	And I click on login button
	Then I should see all items displayed on dashboard page

@Smoketest
Scenario Outline: Create RoadBond on Labc Brand
When I click on plus button and select  quote option
And I read following '<plotdata>' details
And I provide details on key site details page 
And I provide details on other site details page 
And I provide following '<plotdata>' data on plot schedule page
And I provide details on the product details page
And I provide details on additional questions page
And I select relavant roles on roles page
When I provide the details on declaration page
Then I click on complete button to see the application submitted details on the dashboard
#RoadBond
When I select site details on quote 
Then I can create road bond


Examples: 
| plotdata                                                                                                                                              |
|\\\tpgfile\NewCompany\IT\Department\QA Team\Current Projects\CRISP\CrispAutomation\CrispData\LABC-Products\NewHomes.xlsx |

@Smoketest
Scenario Outline: Create SewerBond Labc Brand
When I click on plus button and select  quote option
And I read following '<plotdata>' details
And I provide details on key site details page 
And I provide details on other site details page 
And I provide following '<plotdata>' data on plot schedule page
And I provide details on the product details page
And I provide details on additional questions page
And I select relavant roles on roles page
When I provide the details on declaration page
Then I click on complete button to see the application submitted details on the dashboard
#SewerBond
When I select site details on quote 
Then I can create sewer bond


Examples: 
| plotdata                                                                                                                                              |
|\\\tpgfile\NewCompany\IT\Department\QA Team\Current Projects\CRISP\CrispAutomation\CrispData\LABC-Products\NewHomes.xlsx |

@Smoketest
Scenario Outline: Create SewerBond on PG Brand
When I click on plus button and select  quote option
And I read following '<plotdata>' details
And I provide details on key site details page for PG Brand
And I provide details on other site details page 
And I provide following '<plotdata>' data on plot schedule page
And I provide details on the product details page
And I provide details on additional questions page
And I select relavant PG roles on roles page
When I provide the details on declaration page
Then I click on complete button to see the application submitted details on the dashboard
#SewerBond
When I select site details on quote 
Then I can create sewer bond


Examples: 
| plotdata                                                                                                                                           |
|\\\tpgfile\NewCompany\IT\Department\QA Team\Current Projects\CRISP\CrispAutomation\CrispData\PG-Products\NewHomes.xlsx |


@Smoketest
Scenario Outline: Create RoadBond on PG Brand
When I click on plus button and select  quote option
And I read following '<plotdata>' details
And I provide details on key site details page for PG Brand
And I provide details on other site details page 
And I provide following '<plotdata>' data on plot schedule page
And I provide details on the product details page
And I provide details on additional questions page
And I select relavant PG roles on roles page
When I provide the details on declaration page
Then I click on complete button to see the application submitted details on the dashboard
#RoadBond
When I select site details on quote 
Then I can create road bond


Examples: 
| plotdata                                                                                                                                           |
|\\\tpgfile\NewCompany\IT\Department\QA Team\Current Projects\CRISP\CrispAutomation\CrispData\PG-Products\NewHomes.xlsx |


@Smoketest
Scenario Outline: Conditions on RoadBond on PG Brand
When I click on plus button and select  quote option
And I read following '<plotdata>' details
And I provide details on key site details page for PG Brand
And I provide details on other site details page 
And I provide following '<plotdata>' data on plot schedule page
And I provide details on the product details page
And I provide details on additional questions page
And I select relavant PG roles on roles page
When I provide the details on declaration page
Then I click on complete button to see the application submitted details on the dashboard
#RoadBond
When I select site details on quote 
When I create conditions on bond
Then I should verify conditions on bond


Examples: 
| plotdata                                                                                                                                           |
|\\\tpgfile\NewCompany\IT\Department\QA Team\Current Projects\CRISP\CrispAutomation\CrispData\PG-Products\NewHomes.xlsx |

@Smoketest
Scenario Outline: Accept RoadBond on PG Brand
When I click on plus button and select  quote option
And I read following '<plotdata>' details
And I provide details on key site details page for PG Brand
And I provide details on other site details page 
And I provide following '<plotdata>' data on plot schedule page
And I provide details on the product details page
And I provide details on additional questions page
And I select relavant PG roles on roles page
When I provide the details on declaration page
Then I click on complete button to see the application submitted details on the dashboard
#RoadBond
When I select site details on quote 
When I create single bond
Then I should accept the bond


Examples: 
| plotdata                                                                                                                                           |
|\\\tpgfile\NewCompany\IT\Department\QA Team\Current Projects\CRISP\CrispAutomation\CrispData\PG-Products\NewHomes.xlsx |

@Smoketest
Scenario Outline: Rejecte RoadBond on PG Brand
When I click on plus button and select  quote option
And I read following '<plotdata>' details
And I provide details on key site details page for PG Brand
And I provide details on other site details page 
And I provide following '<plotdata>' data on plot schedule page
And I provide details on the product details page
And I provide details on additional questions page
And I select relavant PG roles on roles page
When I provide the details on declaration page
Then I click on complete button to see the application submitted details on the dashboard
#RoadBond
When I select site details on quote 
When I create single bond
Then I should reject the bond


Examples: 
| plotdata                                                                                                                                           |
|\\\tpgfile\NewCompany\IT\Department\QA Team\Current Projects\CRISP\CrispAutomation\CrispData\PG-Products\NewHomes.xlsx |