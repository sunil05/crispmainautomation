﻿Feature:Person

Background: 
    Given I am on login page
	When I enter crisp username 
	And I enter crisp password
	And I click on login button
	Then I should see all items displayed on dashboard page
	
@SmokeTest
Scenario: Add a Person
	When I click on add button to create a person
	And Select Person Option
	And I enter person details on details page 
	And I enter address details on addresses page
	And I enter socialmedia details on social page
	When I click on save button to save person details
	Then I should see person details on the dashboard 

@SmokeTest
Scenario: Amend a Person Details
	When I click on add button to create a person
	And Select Person Option
	And I enter person details on details page 
	And I enter address details on addresses page
	And I enter socialmedia details on social page
	When I click on save button to save person details
	Then I should see person details on the dashboard 
	#Edit Person Details 
	When I click on edit button to edit person details
	And I edit the person details on details page
	And I edit the existing address details and add new address details 
	And I edit the socialmedia details on social page 
	When I click on save button to update the person details	
	Then I should see updated contact details on the dashboard

@SmokeTest
Scenario: Add Rating To a Person
	When I click on add button to create a person
	And Select Person Option
	And I enter person details on details page 
	And I enter address details on addresses page
	And I enter socialmedia details on social page
	When I click on save button to save person details
	Then I should see person details on the dashboard
	#Add Rating To Person
	When I click on rating option to provide rating to a person 
	And I select add rating button on person page from top right corner
	And I provide the details on details page 
	And I provide the details on warranty providers page 
	And I provide the details on developments page
	And I provide the details on prospective business page  
	And I provide the details on claims history page
	And I provide the details on scores details page 
	And I click on the confirm rating button on the wizard
	Then I should see the rating details for person

	@SmokeTest
Scenario: Amend Rating Details To a Person
	When I click on add button to create a person
	And Select Person Option
	And I enter person details on details page 
	And I enter address details on addresses page
	And I enter socialmedia details on social page
	When I click on save button to save person details
	Then I should see person details on the dashboard
	#Add Rating To Person
	When I click on rating option to provide rating to a person 
	And I select add rating button on person page from top right corner
	And I provide the details on details page 
	And I provide the details on warranty providers page 
	And I provide the details on developments page
	And I provide the details on prospective business page  
	And I provide the details on claims history page
	And I provide the details on scores details page 
	And I click on the confirm rating button on the wizard
	Then I should see the rating details for person
	#Edit Rating To Person
	When I click on rating option to edit rating details to person
	And I select edit rating button on person rating page 
	And I edit the details on rating details page 
	And I edit the details on warranty providers page 
	And I edit the details on developments page
	And I edit the details on prospective business page  
	And I edit the details on claims history page			
	And I edit the details on scores details page 
	When I click on the confirm rating button to update amended rating details
	Then I should see updated rating details for the person

@SmokeTest
Scenario: Verify Searching Contact
   	When I click on add button to create a person
	And Select Person Option
	And I enter person details on details page 
	And I enter address details on addresses page
	And I enter socialmedia details on social page
	When I click on save button to save person details
	Then I should see person details on the dashboard 
    When I search for contacts
	Then I should see contact details 



