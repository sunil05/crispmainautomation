﻿Feature: RegistrationsDocuments
Background: 
    Given I am on login page
	When I enter crisp username 
	And I enter crisp password
	And I click on login button
	Then I should see all items displayed on dashboard page


#PG Regestrations on Company Feature
@Smoketest
Scenario: Verify PG Registration Documents on person
Given I have created a person 
When I select PG registration tab
When I add all type of security documents
Then I should downloand the registration documents and compare it.

#PG Regestrations on Company Feature
@Smoketest
Scenario: Verify PG Registration Document on Company
Given I have created a company 
When I select PG registration tab
When I add a role on Roles page
When I add all type of security documents
Then I should downloand the registration documents and compare it.

#PG Regestrations on Company Feature
@Smoketest
Scenario: Verify LABC Registration Document on person
Given I have created a person 
When I select LABC registration tab
When I add all type of security documents
Then I should downloand the registration documents and compare it.

#PG Regestrations on Company Feature
@Smoketest
Scenario: Verify LABC Registration Document on Company
Given I have created a company 
When I select LABC registration tab
When I add a role on Roles page
When I add all type of security documents
Then I should downloand the registration documents and compare it.

#LABC Site Specific Regestrations Documents 
@Smoketest
Scenario Outline: Verify LABC Registration SiteSpecific Documents
When I click on plus button and select  quote option
And I read following '<plotdata>' details
And I provide details on key site details page
And I provide details on other site details page 
And I provide following '<plotdata>' data on plot schedule page
And I provide details on the product details page
And I provide details on additional questions page
And I select relavant roles on roles page
When I provide the details on declaration page
Then I click on complete button to see the application submitted details on the dashboard
#Send Quote 
When I click on sendquote button 
And I verify the details on send quote rating page
And I verify the details on send quote securities page
And I verify the details on send quote conditions page
And I verify the details on send quote special terms page
And I verify the details on send quote fees page
And I verify the details on send quote file review page
And I verify the details on send quote endorsements page
And I verify the details on send quote confirm details page
And I verify the details on send quote correspondence page
Then I click on send quote button to see quoted details on the dashboard
#Accept Quote
When I click on acceptquote button 
And I verify the details on accept quote products page
And I verify the details on accept quote conditions page
And I verify the details on accept quote file review page 
And I verify the details on accept quote confirm page 
And I verify the details on accept quote internal roles page 
And I verify the details on accept quote correspondence page 
Then I click on accept quote button to see order details on the dashboard
#Security Document condition
When I verify site specific security document conditions 
Then I should downloand the registration documents and compare it.

Examples: 
| plotdata                                                                                                    |
|\\\tpgfile\NewCompany\IT\Department\QA Team\Current Projects\CRISP\CrispAutomation\CrispData\LABC-Products\SelfBuild.xlsx|


#PG Site Specific Regestrations Documents 
@Smoketest
Scenario Outline: Verify PG Registration SiteSpecific Documents
When I click on plus button and select  quote option
And I read following '<plotdata>' details
And I provide details on key site details page for PG Brand
And I provide details on other site details page 
And I provide following '<plotdata>' data on plot schedule page
And I provide details on the product details page
And I provide details on additional questions page
And I select relavant PG roles on roles page
When I provide the details on declaration page
Then I click on complete button to see the application submitted details on the dashboard
#Send Quote 
When I click on the send quote button 
And I verify the details on send quote rating page
And I verify the details on send quote securities page
And I verify the details on send quote conditions page
And I verify the details on send quote special terms page
And I verify the details on send quote fees page
And I verify the details on send quote file review page
And I verify the details on send quote endorsements page
And I verify the details on send quote confirm details page
And I verify the details on send quote correspondence page
Then I click on send quote button to see quoted details on the dashboard
#Accept Quote
When I click on acceptquote button 
And I verify the details on accept quote products page
And I verify the details on accept quote conditions page
And I verify the details on accept quote file review page 
And I verify the details on accept quote confirm page 
And I verify the details on accept quote internal roles page 
And I verify the details on accept quote correspondence page 
Then I click on accept quote button to see order details on the dashboard
#Security Document condition
When I verify site specific security document conditions 
Then I should downloand the registration documents and compare it.

Examples: 
| plotdata                                                                                                    |
|\\\tpgfile\NewCompany\IT\Department\QA Team\Current Projects\CRISP\CrispAutomation\CrispData\PG-Products\SelfBuild.xlsx |
