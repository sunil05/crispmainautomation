﻿Feature:AddCompany
Background: 
	Given I am on login page
	When I enter crisp username 
	And I enter crisp password
	And I click on login button
	Then I should see all items displayed on dashboard page


@SmokeTest
Scenario Outline:Add Company Manually 
	When I click on add button to add company
	And select Company Option
	And I enter a '<companyname>' to create a company on search existing company page 
	And I click on No Existing Match Found button 	
	And I click on No Match Found on companies house page
	And I enter company details and social media details manually 
	And I add the office information on Offices Page  	
	And I add employee on employees page	
	When I click on Save button to save above created company
	Then I should see company details on the dashborad 
	
Examples: 
| companyname | editcompanyname |
| MDISCompany | MDISTestCompany |

@SmokeTest
Scenario Outline: Verify adding Company from crispdatabse
	When I click on add button to add company
	And select Company Option
	And I enter a '<companyname>' to create a company on search existing company page 
	And I select on existing company from the crisp database list 
	Then I should see company details on the dashborad 

Examples: 
| companyname | 
| MDIS    | 

@SmokeTest
Scenario Outline: Verify adding Company from companieshouse
	When I click on add button to add company
	And select Company Option
	And I enter a '<companyname>' to create a company on search existing company page 
	And I click on No Existing Match Found button 		
	And I select on existing company from the companieshouse database list 
	Then I should see company details on the dashborad 	

Examples: 
| companyname | 
| google         | 

@SmokeTest
Scenario Outline:Amend Company Details 
	When I click on add button to add company
	And select Company Option
	And I enter a '<companyname>' to create a company on search existing company page 
	And I click on No Existing Match Found button 	
	And I click on No Match Found on companies house page
	And I enter company details and social media details manually 
	And I add the office information on Offices Page  	
	And I add employee on employees page	
	When I click on Save button to save above created company
	Then I should see company details on the dashborad 
	#Edit Company Details 
	When I select edit button to edit company details 
	And I edit company details on details page '<editcompanyname>'	
	And I edit existing office details and add new office details	
	When I click on Save button to update the company details 
	Then I should see updated company details on the dashboard 	
	#Add Employees and Edit Existing Employees on Company Page
	When I select employees option 
	And I edit existing employee details and add new employee details	
	Then I should see new employee details on the company dashboard	
	
Examples: 
| companyname | editcompanyname |
| MDISCompany | MDISTestCompany |

@SmokeTest
Scenario Outline:Add Rating To Company 
	When I click on add button to add company
	And select Company Option
	And I enter a '<companyname>' to create a company on search existing company page 
	And I click on No Existing Match Found button 	
	And I click on No Match Found on companies house page
	And I enter company details and social media details manually 
	And I add the office information on Offices Page  	
	And I add employee on employees page	
	When I click on Save button to save above created company
	Then I should see company details on the dashborad
	# Add Rating To Company 
	When I click on rating option
	And I select add rating button on companypage from top right corner
	And I provide rating details on details page 
	And I provide details on warranty providers page 
	And I provide details on directors page 
	And I provide details on developments page
	And I provide details on prospective business page  
	And I provide details on claims history page
	And I provide details on group page 
	And I provide details on scores details page 
	And I click on confirm rating button on the wizard
	Then I should see the rating details for company	
	
Examples: 
| companyname | editcompanyname |
| MDISCompany | MDISTestCompany |

@SmokeTest
Scenario Outline:Amend Rating To Company 
	When I click on add button to add company
	And select Company Option
	And I enter a '<companyname>' to create a company on search existing company page 
	And I click on No Existing Match Found button 	
	And I click on No Match Found on companies house page
	And I enter company details and social media details manually 
	And I add the office information on Offices Page  	
	And I add employee on employees page	
	When I click on Save button to save above created company
	Then I should see company details on the dashborad
	# Add Rating To Company 
	When I click on rating option
	And I select add rating button on companypage from top right corner
	And I provide rating details on details page 
	And I provide details on warranty providers page 
	And I provide details on directors page 
	And I provide details on developments page
	And I provide details on prospective business page  
	And I provide details on claims history page
	And I provide details on group page 
	And I provide details on scores details page 
	And I click on confirm rating button on the wizard
	Then I should see the rating details for company
	#Edit Rating To Company
	When I click on rating option to edit rating
	And I select edit rating button 
	And I edit rating details on details page 
	And I edit details on warranty providers page 
	And I edit details on directors page 
	And I edit details on developments page
	And I edit details on prospective business page  
	And I edit details on claims history page
	And I edit details on group page 		
	And I edit details on scores details page 
	When I click on confirm rating button to update amended rating details
	Then I should see updated rating details for the company 

	
Examples: 
| companyname | editcompanyname |
| MDISCompany | MDISTestCompany |