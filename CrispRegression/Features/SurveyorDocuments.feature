﻿Feature: SurveyorDocuments
Background: 
	Given I am on login page
	When I enter crisp username 
	And I enter crisp password
	And I click on login button
	Then I should see all items displayed on dashboard page
	
@Smoketest
Scenario Outline: Verify Surveyor Documents on SRA
When I click on plus button and select  quote option
And I read following '<plotdata>' details
And I provide details on key site details page for PG Brand
And I provide details on other site details page 
And I provide following '<plotdata>' data on plot schedule page
And I provide details on the product details page
And I provide details on additional questions page
And I select relavant PG roles on roles page
When I provide the details on declaration page
Then I click on complete button to see the application submitted details on the dashboard
#Send Quote 
When I click on the send quote button 
And I verify the details on send quote rating page
And I verify the details on send quote securities page
And I verify the details on send quote conditions page
And I verify the details on send quote special terms page
And I verify the details on send quote fees page
And I verify the details on send quote file review page
And I verify the details on send quote endorsements page
And I verify the details on send quote confirm details page
And I verify the details on send quote correspondence page
Then I click on send quote button to see quoted details on the dashboard
#Accept Quote
When I click on acceptquote button 
And I verify the details on accept quote products page
And I verify the details on accept quote conditions page
And I verify the details on accept quote file review page 
And I verify the details on accept quote confirm page 
And I verify the details on accept quote internal roles page 
And I verify the details on accept quote correspondence page 
Then I click on accept quote button to see order details on the dashboard
#SurveyorDocsSRA -Create Docs and Close Docs on SRA 
When I login to surveyor and close all documents if they open already
And I select the one of the site from the list 
And I upload docs on SRA assessments
And I select the one of the site from the list
Then I close docs on SRA assessments
When I select the one of the site from the list
Then I should verify the closed SRA documents 
Then I should complete the surveyor assessments
#SurveyorDocsSRA -Create Docs and Close Docs on DR
When I select the one of the site from the list 
And I upload docs on SRA assessments
And I select the one of the site from the list 
Then I close docs on DR assessments 
#SurveyorDocsSRA -Create Docs and Close Docs on RA
When I select the one of the site from the list 
And I upload docs on SRA assessments
And I select the one of the site from the list 
Then I can not close docs on RA assessments 
#SurveyorDocsSRA -Create Docs and Close Docs on ER
When I select the one of the site from the list 
Then I can not close docs on ER assessments 
#SurveyorDocsSRA -Create Docs and Close Docs on Site Inspection
#When I select the one of the site from the list 
#And I upload docs on SRA assessments
#When I select the one of the site from the list 
#Then I close docs on SiteInspection assessments 
Examples: 
| plotdata                                                    |
|\\\tpgfile\NewCompany\IT\Department\QA Team\Current Projects\CRISP\CrispAutomation\CrispData\Assessment\NewHomes-SRA.xlsx| 

#Verify Refurbishment Assessment on all products when construction type is selected "Conversion"
@Smoketest
Scenario Outline: Verify Surveyor Documents on RA
When I click on plus button and select  quote option
And I read following '<plotdata>' details
And I provide details on key site details page for PG Brand
And I provide details on other site details page 
And I provide following '<plotdata>' data on plot schedule page
And I provide details on the product details page
And I provide details on additional questions page
And I select relavant PG roles on roles page
When I provide the details on declaration page
Then I click on complete button to see the application submitted details on the dashboard
#Send Quote 
When I click on the send quote button 
And I verify the details on send quote rating page
And I verify the details on send quote securities page
And I verify the details on send quote conditions page
And I verify the details on send quote special terms page
And I verify the details on send quote fees page
And I verify the details on send quote file review page
And I verify the details on send quote endorsements page
And I verify the details on send quote confirm details page
And I verify the details on send quote correspondence page
Then I click on send quote button to see quoted details on the dashboard
#Accept Quote
When I click on acceptquote button 
And I verify the details on accept quote products page
And I verify the details on accept quote conditions page
And I verify the details on accept quote file review page 
And I verify the details on accept quote confirm page 
And I verify the details on accept quote internal roles page 
And I verify the details on accept quote correspondence page 
Then I click on accept quote button to see order details on the dashboard
#SurveyorDocsOnRA - Create Docs and Close Docs on RA
When I login to surveyor and close all documents if they open already
And I select the one of the site from the list 
And I upload docs on RA assessments
And I select the one of the site from the list 
Then I close docs on RA assessments 
When I select the one of the site from the list 
Then I should verify the closed RA documents 
Then I should complete the surveyor assessments
#SurveyorDocsOnRA - Create Docs and Close Docs on ER
When I select the one of the site from the list 
And I upload docs on RA assessments
And I select the one of the site from the list 
Then I can not close docs on ER assessments
#SurveyorDocsOnRA - Create Docs and Close Docs on SRA
When I select the one of the site from the list 
Then I can not close docs on SRA assessments
#SurveyorDocsOnRA - Create Docs and Close Docs on DR
When I select the one of the site from the list 
Then I can not close docs on DR assessments
#SurveyorDocsOnRA - Create Docs and Close Docs on Site Inspection
#When I select the one of the site from the list 
#And I upload docs on RA assessments
#And I select the one of the site from the list 
#Then I close docs on SiteInspection assessments 
Examples: 
| plotdata                                                                                              | 
|\\\tpgfile\NewCompany\IT\Department\QA Team\Current Projects\CRISP\CrispAutomation\CrispData\Assessment\NewHomes-Refurbishment.xlsx| 

#Verify Design Review (Should be submitted for over 100+ or All HVS plots or  All BC plots)
@Smoketest
Scenario Outline: Verify Surveyor Documents on DR
When I click on plus button and select  quote option
And I read following '<plotdata>' details
And I provide details on key site details page for PG Brand
And I provide details on other site details page 
And I provide following '<plotdata>' data on plot schedule page
And I provide details on the product details page
And I provide details on additional questions page
And I select relavant PG roles on roles page
When I provide the details on declaration page
Then I click on complete button to see the application submitted details on the dashboard
#Send Quote 
When I click on the send quote button 
And I verify the details on send quote rating page
And I verify the details on send quote securities page
And I verify the details on send quote conditions page
And I verify the details on send quote special terms page
And I verify the details on send quote fees page
And I verify the details on send quote file review page
And I verify the details on send quote endorsements page
And I verify the details on send quote confirm details page
And I verify the details on send quote correspondence page
Then I click on send quote button to see quoted details on the dashboard
#Accept Quote
When I click on acceptquote button 
And I verify the details on accept quote products page
And I verify the details on accept quote conditions page
And I verify the details on accept quote file review page 
And I verify the details on accept quote confirm page 
And I verify the details on accept quote internal roles page 
And I verify the details on accept quote correspondence page 
Then I click on accept quote button to see order details on the dashboard
#SurveyorDocsDR -Create Docs and Close Docs on DR
When I login to surveyor and close all documents if they open already
And I select the one of the site from the list 
And I upload docs on DR assessments
And I select the one of the site from the list 
Then I close docs on DR assessments
When I select the one of the site from the list 
Then I should verify the closed DR documents 
Then I should complete the surveyor assessments 
#SurveyorDocsDR -Create Docs and Close Docs on SRA
When I select the one of the site from the list 
And I upload docs on DR assessments
And I select the one of the site from the list 
Then I can not close docs on SRA assessments 
#SurveyorDocsDR -Create Docs and Close Docs on RA
When I select the one of the site from the list 
Then I can not close docs on RA assessments 
#SurveyorDocsDR -Create Docs and Close Docs on ER
When I select the one of the site from the list 
Then I can not close docs on ER assessments 
#SurveyorDocsDR -Create Docs and Close Docs on Site Inspection
#When I select the one of the site from the list 
#And I upload docs on DR assessments
#When I select the one of the site from the list 
#Then I close docs on SiteInspection assessments

Examples: 
| plotdata                                                        | 
|\\\tpgfile\NewCompany\IT\Department\QA Team\Current Projects\CRISP\CrispAutomation\CrispData\Assessment\NewHomes-SRA.xlsx| 

#Verify Engineer Review (Should be submitted for  All HVS plots and over 7 plots)
@Smoketest
Scenario Outline: Verify Surveyor Documents on ER
When I click on plus button and select  quote option
And I read following '<plotdata>' details
And I provide details on key site details page for PG Brand
And I provide details on other site details page 
And I provide following '<plotdata>' data on plot schedule page
And I provide details on the product details page
And I provide details on additional questions page
And I select relavant PG roles on roles page
When I provide the details on declaration page
Then I click on complete button to see the application submitted details on the dashboard
#Send Quote 
When I click on the send quote button 
And I verify the details on send quote rating page
And I verify the details on send quote securities page
And I verify the details on send quote conditions page
And I verify the details on send quote special terms page
And I verify the details on send quote fees page
And I verify the details on send quote file review page
And I verify the details on send quote endorsements page
And I verify the details on send quote confirm details page
And I verify the details on send quote correspondence page
Then I click on send quote button to see quoted details on the dashboard
#Accept Quote
When I click on acceptquote button 
And I verify the details on accept quote products page
And I verify the details on accept quote conditions page
And I verify the details on accept quote file review page 
And I verify the details on accept quote confirm page 
And I verify the details on accept quote internal roles page 
And I verify the details on accept quote correspondence page 
Then I click on accept quote button to see order details on the dashboard
#SurveyorDocsER -Create Docs and Close Docs on ER
When I login to surveyor and close all documents if they open already
And I select the one of the site from the list 
And I upload docs on ER assessments
And I select the one of the site from the list 
Then I close docs on ER assessments 
When I select the one of the site from the list 
Then I should verify the closed ER documents 
Then I should complete the surveyor assessments
#SurveyorDocsER -Create Docs and Close Docs on SRA
When I select the one of the site from the list 
And I upload docs on ER assessments
And I select the one of the site from the list 
Then I can not close docs on SRA assessments 
#SurveyorDocsER -Create Docs and Close Docs on RA
When I select the one of the site from the list 
Then I can not close docs on RA assessments 
#SurveyorDocsER -Create Docs and Close Docs on DR
When I select the one of the site from the list 
Then I can not close docs on DR assessments 
#SurveyorDocsER -Create Docs and Close Docs on Site Inspection
#When I select the one of the site from the list 
#And I upload docs on ER assessments
#When I select the one of the site from the list 
#Then I close docs on SiteInspection assessments 

Examples: 
| plotdata                                                        | 
|\\\tpgfile\NewCompany\IT\Department\QA Team\Current Projects\CRISP\CrispAutomation\CrispData\Assessment\NewHomes-SRA.xlsx| 

#Verify SiteInspection Documents 
@Smoketest
Scenario Outline: Verify Surveyor Documents on SiteInspection
When I click on plus button and select  quote option
And I read following '<plotdata>' details
And I provide details on key site details page for PG Brand
And I provide details on other site details page 
And I provide following '<plotdata>' data on plot schedule page
And I provide details on the product details page
And I provide details on additional questions page
And I select relavant PG roles on roles page
When I provide the details on declaration page
Then I click on complete button to see the application submitted details on the dashboard
#Send Quote 
When I click on the send quote button 
And I verify the details on send quote rating page
And I verify the details on send quote securities page
And I verify the details on send quote conditions page
And I verify the details on send quote special terms page
And I verify the details on send quote fees page
And I verify the details on send quote file review page
And I verify the details on send quote endorsements page
And I verify the details on send quote confirm details page
And I verify the details on send quote correspondence page
Then I click on send quote button to see quoted details on the dashboard
#Accept Quote
When I click on acceptquote button 
And I verify the details on accept quote products page
And I verify the details on accept quote conditions page
And I verify the details on accept quote file review page 
And I verify the details on accept quote confirm page 
And I verify the details on accept quote internal roles page 
And I verify the details on accept quote correspondence page 
Then I click on accept quote button to see order details on the dashboard
#SurveyorDocsonSiteInspection -Create Docs and Close Docs on SiteInspection
When I login to surveyor and close all documents if they open already
#And I select the one of the site from the list 
#And I upload docs on SiteInspection assessments
#And I select the one of the site from the list 
#Then I close docs on SiteInspection assessments 
#When I select the one of the site from the list 
#Then I should verify the closed SiteInspection documents 
#Then I should complete the surveyor assessments
##SurveyorDocsER -Create Docs and Close Docs on SRA
#When I select the one of the site from the list 
#And I upload docs on ER assessments
#And I select the one of the site from the list 
#Then I can not close docs on SRA assessments 
##SurveyorDocsER -Create Docs and Close Docs on RA
#When I select the one of the site from the list 
#Then I can not close docs on RA assessments 
##SurveyorDocsER -Create Docs and Close Docs on DR
#When I select the one of the site from the list 
#Then I can not close docs on DR assessments 
##SurveyorDocsER -Create Docs and Close Docs on ER
#When I select the one of the site from the list 
#Then I can not close docs on ER assessments 
##SurveyorDocsonSiteInspection  -Create Docs and Close Docs on Site Inspection
#When I select the one of the site from the list 
#And I upload docs on SiteInspection assessments
#When I select the one of the site from the list 
#Then I close docs on SiteInspection assessments 


Examples: 
| plotdata                                                        | 
|\\\tpgfile\NewCompany\IT\Department\QA Team\Current Projects\CRISP\CrispAutomation\CrispData\Assessment\NewHomes-SRA.xlsx| 