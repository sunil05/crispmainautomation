﻿Feature: VerifyActivities
Background: 
	Given I am on login page
	When I enter crisp username 
	And I enter crisp password
	And I click on login button
	Then I should see all items displayed on dashboard page

#Verify activities on lead feature
@SmokeTest
Scenario: Verify Activities on Lead feature
When I click on the plus button 
And I select lead option to create lead
And I provide pg details on the development information page
And I provide details on products details page
And I provide details on the roles page
And I provide details on other details page
When I save the lead details 
Then I should see PG Lead details on dashboard page
Then I should see lead the activity has been recorded
#Shortlisting the Lead
When I perform operation to shortlist the lead
Then I should see the activities that lead has been created and shortlisted 
#Allocating Email To The Lead
#When I perform operation email to the the lead
#Then I should see the activities that email has been allocated to the lead
#Convert Lead in to quote

#Verify activities on quote feature
@Smoketest
Scenario Outline: Verify Activities on Quote feature
When I click on plus button and select  quote option
And I read following '<plotdata>' details
And I provide details on key site details page for PG Brand
And I provide details on other site details page 
And I provide following '<plotdata>' data on plot schedule page
And I provide details on the product details page
And I provide details on additional questions page
And I select relavant PG roles on roles page
When I provide the details on declaration page
Then I click on complete button to see the application submitted details on the dashboard
Then I should see the quote submitted activity has been recorded
#Send PG Brand Quote 
When I click on the send quote button 
And I verify the details on send quote rating page
And I verify the details on send quote securities page
And I verify the details on send quote conditions page
And I verify the details on send quote special terms page
And I verify the details on send quote fees page
And I verify the details on send quote file review page
And I verify the details on send quote endorsements page
And I verify the details on send quote confirm details page
And I verify the details on send quote correspondence page
Then I click on send quote button to see quoted details on the dashboard
Then I should see the quote send activity has been recorded
#AcceptQuote
When I click on acceptquote button 
And I verify the details on accept quote products page
And I verify the details on accept quote conditions page
And I verify the details on accept quote file review page 
And I verify the details on accept quote confirm page 
And I verify the details on accept quote internal roles page 
And I verify the details on accept quote correspondence page 
Then I click on accept quote button to see order details on the dashboard
Then I should see the quote accepted activity has been recorded
#Sending Intial Notice
When I select files and documents page
And I upload sitelocation plan file type and surface storm and foul drainage layout  file type documents 

And I click on send intial notice button 
And I provide the details on initial notice qs page
And I verify the details on file review page 
And I verify the details on confirm page 
And I verify the details on intoclient page 
And I Verify the details on intola page
When I click on send initial notice button to complete sending intial notice
Then I should see the intial notice status has been sent
Then I should see the intial notice sent activity has been recorded
#Respond To Accept Intial Notice 
When I Select respond to initial notice button 
And I select the response type as accepted
And I verify the details on acceptance page 
And I verify the details on fliereview page 
And I verify details on confirm page
And I verify the initial notice acceptance details
When I click on send initial notice response -acceptance button 
Then I should view the resposne status on building control page.
Then I should see the response to the intial notice activity has been recorded



Examples: 
| plotdata                                                    | 
|\\\tpgfile\NewCompany\IT\Department\QA Team\Current Projects\CRISP\CrispAutomation\CrispData\IntialNoticeData\NewHomes.xlsx|

