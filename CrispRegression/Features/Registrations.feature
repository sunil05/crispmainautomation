﻿Feature: Registrations
Background: 
    Given I am on login page
	When I enter crisp username 
	And I enter crisp password
	And I click on login button
	Then I should see all items displayed on dashboard page

#PG Regestrations on Company Feature
@Smoketest
Scenario Outline: Verify Overview and RegDocs section on PGRegistrations Company 
Given I have created company name as <companyname>
When I select PG registration tab
When I verify the operations on overview section 
When I verify the operations on reg documents section

Examples: 
| companyname | 
| PGRegDocsCompany | 

@Smoketest
Scenario Outline: Verify Conditions section on PGRegistrations Company 
Given I have created company name as <companyname>
When I select PG registration tab
When I verfiy the operations on conditions section 

Examples: 
| companyname | 
| PGRegConditionsCompany | 

@Smoketest
Scenario Outline: Verify Terms section on PGRegistrations Company 
Given I have created company name as <companyname>
When I select PG registration tab
When I verify the operations on terms section 

Examples: 
| companyname | 
| PGRegTermsCompany | 

@Smoketest
Scenario Outline: Verify Roles section on PGRegistrations Company 
Given I have created company name as <companyname>
When I select PG registration tab
When I verify the operations on roles section

Examples: 
| companyname | 
| PGRegRolesCompany | 

@Smoketest
Scenario Outline: Verify Account section on PGRegistrations Company 
Given I have created company name as <companyname>
When I select PG registration tab
When I verify the operations on account section 

Examples: 
| companyname | 
| PGRegAccountCompany | 

@Smoketest
Scenario Outline: Verify Tasks section on PGRegistrations Company 
Given I have created company name as <companyname>
When I select PG registration tab
When I verify the operations on tasks section 

Examples: 
| companyname | 
| PGRegTaskCompany | 


@Smoketest
Scenario Outline: Verify correspondence section on PGRegistrations Company 
Given I have created company name as <companyname>
When I select PG registration tab
When I verify the operations on correspondence section


Examples: 
| companyname | 
| PGRegCorrespondenceCompany | 


@Smoketest
Scenario Outline: Verify filesanddocs section on PGRegistrations Company 
Given I have created company name as <companyname>
When I select PG registration tab
When I verify the operations on files&documents section 


Examples: 
| companyname | 
| PGRegFilesandDocsCompany | 

@Smoketest
Scenario Outline: Verify notes section on PGRegistrations Company 
Given I have created company name as <companyname>
When I select PG registration tab
When I verify the operations on notes section
Then I registaions details has been verified 

Examples: 
| companyname | 
| PGRegNotesCompany | 

#LABC Regestrations on Company Feature

@Smoketest
Scenario Outline: Verify Overview and RegDocs section on LABCRegistrations Company 
Given I have created company name as <companyname>
When I select LABC registration tab
When I verify the operations on overview section 
When I verify the operations on reg documents section 

Examples: 
| companyname | 
| LABCRegCompany | 


@Smoketest
Scenario Outline: Verify Conditions Section on LABCRegistrations Company 
Given I have created company name as <companyname>
When I select LABC registration tab
When I verfiy the operations on conditions section 

Examples: 
| companyname | 
| LABCRegCompany | 

@Smoketest
Scenario Outline: Verify Terms section on LABCRegistrations Company 
Given I have created company name as <companyname>
When I select LABC registration tab
When I verify the operations on terms section 

Examples: 
| companyname | 
| LABCRegCompany | 

@Smoketest
Scenario Outline: Verify Roles section on LABCRegistrations Company 
Given I have created company name as <companyname>
When I select LABC registration tab
When I verify the operations on roles section

Examples: 
| companyname | 
| LABCRegCompany | 

@Smoketest
Scenario Outline: Verify Account section on LABCRegistrations Company 
Given I have created company name as <companyname>
When I select LABC registration tab
When I verify the operations on account section 

Examples: 
| companyname | 
| LABCRegCompany | 

@Smoketest
Scenario Outline: Verify Tasks section on LABCRegistrations Company 
Given I have created company name as <companyname>
When I select LABC registration tab
When I verify the operations on tasks section 

Examples: 
| companyname | 
| LABCRegCompany | 

@Smoketest
Scenario Outline: Verify Correspondence section on LABCRegistrations Company 
Given I have created company name as <companyname>
When I select LABC registration tab
When I verify the operations on correspondence section

Examples: 
| companyname | 
| LABCRegCompany | 

@Smoketest
Scenario Outline: Verify Files And Documents section on LABCRegistrations Company 
Given I have created company name as <companyname>
When I select LABC registration tab
When I verify the operations on files&documents section 

Examples: 
| companyname | 
| LABCRegCompany | 

@Smoketest
Scenario Outline: Verify Notes section on LABCRegistrations Company 
Given I have created company name as <companyname>
When I select LABC registration tab
When I verify the operations on notes section
Then I registaions details has been verified 

Examples: 
| companyname | 
| LABCRegCompany | 


#PG Regestrations on Person Feature
@Smoketest
Scenario Outline: Verify Overview and RegDocs on PGRegistrations Person 
Given I have created a person 
When I select PG registration tab
When I verify the operations on overview section 
When I verify the operations on reg documents section 

Examples: 
| companyname  |
| PGRegDocsPerson |



@Smoketest
Scenario Outline: Verify Conditions section on PGRegistrations Person 
Given I have created a person 
When I select PG registration tab
When I verfiy the operations on conditions section 


Examples: 
| companyname | 
| PGConditionsPerson | 

@Smoketest
Scenario Outline:Verify Terms section on PGRegistrations Person 
Given I have created a person 
When I select PG registration tab
When I verify the operations on terms section 

Examples: 
| companyname | 
| PGTermsPerson | 


@Smoketest
Scenario Outline:Verify Roles section on PGRegistrations Person 
Given I have created a person 
When I select PG registration tab
When I verify the operations on roles section

Examples: 
| companyname | 
| PGRolesPerson | 

@Smoketest
Scenario Outline:Verify Account section on PGRegistrations Person 
Given I have created a person 
When I select PG registration tab
When I verify the operations on account section 

Examples: 
| companyname | 
| PGAccountPerson | 

@Smoketest
Scenario Outline:Verify Tasks section on PGRegistrations Person 
Given I have created a person 
When I select PG registration tab
When I verify the operations on tasks section 

Examples: 
| companyname | 
| PGTasksPerson | 

@Smoketest
Scenario Outline:Verify Correspondence section on PGRegistrations Person 
Given I have created a person 
When I select PG registration tab
When I verify the operations on correspondence section

Examples: 
| companyname | 
| PGCorrespondencePerson | 

@Smoketest
Scenario Outline: Verify Files And Docs section on PGRegistrations Person 
Given I have created a person 
When I select PG registration tab
When I verify the operations on files&documents section 

Examples: 
| companyname | 
| PGFilesTestPerson | 

@Smoketest
Scenario Outline: Verify Notes section on PGRegistrations Person 
Given I have created a person 
When I select PG registration tab
When I verify the operations on notes section
Then I registaions details has been verified 

Examples: 
| companyname | 
| PGNotestestPerson | 

#LABC Registrations on Person

@Smoketest
Scenario Outline: Verify LABCRegistrations on Person 
Given I have created a person 
When I select LABC registration tab
#When I verify the operations on overview section 
#When I verify the operations on reg documents section 
#When I verfiy the operations on conditions section 
#When I verify the operations on terms section 
#When I verify the operations on roles section
#When I verify the operations on account section 
#When I verify the operations on tasks section 
#When I verify the operations on correspondence section
#When I verify the operations on files&documents section 
When I verify the operations on notes section
Then I registaions details has been verified 

Examples: 
| companyname | 
| LABCRegCompany | 

