﻿Feature: ExtranetQuotes
Background:  
	Given I am on login page
	When I enter crisp username 
	And I enter crisp password
	And I click on login button
	Then I should see all items displayed on dashboard page	
 
Scenario Outline: Verify LABCSite Quote Records On Home Page
When I create LABC quotes on crisp using '<plotdata>'
Given I Login to LABC Extranet Site 
When I click on quote records on home page
And  I should navigate to quotes page 

Examples: 
| plotdata                                                                                                                                              |
|\\\tpgfile\NewCompany\IT\Department\QA Team\Current Projects\CRISP\CrispAutomation\CrispData\LABC-Products\LABC-LVS.xlsx |


Scenario Outline: Verify PGSite Quote Records On Home Page
When I create PG quotes on crisp using '<plotdata>'
Given I Login to PG Extranet Site 
When I click on quote records on home page 
And  I should navigate to quotes page 

Examples: 
| plotdata                                                                                                                                              |
|\\\tpgfile\NewCompany\IT\Department\QA Team\Current Projects\CRISP\CrispAutomation\CrispData\PG-Products\PG-LVS.xlsx |



Scenario Outline: Verify LABCSite Quote Records On Quotes Page
When I create LABC quotes on crisp using '<plotdata>'
Given I Login to LABC Extranet Site 
When I click on quote records on quotes page
And  I should navigate to quotes page 

Examples: 
| plotdata                                                                                                                                              |
|\\\tpgfile\NewCompany\IT\Department\QA Team\Current Projects\CRISP\CrispAutomation\CrispData\LABC-Products\LABC-LVS.xlsx |



Scenario Outline: Verify PGSite Quote Records On Quotes Page
When I create PG quotes on crisp using '<plotdata>'
Given I Login to PG Extranet Site 
When I click on quote records on quotes page
And  I should navigate to quotes page 

Examples: 
| plotdata                                                                                                                                              |
|\\\tpgfile\NewCompany\IT\Department\QA Team\Current Projects\CRISP\CrispAutomation\CrispData\PG-Products\PG-LVS.xlsx |


