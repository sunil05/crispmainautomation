﻿Feature: ExtranetIssueCOI
Background: 
	Given I am on login page
	When I enter crisp username 
	And I enter crisp password
	And I click on login button
	Then I should see all items displayed on dashboard page

@SmokeTest
Scenario Outline: Verify Request for IssueCOI on LABCSite HomePage
 When I create LABC orders on crisp using '<plotdata>'
 Given I Login to LABC Extranet Site 
 Then I should request coi from homepage
 When I verify coi task and issue the COI from Crisp 
 When I select extranet site
 Then I sould verify coi certifcate on extranet

 Examples: 
| plotdata                                                                                                                |
|\\\tpgfile\NewCompany\IT\Department\QA Team\Current Projects\CRISP\CrispAutomation\CrispData\LABC-Products\LABC-LVS.xlsx |

@SmokeTest
Scenario Outline: Verify Request for IssueCOI on LABCSite OrdersPage  
 When I create LABC orders on crisp using '<plotdata>'
 Given I Login to LABC Extranet Site 
 Then I should request coi from orderspage
 When I verify coi task and issue the COI from Crisp 
 When I select extranet site
 Then I sould verify coi certifcate on extranet


 Examples: 
| plotdata                                                                                                                |
|\\\tpgfile\NewCompany\IT\Department\QA Team\Current Projects\CRISP\CrispAutomation\CrispData\LABC-Products\LABC-LVS.xlsx  |

@SmokeTest
Scenario Outline: Verify Request for IssueCOI on LABCSite PlotData  
 When I create LABC orders on crisp using '<plotdata>'
 Given I Login to LABC Extranet Site 
 Then I should request coi from plotdata
 When I verify coi task and issue the COI from Crisp 
 When I select extranet site
 Then I sould verify coi certifcate on extranet

 Examples: 
| plotdata                                                                                                                |
|\\\tpgfile\NewCompany\IT\Department\QA Team\Current Projects\CRISP\CrispAutomation\CrispData\LABC-Products\LABC-LVS.xlsx  |

@SmokeTest
Scenario Outline: Verify Request for IssueCOI on PGSite HomePage
 When I create PG orders on crisp using '<plotdata>'
 Given  I Login to PG Extranet Site 
 Then I should request coi from homepage
 When I verify coi task and issue the COI from Crisp 
 When I select extranet site
 Then I sould verify coi certifcate on extranet

 Examples: 
| plotdata                                                                                                              |
|\\\tpgfile\NewCompany\IT\Department\QA Team\Current Projects\CRISP\CrispAutomation\CrispData\PG-Products\PG-LVS.xlsx |


@SmokeTest
Scenario Outline: Verify Request for IssueCOI on PGSite OrdersPage
When I create PG orders on crisp using '<plotdata>'
Given I Login to PG Extranet Site 
Then I should request coi from orderspage
When I verify coi task and issue the COI from Crisp 
When I select extranet site
Then I sould verify coi certifcate on extranet

 Examples: 
| plotdata                                                                                                              |
|\\\tpgfile\NewCompany\IT\Department\QA Team\Current Projects\CRISP\CrispAutomation\CrispData\PG-Products\PG-LVS.xlsx |

@SmokeTest
Scenario Outline: Verify Request for IssueCOI on PGSite PlotData
When I create PG orders on crisp using '<plotdata>'
Given I Login to PG Extranet Site 
Then I should request coi from plotdata
When I verify coi task and issue the COI from Crisp 
When I select extranet site
Then I sould verify coi certifcate on extranet


 Examples: 
| plotdata                                                                                                              |
|\\\tpgfile\NewCompany\IT\Department\QA Team\Current Projects\CRISP\CrispAutomation\CrispData\PG-Products\PG-LVS.xlsx|