﻿Feature: ExtranetHomePage
Background:  
	Given I am on login page
	When I enter crisp username 
	And I enter crisp password
	And I click on login button
	Then I should see all items displayed on dashboard page	


@mytag
Scenario Outline: Verify LABCQuotes From Crisp 
 When I create LABC quotes on crisp using '<plotdata>'
 Given I Login to LABC Extranet Site 
 Then I should verify the quotes on homepage

 Examples: 
| plotdata                                                                                                                                              |
|\\\tpgfile\NewCompany\IT\Department\QA Team\Current Projects\CRISP\CrispAutomation\CrispData\LABC-Products\LABC-LVS.xlsx |

@mytag
Scenario Outline: Verify LABC Orders From Crisp  
 When I create LABC orders on crisp using '<plotdata>'
 Given I Login to LABC Extranet Site 
 Then I should verify the orders on homepage

 Examples: 
| plotdata                                                                                                                                              |
|\\\tpgfile\NewCompany\IT\Department\QA Team\Current Projects\CRISP\CrispAutomation\CrispData\LABC-Products\LABC-LVS.xlsx |

@mytag
Scenario: Verify LABCSite Incomplete Applications On Homepage
Given I Login to LABC Extranet Site 
Then I should verify incomplete applications on homepage

@mytag
Scenario: Verify LABCSite Registration Status On Homepage
Given I Login to LABC Extranet Site 
When I click on registration status div on homepage
Then I should navigate to registration page

@mytag
Scenario: Verify LABCSite Manage Company Details On Homepage
Given I Login to LABC Extranet Site 
When I click on manage company details div on homepage
Then I should navigate to company page

@mytag
Scenario: Verify LABCSite Training Guide On Homepage
Given I Login to LABC Extranet Site 
When I click on extranet LABC training guide on homepage
Then I should see training guide is downloaded

@mytag
Scenario: Verify LABCSite Health and Safety Site On Homepage
Given I Login to LABC Extranet Site 
When I click on health and safety div on homepage
Then I should see health and safety site is opened

@mytag
Scenario Outline: Verify PG Quotes From Crisp 
 When I create PG quotes on crisp using '<plotdata>'
 Given I Login to PG Extranet Site 
 Then I should verify the quotes on homepage

 Examples: 
| plotdata                                                                                                                                              |
|\\\tpgfile\NewCompany\IT\Department\QA Team\Current Projects\CRISP\CrispAutomation\CrispData\PG-Products\PG-LVS.xlsx |


@mytag
Scenario Outline: Verify PG Orders From Crisp  
 When I create PG orders on crisp using '<plotdata>'
 Given I Login to PG Extranet Site 
 Then I should verify the orders on homepage

 Examples: 
| plotdata                                                                                                                                              |
|\\\tpgfile\NewCompany\IT\Department\QA Team\Current Projects\CRISP\CrispAutomation\CrispData\PG-Products\PG-LVS.xlsx |

@mytag
Scenario: Verify PGSite Incomplete Applications On Homepage
Given I Login to PG Extranet Site 
Then I should verify incomplete applications on homepage

@mytag
Scenario: Verify PGSite Registration Status On Homepage
Given I Login to PG Extranet Site 
When I click on registration status div on homepage
Then I should navigate to registration page

@mytag
Scenario: Verify PGSite Manage Company Details On Homepage
Given I Login to PG Extranet Site 
When I click on manage company details div on homepage
Then I should navigate to company page

@mytag
Scenario: Verify PGSite Training Guide On Homepage
Given I Login to PG Extranet Site 
When I click on PG extranet training guide on homepage
Then I should see training guide is downloaded

@mytag
Scenario: Verify PGSite Health and Safety Site On Homepage
Given I Login to PG Extranet Site 
When I click on health and safety div on homepage
Then I should see health and safety site is opened