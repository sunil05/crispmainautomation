﻿Feature: ExtranetLoginAndLogout
    In order to make sure users can login 
	As an extranet user
	I want to makesure login is working as expected

Background: 
	Given I am on login page
	When I enter crisp username 
	And I enter crisp password
	And I click on login button
	Then I should see all items displayed on dashboard page

Scenario: Verify Extranet LABC Login - Logout  
	Given I Login to LABC Extranet Site 
	When Verify all the links
	Then I Should Logout From Extranet Site 

Scenario: Verify Extranet PG Login - Logout    
    Given I Login to PG Extranet Site 
	When Verify all the links
	Then I Should Logout From Extranet Site 



