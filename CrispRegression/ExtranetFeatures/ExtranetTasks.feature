﻿Feature: ExtranetTasks
Background: 
	Given I am on login page
	When I enter crisp username 
	And I enter crisp password
	And I click on login button
	Then I should see all items displayed on dashboard page

@SmokeTest
Scenario: Verify Tasks On Crisp Related To Extranet LABC SIte
	When I create LABC orders on crisp using '<plotdata>'
    When I login to surveyor and close all documents if they open already
    And I select the one of the site from the list 
    And I upload docs on SRA assessments
	Given I Login to LABC Extranet Site 
	When I respond to the documents
	Then I should see the tasks on Crisp




@SmokeTest
Scenario: Verify Tasks On Crisp Related To Extranet PG SIte
	When I create PG orders on crisp using '<plotdata>'
    When I login to surveyor and close all documents if they open already
    And I select the one of the site from the list 
    And I upload docs on SRA assessments
	Given I Login to LABC Extranet Site 
	When I respond to the documents
	Then I should see the tasks on Crisp
    
