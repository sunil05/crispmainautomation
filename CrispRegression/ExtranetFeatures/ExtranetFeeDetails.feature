﻿Feature: ExtranetFeeDetails
Background:  
    Given I am on login page
	When I enter crisp username 
	And I enter crisp password
	And I click on login button
	Then I should see all items displayed on dashboard page


@SmokeTest
Scenario Outline: Verify Extranet Fee Details on LABCSite
 When I create LABC orders on crisp using '<plotdata>'
 When I store fee details from crisp
 Given I Login to LABC Extranet Site 
 Then I should verify fee details on extranet
 When I verify account and make the balance payments
 When I store fee details from crisp
 When I select extranet site
 Then I should verify fee details on extranet

 Examples: 
| plotdata                                                                                                                |
|\\\tpgfile\NewCompany\IT\Department\QA Team\Current Projects\CRISP\CrispAutomation\CrispData\LABC-Products\LABC-LVS.xlsx |


@SmokeTest
Scenario Outline: Verify Extranet Fee Details on PGSite
 When I create PG orders on crisp using '<plotdata>'
 When I store fee details from crisp
 Given I Login to PG Extranet Site 
 Then I should verify fee details on extranet
 When I verify account and make the balance payments
 When I store fee details from crisp
 When I select extranet site
 Then I should verify fee details on extranet

 Examples: 
| plotdata                                                                                                                |
|\\\tpgfile\NewCompany\IT\Department\QA Team\Current Projects\CRISP\CrispAutomation\CrispData\PG-Products\PG-LVS.xlsx |

