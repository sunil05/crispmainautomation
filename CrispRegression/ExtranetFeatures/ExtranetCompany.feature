﻿Feature: ExtranetCompanyFeature
Background: 
	Given I am on login page
	When I enter crisp username 
	And I enter crisp password
	And I click on login button
	Then I should see all items displayed on dashboard page

@smoke
Scenario: Verify LABCSite Manage Company Details On Homepage
Given I Login to LABC Extranet Site 
When I click on manage company details div on homepage
Then I should navigate to company page

@smoke
Scenario: Verify LABC Site Extranet Company Offices Section
Given I Login to LABC Extranet Site 
Then I should verify the office details on company page

 
@smoke
Scenario: Verify LABC Site Extranet Company Employees Section
Given I Login to LABC Extranet Site 
Then I should verify the employee details on company page


@smoke
Scenario: Add New Office From Lookup on LABC Site Company Section
Given I Login to LABC Extranet Site 
Then I should add new office from lookup address on company page

@smoke
Scenario: Add New Office Manually on LABC Site Company Section
Given I Login to LABC Extranet Site 
Then I should add new office manually on company page

@smoke
Scenario:Edit Office From Lookup on LABC Site Company Section
Given I Login to LABC Extranet Site 
Then I should add new office manually on company page
Then I should edit office from lookup address on company page

@smoke
Scenario:Edit Office Manually on LABC Site Company Section
Given I Login to LABC Extranet Site 
Then I should edit office manually on company page

@smoke
Scenario: Add New Employee on LABC Site Company Section
Given I Login to LABC Extranet Site 
Then I should add new employee on company page

@smoke
Scenario: Add Edit Employee on LABC Site Company Section
Given I Login to LABC Extranet Site 
Then I should add new employee on company page
Then I should edit employee on company page


#Company Details on PG 
@smoke
Scenario: Verify PGSite Manage Company Details On Homepage
Given I Login to PG Extranet Site 
When I click on manage company details div on homepage
Then I should navigate to company page

@smoke
Scenario: Verify PG Site Extranet Company Offices Section
Given I Login to PG Extranet Site 
Then I should verify the office details on company page

 
@smoke
Scenario: Verify PG Site Extranet Company Employees Section
Given I Login to PG Extranet Site 
Then I should verify the employee details on company page


@smoke
Scenario: Add New Office From Lookup on PG Site Company Section
Given I Login to PG Extranet Site 
Then I should add new office from lookup address on company page

@smoke
Scenario: Add New Office Manually on PG Site Company Section
Given I Login to PG Extranet Site 
Then I should add new office manually on company page

@smoke
Scenario:Edit Office From Lookup on PG Site Company Section
Given I Login to PG Extranet Site 
Then I should edit office from lookup address on company page

@smoke
Scenario:Edit Office Manually on PG Site Company Section
Given I Login to PG Extranet Site 
Then I should add new office manually on company page
Then I should edit office manually on company page

@smoke
Scenario: Add New Employee on PG Site Company Section
Given I Login to PG Extranet Site 
Then I should add new employee on company page

@smoke
Scenario: Add Edit Employee on PG Site Company Section
Given I Login to PG Extranet Site 
Then I should add new employee on company page
Then I should edit employee on company page
