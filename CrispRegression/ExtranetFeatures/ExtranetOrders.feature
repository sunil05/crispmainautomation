﻿Feature: ExtranetOrders
Background: 
	Given I am on login page
	When I enter crisp username 
	And I enter crisp password
	And I click on login button
	Then I should see all items displayed on dashboard page

@SmokeTest
Scenario Outline: Verify LABCSite Order Records On HomePage
When I create LABC orders on crisp using '<plotdata>'
Given I Login to LABC Extranet Site 
Then I should click and verfy orders from homepage

Examples: 
| plotdata                                                                                                                                              |
|\\\tpgfile\NewCompany\IT\Department\QA Team\Current Projects\CRISP\CrispAutomation\CrispData\LABC-Products\LABC-LVS.xlsx |


Scenario Outline: Verify LABCSite Order Count On HomePage
 When I create LABC orders on crisp using '<plotdata>'
Given I Login to LABC Extranet Site 
Then I should verfy orders count from homepage

Examples: 
| plotdata                                                                                                                                              |
|\\\tpgfile\NewCompany\IT\Department\QA Team\Current Projects\CRISP\CrispAutomation\CrispData\LABC-Products\LABC-LVS.xlsx |



Scenario Outline: Verify LABCSite Order Records On OrderPage
When I create LABC orders on crisp using '<plotdata>'
Given I Login to LABC Extranet Site 
Then I should click and verfy orders from orderpage
Examples: 
| plotdata                                                                                                                                              |
|\\\tpgfile\NewCompany\IT\Department\QA Team\Current Projects\CRISP\CrispAutomation\CrispData\LABC-Products\LABC-LVS.xlsx |


Scenario Outline: Verify LABCSite Select Orders Using Ref Numbers On HomePage
When I create LABC orders on crisp using '<plotdata>'
Given I Login to LABC Extranet Site 
Then I should click and verfy orders from homepage
Examples: 
| plotdata                                                                                                                                              |
|\\\tpgfile\NewCompany\IT\Department\QA Team\Current Projects\CRISP\CrispAutomation\CrispData\LABC-Products\LABC-LVS.xlsx |



Scenario Outline: Verify LABCSite Select Orders Using Ref Numbers On OrderPage
When I create LABC orders on crisp using '<plotdata>'
Given I Login to LABC Extranet Site 
Then I should click and verfy orders from orderpage
Examples: 
| plotdata                                                                                                                                              |
|\\\tpgfile\NewCompany\IT\Department\QA Team\Current Projects\CRISP\CrispAutomation\CrispData\LABC-Products\LABC-LVS.xlsx |

#PG Site

Scenario Outline: Verify PGSite Order Records On HomePage
When I create PG orders on crisp using '<plotdata>'
Given I Login to PG Extranet Site 
Then I should click and verfy orders from homepage

Examples: 
| plotdata                                                                                                             |
|\\\tpgfile\NewCompany\IT\Department\QA Team\Current Projects\CRISP\CrispAutomation\CrispData\PG-Products\PG-LVS.xlsx |



Scenario Outline: Verify PGSite Order Records On OrderPage
When I create PG orders on crisp using '<plotdata>'
Given I Login to PG Extranet Site 
Then I should click and verfy orders from orderpage

Examples: 
| plotdata                                                                                                                                              |
|\\\tpgfile\NewCompany\IT\Department\QA Team\Current Projects\CRISP\CrispAutomation\CrispData\PG-Products\PG-LVS.xlsx |



Scenario Outline: Verify PGSite Select Orders Using Ref Numbers On HomePage
When I create PG orders on crisp using '<plotdata>'
Given I Login to PG Extranet Site 
Then I should click and verfy orders from homepage

Examples: 
| plotdata |
|\\\tpgfile\NewCompany\IT\Department\QA Team\Current Projects\CRISP\CrispAutomation\CrispData\PG-Products\PG-LVS.xlsx|



Scenario Outline: Verify PGSite Select Orders Using Ref Numbers On OrderPage
When I create PG orders on crisp using '<plotdata>'
Given I Login to PG Extranet Site 
Then I should click and verfy orders from orderpage

Examples: 
| plotdata |
|\\\tpgfile\NewCompany\IT\Department\QA Team\Current Projects\CRISP\CrispAutomation\CrispData\PG-Products\PG-LVS.xlsx |

