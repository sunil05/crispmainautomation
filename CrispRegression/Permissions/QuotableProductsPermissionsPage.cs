﻿using OpenQA.Selenium;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.PageObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CrispAutomation.Pages
{
    public class QuotableProductsPermissionsPage : Support.Pages
    {
        public IWebDriver wdriver;
        public QuotableProductsPermissionsPage(ISearchContext driver)
        {
            PageFactory.InitElements(driver, this);
            wdriver = (RemoteWebDriver)driver;
        }
    }
}
