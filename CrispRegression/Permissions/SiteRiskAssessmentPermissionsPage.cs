﻿using OpenQA.Selenium;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.PageObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CrispAutomation.Permissions
{
    public class SiteRiskAssessmentPermissionsPage
    {
        public IWebDriver wdriver;
        public SiteRiskAssessmentPermissionsPage(ISearchContext driver)
        {
            PageFactory.InitElements(driver, this);
            wdriver = (RemoteWebDriver)driver;
        }
        public bool NoPermissions { get; set; }
        public bool CreateSiteRiskAssessmentPermission { get; set; }
        public bool AmendSiteRiskAssessmentPermission { get; set; }
        public bool ViewSiteRiskAssessmentPermission { get; set; }
    }

}
