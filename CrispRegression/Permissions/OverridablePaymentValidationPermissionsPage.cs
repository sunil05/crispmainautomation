﻿using OpenQA.Selenium;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.PageObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CrispAutomation.Permissions
{
    public class OverridablePaymentValidationPermissionsPage : Support.Pages
    {
        public IWebDriver wdriver;
        public OverridablePaymentValidationPermissionsPage(ISearchContext driver)
        {
            PageFactory.InitElements(driver, this);
            wdriver = (RemoteWebDriver) driver;
        }    
        public bool OverridePaymentValidationOnCOIermission { get; set; }
        public bool OverridePaymentValidationOnDevICPermission { get; set; }
        public bool OverridePaymentValidationOnFinalNoticePermission { get; set; }
        public bool OverridePaymentValidationOnIssueCoverNotePermission { get; set; }
        public bool OverridePaymentValidationOnIssuePlotICPermission { get; set; }      
    }
}
