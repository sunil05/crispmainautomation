﻿using OpenQA.Selenium;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.PageObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CrispAutomation.Permissions
{
    public class InspectionPermissionsPage : Support.Pages
    {
        public IWebDriver wdriver;
        public InspectionPermissionsPage(ISearchContext driver)
        {
            PageFactory.InitElements(driver, this);
            wdriver = (RemoteWebDriver)driver;
        }
        public bool NoPermissions { get; set; }
        public bool CreateInspectionPermission { get; set; }
        public bool AmendInspectionPermission { get; set; }
        public bool ViewInspectionPermission { get; set; }
    }
}
