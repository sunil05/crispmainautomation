﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.PageObjects;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Threading;

namespace CrispAutomation.Pages
{
    public class ContactsPermissionsPage : Support.Pages
    {
        public IWebDriver wdriver; 
      // bool Create { get; set; }
        public ContactsPermissionsPage(ISearchContext driver)
        {
            PageFactory.InitElements(driver, this);
            wdriver = (RemoteWebDriver)driver;
        }
        public bool NoPermissions { get; set; }
        public bool CreateContactPermission { get; set; }
        public bool AmendContactPermission { get; set; }
        public bool ViewContactPermission { get; set; }
        public bool ManageContactPermission { get; set; }
        public bool SetNonStandardPaymentTermsPermission { get; set; }
        public bool AmendCompanyPermission { get; set; }   
       

        public void ContactPermissions()
        {
            NoPermissions = MainAsyncRemoveAllPermissionsToUser().GetAwaiter().GetResult();
            CreateContactPermission = MainAsyncApplyPermissionsToCreateContact().GetAwaiter().GetResult();
            AmendContactPermission = MainAsyncApplyPermissionsToAmendContact().GetAwaiter().GetResult();
            ViewContactPermission = MainAsyncApplyPermissionsToSearchContact().GetAwaiter().GetResult();
            ManageContactPermission = MainAsyncVerifyPermissionsOnManageContact().GetAwaiter().GetResult();
            SetNonStandardPaymentTermsPermission = MainAsyncApplyNonStandardPaymentPermissions().GetAwaiter().GetResult();
            AmendCompanyPermission = MainAsyncApplyPermissionsToAmendContact().GetAwaiter().GetResult();
        }
        //Remove All permissions applied on user //Permissions Checking on  userid (Auto User)        
        public async Task<bool> MainAsyncRemoveAllPermissionsToUser()
        {
            var accountId = "ab004dd3-ff74-43c7-b39c-a94b00ad0c92";
            var envrironmentApiServer = ConfigurationManager.AppSettings["ApiServerUrl"];
            var client = HttpClientHelpers.CreateClient("scott.williams@mdinsurance.co.uk", "Mdis@1234;");
            string body = string.Format("{{\"id\":\"{0}\",\"permissions\":{{\"accounts\":{{\"importReceipts\":false,\"allocateMoney\":false,\"manageBordereau\":false}},\"internalUsers\":{{\"managePermissions\":false,\"create\":false,\"amend\":false,\"view\":false}},\"contacts\":{{\"setNonStandardPaymentTerms\":false,\"amendCompanyName\":false,\"manage\":false,\"create\":false,\"amend\":false,\"view\":false}},\"leads\":{{\"create\":false,\"amend\":false,\"view\":false}},\"opportunity\":{{\"create\":false,\"amend\":false,\"view\":false}},\"quote\":{{\"accept\":false,\"viewExternalIncomplete\":false,\"reconstructionCostLimit\":0,\"products\":[],\"setSpecialTerms\":false,\"create\":false,\"amend\":false,\"view\":false}},\"order\":{{\"approveReferral\":false,\"issueCoa\":false,\"issueCoa2\":false,\"issueCoi\":false,\"importReceipts\":false,\"allocateMoney\":false,\"ammendRoles\":false,\"issueDevelopmentIc\":false,\"issuePlotIc\":false,\"issueFinalNotice\":false,\"cancel\":false,\"changePaymentTerms\":false,\"reinstate\":false,\"create\":false,\"amend\":false,\"view\":false}},\"manageCompanyLists\":{{\"create\":false,\"amend\":false,\"view\":false}},\"manageCompanyGroups\":{{\"create\":false,\"amend\":false,\"view\":false}},\"refurbAssessment\":{{\"create\":false,\"amend\":false,\"view\":false}},\"siteRiskAssessment\":{{\"create\":false,\"amend\":false,\"view\":false}},\"inspection\":{{\"create\":false,\"amend\":false,\"view\":false}},\"designReview\":{{\"create\":false,\"amend\":false,\"view\":false}},\"engineerReview\":{{\"create\":true,\"amend\":true,\"view\":true}},\"structuralReview\":{{\"create\":false,\"amend\":false,\"view\":false}},\"registration\":{{\"manage\":false,\"create\":false,\"amend\":false,\"view\":false}},\"claim\":{{\"create\":false,\"amend\":false,\"view\":false}},\"recovery\":{{\"create\":false,\"amend\":false,\"view\":false}},\"salesAppointments\":{{\"create\":false,\"amend\":false,\"view\":false}},\"technicalHub\":{{\"createContent\":false,\"approveContent\":false,\"publishContent\":false}},\"companyBlocking\":{{\"create\":false,\"remove\":false}},\"managerOverrides\":{{\"processes\":[],\"paymentValidation\":[]}},\"localAuthorities\":{{\"buildingControlAdmin\":false}},\"notes\":{{\"create\":false,\"amend\":false,\"view\":false}},\"endorsements\":{{\"create\":false}},\"bonds\":{{\"manage\":false,\"bondValueLimit\":1000000000}},\"ratings\":{{\"confirm\":false}},\"extranet\":{{\"administrator\":false}},\"rescind\":{{\"rescindDevelopmentIc\":false,\"rescindPlotIc\":false,\"rescindCoverNote\":false,\"rescindCoa\":false,\"rescindCoi\":false,\"rescindInitialNotice\":false,\"rescindFinalNotice\":false,\"rescindBuildingControlTechnicalSignOff\":false,\"create\":false,\"amend\":false,\"view\":false}}}},\"salutation\":\"Mr\",\"firstname\":\"Auto\",\"surname\":\"User\",\"companyId\":\"60b050bb-73d2-4741-9c84-a7e3c26cd3d5\",\"managerId\":\"84add8b2-d0ae-44ff-af25-b26cbc9e72f7\",\"salesBuddyIds\":[],\"twitterAddress\":null,\"faceBookAddress\":null,\"linkedInAddress\":null,\"googlePlusAddress\":null,\"email\":\"auto.user@mdinsurance.co.uk\",\"phoneCountryCode\":214,\"phone\":\"01483793721\",\"mobileCountryCode\":0,\"mobile\":null,\"websites\":[],\"addresses\":[],\"primaryOffice\":\"c995818e-906a-4679-9415-cbc6cac4873a\",\"offices\":[\"c995818e-906a-4679-9415-cbc6cac4873a\"],\"isCompanyDirector\":false,\"isPurchasingDecisionMaker\":false,\"extension\":\"\",\"suffix\":null,\"isContractor\":false,\"fullTimeEquivalence\":1,\"department\":\"Information Technology\",\"team\":\"Test Team\",\"jobTitle\":\"IT User\",\"groups\":[1,2],\"remoteWorkerPrimaryOperatingLocation\":null}}", accountId.ToString());
            var response = await client.PostAsync($"{envrironmentApiServer}/api/InternalAccounts", new StringContent(body, Encoding.Default, "application/json"));
            return response.StatusCode == HttpStatusCode.OK;
        }
        //Creating Contact permissions applied on User //Permissions Checking on  userid (Auto User)
        public async Task<bool> MainAsyncApplyPermissionsToCreateContact()
        {
            var accountId = "ab004dd3-ff74-43c7-b39c-a94b00ad0c92";
            var envrironmentApiServer = ConfigurationManager.AppSettings["ApiServerUrl"];
            var client = HttpClientHelpers.CreateClient("scott.williams@mdinsurance.co.uk", "Mdis@1234;");
            string body = string.Format("{{\"id\":\"{0}\",\"permissions\":{{\"accounts\":{{\"importReceipts\":false,\"allocateMoney\":false,\"manageBordereau\":false}},\"internalUsers\":{{\"managePermissions\":false,\"create\":false,\"amend\":false,\"view\":false}},\"contacts\":{{\"setNonStandardPaymentTerms\":false,\"amendCompanyName\":false,\"manage\":true,\"create\":true,\"amend\":false,\"view\":true}},\"leads\":{{\"create\":false,\"amend\":false,\"view\":false}},\"opportunity\":{{\"create\":false,\"amend\":false,\"view\":false}},\"quote\":{{\"accept\":false,\"viewExternalIncomplete\":false,\"reconstructionCostLimit\":0,\"products\":[],\"setSpecialTerms\":false,\"create\":false,\"amend\":false,\"view\":false}},\"order\":{{\"approveReferral\":false,\"issueCoa\":false,\"issueCoa2\":false,\"issueCoi\":false,\"importReceipts\":false,\"allocateMoney\":false,\"ammendRoles\":false,\"issueDevelopmentIc\":false,\"issuePlotIc\":false,\"issueFinalNotice\":false,\"cancel\":false,\"changePaymentTerms\":false,\"reinstate\":false,\"create\":false,\"amend\":false,\"view\":false}},\"manageCompanyLists\":{{\"create\":false,\"amend\":false,\"view\":false}},\"manageCompanyGroups\":{{\"create\":false,\"amend\":false,\"view\":false}},\"refurbAssessment\":{{\"create\":false,\"amend\":false,\"view\":false}},\"siteRiskAssessment\":{{\"create\":false,\"amend\":false,\"view\":false}},\"inspection\":{{\"create\":false,\"amend\":false,\"view\":false}},\"designReview\":{{\"create\":false,\"amend\":false,\"view\":false}},\"engineerReview\":{{\"create\":true,\"amend\":true,\"view\":true}},\"structuralReview\":{{\"create\":false,\"amend\":false,\"view\":false}},\"registration\":{{\"manage\":false,\"create\":false,\"amend\":false,\"view\":false}},\"claim\":{{\"create\":false,\"amend\":false,\"view\":false}},\"recovery\":{{\"create\":false,\"amend\":false,\"view\":false}},\"salesAppointments\":{{\"create\":false,\"amend\":false,\"view\":false}},\"technicalHub\":{{\"createContent\":false,\"approveContent\":false,\"publishContent\":false}},\"companyBlocking\":{{\"create\":false,\"remove\":false}},\"managerOverrides\":{{\"processes\":[],\"paymentValidation\":[]}},\"localAuthorities\":{{\"buildingControlAdmin\":false}},\"notes\":{{\"create\":false,\"amend\":false,\"view\":false}},\"endorsements\":{{\"create\":false}},\"bonds\":{{\"manage\":false,\"bondValueLimit\":1000000000}},\"ratings\":{{\"confirm\":false}},\"extranet\":{{\"administrator\":false}},\"rescind\":{{\"rescindDevelopmentIc\":false,\"rescindPlotIc\":false,\"rescindCoverNote\":false,\"rescindCoa\":false,\"rescindCoi\":false,\"rescindInitialNotice\":false,\"rescindFinalNotice\":false,\"rescindBuildingControlTechnicalSignOff\":false,\"create\":false,\"amend\":false,\"view\":false}}}},\"salutation\":\"Mr\",\"firstname\":\"Auto\",\"surname\":\"User\",\"companyId\":\"60b050bb-73d2-4741-9c84-a7e3c26cd3d5\",\"managerId\":\"84add8b2-d0ae-44ff-af25-b26cbc9e72f7\",\"salesBuddyIds\":[],\"twitterAddress\":null,\"faceBookAddress\":null,\"linkedInAddress\":null,\"googlePlusAddress\":null,\"email\":\"auto.user@mdinsurance.co.uk\",\"phoneCountryCode\":214,\"phone\":\"01483793721\",\"mobileCountryCode\":0,\"mobile\":null,\"websites\":[],\"addresses\":[],\"primaryOffice\":\"c995818e-906a-4679-9415-cbc6cac4873a\",\"offices\":[\"c995818e-906a-4679-9415-cbc6cac4873a\"],\"isCompanyDirector\":false,\"isPurchasingDecisionMaker\":false,\"extension\":\"\",\"suffix\":null,\"isContractor\":false,\"fullTimeEquivalence\":1,\"department\":\"Information Technology\",\"team\":\"Test Team\",\"jobTitle\":\"IT User\",\"groups\":[1,2],\"remoteWorkerPrimaryOperatingLocation\":null}}", accountId.ToString());
            var response = await client.PostAsync($"{envrironmentApiServer}/api/InternalAccounts", new StringContent(body, Encoding.Default, "application/json"));
            return response.StatusCode == HttpStatusCode.OK;
        }
        //Manage Contact permissions applied on User //Permissions Checking on  userid (Auto User)
        public async Task<bool> MainAsyncVerifyPermissionsOnManageContact()
        {
            var accountId = "ab004dd3-ff74-43c7-b39c-a94b00ad0c92";
            var envrironmentApiServer = ConfigurationManager.AppSettings["ApiServerUrl"];
            var client = HttpClientHelpers.CreateClient("scott.williams@mdinsurance.co.uk", "Mdis@1234;");
            string body = string.Format("{{\"id\":\"{0}\",\"permissions\":{{\"accounts\":{{\"importReceipts\":false,\"allocateMoney\":false,\"manageBordereau\":false}},\"internalUsers\":{{\"managePermissions\":false,\"create\":false,\"amend\":false,\"view\":false}},\"contacts\":{{\"setNonStandardPaymentTerms\":false,\"amendCompanyName\":false,\"manage\":false,\"create\":true,\"amend\":true,\"view\":true}},\"leads\":{{\"create\":false,\"amend\":false,\"view\":false}},\"opportunity\":{{\"create\":false,\"amend\":false,\"view\":false}},\"quote\":{{\"accept\":false,\"viewExternalIncomplete\":false,\"reconstructionCostLimit\":0,\"products\":[],\"setSpecialTerms\":false,\"create\":false,\"amend\":false,\"view\":false}},\"order\":{{\"approveReferral\":false,\"issueCoa\":false,\"issueCoa2\":false,\"issueCoi\":false,\"importReceipts\":false,\"allocateMoney\":false,\"ammendRoles\":false,\"issueDevelopmentIc\":false,\"issuePlotIc\":false,\"issueFinalNotice\":false,\"cancel\":false,\"changePaymentTerms\":false,\"reinstate\":false,\"create\":false,\"amend\":false,\"view\":false}},\"manageCompanyLists\":{{\"create\":false,\"amend\":false,\"view\":false}},\"manageCompanyGroups\":{{\"create\":false,\"amend\":false,\"view\":false}},\"refurbAssessment\":{{\"create\":false,\"amend\":false,\"view\":false}},\"siteRiskAssessment\":{{\"create\":false,\"amend\":false,\"view\":false}},\"inspection\":{{\"create\":false,\"amend\":false,\"view\":false}},\"designReview\":{{\"create\":false,\"amend\":false,\"view\":false}},\"engineerReview\":{{\"create\":true,\"amend\":true,\"view\":true}},\"structuralReview\":{{\"create\":false,\"amend\":false,\"view\":false}},\"registration\":{{\"manage\":false,\"create\":false,\"amend\":false,\"view\":false}},\"claim\":{{\"create\":false,\"amend\":false,\"view\":false}},\"recovery\":{{\"create\":false,\"amend\":false,\"view\":false}},\"salesAppointments\":{{\"create\":false,\"amend\":false,\"view\":false}},\"technicalHub\":{{\"createContent\":false,\"approveContent\":false,\"publishContent\":false}},\"companyBlocking\":{{\"create\":false,\"remove\":false}},\"managerOverrides\":{{\"processes\":[],\"paymentValidation\":[]}},\"localAuthorities\":{{\"buildingControlAdmin\":false}},\"notes\":{{\"create\":false,\"amend\":false,\"view\":false}},\"endorsements\":{{\"create\":false}},\"bonds\":{{\"manage\":false,\"bondValueLimit\":1000000000}},\"ratings\":{{\"confirm\":false}},\"extranet\":{{\"administrator\":false}},\"rescind\":{{\"rescindDevelopmentIc\":false,\"rescindPlotIc\":false,\"rescindCoverNote\":false,\"rescindCoa\":false,\"rescindCoi\":false,\"rescindInitialNotice\":false,\"rescindFinalNotice\":false,\"rescindBuildingControlTechnicalSignOff\":false,\"create\":false,\"amend\":false,\"view\":false}}}},\"salutation\":\"Mr\",\"firstname\":\"Auto\",\"surname\":\"User\",\"companyId\":\"60b050bb-73d2-4741-9c84-a7e3c26cd3d5\",\"managerId\":\"84add8b2-d0ae-44ff-af25-b26cbc9e72f7\",\"salesBuddyIds\":[],\"twitterAddress\":null,\"faceBookAddress\":null,\"linkedInAddress\":null,\"googlePlusAddress\":null,\"email\":\"auto.user@mdinsurance.co.uk\",\"phoneCountryCode\":214,\"phone\":\"01483793721\",\"mobileCountryCode\":0,\"mobile\":null,\"websites\":[],\"addresses\":[],\"primaryOffice\":\"c995818e-906a-4679-9415-cbc6cac4873a\",\"offices\":[\"c995818e-906a-4679-9415-cbc6cac4873a\"],\"isCompanyDirector\":false,\"isPurchasingDecisionMaker\":false,\"extension\":\"\",\"suffix\":null,\"isContractor\":false,\"fullTimeEquivalence\":1,\"department\":\"Information Technology\",\"team\":\"Test Team\",\"jobTitle\":\"IT User\",\"groups\":[1,2],\"remoteWorkerPrimaryOperatingLocation\":null}}", accountId.ToString());
            var response = await client.PostAsync($"{envrironmentApiServer}/api/InternalAccounts", new StringContent(body, Encoding.Default, "application/json"));
            return response.StatusCode == HttpStatusCode.OK;
        }
        //Amending Contact permissions applied on User //Permissions Checking on  userid (Auto User)
        public async Task<bool> MainAsyncApplyPermissionsToAmendContact()
        {
            var accountId = "ab004dd3-ff74-43c7-b39c-a94b00ad0c92";
            var envrironmentApiServer = ConfigurationManager.AppSettings["ApiServerUrl"];
            var client = HttpClientHelpers.CreateClient("scott.williams@mdinsurance.co.uk", "Mdis@1234;");
            string body = string.Format("{{\"id\":\"{0}\",\"permissions\":{{\"accounts\":{{\"importReceipts\":false,\"allocateMoney\":false,\"manageBordereau\":false}},\"internalUsers\":{{\"managePermissions\":false,\"create\":false,\"amend\":false,\"view\":false}},\"contacts\":{{\"setNonStandardPaymentTerms\":false,\"amendCompanyName\":false,\"manage\":true,\"create\":true,\"amend\":true,\"view\":true}},\"leads\":{{\"create\":false,\"amend\":false,\"view\":false}},\"opportunity\":{{\"create\":false,\"amend\":false,\"view\":false}},\"quote\":{{\"accept\":false,\"viewExternalIncomplete\":false,\"reconstructionCostLimit\":0,\"products\":[],\"setSpecialTerms\":false,\"create\":false,\"amend\":false,\"view\":false}},\"order\":{{\"approveReferral\":false,\"issueCoa\":false,\"issueCoa2\":false,\"issueCoi\":false,\"importReceipts\":false,\"allocateMoney\":false,\"ammendRoles\":false,\"issueDevelopmentIc\":false,\"issuePlotIc\":false,\"issueFinalNotice\":false,\"cancel\":false,\"changePaymentTerms\":false,\"reinstate\":false,\"create\":false,\"amend\":false,\"view\":false}},\"manageCompanyLists\":{{\"create\":false,\"amend\":false,\"view\":false}},\"manageCompanyGroups\":{{\"create\":false,\"amend\":false,\"view\":false}},\"refurbAssessment\":{{\"create\":false,\"amend\":false,\"view\":false}},\"siteRiskAssessment\":{{\"create\":false,\"amend\":false,\"view\":false}},\"inspection\":{{\"create\":false,\"amend\":false,\"view\":false}},\"designReview\":{{\"create\":false,\"amend\":false,\"view\":false}},\"engineerReview\":{{\"create\":true,\"amend\":true,\"view\":true}},\"structuralReview\":{{\"create\":false,\"amend\":false,\"view\":false}},\"registration\":{{\"manage\":false,\"create\":false,\"amend\":false,\"view\":false}},\"claim\":{{\"create\":false,\"amend\":false,\"view\":false}},\"recovery\":{{\"create\":false,\"amend\":false,\"view\":false}},\"salesAppointments\":{{\"create\":false,\"amend\":false,\"view\":false}},\"technicalHub\":{{\"createContent\":false,\"approveContent\":false,\"publishContent\":false}},\"companyBlocking\":{{\"create\":false,\"remove\":false}},\"managerOverrides\":{{\"processes\":[],\"paymentValidation\":[]}},\"localAuthorities\":{{\"buildingControlAdmin\":false}},\"notes\":{{\"create\":false,\"amend\":false,\"view\":false}},\"endorsements\":{{\"create\":false}},\"bonds\":{{\"manage\":false,\"bondValueLimit\":1000000000}},\"ratings\":{{\"confirm\":false}},\"extranet\":{{\"administrator\":false}},\"rescind\":{{\"rescindDevelopmentIc\":false,\"rescindPlotIc\":false,\"rescindCoverNote\":false,\"rescindCoa\":false,\"rescindCoi\":false,\"rescindInitialNotice\":false,\"rescindFinalNotice\":false,\"rescindBuildingControlTechnicalSignOff\":false,\"create\":false,\"amend\":false,\"view\":false}}}},\"salutation\":\"Mr\",\"firstname\":\"Auto\",\"surname\":\"User\",\"companyId\":\"60b050bb-73d2-4741-9c84-a7e3c26cd3d5\",\"managerId\":\"84add8b2-d0ae-44ff-af25-b26cbc9e72f7\",\"salesBuddyIds\":[],\"twitterAddress\":null,\"faceBookAddress\":null,\"linkedInAddress\":null,\"googlePlusAddress\":null,\"email\":\"auto.user@mdinsurance.co.uk\",\"phoneCountryCode\":214,\"phone\":\"01483793721\",\"mobileCountryCode\":0,\"mobile\":null,\"websites\":[],\"addresses\":[],\"primaryOffice\":\"c995818e-906a-4679-9415-cbc6cac4873a\",\"offices\":[\"c995818e-906a-4679-9415-cbc6cac4873a\"],\"isCompanyDirector\":false,\"isPurchasingDecisionMaker\":false,\"extension\":\"\",\"suffix\":null,\"isContractor\":false,\"fullTimeEquivalence\":1,\"department\":\"Information Technology\",\"team\":\"Test Team\",\"jobTitle\":\"IT User\",\"groups\":[1,2],\"remoteWorkerPrimaryOperatingLocation\":null}}", accountId.ToString());
            var response = await client.PostAsync($"{envrironmentApiServer}/api/InternalAccounts", new StringContent(body, Encoding.Default, "application/json"));
            return response.StatusCode == HttpStatusCode.OK;
        }        
        //Viewing Contact permissions applied on User //Permissions Checking on  userid (Auto User)
        public async Task<bool> MainAsyncApplyPermissionsToSearchContact()
        {
            var accountId = "ab004dd3-ff74-43c7-b39c-a94b00ad0c92";
            var envrironmentApiServer = ConfigurationManager.AppSettings["ApiServerUrl"];
            var client = HttpClientHelpers.CreateClient("scott.williams@mdinsurance.co.uk", "Mdis@1234;");
            string body = string.Format("{{\"id\":\"{0}\",\"permissions\":{{\"accounts\":{{\"importReceipts\":false,\"allocateMoney\":false,\"manageBordereau\":false}},\"internalUsers\":{{\"managePermissions\":false,\"create\":false,\"amend\":false,\"view\":false}},\"contacts\":{{\"setNonStandardPaymentTerms\":false,\"amendCompanyName\":false,\"manage\":false,\"create\":false,\"amend\":false,\"view\":true}},\"leads\":{{\"create\":false,\"amend\":false,\"view\":false}},\"opportunity\":{{\"create\":false,\"amend\":false,\"view\":false}},\"quote\":{{\"accept\":false,\"viewExternalIncomplete\":false,\"reconstructionCostLimit\":0,\"products\":[],\"setSpecialTerms\":false,\"create\":false,\"amend\":false,\"view\":false}},\"order\":{{\"approveReferral\":false,\"issueCoa\":false,\"issueCoa2\":false,\"issueCoi\":false,\"importReceipts\":false,\"allocateMoney\":false,\"ammendRoles\":false,\"issueDevelopmentIc\":false,\"issuePlotIc\":false,\"issueFinalNotice\":false,\"cancel\":false,\"changePaymentTerms\":false,\"reinstate\":false,\"create\":false,\"amend\":false,\"view\":false}},\"manageCompanyLists\":{{\"create\":false,\"amend\":false,\"view\":false}},\"manageCompanyGroups\":{{\"create\":false,\"amend\":false,\"view\":false}},\"refurbAssessment\":{{\"create\":false,\"amend\":false,\"view\":false}},\"siteRiskAssessment\":{{\"create\":false,\"amend\":false,\"view\":false}},\"inspection\":{{\"create\":false,\"amend\":false,\"view\":false}},\"designReview\":{{\"create\":false,\"amend\":false,\"view\":false}},\"engineerReview\":{{\"create\":true,\"amend\":true,\"view\":true}},\"structuralReview\":{{\"create\":false,\"amend\":false,\"view\":false}},\"registration\":{{\"manage\":false,\"create\":false,\"amend\":false,\"view\":false}},\"claim\":{{\"create\":false,\"amend\":false,\"view\":false}},\"recovery\":{{\"create\":false,\"amend\":false,\"view\":false}},\"salesAppointments\":{{\"create\":false,\"amend\":false,\"view\":false}},\"technicalHub\":{{\"createContent\":false,\"approveContent\":false,\"publishContent\":false}},\"companyBlocking\":{{\"create\":false,\"remove\":false}},\"managerOverrides\":{{\"processes\":[],\"paymentValidation\":[]}},\"localAuthorities\":{{\"buildingControlAdmin\":false}},\"notes\":{{\"create\":false,\"amend\":false,\"view\":false}},\"endorsements\":{{\"create\":false}},\"bonds\":{{\"manage\":false,\"bondValueLimit\":1000000000}},\"ratings\":{{\"confirm\":false}},\"extranet\":{{\"administrator\":false}},\"rescind\":{{\"rescindDevelopmentIc\":false,\"rescindPlotIc\":false,\"rescindCoverNote\":false,\"rescindCoa\":false,\"rescindCoi\":false,\"rescindInitialNotice\":false,\"rescindFinalNotice\":false,\"rescindBuildingControlTechnicalSignOff\":false,\"create\":false,\"amend\":false,\"view\":false}}}},\"salutation\":\"Mr\",\"firstname\":\"Auto\",\"surname\":\"User\",\"companyId\":\"60b050bb-73d2-4741-9c84-a7e3c26cd3d5\",\"managerId\":\"84add8b2-d0ae-44ff-af25-b26cbc9e72f7\",\"salesBuddyIds\":[],\"twitterAddress\":null,\"faceBookAddress\":null,\"linkedInAddress\":null,\"googlePlusAddress\":null,\"email\":\"auto.user@mdinsurance.co.uk\",\"phoneCountryCode\":214,\"phone\":\"01483793721\",\"mobileCountryCode\":0,\"mobile\":null,\"websites\":[],\"addresses\":[],\"primaryOffice\":\"c995818e-906a-4679-9415-cbc6cac4873a\",\"offices\":[\"c995818e-906a-4679-9415-cbc6cac4873a\"],\"isCompanyDirector\":false,\"isPurchasingDecisionMaker\":false,\"extension\":\"\",\"suffix\":null,\"isContractor\":false,\"fullTimeEquivalence\":1,\"department\":\"Information Technology\",\"team\":\"Test Team\",\"jobTitle\":\"IT User\",\"groups\":[1,2],\"remoteWorkerPrimaryOperatingLocation\":null}}", accountId.ToString());                                    
            var response = await client.PostAsync($"{envrironmentApiServer}/api/InternalAccounts", new StringContent(body, Encoding.Default, "application/json"));
            return response.StatusCode == HttpStatusCode.OK;
        }
        //Amend CompanyName Permissions applied on User //Permissions Checking on  userid (Auto User)
        public async Task<bool> MainAsyncApplyPermissionsToAmendCompanyName()
        {
            var accountId = "ab004dd3-ff74-43c7-b39c-a94b00ad0c92";
            var envrironmentApiServer = ConfigurationManager.AppSettings["ApiServerUrl"];
            var client = HttpClientHelpers.CreateClient("scott.williams@mdinsurance.co.uk", "Mdis@1234;");
            string body = string.Format("{{\"id\":\"{0}\",\"permissions\":{{\"accounts\":{{\"importReceipts\":false,\"allocateMoney\":false,\"manageBordereau\":false}},\"internalUsers\":{{\"managePermissions\":false,\"create\":false,\"amend\":false,\"view\":false}},\"contacts\":{{\"setNonStandardPaymentTerms\":false,\"amendCompanyName\":true,\"manage\":true,\"create\":true,\"amend\":true,\"view\":true}},\"leads\":{{\"create\":false,\"amend\":false,\"view\":false}},\"opportunity\":{{\"create\":false,\"amend\":false,\"view\":false}},\"quote\":{{\"accept\":false,\"viewExternalIncomplete\":false,\"reconstructionCostLimit\":0,\"products\":[],\"setSpecialTerms\":false,\"create\":false,\"amend\":false,\"view\":false}},\"order\":{{\"approveReferral\":false,\"issueCoa\":false,\"issueCoa2\":false,\"issueCoi\":false,\"importReceipts\":false,\"allocateMoney\":false,\"ammendRoles\":false,\"issueDevelopmentIc\":false,\"issuePlotIc\":false,\"issueFinalNotice\":false,\"cancel\":false,\"changePaymentTerms\":false,\"reinstate\":false,\"create\":false,\"amend\":false,\"view\":false}},\"manageCompanyLists\":{{\"create\":false,\"amend\":false,\"view\":false}},\"manageCompanyGroups\":{{\"create\":false,\"amend\":false,\"view\":false}},\"refurbAssessment\":{{\"create\":false,\"amend\":false,\"view\":false}},\"siteRiskAssessment\":{{\"create\":false,\"amend\":false,\"view\":false}},\"inspection\":{{\"create\":false,\"amend\":false,\"view\":false}},\"designReview\":{{\"create\":false,\"amend\":false,\"view\":false}},\"engineerReview\":{{\"create\":true,\"amend\":true,\"view\":true}},\"structuralReview\":{{\"create\":false,\"amend\":false,\"view\":false}},\"registration\":{{\"manage\":false,\"create\":false,\"amend\":false,\"view\":false}},\"claim\":{{\"create\":false,\"amend\":false,\"view\":false}},\"recovery\":{{\"create\":false,\"amend\":false,\"view\":false}},\"salesAppointments\":{{\"create\":false,\"amend\":false,\"view\":false}},\"technicalHub\":{{\"createContent\":false,\"approveContent\":false,\"publishContent\":false}},\"companyBlocking\":{{\"create\":false,\"remove\":false}},\"managerOverrides\":{{\"processes\":[],\"paymentValidation\":[]}},\"localAuthorities\":{{\"buildingControlAdmin\":false}},\"notes\":{{\"create\":false,\"amend\":false,\"view\":false}},\"endorsements\":{{\"create\":false}},\"bonds\":{{\"manage\":false,\"bondValueLimit\":1000000000}},\"ratings\":{{\"confirm\":false}},\"extranet\":{{\"administrator\":false}},\"rescind\":{{\"rescindDevelopmentIc\":false,\"rescindPlotIc\":false,\"rescindCoverNote\":false,\"rescindCoa\":false,\"rescindCoi\":false,\"rescindInitialNotice\":false,\"rescindFinalNotice\":false,\"rescindBuildingControlTechnicalSignOff\":false,\"create\":false,\"amend\":false,\"view\":false}}}},\"salutation\":\"Mr\",\"firstname\":\"Auto\",\"surname\":\"User\",\"companyId\":\"60b050bb-73d2-4741-9c84-a7e3c26cd3d5\",\"managerId\":\"84add8b2-d0ae-44ff-af25-b26cbc9e72f7\",\"salesBuddyIds\":[],\"twitterAddress\":null,\"faceBookAddress\":null,\"linkedInAddress\":null,\"googlePlusAddress\":null,\"email\":\"auto.user@mdinsurance.co.uk\",\"phoneCountryCode\":214,\"phone\":\"01483793721\",\"mobileCountryCode\":0,\"mobile\":null,\"websites\":[],\"addresses\":[],\"primaryOffice\":\"c995818e-906a-4679-9415-cbc6cac4873a\",\"offices\":[\"c995818e-906a-4679-9415-cbc6cac4873a\"],\"isCompanyDirector\":false,\"isPurchasingDecisionMaker\":false,\"extension\":\"\",\"suffix\":null,\"isContractor\":false,\"fullTimeEquivalence\":1,\"department\":\"Information Technology\",\"team\":\"Test Team\",\"jobTitle\":\"IT User\",\"groups\":[1,2],\"remoteWorkerPrimaryOperatingLocation\":null}}", accountId.ToString());
            var response = await client.PostAsync($"{envrironmentApiServer}/api/InternalAccounts", new StringContent(body, Encoding.Default, "application/json"));
            return response.StatusCode == HttpStatusCode.OK;
        }
        //Set Non-Standard Payment Terms Permissions applied on User //Permissions Checking on  userid (Auto User)
        public async Task<bool> MainAsyncApplyNonStandardPaymentPermissions()
        {
            var accountId = "ab004dd3-ff74-43c7-b39c-a94b00ad0c92";
            var envrironmentApiServer = ConfigurationManager.AppSettings["ApiServerUrl"];
            var client = HttpClientHelpers.CreateClient("scott.williams@mdinsurance.co.uk", "Mdis@1234;");
            string body = string.Format("{{\"id\":\"{0}\",\"permissions\":{{\"accounts\":{{\"importReceipts\":false,\"allocateMoney\":false,\"manageBordereau\":false}},\"internalUsers\":{{\"managePermissions\":false,\"create\":false,\"amend\":false,\"view\":false}},\"contacts\":{{\"setNonStandardPaymentTerms\":true,\"amendCompanyName\":true,\"manage\":true,\"create\":true,\"amend\":true,\"view\":true}},\"leads\":{{\"create\":false,\"amend\":false,\"view\":false}},\"opportunity\":{{\"create\":false,\"amend\":false,\"view\":false}},\"quote\":{{\"accept\":false,\"viewExternalIncomplete\":false,\"reconstructionCostLimit\":0,\"products\":[],\"setSpecialTerms\":false,\"create\":false,\"amend\":false,\"view\":false}},\"order\":{{\"approveReferral\":false,\"issueCoa\":false,\"issueCoa2\":false,\"issueCoi\":false,\"importReceipts\":false,\"allocateMoney\":false,\"ammendRoles\":false,\"issueDevelopmentIc\":false,\"issuePlotIc\":false,\"issueFinalNotice\":false,\"cancel\":false,\"changePaymentTerms\":false,\"reinstate\":false,\"create\":false,\"amend\":false,\"view\":false}},\"manageCompanyLists\":{{\"create\":false,\"amend\":false,\"view\":false}},\"manageCompanyGroups\":{{\"create\":false,\"amend\":false,\"view\":false}},\"refurbAssessment\":{{\"create\":false,\"amend\":false,\"view\":false}},\"siteRiskAssessment\":{{\"create\":false,\"amend\":false,\"view\":false}},\"inspection\":{{\"create\":false,\"amend\":false,\"view\":false}},\"designReview\":{{\"create\":false,\"amend\":false,\"view\":false}},\"engineerReview\":{{\"create\":true,\"amend\":true,\"view\":true}},\"structuralReview\":{{\"create\":false,\"amend\":false,\"view\":false}},\"registration\":{{\"manage\":false,\"create\":false,\"amend\":false,\"view\":false}},\"claim\":{{\"create\":false,\"amend\":false,\"view\":false}},\"recovery\":{{\"create\":false,\"amend\":false,\"view\":false}},\"salesAppointments\":{{\"create\":false,\"amend\":false,\"view\":false}},\"technicalHub\":{{\"createContent\":false,\"approveContent\":false,\"publishContent\":false}},\"companyBlocking\":{{\"create\":false,\"remove\":false}},\"managerOverrides\":{{\"processes\":[],\"paymentValidation\":[]}},\"localAuthorities\":{{\"buildingControlAdmin\":false}},\"notes\":{{\"create\":false,\"amend\":false,\"view\":false}},\"endorsements\":{{\"create\":false}},\"bonds\":{{\"manage\":false,\"bondValueLimit\":1000000000}},\"ratings\":{{\"confirm\":false}},\"extranet\":{{\"administrator\":false}},\"rescind\":{{\"rescindDevelopmentIc\":false,\"rescindPlotIc\":false,\"rescindCoverNote\":false,\"rescindCoa\":false,\"rescindCoi\":false,\"rescindInitialNotice\":false,\"rescindFinalNotice\":false,\"rescindBuildingControlTechnicalSignOff\":false,\"create\":false,\"amend\":false,\"view\":false}}}},\"salutation\":\"Mr\",\"firstname\":\"Auto\",\"surname\":\"User\",\"companyId\":\"60b050bb-73d2-4741-9c84-a7e3c26cd3d5\",\"managerId\":\"84add8b2-d0ae-44ff-af25-b26cbc9e72f7\",\"salesBuddyIds\":[],\"twitterAddress\":null,\"faceBookAddress\":null,\"linkedInAddress\":null,\"googlePlusAddress\":null,\"email\":\"auto.user@mdinsurance.co.uk\",\"phoneCountryCode\":214,\"phone\":\"01483793721\",\"mobileCountryCode\":0,\"mobile\":null,\"websites\":[],\"addresses\":[],\"primaryOffice\":\"c995818e-906a-4679-9415-cbc6cac4873a\",\"offices\":[\"c995818e-906a-4679-9415-cbc6cac4873a\"],\"isCompanyDirector\":false,\"isPurchasingDecisionMaker\":false,\"extension\":\"\",\"suffix\":null,\"isContractor\":false,\"fullTimeEquivalence\":1,\"department\":\"Information Technology\",\"team\":\"Test Team\",\"jobTitle\":\"IT User\",\"groups\":[1,2],\"remoteWorkerPrimaryOperatingLocation\":null}}", accountId.ToString());
            var response = await client.PostAsync($"{envrironmentApiServer}/api/InternalAccounts", new StringContent(body, Encoding.Default, "application/json"));
            return response.StatusCode == HttpStatusCode.OK;
        }
        public void VerifyContactOperations()
        {
            NoPermissions = MainAsyncRemoveAllPermissionsToUser().GetAwaiter().GetResult();
            if(NoPermissions == true)
            {
                Dashboardpage.PermissionsLoginMethod();
                Assert.IsTrue(!Dashboardpage.PlusButton.Displayed);
                Assert.IsTrue(!Dashboardpage.SearchButton.Displayed);
            }
            ManageContactPermission = MainAsyncVerifyPermissionsOnManageContact().GetAwaiter().GetResult();
            if (ManageContactPermission == true)
            {
                Dashboardpage.PermissionsLoginMethod();
                Assert.IsTrue(Dashboardpage.PlusButton.Displayed);
                Dashboardpage.PlusButton.Click();
                Assert.IsTrue(Dashboardpage.PersonButton.Displayed);
                Dashboardpage.PersonButton.Click();
                Assert.IsTrue(!AddPersonpage.Salutation.Displayed);
            }
            CreateContactPermission = MainAsyncApplyPermissionsToCreateContact().GetAwaiter().GetResult();
            if (CreateContactPermission == true)
            {
                Dashboardpage.PermissionsLoginMethod();
                //Thread.Sleep(500);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                //Thread.Sleep(500);
                Assert.IsTrue(Dashboardpage.PlusButton.Displayed);
                AddPersonpage.AddPersonMainMethod();
                //Thread.Sleep(500);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                //Thread.Sleep(500);
            }
            ViewContactPermission = MainAsyncApplyPermissionsToSearchContact().GetAwaiter().GetResult();
            if (ViewContactPermission == true)
            {
                Dashboardpage.PermissionsLoginMethod();
                Assert.IsTrue(Dashboardpage.SearchButton.Displayed);
                Dashboardpage.SearchContact();
            }
            AmendContactPermission = MainAsyncApplyPermissionsToAmendContact().GetAwaiter().GetResult();
            if (AmendContactPermission == true)
            {
                Dashboardpage.PermissionsLoginMethod();
                Assert.IsTrue(Dashboardpage.PlusButton.Displayed);
                AddPersonpage.AddPersonMainMethod();
                Assert.IsTrue(EditPersonDetailsPage.EditPersonButton.Displayed);
                EditPersonDetailsPage.EditPersonButton.Click();
                EditPersonDetailsPage.EditPersonDetailsMain();
            }
            AmendCompanyPermission = MainAsyncApplyPermissionsToAmendContact().GetAwaiter().GetResult();
            if (AmendCompanyPermission == true)
            {
                Dashboardpage.PermissionsLoginMethod();
                Assert.IsTrue(Dashboardpage.PlusButton.Displayed);

                EditCompanyDetailsPage.EditCompanyMain();
            }
        }


        //Remove Permissions and Verify Creating, Amending Contacts
        public void RemovePermissionsToUser()
        {
            MainAsyncRemoveAllPermissionsToUser().GetAwaiter().GetResult();
        }
        public void VerifyContactsOperationWhenUsePermissionsRemoved()
        {
            MainAsyncVerifyCreatingPersonWhenUserHasNoPermission().GetAwaiter().GetResult();
            MainAsyncVerifyCreatingCompanyWhenUserHasNoPermission().GetAwaiter().GetResult();
            MainAsyncVerifyAmendPersonDetailsWhenUserHasNoPermission().GetAwaiter().GetResult();
            MainAsyncVerifyAmendCompanyDetailsWhenUserHasNoPermission().GetAwaiter().GetResult();
            MainAsyncVerifyCreatingQuoteWhenUserHasNoPermission().GetAwaiter().GetResult();
            MainAsyncVerifyAmendingQuoteWhenUserHasNoPermission().GetAwaiter().GetResult();
            MainAsyncVerifySearchContactsWhenUserHasNoPermission().GetAwaiter().GetResult();
            MainAsyncVerifyAmendCompanyNameDetailsWhenUserHasNoPermission().GetAwaiter().GetResult();
            NoPermissions = MainAsyncRemoveAllPermissionsToUser().GetAwaiter().GetResult();         
        }
        //Apply  Create Contact Permissions and Verify that user can Create Contact,Not Amending Contacts
        public void ApplyCreatePersonPermissionsToUser()
        {
            MainAsyncApplyPermissionsToCreateContact().GetAwaiter().GetResult();
        }
        public void VerifyCreatingPersonWhenUserApplyPermissions()
        {
            MainAsyncVerifyCreatingPersonWhenUserApplyPermission().GetAwaiter().GetResult();
            MainAsyncVerifyCreatingCompanyWhenUserApplyPermission().GetAwaiter().GetResult();
            MainAsyncVerifyAmendPersonDetailsWhenUserHasNoPermission().GetAwaiter().GetResult();
            MainAsyncVerifyAmendCompanyDetailsWhenUserHasNoPermission().GetAwaiter().GetResult();
            MainAsyncVerifyCreatingQuoteWhenUserHasNoPermission().GetAwaiter().GetResult();
            MainAsyncVerifyAmendingQuoteWhenUserHasNoPermission().GetAwaiter().GetResult();
            MainAsyncVerifySearchContactsWhenUserHasNoPermission().GetAwaiter().GetResult();
           

        }
        //Apply  Create Contact Permissions and Verify that user can Create Contact,Not Amending Contacts
        public void ApplyAmendPersonPermissionsToUser()
        {
            MainAsyncApplyPermissionsToAmendContact().GetAwaiter().GetResult();
        }
        public void VerifyAmendingPersonWhenUserApplyPermissions()
        {
            MainAsyncVerifyAmendPersonDetailsWhenUserApplyPermission().GetAwaiter().GetResult();
            MainAsyncVerifyAmendCompanyDetailsWhenUserApplyPermission().GetAwaiter().GetResult();
            MainAsyncVerifyCreatingPersonWhenUserHasNoPermission().GetAwaiter().GetResult();
            MainAsyncVerifyCreatingCompanyWhenUserHasNoPermission().GetAwaiter().GetResult();
            MainAsyncVerifyCreatingQuoteWhenUserHasNoPermission().GetAwaiter().GetResult();
            MainAsyncVerifyAmendingQuoteWhenUserHasNoPermission().GetAwaiter().GetResult();
            MainAsyncVerifySearchContactsWhenUserHasNoPermission().GetAwaiter().GetResult();
            AmendContactPermission = MainAsyncApplyPermissionsToAmendContact().GetAwaiter().GetResult();
           
        }
        //Apply Search Contact Permissions and Verify that user can serach contact 
        public void ApplySearchContactPermissionsToUser()
        {
            MainAsyncApplyPermissionsToSearchContact().GetAwaiter().GetResult();
        }
        public void VerifySearchContactWhenUserApplyPermissions()
        {
            MainAsyncVerifySearchContactsWhenUserApplyPermission().GetAwaiter().GetResult();
            MainAsyncVerifyCreatingPersonWhenUserHasNoPermission().GetAwaiter().GetResult();
            MainAsyncVerifyCreatingCompanyWhenUserHasNoPermission().GetAwaiter().GetResult();
            MainAsyncVerifyAmendPersonDetailsWhenUserHasNoPermission().GetAwaiter().GetResult();
            MainAsyncVerifyAmendCompanyDetailsWhenUserHasNoPermission().GetAwaiter().GetResult();
            MainAsyncVerifyCreatingQuoteWhenUserHasNoPermission().GetAwaiter().GetResult();
            MainAsyncVerifyAmendingQuoteWhenUserHasNoPermission().GetAwaiter().GetResult();
            ViewContactPermission = MainAsyncApplyPermissionsToSearchContact().GetAwaiter().GetResult();
          
        }
        //Apply Amend CompanyName Permissions and Verify that user can amend the companyname 
        public void ApplyAmendCompanyNamePermissionsToUser()
        {
            MainAsyncApplyPermissionsToAmendCompanyName().GetAwaiter().GetResult();
        }
        public void VerifyAmendCompanyNameWhenUserApplyPermissions()
        {
            MainAsyncVerifySearchContactsWhenUserApplyPermission().GetAwaiter().GetResult();
            MainAsyncVerifyCreatingPersonWhenUserApplyPermission().GetAwaiter().GetResult();
            MainAsyncVerifyCreatingCompanyWhenUserApplyPermission().GetAwaiter().GetResult();
            MainAsyncVerifyAmendPersonDetailsWhenUserApplyPermission().GetAwaiter().GetResult();
            MainAsyncVerifyCreatingCompanyWhenUserApplyPermission().GetAwaiter().GetResult();
            MainAsyncVerifyAmendCompanyNameDetailsWhenUserApplyPermission().GetAwaiter().GetResult();
            MainAsyncVerifySearchContactsWhenUserApplyPermission().GetAwaiter().GetResult();
        }
        //Apply Set-NonStandard Payment Permissions and Verify that user can amend the companyname 
        public void ApplyNonStandardPaymentPermissionsToUser()
        {
            MainAsyncApplyNonStandardPaymentPermissions().GetAwaiter().GetResult();
        }
        public void VerifyNonStandardPaymentWhenUserApplyPermissions()
        {
            MainAsyncVerifySearchContactsWhenUserApplyPermission().GetAwaiter().GetResult();
            MainAsyncVerifyCreatingPersonWhenUserApplyPermission().GetAwaiter().GetResult();
            MainAsyncVerifyCreatingCompanyWhenUserApplyPermission().GetAwaiter().GetResult();
            MainAsyncVerifyAmendPersonDetailsWhenUserApplyPermission().GetAwaiter().GetResult();
            MainAsyncVerifyCreatingCompanyWhenUserApplyPermission().GetAwaiter().GetResult();
            MainAsyncVerifyAmendCompanyNameDetailsWhenUserApplyPermission().GetAwaiter().GetResult();
            MainAsyncVerifySearchContactsWhenUserApplyPermission().GetAwaiter().GetResult();
        }

        public async Task MainAsyncVerifyCreatingPersonWhenUserHasNoPermission()
        {
            var uniqueGuID = System.Guid.NewGuid();
            var envrironmentApiServer = ConfigurationManager.AppSettings["ApiServerUrl"];
            var client = HttpClientHelpers.CreateClient("auto.user@mdinsurance.co.uk", "Mdis@1234;");
            string body = "{\"companyId\":null,\"title\":\"Mr\",\"firstname\":\"Sunilkumar\",\"surname\":\"Sunkishal\",\"twitterAddress\":\"\",\"faceBookAddress\":\"\",\"linkedInAddress\":\"\",\"googlePlusAddress\":\"\",\"email\":\"\",\"phoneCountryCode\":214,\"phone\":\"01483493721\",\"mobileCountryCode\":214,\"mobile\":\"07834233944\",\"websites\":[{\"address\":\"www.test.com\",\"isDiscontinued\":false}],\"isCompanyDirector\":false,\"isPurchasingDecisionMaker\":false,\"extension\":null,\"suffix\":\"\",\"jobTitle\":\"\",\"addresses\":[{\"isDefault\":true,\"address1\":\"M D Insurance Services Ltd\",\"address2\":\"2 Shore Lines Building\",\"address3\":\"Shore Road\",\"town\":\"Merseyside\",\"postcode\":\"CH41 1AU\",\"countryCodeType\":214,\"id\":\"e609651a-4363-4a12-a645-2017bf0c46eb\",\"isDiscontinued\":false}],\"hasLabcExtranetAccount\":false,\"hasPgExtranetAccount\":false,\"salesAndMarketingOptions\":{}}";
            var response = await client.PostAsync($"{envrironmentApiServer}/api/person/save", new StringContent(body, Encoding.Default, "application/json"));
            Assert.AreEqual(HttpStatusCode.Unauthorized, response.StatusCode);
        }
        public async Task MainAsyncVerifyCreatingCompanyWhenUserHasNoPermission()
        {
            var uniqueGuID = System.Guid.NewGuid();
            var envrironmentApiServer = ConfigurationManager.AppSettings["ApiServerUrl"];
            var client = HttpClientHelpers.CreateClient("auto.user@mdinsurance.co.uk", "Mdis@1234;");
            string body = string.Format("{{\"name\":\"MdisTest\",\"offices\":[{{\"name\":\"Test Office\",\"email\":\"test@gmail.com\",\"phoneNumber\":{{\"countryCodeType\":214,\"contactNumber\":\"01483493721\"}},\"isRegisteredAddress\":true,\"isDefault\":true,\"address1\":\"M D Insurance Services Ltd\",\"address2\":\"2 Shore Lines Building\",\"address3\":\"Shore Road\",\"town\":\"Merseyside\",\"postcode\":\"CH41 1AU\",\"countryCodeType\":214,\"salesAndMarketingOptions\":{{}},\"id\":\"{0}\",\"isDiscontinued\":false}}],\"yearEstablished\":2018,\"registrationNumber\":\"12345678\",\"legalFormType\":1,\"isVip\":true,\"websites\":[],\"hasEnhancedSocialHousingInsolvencyEndorsement\":true,\"employees\":[{{\"title\":\"Mr\",\"firstname\":\"Sunil\",\"surname\":\"Kumar\",\"email\":\"test@gmail.com\",\"phoneCountryCode\":214,\"mobileCountryCode\":214,\"personOffices\":[{{\"officeId\":\"{0}\",\"isPrimary\":true}}],\"isCompanyDirector\":true,\"isPurchasingDecisionMaker\":true,\"suffix\":\"s\",\"salesAndMarketingOptions\":{{}},\"isDiscontinued\":false}}],\"insuranceAgreementGroup\":2}}", uniqueGuID.ToString());                        
            var response = await client.PostAsync($"{envrironmentApiServer}/api/company/save", new StringContent(body, Encoding.Default, "application/json"));
            Assert.AreEqual(HttpStatusCode.Unauthorized, response.StatusCode);
        }
        public async Task MainAsyncVerifyAmendPersonDetailsWhenUserHasNoPermission()
        {
            var uniqueGuID = System.Guid.NewGuid();
            var envrironmentApiServer = ConfigurationManager.AppSettings["ApiServerUrl"];
            var client = HttpClientHelpers.CreateClient("auto.user@mdinsurance.co.uk", "Mdis@1234;");      
            string body = "{ \"title\":\"Mr\",\"firstname\":\"Sunil\",\"surname\":\"Sunkishala\",\"companyId\":null,\"twitterAddress\":\"\",\"faceBookAddress\":\"\",\"linkedInAddress\":\"\",\"googlePlusAddress\":\"\",\"email\":\"crisp.dev.c@ext.crisp.co.uk\",\"phoneCountryCode\":214,\"phone\":\"01483493721\",\"mobileCountryCode\":214,\"mobile\":\"07834233944\",\"websites\":[{\"address\":\"www.test.com\",\"id\":\"c0c2c9e3-ae48-4bd0-a31a-a94a00f9e025\",\"dateCreated\":\"2018-08-28T15:09:46.193Z\",\"createdById\":\"802f56f1-1b9a-43cc-9982-c3c4d8b49ae6\",\"dateUpdated\":\"2018-08-28T15:09:46.193Z\",\"updatedById\":\"802f56f1-1b9a-43cc-9982-c3c4d8b49ae6\",\"isDiscontinued\":false},{\"address\":\"www.test1.com\",\"isDiscontinued\":false}],\"addresses\":[{\"isDefault\":true,\"isInMiningArea\":false,\"miningArea\":null,\"radonExposure\":null,\"radonExposureScore\":0,\"weatherExposure\":4,\"isGeographyInformationAvailable\":false,\"isLandfill\":false,\"distanceFromCoast\":0,\"address1\":\"M D Insurance Services Ltd\",\"address2\":\"2 Shore Lines Building\",\"address3\":\"Shore Road\",\"town\":\"Merseyside\",\"postcode\":\"CH41 1AU\",\"fullAddress\":\"M D Insurance Services Ltd, 2 Shore Lines Building, Shore Road, Merseyside, CH41 1AU\",\"countryCodeType\":214,\"salesAndMarketingOptions\":{\"labcOptInForCalls\":false,\"labcOptInForEmails\":false,\"pgOptInForCalls\":false,\"pgOptInForEmails\":false,\"notedOnTelephonePreferenceServiceList\":false},\"kilometersFromCoast\":0,\"radonPotential\":\"0%\",\"id\":\"df89d2b2-2835-4bab-8ae5-a94a00f9e025\",\"dateCreated\":\"2018-08-28T15:09:46.193Z\",\"createdById\":\"802f56f1-1b9a-43cc-9982-c3c4d8b49ae6\",\"dateUpdated\":\"2018-08-28T15:09:46.193Z\",\"updatedById\":\"802f56f1-1b9a-43cc-9982-c3c4d8b49ae6\",\"isDiscontinued\":false}],\"personOffices\":[],\"isCompanyDirector\":false,\"isPurchasingDecisionMaker\":false,\"extension\":null,\"suffix\":\"\",\"isContractor\":false,\"fullTimeEquivalence\":0,\"department\":null,\"team\":null,\"jobTitle\":\"\",\"managerId\":null,\"pictureName\":null,\"signatureId\":null,\"signatureName\":null,\"remoteWorkerPrimaryOperatingLocation\":null,\"remoteWorkerGooglePlaceId\":null,\"hasLabcExtranetAccount\":false,\"hasPgExtranetAccount\":false,\"isBlocked\":false,\"salesAndMarketingOptions\":{\"labcOptInForCalls\":false,\"labcOptInForEmails\":false,\"pgOptInForCalls\":false,\"pgOptInForEmails\":false,\"notedOnTelephonePreferenceServiceList\":false},\"documents\":[],\"documentFolders\":[],\"documentBatches\":[],\"id\":\"1035bc4d-690f-4e8b-ab9b-a94a00f9e025\",\"dateCreated\":\"2018-08-28T15:09:46.193Z\",\"createdById\":\"802f56f1-1b9a-43cc-9982-c3c4d8b49ae6\",\"dateUpdated\":\"2018-08-28T15:09:46.193Z\",\"updatedById\":\"802f56f1-1b9a-43cc-9982-c3c4d8b49ae6\",\"isDiscontinued\":false}";
            var response = await client.PostAsync($"{envrironmentApiServer}/api/person/save", new StringContent(body, Encoding.Default, "application/json"));
            Assert.AreEqual(HttpStatusCode.Unauthorized, response.StatusCode);
        }
        public async Task MainAsyncVerifyAmendCompanyDetailsWhenUserHasNoPermission()
        {
            var uniqueGuID = System.Guid.NewGuid();
            var envrironmentApiServer = ConfigurationManager.AppSettings["ApiServerUrl"];
            var client = HttpClientHelpers.CreateClient("auto.user@mdinsurance.co.uk", "Mdis@1234;");
            string body = string.Format("{{\"name\":\"MdisTest\",\"companyId\":\"f411fdc3-770f-4e13-9395-a94b00c165fd\",\"offices\":[{{\"name\":\"Test Office\",\"email\":\"test@gmail.com\",\"phoneNumber\":{{\"countryCodeType\":214,\"contactNumber\":\"01483493721\",\"isPrimary\":false,\"id\":\"{0}\",\"dateCreated\":\"2018-08-29T11:44:08.566Z\",\"createdById\":\"4871face-876e-47fe-95bb-a94b00ad0c92\",\"dateUpdated\":\"2018-08-29T11:44:08.566Z\",\"updatedById\":\"4871face-876e-47fe-95bb-a94b00ad0c92\",\"isDiscontinued\":false}},\"isRegisteredAddress\":true,\"pgSalesAccountManagerId\":null,\"labcSalesAccountManagerId\":null,\"csuAccountManagerId\":null,\"personOffices\":[],\"salesAndMarketingOptions\":{{\"labcOptInForCalls\":false,\"labcOptInForEmails\":false,\"pgOptInForCalls\":false,\"pgOptInForEmails\":false,\"notedOnTelephonePreferenceServiceList\":false}},\"id\":\"09fc47b8-d0dd-4803-8947-b7ff9c68920a\",\"isDefault\":true,\"isDiscontinued\":false,\"country\":\"\",\"address1\":\"M D Insurance Services Ltd\",\"address2\":\"2 Shore Lines Building\",\"address3\":\"Shore Road\",\"town\":\"Merseyside\",\"postcode\":\"CH41 1AU\",\"countryCodeType\":214}}],\"yearEstablished\":2018,\"registrationNumber\":\"12345678\",\"legalFormType\":1,\"clientSegmentationType\":0,\"isVip\":true,\"isKeyNational\":false,\"twitterAddress\":null,\"faceBookAddress\":null,\"linkedInAddress\":null,\"googlePlusAddress\":null,\"websites\":[],\"isCompaniesHouseRecord\":false,\"hasEnhancedSocialHousingInsolvencyEndorsement\":true,\"depositPaymentPercentage\":null,\"maxNumberOfMonthlyInstalments\":null,\"insuranceAgreementGroup\":2}}", uniqueGuID.ToString());
            var response = await client.PostAsync($"{envrironmentApiServer}/api/company/save", new StringContent(body, Encoding.Default, "application/json"));
            Assert.AreEqual(HttpStatusCode.Unauthorized, response.StatusCode);
        }
        public async Task MainAsyncVerifyAmendCompanyNameDetailsWhenUserHasNoPermission()
        {
            var uniqueGuID = System.Guid.NewGuid();
            var envrironmentApiServer = ConfigurationManager.AppSettings["ApiServerUrl"];
            var client = HttpClientHelpers.CreateClient("auto.user@mdinsurance.co.uk", "Mdis@1234;");
            string body = string.Format("{{\"name\":\"EditComanyNameTest\",\"companyId\":\"f411fdc3-770f-4e13-9395-a94b00c165fd\",\"offices\":[{{\"name\":\"Test Office\",\"email\":\"test@gmail.com\",\"phoneNumber\":{{\"countryCodeType\":214,\"contactNumber\":\"01483493721\",\"isPrimary\":false,\"id\":\"{0}\",\"dateCreated\":\"2018-08-29T11:44:08.566Z\",\"createdById\":\"4871face-876e-47fe-95bb-a94b00ad0c92\",\"dateUpdated\":\"2018-08-29T11:44:08.566Z\",\"updatedById\":\"4871face-876e-47fe-95bb-a94b00ad0c92\",\"isDiscontinued\":false}},\"isRegisteredAddress\":true,\"pgSalesAccountManagerId\":null,\"labcSalesAccountManagerId\":null,\"csuAccountManagerId\":null,\"personOffices\":[],\"salesAndMarketingOptions\":{{\"labcOptInForCalls\":false,\"labcOptInForEmails\":false,\"pgOptInForCalls\":false,\"pgOptInForEmails\":false,\"notedOnTelephonePreferenceServiceList\":false}},\"id\":\"09fc47b8-d0dd-4803-8947-b7ff9c68920a\",\"isDefault\":true,\"isDiscontinued\":false,\"country\":\"\",\"address1\":\"M D Insurance Services Ltd\",\"address2\":\"2 Shore Lines Building\",\"address3\":\"Shore Road\",\"town\":\"Merseyside\",\"postcode\":\"CH41 1AU\",\"countryCodeType\":214}}],\"yearEstablished\":2018,\"registrationNumber\":\"12345678\",\"legalFormType\":1,\"clientSegmentationType\":0,\"isVip\":true,\"isKeyNational\":false,\"twitterAddress\":null,\"faceBookAddress\":null,\"linkedInAddress\":null,\"googlePlusAddress\":null,\"websites\":[],\"isCompaniesHouseRecord\":false,\"hasEnhancedSocialHousingInsolvencyEndorsement\":true,\"depositPaymentPercentage\":null,\"maxNumberOfMonthlyInstalments\":null,\"insuranceAgreementGroup\":2}}", uniqueGuID.ToString());
            var response = await client.PostAsync($"{envrironmentApiServer}/api/company/save", new StringContent(body, Encoding.Default, "application/json"));
            Assert.AreEqual(HttpStatusCode.Unauthorized, response.StatusCode);
        }
        public async Task MainAsyncVerifySearchContactsWhenUserHasNoPermission()
        {
            var uniqueGuID = System.Guid.NewGuid();
            var envrironmentApiServer = ConfigurationManager.AppSettings["ApiServerUrl"];
            var client = HttpClientHelpers.CreateClient("auto.user@mdinsurance.co.uk", "Mdis@1234;");
            string body = "{ \"name\":\"SunilKumar\",\"address\":\"\",\"email\":\"\",\"phone\":\"\",\"includeCompanies\":true,\"includeEmployees\":true,\"includePeople\":true,\"companyTypes\":[],\"employeeRoles\":[],\"internalOnly\":false,\"externalOnly\":false,\"ourRegistrationNumber\":\"\",\"internalBrandFilter\":null,\"start\":30,\"count\":30,\"sortBy\":\"name\",\"sortDirection\":\"asc\"}";           
            var response = await client.PostAsync($"{envrironmentApiServer}/api/contact/search", new StringContent(body, Encoding.Default, "application/json"));
          //  Assert.AreEqual(HttpStatusCode.Unauthorized, response.StatusCode);
        }     
        public async Task MainAsyncVerifyCreatingQuoteWhenUserHasNoPermission()
        {
            var uniqueGuID = System.Guid.NewGuid();
            var envrironmentApiServer = ConfigurationManager.AppSettings["ApiServerUrl"];
            var client = HttpClientHelpers.CreateClient("auto.user@mdinsurance.co.uk", "Mdis@1234;");
            string body = "{\"products\":[{\"productId\":\"9deb8006-8fc5-5883-9c67-68c720cb8b47\",\"length\":10,\"productItems\":[\"4ee6bbab-863d-5962-963f-3d78ea95f6c7\",\"434129ab-56e0-5bcd-b2a8-9928db020d2a\",\"d32a5034-c6fd-5137-8c62-050de9bcab2b\",\"fdd2b69b-b4da-50e2-b739-104842cd4bed\",\"281000cb-0b25-535e-adc2-5b660c6cc3c1\"]},{\"productId\":\"ed59c767-9507-5159-ad26-379d3e40a5fa\",\"length\":null,\"productItems\":[\"5e936810-6f36-5f3e-a998-4748b7897f95\",\"e878cc32-54d3-5b55-aa0e-e8cac563d790\",\"5e977bf2-064d-5df0-b1d3-04a4cffb1183\"]}],\"brand\":1,\"address\":{\"id\":\"38c19c75-0f88-4d68-a585-7d900db43181\",\"address1\":\"M D Insurance Services Ltd\",\"address2\":\"2 Shore Lines Building\",\"address3\":\"Shore Road\",\"town\":\"Merseyside\",\"postcode\":\"CH41 1AU\",\"countryCodeType\":214},\"documents\":[{\"fileSize\":2973,\"fileId\":\"3204d2ee-aacc-4c6f-84d4-a94b008e9817\",\"fileName\":\"Commercial.xlsx\",\"dateReceived\":\"2018-08-29T08:39:02.611Z\"}],\"constructionStartDate\":\"2018-08-01T00:00:00.000Z\",\"constructionEndDate\":\"2018-08-29T00:00:00.000Z\",\"maxStoreysAboveGround\":1,\"maxStoreysBelowGround\":1,\"hasInnovativeMethods\":false,\"hasUnitsOrStructureAttached\":false,\"isSiteInAdministration\":false,\"quoteRecipient\":\"e530a875-3075-49dc-bd73-a92a0110f4c6\",\"additionalRoles\":[{\"role\":5,\"ids\":[\"e530a875-3075-49dc-bd73-a92a0110f4c6\"]},{\"role\":39,\"ids\":[\"e530a875-3075-49dc-bd73-a92a0110f4c6\"]},{\"role\":14,\"ids\":[\"e530a875-3075-49dc-bd73-a92a0110f4c6\"]},{\"role\":37,\"ids\":[\"e530a875-3075-49dc-bd73-a92a0110f4c6\"]},{\"role\":15,\"ids\":[\"4ef240ae-347d-447e-9894-1e4b58d93358\"]},{\"role\":32,\"ids\":[\"e2ac35ab-e72d-451e-a5b7-296575ce7311\"]},{\"role\":20,\"ids\":[\"e4617f00-25cb-4975-8c4b-a92a00b3cb03\"]},{\"role\":7,\"ids\":[\"e4617f00-25cb-4975-8c4b-a92a00b3cb03\"]},{\"role\":19,\"ids\":[\"7446a7ec-d415-4d89-92cf-0086bd07c565\"]}],\"selfBuildAdditionalQuestions\":{\"isArchitectInvolved\":false,\"isHomeOwnerResponsibleForConstructionInThePast\":false,\"numberOfStagePayments\":1,\"isSolePlaceOfResidence\":false},\"completedHousingAdditionalQuestions\":{},\"inAdminstrationAdditionalQuestions\":{},\"generalInsuranceQuestions\":{\"sustainedAnyLossesOrHadAnyClaimsInTheLastThreeYears\":false,\"everBeenRefusedPropertyInsuranceOrSpecialTermsImposed\":false,\"everBeenConvictedOrProsecutionPendingInvolvingDishonesty\":false,\"everBeenProsecutedOrReceivedNotificationUnderHswa\":false,\"everBeenInvolvedWithLiquidatedOrBankruptBuilderOnConstructionCompany\":false},\"conversionQuestions\":{},\"hasApplicantConfirmedDetailsAreCorrect\":true,\"plotScheduleFileId\":\"cdf409fa-08eb-4708-9926-a94b008edcca\",\"brokerAgreementId\":null,\"lossOfGrossRentAnnualAmountNumberOfYears\":null,\"lossOfGrossRentAnnualAmount\":null,\"lossOfRentPayableAnnualAmountNumberOfYears\":null,\"lossOfRentPayableAnnualAmount\":null,\"lossOfRentRecievableAmmountNumberOfYears\":null,\"lossofRentReceivableAnnualAmount\":null,\"quoteVersionsParentQuoteId\":null,\"insuranceAgreementGroup\":2,\"productVersionDate\":\"2018-08-29\"}";
            var response = await client.PostAsync($"{envrironmentApiServer}/api/quote/application/submit", new StringContent(body, Encoding.Default, "application/json"));
            Assert.AreEqual(HttpStatusCode.Unauthorized, response.StatusCode);
        }
        public async Task MainAsyncVerifyAmendingQuoteWhenUserHasNoPermission()
        {
            var uniqueGuID = System.Guid.NewGuid();
            var envrironmentApiServer = ConfigurationManager.AppSettings["ApiServerUrl"];
            var client = HttpClientHelpers.CreateClient("auto.user@mdinsurance.co.uk", "Mdis@1234;");
            string body = "{\"products\":[{\"productId\":\"9deb8006-8fc5-5883-9c67-68c720cb8b47\",\"contractCost\":0,\"length\":10,\"isWorkContractUnderSeal\":false,\"productItems\":[\"4ee6bbab-863d-5962-963f-3d78ea95f6c7\",\"434129ab-56e0-5bcd-b2a8-9928db020d2a\",\"d32a5034-c6fd-5137-8c62-050de9bcab2b\",\"fdd2b69b-b4da-50e2-b739-104842cd4bed\",\"281000cb-0b25-535e-adc2-5b660c6cc3c1\"]},{\"productId\":\"ed59c767-9507 -5159 - ad26 - 379d3e40a5fa\",\"contractCost\":0,\"length\":null,\"isWorkContractUnderSeal\":false,\"productItems\":[\"5e936810-6f36-5f3e-a998-4748b7897f95\",\"e878cc32-54d3-5b55-aa0e-e8cac563d790\",\"5e977bf2-064d-5df0-b1d3-04a4cffb1183\"]}],\"quoteId\":\"35c8c18c-008c-41dc-9488-a94b0092ef66\",\"brand\":1,\"address\":{\"id\":\"d4172961-eec6-4b81-bc1b-a94b0092efb1\",\"isDefault\":false,\"isDiscontinued\":false,\"address1\":\"M D Insurance Services Ltd\",\"address2\":\"2 Shore Lines Building\",\"address3\":\"Shore Road\",\"town\":\"Merseyside\",\"postcode\":\"CH41 1AU\",\"countryCodeType\":214},\"documents\":[{\"fileSize\":2973,\"fileId\":\"3204d2ee-aacc-4c6f-84d4-a94b008e9817\",\"fileName\":\"Commercial.xlsx\",\"dateReceived\":\"2018-08-29T00: 00:00.000Z\"}],\"constructionStartDate\":\"2018-08-02T00: 00:00.000Z\",\"constructionEndDate\":\"2018-08-29T00: 00:00.000Z\",\"maxStoreysAboveGround\":1,\"maxStoreysBelowGround\":1,\"hasInnovativeMethods\":false,\"innovativeMethodsUsed\":null,\"hasUnitsOrStructureAttached\":false,\"isSiteInAdministration\":false,\"numberOfResidentialUnits\":null,\"totalReconCostOfResidentialUnits\":null,\"numberOfCommercialUnits\":null,\"totalReconCostOfCommercialUnits\":null,\"summaryOfWorks\":null,\"isWarrantyAddedLater\":null,\"quoteRecipient\":\"e530a875-3075-49dc-bd73-a92a0110f4c6\",\"additionalRoles\":[{\"role\":19,\"ids\":[\"7446a7ec-d415-4d89-92cf-0086bd07c565\"]},{\"role\":7,\"ids\":[\"e4617f00-25cb-4975-8c4b-a92a00b3cb03\"]},{\"role\":32,\"ids\":[\"e2ac35ab-e72d-451e-a5b7-296575ce7311\"]},{\"role\":37,\"ids\":[\"e530a875-3075-49dc-bd73-a92a0110f4c6\"]},{\"role\":16,\"ids\":[\"f65fef74-814f-4ed1-bc3d-940e723354dc\"]},{\"role\":15,\"ids\":[\"4ef240ae-347d-447e-9894-1e4b58d93358\"]},{\"role\":20,\"ids\":[\"e4617f00-25cb-4975-8c4b-a92a00b3cb03\"]},{\"role\":14,\"ids\":[\"e530a875-3075-49dc-bd73-a92a0110f4c6\"]},{\"role\":39,\"ids\":[\"e530a875 - 3075 - 49dc - bd73 - a92a0110f4c6\"]},{\"role\":5,\"ids\":[\"e530a875 - 3075 - 49dc - bd73 - a92a0110f4c6\"]},{\"role\":44,\"ids\":[\"4018d9f8 - 8790 - 44e4 - bbbb - a9440109b644\"]}],\"selfBuildAdditionalQuestions\":{\"isArchitectInvolved\":false,\"architectLevelOfInvolvementType\":0,\"isHomeOwnerResponsibleForConstructionInThePast\":false,\"homeOwnerResponsibleForConstructionInPastNoOfUnits\":null,\"homeOwnerResponsibleForConstructionInPastTenure\":0,\"numberOfStagePayments\":1,\"isSolePlaceOfResidence\":false},\"completedHousingAdditionalQuestions\":{\"reasonWhyStructuralWarrantyNotInPlace\":null},\"inAdminstrationAdditionalQuestions\":{\"isOriginallyRegisteredWithNewHomeWarranty\":null,\"originalRegisteredWarrantyProvider\":0,\"originalRegisteredWarrantyProviderOtherDetail\":null,\"previousDeveloper\":null,\"previousBuildingControlProvider\":null},\"generalInsuranceQuestions\":{\"sustainedAnyLossesOrHadAnyClaimsInTheLastThreeYears\":false,\"sustainedAnyLossesOrHadAnyClaimsInTheLastThreeYearsDetails\":null,\"everBeenRefusedPropertyInsuranceOrSpecialTermsImposed\":false,\"everBeenRefusedPropertyInsuranceOrSpecialTermsImposedDetails\":null,\"everBeenConvictedOrProsecutionPendingInvolvingDishonesty\":false,\"everBeenConvictedOrProsecutionPendingInvolvingDishonestyDetails\":null,\"everBeenProsecutedOrReceivedNotificationUnderHswa\":false,\"everBeenProsecutedOrReceivedNotificationUnderHswaDetails\":null,\"everBeenInvolvedWithLiquidatedOrBankruptBuilderOnConstructionCompany\":false,\"involvedWithLiquidatedOrBankruptBuilderOnConstructionCompanyName\":null,\"involvedWithLiquidatedOrBankruptBuilderOnConstructionCompanyDetails\":null},\"conversionQuestions\":{\"originalBuildYear\":null,\"buildingPreviousUses\":null,\"buildingPreviousUsesOther\":null,\"buildingListedStatus\":null,\"siteInConservationArea\":null,\"descriptionOfWorks\":null},\"hasApplicantConfirmedDetailsAreCorrect\":true,\"brokerAgreementId\":null,\"leadId\":null,\"lossOfGrossRentAnnualAmountNumberOfYears\":null,\"lossOfGrossRentAnnualAmount\":null,\"lossOfRentPayableAnnualAmountNumberOfYears\":null,\"lossOfRentPayableAnnualAmount\":null,\"lossOfRentRecievableAmmountNumberOfYears\":null,\"lossofRentReceivableAnnualAmount\":null,\"quoteVersionsParentQuoteId\":null,\"insuranceAgreementGroup\":2,\"doesTheSiteIncludeCurtainWalling\":false,\"atkinsBuildDuration\":0,\"productVersionDate\":\"2018-08-28\"}";
            var response = await client.PostAsync($"{envrironmentApiServer}/api/quote/application/save", new StringContent(body, Encoding.Default, "application/json"));
            Assert.AreEqual(HttpStatusCode.Unauthorized, response.StatusCode);
        }
        public async Task MainAsyncVerifyCreatingPersonWhenUserApplyPermission()
        {
            var uniqueGuID = System.Guid.NewGuid();
            var envrironmentApiServer = ConfigurationManager.AppSettings["ApiServerUrl"];
            var client = HttpClientHelpers.CreateClient("auto.user@mdinsurance.co.uk", "Mdis@1234;");
            string body = "{\"companyId\":null,\"title\":\"Mr\",\"firstname\":\"Sunilkumar\",\"surname\":\"Sunkishal\",\"twitterAddress\":\"\",\"faceBookAddress\":\"\",\"linkedInAddress\":\"\",\"googlePlusAddress\":\"\",\"email\":\"\",\"phoneCountryCode\":214,\"phone\":\"01483493721\",\"mobileCountryCode\":214,\"mobile\":\"07834233944\",\"websites\":[{\"address\":\"www.test.com\",\"isDiscontinued\":false}],\"isCompanyDirector\":false,\"isPurchasingDecisionMaker\":false,\"extension\":null,\"suffix\":\"\",\"jobTitle\":\"\",\"addresses\":[{\"isDefault\":true,\"address1\":\"M D Insurance Services Ltd\",\"address2\":\"2 Shore Lines Building\",\"address3\":\"Shore Road\",\"town\":\"Merseyside\",\"postcode\":\"CH41 1AU\",\"countryCodeType\":214,\"id\":\"e609651a-4363-4a12-a645-2017bf0c46eb\",\"isDiscontinued\":false}],\"hasLabcExtranetAccount\":false,\"hasPgExtranetAccount\":false,\"salesAndMarketingOptions\":{}}";
            var response = await client.PostAsync($"{envrironmentApiServer}/api/person/save", new StringContent(body, Encoding.Default, "application/json"));
            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
        }
        public async Task MainAsyncVerifyCreatingCompanyWhenUserApplyPermission()
        {
            var uniqueGuID = System.Guid.NewGuid();
            var envrironmentApiServer = ConfigurationManager.AppSettings["ApiServerUrl"];
            var client = HttpClientHelpers.CreateClient("auto.user@mdinsurance.co.uk", "Mdis@1234;");
            string body = string.Format("{{\"name\":\"MdisTest\",\"offices\":[{{\"name\":\"Test Office\",\"email\":\"test@gmail.com\",\"phoneNumber\":{{\"countryCodeType\":214,\"contactNumber\":\"01483493721\"}},\"isRegisteredAddress\":true,\"isDefault\":true,\"address1\":\"M D Insurance Services Ltd\",\"address2\":\"2 Shore Lines Building\",\"address3\":\"Shore Road\",\"town\":\"Merseyside\",\"postcode\":\"CH41 1AU\",\"countryCodeType\":214,\"salesAndMarketingOptions\":{{}},\"id\":\"{0}\",\"isDiscontinued\":false}}],\"yearEstablished\":2018,\"registrationNumber\":\"12345678\",\"legalFormType\":1,\"isVip\":true,\"websites\":[],\"hasEnhancedSocialHousingInsolvencyEndorsement\":true,\"employees\":[{{\"title\":\"Mr\",\"firstname\":\"Sunil\",\"surname\":\"Kumar\",\"email\":\"test@gmail.com\",\"phoneCountryCode\":214,\"mobileCountryCode\":214,\"personOffices\":[{{\"officeId\":\"{0}\",\"isPrimary\":true}}],\"isCompanyDirector\":true,\"isPurchasingDecisionMaker\":true,\"suffix\":\"s\",\"salesAndMarketingOptions\":{{}},\"isDiscontinued\":false}}],\"insuranceAgreementGroup\":2}}", uniqueGuID.ToString());
            var response = await client.PostAsync($"{envrironmentApiServer}/api/company/save", new StringContent(body, Encoding.Default, "application/json"));
            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
        }
        public async Task MainAsyncVerifyAmendPersonDetailsWhenUserApplyPermission()
        {
            var uniqueGuID = System.Guid.NewGuid();
            var envrironmentApiServer = ConfigurationManager.AppSettings["ApiServerUrl"];
            var client = HttpClientHelpers.CreateClient("auto.user@mdinsurance.co.uk", "Mdis@1234;");
            string body = "{ \"title\":\"Mr\",\"firstname\":\"Sunil\",\"surname\":\"Sunkishala\",\"companyId\":null,\"twitterAddress\":\"\",\"faceBookAddress\":\"\",\"linkedInAddress\":\"\",\"googlePlusAddress\":\"\",\"email\":\"crisp.dev.c@ext.crisp.co.uk\",\"phoneCountryCode\":214,\"phone\":\"01483493721\",\"mobileCountryCode\":214,\"mobile\":\"07834233944\",\"websites\":[{\"address\":\"www.test.com\",\"id\":\"c0c2c9e3-ae48-4bd0-a31a-a94a00f9e025\",\"dateCreated\":\"2018-08-28T15:09:46.193Z\",\"createdById\":\"802f56f1-1b9a-43cc-9982-c3c4d8b49ae6\",\"dateUpdated\":\"2018-08-28T15:09:46.193Z\",\"updatedById\":\"802f56f1-1b9a-43cc-9982-c3c4d8b49ae6\",\"isDiscontinued\":false},{\"address\":\"www.test1.com\",\"isDiscontinued\":false}],\"addresses\":[{\"isDefault\":true,\"isInMiningArea\":false,\"miningArea\":null,\"radonExposure\":null,\"radonExposureScore\":0,\"weatherExposure\":4,\"isGeographyInformationAvailable\":false,\"isLandfill\":false,\"distanceFromCoast\":0,\"address1\":\"M D Insurance Services Ltd\",\"address2\":\"2 Shore Lines Building\",\"address3\":\"Shore Road\",\"town\":\"Merseyside\",\"postcode\":\"CH41 1AU\",\"fullAddress\":\"M D Insurance Services Ltd, 2 Shore Lines Building, Shore Road, Merseyside, CH41 1AU\",\"countryCodeType\":214,\"salesAndMarketingOptions\":{\"labcOptInForCalls\":false,\"labcOptInForEmails\":false,\"pgOptInForCalls\":false,\"pgOptInForEmails\":false,\"notedOnTelephonePreferenceServiceList\":false},\"kilometersFromCoast\":0,\"radonPotential\":\"0%\",\"id\":\"df89d2b2-2835-4bab-8ae5-a94a00f9e025\",\"dateCreated\":\"2018-08-28T15:09:46.193Z\",\"createdById\":\"802f56f1-1b9a-43cc-9982-c3c4d8b49ae6\",\"dateUpdated\":\"2018-08-28T15:09:46.193Z\",\"updatedById\":\"802f56f1-1b9a-43cc-9982-c3c4d8b49ae6\",\"isDiscontinued\":false}],\"personOffices\":[],\"isCompanyDirector\":false,\"isPurchasingDecisionMaker\":false,\"extension\":null,\"suffix\":\"\",\"isContractor\":false,\"fullTimeEquivalence\":0,\"department\":null,\"team\":null,\"jobTitle\":\"\",\"managerId\":null,\"pictureName\":null,\"signatureId\":null,\"signatureName\":null,\"remoteWorkerPrimaryOperatingLocation\":null,\"remoteWorkerGooglePlaceId\":null,\"hasLabcExtranetAccount\":false,\"hasPgExtranetAccount\":false,\"isBlocked\":false,\"salesAndMarketingOptions\":{\"labcOptInForCalls\":false,\"labcOptInForEmails\":false,\"pgOptInForCalls\":false,\"pgOptInForEmails\":false,\"notedOnTelephonePreferenceServiceList\":false},\"documents\":[],\"documentFolders\":[],\"documentBatches\":[],\"id\":\"1035bc4d-690f-4e8b-ab9b-a94a00f9e025\",\"dateCreated\":\"2018-08-28T15:09:46.193Z\",\"createdById\":\"802f56f1-1b9a-43cc-9982-c3c4d8b49ae6\",\"dateUpdated\":\"2018-08-28T15:09:46.193Z\",\"updatedById\":\"802f56f1-1b9a-43cc-9982-c3c4d8b49ae6\",\"isDiscontinued\":false}";
            var response = await client.PostAsync($"{envrironmentApiServer}/api/person/save", new StringContent(body, Encoding.Default, "application/json"));
            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
        }
        public async Task MainAsyncVerifyAmendCompanyDetailsWhenUserApplyPermission()
        {
            var uniqueGuID = System.Guid.NewGuid();
            var envrironmentApiServer = ConfigurationManager.AppSettings["ApiServerUrl"];
            var client = HttpClientHelpers.CreateClient("auto.user@mdinsurance.co.uk", "Mdis@1234;");
            string body = string.Format("{{\"name\":\"MdisTestTest\",\"companyId\":\"f411fdc3-770f-4e13-9395-a94b00c165fd\",\"offices\":[{{\"name\":\"Test Office\",\"email\":\"test@gmail.com\",\"phoneNumber\":{{\"countryCodeType\":214,\"contactNumber\":\"01483493721\",\"isPrimary\":false,\"id\":\"{0}\",\"dateCreated\":\"2018-08-29T11:44:08.566Z\",\"createdById\":\"4871face-876e-47fe-95bb-a94b00ad0c92\",\"dateUpdated\":\"2018-08-29T11:44:08.566Z\",\"updatedById\":\"4871face-876e-47fe-95bb-a94b00ad0c92\",\"isDiscontinued\":false}},\"isRegisteredAddress\":true,\"pgSalesAccountManagerId\":null,\"labcSalesAccountManagerId\":null,\"csuAccountManagerId\":null,\"personOffices\":[],\"salesAndMarketingOptions\":{{\"labcOptInForCalls\":false,\"labcOptInForEmails\":false,\"pgOptInForCalls\":false,\"pgOptInForEmails\":false,\"notedOnTelephonePreferenceServiceList\":false}},\"id\":\"09fc47b8-d0dd-4803-8947-b7ff9c68920a\",\"isDefault\":true,\"isDiscontinued\":false,\"country\":\"\",\"address1\":\"M D Insurance Services Ltd\",\"address2\":\"2 Shore Lines Building\",\"address3\":\"Shore Road\",\"town\":\"Merseyside\",\"postcode\":\"CH41 1AU\",\"countryCodeType\":214}}],\"yearEstablished\":2018,\"registrationNumber\":\"12345678\",\"legalFormType\":1,\"clientSegmentationType\":0,\"isVip\":true,\"isKeyNational\":false,\"twitterAddress\":null,\"faceBookAddress\":null,\"linkedInAddress\":null,\"googlePlusAddress\":null,\"websites\":[],\"isCompaniesHouseRecord\":false,\"hasEnhancedSocialHousingInsolvencyEndorsement\":true,\"depositPaymentPercentage\":null,\"maxNumberOfMonthlyInstalments\":null,\"insuranceAgreementGroup\":2}}", uniqueGuID.ToString());
            var response = await client.PostAsync($"{envrironmentApiServer}/api/company/save", new StringContent(body, Encoding.Default, "application/json"));
            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
        }
        public async Task MainAsyncVerifyAmendCompanyNameDetailsWhenUserApplyPermission()
        {
            var uniqueGuID = System.Guid.NewGuid();
            var envrironmentApiServer = ConfigurationManager.AppSettings["ApiServerUrl"];
            var client = HttpClientHelpers.CreateClient("auto.user@mdinsurance.co.uk", "Mdis@1234;");
            string body = string.Format("{{\"name\":\"EditComanyNameTest\",\"companyId\":\"f411fdc3-770f-4e13-9395-a94b00c165fd\",\"offices\":[{{\"name\":\"Test Office\",\"email\":\"test@gmail.com\",\"phoneNumber\":{{\"countryCodeType\":214,\"contactNumber\":\"01483493721\",\"isPrimary\":false,\"id\":\"{0}\",\"dateCreated\":\"2018-08-29T11:44:08.566Z\",\"createdById\":\"4871face-876e-47fe-95bb-a94b00ad0c92\",\"dateUpdated\":\"2018-08-29T11:44:08.566Z\",\"updatedById\":\"4871face-876e-47fe-95bb-a94b00ad0c92\",\"isDiscontinued\":false}},\"isRegisteredAddress\":true,\"pgSalesAccountManagerId\":null,\"labcSalesAccountManagerId\":null,\"csuAccountManagerId\":null,\"personOffices\":[],\"salesAndMarketingOptions\":{{\"labcOptInForCalls\":false,\"labcOptInForEmails\":false,\"pgOptInForCalls\":false,\"pgOptInForEmails\":false,\"notedOnTelephonePreferenceServiceList\":false}},\"id\":\"09fc47b8-d0dd-4803-8947-b7ff9c68920a\",\"isDefault\":true,\"isDiscontinued\":false,\"country\":\"\",\"address1\":\"M D Insurance Services Ltd\",\"address2\":\"2 Shore Lines Building\",\"address3\":\"Shore Road\",\"town\":\"Merseyside\",\"postcode\":\"CH41 1AU\",\"countryCodeType\":214}}],\"yearEstablished\":2018,\"registrationNumber\":\"12345678\",\"legalFormType\":1,\"clientSegmentationType\":0,\"isVip\":true,\"isKeyNational\":false,\"twitterAddress\":null,\"faceBookAddress\":null,\"linkedInAddress\":null,\"googlePlusAddress\":null,\"websites\":[],\"isCompaniesHouseRecord\":false,\"hasEnhancedSocialHousingInsolvencyEndorsement\":true,\"depositPaymentPercentage\":null,\"maxNumberOfMonthlyInstalments\":null,\"insuranceAgreementGroup\":2}}", uniqueGuID.ToString());
            var response = await client.PostAsync($"{envrironmentApiServer}/api/company/save", new StringContent(body, Encoding.Default, "application/json"));
            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
        }
        public async Task MainAsyncVerifySearchContactsWhenUserApplyPermission()
        {
            var uniqueGuID = System.Guid.NewGuid();
            var envrironmentApiServer = ConfigurationManager.AppSettings["ApiServerUrl"];
            var client = HttpClientHelpers.CreateClient("auto.user@mdinsurance.co.uk", "Mdis@1234;");
            string body = "{\"name\":\"SunilKumar\",\"address\":\"\",\"email\":\"\",\"phone\":\"\",\"includeCompanies\":true,\"includeEmployees\":true,\"includePeople\":true,\"companyTypes\":[],\"employeeRoles\":[],\"internalOnly\":false,\"externalOnly\":false,\"ourRegistrationNumber\":\"\",\"internalBrandFilter\":null,\"start\":30,\"count\":30,\"sortBy\":\"name\",\"sortDirection\":\"asc\"}";
            var response = await client.PostAsync($"{envrironmentApiServer}/api/contact/search", new StringContent(body, Encoding.Default, "application/json"));
            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
        }
        public async Task MainAsyncVerifyCreatingQuoteWhenUserApplyPermission()
        {
            var uniqueGuID = System.Guid.NewGuid();
            var envrironmentApiServer = ConfigurationManager.AppSettings["ApiServerUrl"];
            var client = HttpClientHelpers.CreateClient("auto.user@mdinsurance.co.uk", "Mdis@1234;");
            string body = "{\"products\":[{\"productId\":\"9deb8006-8fc5-5883-9c67-68c720cb8b47\",\"length\":10,\"productItems\":[\"4ee6bbab-863d-5962-963f-3d78ea95f6c7\",\"434129ab-56e0-5bcd-b2a8-9928db020d2a\",\"d32a5034-c6fd-5137-8c62-050de9bcab2b\",\"fdd2b69b-b4da-50e2-b739-104842cd4bed\",\"281000cb-0b25-535e-adc2-5b660c6cc3c1\"]},{\"productId\":\"ed59c767-9507-5159-ad26-379d3e40a5fa\",\"length\":null,\"productItems\":[\"5e936810-6f36-5f3e-a998-4748b7897f95\",\"e878cc32-54d3-5b55-aa0e-e8cac563d790\",\"5e977bf2-064d-5df0-b1d3-04a4cffb1183\"]}],\"brand\":1,\"address\":{\"id\":\"38c19c75-0f88-4d68-a585-7d900db43181\",\"address1\":\"M D Insurance Services Ltd\",\"address2\":\"2 Shore Lines Building\",\"address3\":\"Shore Road\",\"town\":\"Merseyside\",\"postcode\":\"CH41 1AU\",\"countryCodeType\":214},\"documents\":[{\"fileSize\":2973,\"fileId\":\"3204d2ee-aacc-4c6f-84d4-a94b008e9817\",\"fileName\":\"Commercial.xlsx\",\"dateReceived\":\"2018-08-29T08:39:02.611Z\"}],\"constructionStartDate\":\"2018-08-01T00:00:00.000Z\",\"constructionEndDate\":\"2018-08-29T00:00:00.000Z\",\"maxStoreysAboveGround\":1,\"maxStoreysBelowGround\":1,\"hasInnovativeMethods\":false,\"hasUnitsOrStructureAttached\":false,\"isSiteInAdministration\":false,\"quoteRecipient\":\"e530a875-3075-49dc-bd73-a92a0110f4c6\",\"additionalRoles\":[{\"role\":5,\"ids\":[\"e530a875-3075-49dc-bd73-a92a0110f4c6\"]},{\"role\":39,\"ids\":[\"e530a875-3075-49dc-bd73-a92a0110f4c6\"]},{\"role\":14,\"ids\":[\"e530a875-3075-49dc-bd73-a92a0110f4c6\"]},{\"role\":37,\"ids\":[\"e530a875-3075-49dc-bd73-a92a0110f4c6\"]},{\"role\":15,\"ids\":[\"4ef240ae-347d-447e-9894-1e4b58d93358\"]},{\"role\":32,\"ids\":[\"e2ac35ab-e72d-451e-a5b7-296575ce7311\"]},{\"role\":20,\"ids\":[\"e4617f00-25cb-4975-8c4b-a92a00b3cb03\"]},{\"role\":7,\"ids\":[\"e4617f00-25cb-4975-8c4b-a92a00b3cb03\"]},{\"role\":19,\"ids\":[\"7446a7ec-d415-4d89-92cf-0086bd07c565\"]}],\"selfBuildAdditionalQuestions\":{\"isArchitectInvolved\":false,\"isHomeOwnerResponsibleForConstructionInThePast\":false,\"numberOfStagePayments\":1,\"isSolePlaceOfResidence\":false},\"completedHousingAdditionalQuestions\":{},\"inAdminstrationAdditionalQuestions\":{},\"generalInsuranceQuestions\":{\"sustainedAnyLossesOrHadAnyClaimsInTheLastThreeYears\":false,\"everBeenRefusedPropertyInsuranceOrSpecialTermsImposed\":false,\"everBeenConvictedOrProsecutionPendingInvolvingDishonesty\":false,\"everBeenProsecutedOrReceivedNotificationUnderHswa\":false,\"everBeenInvolvedWithLiquidatedOrBankruptBuilderOnConstructionCompany\":false},\"conversionQuestions\":{},\"hasApplicantConfirmedDetailsAreCorrect\":true,\"plotScheduleFileId\":\"cdf409fa-08eb-4708-9926-a94b008edcca\",\"brokerAgreementId\":null,\"lossOfGrossRentAnnualAmountNumberOfYears\":null,\"lossOfGrossRentAnnualAmount\":null,\"lossOfRentPayableAnnualAmountNumberOfYears\":null,\"lossOfRentPayableAnnualAmount\":null,\"lossOfRentRecievableAmmountNumberOfYears\":null,\"lossofRentReceivableAnnualAmount\":null,\"quoteVersionsParentQuoteId\":null,\"insuranceAgreementGroup\":2,\"productVersionDate\":\"2018-08-29\"}";
            var response = await client.PostAsync($"{envrironmentApiServer}/api/quote/application/submit", new StringContent(body, Encoding.Default, "application/json"));
            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
        }
        public async Task MainAsyncVerifyAmendingQuoteWhenUserApplyPermission()
        {
            var uniqueGuID = System.Guid.NewGuid();
            var envrironmentApiServer = ConfigurationManager.AppSettings["ApiServerUrl"];
            var client = HttpClientHelpers.CreateClient("auto.user@mdinsurance.co.uk", "Mdis@1234;");
            string body = "{\"products\":[{\"productId\":\"9deb8006-8fc5-5883-9c67-68c720cb8b47\",\"contractCost\":0,\"length\":10,\"isWorkContractUnderSeal\":false,\"productItems\":[\"4ee6bbab-863d-5962-963f-3d78ea95f6c7\",\"434129ab-56e0-5bcd-b2a8-9928db020d2a\",\"d32a5034-c6fd-5137-8c62-050de9bcab2b\",\"fdd2b69b-b4da-50e2-b739-104842cd4bed\",\"281000cb-0b25-535e-adc2-5b660c6cc3c1\"]},{\"productId\":\"ed59c767-9507 -5159 - ad26 - 379d3e40a5fa\",\"contractCost\":0,\"length\":null,\"isWorkContractUnderSeal\":false,\"productItems\":[\"5e936810-6f36-5f3e-a998-4748b7897f95\",\"e878cc32-54d3-5b55-aa0e-e8cac563d790\",\"5e977bf2-064d-5df0-b1d3-04a4cffb1183\"]}],\"quoteId\":\"35c8c18c-008c-41dc-9488-a94b0092ef66\",\"brand\":1,\"address\":{\"id\":\"d4172961-eec6-4b81-bc1b-a94b0092efb1\",\"isDefault\":false,\"isDiscontinued\":false,\"address1\":\"M D Insurance Services Ltd\",\"address2\":\"2 Shore Lines Building\",\"address3\":\"Shore Road\",\"town\":\"Merseyside\",\"postcode\":\"CH41 1AU\",\"countryCodeType\":214},\"documents\":[{\"fileSize\":2973,\"fileId\":\"3204d2ee-aacc-4c6f-84d4-a94b008e9817\",\"fileName\":\"Commercial.xlsx\",\"dateReceived\":\"2018-08-29T00: 00:00.000Z\"}],\"constructionStartDate\":\"2018-08-02T00: 00:00.000Z\",\"constructionEndDate\":\"2018-08-29T00: 00:00.000Z\",\"maxStoreysAboveGround\":1,\"maxStoreysBelowGround\":1,\"hasInnovativeMethods\":false,\"innovativeMethodsUsed\":null,\"hasUnitsOrStructureAttached\":false,\"isSiteInAdministration\":false,\"numberOfResidentialUnits\":null,\"totalReconCostOfResidentialUnits\":null,\"numberOfCommercialUnits\":null,\"totalReconCostOfCommercialUnits\":null,\"summaryOfWorks\":null,\"isWarrantyAddedLater\":null,\"quoteRecipient\":\"e530a875-3075-49dc-bd73-a92a0110f4c6\",\"additionalRoles\":[{\"role\":19,\"ids\":[\"7446a7ec-d415-4d89-92cf-0086bd07c565\"]},{\"role\":7,\"ids\":[\"e4617f00-25cb-4975-8c4b-a92a00b3cb03\"]},{\"role\":32,\"ids\":[\"e2ac35ab-e72d-451e-a5b7-296575ce7311\"]},{\"role\":37,\"ids\":[\"e530a875-3075-49dc-bd73-a92a0110f4c6\"]},{\"role\":16,\"ids\":[\"f65fef74-814f-4ed1-bc3d-940e723354dc\"]},{\"role\":15,\"ids\":[\"4ef240ae-347d-447e-9894-1e4b58d93358\"]},{\"role\":20,\"ids\":[\"e4617f00-25cb-4975-8c4b-a92a00b3cb03\"]},{\"role\":14,\"ids\":[\"e530a875-3075-49dc-bd73-a92a0110f4c6\"]},{\"role\":39,\"ids\":[\"e530a875 - 3075 - 49dc - bd73 - a92a0110f4c6\"]},{\"role\":5,\"ids\":[\"e530a875 - 3075 - 49dc - bd73 - a92a0110f4c6\"]},{\"role\":44,\"ids\":[\"4018d9f8 - 8790 - 44e4 - bbbb - a9440109b644\"]}],\"selfBuildAdditionalQuestions\":{\"isArchitectInvolved\":false,\"architectLevelOfInvolvementType\":0,\"isHomeOwnerResponsibleForConstructionInThePast\":false,\"homeOwnerResponsibleForConstructionInPastNoOfUnits\":null,\"homeOwnerResponsibleForConstructionInPastTenure\":0,\"numberOfStagePayments\":1,\"isSolePlaceOfResidence\":false},\"completedHousingAdditionalQuestions\":{\"reasonWhyStructuralWarrantyNotInPlace\":null},\"inAdminstrationAdditionalQuestions\":{\"isOriginallyRegisteredWithNewHomeWarranty\":null,\"originalRegisteredWarrantyProvider\":0,\"originalRegisteredWarrantyProviderOtherDetail\":null,\"previousDeveloper\":null,\"previousBuildingControlProvider\":null},\"generalInsuranceQuestions\":{\"sustainedAnyLossesOrHadAnyClaimsInTheLastThreeYears\":false,\"sustainedAnyLossesOrHadAnyClaimsInTheLastThreeYearsDetails\":null,\"everBeenRefusedPropertyInsuranceOrSpecialTermsImposed\":false,\"everBeenRefusedPropertyInsuranceOrSpecialTermsImposedDetails\":null,\"everBeenConvictedOrProsecutionPendingInvolvingDishonesty\":false,\"everBeenConvictedOrProsecutionPendingInvolvingDishonestyDetails\":null,\"everBeenProsecutedOrReceivedNotificationUnderHswa\":false,\"everBeenProsecutedOrReceivedNotificationUnderHswaDetails\":null,\"everBeenInvolvedWithLiquidatedOrBankruptBuilderOnConstructionCompany\":false,\"involvedWithLiquidatedOrBankruptBuilderOnConstructionCompanyName\":null,\"involvedWithLiquidatedOrBankruptBuilderOnConstructionCompanyDetails\":null},\"conversionQuestions\":{\"originalBuildYear\":null,\"buildingPreviousUses\":null,\"buildingPreviousUsesOther\":null,\"buildingListedStatus\":null,\"siteInConservationArea\":null,\"descriptionOfWorks\":null},\"hasApplicantConfirmedDetailsAreCorrect\":true,\"brokerAgreementId\":null,\"leadId\":null,\"lossOfGrossRentAnnualAmountNumberOfYears\":null,\"lossOfGrossRentAnnualAmount\":null,\"lossOfRentPayableAnnualAmountNumberOfYears\":null,\"lossOfRentPayableAnnualAmount\":null,\"lossOfRentRecievableAmmountNumberOfYears\":null,\"lossofRentReceivableAnnualAmount\":null,\"quoteVersionsParentQuoteId\":null,\"insuranceAgreementGroup\":2,\"doesTheSiteIncludeCurtainWalling\":false,\"atkinsBuildDuration\":0,\"productVersionDate\":\"2018-08-28\"}";
            var response = await client.PostAsync($"{envrironmentApiServer}/api/quote/application/save", new StringContent(body, Encoding.Default, "application/json"));
            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
        }
    } 
    public class Account
    {
        public Guid Id { get; set; }
    }
}
