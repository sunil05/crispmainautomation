﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.PageObjects;

namespace CrispAutomation.Permissions
{
    public class EndorsementsPermissionPage : Support.Pages
    {
        public IWebDriver wdriver;
        public EndorsementsPermissionPage(ISearchContext driver)
        {
            PageFactory.InitElements(driver, this);
            wdriver = (RemoteWebDriver)driver;
        }
        public bool NoPermissions { get; set; }
        public bool CreateEndorsementsPermission { get; set; }
    }
}
