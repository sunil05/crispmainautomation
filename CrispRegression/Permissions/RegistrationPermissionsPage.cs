﻿using OpenQA.Selenium;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.PageObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CrispAutomation.Permissions
{
    public class RegistrationPermissionsPage : Support.Pages
    {
        public IWebDriver wdriver;
        public RegistrationPermissionsPage(ISearchContext driver)
        {
            PageFactory.InitElements(driver, this);
            wdriver = (RemoteWebDriver)driver;
        }
        public bool NoPermissions { get; set; }
        public bool ManageRegistrationPermission { get; set; }
        public bool CreateRegistrationPermission { get; set; }
        public bool AmendRegistrationPermission { get; set; }
        public bool ViewRegistrationPermission { get; set; }
    }
}
