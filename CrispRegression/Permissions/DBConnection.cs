﻿using CrispAutomation.Features;
using CrispAutomation.Pages;
using Newtonsoft.Json;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace CrispAutomation.Permissions
{
    public class DBConnection : Support.Pages
    {
       public string userId { get; set; }
        public string ConnectDBGetPermissionUserId()
        {
            string datasource = ConfigurationManager.AppSettings["DataSource"]; ;
            string database = ConfigurationManager.AppSettings["Database"];
            string username = ConfigurationManager.AppSettings["DatabaseUsername"];
            string password = ConfigurationManager.AppSettings["DatabasePassword"];
            string connString = @"Data Source=" + datasource + ";Initial Catalog=" + database + ";Persist Security Info=True;User ID=" + username + ";Password=" + password;
            SqlConnection connection = new SqlConnection(connString);
            string autouser = ConfigurationManager.AppSettings["PermissionUsername"];
            var sqlQueryForSystemAccount = $"Select TOP(1) sap.SystemAccountId from [{database}].[dbo].[SystemAccountPersons] sap join [{database}].dbo.People P on P.Id = sap.PersonId join [{database}].dbo.SystemAccounts sa on sap.SystemAccountId = sa.Id where P.Email like '%{autouser}%'";
            //var sqlQuery = $"Select P.Id,sap.SystemAccountId FROM [CRISP-c].[dbo].[People] P JOIN [CRISP-c].[dbo].[SystemAccountPersons] sap on P.Id = sap.PersonId where P.Email = 'brian.hare@mdinsurance.co.uk' AND sap.Type = 0";
            try
            {
                connection.Open();
                SqlCommand com = new SqlCommand(sqlQueryForSystemAccount, connection);
                using (SqlDataReader read = com.ExecuteReader())
                {
                    while (read.Read())
                    {
                        userId = read["SystemAccountId"].ToString();
                        Statics.PermissionUserId = userId;
                    }
                }
                return userId;
            }
            finally
            {
                connection.Close();
            }
        }
        public string ConnectDBGetCrispUserId()
        {
            string datasource = ConfigurationManager.AppSettings["DataSource"]; ;
            string database = ConfigurationManager.AppSettings["Database"];
            string username = ConfigurationManager.AppSettings["DatabaseUsername"];
            string password = ConfigurationManager.AppSettings["DatabasePassword"];
            string connString = @"Data Source=" + datasource + ";Initial Catalog=" + database + ";Persist Security Info=True;User ID=" + username + ";Password=" + password;
            SqlConnection connection = new SqlConnection(connString);
            string crispuser = ConfigurationManager.AppSettings["CrispUsername"];
            var sqlQueryForSystemAccount = $"Select TOP(1) sap.SystemAccountId from [{database}].[dbo].[SystemAccountPersons] sap join [{database}].dbo.People P on P.Id = sap.PersonId join [{database}].dbo.SystemAccounts sa on sap.SystemAccountId = sa.Id where P.Email like '%{crispuser}%'";
            //var sqlQuery = $"Select P.Id,sap.SystemAccountId FROM [CRISP-c].[dbo].[People] P JOIN [CRISP-c].[dbo].[SystemAccountPersons] sap on P.Id = sap.PersonId where P.Email = 'brian.hare@mdinsurance.co.uk' AND sap.Type = 0";
            try
            {
                connection.Open();
                SqlCommand com = new SqlCommand(sqlQueryForSystemAccount, connection);
                using (SqlDataReader read = com.ExecuteReader())
                {
                    while (read.Read())
                    {
                        userId = read["SystemAccountId"].ToString();
                        Statics.CrispUserId = userId;
                    }
                }
                return userId;
            }
            finally
            {
                connection.Close();
            }
        }

        public void RemovePermissionsOnUser()
        {
            ConnectDBGetPermissionUserId();
            if (userId != null)
            {
                RemovePermissions().GetAwaiter().GetResult();
            }
            else
            {
                CreateUserWithOutPermissions().GetAwaiter().GetResult();
                ConnectDBGetPermissionUserId();
            }
        }
        public void ApplyPermissionsOnCrispUser()
        {
            ConnectDBGetCrispUserId();
            if (userId != null)
            {
                ApplyPermissions().GetAwaiter().GetResult();
            }           
        }
        public async Task CreateUserWithOutPermissions()
        {           
            var envrironmentApiServer = ConfigurationManager.AppSettings["ApiServerUrl"];
            var emailID = ConfigurationManager.AppSettings["PermissionUsername"];           
            string[] parts = emailID.Split(new[] { '@' });
            string partsUser = parts[0];
            string[] names = partsUser.Split(new[] { '.' });
            string firstName = names[0];
            string lastName = names[1];
            var client = HttpClientHelpers.CreateClient("scott.williams@mdinsurance.co.uk", "Mdis@1234;");
            string body = string.Format("{{\"permissions\":{{\"accounts\":{{\"importReceipts\":false,\"allocateMoney\":false,\"manageBordereau\":false}},\"internalUsers\":{{\"managePermissions\":false,\"create\":false,\"amend\":false,\"view\":false}},\"contacts\":{{\"setNonStandardPaymentTerms\":false,\"amendCompanyName\":false,\"manage\":false,\"create\":false,\"amend\":false,\"view\":false}},\"leads\":{{\"canDeleteSharedProfiles\":false,\"canShortListLeads\":false,\"canMatchAbiLeads\":false,\"create\":false,\"amend\":false,\"view\":false}},\"opportunity\":{{\"create\":false,\"amend\":false,\"view\":false}},\"quote\":{{\"accept\":false,\"viewExternalIncomplete\":false,\"reconstructionCostLimit\":0,\"canSetAsMajorProject\":false,\"products\":[],\"setSpecialTerms\":false,\"canChangeProductVersionDate\":false,\"canChangeinsuranceAgreementGroup\":false,\"canMarkAsReadyToQuote\":false,\"reject\":false,\"create\":false,\"amend\":false,\"view\":false}},\"order\":{{\"approveReferral\":false,\"issueCoa\":false,\"issueCoa2\":false,\"issueCoi\":false,\"importReceipts\":false,\"allocateMoney\":false,\"ammendRoles\":false,\"issueDevelopmentIc\":false,\"issuePlotIc\":false,\"issueFinalNotice\":false,\"cancel\":false,\"changePaymentTerms\":false,\"reinstate\":false,\"canAknowledgeQguOrderAmendment\":false,\"create\":false,\"amend\":false,\"view\":false}},\"manageCompanyLists\":{{\"create\":false,\"amend\":false,\"view\":false}},\"manageCompanyGroups\":{{\"create\":false,\"amend\":false,\"view\":false}},\"refurbAssessment\":{{\"create\":false,\"amend\":false,\"view\":false}},\"siteRiskAssessment\":{{\"create\":false,\"amend\":false,\"view\":false}},\"inspection\":{{\"create\":false,\"amend\":false,\"view\":false}},\"designReview\":{{\"create\":false,\"amend\":false,\"view\":false}},\"structuralReview\":{{\"create\":false,\"amend\":false,\"view\":false}},\"registration\":{{\"manage\":false,\"create\":false,\"amend\":false,\"view\":false}},\"claim\":{{\"create\":false,\"amend\":false,\"view\":false}},\"recovery\":{{\"create\":false,\"amend\":false,\"view\":false}},\"salesAppointments\":{{\"create\":false,\"amend\":false,\"view\":false}},\"technicalHub\":{{\"createContent\":false,\"approveContent\":false,\"publishContent\":false}},\"companyBlocking\":{{\"create\":false,\"remove\":false}},\"managerOverrides\":{{\"processes\":[],\"paymentValidation\":[]}},\"localAuthorities\":{{\"buildingControlAdmin\":false}},\"notes\":{{\"create\":false,\"amend\":false,\"view\":false}},\"endorsements\":{{\"create\":false}},\"bonds\":{{\"manage\":false,\"bondValueLimit\":25000000}},\"ratings\":{{\"confirm\":false}},\"extranet\":{{\"administrator\":false}},\"rescind\":{{\"rescindDevelopmentIc\":false,\"rescindPlotIc\":false,\"rescindCoverNote\":false,\"rescindCoa\":false,\"rescindCoi\":false,\"rescindInitialNotice\":false,\"rescindFinalNotice\":false,\"rescindBuildingControlTechnicalSignOff\":false,\"create\":false,\"amend\":false,\"view\":false}}}},\"salutation\":\"Ms\",\"firstname\":\"{0}\",\"surname\":\"{1}\",\"companyId\":\"60b050bb-73d2-4741-9c84-a7e3c26cd3d5\",\"managerId\":\"8af9f4bb-409b-4714-bd7e-a98c00f5146e\",\"salesBuddyIds\":[],\"twitterAddress\":null,\"faceBookAddress\":null,\"linkedInAddress\":null,\"googlePlusAddress\":null,\"email\":\"{2}\",\"phoneCountryCode\":0,\"phone\":null,\"mobileCountryCode\":0,\"mobile\":null,\"websites\":[],\"addresses\":[],\"primaryOffice\":\"23be868b-e50c-4485-bd89-bbf158613753\",\"offices\":[\"23be868b-e50c-4485-bd89-bbf158613753\"],\"isCompanyDirector\":false,\"isPurchasingDecisionMaker\":false,\"extension\":\"\",\"suffix\":null,\"isContractor\":false,\"fullTimeEquivalence\":1,\"department\":\"Information Technology\",\"team\":\"IT\",\"jobTitle\":\"IT User\",\"groups\":[654,1,2],\"remoteWorkerPrimaryOperatingLocation\":null}}", firstName,lastName,emailID);
            var response = await client.PostAsync($"{envrironmentApiServer}/api/InternalAccounts", new StringContent(body, Encoding.Default, "application/json"));
            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode, "Failed to Create a Internal User, Try From the UI or Swagger , then Check user has permissions to create Internal user or Insert data into DB or  ");
        }
        public async Task RemovePermissions()
        {
            var envrironmentApiServer = ConfigurationManager.AppSettings["ApiServerUrl"];
            var emailID = ConfigurationManager.AppSettings["PermissionUsername"];
            string[] parts = emailID.Split(new[] { '@' });
            string partsUser = parts[0];
            string[] names = partsUser.Split(new[] { '.' });
            string firstName = names[0];
            string lastName = names[1];
            var client = HttpClientHelpers.CreateClient("scott.williams@mdinsurance.co.uk", "Mdis@1234;");
            string body = string.Format("{{\"id\":\"{0}\",\"permissions\":{{\"accounts\":{{\"importReceipts\":false,\"allocateMoney\":false,\"manageBordereau\":false}},\"internalUsers\":{{\"managePermissions\":false,\"create\":false,\"amend\":false,\"view\":false}},\"contacts\":{{\"setNonStandardPaymentTerms\":false,\"amendCompanyName\":false,\"manage\":false,\"create\":false,\"amend\":false,\"view\":false}},\"leads\":{{\"canDeleteSharedProfiles\":false,\"canShortListLeads\":false,\"canMatchAbiLeads\":false,\"create\":false,\"amend\":false,\"view\":false}},\"opportunity\":{{\"create\":false,\"amend\":false,\"view\":false}},\"quote\":{{\"accept\":false,\"viewExternalIncomplete\":false,\"reconstructionCostLimit\":0,\"canSetAsMajorProject\":false,\"products\":[],\"setSpecialTerms\":false,\"canChangeProductVersionDate\":false,\"canChangeinsuranceAgreementGroup\":false,\"canMarkAsReadyToQuote\":false,\"reject\":false,\"create\":false,\"amend\":false,\"view\":false}},\"order\":{{\"approveReferral\":false,\"issueCoa\":false,\"issueCoa2\":false,\"issueCoi\":false,\"importReceipts\":false,\"allocateMoney\":false,\"ammendRoles\":false,\"issueDevelopmentIc\":false,\"issuePlotIc\":false,\"issueFinalNotice\":false,\"cancel\":false,\"changePaymentTerms\":false,\"reinstate\":false,\"canAknowledgeQguOrderAmendment\":false,\"create\":false,\"amend\":false,\"view\":false}},\"manageCompanyLists\":{{\"create\":false,\"amend\":false,\"view\":false}},\"manageCompanyGroups\":{{\"create\":false,\"amend\":false,\"view\":false}},\"refurbAssessment\":{{\"create\":false,\"amend\":false,\"view\":false}},\"siteRiskAssessment\":{{\"create\":false,\"amend\":false,\"view\":false}},\"inspection\":{{\"create\":false,\"amend\":false,\"view\":false}},\"designReview\":{{\"create\":false,\"amend\":false,\"view\":false}},\"structuralReview\":{{\"create\":false,\"amend\":false,\"view\":false}},\"registration\":{{\"manage\":false,\"create\":false,\"amend\":false,\"view\":false}},\"claim\":{{\"create\":false,\"amend\":false,\"view\":false}},\"recovery\":{{\"create\":false,\"amend\":false,\"view\":false}},\"salesAppointments\":{{\"create\":false,\"amend\":false,\"view\":false}},\"technicalHub\":{{\"createContent\":false,\"approveContent\":false,\"publishContent\":false}},\"companyBlocking\":{{\"create\":false,\"remove\":false}},\"managerOverrides\":{{\"processes\":[],\"paymentValidation\":[]}},\"localAuthorities\":{{\"buildingControlAdmin\":false}},\"notes\":{{\"create\":false,\"amend\":false,\"view\":false}},\"endorsements\":{{\"create\":false}},\"bonds\":{{\"manage\":false,\"bondValueLimit\":25000000}},\"ratings\":{{\"confirm\":false}},\"extranet\":{{\"administrator\":false}},\"rescind\":{{\"rescindDevelopmentIc\":false,\"rescindPlotIc\":false,\"rescindCoverNote\":false,\"rescindCoa\":false,\"rescindCoi\":false,\"rescindInitialNotice\":false,\"rescindFinalNotice\":false,\"rescindBuildingControlTechnicalSignOff\":false,\"create\":false,\"amend\":false,\"view\":false}}}},\"salutation\":\"Ms\",\"firstname\":\"{1}\",\"surname\":\"{2}\",\"companyId\":\"60b050bb-73d2-4741-9c84-a7e3c26cd3d5\",\"managerId\":\"8af9f4bb-409b-4714-bd7e-a98c00f5146e\",\"salesBuddyIds\":[],\"twitterAddress\":null,\"faceBookAddress\":null,\"linkedInAddress\":null,\"googlePlusAddress\":null,\"email\":\"{3}\",\"phoneCountryCode\":0,\"phone\":null,\"mobileCountryCode\":0,\"mobile\":null,\"websites\":[],\"addresses\":[],\"primaryOffice\":\"23be868b-e50c-4485-bd89-bbf158613753\",\"offices\":[\"23be868b-e50c-4485-bd89-bbf158613753\"],\"isCompanyDirector\":false,\"isPurchasingDecisionMaker\":false,\"extension\":\"\",\"suffix\":null,\"isContractor\":false,\"fullTimeEquivalence\":1,\"department\":\"Information Technology\",\"team\":\"IT\",\"jobTitle\":\"IT User\",\"groups\":[654,1,2],\"remoteWorkerPrimaryOperatingLocation\":null}}", userId, firstName, lastName, emailID);
            var response = await client.PostAsync($"{envrironmentApiServer}/api/InternalAccounts", new StringContent(body, Encoding.Default, "application/json"));
            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode, "Failed to Remove Permissions");
        }
        public async Task ApplyPermissions()
        {
            var envrironmentApiServer = ConfigurationManager.AppSettings["ApiServerUrl"];
            var emailID = ConfigurationManager.AppSettings["CrispUsername"];
            string[] parts = emailID.Split(new[] { '@' });
            string partsUser = parts[0];
            string[] names = partsUser.Split(new[] { '.' });
            string firstName = names[0];
            string lastName = names[1];
            var client = HttpClientHelpers.CreateClient("scott.williams@mdinsurance.co.uk", "Mdis@1234;");
            string body = string.Format("{{\"id\":\"{0}\",\"permissions\":{{\"accounts\":{{\"importReceipts\":true,\"allocateMoney\":true,\"manageBordereau\":true}},\"internalUsers\":{{\"managePermissions\":true,\"create\":true,\"amend\":true,\"view\":true}},\"contacts\":{{\"setNonStandardPaymentTerms\":true,\"amendCompanyName\":true,\"manage\":true,\"create\":true,\"amend\":true,\"view\":true}},\"leads\":{{\"canDeleteSharedProfiles\":true,\"canShortListLeads\":true,\"canMatchAbiLeads\":true,\"create\":true,\"amend\":true,\"view\":true}},\"opportunity\":{{\"create\":true,\"amend\":true,\"view\":true}},\"quote\":{{\"accept\":true,\"viewExternalIncomplete\":true,\"reconstructionCostLimit\":2500000,\"canSetAsMajorProject\":true,\"products\":[7,12,6,11,3,10,18,5,14,15,1,8,16,13,19,17,22,4,23,2,9],\"setSpecialTerms\":true,\"canChangeProductVersionDate\":true,\"canChangeinsuranceAgreementGroup\":true,\"canMarkAsReadyToQuote\":true,\"reject\":true,\"create\":true,\"amend\":true,\"view\":true}},\"order\":{{\"approveReferral\":true,\"issueCoa\":true,\"issueCoa2\":true,\"issueCoi\":true,\"importReceipts\":true,\"allocateMoney\":true,\"ammendRoles\":true,\"issueDevelopmentIc\":true,\"issuePlotIc\":true,\"issueFinalNotice\":true,\"cancel\":true,\"changePaymentTerms\":true,\"reinstate\":true,\"canAknowledgeQguOrderAmendment\":true,\"create\":true,\"amend\":true,\"view\":true}},\"manageCompanyLists\":{{\"create\":true,\"amend\":true,\"view\":true}},\"manageCompanyGroups\":{{\"create\":true,\"amend\":true,\"view\":true}},\"refurbAssessment\":{{\"create\":true,\"amend\":true,\"view\":true}},\"siteRiskAssessment\":{{\"create\":true,\"amend\":true,\"view\":true}},\"inspection\":{{\"create\":true,\"amend\":true,\"view\":true}},\"designReview\":{{\"create\":true,\"amend\":true,\"view\":true}},\"structuralReview\":{{\"create\":true,\"amend\":true,\"view\":true}},\"registration\":{{\"manage\":true,\"create\":true,\"amend\":true,\"view\":true}},\"claim\":{{\"create\":true,\"amend\":true,\"view\":true}},\"recovery\":{{\"create\":true,\"amend\":true,\"view\":true}},\"salesAppointments\":{{\"create\":true,\"amend\":true,\"view\":true}},\"technicalHub\":{{\"createContent\":true,\"approveContent\":true,\"publishContent\":true}},\"companyBlocking\":{{\"create\":true,\"remove\":true}},\"managerOverrides\":{{\"processes\":[9,8,13,23,3,5,18,24,11,25,4,1,2,27],\"paymentValidation\":[13,3,23,11,4]}},\"localAuthorities\":{{\"buildingControlAdmin\":true}},\"notes\":{{\"create\":true,\"amend\":true,\"view\":true}},\"endorsements\":{{\"create\":true}},\"bonds\":{{\"manage\":true,\"bondValueLimit\":250000000}},\"ratings\":{{\"confirm\":true}},\"extranet\":{{\"administrator\":true}},\"rescind\":{{\"rescindDevelopmentIc\":true,\"rescindPlotIc\":true,\"rescindCoverNote\":true,\"rescindCoa\":true,\"rescindCoi\":true,\"rescindInitialNotice\":true,\"rescindFinalNotice\":true,\"rescindBuildingControlTechnicalSignOff\":true,\"create\":true,\"amend\":true,\"view\":true}}}},\"salutation\":\"Ms\",\"firstname\":\"{1}\",\"surname\":\"{2}\",\"companyId\":\"60b050bb-73d2-4741-9c84-a7e3c26cd3d5\",\"managerId\":\"802f56f1-1b9a-43cc-9982-c3c4d8b49ae6\",\"salesBuddyIds\":[],\"twitterAddress\":null,\"faceBookAddress\":null,\"linkedInAddress\":null,\"googlePlusAddress\":null,\"email\":\"{3}\",\"phoneCountryCode\":0,\"phone\":null,\"mobileCountryCode\":0,\"mobile\":null,\"websites\":[],\"addresses\":[],\"primaryOffice\":\"b2481302-613c-e911-baa7-a08cfdf4bbef\",\"offices\":[\"b2481302-613c-e911-baa7-a08cfdf4bbef\"],\"isCompanyDirector\":false,\"isPurchasingDecisionMaker\":false,\"extension\":\"\",\"suffix\":null,\"isContractor\":false,\"fullTimeEquivalence\":1,\"department\":\"Information Technology\",\"team\":\"IT\",\"jobTitle\":\"IT User\",\"groups\":[654,1,2,200,600,651,652,653,650,700,400,3,900],\"remoteWorkerPrimaryOperatingLocation\":null}}", userId, firstName, lastName, emailID);
            var response = await client.PostAsync($"{envrironmentApiServer}/api/InternalAccounts", new StringContent(body, Encoding.Default, "application/json"));
            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode, "Failed to Apply Permissions");
        }
    }  

}
