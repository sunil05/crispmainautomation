﻿using OpenQA.Selenium;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.PageObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CrispAutomation.Permissions
{
    public class GroupPermissions : Support.Pages
    {
        public IWebDriver wdriver;
        public GroupPermissions(ISearchContext driver)
        {
            PageFactory.InitElements(driver, this);
            wdriver = (RemoteWebDriver)driver;
        }
        //Sales Team permissions 
        public bool SalesAccountManagerPermission { get; set; }
        public bool SalesDirectorPermission { get; set; }
        //Technical Referral Team Permissions
        public bool TechnicalReferralTeamPermission { get; set; }
        //QGU Team Permissions
        public bool QGUTeamPermission { get; set; }
        public bool QGUTeamCancelOrderPermission { get; set; }
        //Inpitting Team Permissions
        public bool InputtingPermission { get; set; }
        public bool InputtingTeamManageExternalQuotePermission { get; set; }
        //Rescind Process Permissions
        public bool RescindProcessPermission { get; set; }
        public bool RescindDevICPermission { get; set; }
        public bool RescindPlotICPermission { get; set; }
        public bool RescindCoverNotePermission { get; set; }
        public bool RescindCOAPermission { get; set; }
        public bool RescindCOIPermission { get; set; }
        public bool RescindIntialNoticePermission { get; set; }
        public bool RescindFinalNoticePermission { get; set; }
        public bool RescindBCTechSignOffPermission { get; set; }

        //CSU Team Permissions
        public bool CSUTeamPermission { get; set; }
        public bool CSUTeamKeyNationalPermission { get; set; }
        public bool CSUTeamBrokerPermission { get; set; }
        public bool CSUTeamHVSPermission { get; set; }
        public bool CSUTeamSelfBuildPermission { get; set; }
        public bool CSUTeamCompletedHousingPermission { get; set; }
        public bool CSUTeamVIPPermission { get; set; }
        //CreditControl Team Permissions
        public bool CreditControlTeamPermission { get; set; }
        //EmailNotificationTasks Team Permissions
        public bool EmailNotificationTasksPermission { get; set; }
        //Surveyors Team Permissions
        public bool ActiveSurveyorPermission { get; set; }
        public bool RefurbhishmentSurveyorPermission { get; set; }
        public bool DesignReviewSurveyorPermission { get; set; }
        public bool MajorProjectsManagerSurveyorPermission { get; set; }
        public bool DirectorSurveyorPermission { get; set; }
        //Registration Team Permissions
        public bool RegistrationTeamPermission { get; set; }
        public bool RegistrationTeamAllocatedTasksPermission { get; set; }

    }
}
