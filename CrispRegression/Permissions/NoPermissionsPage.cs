﻿using Newtonsoft.Json;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.PageObjects;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Linq.Expressions;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace CrispAutomation.Pages
{
    public class NoPermissionsPage : Support.Pages
    {
        public IWebDriver wdriver;
        public NoPermissionsPage(ISearchContext driver)
        {
            PageFactory.InitElements(driver, this);
            wdriver = (RemoteWebDriver)driver;
        }
        public bool NoPermissions { get; set; }

        //Remove All permissions applied on user //Permissions Checking on  userid (Auto User)        
        public async Task<bool> MainAsyncRemoveAllPermissionsToUser()
        {
            var accountId = "ab004dd3-ff74-43c7-b39c-a94b00ad0c92";
            var envrironmentApiServer = ConfigurationManager.AppSettings["ApiServerUrl"];
            var client = HttpClientHelpers.CreateClient("scott.williams@mdinsurance.co.uk", "Mdis@1234;");
            string body = string.Format("{{\"id\":\"{0}\",\"permissions\":{{\"accounts\":{{\"importReceipts\":false,\"allocateMoney\":false,\"manageBordereau\":false}},\"internalUsers\":{{\"managePermissions\":false,\"create\":false,\"amend\":false,\"view\":false}},\"contacts\":{{\"setNonStandardPaymentTerms\":false,\"amendCompanyName\":false,\"manage\":false,\"create\":false,\"amend\":false,\"view\":false}},\"leads\":{{\"create\":false,\"amend\":false,\"view\":false}},\"opportunity\":{{\"create\":false,\"amend\":false,\"view\":false}},\"quote\":{{\"accept\":false,\"viewExternalIncomplete\":false,\"reconstructionCostLimit\":0,\"products\":[],\"setSpecialTerms\":false,\"create\":false,\"amend\":false,\"view\":false}},\"order\":{{\"approveReferral\":false,\"issueCoa\":false,\"issueCoa2\":false,\"issueCoi\":false,\"importReceipts\":false,\"allocateMoney\":false,\"ammendRoles\":false,\"issueDevelopmentIc\":false,\"issuePlotIc\":false,\"issueFinalNotice\":false,\"cancel\":false,\"changePaymentTerms\":false,\"reinstate\":false,\"create\":false,\"amend\":false,\"view\":false}},\"manageCompanyLists\":{{\"create\":false,\"amend\":false,\"view\":false}},\"manageCompanyGroups\":{{\"create\":false,\"amend\":false,\"view\":false}},\"refurbAssessment\":{{\"create\":false,\"amend\":false,\"view\":false}},\"siteRiskAssessment\":{{\"create\":false,\"amend\":false,\"view\":false}},\"inspection\":{{\"create\":false,\"amend\":false,\"view\":false}},\"designReview\":{{\"create\":false,\"amend\":false,\"view\":false}},\"engineerReview\":{{\"create\":false,\"amend\":false,\"view\":false}},\"structuralReview\":{{\"create\":false,\"amend\":false,\"view\":false}},\"registration\":{{\"manage\":false,\"create\":false,\"amend\":false,\"view\":false}},\"claim\":{{\"create\":false,\"amend\":false,\"view\":false}},\"recovery\":{{\"create\":false,\"amend\":false,\"view\":false}},\"salesAppointments\":{{\"create\":false,\"amend\":false,\"view\":false}},\"technicalHub\":{{\"createContent\":false,\"approveContent\":false,\"publishContent\":false}},\"companyBlocking\":{{\"create\":false,\"remove\":false}},\"managerOverrides\":{{\"processes\":[],\"paymentValidation\":[]}},\"localAuthorities\":{{\"buildingControlAdmin\":false}},\"notes\":{{\"create\":false,\"amend\":false,\"view\":false}},\"endorsements\":{{\"create\":false}},\"bonds\":{{\"manage\":false,\"bondValueLimit\":1000000000}},\"ratings\":{{\"confirm\":false}},\"extranet\":{{\"administrator\":false}},\"rescind\":{{\"rescindDevelopmentIc\":false,\"rescindPlotIc\":false,\"rescindCoverNote\":false,\"rescindCoa\":false,\"rescindCoi\":false,\"rescindInitialNotice\":false,\"rescindFinalNotice\":false,\"rescindBuildingControlTechnicalSignOff\":false,\"create\":false,\"amend\":false,\"view\":false}}}},\"salutation\":\"Mr\",\"firstname\":\"Auto\",\"surname\":\"User\",\"companyId\":\"60b050bb-73d2-4741-9c84-a7e3c26cd3d5\",\"managerId\":\"84add8b2-d0ae-44ff-af25-b26cbc9e72f7\",\"salesBuddyIds\":[],\"twitterAddress\":null,\"faceBookAddress\":null,\"linkedInAddress\":null,\"googlePlusAddress\":null,\"email\":\"auto.user@mdinsurance.co.uk\",\"phoneCountryCode\":214,\"phone\":\"01483793721\",\"mobileCountryCode\":0,\"mobile\":null,\"websites\":[],\"addresses\":[],\"primaryOffice\":\"c995818e-906a-4679-9415-cbc6cac4873a\",\"offices\":[\"c995818e-906a-4679-9415-cbc6cac4873a\"],\"isCompanyDirector\":false,\"isPurchasingDecisionMaker\":false,\"extension\":\"\",\"suffix\":null,\"isContractor\":false,\"fullTimeEquivalence\":1,\"department\":\"Information Technology\",\"team\":\"Test Team\",\"jobTitle\":\"IT User\",\"groups\":[1,2],\"remoteWorkerPrimaryOperatingLocation\":null}}", accountId.ToString());
            var response = await client.PostAsync($"{envrironmentApiServer}/api/InternalAccounts", new StringContent(body, Encoding.Default, "application/json"));
            return response.StatusCode == HttpStatusCode.OK;
        }
        //Remove Permissions and Verify Creating, Amending Contacts
        public void RemovePermissionsToUser()
        {
            NoPermissions = MainAsyncRemoveAllPermissionsToUser().GetAwaiter().GetResult();            
        }
        public void VerifyCreateOperationWhenPermissionsRemoved()
        {
            if (NoPermissions == true)
            {
                MainAsyncVerifyCreatingPersonWhenUserHasNoPermission().GetAwaiter().GetResult();
                MainAsyncVerifyCreatingCompanyWhenUserHasNoPermission().GetAwaiter().GetResult();
                MainAsyncVerifyCreatingQuoteWhenUserHasNoPermission().GetAwaiter().GetResult();
                MainAsyncVerifyCreatingLeadWhenUserHasNoPermission().GetAwaiter().GetResult();
                MainAsyncVerifyCreatingInternalUserWhenUserHasNoPermission().GetAwaiter().GetResult();
            }
        }
        public void VerifyAmendOperationWhenPermissionsRemoved()
        {
            MainAsyncVerifyAmendPersonDetailsWhenUserHasNoPermission().GetAwaiter().GetResult();
            MainAsyncVerifyAmendCompanyDetailsWhenUserHasNoPermission().GetAwaiter().GetResult();          
            MainAsyncVerifyAmendingQuoteWhenUserHasNoPermission().GetAwaiter().GetResult();
            MainAsyncVerifyAmendCompanyNameDetailsWhenUserHasNoPermission().GetAwaiter().GetResult();
            MainAsyncVerifyAmendingLeadWhenUserHasNoPermission().GetAwaiter().GetResult();
            MainAsyncVerifyAmendingInternalUserWhenUserHasNoPermission().GetAwaiter().GetResult();
        }
        public void VerifySearchOperationWhenPermissionsRemoved()
        {
            MainAsyncVerifySearchContactsWhenUserHasNoPermission().GetAwaiter().GetResult();
            MainAsyncVerifySearchSitesWhenUserHasNoPermission().GetAwaiter().GetResult();
            MainAsyncVerifySearchPlotsWhenUserHasNoPermission().GetAwaiter().GetResult();
            MainAsyncVerifySearchLeadWhenUserHasNoPermission().GetAwaiter().GetResult();
        }
        //Contact operation
        public async Task MainAsyncVerifyCreatingPersonWhenUserHasNoPermission()
        {
            var uniqueGuID = System.Guid.NewGuid();
            var envrironmentApiServer = ConfigurationManager.AppSettings["ApiServerUrl"];
            var client = HttpClientHelpers.CreateClient("auto.user@mdinsurance.co.uk", "Mdis@1234;");
            string body = "{\"companyId\":null,\"title\":\"Mr\",\"firstname\":\"Sunilkumar\",\"surname\":\"Sunkishal\",\"twitterAddress\":\"\",\"faceBookAddress\":\"\",\"linkedInAddress\":\"\",\"googlePlusAddress\":\"\",\"email\":\"\",\"phoneCountryCode\":214,\"phone\":\"01483493721\",\"mobileCountryCode\":214,\"mobile\":\"07834233944\",\"websites\":[{\"address\":\"www.test.com\",\"isDiscontinued\":false}],\"isCompanyDirector\":false,\"isPurchasingDecisionMaker\":false,\"extension\":null,\"suffix\":\"\",\"jobTitle\":\"\",\"addresses\":[{\"isDefault\":true,\"address1\":\"M D Insurance Services Ltd\",\"address2\":\"2 Shore Lines Building\",\"address3\":\"Shore Road\",\"town\":\"Merseyside\",\"postcode\":\"CH41 1AU\",\"countryCodeType\":214,\"id\":\"e609651a-4363-4a12-a645-2017bf0c46eb\",\"isDiscontinued\":false}],\"hasLabcExtranetAccount\":false,\"hasPgExtranetAccount\":false,\"salesAndMarketingOptions\":{}}";
            var response = await client.PostAsync($"{envrironmentApiServer}/api/person/save", new StringContent(body, Encoding.Default, "application/json"));
            Assert.AreEqual(HttpStatusCode.Unauthorized, response.StatusCode);
        }
        public async Task MainAsyncVerifyCreatingCompanyWhenUserHasNoPermission()
        {
            var uniqueGuID = System.Guid.NewGuid();
            var envrironmentApiServer = ConfigurationManager.AppSettings["ApiServerUrl"];
            var client = HttpClientHelpers.CreateClient("auto.user@mdinsurance.co.uk", "Mdis@1234;");
            string body = string.Format("{{\"name\":\"MdisTest\",\"offices\":[{{\"name\":\"Test Office\",\"email\":\"test@gmail.com\",\"phoneNumber\":{{\"countryCodeType\":214,\"contactNumber\":\"01483493721\"}},\"isRegisteredAddress\":true,\"isDefault\":true,\"address1\":\"M D Insurance Services Ltd\",\"address2\":\"2 Shore Lines Building\",\"address3\":\"Shore Road\",\"town\":\"Merseyside\",\"postcode\":\"CH41 1AU\",\"countryCodeType\":214,\"salesAndMarketingOptions\":{{}},\"id\":\"{0}\",\"isDiscontinued\":false}}],\"yearEstablished\":2018,\"registrationNumber\":\"12345678\",\"legalFormType\":1,\"isVip\":true,\"websites\":[],\"hasEnhancedSocialHousingInsolvencyEndorsement\":true,\"employees\":[{{\"title\":\"Mr\",\"firstname\":\"Sunil\",\"surname\":\"Kumar\",\"email\":\"test@gmail.com\",\"phoneCountryCode\":214,\"mobileCountryCode\":214,\"personOffices\":[{{\"officeId\":\"{0}\",\"isPrimary\":true}}],\"isCompanyDirector\":true,\"isPurchasingDecisionMaker\":true,\"suffix\":\"s\",\"salesAndMarketingOptions\":{{}},\"isDiscontinued\":false}}],\"insuranceAgreementGroup\":2}}", uniqueGuID.ToString());
            var response = await client.PostAsync($"{envrironmentApiServer}/api/company/save", new StringContent(body, Encoding.Default, "application/json"));
            Assert.AreEqual(HttpStatusCode.Unauthorized, response.StatusCode);
        }
        public async Task MainAsyncVerifyAmendPersonDetailsWhenUserHasNoPermission()
        {
            var uniqueGuID = System.Guid.NewGuid();
            var envrironmentApiServer = ConfigurationManager.AppSettings["ApiServerUrl"];
            var client = HttpClientHelpers.CreateClient("auto.user@mdinsurance.co.uk", "Mdis@1234;");
            string body = "{ \"title\":\"Mr\",\"firstname\":\"Sunil\",\"surname\":\"Sunkishala\",\"companyId\":null,\"twitterAddress\":\"\",\"faceBookAddress\":\"\",\"linkedInAddress\":\"\",\"googlePlusAddress\":\"\",\"email\":\"crisp.dev.c@ext.crisp.co.uk\",\"phoneCountryCode\":214,\"phone\":\"01483493721\",\"mobileCountryCode\":214,\"mobile\":\"07834233944\",\"websites\":[{\"address\":\"www.test.com\",\"id\":\"c0c2c9e3-ae48-4bd0-a31a-a94a00f9e025\",\"dateCreated\":\"2018-08-28T15:09:46.193Z\",\"createdById\":\"802f56f1-1b9a-43cc-9982-c3c4d8b49ae6\",\"dateUpdated\":\"2018-08-28T15:09:46.193Z\",\"updatedById\":\"802f56f1-1b9a-43cc-9982-c3c4d8b49ae6\",\"isDiscontinued\":false},{\"address\":\"www.test1.com\",\"isDiscontinued\":false}],\"addresses\":[{\"isDefault\":true,\"isInMiningArea\":false,\"miningArea\":null,\"radonExposure\":null,\"radonExposureScore\":0,\"weatherExposure\":4,\"isGeographyInformationAvailable\":false,\"isLandfill\":false,\"distanceFromCoast\":0,\"address1\":\"M D Insurance Services Ltd\",\"address2\":\"2 Shore Lines Building\",\"address3\":\"Shore Road\",\"town\":\"Merseyside\",\"postcode\":\"CH41 1AU\",\"fullAddress\":\"M D Insurance Services Ltd, 2 Shore Lines Building, Shore Road, Merseyside, CH41 1AU\",\"countryCodeType\":214,\"salesAndMarketingOptions\":{\"labcOptInForCalls\":false,\"labcOptInForEmails\":false,\"pgOptInForCalls\":false,\"pgOptInForEmails\":false,\"notedOnTelephonePreferenceServiceList\":false},\"kilometersFromCoast\":0,\"radonPotential\":\"0%\",\"id\":\"df89d2b2-2835-4bab-8ae5-a94a00f9e025\",\"dateCreated\":\"2018-08-28T15:09:46.193Z\",\"createdById\":\"802f56f1-1b9a-43cc-9982-c3c4d8b49ae6\",\"dateUpdated\":\"2018-08-28T15:09:46.193Z\",\"updatedById\":\"802f56f1-1b9a-43cc-9982-c3c4d8b49ae6\",\"isDiscontinued\":false}],\"personOffices\":[],\"isCompanyDirector\":false,\"isPurchasingDecisionMaker\":false,\"extension\":null,\"suffix\":\"\",\"isContractor\":false,\"fullTimeEquivalence\":0,\"department\":null,\"team\":null,\"jobTitle\":\"\",\"managerId\":null,\"pictureName\":null,\"signatureId\":null,\"signatureName\":null,\"remoteWorkerPrimaryOperatingLocation\":null,\"remoteWorkerGooglePlaceId\":null,\"hasLabcExtranetAccount\":false,\"hasPgExtranetAccount\":false,\"isBlocked\":false,\"salesAndMarketingOptions\":{\"labcOptInForCalls\":false,\"labcOptInForEmails\":false,\"pgOptInForCalls\":false,\"pgOptInForEmails\":false,\"notedOnTelephonePreferenceServiceList\":false},\"documents\":[],\"documentFolders\":[],\"documentBatches\":[],\"id\":\"1035bc4d-690f-4e8b-ab9b-a94a00f9e025\",\"dateCreated\":\"2018-08-28T15:09:46.193Z\",\"createdById\":\"802f56f1-1b9a-43cc-9982-c3c4d8b49ae6\",\"dateUpdated\":\"2018-08-28T15:09:46.193Z\",\"updatedById\":\"802f56f1-1b9a-43cc-9982-c3c4d8b49ae6\",\"isDiscontinued\":false}";
            var response = await client.PostAsync($"{envrironmentApiServer}/api/person/save", new StringContent(body, Encoding.Default, "application/json"));
            Assert.AreEqual(HttpStatusCode.Unauthorized, response.StatusCode);
        }
        public async Task MainAsyncVerifyAmendCompanyDetailsWhenUserHasNoPermission()
        {
            var uniqueGuID = System.Guid.NewGuid();
            var envrironmentApiServer = ConfigurationManager.AppSettings["ApiServerUrl"];
            var client = HttpClientHelpers.CreateClient("auto.user@mdinsurance.co.uk", "Mdis@1234;");
            string body = string.Format("{{\"name\":\"MdisTest\",\"companyId\":\"f411fdc3-770f-4e13-9395-a94b00c165fd\",\"offices\":[{{\"name\":\"Test Office\",\"email\":\"test@gmail.com\",\"phoneNumber\":{{\"countryCodeType\":214,\"contactNumber\":\"01483493721\",\"isPrimary\":false,\"id\":\"{0}\",\"dateCreated\":\"2018-08-29T11:44:08.566Z\",\"createdById\":\"4871face-876e-47fe-95bb-a94b00ad0c92\",\"dateUpdated\":\"2018-08-29T11:44:08.566Z\",\"updatedById\":\"4871face-876e-47fe-95bb-a94b00ad0c92\",\"isDiscontinued\":false}},\"isRegisteredAddress\":true,\"pgSalesAccountManagerId\":null,\"labcSalesAccountManagerId\":null,\"csuAccountManagerId\":null,\"personOffices\":[],\"salesAndMarketingOptions\":{{\"labcOptInForCalls\":false,\"labcOptInForEmails\":false,\"pgOptInForCalls\":false,\"pgOptInForEmails\":false,\"notedOnTelephonePreferenceServiceList\":false}},\"id\":\"09fc47b8-d0dd-4803-8947-b7ff9c68920a\",\"isDefault\":true,\"isDiscontinued\":false,\"country\":\"\",\"address1\":\"M D Insurance Services Ltd\",\"address2\":\"2 Shore Lines Building\",\"address3\":\"Shore Road\",\"town\":\"Merseyside\",\"postcode\":\"CH41 1AU\",\"countryCodeType\":214}}],\"yearEstablished\":2018,\"registrationNumber\":\"12345678\",\"legalFormType\":1,\"clientSegmentationType\":0,\"isVip\":true,\"isKeyNational\":false,\"twitterAddress\":null,\"faceBookAddress\":null,\"linkedInAddress\":null,\"googlePlusAddress\":null,\"websites\":[],\"isCompaniesHouseRecord\":false,\"hasEnhancedSocialHousingInsolvencyEndorsement\":true,\"depositPaymentPercentage\":null,\"maxNumberOfMonthlyInstalments\":null,\"insuranceAgreementGroup\":2}}", uniqueGuID.ToString());
            var response = await client.PostAsync($"{envrironmentApiServer}/api/company/save", new StringContent(body, Encoding.Default, "application/json"));
            Assert.AreEqual(HttpStatusCode.Unauthorized, response.StatusCode);
        }
        public async Task MainAsyncVerifyAmendCompanyNameDetailsWhenUserHasNoPermission()
        {
            var uniqueGuID = System.Guid.NewGuid();
            var envrironmentApiServer = ConfigurationManager.AppSettings["ApiServerUrl"];
            var client = HttpClientHelpers.CreateClient("auto.user@mdinsurance.co.uk", "Mdis@1234;");
            string body = string.Format("{{\"name\":\"EditComanyNameTest\",\"companyId\":\"f411fdc3-770f-4e13-9395-a94b00c165fd\",\"offices\":[{{\"name\":\"Test Office\",\"email\":\"test@gmail.com\",\"phoneNumber\":{{\"countryCodeType\":214,\"contactNumber\":\"01483493721\",\"isPrimary\":false,\"id\":\"{0}\",\"dateCreated\":\"2018-08-29T11:44:08.566Z\",\"createdById\":\"4871face-876e-47fe-95bb-a94b00ad0c92\",\"dateUpdated\":\"2018-08-29T11:44:08.566Z\",\"updatedById\":\"4871face-876e-47fe-95bb-a94b00ad0c92\",\"isDiscontinued\":false}},\"isRegisteredAddress\":true,\"pgSalesAccountManagerId\":null,\"labcSalesAccountManagerId\":null,\"csuAccountManagerId\":null,\"personOffices\":[],\"salesAndMarketingOptions\":{{\"labcOptInForCalls\":false,\"labcOptInForEmails\":false,\"pgOptInForCalls\":false,\"pgOptInForEmails\":false,\"notedOnTelephonePreferenceServiceList\":false}},\"id\":\"09fc47b8-d0dd-4803-8947-b7ff9c68920a\",\"isDefault\":true,\"isDiscontinued\":false,\"country\":\"\",\"address1\":\"M D Insurance Services Ltd\",\"address2\":\"2 Shore Lines Building\",\"address3\":\"Shore Road\",\"town\":\"Merseyside\",\"postcode\":\"CH41 1AU\",\"countryCodeType\":214}}],\"yearEstablished\":2018,\"registrationNumber\":\"12345678\",\"legalFormType\":1,\"clientSegmentationType\":0,\"isVip\":true,\"isKeyNational\":false,\"twitterAddress\":null,\"faceBookAddress\":null,\"linkedInAddress\":null,\"googlePlusAddress\":null,\"websites\":[],\"isCompaniesHouseRecord\":false,\"hasEnhancedSocialHousingInsolvencyEndorsement\":true,\"depositPaymentPercentage\":null,\"maxNumberOfMonthlyInstalments\":null,\"insuranceAgreementGroup\":2}}", uniqueGuID.ToString());
            var response = await client.PostAsync($"{envrironmentApiServer}/api/company/save", new StringContent(body, Encoding.Default, "application/json"));
            Assert.AreEqual(HttpStatusCode.Unauthorized, response.StatusCode);
        }
        
        //Quote Operation

        public async Task MainAsyncVerifyCreatingQuoteWhenUserHasNoPermission()
        {
            var uniqueGuID = System.Guid.NewGuid();
            var envrironmentApiServer = ConfigurationManager.AppSettings["ApiServerUrl"];
            var client = HttpClientHelpers.CreateClient("auto.user@mdinsurance.co.uk", "Mdis@1234;");
            string body = "{\"products\":[{\"productId\":\"9deb8006-8fc5-5883-9c67-68c720cb8b47\",\"length\":10,\"productItems\":[\"4ee6bbab-863d-5962-963f-3d78ea95f6c7\",\"434129ab-56e0-5bcd-b2a8-9928db020d2a\",\"d32a5034-c6fd-5137-8c62-050de9bcab2b\",\"fdd2b69b-b4da-50e2-b739-104842cd4bed\",\"281000cb-0b25-535e-adc2-5b660c6cc3c1\"]},{\"productId\":\"ed59c767-9507-5159-ad26-379d3e40a5fa\",\"length\":null,\"productItems\":[\"5e936810-6f36-5f3e-a998-4748b7897f95\",\"e878cc32-54d3-5b55-aa0e-e8cac563d790\",\"5e977bf2-064d-5df0-b1d3-04a4cffb1183\"]}],\"brand\":1,\"address\":{\"id\":\"38c19c75-0f88-4d68-a585-7d900db43181\",\"address1\":\"M D Insurance Services Ltd\",\"address2\":\"2 Shore Lines Building\",\"address3\":\"Shore Road\",\"town\":\"Merseyside\",\"postcode\":\"CH41 1AU\",\"countryCodeType\":214},\"documents\":[{\"fileSize\":2973,\"fileId\":\"3204d2ee-aacc-4c6f-84d4-a94b008e9817\",\"fileName\":\"Commercial.xlsx\",\"dateReceived\":\"2018-08-29T08:39:02.611Z\"}],\"constructionStartDate\":\"2018-08-01T00:00:00.000Z\",\"constructionEndDate\":\"2018-08-29T00:00:00.000Z\",\"maxStoreysAboveGround\":1,\"maxStoreysBelowGround\":1,\"hasInnovativeMethods\":false,\"hasUnitsOrStructureAttached\":false,\"isSiteInAdministration\":false,\"quoteRecipient\":\"e530a875-3075-49dc-bd73-a92a0110f4c6\",\"additionalRoles\":[{\"role\":5,\"ids\":[\"e530a875-3075-49dc-bd73-a92a0110f4c6\"]},{\"role\":39,\"ids\":[\"e530a875-3075-49dc-bd73-a92a0110f4c6\"]},{\"role\":14,\"ids\":[\"e530a875-3075-49dc-bd73-a92a0110f4c6\"]},{\"role\":37,\"ids\":[\"e530a875-3075-49dc-bd73-a92a0110f4c6\"]},{\"role\":15,\"ids\":[\"4ef240ae-347d-447e-9894-1e4b58d93358\"]},{\"role\":32,\"ids\":[\"e2ac35ab-e72d-451e-a5b7-296575ce7311\"]},{\"role\":20,\"ids\":[\"e4617f00-25cb-4975-8c4b-a92a00b3cb03\"]},{\"role\":7,\"ids\":[\"e4617f00-25cb-4975-8c4b-a92a00b3cb03\"]},{\"role\":19,\"ids\":[\"7446a7ec-d415-4d89-92cf-0086bd07c565\"]}],\"selfBuildAdditionalQuestions\":{\"isArchitectInvolved\":false,\"isHomeOwnerResponsibleForConstructionInThePast\":false,\"numberOfStagePayments\":1,\"isSolePlaceOfResidence\":false},\"completedHousingAdditionalQuestions\":{},\"inAdminstrationAdditionalQuestions\":{},\"generalInsuranceQuestions\":{\"sustainedAnyLossesOrHadAnyClaimsInTheLastThreeYears\":false,\"everBeenRefusedPropertyInsuranceOrSpecialTermsImposed\":false,\"everBeenConvictedOrProsecutionPendingInvolvingDishonesty\":false,\"everBeenProsecutedOrReceivedNotificationUnderHswa\":false,\"everBeenInvolvedWithLiquidatedOrBankruptBuilderOnConstructionCompany\":false},\"conversionQuestions\":{},\"hasApplicantConfirmedDetailsAreCorrect\":true,\"plotScheduleFileId\":\"cdf409fa-08eb-4708-9926-a94b008edcca\",\"brokerAgreementId\":null,\"lossOfGrossRentAnnualAmountNumberOfYears\":null,\"lossOfGrossRentAnnualAmount\":null,\"lossOfRentPayableAnnualAmountNumberOfYears\":null,\"lossOfRentPayableAnnualAmount\":null,\"lossOfRentRecievableAmmountNumberOfYears\":null,\"lossofRentReceivableAnnualAmount\":null,\"quoteVersionsParentQuoteId\":null,\"insuranceAgreementGroup\":2,\"productVersionDate\":\"2018-08-29\"}";
            var response = await client.PostAsync($"{envrironmentApiServer}/api/quote/application/submit", new StringContent(body, Encoding.Default, "application/json"));
            Assert.AreEqual(HttpStatusCode.Unauthorized, response.StatusCode);
        }
        public async Task MainAsyncVerifyAmendingQuoteWhenUserHasNoPermission()
        {
            var uniqueGuID = System.Guid.NewGuid();
            var envrironmentApiServer = ConfigurationManager.AppSettings["ApiServerUrl"];
            var client = HttpClientHelpers.CreateClient("auto.user@mdinsurance.co.uk", "Mdis@1234;");
            string body = "{\"products\":[{\"productId\":\"9deb8006-8fc5-5883-9c67-68c720cb8b47\",\"contractCost\":0,\"length\":10,\"isWorkContractUnderSeal\":false,\"productItems\":[\"4ee6bbab-863d-5962-963f-3d78ea95f6c7\",\"434129ab-56e0-5bcd-b2a8-9928db020d2a\",\"d32a5034-c6fd-5137-8c62-050de9bcab2b\",\"fdd2b69b-b4da-50e2-b739-104842cd4bed\",\"281000cb-0b25-535e-adc2-5b660c6cc3c1\"]},{\"productId\":\"ed59c767-9507 -5159 - ad26 - 379d3e40a5fa\",\"contractCost\":0,\"length\":null,\"isWorkContractUnderSeal\":false,\"productItems\":[\"5e936810-6f36-5f3e-a998-4748b7897f95\",\"e878cc32-54d3-5b55-aa0e-e8cac563d790\",\"5e977bf2-064d-5df0-b1d3-04a4cffb1183\"]}],\"quoteId\":\"35c8c18c-008c-41dc-9488-a94b0092ef66\",\"brand\":1,\"address\":{\"id\":\"d4172961-eec6-4b81-bc1b-a94b0092efb1\",\"isDefault\":false,\"isDiscontinued\":false,\"address1\":\"M D Insurance Services Ltd\",\"address2\":\"2 Shore Lines Building\",\"address3\":\"Shore Road\",\"town\":\"Merseyside\",\"postcode\":\"CH41 1AU\",\"countryCodeType\":214},\"documents\":[{\"fileSize\":2973,\"fileId\":\"3204d2ee-aacc-4c6f-84d4-a94b008e9817\",\"fileName\":\"Commercial.xlsx\",\"dateReceived\":\"2018-08-29T00: 00:00.000Z\"}],\"constructionStartDate\":\"2018-08-02T00: 00:00.000Z\",\"constructionEndDate\":\"2018-08-29T00: 00:00.000Z\",\"maxStoreysAboveGround\":1,\"maxStoreysBelowGround\":1,\"hasInnovativeMethods\":false,\"innovativeMethodsUsed\":null,\"hasUnitsOrStructureAttached\":false,\"isSiteInAdministration\":false,\"numberOfResidentialUnits\":null,\"totalReconCostOfResidentialUnits\":null,\"numberOfCommercialUnits\":null,\"totalReconCostOfCommercialUnits\":null,\"summaryOfWorks\":null,\"isWarrantyAddedLater\":null,\"quoteRecipient\":\"e530a875-3075-49dc-bd73-a92a0110f4c6\",\"additionalRoles\":[{\"role\":19,\"ids\":[\"7446a7ec-d415-4d89-92cf-0086bd07c565\"]},{\"role\":7,\"ids\":[\"e4617f00-25cb-4975-8c4b-a92a00b3cb03\"]},{\"role\":32,\"ids\":[\"e2ac35ab-e72d-451e-a5b7-296575ce7311\"]},{\"role\":37,\"ids\":[\"e530a875-3075-49dc-bd73-a92a0110f4c6\"]},{\"role\":16,\"ids\":[\"f65fef74-814f-4ed1-bc3d-940e723354dc\"]},{\"role\":15,\"ids\":[\"4ef240ae-347d-447e-9894-1e4b58d93358\"]},{\"role\":20,\"ids\":[\"e4617f00-25cb-4975-8c4b-a92a00b3cb03\"]},{\"role\":14,\"ids\":[\"e530a875-3075-49dc-bd73-a92a0110f4c6\"]},{\"role\":39,\"ids\":[\"e530a875 - 3075 - 49dc - bd73 - a92a0110f4c6\"]},{\"role\":5,\"ids\":[\"e530a875 - 3075 - 49dc - bd73 - a92a0110f4c6\"]},{\"role\":44,\"ids\":[\"4018d9f8 - 8790 - 44e4 - bbbb - a9440109b644\"]}],\"selfBuildAdditionalQuestions\":{\"isArchitectInvolved\":false,\"architectLevelOfInvolvementType\":0,\"isHomeOwnerResponsibleForConstructionInThePast\":false,\"homeOwnerResponsibleForConstructionInPastNoOfUnits\":null,\"homeOwnerResponsibleForConstructionInPastTenure\":0,\"numberOfStagePayments\":1,\"isSolePlaceOfResidence\":false},\"completedHousingAdditionalQuestions\":{\"reasonWhyStructuralWarrantyNotInPlace\":null},\"inAdminstrationAdditionalQuestions\":{\"isOriginallyRegisteredWithNewHomeWarranty\":null,\"originalRegisteredWarrantyProvider\":0,\"originalRegisteredWarrantyProviderOtherDetail\":null,\"previousDeveloper\":null,\"previousBuildingControlProvider\":null},\"generalInsuranceQuestions\":{\"sustainedAnyLossesOrHadAnyClaimsInTheLastThreeYears\":false,\"sustainedAnyLossesOrHadAnyClaimsInTheLastThreeYearsDetails\":null,\"everBeenRefusedPropertyInsuranceOrSpecialTermsImposed\":false,\"everBeenRefusedPropertyInsuranceOrSpecialTermsImposedDetails\":null,\"everBeenConvictedOrProsecutionPendingInvolvingDishonesty\":false,\"everBeenConvictedOrProsecutionPendingInvolvingDishonestyDetails\":null,\"everBeenProsecutedOrReceivedNotificationUnderHswa\":false,\"everBeenProsecutedOrReceivedNotificationUnderHswaDetails\":null,\"everBeenInvolvedWithLiquidatedOrBankruptBuilderOnConstructionCompany\":false,\"involvedWithLiquidatedOrBankruptBuilderOnConstructionCompanyName\":null,\"involvedWithLiquidatedOrBankruptBuilderOnConstructionCompanyDetails\":null},\"conversionQuestions\":{\"originalBuildYear\":null,\"buildingPreviousUses\":null,\"buildingPreviousUsesOther\":null,\"buildingListedStatus\":null,\"siteInConservationArea\":null,\"descriptionOfWorks\":null},\"hasApplicantConfirmedDetailsAreCorrect\":true,\"brokerAgreementId\":null,\"leadId\":null,\"lossOfGrossRentAnnualAmountNumberOfYears\":null,\"lossOfGrossRentAnnualAmount\":null,\"lossOfRentPayableAnnualAmountNumberOfYears\":null,\"lossOfRentPayableAnnualAmount\":null,\"lossOfRentRecievableAmmountNumberOfYears\":null,\"lossofRentReceivableAnnualAmount\":null,\"quoteVersionsParentQuoteId\":null,\"insuranceAgreementGroup\":2,\"doesTheSiteIncludeCurtainWalling\":false,\"atkinsBuildDuration\":0,\"productVersionDate\":\"2018-08-28\"}";
            var response = await client.PostAsync($"{envrironmentApiServer}/api/quote/application/save", new StringContent(body, Encoding.Default, "application/json"));
            Assert.AreEqual(HttpStatusCode.Unauthorized, response.StatusCode);
        }
        //search operations
        public async Task MainAsyncVerifySearchContactsWhenUserHasNoPermission()
        {
            var uniqueGuID = System.Guid.NewGuid();
            var envrironmentApiServer = ConfigurationManager.AppSettings["ApiServerUrl"];
            var client = HttpClientHelpers.CreateClient("auto.user@mdinsurance.co.uk", "Mdis@1234;");
            string body = "{ \"name\":\"SunilKumar\",\"address\":\"\",\"email\":\"\",\"phone\":\"\",\"includeCompanies\":true,\"includeEmployees\":true,\"includePeople\":true,\"companyTypes\":[],\"employeeRoles\":[],\"internalOnly\":false,\"externalOnly\":false,\"ourRegistrationNumber\":\"\",\"internalBrandFilter\":null,\"start\":30,\"count\":30,\"sortBy\":\"name\",\"sortDirection\":\"asc\"}";
            var response = await client.PostAsync($"{envrironmentApiServer}/api/contact/search", new StringContent(body, Encoding.Default, "application/json"));
           // Assert.AreEqual(HttpStatusCode.Unauthorized, response.StatusCode);
        }
        public async Task MainAsyncVerifySearchSitesWhenUserHasNoPermission()
        {
            var uniqueGuID = System.Guid.NewGuid();
            var envrironmentApiServer = ConfigurationManager.AppSettings["ApiServerUrl"];
            var client = HttpClientHelpers.CreateClient("auto.user@mdinsurance.co.uk", "Mdis@1234;");
            string body = "{\"roleSearch\":[],\"includeLeads\":true,\"includeQuotes\":true,\"includeOrders\":false,\"includeLabc\":true,\"includePg\":true,\"reference\":\"Pl-Pg-21210\",\"address\":\"\",\"includeRemoved\":false,\"search\":null,\"start\":0,\"count\":30,\"sortBy\":\"brandShortName\",\"sortDirection\":\"asc\"}";
            var response = await client.PostAsync($"{envrironmentApiServer}/api/sites/search", new StringContent(body, Encoding.Default, "application/json"));
            //Assert.AreEqual(HttpStatusCode.Unauthorized, response.StatusCode);
        }
        public async Task MainAsyncVerifySearchPlotsWhenUserHasNoPermission()
        {
            var uniqueGuID = System.Guid.NewGuid();
            var envrironmentApiServer = ConfigurationManager.AppSettings["ApiServerUrl"];
            var client = HttpClientHelpers.CreateClient("auto.user@mdinsurance.co.uk", "Mdis@1234;");
            string body = "{\"includeLABC\":true,\"includePG\":true,\"address\":\"Flat 5\",\"includeRemoved\":false,\"start\":0,\"count\":30,\"sortBy\":\"\",\"sortDirection\":\"asc\"}";
            var response = await client.PostAsync($"{envrironmentApiServer}/api/plotsearch/search", new StringContent(body, Encoding.Default, "application/json"));
            //Assert.AreEqual(HttpStatusCode.Unauthorized, response.StatusCode);
        }
        //Lead operations 
        public async Task MainAsyncVerifyCreatingLeadWhenUserHasNoPermission()
        {
            var uniqueGuID = System.Guid.NewGuid();
            var envrironmentApiServer = ConfigurationManager.AppSettings["ApiServerUrl"];
            var client = HttpClientHelpers.CreateClient("auto.user@mdinsurance.co.uk", "Mdis@1234;");
            string body = string.Format("{{\"brand\":1,\"address\":{{\"id\":\"96941b02-41f0-4bb9-a487-2fffc7d52278\",\"address1\":\"21 Jamaica Street\",\"address2\":\"Liverpool\",\"address3\":\"\",\"town\":\"Merseyside\",\"postcode\":\"L1 0AA\",\"countryCodeType\":214}},\"quoteRecipient\":\"b91dbc16-32cd-40d5-b9d2-a92a0114c4b5\",\"additionalRoles\":[{{\"role\":8,\"ids\":[\"b91dbc16-32cd-40d5-b9d2-a92a0114c4b5\"]}}],\"descriptionOfWorks\":\"teast\",\"numberOfStoreysAboveGround\":0,\"numberOfStoreysBelowGround\":0,\"totalFloorArea\":10,\"includesConversionElements\":true,\"isNewBuild\":true,\"products\":[{{\"productId\":\"28f685e8-9a11-5bbf-a52c-891af64e068b\",\"plots\":1,\"totalReconstructionCosts\":1,\"totalEstimateSalePrice\":1,\"productItems\":[\"42af0b4d-2f89-5f13-a154-7ab87689032c\",\"589e5b69-3326-55aa-b89c-46ced7832887\",\"d001feb8-37bc-5a7a-96f8-fafa5d4ab722\"]}},{{\"productId\":\"ed59c767-9507-5159-ad26-379d3e40a5fa\",\"plots\":1,\"totalReconstructionCosts\":1,\"totalEstimateSalePrice\":1,\"productItems\":[\"5e936810-6f36-5f3e-a998-4748b7897f95\",\"e878cc32-54d3-5b55-aa0e-e8cac563d790\",\"5e977bf2-064d-5df0-b1d3-04a4cffb1183\"]}},{{\"productId\":\"2f4b815d-0da5-56c2-8fc8-017cf9df1faf\",\"plots\":1,\"totalReconstructionCosts\":1,\"totalEstimateSalePrice\":1,\"productItems\":[\"e6ce21d7-33db-59aa-8f3a-991835da15b5\",\"28123ab8-e850-5b86-9ff1-8c20aa7258f1\",\"9e1f18d5-52c6-5bc0-8d1e-d952e92bbfa0\",\"e16de7e6-7da1-5f09-b82d-a9fed3488992\",\"baab5893-82c1-5495-9464-408cba637720\"]}},{{\"productId\":\"e58f03e6-ecdd-5248-b633-b23a76369a48\",\"plots\":1,\"numberOfApartmentPlots\":1,\"totalReconstructionCosts\":1,\"totalEstimateSalePrice\":1,\"productItems\":[\"a900ddad-6aeb-5bb8-ad6b-1234f4403bf3\",\"35ad49c7-862d-52e9-8728-3d22859dd711\",\"12174170-2cf2-5b68-9b1e-416eb2c412d5\",\"2b5a84dd-4102-50c2-9f92-861ae96ceb47\",\"99e8b716-3773-5e19-8253-a54e01b07d70\",\"94f28d63-b61d-5672-bebf-0abfd3f53e75\"]}},{{\"productId\":\"ec3e3822-11f2-5b67-962b-a146f597d5f4\",\"plots\":1,\"totalReconstructionCosts\":1,\"totalEstimateSalePrice\":1,\"productItems\":[\"14eab6a5-374e-5815-a87a-f5ecb5ff0e92\",\"c31d11f0-f4ce-5d28-8d21-53791c6f30c5\",\"a25e4ac6-b656-5251-b7a5-b8c1bc8e786f\",\"9306e50e-c16f-5a22-89a2-73967d5e17bf\",\"bcf71b9d-c7c3-51e8-ba74-e7be4424d22b\"]}},{{\"productId\":\"e3edd98a-7638-5ad8-a9cc-8e761afa8a1d\",\"plots\":1,\"numberOfApartmentPlots\":1,\"totalReconstructionCosts\":1,\"totalEstimateSalePrice\":1,\"productItems\":[\"16d1e682-bc6f-5118-bb3c-e238d7bb9bbd\",\"62c70885-3a11-5a8a-8a8e-26bfff84be83\",\"767e44ef-5218-553d-8c5f-2eb8729bf6d2\",\"3c29cd7b-5f5b-533c-af31-48d545716390\",\"f7599bfe-332a-552a-9535-bd1ef3bd3f7f\"]}},{{\"productId\":\"f61b6dc6-216a-516c-8697-5b014c191025\",\"plots\":1,\"numberOfApartmentPlots\":1,\"totalReconstructionCosts\":1,\"totalEstimateSalePrice\":1,\"productItems\":[\"a76458d5-5f0a-5302-8261-806e82a65d9d\",\"61b6215a-fd67-5854-b75f-7e8989051f8a\",\"f523989c-37cc-53f1-a755-813a12bac980\",\"42ba0d0e-3c50-571d-8c0a-746f29debba3\",\"72e99fb2-14b6-52a7-9a25-fe9d25fb8b1f\",\"bffce536-0092-5b03-a6f4-338fdc886d9b\",\"29e38f58-b7ad-551c-a670-bc6b7fc7e2e0\",\"328bec3c-c6cd-5823-a267-4245699837c6\"]}},{{\"productId\":\"aa1ffbd4-cae9-5e0b-9383-38824498cf39\",\"plots\":1,\"numberOfApartmentPlots\":1,\"totalReconstructionCosts\":1,\"totalEstimateSalePrice\":1,\"productItems\":[\"7506fc0b-4242-5ee1-b49a-a7723d5acf8f\",\"439d5e66-a292-564e-bc14-83715f7c693f\",\"863a814e-f38f-546e-b2ac-de3304beded9\",\"cb8e708d-1cc9-5fc0-b473-32a5307c12c0\",\"96416ea1-5345-5eb1-9f67-67cdca7da8ee\",\"4f327ce1-6764-5fa5-928d-efb8f7f9a769\",\"418f3cd6-ed66-549d-900c-e00d01254421\"]}},{{\"productId\":\"9deb8006-8fc5-5883-9c67-68c720cb8b47\",\"plots\":1,\"numberOfApartmentPlots\":1,\"totalReconstructionCosts\":1,\"totalEstimateSalePrice\":1,\"productItems\":[\"4ee6bbab-863d-5962-963f-3d78ea95f6c7\",\"434129ab-56e0-5bcd-b2a8-9928db020d2a\",\"d32a5034-c6fd-5137-8c62-050de9bcab2b\",\"fdd2b69b-b4da-50e2-b739-104842cd4bed\",\"281000cb-0b25-535e-adc2-5b660c6cc3c1\"]}},{{\"productId\":\"4a77a1f6-3e71-5149-83b1-4d0e74edcb9a\",\"plots\":1,\"numberOfApartmentPlots\":1,\"totalReconstructionCosts\":1,\"totalEstimateSalePrice\":1,\"productItems\":[\"6c03a08e-2aff-5da3-9e44-e31feaf2a31b\",\"27a6bc69-e483-5486-a8de-0b74adb31732\",\"4d7dfdd5-057c-5e8b-9d5b-b70b402adf82\",\"61468901-ba66-53d3-bc29-cdada359cf03\",\"6e4f2a4f-c040-55a4-9b28-c23966d81208\",\"8e0a79c8-2722-5331-84aa-2248d8f8e37b\",\"29ca8125-591e-58ce-94b6-c36b1aef88e7\"]}}],\"quoteRecipientsOfficeId\":null,\"siteRating\":2}}");
            var response = await client.PostAsync($"{envrironmentApiServer}/api/leads/save", new StringContent(body, Encoding.Default, "application/json"));
            Assert.AreEqual(HttpStatusCode.Unauthorized, response.StatusCode);
        }
    
        public async Task MainAsyncVerifyAmendingLeadWhenUserHasNoPermission()
        {
            var uniqueGuID = System.Guid.NewGuid();
            var envrironmentApiServer = ConfigurationManager.AppSettings["ApiServerUrl"];
            var client = HttpClientHelpers.CreateClient("auto.user@mdinsurance.co.uk", "Mdis@1234;");
            string body = string.Format("{{\"leadId\":\"82d63cc9-e128-4399-8fe9-a95100ed78cb\",\"brand\":2,\"address\":{{\"id\":\"a06a5290-5f95-426e-9a16-a95100edbc4e\",\"isDefault\":false,\"isDiscontinued\":false,\"address1\":\"21 Jamaica Street\",\"address2\":\"Liverpool\",\"address3\":\"\",\"town\":\"Merseyside\",\"postcode\":\"L1 0AA\",\"countryCodeType\":214}},\"quoteRecipient\":\"b91dbc16-32cd-40d5-b9d2-a92a0114c4b5\",\"additionalRoles\":[{{\"role\":8,\"ids\":[\"b91dbc16-32cd-40d5-b9d2-a92a0114c4b5\"]}},{{\"role\":7,\"ids\":[\"b91dbc16-32cd-40d5-b9d2-a92a0114c4b5\"]}}],\"descriptionOfWorks\":\"test\",\"estimatedConstructionStartDate\":\"2018-09-01T00:00:00.000Z\",\"estimatedConstructionEndDate\":\"2018-09-30T00:00:00.000Z\",\"quoteDecisionDate\":\"2018-09-04T00:00:00.000Z\",\"numberOfStoreysAboveGround\":1,\"numberOfStoreysBelowGround\":1,\"totalFloorArea\":1,\"currentPlanningStage\":3,\"includesConversionElements\":true,\"isNewBuild\":true,\"products\":[{{\"productId\":\"717a08ad-e4a6-5965-a3fc-7fc16c823270\",\"plots\":1,\"numberOfApartmentPlots\":null,\"totalReconstructionCosts\":1,\"totalEstimateSalePrice\":1,\"length\":0,\"productItems\":[\"1a1a8236-39e9-5beb-97c3-f05f0e738e4b\",\"1fdb4cb8-0d16-50c3-a182-f5084b255916\",\"6d4de386-e393-5ce9-877d-06db8bbf4bb0\",\"30dd0f2b-1adc-5b61-a435-08349c94a852\",\"2649727e-a64a-592a-8f89-77b233d60d52\"]}},{{\"productId\":\"d7ab6046-cb5c-5f37-9b48-9abaf0100afb\",\"plots\":1,\"numberOfApartmentPlots\":1,\"totalReconstructionCosts\":1,\"totalEstimateSalePrice\":1,\"length\":0,\"productItems\":[\"115e0c8b-9b7c-5eb3-9df4-7b35985d5103\",\"02b4e2d1-f3ce-593d-a357-c41b62798057\",\"07081ec4-4edd-5034-9204-3653eb923e82\",\"9bf5f571-0c5f-5c7c-98f6-70ab487c5273\",\"4d02328c-b072-5786-984e-273541ddb5ea\"]}},{{\"productId\":\"c585c0a8-206d-54b4-85a4-f7f45d6de4aa\",\"plots\":1,\"numberOfApartmentPlots\":1,\"totalReconstructionCosts\":1,\"totalEstimateSalePrice\":1,\"length\":0,\"productItems\":[\"005c9b9a-18c6-5de9-ba2a-b43ab20fd441\",\"ed47d1e4-0c25-53a9-bcce-241a72e3ab1e\",\"94fea1b6-7971-5bd2-8b96-a5b996ece28e\",\"56977def-b2b9-57d0-983c-6703177ff5e8\",\"980b6810-af7d-5192-b323-bf5b18a775d8\",\"55113f73-cefc-5e2f-8d78-bc792ad6d805\",\"29acbe5e-0524-57bc-97dc-d6c8a0b66534\",\"5fecb6c3-4972-572b-91c7-263c7f2cf0fc\"]}},{{\"productId\":\"d2e82569-d843-5214-8b44-0fac68b3c4de\",\"plots\":1,\"numberOfApartmentPlots\":1,\"totalReconstructionCosts\":1,\"totalEstimateSalePrice\":1,\"length\":0,\"productItems\":[\"f82c5089-f54c-5925-ba79-b5a1e63df410\",\"96db6391-b9ad-56cb-80ac-58eaa1e2e5cd\",\"35e0aa84-603f-5e4a-b247-38700a8ab8e8\",\"c7c2fc6b-4509-5f86-9f41-33b2300e2dd9\",\"73f39b5b-6954-5c22-9866-301cc5a1e5b0\",\"48419248-6cfe-549c-a2eb-370e2a653f2f\",\"6d5336d0-bf8b-5cd5-9925-096025a7a528\"]}},{{\"productId\":\"c60951b3-1a4c-5f9d-a834-9532f2640b6d\",\"plots\":1,\"numberOfApartmentPlots\":1,\"totalReconstructionCosts\":1,\"totalEstimateSalePrice\":1,\"length\":0,\"productItems\":[\"6308af89-7b9e-5c5f-bf9e-ada1f542ab60\",\"87ef5804-a575-50c7-a7af-24583f525e74\",\"5e1c1f7b-f420-5089-95f0-d79391800d21\",\"00faa916-2a01-5188-87c1-541741b5c39c\",\"e5fa69b9-4df1-5ad8-8d4e-ee6bf24253c4\"]}},{{\"productId\":\"68bc9325-9dc5-50c1-a694-cce10daacb82\",\"plots\":1,\"numberOfApartmentPlots\":1,\"totalReconstructionCosts\":1,\"totalEstimateSalePrice\":1,\"length\":0,\"productItems\":[\"5be3c9eb-94b0-5018-bc75-6355c2121122\",\"3d391c31-9ba8-5dbe-8183-fe40eed63ad7\",\"3bce8bef-fa18-529f-b5ed-63b61dfbadd9\",\"490bffac-48fe-55e4-a30e-a641540fb76d\",\"2b991b5c-93d9-5e82-a6e4-ed6dbcc486a7\",\"56869b7b-8676-5219-a127-10dc375c0bf6\",\"53dfd375-615c-5da5-9ff6-7897f9621dda\"]}}],\"siteRating\":2}}");
            var response = await client.PostAsync($"{envrironmentApiServer}/api/leads/save", new StringContent(body, Encoding.Default, "application/json"));
            Assert.AreEqual(HttpStatusCode.Unauthorized, response.StatusCode);
        }
        public async Task MainAsyncVerifySearchLeadWhenUserHasNoPermission()
        {
            var uniqueGuID = System.Guid.NewGuid();
            var envrironmentApiServer = ConfigurationManager.AppSettings["ApiServerUrl"];
            var client = HttpClientHelpers.CreateClient("auto.user@mdinsurance.co.uk", "Mdis@1234;");
            string body = string.Format("{{\"roleSearch\":[],\"includeLeads\":true,\"includeQuotes\":true,\"includeOrders\":false,\"includeLabc\":true,\"includePg\":true,\"reference\":\"Lead - Labc\",\"address\":\"\",\"includeRemoved\":false,\"search\":null,\"start\":30,\"count\":30,\"sortBy\":\"brandShortName\",\"sortDirection\":\"asc\"}}");
            var response = await client.PostAsync($"{envrironmentApiServer}/api/sites/search", new StringContent(body, Encoding.Default, "application/json"));
            Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
        }
        //Internal user Operations
        public async Task MainAsyncVerifyCreatingInternalUserWhenUserHasNoPermission()
        {
            var uniqueGuID = System.Guid.NewGuid();
            var envrironmentApiServer = ConfigurationManager.AppSettings["ApiServerUrl"];
            var client = HttpClientHelpers.CreateClient("auto.user@mdinsurance.co.uk", "Mdis@1234;");
            string body = string.Format("{{\"permissions\":{{\"accounts\":{{\"importReceipts\":true,\"allocateMoney\":true,\"manageBordereau\":false}},\"internalUsers\":{{\"managePermissions\":true,\"create\":true,\"amend\":true,\"view\":true}},\"contacts\":{{\"setNonStandardPaymentTerms\":true,\"amendCompanyName\":false,\"manage\":true,\"create\":true,\"amend\":true,\"view\":true}},\"leads\":{{\"create\":true,\"amend\":true,\"view\":true}},\"opportunity\":{{\"create\":true,\"amend\":true,\"view\":true}},\"quote\":{{\"accept\":true,\"viewExternalIncomplete\":true,\"reconstructionCostLimit\":0,\"products\":[],\"setSpecialTerms\":false,\"create\":true,\"amend\":true,\"view\":true}},\"order\":{{\"approveReferral\":true,\"issueCoa\":true,\"issueCoa2\":true,\"issueCoi\":true,\"importReceipts\":true,\"allocateMoney\":true,\"ammendRoles\":true,\"issueDevelopmentIc\":true,\"issuePlotIc\":true,\"issueFinalNotice\":true,\"cancel\":true,\"changePaymentTerms\":false,\"reinstate\":false,\"create\":true,\"amend\":true,\"view\":true}},\"manageCompanyLists\":{{\"create\":true,\"amend\":true,\"view\":true}},\"manageCompanyGroups\":{{\"create\":true,\"amend\":true,\"view\":true}},\"refurbAssessment\":{{\"create\":true,\"amend\":true,\"view\":true}},\"siteRiskAssessment\":{{\"create\":true,\"amend\":true,\"view\":true}},\"inspection\":{{\"create\":true,\"amend\":true,\"view\":true}},\"designReview\":{{\"create\":true,\"amend\":true,\"view\":true}},\"engineerReview\":{{\"create\":true,\"amend\":true,\"view\":true}},\"structuralReview\":{{\"create\":true,\"amend\":true,\"view\":true}},\"registration\":{{\"manage\":true,\"create\":true,\"amend\":true,\"view\":true}},\"claim\":{{\"create\":true,\"amend\":true,\"view\":true}},\"recovery\":{{\"create\":true,\"amend\":true,\"view\":true}},\"salesAppointments\":{{\"create\":true,\"amend\":true,\"view\":true}},\"technicalHub\":{{\"createContent\":true,\"approveContent\":true,\"publishContent\":true}},\"companyBlocking\":{{\"create\":true,\"remove\":true}},\"managerOverrides\":{{\"processes\":[],\"paymentValidation\":[]}},\"localAuthorities\":{{\"buildingControlAdmin\":true}},\"notes\":{{\"create\":true,\"amend\":true,\"view\":true}},\"endorsements\":{{\"create\":true}},\"bonds\":{{\"manage\":true,\"bondValueLimit\":10000000}},\"ratings\":{{\"confirm\":true}},\"extranet\":{{\"administrator\":true}},\"rescind\":{{\"rescindDevelopmentIc\":false,\"rescindPlotIc\":false,\"rescindCoverNote\":false,\"rescindCoa\":false,\"rescindCoi\":false,\"rescindInitialNotice\":false,\"rescindFinalNotice\":false,\"rescindBuildingControlTechnicalSignOff\":false,\"create\":false,\"amend\":false,\"view\":false}}}},\"salutation\":\"Mr\",\"firstname\":\"Sunil\",\"surname\":\"kumar\",\"companyId\":\"60b050bb-73d2-4741-9c84-a7e3c26cd3d5\",\"managerId\":\"84add8b2-d0ae-44ff-af25-b26cbc9e72f7\",\"salesBuddyIds\":[],\"email\":\"test{0}@mdinsurance.co.uk\",\"phoneCountryCode\":214,\"phone\":\"01483493721\",\"primaryOffice\":\"c995818e-906a-4679-9415-cbc6cac4873a\",\"offices\":[\"c995818e-906a-4679-9415-cbc6cac4873a\"],\"fullTimeEquivalence\":1,\"department\":\"Information Technology\",\"team\":\"Test\",\"jobTitle\":\"IT User\",\"groups\":[1,2]}}", uniqueGuID);
            var response = await client.PostAsync($"{envrironmentApiServer}/api/InternalAccounts", new StringContent(body, Encoding.Default, "application/json"));
            Assert.AreEqual(HttpStatusCode.Unauthorized, response.StatusCode);
        }
        public async Task MainAsyncVerifyAmendingInternalUserWhenUserHasNoPermission()
        {
            var uniqueGuID = System.Guid.NewGuid();
            var envrironmentApiServer = ConfigurationManager.AppSettings["ApiServerUrl"];
            var client = HttpClientHelpers.CreateClient("auto.user@mdinsurance.co.uk", "Mdis@1234;");
            string body = string.Format("{{\"permissions\":{{\"accounts\":{{\"importReceipts\":true,\"allocateMoney\":true,\"manageBordereau\":false}},\"internalUsers\":{{\"managePermissions\":true,\"create\":true,\"amend\":true,\"view\":true}},\"contacts\":{{\"setNonStandardPaymentTerms\":true,\"amendCompanyName\":false,\"manage\":true,\"create\":true,\"amend\":true,\"view\":true}},\"leads\":{{\"create\":true,\"amend\":true,\"view\":true}},\"opportunity\":{{\"create\":true,\"amend\":true,\"view\":true}},\"quote\":{{\"accept\":true,\"viewExternalIncomplete\":true,\"reconstructionCostLimit\":0,\"products\":[],\"setSpecialTerms\":false,\"create\":true,\"amend\":true,\"view\":true}},\"order\":{{\"approveReferral\":true,\"issueCoa\":true,\"issueCoa2\":true,\"issueCoi\":true,\"importReceipts\":true,\"allocateMoney\":true,\"ammendRoles\":true,\"issueDevelopmentIc\":true,\"issuePlotIc\":true,\"issueFinalNotice\":true,\"cancel\":true,\"changePaymentTerms\":false,\"reinstate\":false,\"create\":true,\"amend\":true,\"view\":true}},\"manageCompanyLists\":{{\"create\":true,\"amend\":true,\"view\":true}},\"manageCompanyGroups\":{{\"create\":true,\"amend\":true,\"view\":true}},\"refurbAssessment\":{{\"create\":true,\"amend\":true,\"view\":true}},\"siteRiskAssessment\":{{\"create\":true,\"amend\":true,\"view\":true}},\"inspection\":{{\"create\":true,\"amend\":true,\"view\":true}},\"designReview\":{{\"create\":true,\"amend\":true,\"view\":true}},\"engineerReview\":{{\"create\":true,\"amend\":true,\"view\":true}},\"structuralReview\":{{\"create\":true,\"amend\":true,\"view\":true}},\"registration\":{{\"manage\":true,\"create\":true,\"amend\":true,\"view\":true}},\"claim\":{{\"create\":true,\"amend\":true,\"view\":true}},\"recovery\":{{\"create\":true,\"amend\":true,\"view\":true}},\"salesAppointments\":{{\"create\":true,\"amend\":true,\"view\":true}},\"technicalHub\":{{\"createContent\":true,\"approveContent\":true,\"publishContent\":true}},\"companyBlocking\":{{\"create\":true,\"remove\":true}},\"managerOverrides\":{{\"processes\":[],\"paymentValidation\":[]}},\"localAuthorities\":{{\"buildingControlAdmin\":true}},\"notes\":{{\"create\":true,\"amend\":true,\"view\":true}},\"endorsements\":{{\"create\":true}},\"bonds\":{{\"manage\":true,\"bondValueLimit\":10000000}},\"ratings\":{{\"confirm\":true}},\"extranet\":{{\"administrator\":true}},\"rescind\":{{\"rescindDevelopmentIc\":false,\"rescindPlotIc\":false,\"rescindCoverNote\":false,\"rescindCoa\":false,\"rescindCoi\":false,\"rescindInitialNotice\":false,\"rescindFinalNotice\":false,\"rescindBuildingControlTechnicalSignOff\":false,\"create\":false,\"amend\":false,\"view\":false}}}},\"salutation\":\"Mr\",\"firstname\":\"Sunil\",\"surname\":\"kumar\",\"companyId\":\"60b050bb-73d2-4741-9c84-a7e3c26cd3d5\",\"managerId\":\"84add8b2-d0ae-44ff-af25-b26cbc9e72f7\",\"salesBuddyIds\":[],\"email\":\"test{0}@mdinsurance.co.uk\",\"phoneCountryCode\":214,\"phone\":\"01483493721\",\"primaryOffice\":\"c995818e-906a-4679-9415-cbc6cac4873a\",\"offices\":[\"c995818e-906a-4679-9415-cbc6cac4873a\"],\"fullTimeEquivalence\":1,\"department\":\"Information Technology\",\"team\":\"Test\",\"jobTitle\":\"IT User\",\"groups\":[1,2]}}", uniqueGuID);
            var response = await client.PostAsync($"{envrironmentApiServer}/api/InternalAccounts", new StringContent(body, Encoding.Default, "application/json"));
            Assert.AreEqual(HttpStatusCode.Unauthorized, response.StatusCode);
        }
        public async Task MainAsyncVerifyViewingInternalUserWhenUserHasNoPermission()
        {
            var uniqueGuID = System.Guid.NewGuid();
            var envrironmentApiServer = ConfigurationManager.AppSettings["ApiServerUrl"];
            var client = HttpClientHelpers.CreateClient("auto.user@mdinsurance.co.uk", "Mdis@1234;");
            string body = string.Format("{{\"permissions\":{{\"accounts\":{{\"importReceipts\":true,\"allocateMoney\":true,\"manageBordereau\":false}},\"internalUsers\":{{\"managePermissions\":true,\"create\":true,\"amend\":true,\"view\":true}},\"contacts\":{{\"setNonStandardPaymentTerms\":true,\"amendCompanyName\":false,\"manage\":true,\"create\":true,\"amend\":true,\"view\":true}},\"leads\":{{\"create\":true,\"amend\":true,\"view\":true}},\"opportunity\":{{\"create\":true,\"amend\":true,\"view\":true}},\"quote\":{{\"accept\":true,\"viewExternalIncomplete\":true,\"reconstructionCostLimit\":0,\"products\":[],\"setSpecialTerms\":false,\"create\":true,\"amend\":true,\"view\":true}},\"order\":{{\"approveReferral\":true,\"issueCoa\":true,\"issueCoa2\":true,\"issueCoi\":true,\"importReceipts\":true,\"allocateMoney\":true,\"ammendRoles\":true,\"issueDevelopmentIc\":true,\"issuePlotIc\":true,\"issueFinalNotice\":true,\"cancel\":true,\"changePaymentTerms\":false,\"reinstate\":false,\"create\":true,\"amend\":true,\"view\":true}},\"manageCompanyLists\":{{\"create\":true,\"amend\":true,\"view\":true}},\"manageCompanyGroups\":{{\"create\":true,\"amend\":true,\"view\":true}},\"refurbAssessment\":{{\"create\":true,\"amend\":true,\"view\":true}},\"siteRiskAssessment\":{{\"create\":true,\"amend\":true,\"view\":true}},\"inspection\":{{\"create\":true,\"amend\":true,\"view\":true}},\"designReview\":{{\"create\":true,\"amend\":true,\"view\":true}},\"engineerReview\":{{\"create\":true,\"amend\":true,\"view\":true}},\"structuralReview\":{{\"create\":true,\"amend\":true,\"view\":true}},\"registration\":{{\"manage\":true,\"create\":true,\"amend\":true,\"view\":true}},\"claim\":{{\"create\":true,\"amend\":true,\"view\":true}},\"recovery\":{{\"create\":true,\"amend\":true,\"view\":true}},\"salesAppointments\":{{\"create\":true,\"amend\":true,\"view\":true}},\"technicalHub\":{{\"createContent\":true,\"approveContent\":true,\"publishContent\":true}},\"companyBlocking\":{{\"create\":true,\"remove\":true}},\"managerOverrides\":{{\"processes\":[],\"paymentValidation\":[]}},\"localAuthorities\":{{\"buildingControlAdmin\":true}},\"notes\":{{\"create\":true,\"amend\":true,\"view\":true}},\"endorsements\":{{\"create\":true}},\"bonds\":{{\"manage\":true,\"bondValueLimit\":10000000}},\"ratings\":{{\"confirm\":true}},\"extranet\":{{\"administrator\":true}},\"rescind\":{{\"rescindDevelopmentIc\":false,\"rescindPlotIc\":false,\"rescindCoverNote\":false,\"rescindCoa\":false,\"rescindCoi\":false,\"rescindInitialNotice\":false,\"rescindFinalNotice\":false,\"rescindBuildingControlTechnicalSignOff\":false,\"create\":false,\"amend\":false,\"view\":false}}}},\"salutation\":\"Mr\",\"firstname\":\"Sunil\",\"surname\":\"kumar\",\"companyId\":\"60b050bb-73d2-4741-9c84-a7e3c26cd3d5\",\"managerId\":\"84add8b2-d0ae-44ff-af25-b26cbc9e72f7\",\"salesBuddyIds\":[],\"email\":\"test{0}@mdinsurance.co.uk\",\"phoneCountryCode\":214,\"phone\":\"01483493721\",\"primaryOffice\":\"c995818e-906a-4679-9415-cbc6cac4873a\",\"offices\":[\"c995818e-906a-4679-9415-cbc6cac4873a\"],\"fullTimeEquivalence\":1,\"department\":\"Information Technology\",\"team\":\"Test\",\"jobTitle\":\"IT User\",\"groups\":[1,2]}}", uniqueGuID);
            var response = await client.PostAsync($"{envrironmentApiServer}/api/InternalAccounts", new StringContent(body, Encoding.Default, "application/json"));
            Assert.AreEqual(HttpStatusCode.Unauthorized, response.StatusCode);
        }
    }
    public class Nopermission
    {
        public bool NoPermissions { get; set; }
    }
    public class GeneralPermissions
    {
        public bool Create { get; set; }

        public bool Amend { get; set; }

        public bool View { get; set; }
    }

    public class ManagedGeneralPermissions : GeneralPermissions
    {
        public bool Manage { get; set; }
    }
    public class AccountsPermissions
    {
        public bool ImportReceipts { get; set; }

        public bool AllocateMoney { get; set; }

        public bool ManageBordereau { get; set; }
    }
    public class BondsPermissions
    {
        public bool Manage { get; set; }

        public decimal BondValueLimit { get; set; }
    }
    public class CompanyBlockingPermissions
    {
        public bool Create { get; set; }

        public bool Remove { get; set; }
    }
    public class ContactPermissions : ManagedGeneralPermissions
    {
        public bool SetNonStandardPaymentTerms { get; set; }

        public bool AmendCompanyName { get; set; }
    }
    public class EndorsementPermissions
    {
        public bool Create { get; set; }
    }
    public class ExtranetPermissions
    {
        public bool Administrator { get; set; }
    }
    public class FinancialAccountsPermissions
    {
        public bool ImportReceipts { get; set; }

        public bool AllocateMoney { get; set; }
    }
   
    public class LocalAuthorityPermissions
    {
        public bool BuildingControlAdmin { get; set; }
    }
    public class OrderPermissions : GeneralPermissions
    {
        public bool ApproveReferral { get; set; }

        public bool IssueCoa { get; set; }

        public bool IssueCoa2 { get; set; }

        public bool IssueCoi { get; set; }

        public bool ImportReceipts { get; set; }

        public bool AllocateMoney { get; set; }

        public bool AmmendRoles { get; set; }

        public bool IssueDevelopmentIc { get; set; }

        public bool IssuePlotIc { get; set; }

        public bool IssueFinalNotice { get; set; }

        public bool Cancel { get; set; }

        public bool ChangePaymentTerms { get; set; }

        public bool Reinstate { get; set; }
    }
    public class QuotePermissions : GeneralPermissions
    {      
        public bool Accept { get; set; }

        public bool ViewExternalIncomplete { get; set; }

        public decimal ReconstructionCostLimit { get; set; }

    }
    public class RatingsPermissions
    {
        public bool Confirm { get; set; }
    }
    public class RescindPermissions : GeneralPermissions
    {
        public bool RescindDevelopmentIc { get; set; }

        public bool RescindPlotIc { get; set; }

        public bool RescindCoverNote { get; set; }

        public bool RescindCoa { get; set; }

        public bool RescindCoi { get; set; }

        public bool RescindInitialNotice { get; set; }

        public bool RescindFinalNotice { get; set; }

        public bool RescindBuildingControlTechnicalSignOff { get; set; }
    }
    public class TechnicalHubPermissions
    {
        public bool CreateContent { get; set; }

        public bool ApproveContent { get; set; }

        public bool PublishContent { get; set; }
    }
    public class UserMangementPermissions : GeneralPermissions
    {
        public bool ManagePermissions { get; set; }
    }
    public class UserPermissions
    {
        public Nopermission NoPerm { get; set; } = new Nopermission();
        public AccountsPermissions Accounts { get; set; } = new AccountsPermissions();

        public UserMangementPermissions InternalUsers { get; set; } = new UserMangementPermissions();

        public ContactPermissions Contacts { get; set; } = new ContactPermissions();

        public GeneralPermissions Leads { get; set; } = new GeneralPermissions();

        public GeneralPermissions Opportunity { get; set; } = new GeneralPermissions();

        public QuotePermissions Quote { get; set; } = new QuotePermissions();

        public OrderPermissions Order { get; set; } = new OrderPermissions();

        public GeneralPermissions ManageCompanyLists { get; set; } = new GeneralPermissions();

        public GeneralPermissions ManageCompanyGroups { get; set; } = new GeneralPermissions();

        public GeneralPermissions RefurbAssessment { get; set; } = new GeneralPermissions();

        public GeneralPermissions SiteRiskAssessment { get; set; } = new GeneralPermissions();

        public GeneralPermissions Inspection { get; set; } = new GeneralPermissions();

        public GeneralPermissions DesignReview { get; set; } = new GeneralPermissions();

        public GeneralPermissions EngineerReview { get; set; } = new GeneralPermissions();

        public GeneralPermissions StructuralReview { get; set; } = new GeneralPermissions();

        public ManagedGeneralPermissions Registration { get; set; } = new ManagedGeneralPermissions();

        public GeneralPermissions Claim { get; set; } = new GeneralPermissions();

        public GeneralPermissions Recovery { get; set; } = new GeneralPermissions();

        public GeneralPermissions SalesAppointments { get; set; } = new GeneralPermissions();

        public TechnicalHubPermissions TechnicalHub { get; set; } = new TechnicalHubPermissions();

        public CompanyBlockingPermissions CompanyBlocking { get; set; } = new CompanyBlockingPermissions();

       // public ManagerOverridePermissions ManagerOverrides { get; set; } = new ManagerOverridePermissions();

        public LocalAuthorityPermissions LocalAuthorities { get; set; } = new LocalAuthorityPermissions();

        public GeneralPermissions Notes { get; set; } = new GeneralPermissions();

        public EndorsementPermissions Endorsements { get; set; } = new EndorsementPermissions();

        public BondsPermissions Bonds { get; set; } = new BondsPermissions();

        public RatingsPermissions Ratings { get; set; } = new RatingsPermissions();

        public ExtranetPermissions Extranet { get; set; } = new ExtranetPermissions();

        public RescindPermissions Rescind { get; set; } = new RescindPermissions();
    }
  
}
