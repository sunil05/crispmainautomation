﻿using OpenQA.Selenium;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.PageObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CrispAutomation.Permissions
{
    public class OverridableProcessesPage : Support.Pages
    {
        public IWebDriver wdriver;
        public OverridableProcessesPage(ISearchContext driver)
        {
            PageFactory.InitElements(driver, this);
            wdriver = (RemoteWebDriver)driver;
        }
        public bool NoPermissions { get; set; }
        public bool OverrideBCTechincalSignOffPermission{ get; set; }
        public bool OverrideCOAPermission { get; set; }
        public bool OverrideCOIPermission { get; set; }
        public bool OverrideDesignReviewPermission { get; set; }
        public bool OverrideDevICPermission { get; set; }
        public bool OverrideFinalNoticePermission { get; set; }
        public bool OverrideIntialNoticePermission { get; set; }
        public bool OverrideIntialNoticeAcceptanceToClientPermission { get; set; }
        public bool OverrideIntialNoticeRejectionToClientPermission { get; set; }
        public bool OverrideIssueCoverNotePermission { get; set; }
        public bool OverrideLocalEnactmentsToFireAuthorityPermission { get; set; }
        public bool OverridePlotICPermission { get; set; }
        public bool OverrideQuotePermission { get; set; }
        public bool OverrideQuoteAcceptancePermission { get; set; }
        public bool OverrideRefurbishmentAssessmentPermission { get; set; }
        public bool OverrideRegistrationRenewalPermission { get; set; }
        public bool OverrideSiteInspectionPermission { get; set; }
        public bool OverrideSiteRiskAssessmentPermission { get; set; }
   
    }
}
