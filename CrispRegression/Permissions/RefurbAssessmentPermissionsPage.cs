﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.PageObjects;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace CrispAutomation.Permissions
{
 
    public class RefurbAssessmentPermissionsPage : Support.Pages
    {
        public IWebDriver wdriver;
        public RefurbAssessmentPermissionsPage(ISearchContext driver)
        {
            PageFactory.InitElements(driver, this);
            wdriver = (RemoteWebDriver)driver;
        }
        public bool NoPermissions { get; set; }
        public bool CreateRefurbAssessmentPermission { get; set; }
        public bool AmendRefurbAssessmentPermission { get; set; }
        public bool ViewRefurbAssessmentPermission { get; set; }
    }
}
