﻿using OpenQA.Selenium;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.PageObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using System.Threading;
using OpenQA.Selenium.Support.UI;
using System.Configuration;
using System.Net.Http;
using System.Data.SqlClient;
using Newtonsoft.Json;
using System.Data;

namespace CrispAutomation.Pages
{
    public class ManagePermissionsPage
    {
        public IWebDriver wdriver;
        public ManagePermissionsPage(ISearchContext driver)
        {
            PageFactory.InitElements(driver, this);
            wdriver = (RemoteWebDriver)driver;
        }
        [FindsBy(How = How.XPath, Using = "//I[@class='au-target fa fa-plus']")]
        public IWebElement PlusButton;

        [FindsBy(How = How.XPath, Using = "//A[@click.delegate='sort(item)'][text()='Person']")]
        public IWebElement PersonButton;

        [FindsBy(How = How.XPath, Using = "//A[@click.delegate='sort(item)'][text()='Company']")]
        public IWebElement CompanyButton;

        [FindsBy(How = How.XPath, Using = "//A[@click.delegate='sort(item)'][text()='Lead']")]
        public IWebElement LeadButton;

        [FindsBy(How = How.CssSelector, Using = "crisp-button:nth-child(2) > span > button")]
        public IWebElement LeadOkButton;

        [FindsBy(How = How.XPath, Using = "//div[@class='au-target modal dialog brand default modal-has-header']")]
        public IWebElement PermissionsWizard;

        [FindsBy(How = How.XPath, Using = "//div[@class='au-target modal dialog brand default modal-fixed-footer modal-has-header modal-full-bleed modal-tall modal-wide']")]
        public IWebElement AddNewQuoteWizard;

        [FindsBy(How = How.XPath, Using = "//A[@click.delegate='sort(item)'][text()='Quote']")]
        public IWebElement QuoteButton;

        [FindsBy(How = How.XPath, Using = "//div[@class='au-target modal dialog brand default modal-fixed-footer modal-has-header modal-full-bleed modal-tall modal-wide']")]
        public IWebElement AddWizard;

        [FindsBy(How = How.CssSelector, Using = "crisp-header-actions > crisp-header-button:nth-child(2) > span > button")]
        public IWebElement EditPersonButton;

        [FindsBy(How = How.CssSelector, Using = "crisp-header-actions > crisp-header-button > span > button")]
        public IWebElement EditCompanyButton;

        [FindsBy(How = How.XPath, Using = "//crisp-input-bool//label[text()='Include companies?']")]
        public IWebElement IncludeCompaniesCheckbox;

        [FindsBy(How = How.XPath, Using = "//crisp-input-bool//label[text()='Include employees?']")]
        public IWebElement IncludeEmployeesCheckbox;

        [FindsBy(How = How.XPath, Using = "//crisp-input-bool//label[text()='Include people?']")]
        public IWebElement IncludePeopleCheckbox;

        [FindsBy(How = How.XPath, Using = "//LABEL[@data-error.bind='error'][text()='Name']")]
        public IWebElement NameField;

        [FindsBy(How = How.XPath, Using = "//crisp-input-text[@class='au-target'][@icon='user']/div/input[@type='text']")]
        public IWebElement EnterName;

        [FindsBy(How = How.XPath, Using = "//crisp-list[@ref='listElm']/ul/li[3]")]
        public IWebElement SelectContact;

        [FindsBy(How = How.XPath, Using = "//div//ul[@class='dropdown-content au-target active']//a[text()='Contacts']")]
        public IWebElement ContactOption;

        [FindsBy(How = How.XPath, Using = "//div//ul[@class='dropdown-content au-target active']//a[text()='Sites']")]
        public IWebElement SiteOption;

        [FindsBy(How = How.XPath, Using = "//crisp-input-bool[@label='Include quotes?']/label")]
        public IWebElement IncludeQuotes;

        [FindsBy(How = How.XPath, Using = "//crisp-input-bool[@label='Include orders?']/label")]
        public IWebElement IncludeOrders;

        [FindsBy(How = How.XPath, Using = "//crisp-input-bool[@label='Premier Guarantee']/label")]
        public IWebElement FilterPGcheckBox;

        [FindsBy(How = How.XPath, Using = "//crisp-input-bool[@label='LABC']/label")]
        public IWebElement FilterLABCcheckBox;

        [FindsBy(How = How.XPath, Using = "//ul[@ref='theList']/li[4]/crisp-list-item[@class='au-target brand labc']")]
        public IWebElement LABCFilterOption;

        [FindsBy(How = How.CssSelector, Using = "crisp-header-button:nth-child(4) > span > button")]
        public IWebElement EditLeadButton;

        [FindsBy(How = How.XPath, Using = "//ul[@ref='theList']/li[4]/crisp-list-item[@class='au-target brand pg']")]
        public IWebElement PGFilterOption;


        [FindsBy(How = How.XPath,
            Using = "//crisp-input-radio[@label='Brand']/div/ul/li/label[text()='Premier Guarantee']")]
        public IWebElement PGRadioButton;

        [FindsBy(How = How.XPath,
            Using = "//crisp-input-radio[@label='Brand']/div/ul/li/label[text()='LABC Warranty']")]
        public IWebElement LABCRadioButton;

        [FindsBy(How = How.CssSelector, Using = "crisp-input-object:nth-child(2) > div > label")]
        public IWebElement QuoteRecipientLabel;

        [FindsBy(How = How.CssSelector, Using = "li:nth-child(9) > crisp-list-item > div")]
        public IWebElement QuoteRecipient;

        [FindsBy(How = How.CssSelector, Using = "crisp-input-object:nth-child(3) > div > label")]
        public IWebElement AddressLabel;

        [FindsBy(How = How.XPath, Using = "//crisp-button/span/button[text()='Search']")]
        public IWebElement SearchButton;

        [FindsBy(How = How.CssSelector, Using = "span > i[class='au-target fa fa-search']")]
        public IWebElement SearchOption;

        [FindsBy(How = How.CssSelector, Using = "crisp-input-text > div > label")]
        public IWebElement PostcodeSearchLabel;

        [FindsBy(How = How.CssSelector, Using = "crisp-input-text > div > input")]
        public IWebElement PostcodeInput;

        [FindsBy(How = How.XPath, Using = "//crisp-picker[@label='Address']/div/div/input[@class='select-dropdown']")]
        public IWebElement AddressDropdown;

        [FindsBy(How = How.XPath, Using = "//ul[@class='dropdown-content select-dropdown active']/li[3]")]
        public IWebElement AddressDropdownInput;

        [FindsBy(How = How.XPath, Using = "//crisp-button[1]/span/button[text()='Confirm Selection']")]
        public IWebElement ConfirmSelectionButton;

        [FindsBy(How = How.CssSelector, Using = "crisp-card-actions > div > crisp-action-button > div > a > i")]
        public IWebElement AddQuoteAppDocs;

        [FindsBy(How = How.XPath, Using = "//crisp-input-file/div/div[1]/input")]
        public IWebElement UploadDoc;

        [FindsBy(How = How.XPath, Using = "//crisp-button[1]/span/button[text()='OK']")]
        public IWebElement UploadOkButton;

        [FindsBy(How = How.XPath, Using = "//crisp-input-date/div/label[text()='Received date']")]
        public IWebElement ReceivedDateLabel;

        [FindsBy(How = How.XPath, Using = "//div[@class='picker picker--opened']/div/div/div/div/div[3]/button[1]")]
        public IWebElement ReceivedDateInput;

        [FindsBy(How = How.XPath, Using = "//crisp-footer/nav/div/crisp-button[3]/span/button[text()='Next']")]
        public IWebElement NextButton;

        [FindsBy(How = How.XPath, Using = "//crisp-input-date[1]/div/label[text()='Construction start date']")]
        public IWebElement ConstructionStartDateLabel;

        [FindsBy(How = How.XPath, Using = "//div[@class='picker picker--opened']/div/div/div/div/div[3]/button[text()='Today']")]
        public IWebElement ConstructionStartDateinput;

        [FindsBy(How = How.XPath, Using = "//crisp-input-date/div/label[text()='Construction end date']")]
        public IWebElement ConstructionEndDateLabel;

        [FindsBy(How = How.XPath, Using = "//crisp-input-date[2]/div/input")]
        public IWebElement ConstructionEndDateinput;

        [FindsBy(How = How.CssSelector, Using = "div:nth-child(1) > crisp-input-number:nth-child(3) > div > label")]
        public IWebElement NumberOffStoreysAboveGround;

        [FindsBy(How = How.CssSelector, Using = "div:nth-child(1) > crisp-input-number:nth-child(3) > div > input")]
        public IWebElement NumberOffStoreysAboveGroundInput;

        [FindsBy(How = How.CssSelector, Using = "div:nth-child(1) > crisp-input-number:nth-child(4) > div > label")]
        public IWebElement NumberOffStoreysBelowGround;

        [FindsBy(How = How.CssSelector, Using = "div:nth-child(1) > crisp-input-number:nth-child(4) > div > input")]
        public IWebElement NumberOffStoreysBelowGroundInput;

        [FindsBy(How = How.XPath, Using = "//crisp-content/crisp-input-bool/label[text()='Building control only?']")]
        public IWebElement ProductTypeCheckBox;

        [FindsBy(How = How.XPath, Using = "//crisp-input-radio[@label='Any innovative construction methods?']/div/ul/li/label[text()='Yes']")]
        public IWebElement InnovativeMethodsRadioButton;

        [FindsBy(How = How.XPath, Using = "//crisp-input-radio[@label='Are any units attached or structurally connected to any other structure not included within this application?']/div/ul/li/label[text()='Yes']")]
        public IWebElement NoOfUnitsAttachedRadioButton;

        [FindsBy(How = How.XPath, Using = "//crisp-input-radio[@label='Is site in Administration?']/div/ul/li/label[text()='Yes']")]
        public IWebElement SiteAdminRadioButton;

        [FindsBy(How = How.XPath, Using = "//crisp-input-radio[@label='Will warranty be added to this Building Control quotation at a later date?']/div/ul/li/label[text()='Yes']")]
        public IWebElement WarrantyBuildingControl;

        [FindsBy(How = How.XPath, Using = "//crisp-footer/nav/div/crisp-button[2]/span/button[text()='Save']")]
        public IWebElement SaveButton;

        [FindsBy(How = How.XPath, Using = "//crisp-button[2]/span/button[text()='Ok']")]
        public IWebElement OkButton;

        [FindsBy(How = How.XPath, Using = "//i[@class='au-target fa fa-cog']")]
        public IWebElement AdministrationButton;

        [FindsBy(How = How.XPath, Using = "//a[@click.delegate='subItem.onClick()'][text()='Company Groups']")]
        public IWebElement CompanyGroupsOption;

        [FindsBy(How = How.XPath, Using = "//crisp-list/ul/li[2]/crisp-list-item/div/crisp-list-item-title/div[@class='title']")]
        public IWebElement CompanyGroupResult;

        [FindsBy(How = How.XPath, Using = "//a[@click.delegate='subItem.onClick()'][text()='Manage Company Lists']")]
        public IWebElement ManageCompanyList;

        [FindsBy(How = How.CssSelector, Using = "crisp-nav-bar > a:nth-child(6)")]
        public IWebElement FinancesButton;


        [FindsBy(How = How.XPath, Using = "//ul[@class='dropdown-content au-target active']/li/a[text()='Bank statement']")]
        public IWebElement BankStatementsOption;

        [FindsBy(How = How.XPath, Using = "//ul[@class='dropdown-content au-target active']/li/a[text()='Transfer money']")]
        public IWebElement TransferMoneyOption;

        [FindsBy(How = How.XPath, Using = "//ul[@class='dropdown-content au-target active']/li/a[text()='Transfer money from MDIS']")]
        public IWebElement TransferMoneyFromMDISOption;

        [FindsBy(How = How.XPath, Using = "//router-view[@class='au-target brand default']/h1[contains(text(),'Oh no you')]")]
        public IWebElement NoPermission;

        public static SqlConnection GetDBConnection()
        {
            string datasource = "T-GBBHSRDB03.MDISDEV.local";
            string database = "CRISP-C";
            string username = "KumarS";
            string password = "effe52264d";
            string connString = @"Data Source=" + datasource + ";Initial Catalog=" + database + ";Persist Security Info=True;User ID=" + username + ";Password=" + password;
            SqlConnection conn = new SqlConnection(connString);
            return conn;
        }
        public async Task MainAsyncAddAutoUserandUpdatePassword()
        {
            var envrironmentApiServer = ConfigurationManager.AppSettings["ApiServerUrl"];
            var client = HttpClientHelpers.CreateClient("scott.williams@mdinsurance.co.uk", "Mdis@1234;");
            string body = "{\"permissions\":{\"accounts\":{\"importReceipts\":true,\"allocateMoney\":true,\"manageBordereau\":false},\"internalUsers\":{\"managePermissions\":true,\"create\":true,\"amend\":true,\"view\":true},\"contacts\":{\"setNonStandardPaymentTerms\":true,\"amendCompanyName\":false,\"manage\":true,\"create\":true,\"amend\":true,\"view\":true},\"leads\":{\"create\":true,\"amend\":true,\"view\":true},\"opportunity\":{\"create\":true,\"amend\":true,\"view\":true},\"quote\":{\"accept\":true,\"viewExternalIncomplete\":true,\"reconstructionCostLimit\":0,\"products\":[],\"setSpecialTerms\":false,\"create\":true,\"amend\":true,\"view\":true},\"order\":{\"approveReferral\":true,\"issueCoa\":true,\"issueCoa2\":true,\"issueCoi\":true,\"importReceipts\":true,\"allocateMoney\":true,\"ammendRoles\":true,\"issueDevelopmentIc\":true,\"issuePlotIc\":true,\"issueFinalNotice\":true,\"cancel\":true,\"changePaymentTerms\":false,\"reinstate\":false,\"create\":true,\"amend\":true,\"view\":true},\"manageCompanyLists\":{\"create\":true,\"amend\":true,\"view\":true},\"manageCompanyGroups\":{\"create\":true,\"amend\":true,\"view\":true},\"refurbAssessment\":{\"create\":true,\"amend\":true,\"view\":true},\"siteRiskAssessment\":{\"create\":true,\"amend\":true,\"view\":true},\"inspection\":{\"create\":true,\"amend\":true,\"view\":true},\"designReview\":{\"create\":true,\"amend\":true,\"view\":true},\"engineerReview\":{\"create\":true,\"amend\":true,\"view\":true},\"structuralReview\":{\"create\":true,\"amend\":true,\"view\":true},\"registration\":{\"manage\":true,\"create\":true,\"amend\":true,\"view\":true},\"claim\":{\"create\":true,\"amend\":true,\"view\":true},\"recovery\":{\"create\":true,\"amend\":true,\"view\":true},\"salesAppointments\":{\"create\":true,\"amend\":true,\"view\":true},\"technicalHub\":{\"createContent\":true,\"approveContent\":true,\"publishContent\":true},\"companyBlocking\":{\"create\":true,\"remove\":true},\"managerOverrides\":{\"processes\":[],\"paymentValidation\":[]},\"localAuthorities\":{\"buildingControlAdmin\":true},\"notes\":{\"create\":true,\"amend\":true,\"view\":true},\"endorsements\":{\"create\":true},\"bonds\":{\"manage\":true,\"bondValueLimit\":1000000000},\"ratings\":{\"confirm\":true},\"extranet\":{\"administrator\":true},\"rescind\":{\"rescindDevelopmentIc\":false,\"rescindPlotIc\":false,\"rescindCoverNote\":false,\"rescindCoa\":false,\"rescindCoi\":false,\"rescindInitialNotice\":false,\"rescindFinalNotice\":false,\"rescindBuildingControlTechnicalSignOff\":false,\"create\":false,\"amend\":false,\"view\":false}},\"salutation\":\"Mr\",\"firstname\":\"Auto\",\"surname\":\"User\",\"companyId\":\"60b050bb-73d2-4741-9c84-a7e3c26cd3d5\",\"managerId\":\"84add8b2-d0ae-44ff-af25-b26cbc9e72f7\",\"salesBuddyIds\":[],\"email\":\"auto.user@mdinsurance.co.uk\",\"phoneCountryCode\":214,\"phone\":\"01483793721\",\"primaryOffice\":\"c995818e-906a-4679-9415-cbc6cac4873a\",\"offices\":[\"c995818e-906a-4679-9415-cbc6cac4873a\"],\"fullTimeEquivalence\":1,\"department\":\"Information Technology\",\"team\":\"Test Team\",\"jobTitle\":\"IT User\",\"groups\":[1,2]}";
            var response = await client.PostAsync($"{envrironmentApiServer}/api/InternalAccounts", new StringContent(body, Encoding.Default, "application/json"));
            var acc = JsonConvert.DeserializeObject<Account>(response.Content.ToString());
            var datasource = "T-GBBHSRDB03.MDISDEV.local";
            var database = "CRISP-C";
            var username = "KumarS";
            var password = "effe52264d";
            var newPassword = "clear-Mdis@1234;";
            var sqlQuery = "update[SystemAccounts] set [PasswordHash] = @newPassword;";
            using (
            var connection =
                new SqlConnection(
                    ConfigurationManager.ConnectionStrings[$"Data Source={datasource};Initial Catalog={database};Persist Security Info=True;User ID={username};Password={password}"]
                        .ConnectionString))
            {
                using (var command = new SqlCommand(sqlQuery, connection))
                {
                    command.Parameters.Add("@newPassword", SqlDbType.NVarChar);
                    command.Parameters["@newPassword"].Value = newPassword;
                    command.Parameters.Add("@systemAccountId", SqlDbType.UniqueIdentifier);
                    command.Parameters["@systemAccountId"].Value = acc.Id;
                    connection.Open();
                    command.ExecuteReader();
                    connection.Close();
                }
            }
        }

        public void ApplyUserPermissions()
        {
            //Apply permissions to Crisp internal USer 
            MainAsyncInternalUser().GetAwaiter().GetResult();           
            //Apply permissions to Surveyor 
            MainAsyncSurveyorUser().GetAwaiter().GetResult();
        }
        
        //Apply Permissions to Internal User //Internal userid (Brian Hare)
        public async Task MainAsyncInternalUser()
        {
            var envrironmentApiServer = ConfigurationManager.AppSettings["ApiServerUrl"];
            var client = HttpClientHelpers.CreateClient("scott.williams@mdinsurance.co.uk", "Mdis@1234;");
            string body = "{\"id\":\"f58882c6-d39c-519e-85b7-97447162e05b\",\"permissions\":{\"accounts\":{\"importReceipts\":true,\"allocateMoney\":true,\"manageBordereau\":true},\"internalUsers\":{\"managePermissions\":true,\"create\":true,\"amend\":true,\"view\":true},\"contacts\":{\"setNonStandardPaymentTerms\":true,\"amendCompanyName\":true,\"manage\":true,\"create\":true,\"amend\":true,\"view\":true},\"leads\":{\"create\":true,\"amend\":true,\"view\":true},\"opportunity\":{\"create\":true,\"amend\":true,\"view\":true},\"quote\":{\"accept\":true,\"viewExternalIncomplete\":true,\"reconstructionCostLimit\":25000000,\"products\":[7,6,12,11,3,18,10,5,14,15,1,16,8,13,17,4,2,9,22,23,19],\"setSpecialTerms\":true,\"create\":true,\"amend\":true,\"view\":true},\"order\":{\"approveReferral\":true,\"issueCoa\":true,\"issueCoa2\":true,\"issueCoi\":true,\"importReceipts\":true,\"allocateMoney\":true,\"ammendRoles\":true,\"issueDevelopmentIc\":true,\"issuePlotIc\":true,\"issueFinalNotice\":true,\"cancel\":true,\"changePaymentTerms\":true,\"reinstate\":true,\"create\":true,\"amend\":true,\"view\":true},\"manageCompanyLists\":{\"create\":true,\"amend\":true,\"view\":true},\"manageCompanyGroups\":{\"create\":true,\"amend\":true,\"view\":true},\"refurbAssessment\":{\"create\":true,\"amend\":true,\"view\":true},\"siteRiskAssessment\":{\"create\":true,\"amend\":true,\"view\":true},\"inspection\":{\"create\":true,\"amend\":true,\"view\":true},\"designReview\":{\"create\":true,\"amend\":true,\"view\":true},\"engineerReview\":{\"create\":true,\"amend\":true,\"view\":true},\"structuralReview\":{\"create\":true,\"amend\":true,\"view\":true},\"registration\":{\"manage\":true,\"create\":true,\"amend\":true,\"view\":true},\"claim\":{\"create\":true,\"amend\":true,\"view\":true},\"recovery\":{\"create\":true,\"amend\":true,\"view\":true},\"salesAppointments\":{\"create\":true,\"amend\":true,\"view\":true},\"technicalHub\":{\"createContent\":true,\"approveContent\":true,\"publishContent\":true},\"companyBlocking\":{\"create\":true,\"remove\":true},\"managerOverrides\":{\"processes\":[19,6,7,23,9,13,8,11,5,18,24,3,4,25,1,2,20,27],\"paymentValidation\":[3,4,23,13,11]},\"localAuthorities\":{\"buildingControlAdmin\":true},\"notes\":{\"create\":true,\"amend\":true,\"view\":true},\"endorsements\":{\"create\":true},\"bonds\":{\"manage\":true,\"bondValueLimit\":25000000},\"ratings\":{\"confirm\":true},\"extranet\":{\"administrator\":true},\"rescind\":{\"rescindDevelopmentIc\":true,\"rescindPlotIc\":true,\"rescindCoverNote\":true,\"rescindCoa\":true,\"rescindCoi\":true,\"rescindInitialNotice\":true,\"rescindFinalNotice\":true,\"rescindBuildingControlTechnicalSignOff\":true,\"create\":true,\"amend\":true,\"view\":true}},\"salutation\":\"Mr\",\"firstname\":\"Brian\",\"surname\":\"Hare\",\"companyId\":\"60b050bb-73d2-4741-9c84-a7e3c26cd3d5\",\"managerId\":\"d941a603-06ca-45bb-9652-6dee249413fb\",\"salesBuddyIds\":[\"84add8b2-d0ae-44ff-af25-b26cbc9e72f7\"],\"twitterAddress\":null,\"faceBookAddress\":null,\"linkedInAddress\":null,\"googlePlusAddress\":null,\"email\":\"brian.hare@mdinsurance.co.uk\",\"phoneCountryCode\":214,\"phone\":\"07777777777\",\"mobileCountryCode\":0,\"mobile\":null,\"websites\":[],\"addresses\":[],\"primaryOffice\":\"c995818e-906a-4679-9415-cbc6cac4873a\",\"offices\":[\"c995818e-906a-4679-9415-cbc6cac4873a\"],\"isCompanyDirector\":true,\"isPurchasingDecisionMaker\":true,\"extension\":\"417\",\"suffix\":null,\"isContractor\":true,\"fullTimeEquivalence\":1,\"department\":\"IT\",\"team\":\"BIS 2\",\"jobTitle\":\"IT Tester\",\"groups\":[500,1000,1,2,200,250,600,601,602,607,606,605,604,603,608,651,652,653,650,700,701,702,703,704,705,706,400,401,300,500,800,3,900,901,1000,1001],\"remoteWorkerPrimaryOperatingLocation\":\"CH41 1AU\"}";
            var response = await client.PostAsync($"{envrironmentApiServer}/api/InternalAccounts", new StringContent(body, Encoding.Default, "application/json"));
        }
        //Apply permissions to Surveyor //Surveyor userid (Peter King)
        public async Task MainAsyncSurveyorUser()
        {
            var envrironmentApiServer = ConfigurationManager.AppSettings["ApiServerUrl"];
            var client = HttpClientHelpers.CreateClient("scott.williams@mdinsurance.co.uk", "Mdis@1234;");
            string body = "{\"id\":\"460ef137-1d07-59c0-96d9-ede704ed5b86\",\"permissions\":{\"accounts\":{\"importReceipts\":true,\"allocateMoney\":true,\"manageBordereau\":true},\"internalUsers\":{\"managePermissions\":true,\"create\":true,\"amend\":true,\"view\":true},\"contacts\":{\"setNonStandardPaymentTerms\":true,\"amendCompanyName\":true,\"manage\":true,\"create\":true,\"amend\":true,\"view\":true},\"leads\":{\"create\":true,\"amend\":true,\"view\":true},\"opportunity\":{\"create\":true,\"amend\":true,\"view\":true},\"quote\":{\"accept\":true,\"viewExternalIncomplete\":true,\"reconstructionCostLimit\":10000000,\"products\":[7,6,12,11,3,18,10,5,14,15,1,16,8,13,17,4,2,9,22,23,19],\"setSpecialTerms\":true,\"create\":true,\"amend\":true,\"view\":true},\"order\":{\"approveReferral\":true,\"issueCoa\":true,\"issueCoa2\":true,\"issueCoi\":true,\"importReceipts\":true,\"allocateMoney\":true,\"ammendRoles\":true,\"issueDevelopmentIc\":true,\"issuePlotIc\":true,\"issueFinalNotice\":true,\"cancel\":true,\"changePaymentTerms\":true,\"reinstate\":true,\"create\":true,\"amend\":true,\"view\":true},\"manageCompanyLists\":{\"create\":true,\"amend\":true,\"view\":true},\"manageCompanyGroups\":{\"create\":true,\"amend\":true,\"view\":true},\"refurbAssessment\":{\"create\":true,\"amend\":true,\"view\":true},\"siteRiskAssessment\":{\"create\":true,\"amend\":true,\"view\":true},\"inspection\":{\"create\":true,\"amend\":true,\"view\":true},\"designReview\":{\"create\":true,\"amend\":true,\"view\":true},\"engineerReview\":{\"create\":true,\"amend\":true,\"view\":true},\"structuralReview\":{\"create\":true,\"amend\":true,\"view\":true},\"registration\":{\"manage\":true,\"create\":true,\"amend\":true,\"view\":true},\"claim\":{\"create\":true,\"amend\":true,\"view\":true},\"recovery\":{\"create\":true,\"amend\":true,\"view\":true},\"salesAppointments\":{\"create\":true,\"amend\":true,\"view\":true},\"technicalHub\":{\"createContent\":true,\"approveContent\":true,\"publishContent\":true},\"companyBlocking\":{\"create\":true,\"remove\":true},\"managerOverrides\":{\"processes\":[],\"paymentValidation\":[]},\"localAuthorities\":{\"buildingControlAdmin\":true},\"notes\":{\"create\":true,\"amend\":true,\"view\":true},\"endorsements\":{\"create\":true},\"bonds\":{\"manage\":true,\"bondValueLimit\":1000000000},\"ratings\":{\"confirm\":true},\"extranet\":{\"administrator\":true},\"rescind\":{\"rescindDevelopmentIc\":true,\"rescindPlotIc\":true,\"rescindCoverNote\":true,\"rescindCoa\":true,\"rescindCoi\":true,\"rescindInitialNotice\":true,\"rescindFinalNotice\":true,\"rescindBuildingControlTechnicalSignOff\":true,\"create\":true,\"amend\":true,\"view\":true}},\"salutation\":\"Mr\",\"firstname\":\"Peter\",\"surname\":\"King\",\"companyId\":\"2812c13b-ba88-4a81-b766-4979ed1c6960\",\"managerId\":\"8fbdd0d4-a6b0-4e14-8f19-135caa0089bb\",\"salesBuddyIds\":[],\"twitterAddress\":null,\"faceBookAddress\":null,\"linkedInAddress\":null,\"googlePlusAddress\":null,\"email\":\"peter.king@mdinsurance.co.uk\",\"phoneCountryCode\":214,\"phone\":\"07585703094\",\"mobileCountryCode\":0,\"mobile\":null,\"websites\":[],\"addresses\":[],\"primaryOffice\":\"256c7989-0682-4f46-a63e-a259a8437879\",\"offices\":[\"256c7989-0682-4f46-a63e-a259a8437879\"],\"isCompanyDirector\":true,\"isPurchasingDecisionMaker\":true,\"extension\":\"\",\"suffix\":null,\"isContractor\":true,\"fullTimeEquivalence\":1,\"department\":\"North Region\",\"team\":\"East Midlands\",\"jobTitle\":\"Surveying Inspector\",\"groups\":[500,1000,1,2,200,250,600,601,602,607,606,605,604,603,608,651,652,653,650,700,701,702,703,704,705,706,400,401,300,500,800,3,900,1000,1001],\"remoteWorkerPrimaryOperatingLocation\":\"IP33 2BN\"}";
            var response = await client.PostAsync($"{envrironmentApiServer}/api/InternalAccounts", new StringContent(body, Encoding.Default, "application/json"));
        }




        public void CreateMethod()
        {
            // Try to add person when user does not permissions
            try
            {
                PersonButton.Click();
                //Thread.Sleep(500);
                Assert.IsFalse(AddWizard.Displayed);
            }
            catch
            {
                Console.WriteLine("User do not have permissions to Create Person");

            }
            // Try to add company when user does not permissions
            try
            {
                //Thread.Sleep(500);
                PlusButton.Click();
                //Thread.Sleep(500);
                CompanyButton.Click();
                Assert.IsFalse(AddWizard.Displayed);
            }
            catch
            {
                Console.WriteLine("User do not have permissions to Create Company");
            }
            //Try to add lead when user does not permissions
            try
            {
                PlusButton.Click();
                //Thread.Sleep(500);
                LeadButton.Click();
                //Thread.Sleep(500);

                if (PermissionsWizard.Displayed)
                {
                    LeadOkButton.Click();
                    //Thread.Sleep(500);
                }
                Assert.IsFalse(AddWizard.Displayed);
            }
            catch
            {
                Console.WriteLine("User do not have permissions to Create Lead");

            }


            // Try to add ManageCompanyList when user does not permissions
            try
            {
                AdministrationButton.Click();
                //Thread.Sleep(500);
                ManageCompanyList.Click();
                //Thread.Sleep(500);
                Assert.IsFalse(AddWizard.Displayed);
            }
            catch
            {
                Console.WriteLine("User do not have permissions to Manage Company List");

            }

            // Try to add Bank Statements From Finance when user does not permissions
            try
            {
                FinancesButton.Click();
                //Thread.Sleep(500);
                BankStatementsOption.Click();
                //Thread.Sleep(500);
                Assert.IsFalse(AddWizard.Displayed);
            }
            catch
            {
                Console.WriteLine("User do not have permissions to Bank Statements");

            }

            // Try to add Transfer Money From Finance when user does not permissions
            try
            {
                FinancesButton.Click();
                //Thread.Sleep(500);
                TransferMoneyOption.Click();
                //Thread.Sleep(500);
                Assert.IsFalse(AddWizard.Displayed);
            }
            catch
            {
                Console.WriteLine("User do not have permissions to Transfer Money");

            }
            // Try to add Transfer Money From MDIS  when user does not permissions
            try
            {
                FinancesButton.Click();
                //Thread.Sleep(500);
                TransferMoneyFromMDISOption.Click();
                //Thread.Sleep(500);
                Assert.IsFalse(AddWizard.Displayed);
            }
            catch
            {
                Console.WriteLine("User do not have permissions to Transfer Money From MDIS");

            }
        }

        public void EditMethod()
        {
            // Try to edit person when user does not permissions
            try
            {
                IncludeCompaniesCheckbox.Click();
                IncludeEmployeesCheckbox.Click();
                NameField.Click();
                EnterName.SendKeys("Sunilkumar");
                //Thread.Sleep(1000);
                SelectContact.Click();
                //Thread.Sleep(1000);
                EditPersonButton.Click();
                //Thread.Sleep(1000);
                Assert.IsFalse(AddWizard.Displayed);
            }
            catch
            {
                Console.WriteLine("User do not have permissions to Edit Person Details");

            }
            // Try to edit company when user does not permissions
            try
            {
                SearchOption.Click();
                //Thread.Sleep(500);
                ContactOption.Click();
                IncludePeopleCheckbox.Click();
                IncludeEmployeesCheckbox.Click();
                NameField.Click();
                EnterName.SendKeys("MDIS");
                //Thread.Sleep(1000);
                SelectContact.Click();
                //Thread.Sleep(1000);
                EditCompanyButton.Click();
                //Thread.Sleep(1000);
                Assert.IsFalse(AddWizard.Displayed);
            }
            catch
            {
                Console.WriteLine("User do not have permissions to Edit Company Details");
            }

            // Try to edit employee when user does not permissions
            try
            {
                SearchOption.Click();
                //Thread.Sleep(500);
                ContactOption.Click();
                IncludeCompaniesCheckbox.Click();
                IncludePeopleCheckbox.Click();
                NameField.Click();
                EnterName.SendKeys("Sunilkumar");
                //Thread.Sleep(1000);
                SelectContact.Click();
                //Thread.Sleep(1000);
                EditCompanyButton.Click();
                //Thread.Sleep(1000);
                Assert.IsFalse(AddWizard.Displayed);
            }
            catch
            {
                Console.WriteLine("User do not have permissions to Edit Employee Details");

            }

            // Try to edit PGLead when user does not permissions
            try
            {
                SearchOption.Click();
                //Thread.Sleep(500);
                SiteOption.Click();
                //Thread.Sleep(1000);
                IncludeQuotes.Click();
                //Thread.Sleep(1000);
                IncludeOrders.Click();
                //Thread.Sleep(1000);
                FilterLABCcheckBox.Click();
                //Thread.Sleep(1000);
                PGFilterOption.Click();
                //Thread.Sleep(1000);
                Assert.IsFalse(EditLeadButton.Displayed);
            }
            catch
            {
                Console.WriteLine("User do not have permissions to Edit PG Lead Details");

            }
            // Try to edit LABCLead when user does not permissions
            try
            {
                SearchOption.Click();
                //Thread.Sleep(500);
                SiteOption.Click();
                //Thread.Sleep(1000);
                IncludeQuotes.Click();
                //Thread.Sleep(1000);
                IncludeOrders.Click();
                //Thread.Sleep(1000);
                FilterPGcheckBox.Click();
                //Thread.Sleep(1000);
                LABCFilterOption.Click();
                //Thread.Sleep(1000);
                Assert.IsFalse(EditLeadButton.Displayed);
            }
            catch
            {
                Console.WriteLine("User do not have permissions to Edit LABC Lead Details");

            }

        }
    }
}
