﻿using OpenQA.Selenium;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.PageObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CrispAutomation.Permissions
{
    public class CompanyBlockingPermissionsPage : Support.Pages
    {
        public IWebDriver wdriver;
        public CompanyBlockingPermissionsPage(ISearchContext driver)
        {
            PageFactory.InitElements(driver, this);
            wdriver = (RemoteWebDriver)driver;
        }
        public bool NoPermissions { get; set; }
        public bool CreateCompanyBlockingPermission { get; set; }
        public bool RemoveCompanyBlockingPermission { get; set; }
    }
}
