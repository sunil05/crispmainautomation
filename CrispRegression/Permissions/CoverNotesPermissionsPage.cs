﻿using OpenQA.Selenium;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.PageObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CrispAutomation.Permissions
{
    public class CoverNotesPermissionsPage : Support.Pages
    {
        public IWebDriver wdriver;
        public CoverNotesPermissionsPage(ISearchContext driver)
        {
            PageFactory.InitElements(driver, this);
            wdriver = (RemoteWebDriver)driver;
        }
        public bool NoPermissions { get; set; }
        public bool CreateCoverNotePermission { get; set; }
        public bool AmendCoverNotePermission { get; set; }
        public bool ViewCoverNotePermission { get; set; }
    }
}
