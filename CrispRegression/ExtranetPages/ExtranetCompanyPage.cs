﻿using CrispAutomation.Features;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.PageObjects;
using Protractor;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using static CrispAutomation.Support.ExtensionMethods;

namespace CrispAutomation.ExtranetPages
{
    public class ExtranetCompanyPage : Support.Pages
    {
        public IWebDriver wdriver;
        public NgWebDriver ngDriver;
        public ExtranetCompanyPage(ISearchContext driver)
        {
            PageFactory.InitElements(driver, this);
            wdriver = (RemoteWebDriver)driver;
        }
        //Extranet Office Page
        [FindsBy(How = How.Id, Using = "offices")]
        public IWebElement OfficeTable;

        [FindsBy(How = How.XPath, Using = "//section[@class='content']//div[@class='header container-header']//h2//span[contains(@class,'companyName')]")]
        public IWebElement CompanyName;

        [FindsBy(How = How.XPath, Using = "//section[@class='content']//div[@class='header container-header']//h2//span[contains(text(),'Reg No')]")]
        public IWebElement RegNo;

        [FindsBy(How = How.XPath, Using = "//section[@class='content']//div[@class='header container-header']//h2//span[contains(text(),'Offices')]")]
        public IWebElement Offices;

        [FindsBy(How = How.XPath, Using = "//section[@class='content']//div[@class='header container-header']//h2//span[contains(text(),'Employees')]")]
        public IWebElement Employees;

        [FindsBy(How = How.XPath, Using = "//section[@class='content']//div[@class='header container-header']//h2//span[contains(text(),'Employees')]//span[@id='company-employee-count']")]
        public IWebElement EmployeesCount;

        [FindsBy(How = How.XPath, Using = "//div//a[contains(@class,'new-office-btn')]")]
        public IWebElement AddNewOfficeButton;

        [FindsBy(How = How.XPath, Using = "//div[@id='addOfficeModal']//h5/span[text()='Add New Office']")]
        public IWebElement NewOfficeWindow;

        [FindsBy(How = How.XPath, Using = "//table[@id='offices']//tbody//tr[@role='row']//td//span[contains(@id,'office-name')]")]
        public IList<IWebElement> OfficeNames;

        [FindsBy(How = How.XPath, Using = "//table[@id='offices']//tbody//tr[@role='row']//td//span[contains(@id,'office-address')]")]
        public IList<IWebElement> OfficeAddress;

        [FindsBy(How = How.XPath, Using = "//table[@id='offices']//tbody//tr[@role='row']//td//span[contains(@id,'office-employee')]")]
        public IList<IWebElement> OfficeEmployees;

        [FindsBy(How = How.XPath, Using = "//table[@id='offices']//tbody//tr[@role='row']//td//div//i[contains(@id,'expand')][text()='expand_more']")]
        public IList<IWebElement> OfficeExpandMore;

        [FindsBy(How = How.XPath, Using = "//table[@id='offices']//tbody//tr[@role='row']//td//div//i[contains(@id,'expand')][text()='expand_less']")]
        public IList<IWebElement> OfficeExpandLess;
       
        [FindsBy(How = How.XPath, Using = "//table[@id='offices']//tbody//tr[contains(@id,'office-details')]//div[contains(@class,'office-container')]//div//ul[contains(@class,'company-office')]")]
        public IWebElement OfficeContainer;

        [FindsBy(How = How.XPath, Using = "//table[@id='offices']//tbody//tr[contains(@id,'office-details')]//div[contains(@class,'office-container')]//div//div[contains(@class,'edit-employee')]//a[contains(text(),'Edit Office')]")]
        public IList<IWebElement> EditOfficeButton;

        [FindsBy(How = How.XPath, Using = "//div[@id='addOfficeModal']//h5/span[text()='Edit Office']")]
        public IWebElement EditOfficeWindow;        

        [FindsBy(How = How.XPath, Using = "//table[@id='offices'][1]/tbody/tr[@role='row']")]
        public IList<IWebElement> OfficeRows;

        [FindsBy(How = How.XPath, Using = "//div[contains(@class,'company-main-container')]//div[contains(@class,'container-header')]//h2//span[3][contains(text(),'Offices')]")]
        public IWebElement OfficeCount;        

        [FindsBy(How = How.XPath, Using = "//div[@id='offices_paginate']//ul[@class='pagination']//li//a")]
        public IList<IWebElement> OfficePaginationRows;

        [FindsBy(How = How.XPath, Using = "//div[@id='offices_paginate']//ul[@class='pagination']//li[@id='offices_next']//a")]
        public IWebElement OfficePaginateNextButton;

        [FindsBy(How = How.XPath, Using = "//div[@id='offices_paginate']//ul[@class='pagination']//li[contains(@class,'paginate_button next')]//preceding-sibling::li[1]//a")]
        public IWebElement OfficePaginateLastButton;

        [FindsBy(How = How.XPath, Using = "//div[@id='offices_paginate']//ul[@class='pagination']//li[@id='offices_next']//a")]
        public IWebElement EditOffice;

        //Add New Office Details 

        [FindsBy(How = How.XPath, Using = "//div//input[@id='office-isdefault']//following-sibling::label[1]")]
        public IWebElement CorrespondenceAddress;

        [FindsBy(How = How.XPath, Using = "//div//a[@id='no-office-btn']")]
        public IWebElement CorrespondenceConfirmationNoButton;

        [FindsBy(How = How.XPath, Using = "//div//a[@id='yes-office-btn']")]
        public IWebElement CorrespondenceConfirmationYesButton;

        [FindsBy(How = How.XPath, Using = "//div[@name='yes-no-postcode-lookup'][1]//label[1]")]
        public IWebElement PostcodeLookupYesButton;

        [FindsBy(How = How.XPath, Using = "//button[@title='Select and Begin Typing']")]
        public IWebElement PostCodeInputbutton;

        [FindsBy(How = How.XPath, Using = "//div[@class='dropdown-menu open']//input[contains(@placeholder,'Search')]")]
        public IWebElement PostCodeInputtext;

        [FindsBy(How = How.XPath, Using = "//button[@data-id='office-autocomplete']//following-sibling::div[@class='dropdown-menu open']//ul[@class='dropdown-menu inner']//li ")]
        public IList<IWebElement> AddressList;

        [FindsBy(How = How.Id, Using = "save-office-btn")]
        public IWebElement SaveOfficeButton;

        [FindsBy(How = How.XPath, Using = "//div[@name='yes-no-postcode-lookup'][1]//label[2]")]
        public IWebElement PostcodeLookupNoButton;

        [FindsBy(How = How.Id, Using = "office-addressline1")]
        public IWebElement OfficeAddressLineOne;

        [FindsBy(How = How.Id, Using = "office-addressline2")]
        public IWebElement OfficeAddressLineTwo;

        [FindsBy(How = How.Id, Using = "office-addressline3")]
        public IWebElement OfficeAddressLineThree;

        [FindsBy(How = How.Id, Using = "office-town")]
        public IWebElement OfficeAddressTown;

        [FindsBy(How = How.Id, Using = "office-postcode")]
        public IWebElement OfficeAddressPostcode;

        [FindsBy(How = How.Id, Using = "office-name")]
        public IWebElement NewOfficeName;

        [FindsBy(How = How.Id, Using = "office-email")]
        public IWebElement NewOfficeEmail;

        [FindsBy(How = How.Id, Using = "office-officenumber")]
        public IWebElement NewOfficePhoneNumber;

        [FindsBy(How = How.XPath, Using = "//div[contains(@id,'offices_info')]")]
        public IWebElement OfficeTableInfo;

        //Employee Elements 

        [FindsBy(How = How.XPath, Using = "//div[contains(@class,'employee-container')]//div[contains(@id,'employees')]")]
        public IWebElement EmployeeContainer;

        [FindsBy(How = How.XPath, Using = "//div[contains(@class,'employee-container')]//div[contains(@id,'employees')]//a[contains(text(),'Add New Employee')]")]
        public IList<IWebElement> AddNewEmployeeButton;

        [FindsBy(How = How.XPath, Using = "//table[contains(@id,'employee-dataTable')]//tbody//tr//td//span[contains(@onclick,'Employee.edit')]")]
        public IList<IWebElement> EditEmployeeButton;        

        [FindsBy(How = How.XPath, Using = "//div[@id='addEmployeeModal']//div[@class='modal-content']")]
        public IWebElement EmployeeDiv;        

        [FindsBy(How = How.XPath, Using = "//div[contains(@class,'employee-container')]//div[contains(@id,'employee-table')]//table[contains(@id,'employee')]//tr//th")]
        public IList<IWebElement> EmployeeTable;

        [FindsBy(How = How.XPath, Using = "//div[contains(@id,'employee-dataTable')]//table[contains(@id,'employee-dataTable')]//tbody//tr")]
        public IList<IWebElement> EmployeeRows;        

        [FindsBy(How = How.XPath, Using = "//div[contains(@id,'employee-dataTable')]//div[@class='dataTables_info']")]
        public IWebElement EmployeeTableInfo;

        [FindsBy(How = How.Id, Using = "employee-title")]
        public IWebElement EmployeeTitle;

        [FindsBy(How = How.Id, Using = "employee-firstname")]
        public IWebElement EmployeeFirstName;

        [FindsBy(How = How.Id, Using = "employee-lastname")]
        public IWebElement EmployeeLastName;

        [FindsBy(How = How.XPath, Using = "//button[@data-id='employee-office']")]
        public IWebElement OfficeAssociatedButton;

        [FindsBy(How = How.XPath, Using = "//div[contains(@class,'form-control open')]//button[@data-id='employee-office']//following-sibling::div[@class='dropdown-menu open']//ul[@class='dropdown-menu inner']//li")]
        public IList<IWebElement> AssociatedOfficesList;        

        [FindsBy(How = How.XPath, Using = "//label[@for='employee-office'][text()='Office(s) Associated to']")]
        public IWebElement OfficeAssociatedText;

        [FindsBy(How = How.XPath, Using = "//div[@id='primary-office-container'][contains(@style,'block')]//button[@data-id='employee-primary-office']")]
        public IList<IWebElement> PrimaryOfficeButton;

        [FindsBy(How = How.XPath, Using = "//div[contains(@class,'form-control')]//button[@data-id='employee-primary-office']//following-sibling::div[@class='dropdown-menu open']//ul[@class='dropdown-menu inner']//li//a")]
        public IList<IWebElement> PrimaryOfficeList;
       
        [FindsBy(How = How.Id, Using = "employee-email")]
        public IWebElement EmployeeEmail;

        [FindsBy(How = How.XPath, Using = "//button[@data-id='employee-mobile-country']")]
        public IWebElement EmployeeMobileCountry;

        [FindsBy(How = How.Id, Using = "employee-mobilenumber")]
        public IWebElement EmployeeMobileNumber;

        [FindsBy(How = How.XPath, Using = "//div[contains(@class,'form-control open')]//following-sibling::div[@class='dropdown-menu open']//ul[@class='dropdown-menu inner']//li//a//span[@class='text'][text()='United Kingdom']")]
        public IWebElement CountryInput;

        [FindsBy(How = How.XPath, Using = "//button[@data-id='employee-officenumber-country']")]
        public IWebElement OfficePhoneCountry;

        [FindsBy(How = How.Id, Using = "employee-officenumber")]
        public IWebElement EmployeePhoneNumber;

        [FindsBy(How = How.XPath, Using = "//div[contains(@class,'extranet-access')]//div[@class='yes-no']//label[@class='no yes-no-label-ext-access']")]
        public IWebElement ExtranetAccessNo;

        [FindsBy(How = How.XPath, Using = "//div[contains(@class,'extranet-access')]//div[@class='yes-no']//label[@class='yes yes-no-label-ext-access']")]
        public IWebElement ExtranetAccessYes;

        [FindsBy(How = How.XPath, Using = "//div[contains(@id,'employee-adminaccess-container')]//div[@id='yes-no-ext-admin-access']//label[@class='no']")]
        public IWebElement ExtranetAdminAccessNo;

        [FindsBy(How = How.XPath, Using = "//div[contains(@id,'employee-adminaccess-container')]//div[@id='yes-no-ext-admin-access']//label[@class='yes']")]
        public IWebElement ExtranetAdminAccessYes;

        [FindsBy(How = How.Id, Using = "save-employee-btn")]
        public IWebElement SaveEmployeeButton;

        public static int LastEntryValue { get; set; }
        public static int FirstEntryValue { get; set; }

        List<string> EmployeeTableColumns = new List<string>();

        public static int officesCount { get; set; }

        public static int employeesCount { get; set; }

        public void SelectCompanyLink()
        {
           // CloseSpinnerOnExtranet();
            WaitForElement(ExtranetHomePage.CompanyLink);
            ExtranetHomePage.CompanyLink.Click();
            CloseSpinnerOnExtranet();
            Assert.IsTrue(OfficeTable.Displayed);
        }

        public void VerifCompanySection()
        {
            SelectCompanyLink();
            CloseSpinnerOnExtranet();
            Thread.Sleep(1000);
            Assert.IsTrue(CompanyName.Displayed);
            Assert.IsTrue(RegNo.Displayed);
            Assert.IsTrue(Offices.Displayed);
            Assert.IsTrue(Employees.Displayed);
            Assert.IsTrue(AddNewOfficeButton.Displayed);
            Assert.IsTrue(OfficeNames.Count>0);
            Assert.IsTrue(OfficeAddress.Count>0);
            Assert.IsTrue(OfficeEmployees.Count>0);            
            Assert.IsTrue(OfficeContainer.Displayed);            
            Assert.IsTrue(EditOfficeButton.Count>0);
            officesCount = OfficeRows.Count;
            int numberOfPages = Convert.ToInt32(OfficePaginationRows.Count());
            if (numberOfPages <= 3)
            {
                WaitForElement(OfficeTableInfo);
                string officeInfo = OfficeTableInfo.Text;
                if (officeInfo != null)
                {
                    GetLastEntryFromString(officeInfo);
                    if (LastEntryValue >= 0)
                    {
                        Assert.AreEqual(LastEntryValue, officesCount);
                    }
                }
            }
            else 
            {
                GetOfficesCount();
            }
        }
        public void GetOfficesCount()
        {
         
            WaitForElements(OfficePaginationRows);
            int numberOfPages = Convert.ToInt32(OfficePaginateLastButton.Text.ToString());
            if (numberOfPages > 1)
            {
                officesCount = OfficeRows.Count;
                for (int i = 0; i < numberOfPages - 1; i++)
                {
                    WaitForElement(OfficePaginateNextButton);
                    OfficePaginateNextButton.Click();
                    WaitForElements(OfficeRows);
                    //Thread.Sleep(500);
                    CloseSpinnerOnExtranet();
                    Thread.Sleep(500);
                    officesCount += OfficeRows.Count;
                    //Thread.Sleep(500);
                    CloseSpinnerOnExtranet();
                    //Thread.Sleep(500);
                }
                WaitForElement(OfficeTableInfo);
                string officeInfo = OfficeTableInfo.Text;
                if (officeInfo != null)
                {
                    GetLastEntryFromString(officeInfo);
                    CloseSpinnerOnExtranet();
                    if (LastEntryValue >= 0)
                    {
                        Assert.AreEqual(LastEntryValue, officesCount);
                    }
                }
            }
            else
            { 
                officesCount = OfficeRows.Count;
                if (officesCount >= 1)
                {
                    WaitForElement(OfficeTableInfo);
                    string officeInfo = OfficeTableInfo.Text;
                    if (officeInfo != null)
                    {
                        GetLastEntryFromString(officeInfo);
                        if (LastEntryValue >= 0)
                        {
                            Assert.AreEqual(LastEntryValue, officesCount);
                        }
                    }
                }
            }
            
        }
        public void VerifyEmployeesSection()
        {
            CloseSpinnerOnExtranet();
            SelectCompanyLink();           
            CloseSpinnerOnExtranet();
            Assert.IsTrue(EmployeeContainer.Displayed);
            Assert.IsTrue(AddNewEmployeeButton.Count>0);
            Assert.IsTrue(EmployeeTable.Count > 0);
            EmployeeTableColumns = EmployeeTable.Select(i => i.Text).ToList();
            Assert.IsTrue(EmployeeTableColumns.Any(o => o.Contains("Name")));
            Assert.IsTrue(EmployeeTableColumns.Any(o => o.Contains("Email Address")));
            Assert.IsTrue(EmployeeTableColumns.Any(o => o.Contains("Telephone")));
            Assert.IsTrue(EmployeeTableColumns.Any(o => o.Contains("Mobile")));
            Assert.IsTrue(EmployeeTableColumns.Any(o => o.Contains("Extranet Status")));
            Assert.IsTrue(EmployeeTableColumns.Any(o => o.Contains("Action")));
            employeesCount = Convert.ToInt32(OfficeEmployees[0].Text.ToString());
            if(employeesCount >= 1)
            {
                WaitForElement(EmployeeTableInfo);
                string empInfo = EmployeeTableInfo.Text;
                if (empInfo != null)
                {
                    GetLastEntryFromString(empInfo);
                    if (LastEntryValue >= 0)
                    {
                        Assert.AreEqual(LastEntryValue, employeesCount);
                    }
                }
            }          
        }
        public int GetLastEntryFromString(string input)
        {
            input = Regex.Replace(input, @"\s+", string.Empty);
            string[] numbers = Regex.Split(input, @"\D+");
            numbers = numbers.Where(x => !string.IsNullOrEmpty(x)).ToArray();
            if (numbers != null)
            {
                LastEntryValue = Convert.ToInt32(numbers.LastOrDefault().ToString());
                FirstEntryValue = Convert.ToInt32(numbers.FirstOrDefault().ToString());
            }
            return LastEntryValue & FirstEntryValue;
        }
        public void AddNewOfficeDetailsFromLookup()
        {
            CloseSpinnerOnExtranet();
            SelectCompanyLink();
            CloseSpinnerOnExtranet();
            Assert.IsTrue(OfficeTable.Displayed);
            WaitForElements(OfficeRows);
            officesCount = OfficeRows.Count;
            AddOfficeAddressFromLookup();
            //WaitForElements(OfficeRows);
            //CloseSpinnerOnExtranet();
            //Thread.Sleep(1000);
            //WaitForElements(OfficeRows);
            //int newOfficeCount  = OfficeRows.Count;
            //CloseSpinnerOnExtranet();
            //Thread.Sleep(1000);
            //WaitForElements(OfficeRows);
            //Assert.IsTrue(officesCount + 1 == newOfficeCount);
        }
        public void AddNewOfficeDetailsManually()
        {
            CloseSpinnerOnExtranet();
            SelectCompanyLink();
            CloseSpinnerOnExtranet();
            Assert.IsTrue(OfficeTable.Displayed);
            WaitForElements(OfficeRows);
            GetOfficesCount();      
            AddOfficeAddressManually();
            WaitForElements(OfficeRows);
            CloseSpinnerOnExtranet();
            Thread.Sleep(500);
            string officeCountText = OfficeCount.Text;
            string newOffice = officeCountText.Replace("OFFICES :", "").ToString();
            string newOfficeText = newOffice.Replace("|", "").ToString();

            int newOfficeCount = Convert.ToInt32(newOfficeText.ToString());   
            Assert.IsTrue(officesCount + 1 == newOfficeCount , "New office count is not updating");
        }
        public void AddOfficeAddressFromLookup()
        { 
            WaitForElement(AddNewOfficeButton);
            WaitForElementToClick(AddNewOfficeButton);
            Scrollup();
            AddNewOfficeButton.Click();
            WaitForElement(CorrespondenceAddress);
            CorrespondenceAddress.Click();
            WaitForElement(CorrespondenceConfirmationNoButton);
            CorrespondenceConfirmationNoButton.Click();
            WaitForElement(PostcodeLookupYesButton);
            PostcodeLookupYesButton.Click();
            WaitForElement(PostCodeInputbutton);
            PostCodeInputbutton.Click();
            WaitForElement(PostCodeInputtext);
            PostCodeInputtext.Click();
            PostCodeInputtext.Clear();
            PostCodeInputtext.SendKeys("Ch41 1AU");
            WaitForElements(AddressList);
            if (AddressList.Count >= 1)
            {
                AddressList[0].Click();
            }
            WaitForElement(NewOfficeEmail);
            NewOfficeEmail.Click();
            NewOfficeEmail.Clear();
            NewOfficeEmail.SendKeys(ConfigurationManager.AppSettings["ExtranetEmail"]);
            WaitForElement(NewOfficePhoneNumber);
            NewOfficePhoneNumber.Click();
            NewOfficePhoneNumber.Clear();
            NewOfficePhoneNumber.SendKeys("03300241158");
            WaitForElement(SaveOfficeButton);
            SaveOfficeButton.Click();
            int newOfficesCount = OfficeRows.Count;
        }
        public void AddOfficeAddressManually()
        {           
            WaitForElement(AddNewOfficeButton);
            WaitForElementToClick(AddNewOfficeButton);
            Scrollup();
            AddNewOfficeButton.Click();
            WaitForElement(CorrespondenceAddress);
            CorrespondenceAddress.Click();
            WaitForElement(CorrespondenceConfirmationNoButton);
            CorrespondenceConfirmationNoButton.Click();
            WaitForElement(PostcodeLookupNoButton);
            PostcodeLookupNoButton.Click();
            WaitForElement(OfficeAddressLineOne);
            OfficeAddressLineOne.Click();
            OfficeAddressLineOne.Clear();
            OfficeAddressLineOne.SendKeys("M D Insurance Services Ltd");
            WaitForElement(OfficeAddressLineTwo);
            OfficeAddressLineTwo.Click();
            OfficeAddressLineTwo.Clear();
            OfficeAddressLineTwo.SendKeys("2 Shore Lines Building");
            WaitForElement(OfficeAddressLineThree);
            OfficeAddressLineThree.Click();
            OfficeAddressLineThree.Clear();
            OfficeAddressLineThree.SendKeys("Shore Road");
            WaitForElement(OfficeAddressTown);
            OfficeAddressTown.Click();
            OfficeAddressTown.Clear();
            OfficeAddressTown.SendKeys("Merseyside");
            WaitForElement(OfficeAddressPostcode);
            OfficeAddressPostcode.Click();
            OfficeAddressPostcode.Clear();
            OfficeAddressPostcode.SendKeys("CH41 1AU");
            WaitForElement(NewOfficeName);
            NewOfficeName.Click();
            NewOfficeName.Clear();
            NewOfficeName.SendKeys("AutoTestOffice");
            WaitForElement(NewOfficeEmail);
            NewOfficeEmail.Click();
            NewOfficeEmail.Clear();
            NewOfficeEmail.SendKeys(ConfigurationManager.AppSettings["ExtranetEmail"]);
            WaitForElement(NewOfficePhoneNumber);
            NewOfficePhoneNumber.Click();
            NewOfficePhoneNumber.Clear();
            NewOfficePhoneNumber.SendKeys("03300241158");
            WaitForElement(SaveOfficeButton);
            SaveOfficeButton.Click();
        }
        public void EditOfficeDetailsManually()
        {
            CloseSpinnerOnExtranet();
            SelectCompanyLink();
            CloseSpinnerOnExtranet();
            Assert.IsTrue(OfficeTable.Displayed);
            officesCount = OfficeRows.Count;
            if(officesCount > 1)
            {
                WaitForElements(OfficeExpandMore);
                foreach(var expandOffice in OfficeExpandMore)
                {
                    expandOffice.Click();
                    //Thread.Sleep(500);
                    CloseSpinnerOnExtranet();
                    //Thread.Sleep(500);
                }
                Scrollup();
                WaitForElements(EditOfficeButton);
                if (EditOfficeButton.Count > 0)
                {
                    CloseSpinnerOnExtranet();
                    Thread.Sleep(500);
                    WaitForElementOnExtranet(EditOfficeButton[1]);
                    EditOfficeButton[1].Click();
                    WaitForElement(EditOfficeWindow);
                    Assert.IsTrue(EditOfficeWindow.Displayed);
                    WaitForElement(OfficeAddressLineOne);
                    OfficeAddressLineOne.Click();
                    OfficeAddressLineOne.Clear();
                    OfficeAddressLineOne.SendKeys("Apartment 2");
                    WaitForElement(OfficeAddressLineTwo);
                    OfficeAddressLineTwo.Click();
                    OfficeAddressLineTwo.Clear();
                    OfficeAddressLineTwo.SendKeys("Hamilton Plaza");
                    WaitForElement(OfficeAddressLineThree);
                    OfficeAddressLineThree.Click();
                    OfficeAddressLineThree.Clear();
                    OfficeAddressLineThree.SendKeys("Duncan Street");
                    WaitForElement(OfficeAddressTown);
                    OfficeAddressTown.Click();
                    OfficeAddressTown.Clear();
                    OfficeAddressTown.SendKeys("Merseyside");
                    WaitForElement(OfficeAddressPostcode);
                    OfficeAddressPostcode.Click();
                    OfficeAddressPostcode.Clear();
                    OfficeAddressPostcode.SendKeys("CH41 5EY");
                    WaitForElement(NewOfficeName);
                    NewOfficeName.Click();
                    NewOfficeName.Clear();
                    NewOfficeName.SendKeys("AutoTestAmendedOffice");
                    WaitForElement(NewOfficeEmail);
                    NewOfficeEmail.Click();
                    NewOfficeEmail.Clear();
                    NewOfficeEmail.SendKeys(ConfigurationManager.AppSettings["ExtranetEmail"]);
                    WaitForElement(NewOfficePhoneNumber);
                    NewOfficePhoneNumber.Click();
                    NewOfficePhoneNumber.Clear();
                    NewOfficePhoneNumber.SendKeys("03300242269");
                    WaitForElement(SaveOfficeButton);
                    SaveOfficeButton.Click();
                }
            }
        }
        public void EditOfficeDetailsFromLookup()
        {
            CloseSpinnerOnExtranet();
            SelectCompanyLink();
            CloseSpinnerOnExtranet();
            Assert.IsTrue(OfficeTable.Displayed);
            officesCount = OfficeRows.Count;
            if (officesCount > 1)
            {
                WaitForElements(OfficeExpandMore);
                foreach (var expandOffice in OfficeExpandMore)
                {
                    WaitForElement(expandOffice);
                    expandOffice.Click();
                    Thread.Sleep(500);
                    CloseSpinnerOnExtranet();
                    Thread.Sleep(500);
                }
                Scrollup();         
                WaitForElements(EditOfficeButton);
                if (EditOfficeButton.Count > 0)
                {
                    CloseSpinnerOnExtranet();
                    Thread.Sleep(500);
                    WaitForElementOnExtranet(EditOfficeButton[1]);
                    EditOfficeButton[1].Click();
                    WaitForElement(EditOfficeWindow);
                    Assert.IsTrue(EditOfficeWindow.Displayed);
                    WaitForElement(PostcodeLookupYesButton);
                    PostcodeLookupYesButton.Click();
                    WaitForElement(PostCodeInputbutton);
                    PostCodeInputbutton.Click();
                    WaitForElement(PostCodeInputtext);
                    PostCodeInputtext.Click();
                    PostCodeInputtext.Clear();
                    PostCodeInputtext.SendKeys("Ch41 1AU");
                    WaitForElements(AddressList);
                    if (AddressList.Count >= 1)
                    {
                        AddressList[0].Click();
                    }
                    WaitForElement(NewOfficeEmail);
                    NewOfficeEmail.Click();
                    NewOfficeEmail.Clear();
                    NewOfficeEmail.SendKeys(ConfigurationManager.AppSettings["ExtranetEmail"]);
                    WaitForElement(NewOfficePhoneNumber);
                    NewOfficePhoneNumber.Click();
                    NewOfficePhoneNumber.Clear();
                    NewOfficePhoneNumber.SendKeys("03300241158");
                    WaitForElement(SaveOfficeButton);
                    SaveOfficeButton.Click();                  
                }
            }
        }
        public void VerifyCorrespondenceOffice()
        {
            string newOfficeName = "TestingCorrespondenceOffice";
            WaitForElement(AddNewOfficeButton);
            WaitForElementToClick(AddNewOfficeButton);
            Scrollup();
            AddNewOfficeButton.Click();
            WaitForElement(CorrespondenceAddress);
            CorrespondenceAddress.Click();
            WaitForElement(CorrespondenceConfirmationYesButton);
            CorrespondenceConfirmationYesButton.Click();
            WaitForElement(PostcodeLookupNoButton);
            PostcodeLookupNoButton.Click();
            WaitForElement(OfficeAddressLineOne);
            OfficeAddressLineOne.Click();
            OfficeAddressLineOne.Clear();
            OfficeAddressLineOne.SendKeys("M D Insurance Services Ltd");
            WaitForElement(OfficeAddressLineTwo);
            OfficeAddressLineTwo.Click();
            OfficeAddressLineTwo.Clear();
            OfficeAddressLineTwo.SendKeys("2 Shore Lines Building");
            WaitForElement(OfficeAddressLineThree);
            OfficeAddressLineThree.Click();
            OfficeAddressLineThree.Clear();
            OfficeAddressLineThree.SendKeys("Shore Road");
            WaitForElement(OfficeAddressTown);
            OfficeAddressTown.Click();
            OfficeAddressTown.Clear();
            OfficeAddressTown.SendKeys("Merseyside");
            WaitForElement(OfficeAddressPostcode);
            OfficeAddressPostcode.Click();
            OfficeAddressPostcode.Clear();
            OfficeAddressPostcode.SendKeys("CH411AU");
            WaitForElement(NewOfficeName);
            NewOfficeName.Click();
            NewOfficeName.Clear();
            NewOfficeName.SendKeys(newOfficeName);
            WaitForElement(NewOfficeEmail);
            NewOfficeEmail.Click();
            NewOfficeEmail.Clear();
            NewOfficeEmail.SendKeys(ConfigurationManager.AppSettings["ExtranetEmail"]);
            WaitForElement(NewOfficePhoneNumber);
            NewOfficePhoneNumber.Click();
            NewOfficePhoneNumber.Clear();
            NewOfficePhoneNumber.SendKeys("03300241158");
            WaitForElement(SaveOfficeButton);
            SaveOfficeButton.Click();
            WaitForLoadElements(OfficeExpandLess);
            if(OfficeExpandLess.Count>0)
            {
                foreach (var expandOffice in OfficeExpandLess)
                {
                    expandOffice.Click();
                    //Thread.Sleep(500);
                    CloseSpinnerOnExtranet();
                    //Thread.Sleep(500);
                    WaitForElements(OfficeRows);
                    if(OfficeRows.Count>0)
                    {
                        WaitForElements(OfficeNames);
                        Assert.IsTrue(OfficeNames[0].Text.Contains(newOfficeName));
                    }
                }               
            }
        }
        public void AddNewEmployeesection()
        {
            CloseSpinnerOnExtranet();
            SelectCompanyLink();
            CloseSpinnerOnExtranet();
            Assert.IsTrue(OfficeTable.Displayed);
            WaitForElements(OfficeEmployees);
            employeesCount  = Convert.ToInt32(OfficeEmployees[0].Text.ToString());           
            AddNewEmployeeDetails();
            CloseSpinnerOnExtranet();
            Thread.Sleep(500); 
            WaitForElement(EmployeeTableInfo);           
            string empInfo = EmployeeTableInfo.Text;
            if (empInfo != null)
            {
                GetLastEntryFromString(empInfo);
                if (LastEntryValue >= 0)
                {
                    Assert.AreEqual(LastEntryValue, employeesCount+1, "Failed to update the count for new added employee");
                }
            }
           
        }
        public void EditEmployeesection()
        {
            CloseSpinnerOnExtranet();
            SelectCompanyLink();
            CloseSpinnerOnExtranet();
            Assert.IsTrue(OfficeTable.Displayed);
            WaitForElements(OfficeEmployees);
            employeesCount = Convert.ToInt32(OfficeEmployees[0].Text.ToString());
            EditEmployeeDetails();
            CloseSpinnerOnExtranet();
            Thread.Sleep(500);
            CloseSpinnerOnExtranet();
            Thread.Sleep(500);
            WaitForElement(EmployeeTableInfo);
            string empInfo = EmployeeTableInfo.Text;
            if (empInfo != null)
            {
                GetLastEntryFromString(empInfo);
                if (LastEntryValue >= 0)
                {
                    Assert.AreEqual(LastEntryValue, employeesCount, "Failed to update the details for employee");
                }
            }
        }
        public void AddNewEmployeeDetails()
        {
            WaitForElements(AddNewEmployeeButton);
            AddNewEmployeeButton[0].Click();
            WaitForElement(EmployeeDiv);
            Assert.IsTrue(EmployeeDiv.Displayed);
            WaitForElement(EmployeeTitle);
            EmployeeTitle.Click();
            EmployeeTitle.Clear();
            EmployeeTitle.SendKeys("Mr");
            WaitForElement(EmployeeFirstName);
            EmployeeFirstName.Click();
            EmployeeFirstName.Clear();
            EmployeeFirstName.SendKeys("Sunil");
            WaitForElement(EmployeeLastName);
            EmployeeLastName.Click();
            EmployeeLastName.Clear();
            EmployeeLastName.SendKeys("Sunil");
            WaitForElement(OfficeAssociatedButton);
            OfficeAssociatedButton.Click();
            WaitForElements(AssociatedOfficesList);
            if(AssociatedOfficesList.Count>0)
            {
                List<string> temp = new List<string>();
                for (int i = 0; i < AssociatedOfficesList.Count; i++)
                {
                    temp.Add(AssociatedOfficesList[i].GetAttribute("class"));
                }
                if(temp.Any(o=>o.Contains("selected")))
                {
                    OfficeAssociatedText.Click();
                }
                else
                {
                    AssociatedOfficesList[0].Click();
                    OfficeAssociatedText.Click();
                }     
                if(PrimaryOfficeButton.Count>0)
                {
                    WaitForElement(PrimaryOfficeButton[0]);
                    WaitForElements(PrimaryOfficeList);
                    PrimaryOfficeList[2].Click();
                }
            }
            WaitForElement(EmployeeEmail);
            EmployeeEmail.Click();
            EmployeeEmail.Clear();
            EmployeeEmail.SendKeys("Sunil.kumar@mdinsurance.com");
            WaitForElement(EmployeeMobileNumber);
            EmployeeMobileNumber.Click();
            EmployeeMobileNumber.Clear();
            EmployeeMobileNumber.SendKeys("07834233955");
            WaitForElement(EmployeePhoneNumber);
            EmployeePhoneNumber.Click();
            EmployeePhoneNumber.Clear();
            EmployeePhoneNumber.SendKeys("01483493721");
            WaitForElement(ExtranetAccessNo);
            ExtranetAccessNo.Click();
            WaitForElement(SaveEmployeeButton);
            SaveEmployeeButton.Click();
        }
        public void EditEmployeeDetails()
        {
            WaitForElements(EditEmployeeButton);
            EditEmployeeButton[0].Click();
            WaitForElement(EmployeeDiv);
            Assert.IsTrue(EmployeeDiv.Displayed);
            //WaitForElement(EmployeeTitle);
            //EmployeeTitle.Click();
            //EmployeeTitle.Clear();
            //EmployeeTitle.SendKeys("Mr");
            //WaitForElement(EmployeeFirstName);
            //EmployeeFirstName.Click();
            //EmployeeFirstName.Clear();
            //EmployeeFirstName.SendKeys("Sunil");
            //WaitForElement(EmployeeLastName);
            //EmployeeLastName.Click();
            //EmployeeLastName.Clear();
            //EmployeeLastName.SendKeys("Sunil");
            WaitForElement(OfficeAssociatedButton);
            OfficeAssociatedButton.Click();
            CloseSpinnerOnExtranet();
            Thread.Sleep(500);
            WaitForElements(AssociatedOfficesList);
            if (AssociatedOfficesList.Count > 0)
            {
                List<string> temp = new List<string>();
                for (int i = 0; i < AssociatedOfficesList.Count; i++)
                {
                    temp.Add(AssociatedOfficesList[i].GetAttribute("class"));
                }
                if (temp.Any(o => o.Contains("selected")))
                {
                    OfficeAssociatedText.Click();
                }
                else
                {
                    AssociatedOfficesList[0].Click();
                    OfficeAssociatedText.Click();
                    CloseSpinnerOnExtranet();
                    Thread.Sleep(500);
                }
                CloseSpinnerOnExtranet();
                Thread.Sleep(500);
                if (PrimaryOfficeButton.Count > 0)
                {
                    WaitForElement(PrimaryOfficeButton[0]);
                    PrimaryOfficeButton[0].Click();
                    CloseSpinnerOnExtranet();
                    Thread.Sleep(500);
                    WaitForElements(PrimaryOfficeList);
                    PrimaryOfficeList[1].Click();
                    CloseSpinnerOnExtranet();
                    Thread.Sleep(500);
                }
            }
            //WaitForElement(EmployeeEmail);
            //EmployeeEmail.Click();
            //EmployeeEmail.Clear();
            //EmployeeEmail.SendKeys($"Sunil.kumar{RunId.Id}@mdinsurance.com");
            WaitForElement(EmployeeMobileNumber);
            EmployeeMobileNumber.Click();
            EmployeeMobileNumber.Clear();
            EmployeeMobileNumber.SendKeys("07834233955");
            WaitForElement(EmployeePhoneNumber);
            EmployeePhoneNumber.Click();
            EmployeePhoneNumber.Clear();
            EmployeePhoneNumber.SendKeys("01483493721");
            WaitForElement(ExtranetAccessYes);
            ExtranetAccessYes.Click();
            WaitForElement(ExtranetAdminAccessYes);
            ExtranetAdminAccessYes.Click();
            WaitForElement(SaveEmployeeButton);
            SaveEmployeeButton.Click();
        }
    }
}
