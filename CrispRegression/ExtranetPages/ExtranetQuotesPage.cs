﻿using CrispAutomation.Features;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;
using Protractor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace CrispAutomation.ExtranetPages
{
    public class ExtranetQuotesPage : Support.Pages
    {
        public IWebDriver wdriver;
        public NgWebDriver ngDriver;
        public ExtranetQuotesPage(ISearchContext driver)
        {
            PageFactory.InitElements(driver, this);
            wdriver = (RemoteWebDriver)driver;
            ngDriver = new NgWebDriver(wdriver);
        }

        [FindsBy(How = How.XPath, Using = "//section//aside[@id='leftsidebar']//div[@class='slimScrollDiv']//ul[@class='list']//li[@class='active']//span[text()='Quotes']")]
        public IWebElement QuotesActiveLink;     

        //Quotes Page Elements 
        [FindsBy(How = How.Id, Using = "quotes")]
        public IWebElement YourQuotes;

        [FindsBy(How = How.Id, Using = "quotes-info-tile")]
        public IWebElement QuotesInfotitle;

        [FindsBy(How = How.Id, Using = "applications-info-tile")]
        public IWebElement ApplicationInfoTitle;

        [FindsBy(How = How.Id, Using = "pending-info-tile")]
        public IWebElement PendingInfoTitle;

        [FindsBy(How = How.XPath, Using = "//div[@id='yourQuotes_wrapper']//table[@id='yourQuotes']//tbody//tr[@role='row']")]
        public IList<IWebElement> YourQuotesRecords;
        
        [FindsBy(How = How.XPath, Using = "//div[@id='yourQuotes_wrapper']//table[@id='yourQuotes']//tbody//tr[@role='row']//following::tr//td[contains(@id,'quoteBreakdown')]")]
        public IWebElement QuoteRecordData;

        [FindsBy(How = How.XPath, Using = "//div[@id='yourQuotes_wrapper']//table[@id='yourQuotes']//tbody//tr[@role='row']//following::tr//td[contains(@id,'quoteBreakdown')]//div")]
        public IList<IWebElement> QuoteContainerDivs;

        [FindsBy(How = How.XPath, Using = "//div[@id='quotes']//span[contains(@class,'yourQuotesCount')]")]
        public IWebElement QuotesCount;

        [FindsBy(How = How.Id, Using = "quotesMessage")]
        public IWebElement QuotesMessage;

        [FindsBy(How = How.XPath, Using = "//table[@id='quotesTable']//tbody//tr")]
        public IList<IWebElement> QuotesTableCount;

        [FindsBy(How = How.Id, Using = "Quotes")]
        public IWebElement QuotesDiv;

        [FindsBy(How = How.XPath, Using = "//table[@id='quotesTable']")]
        public IWebElement QuotesTableInfo;

        [FindsBy(How = How.XPath, Using = "//table[@id='quotesTable']//tbody//tr[@role='row']//td//a")]
        public IList<IWebElement> QuotesRecords;

        [FindsBy(How = How.XPath, Using = "//div[@id='quotesTable_filter']//label")]
        public IWebElement SearchForQuote;

        [FindsBy(How = How.XPath, Using = "//div[@id='yourQuotes_filter']//label//input[@type='search'][@aria-controls='quotesTable']")]
        public IWebElement SearchForQuoteInput;

        [FindsBy(How = How.XPath, Using = "//table[@id='yourQuotes']//tbody//tr[@role='row']//td[1]")]
        public IList<IWebElement> QuotesRecordList;

        [FindsBy(How = How.XPath, Using = "//div[@id='yourQuotes_filter']//label")]
        public IWebElement SearchForQuoteOnQuotesPage;

        [FindsBy(How = How.XPath, Using = "//table[@id='yourQuotes']//td[contains(@id,'quoteBreakdown')]//table//thead//tr//th")]
        public IList<IWebElement> QuoteTableColumns;

        List<string> QuoteTableColumnNames = new List<string>();

        public void SearchAndSelectQuoteFromQuotesPage()
        {
            WaitForElement(QuotesCount);
            if (QuotesCount.Text != "0")
            {
                WaitForElement(SearchForQuoteOnQuotesPage);
                SearchForQuoteOnQuotesPage.Click();
                WaitForElement(SearchForQuoteInput);
                SearchForQuoteInput.SendKeys(Statics.QuoteNumber);
                CloseSpinnerOnExtranet();
                Thread.Sleep(500);
                WaitForElements(QuotesRecordList);
                var quote = QuotesRecordList.FirstOrDefault(x => x.Text.Contains(Statics.QuoteNumber));
                if (quote != null)
                {
                    CloseSpinnerOnExtranet();
                    Thread.Sleep(500);
                    quote.Click();
                    CloseSpinnerOnExtranet();
                    Thread.Sleep(1000);
                }
            }

        }        
        public void VerifyQuotesRecordsFromHomePage()
        {
            WaitForElement(ExtranetHomePage.HomeLink);
            ExtranetHomePage.HomeLink.Click();
            WaitForElement(ExtranetHomePage.QuotesDiv);
            CloseSpinnerOnExtranet();
            if (ExtranetHomePage.QuotesRecords.Count>0)
            {
                ExtranetHomePage.SearchAndSelectQuoteFromHomePage();
                WaitForElement(QuotesActiveLink);
                Assert.IsTrue(QuotesActiveLink.Displayed,"Failed To Navigate To Quote Page");
                WaitForElements(YourQuotesRecords);
                if(YourQuotesRecords.Count>0)
                {
                    YourQuotesRecords[0].Click();
                    //Thread.Sleep(500);
                    WaitForElement(QuoteRecordData);
                    Assert.IsTrue(QuoteRecordData.Displayed,"Failed To Display Quote Data For Selected Records");
                    Assert.IsTrue(QuoteContainerDivs.Count > 0,"Quote Container Divs(Sales Manager, Stats and Summary) Failed To Display");
                  
                }
                else
                {
                    WaitForElement(ExtranetHomePage.QuoteMessage);
                    var quoteMessage = ExtranetHomePage.QuoteMessage.Text;
                    Assert.IsTrue(quoteMessage.Contains("You don't currently have any quotes."));
                }
            }
        }
        public void VerifyQuotesRecordsFromQuotesPage()
        {
            WaitForElement(ExtranetHomePage.QuotesLink);
            ExtranetHomePage.QuotesLink.Click();
            WaitForElement(QuotesActiveLink);
            Assert.IsTrue(QuotesActiveLink.Displayed, "Failed To Navigate To Quote Page");
            WaitForElement(ExtranetHomePage.QuotesDiv);
            CloseSpinnerOnExtranet();
            if (ExtranetHomePage.QuotesRecords.Count > 0)
            {
                SearchAndSelectQuoteFromQuotesPage();               
                WaitForElements(YourQuotesRecords);               
            }
            else
            {
                WaitForElement(ExtranetHomePage.QuoteMessage);
                var quoteMessage = ExtranetHomePage.QuoteMessage.Text;
                Assert.IsTrue(quoteMessage.Contains("You don't currently have any quotes."));
            }
        }

        public void VerifyQuoteTable()
        {
            if (QuoteContainerDivs.Count > 0)
            {
                List<string> temp = new List<string>();
                for (int i = 0; i < QuoteContainerDivs.Count; i++)
                {
                    temp.Add(QuoteContainerDivs[i].GetAttribute("class"));
                }
                Assert.IsTrue(temp.Any(o => o.Contains("sales-manager-container")));
                Assert.IsTrue(temp.Any(o => o.Contains("product-summary-container")));
                Assert.IsTrue(temp.Any(o => o.Contains("quote-stats-container")));
                Assert.IsTrue(temp.Any(o => o.Contains("quote-summary-action-btns-container")));
                QuoteTableColumnNames = QuoteTableColumns.Select(i => i.Text).ToList();
                Assert.IsTrue(QuoteTableColumnNames.Any(o => o.Contains("Product Name")));
                Assert.IsTrue(QuoteTableColumnNames.Any(o => o.Contains("Number of Plots")));
                Assert.IsTrue(QuoteTableColumnNames.Any(o => o.Contains("Total Reconstruction Cost")));
                Assert.IsTrue(QuoteTableColumnNames.Any(o => o.Contains("Total Estimated Sale Price")));
                Assert.IsTrue(QuoteTableColumnNames.Any(o => o.Contains("Total Fees")));
            }
        }

      

        //public void SiteDetailsSection()
        //{
        //    QuoteRecipientDetails();
        //    AddSiteAddress();
        //    AddConstructionDetails();
        //    Nextbutton();
        //}

        //public void PlotDetailsSection()
        //{
        //    NgWebElement UploadYesButton = ngDriver.FindElement(By.XPath("//app-quote-application-wizard-plot-form//form//input-yes-no[@label='Do you want to use the download/upload plot matrix?']//button//span[text()='Yes']"));
        //    UploadYesButton.Click();
        //    NgWebElement UploadFilesButton = ngDriver.FindElement(By.XPath("//div[@id='documents-dropzone']//dropzone[@class='dropzone-component']//div[@class='dz-text']"));
        //    UploadFilesButton.Click();
        //    UploadFilesButton.SendKeys(@"\\tpgfile\NewCompany\IT\Department\QA Team\Current Projects\CRISP\CrispAutomation\CrispData\ExtraNet\Plot_Matrix.xlsx");
        //    NgWebElement PlotData = ngDriver.FindElement(By.XPath("//app-quote-application-wizard-plot-form//form//section//div//table[@class='jexcel bossanova-ui']"));
        //    Assert.IsTrue(PlotData.Displayed);
        //    Nextbutton();
        //}
        ////Site Details Section Mwthods
        //public void QuoteRecipientDetails()
        //{
        //    NgWebElement QuoteRecipient = ngDriver.FindElement(By.XPath("//input[@formcontrolname='quoteRecipient']"));
        //    NgWebElement QuoteRecipientOffice = ngDriver.FindElement(By.XPath("//input[@formcontrolname='recipientOffice']"));
        //}
        //public void AddSiteAddress()
        //{
        //    NgWebElement AddressLineOne = ngDriver.FindElement(By.XPath("//input[@formcontrolname='addressLineOne']"));
        //    AddressLineOne.Click();
        //    AddressLineOne.Clear();
        //    AddressLineOne.SendKeys("Extranet Test Address Line 1");
        //    NgWebElement AddressLineTwo = ngDriver.FindElement(By.XPath("//input[@formcontrolname='addressLineTwo']"));
        //    AddressLineTwo.Click();
        //    AddressLineTwo.Clear();
        //    AddressLineTwo.SendKeys("Extranet Test address Line 2");
        //    NgWebElement AddressLineThree = ngDriver.FindElement(By.XPath("//input[@formcontrolname='addressLineThree']"));
        //    AddressLineThree.Click();
        //    AddressLineThree.Clear();
        //    AddressLineThree.SendKeys("Extranet Test address Line 3");
        //    NgWebElement Town = ngDriver.FindElement(By.XPath("//input[@formcontrolname='townCity']"));
        //    Town.Click();
        //    Town.Clear();
        //    Town.SendKeys("Extranet Test Town");
        //    NgWebElement Postcode = ngDriver.FindElement(By.XPath("//input[@formcontrolname='postcode']"));
        //    Postcode.Click();
        //    Postcode.Clear();
        //    Postcode.SendKeys("CH41 1AU");
        //}
        //public void AddConstructionDetails()
        //{
        //    NgWebElement ConstrcutionStartDate = ngDriver.FindElement(By.XPath("//input[@formcontrolname='startDate']"));
        //    ConstrcutionStartDate.Click();
        //    SelectStartDate();
        //    NgWebElement ConstrcutionEndDate = ngDriver.FindElement(By.XPath("//input[@formcontrolname='endDate']"));
        //    ConstrcutionEndDate.Click();
        //    SelectEndDate();
        //    NgWebElement StoreysAboveGround = ngDriver.FindElement(By.XPath("//input[@formcontrolname='storiesAboveGround']"));
        //    StoreysAboveGround.Click();
        //    StoreysAboveGround.Clear();
        //    StoreysAboveGround.SendKeys("2");
        //    NgWebElement StoreysBelowGround = ngDriver.FindElement(By.XPath("//input[@formcontrolname='storiesBelowGround']"));
        //    StoreysBelowGround.Click();
        //    StoreysBelowGround.Clear();
        //    StoreysBelowGround.SendKeys("1");
        //    NgWebElement InnovativeMethods = ngDriver.FindElement(By.XPath("//div[@id='constructionDetails']//div[@class='innovativeMethods']//input-yes-no[@formcontrolname='innovativeMethods']//div[contains(@class,'yes-no')]//button//span[text()='No']//parent::button"));
        //    InnovativeMethods.Click();
        //    NgWebElement UnitsAttached = ngDriver.FindElement(By.XPath("//div[@id='constructionDetails']//div[@class='unitsAttatched']//input-yes-no[@formcontrolname='unitsAttatched']//div[contains(@class,'yes-no')]//button//span[text()='No']//parent::button"));
        //    UnitsAttached.Click();
        //    NgWebElement SiteAdminButton = ngDriver.FindElement(By.XPath("//div[@id='constructionDetails']//div[@class='isSiteInAdministration']//input-yes-no[@formcontrolname='isSiteInAdministration']//div[contains(@class,'yes-no')]//button//span[text()='No']//parent::button"));
        //    SiteAdminButton.Click();
        //}


        //public void Nextbutton()
        //{
        //    NgWebElement NextButton = ngDriver.FindElement(By.XPath("//button[contains(@class,'btn-quoteapp-nav-next-prev mat-raised-button mat-primary')]//span[text()='Next']"));
        //    if (NextButton.Displayed)
        //    {
        //        NextButton.Click();
        //    }

        //}

        //public void SelectStartDate()
        //{
        //    //FutureData
        //    DateTime date = DateTime.Today;
        //    string startDate = date.ToString("d MMMM yyyy");
        //    //Thread.Sleep(5000);
        //    var DateInput = $"//mat-calendar//div[@class='mat-calendar-content']//table//tbody//tr[@role='row']//td[@aria-label='{startDate}']";
        //    //Thread.Sleep(5000);
        //    var constructionStartDate = ngDriver.FindElement(By.XPath(DateInput));
        //    WaitForElementToClick(constructionStartDate);
        //    constructionStartDate.Click();

        //}
        //public void SelectEndDate()
        //{
        //    DateTime date = DateTime.Today.AddYears(1);
        //    string endDate = date.ToString("d MMMM yyyy");
        //    DateTime d = Convert.ToDateTime(endDate);
        //    int year = d.Year;
        //    string month = DateTime.Now.ToString("MMMM");
        //    int day = d.Day;
        //    //Thread.Sleep(5000);
        //    var selectYear = "//mat-calendar//button[@aria-label='Choose month and year']";
        //    //Thread.Sleep(5000);          
        //    var ChooseYear = ngDriver.FindElement(By.XPath(selectYear));
        //    //Thread.Sleep(1000);          
        //    WaitForElementToClick(ChooseYear);
        //    ChooseYear.Click();
        //    //Thread.Sleep(5000);
        //    var yearInput = $"//mat-calendar//div[@class='mat-calendar-content']//mat-multi-year-view//table//tbody//tr[@role='row']//td[@aria-label='{year}']";
        //    //Thread.Sleep(5000);         
        //    var YearInput = ngDriver.FindElement(By.XPath(yearInput));
        //    //Thread.Sleep(1000);           
        //    WaitForElementToClick(YearInput);
        //    YearInput.Click();
        //    //Thread.Sleep(5000);
        //    var monthInput = $"//mat-calendar//div[@class='mat-calendar-content']//mat-year-view//table//tbody//tr[@role='row']/td[@aria-label='{month} {year}']";
        //    //Thread.Sleep(5000);                   
        //    var MonthInput = ngDriver.FindElement(By.XPath(monthInput));
        //    //Thread.Sleep(1000);           
        //    WaitForElementToClick(MonthInput);
        //    MonthInput.Click();
        //    //Thread.Sleep(5000);
        //    var DateInput = $"//mat-calendar//div[@class='mat-calendar-content']//table//tbody//tr//td[@aria-label='{endDate}']";
        //    //Thread.Sleep(5000);
        //    var constructionEndDate = ngDriver.FindElement(By.XPath(DateInput));
        //    //Thread.Sleep(500);
        //    CloseSpinneronPage();
        //    CloseSpinnerOnExtranet();
        //    //Thread.Sleep(500);
        //    WaitForElementToClick(constructionEndDate);
        //    constructionEndDate.Click();
        //}
    }
}
