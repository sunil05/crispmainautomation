﻿using CrispAutomation.Features;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.PageObjects;
using Protractor;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace CrispAutomation.ExtranetPages
{
    public class ExtranetLoginPage : Support.Pages
    {
        public IWebDriver wdriver;
        public NgWebDriver ngDriver;
        public ExtranetLoginPage(ISearchContext driver)
        {
            PageFactory.InitElements(driver, this);
            wdriver = (RemoteWebDriver)driver;
        }
        //LABC Login Page 
        [FindsBy(How = How.XPath, Using = "//div[@class='login-box']//img[@alt='LABC Warranty']")]
        public IWebElement LABCWarrentyOnLoginPage;
        //PG Login Page 
        [FindsBy(How = How.XPath, Using = "//div[@class='login-box']//img[@alt='Premier Guarantee']")]
        public IWebElement PGOnLoginPage;

        [FindsBy(How = How.XPath, Using = "//div[@class='login-box']//img")]
        public IList<IWebElement> LoginPage;

        //Login Page  Elements
        [FindsBy(How = How.Id, Using = "Input_Email")]
        public IWebElement UserName;
        [FindsBy(How = How.Id, Using = "Input_Password")]
        public IWebElement PassWord;
        [FindsBy(How = How.XPath, Using = "//button[text()='Sign In']")]
        public IWebElement LoginButton;
        //Dashboard page Elements 
        [FindsBy(How = How.XPath, Using = "//section//div/a//img[@alt='LABC Warranty']")]
        public IWebElement LABCLogo;
        [FindsBy(How = How.XPath, Using = "//section//div/a//img[@alt='Premier Guarantee']")]
        public IWebElement PGLogo;
        //LogOut Elements 
        [FindsBy(How = How.Id, Using = "pop")]
        public IWebElement UserIntialsControl;
        [FindsBy(How = How.XPath, Using = "//div[@class='popover-content']//span[@class='btn-logout']//a[@href='Account/Logout']")]
        public IWebElement LogOutButton;
        [FindsBy(How = How.XPath, Using = "//div[@class='container-fluid']//p")]
        public IWebElement LogOutMessage;
        public void ExtranetLABCLogin()
        {
            ngDriver = new NgWebDriver(Driver);
            ngDriver.IgnoreSynchronization = true;
            wdriver.Navigate().GoToUrl(ConfigurationManager.AppSettings["ExtranetLABCUrl"]);
            ngDriver.Url = wdriver.Url;           
            Assert.IsTrue(LABCWarrentyOnLoginPage.Displayed);
            UserName.Click();
            UserName.SendKeys(ConfigurationManager.AppSettings["ExtranetEmail"]);
            PassWord.Click();
            PassWord.SendKeys(ConfigurationManager.AppSettings["ExtranetPassword"]);
            LoginButton.Click();
            CloseSpinnerOnExtranet();
            Thread.Sleep(500);
            WaitForElement(LABCLogo);
            Assert.IsTrue(LABCLogo.Displayed);
            CloseSpinnerOnExtranet();
            Thread.Sleep(500);
        }
        public void ExtranetPGLogin()
        {
            ngDriver = new NgWebDriver(Driver);
            ngDriver.IgnoreSynchronization = true;
            wdriver.Navigate().GoToUrl(ConfigurationManager.AppSettings["ExtranetPGUrl"]);
            ngDriver.Url = wdriver.Url;
            Assert.IsTrue(PGOnLoginPage.Displayed);
            UserName.Click();
            UserName.SendKeys(ConfigurationManager.AppSettings["ExtranetEmail"]);
            PassWord.Click();
            PassWord.SendKeys(ConfigurationManager.AppSettings["ExtranetPassword"]);
            LoginButton.Click();
            WaitForElement(PGLogo);
            Assert.IsTrue(PGLogo.Displayed);
        }
        public void ExtranetLogOut()
        {
            WaitForElement(UserIntialsControl);
            UserIntialsControl.Click();
            WaitForElement(LogOutButton);
            LogOutButton.Click();
            string logoutMessage = LogOutMessage.Text;            
            Assert.IsTrue(LogOutMessage.Text.Contains("You are now logged out."), "User Failed to logout On Extranet");
        }
        public void SelectExtranetSite()
        {
            ngDriver = new NgWebDriver(Driver);
            ngDriver.IgnoreSynchronization = true;
            if (Statics.OrderNumber.Contains("PL-PG"))
            {
                wdriver.Navigate().GoToUrl(ConfigurationManager.AppSettings["ExtranetPGUrl"]);
            }
            if (Statics.OrderNumber.Contains("PL-LABC"))
            {
                wdriver.Navigate().GoToUrl(ConfigurationManager.AppSettings["ExtranetLABCUrl"]);
            }
            ngDriver.Url = wdriver.Url;
            if(LoginPage.Count>0)
            {
                UserName.Click();
                UserName.SendKeys(ConfigurationManager.AppSettings["ExtranetEmail"]);
                PassWord.Click();
                PassWord.SendKeys(ConfigurationManager.AppSettings["ExtranetPassword"]);
                LoginButton.Click();
                CloseSpinnerOnExtranet();
            }
        }

    }
}
