﻿using OpenQA.Selenium;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.PageObjects;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Protractor;
using OpenQA.Selenium.Chrome;
using NUnit.Framework;
using System.IO;
using CrispAutomation.Features;
using CrispAutomation.Support;

namespace CrispAutomation.ExtranetPages
{
    public class ExtranetHomePage : Support.Pages
    {
        public IWebDriver wdriver;
        public NgWebDriver ngDriver;
        public ExtranetHomePage(ISearchContext driver)
        {
            PageFactory.InitElements(driver, this);
            wdriver = (RemoteWebDriver)driver;
        }
        //Home Page Elements 
        [FindsBy(How = How.XPath, Using = "//div[@class='user-welcome']")]
        public IWebElement WelcomeDiv;
        [FindsBy(How = How.Id, Using = "getQuotebtn")]
        public IWebElement GetAQuote;
        [FindsBy(How = How.Id, Using = "getCertificate")]
        public IWebElement GetACertificate;
        [FindsBy(How = How.XPath, Using = "//section//aside[@id='leftsidebar']//div[@class='slimScrollDiv']//ul[@class='list']//li//span")]
        public IList<IWebElement> DashBoardItems;
        public IWebElement HomeLink => DashBoardItems.FirstOrDefault(x => x.Text.Contains("HOME"));
        public IWebElement QuotesLink => DashBoardItems.FirstOrDefault(x => x.Text.Contains("QUOTES"));
        public IWebElement OrdersLink => DashBoardItems.FirstOrDefault(x => x.Text.Contains("ORDERS"));
        public IWebElement CompanyLink => DashBoardItems.FirstOrDefault(x => x.Text.Contains("COMPANY"));
        public IWebElement RegistrationLink => DashBoardItems.FirstOrDefault(x => x.Text.Contains("REGISTRATION"));
        public IWebElement TrainingGuideLink => DashBoardItems.FirstOrDefault(x => x.Text.Contains("TRAINING GUIDE"));
        public IWebElement FAQSLink => DashBoardItems.FirstOrDefault(x => x.Text.Contains("FAQS"));
        public IWebElement ContactUsLink => DashBoardItems.FirstOrDefault(x => x.Text.Contains("CONTACT US"));
        public IWebElement PrivacyPolicyLink => DashBoardItems.FirstOrDefault(x => x.Text.Contains("PRIVACY POLICY"));
        public IWebElement TermsOfUseLink => DashBoardItems.FirstOrDefault(x => x.Text.Contains("TERMS OF USE"));

        //HomePage Elements
        [FindsBy(How = How.Id, Using = "Orders")]
        public IWebElement Orders;

        [FindsBy(How = How.Id, Using = "Quotes")]
        public IWebElement Quotes;

        //Company Page Elements 
        [FindsBy(How = How.Id, Using = "offices")]
        public IWebElement OfficeDetails;

        //Registration Elements 
        [FindsBy(How = How.XPath, Using = "//div[contains(@class,'outstanding-container')]")]
        public IWebElement OutstandingRegDocuments;

        [FindsBy(How = How.XPath, Using = "//div[contains(@class,'confirmed-container')]")]
        public IWebElement ConfirmedRegDocuments;

        [FindsBy(How = How.XPath, Using = "//div[contains(@class,'term-details-container')]")]
        public IWebElement TermDetailsOnRegistration;

        //Training Guide Elements         
        [FindsBy(How = How.XPath, Using = "//div[contains(@class,'training-container')]")]
        public IWebElement TrainingContainer;

        //FAQS Elements 
        [FindsBy(How = How.XPath, Using = "//div[contains(@class,'faqs-container')]")]
        public IWebElement FaqsContainer;

        //Contact Us Elements 
        [FindsBy(How = How.XPath, Using = "//div[contains(@class,'contact-container')]")]
        public IWebElement ContactContainer;

        //Privacy Policy Elements 
        [FindsBy(How = How.XPath, Using = "//div[contains(@class,'privacy-container')]")]
        public IWebElement PrivacyContainer;

        [FindsBy(How = How.XPath, Using = "//div[@id='privacyAccordion']//div[contains(@class,'accordion-container')]")]
        public IList<IWebElement> PrivacyContainerLinks;

        //TermsOfUse Policy Elements 
        [FindsBy(How = How.XPath, Using = "//div[contains(@class,'terms-container')]")]
        public IWebElement TermsContainer;

        [FindsBy(How = How.XPath, Using = "//div[@id='termsAccordion']//div[contains(@class,'accordion-container')][@aria-expanded='false']")]
        public IList<IWebElement> TermsContainerLinks;

        [FindsBy(How = How.XPath, Using = "//div[@id='termsAccordion']//div[contains(@class,'accordion-container')][@aria-expanded='true']")]
        public IList<IWebElement> TermsContainerExpandedLinks;

        //Quotes Table Elements on Home Page
        [FindsBy(How = How.Id, Using = "quoteCount")]
        public IWebElement QuotesCount;

        [FindsBy(How = How.Id, Using = "quotesMessage")]
        public IWebElement QuotesMessage;

        [FindsBy(How = How.XPath, Using = "//table[@id='quotesTable']//tbody//tr")]
        public IList<IWebElement> QuotesTableCount;

        [FindsBy(How = How.Id, Using = "Quotes")]
        public IWebElement QuotesDiv;
      
        [FindsBy(How = How.XPath, Using = "//table[@id='quotesTable']")]
        public IWebElement QuotesTableInfo;

        [FindsBy(How = How.XPath, Using = "//table[@id='quotesTable']//tbody//tr[@role='row']//td//a")]
        public IList<IWebElement> QuotesRecords;

        [FindsBy(How = How.Id, Using = "quotesMessage")]
        public IWebElement QuoteMessage;

        [FindsBy(How = How.Id, Using = "quotesTable_paginate")]
        public IWebElement QuotesTablePaginate;

        [FindsBy(How = How.Id, Using = "quotesTable_previous")]
        public IWebElement QuotesTablePrevousPaginate;

        [FindsBy(How = How.XPath, Using = "//div[@id='quotesTable_paginate']//ul[@class='pagination']//li//a[@aria-controls='quotesTable']")]
        public IList<IWebElement> QuotesTablePaginateButtons;

        [FindsBy(How = How.XPath, Using = "//div[@id='quotesTable_paginate']//ul[@class='pagination']//li[contains(@class,'paginate_button next')]//preceding-sibling::li[1]//a")]
        public IWebElement QuotesTablePaginateLastButton;

        [FindsBy(How = How.XPath, Using = "//div[@id='quotesTable_paginate']//ul[@class='pagination']//li[@id='quotesTable_next']//a")]
        public IWebElement QuotesTableNextPaginate;

        [FindsBy(How = How.XPath, Using = "//table[@id='ordersTable']//tbody//tr[@role='row']//td[1]//a")]
        public IList<IWebElement> OrdersRecordsWithRefNumber;


        //Incomplete Application  Elements On Home Page 

        [FindsBy(How = How.XPath, Using = "//div[contains(@class,'order-btn-container')]//ul//li//a[contains(text(),'Incomplete Applications')]")]
        public IWebElement IncompleteApplicationDiv;

        [FindsBy(How = How.XPath, Using = "//div[contains(@class,'order-btn-container')]//ul//li//a[contains(text(),'Incomplete Applications')]//span")]
        public IWebElement IncompleteApplicationCount;

        //Order Table Elements On Home Page
        [FindsBy(How = How.Id, Using = "orderCount")]
        public IWebElement OrdersCount;

        [FindsBy(How = How.Id, Using = "orderMessage")]
        public IWebElement OrdersMessage;

        [FindsBy(How = How.Id, Using = "ordersTable")]
        public IList<IWebElement> OrdersTable;

        [FindsBy(How = How.XPath, Using = "//table[@id='ordersTable']//thead//tr[@role='row']//th")]
        public IList<IWebElement> OrdersTableHeaders;

        [FindsBy(How = How.XPath, Using = "//table[@id='ordersTable']//tbody//tr")]
        public IList<IWebElement> OrdersTableCount;

        [FindsBy(How = How.Id, Using = "Orders")]
        public IWebElement OrdersTableInfo;

        [FindsBy(How = How.XPath, Using = "//table[@id='ordersTable']//tbody//tr[@role='row']//td[1]//a")]
        public IList<IWebElement> QuotesRecordsWithRefNumber;

        [FindsBy(How = How.XPath, Using = "//table[@id='ordersTable']//tbody//tr[@role='row']//td[4]//a")]
        public IList<IWebElement> OrdersRecordWithActions;

        [FindsBy(How = How.XPath, Using = "//table[@id='ordersTable']//tbody//tr[@role='row']//td[5]//a")]
        public IList<IWebElement> OrdersRecordWithUrgentActions;

        [FindsBy(How = How.Id, Using = "ordersTable_paginate")]
        public IWebElement OrdersTablePaginate;

        [FindsBy(How = How.Id, Using = "ordersTable_previous")]
        public IWebElement OrdersTablePrevousPaginate;

        [FindsBy(How = How.XPath, Using = "//div[@id='ordersTable_paginate']//ul[@class='pagination']//li//a[@aria-controls='ordersTable']")]
        public IList<IWebElement> OrdersTablePaginateButtons;

        [FindsBy(How = How.XPath, Using = "//div[@id='ordersTable_paginate']//ul[@class='pagination']//li[contains(@class,'paginate_button next')]//preceding-sibling::li[1]//a")]
        public IWebElement OrdersTablePaginateLastButton;

        [FindsBy(How = How.XPath, Using = "//div[@id='ordersTable_paginate']//ul[@class='pagination']//li[@id='ordersTable_next']//a")]
        public IWebElement OrdersTableNextPaginate;

        //Registration status

        [FindsBy(How = How.XPath, Using = "//div//span[text()='Registration Status']//parent::span//parent::div[contains(@onclick,'Registration')]")]
        public IWebElement RegistrationStatusDiv;

        [FindsBy(How = How.Id, Using = "home-registration-container")]
        public IWebElement RegistrationContainerOnHomePage;
        [FindsBy(How = How.XPath, Using = "//div[@id='home-registration-container']//span[text()='Registration Status']")]
        public IWebElement RegistrationStatus;
        
        //Manage Company Details 

        [FindsBy(How = How.XPath, Using = "//div[@class='action-rquired-container']//ul//li//span[contains(@onclick,'Company')]")]
        public IWebElement ManageCompanyDetailsDiv;

        //Download Extranet Training Guide
        [FindsBy(How = How.XPath, Using = "//a[contains(@class,'extranet-guide')]")]
        public IWebElement TrainingGuideDiv;

        [FindsBy(How = How.XPath, Using = "//div[@class='card faqs-container']//following-sibling::div//div[@class='link-to-external-content']//a[text()='here']")]
        public IWebElement FAQClickHereLink;

        [FindsBy(How = How.XPath, Using = "//div[@id='body']//section[@class='text-section']//a[contains(@title,'Extranet FAQs.pdf')]//img")]
        public IWebElement LABCFAQsPDF;

        [FindsBy(How = How.XPath, Using = "//a[contains(text(),'Download the Extranet FAQs')]")]
        public IWebElement PGFAQsPDF;

        //Download Extranet Training Guide
        [FindsBy(How = How.XPath, Using = "//div[contains(@class,'health-safety-container')]")]
        public IWebElement HealthAndSafetydiv;

        [FindsBy(How = How.XPath, Using = "//div[@id='ordersTable_filter']//label")]
        public IWebElement SearchForOrder;

        [FindsBy(How = How.XPath, Using = "//div[@id='ordersTable_filter']//label//input[@type='search'][@aria-controls='ordersTable']")]
        public IWebElement SearchForOrderInput;

        [FindsBy(How = How.XPath, Using = "//table[@id='ordersTable']//tbody//tr//td[1]//a")]
        public IList<IWebElement> OrdersRecordList;

        [FindsBy(How = How.XPath, Using = "//div[@id='quotesTable_filter']//label")]
        public IWebElement SearchForQuoteOnHomePage;

        [FindsBy(How = How.XPath, Using = "//div[@id='quotesTable_filter']//label//input[@type='search'][@aria-controls='quotesTable']")]
        public IWebElement SearchForQuoteInput;

        [FindsBy(How = How.XPath, Using = "//table[@id='quotesTable']//tbody//tr//td[1]//a")]
        public IList<IWebElement> QuotesRecordList;

        //Main pages Links        
        public void VerifyMainPageLinks()
        {
            Assert.IsTrue(WelcomeDiv.Displayed);         
            if (DashBoardItems.Count > 0)
            {
                VerifyHomePagelinks();
            }
        }
        //Home Page Links and Divs        
        public void VerifyHomePagelinks()
        {
            //Home Link and Elements            
            Assert.IsTrue(HomeLink.Displayed, "Home Link Not Displayed On Main Page");
            WaitForElement(HomeLink);
            HomeLink.Click();
            Assert.IsTrue(Orders.Displayed, "Orders Card Not Displayed On Home Page");
            WaitForElement(Orders);
            WaitForElement(Quotes);          
            Assert.IsTrue(Quotes.Displayed, "Quotes Card Not Displayed On Home Page");
            //Quotes Link and Elements 
            Assert.IsTrue(QuotesLink.Displayed, "Quotes Link Not Displayed On Main Page");
            QuotesLink.Click();
            Assert.IsTrue(ExtranetQuotesPage.QuotesInfotitle.Displayed, "Quotes Info Title Not Displayed On Quotes Page");
            Assert.IsTrue(ExtranetQuotesPage.ApplicationInfoTitle.Displayed, "Incomple Applications Not Displayed On Quotes Page");
            Assert.IsTrue(ExtranetQuotesPage.PendingInfoTitle.Displayed, "Pending Applications Not Displayed On Quotes Page");
            Assert.IsTrue(ExtranetQuotesPage.YourQuotes.Displayed, "Your Quotes Card Not Displayed On Quotes Page");
            //Orders Link and Elements 
            Assert.IsTrue(OrdersLink.Displayed, "Orders Link Not Displayed On Main Page");
            OrdersLink.Click();
            WaitForElement(ExtranetOrdersPage.YourOrders);
            Assert.IsTrue(ExtranetOrdersPage.YourOrders.Displayed, "Your Orders Card Not Displayed On orders Page");
            // Assert.IsTrue(ExtranetOrdersPage.YourOrders.Text.Contains("Your Orders"));
            //Company Link and Elements 
            Assert.IsTrue(CompanyLink.Displayed, "Company Link Not Displayed On Main Page");
            CompanyLink.Click();
            Assert.IsTrue(OfficeDetails.Displayed, "Office Details Card Not Displayed On orders Page");
            //Registration Link and Elements 
            //Assert.IsTrue(RegistrationLink.Displayed, "Registration Link Not Displayed On Main Page");
            //RegistrationLink.Click();
            //Assert.IsTrue(OutstandingRegDocuments.Displayed, "Outstanding Registration Documents Card Not Displayed On Registration Page");
            //Assert.IsTrue(ConfirmedRegDocuments.Displayed, "Confirmed Registration Documents Card Not Displayed On Registration Page");
            //Assert.IsTrue(TermDetailsOnRegistration.Displayed, "TermsDetails Card Not Displayed On Registration Page");
            //Training Guide Link and Elements 
            Assert.IsTrue(TrainingGuideLink.Displayed, "Training Link Not Displayed On Main Page");
            TrainingGuideLink.Click();
            Assert.IsTrue(TrainingContainer.Displayed, "Training Card Not Displayed On Training Guide Page");
            CloseSpinnerOnExtranet();
            Thread.Sleep(500);
            //FAQS Link And Elements 
            Assert.IsTrue(FAQSLink.Displayed, "FAQS Link Not Displayed On Main Page");
            FAQSLink.Click();
            Assert.IsTrue(FaqsContainer.Displayed, "Faqs Card Not Displayed On FAQS Page");
            //Contact Us Links and Elements 
            Assert.IsTrue(ContactUsLink.Displayed, "Contact Us Link Not Displayed On Main Page");
            ContactUsLink.Click();
            Assert.IsTrue(ContactContainer.Displayed, "Contact Card Not Displayed On Contact Us Page");
            //Privacy Policy Links and Elements
            Assert.IsTrue(PrivacyPolicyLink.Displayed, "Privacy Policy Link Not Displayed On Main Page");
            PrivacyPolicyLink.Click();
            wdriver.SwitchTo().Window(wdriver.WindowHandles.Last());
            WaitForElement(PrivacyContainer);
            Assert.IsTrue(PrivacyContainer.Displayed, "Privacy Policy Card Not Displayed On Privacy-Policy Page");
            Assert.IsTrue(PrivacyContainerLinks.Count > 0, "Privacy Policy Links Are Failed To Display On Privacy Policy Page");
            if (PrivacyContainerLinks.Count > 0)
            {
                WaitForElements(PrivacyContainerLinks);
                if (PrivacyContainerLinks.Any(o => o.GetAttribute("aria-expanded") == "true"))
                {
                    foreach (var eachLink in PrivacyContainerLinks)
                    {
                        if (eachLink.GetAttribute("aria-expanded") == "true")
                        {
                            eachLink.Click();
                        }
                    }
                }
                foreach (var eachLink in PrivacyContainerLinks)
                {
                    WaitForElement(eachLink);
                    eachLink.Click();
                    //Thread.Sleep(500);
                }

            }
            wdriver.SwitchTo().Window(wdriver.WindowHandles.First());
            //Terms Of Use Links and Elements 
            Assert.IsTrue(TermsOfUseLink.Displayed, "Terms Of Use Link Not Displayed On Main Page");
            TermsOfUseLink.Click();
            wdriver.SwitchTo().Window(wdriver.WindowHandles.Last());
            WaitForElement(TermsContainer);
            Assert.IsTrue(TermsContainer.Displayed, "Terms Card Not Displayed On TermsOfUse Page");
            Assert.IsTrue(TermsContainerLinks.Count > 0, "Terms Links Are Failed To Display On Terms Page");
            if (TermsContainerLinks.Count > 0)
            {
                WaitForElements(TermsContainerLinks);
                if (TermsContainerLinks.Any(o => o.GetAttribute("aria-expanded") == "true"))
                {
                    foreach (var eachLink in TermsContainerLinks)
                    {
                        if (eachLink.GetAttribute("aria-expanded") == "true")
                        {
                            eachLink.Click();
                        }
                    }
                }
                foreach (var eachLink in TermsContainerLinks)
                {
                    WaitForElement(eachLink);
                    eachLink.Click();
                    //Thread.Sleep(500);
                }
            }
            wdriver.SwitchTo().Window(wdriver.WindowHandles.First());
        }
        //Verify Quotes Table On home Page 
        public void VerifyQuotesOnHomePage()
        {
            WaitForElement(ExtranetHomePage.HomeLink);
            ExtranetHomePage.HomeLink.Click();
            WaitForElement(QuotesCount);
            if (QuotesCount.Text == "0")
            {
                Assert.IsTrue(QuotesMessage.Text.Contains("You don't currently have any quotes."));
                Assert.IsTrue(GetAQuote.Displayed);
            }
            else
            {
                int actualQuoteCount = 0;
                int quotesCount = Convert.ToInt32(QuotesCount.Text.ToString());
                WaitForElements(QuotesTablePaginateButtons);
                int numberOfPages = Convert.ToInt32(QuotesTablePaginateLastButton.Text.ToString());
                if (quotesCount <= 3)
                {
                    Assert.AreEqual(quotesCount, QuotesTableCount.Count);
                    Assert.IsTrue(QuotesTablePaginateButtons.Count == 3);
                }
                else
                {
                    actualQuoteCount = 3;
                    string quotesInfo = QuotesTableInfo.Text;
                    Assert.IsTrue(quotesInfo.Contains(quotesCount.ToString()));
                    Assert.IsTrue(QuotesTablePaginateButtons.Count >= 3);
                    for (int i = 0; i < numberOfPages - 1; i++)
                    {
                        WaitForElement(QuotesTableNextPaginate);
                        QuotesTableNextPaginate.Click();
                        WaitForElements(QuotesTableCount);
                        //Thread.Sleep(200);
                        CloseSpinnerOnExtranet();
                        //Thread.Sleep(200);
                        actualQuoteCount += QuotesTableCount.Count;
                        Thread.Sleep(200);
                        CloseSpinnerOnExtranet();
                        Thread.Sleep(200);
                    }
                    Assert.AreEqual(actualQuoteCount, quotesCount);
                    Assert.IsTrue(QuotesTablePrevousPaginate.Displayed);
                    Assert.IsTrue(QuotesTableNextPaginate.Displayed);
                }
            }
            VerifySelectQuoteUsingRefNumberFromHomePage();
        }
        //Verify Quotes Table On home Page 
        public void VerifyOrdersOnHomePage()
        {
            WaitForElement(ExtranetHomePage.HomeLink);
            ExtranetHomePage.HomeLink.Click();
            WaitForElement(OrdersCount);
            if (OrdersCount.Text == "0")
            {
                Assert.IsTrue(OrdersMessage.Text.Contains("You don't currently have any orders."));
                Assert.IsFalse(GetACertificate.Displayed);
            }
            else
            {
                int actualorderCount = 0;
                WaitForElement(OrdersCount);
                CloseSpinnerOnExtranet();
                Thread.Sleep(500);               
                string count = OrdersCount.Text.ToString();
                CloseSpinnerOnExtranet();
                Thread.Sleep(500);
                int ordersCount = Convert.ToInt32(count);
                WaitForElements(OrdersTablePaginateButtons);
                int numberOfPages = Convert.ToInt32(OrdersTablePaginateLastButton.Text.ToString());
                if (ordersCount <= 3)
                {
                    Assert.AreEqual(ordersCount, OrdersTableCount.Count);
                    Assert.IsTrue(OrdersTablePaginateButtons.Count == 3);
                }
                else
                {
                    actualorderCount = 3;
                    string orderInfo = OrdersTableInfo.Text;
                    Assert.IsTrue(orderInfo.Contains(ordersCount.ToString()));
                    Assert.IsTrue(OrdersTablePaginateButtons.Count >= 3);
                    for (int i = 0; i < numberOfPages - 1; i++)
                    {
                        WaitForElement(OrdersTableNextPaginate);
                        OrdersTableNextPaginate.Click();
                        WaitForElements(OrdersTableCount);
                        //Thread.Sleep(200);
                        CloseSpinnerOnExtranet();
                        //Thread.Sleep(200);
                        actualorderCount += OrdersTableCount.Count;
                        Thread.Sleep(200);
                        CloseSpinnerOnExtranet();
                        Thread.Sleep(200);
                    }
                    Assert.AreEqual(actualorderCount, ordersCount);
                    Assert.IsTrue(OrdersTablePrevousPaginate.Displayed);
                    Assert.IsTrue(OrdersTableNextPaginate.Displayed);
                }
            }
            VerifySelectOrderUsingRefNumberFromHomePage();
        }
        //Verify Application Submit On home Page 
        public void VerifyIncompleteApplicationsOnHomePage()
        {
            WaitForElement(ExtranetHomePage.HomeLink);
            ExtranetHomePage.HomeLink.Click();
            WaitForElement(IncompleteApplicationDiv);
            int incompleteApplicationsCount = Convert.ToInt32(IncompleteApplicationCount.Text.ToString());
            if (incompleteApplicationsCount == 0)
            {
                var incompletepplicationState = IncompleteApplicationDiv.GetAttribute("class");
                Assert.IsTrue(incompletepplicationState.Contains("action-disabled"));
            }
            else
            {
                var incompletepplicationState = IncompleteApplicationDiv.GetAttribute("class");
                Assert.IsFalse(incompletepplicationState.Contains("action-disabled"));
            }
        }

        //Verify Registration Status On home Page 
        public void VerifyRegistrationStatusOnHomePage()
        {
            WaitForElement(ExtranetHomePage.HomeLink);
            ExtranetHomePage.HomeLink.Click();
            CloseSpinnerOnExtranet();          
            WaitForElement(RegistrationContainerOnHomePage);
            Assert.IsTrue(RegistrationStatus.Displayed);
            //RegistrationStatusDiv.Click();
            //Assert.IsTrue(OutstandingRegDocuments.Displayed, "Failed to Navigate Registration Page");
            //Assert.IsTrue(ConfirmedRegDocuments.Displayed, "Failed to Navigate Registration Page");
            //Assert.IsTrue(TermDetailsOnRegistration.Displayed, "Failed to Navigate Registration Page");
        }

        //Verify Manage Company Details On home Page 
        public void VerifyManageCompanyDetailsOnHomePage()
        {
            WaitForElement(ExtranetHomePage.HomeLink);
            ExtranetHomePage.HomeLink.Click();
            CloseSpinnerOnExtranet();
            WaitForElement(ManageCompanyDetailsDiv);
            ManageCompanyDetailsDiv.Click();
            CloseSpinnerOnExtranet();
            Assert.IsTrue(OfficeDetails.Displayed, "Failed to Navigate Company Page");
        }

        //Verify Download Training Guide Details On home Page 
        public void VerifyDownloadLABCTrainingGuideOnHomePage()
        {
            WaitForElement(ExtranetHomePage.HomeLink);
            ExtranetHomePage.HomeLink.Click();         
            WaitForElement(TrainingGuideDiv);
            TrainingGuideDiv.Click();
            Thread.Sleep(500);
            CloseSpinnerOnExtranet();
            WaitForElement(FAQClickHereLink);
            Assert.IsTrue(FAQClickHereLink.Displayed);
            FAQClickHereLink.Click();
            Thread.Sleep(500);
            CloseSpinnerOnExtranet();
            wdriver.SwitchTo().Window(wdriver.WindowHandles.Last());
            WaitForElement(LABCFAQsPDF);
            LABCFAQsPDF.Click();
            Thread.Sleep(500);
            CloseSpinnerOnExtranet();
            string currentURL = wdriver.Url;           
            Assert.IsTrue(currentURL.Contains("media"));
        }
        public void VerifyDownloadPGTrainingGuideOnHomePage()
        {
            WaitForElement(ExtranetHomePage.HomeLink);
            ExtranetHomePage.HomeLink.Click();
            WaitForElement(TrainingGuideDiv);
            TrainingGuideDiv.Click();
            Thread.Sleep(500);
            CloseSpinnerOnExtranet();
            WaitForElement(FAQClickHereLink);
            Assert.IsTrue(FAQClickHereLink.Displayed);
            FAQClickHereLink.Click();
            Thread.Sleep(500);
            CloseSpinnerOnExtranet();
            wdriver.SwitchTo().Window(wdriver.WindowHandles.Last());
            WaitForElement(PGFAQsPDF);
            PGFAQsPDF.Click();
            Thread.Sleep(500);
            CloseSpinnerOnExtranet();
            Thread.Sleep(500);
            string currentURL = wdriver.Url;
            Thread.Sleep(500);
            CloseSpinnerOnExtranet();
            Assert.IsTrue(currentURL.Contains("media"));
        }

        //Verify Download Training Guide Details On home Page 
        public void VerifyHealthAndSafetyDivOnHomePage()
        {
            WaitForElement(ExtranetHomePage.HomeLink);
            ExtranetHomePage.HomeLink.Click();
            CloseSpinnerOnExtranet();
            Thread.Sleep(500);
            WaitForElement(HealthAndSafetydiv);
            HealthAndSafetydiv.Click();

            wdriver.SwitchTo().Window(wdriver.WindowHandles.Last());
        }

        public void VerifyOrdersCountOnHomePage()
        {
            WaitForElement(ExtranetHomePage.HomeLink);
            ExtranetHomePage.HomeLink.Click();
            WaitForElement(OrdersCount);
            CloseSpinnerOnExtranet();
            if (OrdersCount.Text == "0")
            {
                Assert.IsTrue(OrdersMessage.Text.Contains("You don't currently have any active orders."));
                Assert.IsFalse(GetACertificate.Displayed,"Get A Certificate Displayed Even Order Count Is '0'");
            }
            else
            {
                CloseSpinnerOnExtranet();
                Assert.IsTrue(GetACertificate.Displayed, "Failed to display Get A Certficate Button");
                int actualOrdersCount = 0;
                WaitForElement(OrdersCount);
                string count = OrdersCount.Text;
                int ordersCount = Convert.ToInt32(count);
                var reference = Statics.OrderNumber;
                WaitForElements(OrdersTablePaginateButtons);                
                if (ordersCount <= 3)
                {
                    Assert.AreEqual(ordersCount, OrdersTableCount.Count);
                    Assert.IsTrue(OrdersTablePaginateButtons.Count == 3);
                    VerifyOrderTableHeaders();
                }
                else
                {
                    VerifyOrderTableHeaders();
                    int numberOfPages = Convert.ToInt32(OrdersTablePaginateLastButton.Text.ToString());
                    actualOrdersCount = 3;
                    for (int i = 0; i < numberOfPages - 1; i++)
                    {
                        WaitForElement(OrdersTableNextPaginate);
                        ExtranetHomePage.OrdersTableNextPaginate.Click();
                        WaitForElements(OrdersTableCount);
                        //Thread.Sleep(500);
                        CloseSpinnerOnExtranet();
                        Thread.Sleep(500);
                        actualOrdersCount += OrdersTableCount.Count;
                        //Thread.Sleep(500);
                        CloseSpinnerOnExtranet();
                        Thread.Sleep(500);
                    }
                    Assert.AreEqual(actualOrdersCount, ordersCount);
                }

                Assert.IsTrue(OrdersTablePrevousPaginate.Displayed);
                Assert.IsTrue(OrdersTableNextPaginate.Displayed);
            }
        }
        public void VerifyOrderTableHeaders()
        {
            if(OrdersTable.Count>0)
            {
                WaitForElements(OrdersTableHeaders);
                List<string> OrderTableColumns = OrdersTableHeaders.Select(i => i.Text).ToList();
                Assert.IsTrue(OrderTableColumns.Any(o => o.Contains("Reference")));
                Assert.IsTrue(OrderTableColumns.Any(o => o.Contains("Site Address")));
                Assert.IsTrue(OrderTableColumns.Any(o => o.Contains("Plots")));           
                Assert.IsTrue(OrderTableColumns.Any(o => o.Contains("Actions")));
                Assert.IsTrue(OrderTableColumns.Any(o => o.Contains("Urgent Actions")));
            }
        }
        //Verify Quotes Table On home Page 
        public void VerifySelectQuoteUsingRefNumberFromHomePage()
        {
            WaitForElement(ExtranetHomePage.HomeLink);
            ExtranetHomePage.HomeLink.Click();
            WaitForElement(QuotesCount);
            if (QuotesCount.Text == "0")
            {
                Assert.IsTrue(QuotesMessage.Text.Contains("You don't currently have any quotes."));
                Assert.IsTrue(GetAQuote.Displayed);
            }
            else
            {
                int actualQuoteCount = 0;
                int quotesCount = Convert.ToInt32(QuotesCount.Text.ToString());
                WaitForElements(QuotesTablePaginateButtons);
                int numberOfPages = Convert.ToInt32(QuotesTablePaginateLastButton.Text.ToString());
               
                if (quotesCount <= 3)
                {
                    Assert.AreEqual(quotesCount, QuotesTableCount.Count);
                    Assert.IsTrue(QuotesTablePaginateButtons.Count == 3);
                    SearchAndSelectQuoteFromHomePage();
                }
                else
                {
                    SearchAndSelectQuoteFromHomePage();
                }            
            }
        }

        public void VerifySelectOrderUsingRefNumberFromHomePage()
        {
            WaitForElement(ExtranetHomePage.HomeLink);
            ExtranetHomePage.HomeLink.Click();
            CloseSpinnerOnExtranet();
            Thread.Sleep(500);
            WaitForElement(OrdersCount);
            if (OrdersCount.Text == "0")
            {
                Assert.IsTrue(OrdersMessage.Text.Contains("You don't currently have any active orders."));
                Assert.IsFalse(GetACertificate.Displayed, "Get A Certificate Displayed Even Order Count Is '0'");
            }
            else
            {
                Assert.IsTrue(GetACertificate.Displayed, "Get A Certficate Button Not Displayed");
                int actualOrdersCount = 0;
                string count = OrdersCount.Text.ToString();
                int ordersCount = Convert.ToInt32(count);
                var reference = Statics.OrderNumber;
                WaitForElements(OrdersTablePaginateButtons);
                int numberOfPages = Convert.ToInt32(OrdersTablePaginateLastButton.Text.ToString());
                if (ordersCount <= 3)
                {
                    Assert.AreEqual(ordersCount, OrdersTableCount.Count);
                    Assert.IsTrue(OrdersTablePaginateButtons.Count == 3);
                    SearchAndSelectOrderFromOrderTable();
                }
                else
                {
                    SearchAndSelectOrderFromOrderTable();
                }               
            }
        }
        
        public void SelectOrderUsingActionsFromHomePage()
        {
            int actualOrdersCount = 0;
            int ordersCount = Convert.ToInt32(ExtranetHomePage.OrdersCount.Text.ToString());
            var reference = Statics.OrderNumber;
            WaitForElements(ExtranetHomePage.OrdersTablePaginateButtons);
            int numberOfPages = Convert.ToInt32(ExtranetHomePage.OrdersTablePaginateLastButton.Text.ToString());
            if (numberOfPages > 3)
            {
                actualOrdersCount = 3;
                for (int i = 0; i < numberOfPages - 1; i++)
                {
                    IList<string> OrderRecordsWithRefs = OrdersRecordWithActions.Select(o => o.Text).ToList();
                    foreach (var row in OrdersRecordWithActions)
                    {
                        int rowValue = Convert.ToInt32(row.Text);

                        if (rowValue >0)
                        {
                            row.Click();
                            break;
                        }
                        else
                        {
                            WaitForElement(ExtranetHomePage.OrdersTableNextPaginate);
                            ExtranetHomePage.OrdersTableNextPaginate.Click();                           
                            if (rowValue > 0)
                            {
                                row.Click();
                                break;
                            }
                        }
                    }
                }
            }
            else
            {
                IList<String> OrderRecordsWithRefs = OrdersRecordsWithRefNumber.Select(o => o.Text).ToList();

                foreach (var row in OrdersRecordWithActions)
                {
                    int rowValue = Convert.ToInt32(row.Text);

                    if (rowValue > 0)
                    {
                        row.Click();
                        CloseSpinnerOnExtranet();
                        break;
                    }
                    else
                    {
                        WaitForElement(ExtranetHomePage.OrdersTableNextPaginate);
                        ExtranetHomePage.OrdersTableNextPaginate.Click();
                        if (rowValue > 0)
                        {
                            row.Click();
                            CloseSpinnerOnExtranet();
                            break;
                        }
                    }
                }     
            }
        }
        public void SelectOrderUsingUrgentActionsFromHomePage()
        {
            int actualOrdersCount = 0;
            int ordersCount = Convert.ToInt32(ExtranetHomePage.OrdersCount.Text.ToString());
            var reference = Statics.OrderNumber;
            WaitForElements(ExtranetHomePage.OrdersTablePaginateButtons);
            int numberOfPages = Convert.ToInt32(ExtranetHomePage.OrdersTablePaginateLastButton.Text.ToString());
            if (numberOfPages > 3)
            {
                actualOrdersCount = 3;
                for (int i = 0; i < numberOfPages - 1; i++)
                {
                    IList<string> OrderRecordsWithRefs = OrdersRecordWithUrgentActions.Select(o => o.Text).ToList();

                    foreach (var row in OrdersRecordWithUrgentActions)
                    {
                        int rowValue = Convert.ToInt32(row.Text);

                        if (rowValue > 0)
                        {
                            row.Click();
                            CloseSpinnerOnExtranet();
                            break;
                        }
                        else
                        {
                            WaitForElement(ExtranetHomePage.OrdersTableNextPaginate);
                            ExtranetHomePage.OrdersTableNextPaginate.Click();
                            if (rowValue > 0)
                            {
                                row.Click();
                                CloseSpinnerOnExtranet();
                                break;
                            }
                        }
                    }
                }
            }
            else
            {
                IList<String> OrderRecordsWithRefs = OrdersRecordsWithRefNumber.Select(o => o.Text).ToList();
                foreach (var row in OrdersRecordsWithRefNumber)
                {
                    if (OrderRecordsWithRefs.Contains(Statics.SiteRefNumber))
                    {
                        row.Click();
                        CloseSpinnerOnExtranet();
                        break;
                    }
                }
            }
        }
        public void SearchAndSelectOrderFromOrderTable()
        {          
            WaitForElement(OrdersCount);
            if (OrdersCount.Text != "0")
            {
                WaitForElement(SearchForOrder);
                SearchForOrder.Click();
                WaitForElement(SearchForOrderInput);
                SearchForOrderInput.SendKeys(Statics.OrderNumber);
                CloseSpinnerOnExtranet();
                Thread.Sleep(500);
                WaitForElements(OrdersRecordList);
                var order = OrdersRecordList.FirstOrDefault(x => x.Text.Contains(Statics.OrderNumber));
                if (order != null)
                {
                    order.Click();
                    CloseSpinnerOnExtranet();
                    Thread.Sleep(1000);
                }
            }

        }
        public void SearchAndSelectQuoteFromHomePage()
        {         
            WaitForElement(QuotesCount);
            if (QuotesCount.Text != "0")
            {
                WaitForElement(SearchForQuoteOnHomePage);
                SearchForQuoteOnHomePage.Click();
                WaitForElement(SearchForQuoteInput);
                SearchForQuoteInput.SendKeys(Statics.QuoteNumber);
                CloseSpinnerOnExtranet();
                Thread.Sleep(500);
                WaitForElements(QuotesRecordList);
                var quote = QuotesRecordList.FirstOrDefault(x => x.Text.Contains(Statics.QuoteNumber));
                if (quote != null)
                {
                    CloseSpinnerOnExtranet();
                    Thread.Sleep(500);
                    quote.Click();
                    CloseSpinnerOnExtranet();
                    Thread.Sleep(1000);
                }
            }

        }
    }
}
