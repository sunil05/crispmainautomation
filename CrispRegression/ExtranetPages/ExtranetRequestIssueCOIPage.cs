﻿using CrispAutomation.Features;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.PageObjects;
using Protractor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace CrispAutomation.ExtranetPages
{
    public class ExtranetRequestIssueCOIPage : Support.Pages
    {
        public IWebDriver wdriver;
        public NgWebDriver ngDriver;
        public ExtranetRequestIssueCOIPage(ISearchContext driver)
        {
            PageFactory.InitElements(driver, this);
            wdriver = (RemoteWebDriver)driver;
        }

        [FindsBy(How = How.XPath, Using = "//ul[@class='user-controls']//li[@id='getCertificate']")]
        public IList<IWebElement> GetACertificateButton;

        [FindsBy(How = How.XPath, Using = "//div[@id='products']//span[@data-tooltip='Select one or many plots to see if a Certificate of Insurance can be issued']")]
        public IList<IWebElement> GetACertificateButtonOnOrderTable;


        [FindsBy(How = How.XPath, Using = "//table[@id='ordersTable']//tbody//tr[@role='row']//td//span")]
        public IList<IWebElement> GetACertificateButtonOnPlotTable;

        [FindsBy(How = How.XPath, Using = "//table[@id='ordersTable']//tbody//tr//td//span[contains(@onclick,'RequestCoi')]//parent::td")]
        public IWebElement RequestCOIFromOrder;

        [FindsBy(How = How.XPath, Using = "//div[@id='requestCoiModel'][contains(@style,'block')]//div[@class='modal-content']")]
        public IWebElement COIDialogueWindow;

        [FindsBy(How = How.XPath, Using = "//div[@id='orderSelectorContainer']//h5[text()='Please Choose an Order']")]
        public IList<IWebElement> OrderTableList;

        [FindsBy(How = How.Id, Using = "selectOrdersTable_wrapper")]
        public IWebElement OrderTable;

        [FindsBy(How = How.XPath, Using = "//table[@id='selectOrdersTable']//tbody//tr//td[1]//a")]
        public IList<IWebElement> OrdersRecordList;

        [FindsBy(How = How.XPath, Using = "//div[@id='selectOrdersTable_paginate']//ul[@class='pagination']//li//a[@aria-controls='selectOrdersTable']")]
        public IList<IWebElement> COIOrderTablePaginateButtons;

        [FindsBy(How = How.XPath, Using = "//div[@id='selectOrdersTable_paginate']//ul[@class='pagination']//li[contains(@class,'paginate_button next')]//preceding-sibling::li[1]//a")]
        public IWebElement COIOrderTablePaginateLastButton;

        [FindsBy(How = How.XPath, Using = "//div[@id='selectOrdersTable_paginate']//ul[@class='pagination']//li[@id='ordersTable_next']//a")]
        public IWebElement COIOrderTableNextPaginate;

        [FindsBy(How = How.XPath, Using = "//div[@id='selectOrdersTable_filter']//label")]
        public IWebElement SearchForOrderOnCOITable;

        [FindsBy(How = How.XPath, Using = "//div[@id='selectOrdersTable_filter']//label//input[@aria-controls='selectOrdersTable']")]
        public IWebElement SearchForOrderInputOnCOITable;

        [FindsBy(How = How.XPath, Using = "//div[@id='plotSelectionContainer'][contains(@style,'block')]")]
        public IList<IWebElement> COIPlotSection;        

        [FindsBy(How = How.Id, Using = "coiPlotsTable_wrapper")]
        public IWebElement AvailablePlotsContainer;

        [FindsBy(How = How.XPath, Using = "//table[@id='coiPlotsTable']//tbody//tr[@role='row']")]
        public IList<IWebElement> AvailablePlots;

        [FindsBy(How = How.XPath, Using = "//div[@id='coiPlotsTable_wrapper']//div//button[@aria-controls='coiPlotsTable']//span[text()='Select all Plots']")]
        public IWebElement SelectAllPlotsButton;

        [FindsBy(How = How.Id, Using = "selectedPlotsContainer")]
        public IWebElement SelectedPlotsContainer;

        [FindsBy(How = How.XPath, Using = "//div[@id='coiPlotsTable_filter']//label//input[@type='search']")]
        public IWebElement SearchOnCoiTable;

        [FindsBy(How = How.XPath, Using = "//div[@id='selectedPlotsContainer']//div//button[@aria-controls='selectedPlotsTable']//span[text()='Remove all plots']")]
        public IWebElement RemoveAllPlotsButton;

        [FindsBy(How = How.XPath, Using = "//table[@id='coiPlotsTable']")]
        public IWebElement COIPlotsTable;

        [FindsBy(How = How.XPath, Using = "//table[@id='selectedPlotsTable']")]
        public IWebElement SelectedPlotsTable;

        [FindsBy(How = How.XPath, Using = "//div[@id='selectedPlotsTable_filter']//label//input[@type='search']")]
        public IWebElement SearchOnSelecedPlotsTable;

        [FindsBy(How = How.XPath, Using = "//table[@id='selectedPlotsTable']//tr//td[text()='No data available in table']")]
        public IWebElement NoDataAvailableOnSelectedPlots;

        [FindsBy(How = How.XPath, Using = "//table[@id='coiPlotsTable']//tr//td[text()='No data available in table']")]
        public IWebElement NoDataAvailableOnCOIPlots;

        [FindsBy(How = How.XPath, Using = "//button[@id='run-file-review-btn']//span[contains(text(),'Next')]//parent::button")]
        public IWebElement NextButton;

        [FindsBy(How = How.XPath, Using = "//div[@id='requestCoiModel']//button[@aria-label='Close']")]
        public IWebElement CloseButton;

        [FindsBy(How = How.Id, Using = "plotsSelected")]
        public IWebElement PlotsSelected;

        [FindsBy(How = How.XPath, Using = "//div[@id='conditions']//div[@class='header container-header']//h2[contains(text(),'Outstanding Items Preventing Your Certificates Being Issued')]")]
        public IWebElement OutstandngConditionsMessage;

        [FindsBy(How = How.XPath, Using = "//div[@id='conditionsContainer']//table[@id='conditionsTable']//tbody//tr[@role='row']//td[1]")]
        public IList<IWebElement> ConditionsRows;

        [FindsBy(How = How.XPath, Using = "//div[@id='processFileReviewContainer']//h4[@id='mainDescription']")]
        public IWebElement COIMessage;

        [FindsBy(How = How.Id, Using = "request-coi-btn")]
        public IWebElement COISendButton;

        [FindsBy(How = How.XPath, Using = "//button[text()='OK']")]
        public IWebElement COISubmittedOKButton;


        [FindsBy(How = How.XPath, Using = "//div[@id='requestCoiModel']//div[@class='modal-content']//h5//span[text()='Request Certificate of Insurance']")]
        public IList<IWebElement> ChooseOrderTable;
        

        public static List<string> OrderNumbers { get; set; }


        public void RequestCOIfromHomePage()
        {
            SelectGetACertficateButtonFromHomePage();
            VerifyRequestCOIWizard();
            SelectOrderToRequestIssueCOI();
            VerifyTasksInTheCrisp();
            IssueCertificatePage.COIForExtranet();
        }
        public void RequestCOIfromOrdersPage()
        {
            SelectGetACertficateFromOrderTable();
            VerifyRequestCOIWizard();
            SelectOrderToRequestIssueCOI();
            
        }
        public void RequestCOIfromPlotData()
        {
            SelectGetACertficateFromPlotTable();
            VerifyRequestCOIWizard();
            SelectOrderToRequestIssueCOI();
            VerifyTasksInTheCrisp();
            IssueCertificatePage.COIForExtranet();
        }
        public void SelectGetACertficateButtonFromHomePage()
        {
            WaitForElement(ExtranetHomePage.HomeLink);
            ExtranetHomePage.HomeLink.Click();
            if (GetACertificateButton.Count > 0)
            {
                WaitForElements(GetACertificateButton);
                GetACertificateButton[0].Click();
                CloseSpinnerOnExtranet();
                WaitForElement(COIDialogueWindow);
                Assert.IsTrue(COIDialogueWindow.Displayed);
                CloseSpinnerOnExtranet();
                if (OrderTableList.Count > 0)
                {
                    WaitForElement(OrderTable);
                    WaitForElements(OrdersRecordList);
                    CloseSpinnerOnExtranet();
                    if (OrdersRecordList.Count > 0)
                    {
                        WaitForElements(OrdersRecordList);
                        CloseSpinnerOnExtranet();
                        SearchAndSelectOrderToRequestFromCOITable();
                        CloseSpinnerOnExtranet();
                    }
                }
            }
        }
        public void SelectGetACertficateFromOrderTable()
        {         
            CloseSpinnerOnExtranet();
            Thread.Sleep(500);
            if(ChooseOrderTable.Count>0)
            {
                WaitForElement(SearchForOrderOnCOITable);
                SearchForOrderOnCOITable.Click();
                WaitForElement(SearchForOrderInputOnCOITable);
                SearchForOrderInputOnCOITable.SendKeys(Statics.OrderNumber);
                CloseSpinnerOnExtranet();
                Thread.Sleep(500);
                WaitForElements(OrdersRecordList);
                var order = OrdersRecordList.FirstOrDefault(x => x.Text.Contains(Statics.OrderNumber));
                if (order != null)
                {
                    order.Click();
                    CloseSpinnerOnExtranet();
                    Thread.Sleep(1000);
                }
            }
            
            if (GetACertificateButtonOnOrderTable.Count > 0)
            {
                WaitForElements(GetACertificateButtonOnOrderTable);
                GetACertificateButtonOnOrderTable[0].Click();
                WaitForElement(COIDialogueWindow);
                Assert.IsTrue(COIDialogueWindow.Displayed);
                WaitForElement(OrderTable);
                WaitForElements(OrdersRecordList);
                if (OrdersRecordList.Count > 0)
                {
                    WaitForElements(OrdersRecordList);
                    SearchAndSelectOrderToRequestFromCOITable();
                }
            }
        }
        public void SelectGetACertficateFromPlotTable()
        {
            WaitForElement(ExtranetHomePage.OrdersLink);
            ExtranetHomePage.OrdersLink.Click();
            if (GetACertificateButtonOnPlotTable.Count > 0)
            {
                WaitForElements(GetACertificateButtonOnPlotTable);
                GetACertificateButtonOnPlotTable[0].Click();
                CloseSpinnerOnExtranet();
                WaitForElement(COIDialogueWindow);
                Assert.IsTrue(COIDialogueWindow.Displayed);
                WaitForElement(OrderTable);
                WaitForElements(OrdersRecordList);
                if (OrdersRecordList.Count > 0)
                {
                    WaitForElements(OrdersRecordList);
                    SearchAndSelectOrderToRequestFromCOITable();
                }
            }
        }
        public void SearchAndSelectOrderToRequestFromCOITable()
        {
          
            WaitForElement(SearchForOrderOnCOITable);
            SearchForOrderOnCOITable.Click();
            WaitForElement(SearchForOrderInputOnCOITable);
            SearchForOrderInputOnCOITable.SendKeys(Statics.OrderNumber);            
            CloseSpinnerOnExtranet();
            Thread.Sleep(500);
            WaitForElements(OrdersRecordList);
            var order = OrdersRecordList.FirstOrDefault(x => x.Text.Contains(Statics.OrderNumber));
            if (order!=null)
            {
                order.Click();
                CloseSpinnerOnExtranet();
                Thread.Sleep(1000);
            }            
        }       
        public void VerifyRequestCOIWizard()
        {
            if(COIPlotSection.Count>0)
            {
                WaitForElement(AvailablePlotsContainer);
                Assert.IsTrue(AvailablePlotsContainer.Displayed);
                Assert.IsTrue(SelectAllPlotsButton.Displayed);
                Assert.IsTrue(SearchOnCoiTable.Displayed);
                WaitForElement(SelectedPlotsContainer);
                Assert.IsTrue(SelectedPlotsContainer.Displayed);
                Assert.IsTrue(RemoveAllPlotsButton.Displayed);
                Assert.IsTrue(SearchOnSelecedPlotsTable.Displayed);
            }
            CloseSpinnerOnExtranet();
            Thread.Sleep(1000);

        }
        public void SelectOrderToRequestIssueCOI()
        {
            CloseSpinnerOnExtranet();
            Thread.Sleep(1000);
            if (AvailablePlots.Count > 0)
            {
                WaitForElement(SelectAllPlotsButton);
                SelectAllPlotsButton.Click();
                WaitForElement(NextButton);
                NextButton.Click();
                CloseSpinnerOnExtranet();
                WaitForElement(PlotsSelected);
                Assert.IsTrue(PlotsSelected.Displayed);
                if (ConditionsRows.Count > 0)
                {
                    VerifyandCloseConditions();
                    RequestCOIWhenConditionsClosed();
                }
                else
                {
                    RequestCOIWhenConditionsClosed();
                }
            }
            else
            {
                SearchAndSelectOrderToRequestFromCOITable();
            }
        }
        //public void SendCOIRequestToCSU()
        //{
        //    if (AvailablePlots.Count > 0)
        //    {
        //        WaitForElement(SelectAllPlotsButton);
        //        SelectAllPlotsButton.Click();
        //        WaitForElement(NextButton);
        //        NextButton.Click();
        //        CloseSpinnerOnExtranet();
        //        WaitForElement(PlotsSelected);
        //        Assert.IsTrue(PlotsSelected.Displayed);
        //        if (ConditionsRows.Count > 0)
        //        {
        //            VerifyandCloseConditions();
        //            RequestCOIWhenConditionsClosed();
        //        }
        //        else
        //        {
        //            RequestCOIWhenConditionsClosed();
        //        }
        //    }
        //    else
        //    {
        //        SearchAndSelectOrderToRequestFromCOITable();
        //    }
        //}
        public void VerifyTasksInTheCrisp()
        {
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            if (Dashboardpage.DecisionButton.Count <= 0)
            {
                Dashboardpage.SelectOrder();
                CloseSpinneronDiv();
                CloseSpinneronPage();
                Thread.Sleep(500);
            }           
            WaitForElement(TasksPage.TasksTab);
            TasksPage.TasksTab.Click();
            WaitForElement(TasksPage.TasksRefreshButton);
            TasksPage.TasksRefreshButton.Click();
            WaitForLoadElements(TasksPage.TasksList);
            Assert.IsTrue(TasksPage.TasksList.Any(x => x.Text.Contains("Extranet - Issue Certificate of Insurance requested")));
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
        }
        public void VerifyCOIDocOnExtranet()
        {
            ExtranetLoginPage.SelectExtranetSite();
        }
        public void VerifyandCloseConditions()
        {
            WaitForElement(OutstandngConditionsMessage);
            Assert.IsTrue(OutstandngConditionsMessage.Displayed);
            WaitForElements(ConditionsRows);
            Statics.ExtranetConditions = ConditionsRows.Select(i => i.Text).ToList();
            WaitForElement(CloseButton);
            CloseButton.Click();
            CloseSpinnerOnExtranet();
            CloseConditionsOnCrisp();
        }
        public void RequestCOIWhenConditionsClosed()
        {
            ExtranetLoginPage.SelectExtranetSite();
            CloseSpinnerOnExtranet();
            Thread.Sleep(500);
            SelectGetACertficateButtonFromHomePage();
            CloseSpinnerOnExtranet();
            Thread.Sleep(500);
            WaitForElement(SelectAllPlotsButton);
            SelectAllPlotsButton.Click();
            CloseSpinnerOnExtranet();
            Thread.Sleep(500);
            WaitForElement(NextButton);
            NextButton.Click();
            CloseSpinnerOnExtranet();
            Thread.Sleep(500);
            WaitForElement(PlotsSelected);
            Assert.IsTrue(PlotsSelected.Displayed);
            if (ConditionsRows.Count == 0)
            {
                WaitForElement(COIMessage);
                var coiMessage = COIMessage.Text;
                Assert.IsTrue(coiMessage.Contains("Your request to issue the Certificates of Insurance will be referred to your Customer Services Account handler who will email you the Certificates of Insurance."));
                WaitForElement(COISendButton);
                COISendButton.Click();
                CloseSpinnerOnExtranet();
                Thread.Sleep(500);        
                WaitForElement(COISubmittedOKButton);
                COISubmittedOKButton.Click();
                CloseSpinnerOnExtranet();
                Thread.Sleep(500);
            }
        }
        public void CloseConditionsOnCrisp()
        {           
            VerifyExtranetConditionsOnCrisp();
            ConditionsPage.CloseConditionsForExtranet();
        }
        public void VerifyExtranetConditionsOnCrisp()
        {
            if (Dashboardpage.DecisionButton.Count <= 0)
            {
                Dashboardpage.SelectOrder();
            }
            WaitForElement(ConditionsPage.ConditionsTab);
            ConditionsPage.ConditionsTab.Click();
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            if (ConditionsPage.ConditionsTable.Count > 0)
            {
                WaitForElements(ConditionsPage.ConditionsRows);
                if (ConditionsPage.ConditionsRows.Count > 0)
                {
                    Statics.ConditionsList = ConditionsPage.ConditionsRows.Select(i => i.Text.ToString()).ToList();                    
                }
                for (int i=0; i<Statics.ExtranetConditions.Count; i++)
                {
                    Assert.IsTrue(Statics.ConditionsList.Contains(Statics.ExtranetConditions[i]),$"{Statics.ExtranetConditions[i]} is not exist on crisp");
                }                
            }
        }
    }
}
