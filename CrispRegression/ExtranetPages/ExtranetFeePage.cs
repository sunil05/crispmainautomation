﻿using CrispAutomation.Features;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.PageObjects;
using Protractor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace CrispAutomation.Pages
{
    public class ExtranetFeePage : Support.Pages
    {
        public IWebDriver wdriver;
        public NgWebDriver ngDriver;
        public ExtranetFeePage(ISearchContext driver)
        {
            PageFactory.InitElements(driver, this);
            wdriver = (RemoteWebDriver)driver;
        }
        [FindsBy(How = How.XPath, Using = "//crisp-tabs//div//ul[@ref='tabHeader'][@class='au-target tabs highlight-background']//li[@class='au-target tab']//span//a[contains(text(),'Account')][contains(@class,'au-target highlight-colour')]")]
        public IWebElement AccountTab;

        [FindsBy(How = How.XPath, Using = "//div//account-summary[@summary.bind='summary']//dl/dt[text()='Fee balance']//following-sibling::dd[1]")]
        public IWebElement FeeBalance;
        [FindsBy(How = How.XPath, Using = "//div//account-summary[@summary.bind='summary']//dl/dt[text()='Fee due']//following-sibling::dd[1]")]
        public IWebElement FeeDue;
        [FindsBy(How = How.XPath, Using = "//div//account-summary[@summary.bind='summary']//dl/dt[text()='Fee paid']//following-sibling::dd[1]")]
        public IWebElement FeePaid;
        [FindsBy(How = How.XPath, Using = "//div//account-summary[@summary.bind='summary']//dl/dt[text()='Escrow balance']//following-sibling::dd[1]")]
        public IWebElement EscrowBalance;
        [FindsBy(How = How.XPath, Using = "//div//account-summary[@summary.bind='summary']//dl/dt[text()='Escrow due']//following-sibling::dd[1]")]
        public IWebElement EscrowDue;
        [FindsBy(How = How.XPath, Using = "//div//account-summary[@summary.bind='summary']//dl/dt[text()='Escrow paid']//following-sibling::dd[1]")]
        public IWebElement EscrowPaid;
        [FindsBy(How = How.XPath, Using = "//div[@id='outstandingfees-info-tile']//span[contains(@class,'reg-stat-container-stat')]")]
        public IWebElement ExtranetTotalFee;

     

        public void GetFeeDetailsFromCrisp()
        {
            if (Dashboardpage.DecisionButton.Count <= 0)
            {
                Dashboardpage.SelectOrder();
            }
            //Thread.Sleep(1500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            WaitForElement(AccountTab);
            WaitForElementToClick(AccountTab);
            AccountTab.Click();
            //Thread.Sleep(1000);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            WaitForElement(FeeBalance);
            //Thread.Sleep(1000);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            var FeeBalanceValue = FeeBalance.Text.Replace("£", "");
            double actualFeeBal = Convert.ToDouble(FeeBalanceValue);
            //Thread.Sleep(1000);
            Statics.CrispFeeBalance = Math.Round(actualFeeBal,2);
            var FeeDueValue = FeeDue.Text.Replace("£", "");
            double actualFeeDue = Convert.ToDouble(FeeDueValue);
            //Thread.Sleep(1000);
            Statics.CrispFeeDue = Math.Round(actualFeeDue,2);
            var FeePaidValue = FeePaid.Text.Replace("£", "");
            double actualFeePaid = Convert.ToDouble(FeePaidValue);
            //Thread.Sleep(1000);
            Statics.CrispFeepaid = Math.Round(actualFeePaid,2);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            WaitForElement(EscrowBalance);
            var EscrowBalanceValue = EscrowBalance.Text.Replace("£", "");
            double actualEscrowBal = Convert.ToDouble(EscrowBalanceValue);
            Statics.CrispEscrowBalance = Math.Round(actualEscrowBal,2);
            //Thread.Sleep(1000);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(1000);
            var EscrowDueValue = EscrowDue.Text.Replace("£", "");
            double actualEscrowDue = Convert.ToDouble(EscrowDueValue);
            //Thread.Sleep(1000);
            Statics.CrispEscrowDue = Math.Round(actualEscrowDue,2);
            var EscrowPaidValue = EscrowPaid.Text.Replace("£", "");
            double acualEscrowPaid = Convert.ToDouble(EscrowPaidValue);
            //Thread.Sleep(1000);
            Statics.CrispEscrowPaid = Math.Round(acualEscrowPaid,2);
            double totalFeeValue = Convert.ToDouble(Statics.CrispFeeBalance + Statics.CrispEscrowBalance);
            Statics.CrispTotalFee = Math.Round(totalFeeValue,2);
        }

        public void GetandVerifyFeeDetailsOnExtranet()
        {
            ExtranetLoginPage.SelectExtranetSite();
            ExtranetHomePage.SearchAndSelectOrderFromOrderTable();
            WaitForElement(ExtranetOrdersPage.FeesTile);
            ExtranetOrdersPage.FeesTile.Click();
            Thread.Sleep(500);
            CloseSpinnerOnExtranet();
            Thread.Sleep(500);
            Assert.IsTrue(ExtranetOrdersPage.FeesTileHeader.Text.Contains("FEES"));
            string feeValue = ExtranetOrdersPage.FeesTileStat.Text.ToString();
            string fee = feeValue.Replace("£", "");
            Statics.ExtranetFee = Math.Round(Convert.ToDouble(fee.ToString()),2);            
            TotalFeesDetailsOnExtranet();
            EscrowFeeDetailsOnExtranet();
            VerifyFeeDetailsOnExtranet();
        }
        public void TotalFeesDetailsOnExtranet()
        {
            WaitForElement(ExtranetOrdersPage.TotalFeesDue);
            string totalfees = ExtranetOrdersPage.TotalFeesDue.Text.ToString();
            string totalfeesDue = totalfees.Replace("£", "");
            WaitForElement(ExtranetOrdersPage.TotalFeesOutstanding);
            string totalFeeOutstanding = ExtranetOrdersPage.TotalFeesOutstanding.Text;
            string totalFeesOutstanding = totalFeeOutstanding.Replace("£", "");
            WaitForElement(ExtranetOrdersPage.TotalFeesReceivedToDate);
            string totalFeeReceivedToDate = ExtranetOrdersPage.TotalFeesReceivedToDate.Text;
            string totalFeesReceivedToDate = totalFeeReceivedToDate.Replace("£", "");
            Statics.ExtranetTotalFeeDue = Math.Round(Convert.ToDouble(totalfeesDue.ToString()),2);
            Statics.ExtranetTotalFeeOutstanding = Math.Round(Convert.ToDouble(totalFeesOutstanding.ToString()),2);
            Statics.ExtranetTotalFeeReceivedToDate = Math.Round(Convert.ToDouble(totalFeesReceivedToDate.ToString()),2);

        }
        public void EscrowFeeDetailsOnExtranet()
        {
            WaitForElement(ExtranetOrdersPage.EscrowFeesDue);
            string totalEscrowFeeDue = ExtranetOrdersPage.EscrowFeesDue.Text;
            string totalEscrowFeesDue = totalEscrowFeeDue.Replace("£","");
            WaitForElement(ExtranetOrdersPage.EscrowFeesOutstanding);
            string totalEscrowFeeOutstanding = ExtranetOrdersPage.EscrowFeesOutstanding.Text;
            string totalEscrowFeesOutstanding = totalEscrowFeeOutstanding.Replace("£", "");
            WaitForElement(ExtranetOrdersPage.EscrowFeesReceivedToDate);
            string totalEscrowFeeReceivedToDate = ExtranetOrdersPage.EscrowFeesReceivedToDate.Text;
            string totalEscrowFeesReceivedToDate = totalEscrowFeeReceivedToDate.Replace("£", "");
            Statics.ExtranetEscrowFeeDue = Math.Round(Convert.ToDouble(totalEscrowFeesDue.ToString()),2);
            Statics.ExtranetEscrowFeeOutstanding = Math.Round(Convert.ToDouble(totalEscrowFeesOutstanding.ToString()),2);
            Statics.ExtranetEscrowFeeReceivedToDate = Math.Round(Convert.ToDouble(totalEscrowFeesReceivedToDate.ToString()),2);
        }       
        public void VerifyFeeDetailsOnExtranet()
        {
            if (Statics.CrispTotalFee >= 0)
            {
                Assert.AreEqual(Statics.CrispTotalFee, Statics.ExtranetFee, "Total Fee Details Are Not Matching on Extranet");

            }
            Assert.AreEqual(Statics.CrispFeeDue, Statics.ExtranetTotalFeeDue, "Total Fee Due Details Are Not Matching on Extranet");
            Assert.AreEqual(Statics.CrispFeeBalance, Statics.ExtranetTotalFeeOutstanding, "Total Fee Outstanding Details Are Not Matching on Extranet");
            Assert.AreEqual(Statics.CrispFeepaid, Statics.ExtranetTotalFeeReceivedToDate, "Total Fee Paid Details Are Not Matching on Extranet");
            Assert.AreEqual(Statics.CrispEscrowDue, Statics.ExtranetEscrowFeeDue, "Escrow Fee Due Details Are Not Matching on Extranet");           
            Assert.AreEqual(Statics.CrispEscrowBalance, Statics.ExtranetEscrowFeeOutstanding, "Escrow Fee Outstanding Details Are Not Matching on Extranet");
            Assert.AreEqual(Statics.CrispEscrowPaid, Statics.ExtranetEscrowFeeReceivedToDate, "Escrow Fee Paid Details Are Not Matching on Extranet");
        }
    }
}
