﻿using CrispAutomation.Features;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.PageObjects;
using Protractor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;

namespace CrispAutomation.ExtranetPages
{
    public class ExtranetOrdersPage : Support.Pages
    {
        public IWebDriver wdriver;
        public NgWebDriver ngDriver;
        public ExtranetOrdersPage(ISearchContext driver)
        {
            PageFactory.InitElements(driver, this);
            wdriver = (RemoteWebDriver)driver;
            ngDriver = new NgWebDriver(wdriver);
        }

        [FindsBy(How = How.XPath, Using = "//section//aside[@id='leftsidebar']//div[@class='slimScrollDiv']//ul[@class='list']//li//span[text()='Orders']")]
        public IWebElement OrdersActiveLink;

        //Ordes Page Elements 
        [FindsBy(How = How.XPath, Using = "//section[@class='content']//div")]
        public IWebElement OrderDiv;
        [FindsBy(How = How.XPath, Using = "//section[@class='content']//div[@class='body']//ul[contains(@class,'order-stat-container')]//li[contains(@class,'top-stat')]")]
        public IWebElement OrderRefContainer;
        [FindsBy(How = How.XPath, Using = "//div[@id='OrdersHome']//h2")]
        public IWebElement YourOrders;
        [FindsBy(How = How.Id, Using = "orderTotal")]
        public IWebElement OrderTotalCount;
        [FindsBy(How = How.XPath, Using = "//div[@id='ordersTable_wrapper']//table[@id='ordersTable']")]
        public IWebElement OrderTable;
        [FindsBy(How = How.XPath, Using = "//table[@id='ordersTable']//thead//tr[@role='row']//th")]
        public IList<IWebElement> OrderTableHeader;
        [FindsBy(How = How.XPath, Using = "//table[@id='ordersTable']//thead//tr[@role='row']//th[text()='Reference']//parent::tr//parent::thead//following::tbody//tr[@role='row']//td[1]//a[@data-tooltip='Click to view this order']")]
        public IList<IWebElement> OrderTableRows;

        //Product Tile Elements 
        [FindsBy(How = How.Id, Using = "products-info-tile")]
        public IWebElement ProductsTile;
        [FindsBy(How = How.XPath, Using = "//div[@id='products-info-tile']//span[contains(@class,'reg-stat-container-header')]")]
        public IWebElement ProductsTileHeader;
        [FindsBy(How = How.XPath, Using = "//div[@id='products-info-tile']//span[contains(@class,'reg-stat-container-stat')]")]
        public IWebElement ProductsTileStat;
        [FindsBy(How = How.XPath, Using = "//div[@id='products']//h2[contains(text(),'Product Summary')]//parent::div//following-sibling::div[@class='body']")]
        public IWebElement ProductsSummary;
        [FindsBy(How = How.Id, Using = "orderSummaryTable")]
        public IWebElement ProductSummaryTable;
        [FindsBy(How = How.XPath, Using = "//table[@id='orderSummaryTable']//thead//tr[@role='row']//th")]
        public IList<IWebElement> ProductSummaryTableHeader;
        [FindsBy(How = How.XPath, Using = "//table[@id='orderSummaryTable']//tbody//tr[@role='row']")]
        public IList<IWebElement> ProductSummaryTableRows;
        [FindsBy(How = How.XPath, Using = "//table[@id='orderSummaryTable']//tbody//tr[@role='row']//td[@class=' expandProduct']")]
        public IWebElement ExpandProductSummaryTableRows;
        [FindsBy(How = How.XPath, Using = "//table[@id='orderSummaryTable']//tbody//tr//td//table//thead//tr//th[text()='Covers']")]
        public IWebElement ProductCovers;

        //Plot Tile Elements 
        [FindsBy(How = How.Id, Using = "plots-info-tile")]
        public IWebElement PlotsTile;
        [FindsBy(How = How.XPath, Using = "//div[@id='plots-info-tile']//span[contains(@class,'reg-stat-container-header')]")]
        public IWebElement PlotsTileHeader;
        [FindsBy(How = How.XPath, Using = "//div[@id='plots-info-tile']//span[contains(@class,'reg-stat-container-stat')]")]
        public IWebElement PlotsTileStat;
        [FindsBy(How = How.XPath, Using = "//div[@id='plots']//h2[contains(text(),'Plots')]//parent::div//following-sibling::div[@class='body']")]
        public IWebElement PlotsTableBody;
        [FindsBy(How = How.Id, Using = "plotTable_wrapper")]
        public IWebElement PlotsTable;
        [FindsBy(How = How.XPath, Using = "//table[@id='plotTable']//thead//tr[@role='row']//th")]
        public IList<IWebElement> PlotsTableHeader;
        [FindsBy(How = How.XPath, Using = "//table[@id='plotTable']//tbody//tr[@role='row']")]
        public IList<IWebElement> PlotsTableRows;
        [FindsBy(How = How.XPath, Using = "//div[@id='plotTable_info']")]
        public IWebElement PlotTableInfo;

        //Action Tile Elements 
        [FindsBy(How = How.Id, Using = "todo-info-tile")]
        public IWebElement ActionTile;
        [FindsBy(How = How.XPath, Using = "//div[@id='todo-info-tile']//span[contains(@class,'reg-stat-container-header')]")]
        public IWebElement ActionsTileHeader;
        [FindsBy(How = How.XPath, Using = "//div[@id='todo-info-tile']//span[contains(@class,'reg-stat-container-stat')]")]
        public IWebElement ActionsTileStat;
        [FindsBy(How = How.XPath, Using = "//div[@id='todo']//div[@id='to-do-section']//h2")]
        public IWebElement ActionRequired;
        [FindsBy(How = How.Id, Using = "toDoTable")]
        public IWebElement ToDoTable;
        [FindsBy(How = How.XPath, Using = "//table[@id='toDoTable']//thead//tr[@role='row']//th")]
        public IList<IWebElement> ToDoTableHeader;
        [FindsBy(How = How.XPath, Using = "//table[@id='toDoTable']//tbody//tr[@role='row']")]
        public IList<IWebElement> ToDoTableRows;
        [FindsBy(How = How.XPath, Using = "//div[@id='todo']//div[@id='to-do-section']//div[@class='body']//p")]
        public IWebElement OutstandingActions;
        [FindsBy(How = How.XPath, Using = "//div[@id='to-do-section']//div[@id='toDoTable_info']")]
        public IWebElement ToDoTableInfo;
        [FindsBy(How = How.XPath, Using = "//div[@id='todo']//div[@id='to-do-section']//div[@class='card']//div[@class='pull-right']//ul[@class='order-sub-btn-container-todo']//li[@data-filter-key='plotsNearCompletion']//span")]
        public IWebElement PlotsNearlyComplete;

        //Technical Summary Elements 
        [FindsBy(How = How.Id, Using = "technical-info-tile")]
        public IWebElement TechSummaryTile;
        [FindsBy(How = How.XPath, Using = "//div[@id='technical-info-tile']//span[contains(@class,'reg-stat-container-header')]")]
        public IWebElement TechSummaryTileHeader;
        [FindsBy(How = How.XPath, Using = "//div[@id='technical-info-tile']//span[contains(@class,'reg-stat-container-stat')]")]
        public IWebElement TechSummaryTileStat;
        [FindsBy(How = How.XPath, Using = "//div[@id='technical']//div[@id='technical-summary-section']//h2")]
        public IWebElement TechnicalSummaryTitle;
        [FindsBy(How = How.XPath, Using = "//div[@id='technical-summary-section']//div[@class='header container-header']//div//span[1]")]
        public IList<IWebElement> TechSummaryTableHeader;
        [FindsBy(How = How.XPath, Using = "//li[@id='docs-info-tile']//span[contains(text(),'Design Documents')]//span[contains(text(),'OPEN')]//span[@class='text-underline']")]
        public IWebElement DesignDocs;
        [FindsBy(How = How.XPath, Using = "//li[@id='items-info-tile']//span[contains(text(),'Design Items')]//span[contains(text(),'OPEN')]//span[@class='text-underline']")]
        public IWebElement DesignItems;
        [FindsBy(How = How.XPath, Using = "//li[@id='risk-info-tile']//span[contains(text(),'Risk Items')]//span[contains(text(),'OPEN')]//span[@class='text-underline']")]
        public IWebElement RiskItems;
        [FindsBy(How = How.XPath, Using = "//li[@id='completion-info-tile']//span[contains(text(),'Completion Docs')]//span[contains(text(),'OPEN')]//span[@class='text-underline']")]
        public IWebElement CompletionDocs;
        [FindsBy(How = How.XPath, Using = "//li[@id='defects-info-tile']//span[contains(text(),'Site Defects')]//span[contains(text(),'OPEN')]//span[@class='text-underline']")]
        public IWebElement SiteDefects;
        [FindsBy(How = How.Id, Using = "docs-info-tile")]
        public IWebElement DesignDocsInfoTile;        
        [FindsBy(How = How.XPath, Using = "//table[@id='docs']//thead//tr//th")]
        public IList<IWebElement> DesignDocsTableHeader;
        [FindsBy(How = How.XPath, Using = "//table[@id='docs']//thead//following-sibling::tbody//tr")]
        public IList<IWebElement> DesignDocsTableRows;
        [FindsBy(How = How.Id, Using = "items-info-tile")]
        public IWebElement DesignItemsInfoTile;
        [FindsBy(How = How.XPath, Using = "//table[@id='items']//thead//tr//th")]
        public IList<IWebElement> DesignItemsTableHeader;
        [FindsBy(How = How.XPath, Using = "//table[@id='designItems']//thead//following-sibling::tbody//tr")]
        public IList<IWebElement> DesignItemsTableRows;
        [FindsBy(How = How.Id, Using = "risk-info-tile")]
        public IWebElement RiskItemsInfoTile;
        [FindsBy(How = How.XPath, Using = "//table[@id='riskItems']//thead//tr//th")]
        public IList<IWebElement> RiskItemsTableHeader;
        [FindsBy(How = How.XPath, Using = "//table[@id='completion']//thead//following-sibling::tbody//tr")]
        public IList<IWebElement> RiskItemsTableRows;
        [FindsBy(How = How.Id, Using = "completion-info-tile")]
        public IWebElement CompletionDocsInfoTile;
        [FindsBy(How = How.XPath, Using = "//table[@id='completion']//thead//tr//th")]
        public IList<IWebElement> CompletionDocsTableHeader;
        [FindsBy(How = How.XPath, Using = "//table[@id='completion']//thead//following-sibling::tbody//tr")]
        public IList<IWebElement> CompletionDocsTableRows;
        [FindsBy(How = How.XPath, Using = "//table[@id='completion']//thead//tr//th")]
        public IList<IWebElement> SiteDefectsTableHeader;
        [FindsBy(How = How.XPath, Using = "//table[@id='completion']//thead//following-sibling::tbody//tr")]
        public IList<IWebElement> SiteDefectsTableRows;
        [FindsBy(How = How.Id, Using = "defects-info-tile")]
        public IWebElement SiteDefectsInfoTile;
        [FindsBy(How = How.XPath, Using = "//span[@id='noTechnicalItems'][text()='There are no design documents outstanding for this order']")]
        public IWebElement NoDesignDocs;
        [FindsBy(How = How.XPath, Using = "//span[@id='noTechnicalItems'][text()='There are no design items outstanding for this order']")]
        public IWebElement NoDesignItems;
        [FindsBy(How = How.XPath, Using = "//span[@id='noTechnicalItems'][text()='There are no risk items outstanding for this order']")]
        public IWebElement NoRiskItems;
        [FindsBy(How = How.XPath, Using = "//span[@id='noTechnicalItems'][text()='There are no completion documents outstanding for this order']")]
        public IWebElement NoCompletionDocs;
        [FindsBy(How = How.XPath, Using = "//span[@id='noTechnicalItems'][text()='There are no defects outstanding for this order']")]
        public IWebElement NoSiteDefects;

        //Site Inspection Elements 
        [FindsBy(How = How.Id, Using = "site-info-tile")]
        public IWebElement SiteInspectionTile;
        [FindsBy(How = How.XPath, Using = "//div[@id='site-info-tile']//span[contains(@class,'reg-stat-container-header')]")]
        public IWebElement SiteInspectionTileHeader;
        [FindsBy(How = How.XPath, Using = "//div[@id='site-info-tile']//span[contains(@class,'reg-stat-container-stat')]")]
        public IWebElement SiteInspectionTileStat;
        [FindsBy(How = How.XPath, Using = "//div[@id='site']//div[@class='card']//div//h2")]
        public IWebElement SiteInspections;
        [FindsBy(How = How.XPath, Using = "//div[@id='site']//div[@class='card']//div//h2//parent::div//following-sibling::div[@class='body']//span")]
        public IWebElement SiteInspectionsTableMessage;

        //Roles Tile Elements 
        [FindsBy(How = How.Id, Using = "roles-info-tile")]
        public IWebElement RolesTile;
        [FindsBy(How = How.XPath, Using = "//div[@id='roles-info-tile']//span[contains(@class,'reg-stat-container-header')]")]
        public IWebElement RolesTileHeader;
        [FindsBy(How = How.XPath, Using = "//div[@id='roles-info-tile']//span[contains(@class,'reg-stat-container-stat')]")]
        public IWebElement RolesTileStat;
        [FindsBy(How = How.XPath, Using = "//div[@id='roles']//div[@class='card']//div//h2")]
        public IWebElement Roles;
        [FindsBy(How = How.Id, Using = "externalRoles")]
        public IWebElement ExternalRoles;
        [FindsBy(How = How.Id, Using = "internalRoles")]
        public IWebElement InternalRoles;
        [FindsBy(How = How.XPath, Using = "//div[@id='external-main-roles']//ul[contains(@class,'order-roles')]//li[1]")]
        public IList<IWebElement> ExternalRolesList;
        [FindsBy(How = How.XPath, Using = "//div[@id='internal-main-roles']//ul[contains(@class,'order-roles')]//li[1]")]
        public IList<IWebElement> InternalRolesList;

        //Fees Tile Elements 
        [FindsBy(How = How.Id, Using = "outstandingfees-info-tile")]
        public IWebElement FeesTile;
        [FindsBy(How = How.XPath, Using = "//div[@id='outstandingfees-info-tile']//span[contains(@class,'reg-stat-container-header')]")]
        public IWebElement FeesTileHeader;
        [FindsBy(How = How.XPath, Using = "//div[@id='outstandingfees-info-tile']//span[contains(@class,'reg-stat-container-stat')]")]
        public IWebElement FeesTileStat;
        //[FindsBy(How = How.XPath, Using = "//table[@id='orderSummaryTable']//tbody//tr//td[@class=' expandProduct']")]


        [FindsBy(How = How.XPath, Using = "//div[@id='outstandingfees']//div[@class='card']//div//h2")]
        public IWebElement FeesTable;
        [FindsBy(How = How.XPath, Using = "//div[@id='outstandingfees']//div[@class='body']//div[contains(@class,'total-fees')]//div//h1[text()='Total Fees']")]
        public IWebElement TotalFees;
        [FindsBy(How = How.XPath, Using = "//div[@id='outstandingfees']//div[@class='body']//div[contains(@class,'total-fees')]//div//h1[text()='Total Fees']//parent::div//parent::div[@class='row']//ul[@class='total-fees']//li[contains(text(),'Fees Due')]//span")]
        public IWebElement TotalFeesDue;
        [FindsBy(How = How.XPath, Using = "//div[@id='outstandingfees']//div[@class='body']//div[contains(@class,'total-fees')]//div//h1[text()='Total Fees']//parent::div//parent::div[@class='row']//ul[@class='total-fees']//li[contains(text(),'Fees Received To Date')]//span")]
        public IWebElement TotalFeesReceivedToDate;
        [FindsBy(How = How.XPath, Using = "//div[@id='outstandingfees']//div[@class='body']//div[contains(@class,'total-fees')]//div//h1[text()='Total Fees']//parent::div//parent::div[@class='row']//ul[@class='total-fees']//li[contains(text(),'Fees Outstanding')]//span")]
        public IWebElement TotalFeesOutstanding;

        [FindsBy(How = How.XPath, Using = "//div[@id='outstandingfees']//div[@class='body']//div[contains(@class,'total-fees')]//div//h1[text()='Escrow']")]
        public IWebElement EscrowFees;
        [FindsBy(How = How.XPath, Using = "//div[@id='outstandingfees']//div[@class='body']//div[contains(@class,'total-fees')]//div//h1[text()='Escrow']//parent::div//parent::div[@class='row']//ul[@class='total-fees']//li[contains(text(),'Fees Due')]//span")]
        public IWebElement EscrowFeesDue;
        [FindsBy(How = How.XPath, Using = "//div[@id='outstandingfees']//div[@class='body']//div[contains(@class,'total-fees')]//div//h1[text()='Escrow']//parent::div//parent::div[@class='row']//ul[@class='total-fees']//li[contains(text(),'Fees Received To Date')]//span")]
        public IWebElement EscrowFeesReceivedToDate;
        [FindsBy(How = How.XPath, Using = "//div[@id='outstandingfees']//div[@class='body']//div[contains(@class,'total-fees')]//div//h1[text()='Escrow']//parent::div//parent::div[@class='row']//ul[@class='total-fees']//li[contains(text(),'Fees Outstanding')]//span")]
        public IWebElement EscrowFeesOutstanding;



        public static int ProductsCount { get; set; }
        public static int PlotsCount { get; set; }
        public static int ToDoListCount { get; set; }
        public static int TechSummaryCount { get; set; }
        public static int SiteInspectionsCount { get; set; }
        public static int RolesCount { get; set; }
        public static int NumberOfActions { get; set; }
        public static int NumberOfUrgentActions { get; set; }
        public static int LastEntryValue { get; set; }
        public static int FirstEntryValue { get; set; }
        public static double FeesValue { get; set; }
        List<string> OrderTableColumns = new List<string>();
        List<string> ProductsTableColumns = new List<string>();
        List<string> PlotsTableColumns = new List<string>();
        List<string> TodoTableColumns = new List<string>();
        List<string> TechSummaryTableHeaders = new List<string>();
        List<string> TechDocsTableHeaders = new List<string>();
        List<string> TechSummaryTableColumns = new List<string>();
        List<string> SiteInspectionTableColumns = new List<string>();
        List<string> RolesTableColumns = new List<string>();
        List<string> FeesTableColumns = new List<string>();
        List<string> ExternalRoleNames = new List<string>();
        List<string> InternalRoleNames = new List<string>();



        public void VerifyOrderCountOnOrdersPage()
        {
            int actualOrdersCount = 0;
            int ordersCount = Convert.ToInt32(ExtranetHomePage.OrdersCount.Text.ToString());
            var reference = Statics.OrderNumber;
            WaitForElements(ExtranetHomePage.OrdersTablePaginateButtons);
            int numberOfPages = Convert.ToInt32(ExtranetHomePage.OrdersTablePaginateLastButton.Text.ToString());
            if (numberOfPages > 3)
            {
                actualOrdersCount = 3;
                for (int i = 0; i < numberOfPages - 1; i++)
                {
                    WaitForElement(ExtranetHomePage.OrdersTableNextPaginate);
                    ExtranetHomePage.OrdersTableNextPaginate.Click();
                    WaitForElements(ExtranetHomePage.OrdersTableCount);
                    //Thread.Sleep(500);
                    CloseSpinnerOnExtranet();
                    //Thread.Sleep(500);
                    actualOrdersCount += ExtranetHomePage.OrdersTableCount.Count;
                    //Thread.Sleep(500);
                    CloseSpinnerOnExtranet();
                    Thread.Sleep(500);
                }
                Assert.AreEqual(actualOrdersCount, ordersCount);
            }
        }


        //Veiryf Order Record from Home Page by clicking on reference column
        public void VerifyOrderRecordsFromReferenceCoulmn()
        {
            WaitForElement(ExtranetHomePage.HomeLink);
            ExtranetHomePage.HomeLink.Click();
            Thread.Sleep(1000);
            CloseSpinnerOnExtranet();
            WaitForElement(ExtranetHomePage.OrdersTableInfo);
           // WaitForElements(ExtranetHomePage.OrdersRecordsWithRefNumber);
            if (ExtranetHomePage.OrdersRecordsWithRefNumber.Count > 0)
            {
                ExtranetHomePage.OrdersRecordsWithRefNumber[0].Click();
                WaitForElement(OrdersActiveLink);
                Assert.IsTrue(OrdersActiveLink.Displayed, "Failed To Navigate To Order Page");
                WaitForElement(OrderDiv);
                Assert.IsTrue(OrderDiv.Displayed, "Failed To Navigate To Order Content");
                Assert.IsTrue(OrderRefContainer.Displayed, "Failed To Display Order Ref Container");
                Assert.IsTrue(ProductsTile.Displayed, "Failed To Display Products Information");
                var productTile = ProductsTile.GetAttribute("class");
                Assert.IsTrue(productTile.Contains("active"), "Failed To Display active tile");
                Assert.IsTrue(PlotsTile.Displayed, "Failed To Display Plots Information");
                Assert.IsTrue(ActionTile.Displayed, "Failed To Display Actions Information");
                Assert.IsTrue(TechSummaryTile.Displayed, "Failed To Display Technical Summary Information");
                Assert.IsTrue(SiteInspectionTile.Displayed, "Failed To Display Site Information");
                Assert.IsTrue(RolesTile.Displayed, "Failed To Display Role Information");
                Assert.IsTrue(FeesTile.Displayed, "Failed To Display Fees Information");
            }
            else
            {
                WaitForElement(ExtranetHomePage.OrdersMessage);
                var orderMessage = ExtranetHomePage.OrdersMessage.Text;
                Assert.IsTrue(orderMessage.Contains("You don't currently have any active orders."));
            }
        }

        //Veiryf Order Record from Home Page by clicking on actions column
        public void VerifyOrderRecordsFromActionsCoulmn()
        {
            WaitForElement(ExtranetHomePage.HomeLink);
            ExtranetHomePage.HomeLink.Click();
              CloseSpinnerOnExtranet();
            CloseSpinneronPage();
            CloseSpinnerOnExtranet();
            if (ExtranetHomePage.OrdersRecordWithActions.Count > 0)
            {
                ExtranetHomePage.SelectOrderUsingActionsFromHomePage();
                WaitForElement(OrdersActiveLink);
                Assert.IsTrue(OrdersActiveLink.Displayed, "Failed To Navigate To Order Page");
                WaitForElement(OrderDiv);
                Assert.IsTrue(OrderDiv.Displayed, "Failed To Navigate To Order Content");
                Assert.IsTrue(OrderRefContainer.Displayed, "Failed To Display Order Ref Container");
                Assert.IsTrue(ProductsTile.Displayed, "Failed To Display Products Information");
                Assert.IsTrue(PlotsTile.Displayed, "Failed To Display Plots Information");
                Assert.IsTrue(ActionTile.Displayed, "Failed To Display Actions Information");
                var actionTile = ActionTile.GetAttribute("class");
                Assert.IsTrue(actionTile.Contains("active"), "Failed To Display active tile");
                Assert.IsTrue(TechSummaryTile.Displayed, "Failed To Display Technical Summary Information");
                Assert.IsTrue(SiteInspectionTile.Displayed, "Failed To Display Site Information");
                Assert.IsTrue(RolesTile.Displayed, "Failed To Display Role Information");
                Assert.IsTrue(FeesTile.Displayed, "Failed To Display Fees Information");
            }
        }
        //Veiryf Order Record from Home Page by clicking on actions column
        public void VerifyOrderRecordsFromUrgentActionsCoulmn()
        {
            WaitForElement(ExtranetHomePage.HomeLink);
            ExtranetHomePage.HomeLink.Click();
              CloseSpinnerOnExtranet();
            CloseSpinneronPage();
            CloseSpinnerOnExtranet();
            if (ExtranetHomePage.OrdersRecordWithUrgentActions.Count > 0)
            {
                ExtranetHomePage.SelectOrderUsingUrgentActionsFromHomePage();
                WaitForElement(OrdersActiveLink);
                Assert.IsTrue(OrdersActiveLink.Displayed, "Failed To Navigate To Order Page");
                WaitForElement(OrderDiv);
                Assert.IsTrue(OrderDiv.Displayed, "Failed To Navigate To Order Content");
                Assert.IsTrue(OrderRefContainer.Displayed, "Failed To Display Order Ref Container");
                Assert.IsTrue(ProductsTile.Displayed, "Failed To Display Products Information");
                Assert.IsTrue(PlotsTile.Displayed, "Failed To Display Plots Information");
                Assert.IsTrue(ActionTile.Displayed, "Failed To Display Actions Information");
                var actionTile = ActionTile.GetAttribute("class");
                Assert.IsTrue(actionTile.Contains("active"), "Failed To Display active tile");
                Assert.IsTrue(TechSummaryTile.Displayed, "Failed To Display Technical Summary Information");
                Assert.IsTrue(SiteInspectionTile.Displayed, "Failed To Display Site Information");
                Assert.IsTrue(RolesTile.Displayed, "Failed To Display Role Information");
                Assert.IsTrue(FeesTile.Displayed, "Failed To Display Fees Information");
            }          
        }
        public void VerifyOrdersPage()
        {
            WaitForElement(ExtranetHomePage.OrdersLink);
            ExtranetHomePage.OrdersLink.Click();
            WaitForElement(YourOrders);
            WaitForElement(OrderTotalCount);
            Thread.Sleep(1000);
            CloseSpinnerOnExtranet();
            int ordersCount = Convert.ToInt32(OrderTotalCount.Text.ToString());
            if (ordersCount > 0)
            {
                Assert.IsTrue(OrderTable.Displayed, "Order Table Displayed");
                OrderTableColumns = OrderTableHeader.Select(i => i.Text).ToList();
                Assert.IsTrue(OrderTableColumns.Any(o => o.Contains("Reference")));
                Assert.IsTrue(OrderTableColumns.Any(o => o.Contains("Site Address")));
                Assert.IsTrue(OrderTableColumns.Any(o => o.Contains("Plots")));
                Assert.IsTrue(OrderTableColumns.Any(o => o.Contains("Plots Near Completion")));
                Assert.IsTrue(OrderTableColumns.Any(o => o.Contains("Actions")));
                Assert.IsTrue(OrderTableColumns.Any(o => o.Contains("Urgent Actions")));
                CloseSpinnerOnExtranet();
                Assert.IsTrue(OrderTableRows.Count > 0, "No Orders Are Displayed");
                if (OrderTableRows.Count > 0)
                {
                    CloseSpinnerOnExtranet();
                    Thread.Sleep(500);
                    WaitForElement(OrderTableRows[0]);
                    OrderTableRows[0].Click();
                    CloseSpinnerOnExtranet();
                    Thread.Sleep(500);
                    VerifyProductTile();
                    VerifyPlotTile();
                    VerifyActionsTile();
                    VerifyTechnicalSummaryTile();
                    VerifySiteInspectionTile();
                    VerifyRolesTile();
                    VerifyFeesTile();
                }
            }
        }

        public void VerifyProductTile()
        {
            CloseSpinnerOnExtranet();
            Thread.Sleep(500);
            WaitForElementOnExtranet(PlotsTile);
            ProductsTile.Click();
            CloseSpinnerOnExtranet();
            Thread.Sleep(500);
            var productTile = ProductsTile.GetAttribute("class");
            Assert.IsTrue(productTile.Contains("active"), "Failed To Display active tile");
            CloseSpinnerOnExtranet();
            Thread.Sleep(500); ;
            var productTileHeader = ProductsTileHeader.Text;
            Assert.IsTrue(productTileHeader.Contains("PRODUCTS"));
            CloseSpinnerOnExtranet();
            Thread.Sleep(500);
            WaitForElements(ProductSummaryTableHeader);
            ProductsTableColumns = ProductSummaryTableHeader.Select(i => i.Text).ToList();
            Assert.IsTrue(ProductsTableColumns.Any(o => o.Contains("Product Name")));
            Assert.IsTrue(ProductsTableColumns.Any(o => o.Contains("Number of Plots")));
            Assert.IsTrue(ProductsTableColumns.Any(o => o.Contains("Total Reconstruction Cost")));
            Assert.IsTrue(ProductsTableColumns.Any(o => o.Contains("Total Estimated Sale Price")));
            Assert.IsTrue(ProductsTableColumns.Any(o => o.Contains("Total Fees")));
            string products = ProductsTileStat.Text;
            ProductsCount = Convert.ToInt32(products);
            Assert.IsTrue(ProductsCount > 0, "Products Count is failed to display");
            if (ProductsCount > 0)
            {
                Assert.IsTrue(ProductsSummary.Displayed);
                Assert.IsTrue(ProductSummaryTable.Displayed);
                if (ProductSummaryTableRows.Count > 0)
                {
                    WaitForElement(ExpandProductSummaryTableRows);
                    ExpandProductSummaryTableRows.Click();
                    Assert.IsTrue(ProductCovers.Displayed, "Failed to display product covers");
                }
            }
            else
            {
               
            }
        }

        public void VerifyPlotTile()
        {
            CloseSpinnerOnExtranet();
            Thread.Sleep(500);
            WaitForElementOnExtranet(PlotsTile);
            WaitForElementToClick(PlotsTile);
            PlotsTile.Click();
            CloseSpinnerOnExtranet();
            Thread.Sleep(500);
            var plotTile = PlotsTile.GetAttribute("class");
            Assert.IsTrue(plotTile.Contains("active"), "Failed To Display active tile");
            Assert.IsTrue(PlotsTileHeader.Text.Contains("PLOTS"));
            string plots = PlotsTileStat.Text;
            PlotsCount = Convert.ToInt32(plots);
            Assert.IsTrue(PlotsCount > 0, "Products Count is failed to display");
            if (PlotsCount > 0)
            {
                Assert.IsTrue(PlotsTableBody.Displayed);
                Assert.IsTrue(PlotsTable.Displayed);
                Assert.IsTrue(PlotsTableHeader.Count > 0);
                PlotsTableColumns = PlotsTableHeader.Select(i => i.Text).ToList();
                Assert.IsTrue(PlotsTableColumns.Any(o => o.Contains("Reference")));
                Assert.IsTrue(PlotsTableColumns.Any(o => o.Contains("Name")));
                Assert.IsTrue(PlotsTableColumns.Any(o => o.Contains("Plot Address")));
                Assert.IsTrue(PlotsTableColumns.Any(o => o.Contains("Warranty Product")));
                Assert.IsTrue(PlotsTableColumns.Any(o => o.Contains("Stage of Works")));
                Assert.IsTrue(PlotsTableColumns.Any(o => o.Contains("Actions")));
                Assert.IsTrue(PlotsTableColumns.Any(o => o.Contains("Certificate")));
                WaitForElements(PlotsTableRows);
                Assert.IsTrue(PlotsTableRows.Count > 0);
                CloseSpinneronPage();
                  CloseSpinnerOnExtranet();
                string actionsInfo = PlotTableInfo.Text;
                if (actionsInfo != null)
                {
                    GetLastEntryFromString(actionsInfo);
                    if (LastEntryValue >= 0)
                    {

                        Assert.AreEqual(LastEntryValue, PlotsCount);
                    }
                }
            }
        }


        public void VerifyActionsTile()
        {
            CloseSpinnerOnExtranet();
            Thread.Sleep(500);
            WaitForElementOnExtranet(ActionTile);
            Scrollup();
            ActionTile.Click();
            CloseSpinnerOnExtranet();
            Thread.Sleep(500);
            var active = ActionTile.GetAttribute("class");
            Assert.IsTrue(active.Contains("active"), "Failed To Display active tile");
            Assert.IsTrue(ActionsTileHeader.Text.Contains("ACTION REQUIRED"));
            string todoList = ActionsTileStat.Text.ToString();
            //ToDoListCount = Convert.ToInt32(todoList);
            WaitForElement(ActionRequired);
            Assert.IsTrue(ActionRequired.Text.Contains("ACTION REQUIRED"));
            if (todoList != null)
            {
                GetLastEntryFromString(todoList);
                NumberOfActions = FirstEntryValue;
                NumberOfUrgentActions = LastEntryValue;
                CloseSpinnerOnExtranet();
                Thread.Sleep(500);
                WaitForElementOnExtranet(ToDoTable);
                WaitForElements(ToDoTableHeader);
                TodoTableColumns = ToDoTableHeader.Select(i => i.Text).ToList();
                Assert.IsTrue(TodoTableColumns.Any(o => o.Contains("Details of Actions Outstanding")));
                Assert.IsTrue(TodoTableColumns.Any(o => o.Contains("Applies To")));
                Assert.IsTrue(TodoTableColumns.Any(o => o.Contains("Respond To Action")));
                Assert.IsTrue(ToDoTableRows.Count > 0);
                string actionsInfo = ToDoTableInfo.Text;
                if (actionsInfo != null)
                {
                    GetLastEntryFromString(actionsInfo);
                    if (NumberOfActions != 0)
                    {
                        Assert.AreEqual(LastEntryValue, NumberOfActions);
                    }
                    CloseSpinnerOnExtranet();
                    Thread.Sleep(500);
                    VerifyNearlyCompleteSitesOnActionsTile();
                }
            }
            else
            {
                var todoTableMessage = OutstandingActions.Text;
                Assert.IsTrue(todoTableMessage.Contains("There are no outstanding actions"), "Failed to display outstanding actions message");
            }
        }
        public void VerifyNearlyCompleteSitesOnActionsTile()
        {
            CloseSpinnerOnExtranet();
            Thread.Sleep(500);
            WaitForElementOnExtranet(ActionTile);
            Scrollup();
            ActionTile.Click();
            CloseSpinnerOnExtranet();
            Thread.Sleep(500);
            var active = ActionTile.GetAttribute("class");
            Assert.IsTrue(active.Contains("active"), "Failed To Display active tile");
            Assert.IsTrue(ActionsTileHeader.Text.Contains("ACTION REQUIRED"));
            string todoList = ActionsTileStat.Text.ToString();
            //ToDoListCount = Convert.ToInt32(todoList);
            WaitForElement(ActionRequired);
            Assert.IsTrue(ActionRequired.Text.Contains("ACTION REQUIRED"));
            if (todoList != null)
            {
                GetLastEntryFromString(todoList);
                NumberOfUrgentActions = LastEntryValue;
                WaitForElement(ToDoTable);
                WaitForElements(ToDoTableHeader);
                TodoTableColumns = ToDoTableHeader.Select(i => i.Text).ToList();
                Assert.IsTrue(TodoTableColumns.Any(o => o.Contains("Details of Actions Outstanding")));
                Assert.IsTrue(TodoTableColumns.Any(o => o.Contains("Applies To")));
                Assert.IsTrue(TodoTableColumns.Any(o => o.Contains("Respond To Action")));
                Assert.IsTrue(ToDoTableRows.Count > 0);
                if (NumberOfUrgentActions >= 0)
                {
                    WaitForElement(PlotsNearlyComplete);
                    PlotsNearlyComplete.Click();
                    CloseSpinneronPage();
                      CloseSpinnerOnExtranet();
                    string plotsnearlycomplete = ToDoTableInfo.Text;
                    GetLastEntryFromString(plotsnearlycomplete);
                    if (LastEntryValue >= 0)
                    {
                        CloseSpinneronPage();
                          CloseSpinnerOnExtranet();
                        Assert.AreEqual(LastEntryValue, NumberOfUrgentActions);
                    }
                }
            }
            else
            {
                var todoTableMessage = OutstandingActions.Text;
                Assert.IsTrue(todoTableMessage.Contains("There are no outstanding actions"), "Failed to display outstanding actions message");
            }
        }


        public void VerifyTechnicalSummaryTile()
        {
            CloseSpinnerOnExtranet();
            Thread.Sleep(500);
            WaitForElementOnExtranet(TechSummaryTile);
            Scrollup();
            TechSummaryTile.Click();
            CloseSpinnerOnExtranet();
            Thread.Sleep(500);
            var active = TechSummaryTile.GetAttribute("class");
            Assert.IsTrue(active.Contains("active"), "Failed To Display active tile");
            Assert.IsTrue(TechSummaryTileHeader.Text.Contains("TECHNICAL SUMMARY"));
            string techSummaryList = TechSummaryTileStat.Text.ToString();
            WaitForElement(TechnicalSummaryTitle);
            Assert.IsTrue(TechnicalSummaryTitle.Text.Contains("TECHNICAL SUMMARY"));
            int technicalCount = Convert.ToInt32(techSummaryList);
            CloseSpinnerOnExtranet();
            Thread.Sleep(500);
            WaitForElements(TechSummaryTableHeader);
            TechSummaryTableHeaders = TechSummaryTableHeader.Select(i => i.Text).ToList();
            CloseSpinnerOnExtranet();
            Thread.Sleep(500);
            Assert.IsTrue(TechSummaryTableHeaders.Any(o => o.Contains("TECHNICAL MANUAL")));
            Assert.IsTrue(TechSummaryTableHeaders.Any(o => o.Contains("GET A CERTIFICATE")));
            Assert.IsTrue(DesignDocs.Displayed);
            Assert.IsTrue(DesignItems.Displayed);
            Assert.IsTrue(RiskItems.Displayed);
            Assert.IsTrue(CompletionDocs.Displayed);
            Assert.IsTrue(SiteDefects.Displayed);
            int designDocsCount = Convert.ToInt32(DesignDocs.Text.ToString());
            int designItemsCount = Convert.ToInt32(DesignItems.Text.ToString());
            int riskItemsCount = Convert.ToInt32(RiskItems.Text.ToString());
            int completionDocsCount = Convert.ToInt32(CompletionDocs.Text.ToString());
            int siteDefectsCount = Convert.ToInt32(SiteDefects.Text.ToString());
            CloseSpinnerOnExtranet();         
            if (designDocsCount > 0)
            {
                WaitForElement(DesignDocsInfoTile);
                Scrollup();
                DesignDocsInfoTile.Click();
                CloseSpinnerOnExtranet();
                WaitForElements(DesignDocsTableHeader);
                WaitForElements(DesignDocsTableRows);
                TechSummaryTableHeaders = DesignDocsTableHeader.Select(i => i.Text).ToList();
                Assert.IsTrue(TechSummaryTableHeaders.Any(o => o.Contains("Area")));
                Assert.IsTrue(TechSummaryTableHeaders.Any(o => o.Contains("Description of Outstanding Design Document")));
                Assert.IsTrue(TechSummaryTableHeaders.Any(o => o.Contains("Applies To")));
                Assert.IsTrue(TechSummaryTableHeaders.Any(o => o.Contains("Date Raised")));
                Assert.IsTrue(TechSummaryTableHeaders.Any(o => o.Contains("Respond")));
            }
            else
            {
                WaitForElement(DesignDocsInfoTile);
                Scrollup();
                DesignDocsInfoTile.Click();
                WaitForElement(NoDesignDocs);
                Assert.IsTrue(NoDesignDocs.Displayed);
            }
            if (designItemsCount > 0)
            {
                WaitForElement(DesignItemsInfoTile);
                Scrollup();
                DesignItemsInfoTile.Click();
                WaitForElements(DesignItemsTableHeader);
                WaitForElements(DesignItemsTableRows);
                TechSummaryTableHeaders = DesignItemsTableHeader.Select(i => i.Text).ToList();
                Assert.IsTrue(TechSummaryTableHeaders.Any(o => o.Contains("Area")));
                Assert.IsTrue(TechSummaryTableHeaders.Any(o => o.Contains("Description of Outstanding Design Items")));
                Assert.IsTrue(TechSummaryTableHeaders.Any(o => o.Contains("Applies To")));
                Assert.IsTrue(TechSummaryTableHeaders.Any(o => o.Contains("Date Raised")));
                Assert.IsTrue(TechSummaryTableHeaders.Any(o => o.Contains("Respond")));
            }
            else
            {
                WaitForElement(DesignItemsInfoTile);
                Scrollup();
                DesignItemsInfoTile.Click();
                WaitForElement(NoDesignItems);
                Assert.IsTrue(NoDesignItems.Displayed);
            }
            if (riskItemsCount > 0)
            {
                WaitForElement(RiskItemsInfoTile);
                Scrollup();
                RiskItemsInfoTile.Click();
                WaitForElements(RiskItemsTableHeader);
                WaitForElements(RiskItemsTableRows);
                TechSummaryTableHeaders = RiskItemsTableHeader.Select(i => i.Text).ToList();
                Assert.IsTrue(TechSummaryTableHeaders.Any(o => o.Contains("Area")));
                Assert.IsTrue(TechSummaryTableHeaders.Any(o => o.Contains("Description of Outstanding Risk Items")));
                Assert.IsTrue(TechSummaryTableHeaders.Any(o => o.Contains("Applies To")));
                Assert.IsTrue(TechSummaryTableHeaders.Any(o => o.Contains("Date Raised")));
                Assert.IsTrue(TechSummaryTableHeaders.Any(o => o.Contains("Respond")));
            }
            else
            {
                WaitForElement(RiskItemsInfoTile);
                Scrollup();
                RiskItemsInfoTile.Click();
                WaitForElement(NoRiskItems);
                Assert.IsTrue(NoRiskItems.Displayed);
            }
            if (completionDocsCount > 0)
            {
                WaitForElement(CompletionDocsInfoTile);
                Scrollup();
                CompletionDocsInfoTile.Click();
                WaitForElements(CompletionDocsTableHeader);
                WaitForElements(CompletionDocsTableRows);
                TechSummaryTableHeaders = CompletionDocsTableHeader.Select(i => i.Text).ToList();
                Assert.IsTrue(TechSummaryTableHeaders.Any(o => o.Contains("Area")));
                Assert.IsTrue(TechSummaryTableHeaders.Any(o => o.Contains("Description of Outstanding Completion Document")));
                Assert.IsTrue(TechSummaryTableHeaders.Any(o => o.Contains("Applies To")));
                Assert.IsTrue(TechSummaryTableHeaders.Any(o => o.Contains("Date Raised")));
                Assert.IsTrue(TechSummaryTableHeaders.Any(o => o.Contains("Respond")));
            }
            else
            {
                WaitForElement(CompletionDocsInfoTile);
                Scrollup();
                CompletionDocsInfoTile.Click();
                WaitForElement(NoCompletionDocs);
                Assert.IsTrue(NoCompletionDocs.Displayed);
            }
            if (siteDefectsCount > 0)
            {
                WaitForElements(SiteDefectsTableHeader);
                WaitForElements(SiteDefectsTableRows);
                TechSummaryTableHeaders = SiteDefectsTableHeader.Select(i => i.Text).ToList();
                Assert.IsTrue(TechSummaryTableHeaders.Any(o => o.Contains("Area")));
                Assert.IsTrue(TechSummaryTableHeaders.Any(o => o.Contains("Description of Outstanding Site Defects")));
                Assert.IsTrue(TechSummaryTableHeaders.Any(o => o.Contains("Applies To")));
                Assert.IsTrue(TechSummaryTableHeaders.Any(o => o.Contains("Date Raised")));
                Assert.IsTrue(TechSummaryTableHeaders.Any(o => o.Contains("Respond")));
            }
            else
            {
                WaitForElement(SiteDefectsInfoTile);
                Scrollup();
                SiteDefectsInfoTile.Click();
                WaitForElement(NoSiteDefects);
                Assert.IsTrue(NoSiteDefects.Displayed);
            }
        }
        public void VerifySiteInspectionTile()
        {
            CloseSpinnerOnExtranet();
            WaitForElementOnExtranet(SiteInspectionTile);          
            Thread.Sleep(500);
            Scrollup();
            SiteInspectionTile.Click();
            CloseSpinnerOnExtranet();
            Thread.Sleep(500);
            var active = SiteInspectionTile.GetAttribute("class");
            Assert.IsTrue(active.Contains("active"), "Failed To Display active tile");
            Assert.IsTrue(SiteInspectionTileHeader.Text.Contains("SITE INSPECTIONS"));
            string siteIndpectionsCount = SiteInspectionTileStat.Text.ToString();
            SiteInspectionsCount = Convert.ToInt32(siteIndpectionsCount);
            WaitForElement(SiteInspections);
            Assert.IsTrue(SiteInspections.Text.Contains("SITE INSPECTIONS"));
            if (SiteInspectionsCount > 0)
            {

            }
            else
            {
                var siteInspectionTableMessage = SiteInspectionsTableMessage.Text;
                Assert.IsTrue(siteInspectionTableMessage.Contains("There have been no inspections completed."), "Failed to display no inspections completed message");
            }
        }
        public void VerifyRolesTile()
        {
            CloseSpinnerOnExtranet();
            Thread.Sleep(500);
            WaitForElementOnExtranet(RolesTile);
            Scrollup();
            RolesTile.Click();
            CloseSpinnerOnExtranet();
            Thread.Sleep(500);
            var active = RolesTile.GetAttribute("class");
            Assert.IsTrue(active.Contains("active"), "Failed To Display active tile");
            Assert.IsTrue(RolesTileHeader.Text.Contains("ROLES"));
            string rolesCount = RolesTileStat.Text.ToString();
            RolesCount = Convert.ToInt32(rolesCount);
            WaitForElement(Roles);
            Assert.IsTrue(Roles.Text.Contains("ROLES"));
            if (RolesCount > 0)
            {
                WaitForElements(ExternalRolesList);
                ExternalRoleNames = ExternalRolesList.Select(i => i.Text).ToList();
                Assert.IsTrue(ExternalRoleNames.Any(o => o.Contains("Quote Recipient")));
                Assert.IsTrue(ExternalRoleNames.Any(o => o.Contains("Invoice Recipient")));
                Assert.IsTrue(ExternalRoleNames.Any(o => o.Contains("Warranty Document Recipient")));
                Assert.IsTrue(ExternalRoleNames.Any(o => o.Contains("Site Report Recipient")));
                WaitForElements(InternalRolesList);
                InternalRoleNames = InternalRolesList.Select(i => i.Text).ToList();
                Assert.IsTrue(InternalRoleNames.Any(o => o.Contains("Sales Account Manager")));
                Assert.IsTrue(InternalRoleNames.Any(o => o.Contains("Surveyor")));
                Assert.IsTrue(InternalRoleNames.Any(o => o.Contains("Customer Service Account Handler")));
                Assert.IsTrue(InternalRoleNames.Any(o => o.Contains("Technical Administrator")));
            }
            
        }
        public void VerifyFeesTile()
        {
            CloseSpinnerOnExtranet();
            Thread.Sleep(500);
            WaitForElementOnExtranet(FeesTile);
            Scrollup();
            FeesTile.Click();
            Thread.Sleep(500);
            CloseSpinnerOnExtranet();
            Thread.Sleep(500);
            var active = FeesTile.GetAttribute("class");
            Assert.IsTrue(active.Contains("active"), "Failed To Display active tile");
            Assert.IsTrue(FeesTileHeader.Text.Contains("FEES"));
            string feeValue = FeesTileStat.Text.ToString();
            string fee = feeValue.Replace("£", "");
            FeesValue = Convert.ToDouble(fee.ToString());
            WaitForElement(FeesTable);
            Assert.IsTrue(FeesTable.Text.Contains("FEES"));
            if (FeesValue > 0)
            {
                WaitForElement(TotalFees);
                Assert.IsTrue(TotalFeesDue.Displayed);
                Assert.IsTrue(TotalFeesReceivedToDate.Displayed);
                Assert.IsTrue(TotalFeesOutstanding.Displayed);
                WaitForElement(EscrowFees);
                Assert.IsTrue(EscrowFeesDue.Displayed);
                Assert.IsTrue(EscrowFeesReceivedToDate.Displayed);
                Assert.IsTrue(EscrowFeesOutstanding.Displayed);
            }
          
        }
        public int GetLastEntryFromString(string input)
        {
            input = Regex.Replace(input, @"\s+", string.Empty);
            string[] numbers = Regex.Split(input, @"\D+");
            numbers = numbers.Where(x => !string.IsNullOrEmpty(x)).ToArray();
            if (numbers != null)
            {
                LastEntryValue = Convert.ToInt32(numbers.LastOrDefault().ToString());
                FirstEntryValue = Convert.ToInt32(numbers.FirstOrDefault().ToString());
            }
            return LastEntryValue & FirstEntryValue;
        }
    }
}
