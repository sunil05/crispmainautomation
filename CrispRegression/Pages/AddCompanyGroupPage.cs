﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.PageObjects;
using System.Net.Configuration;
using System.Security;
using System.Threading;
using OpenQA.Selenium.Support.UI;
using CrispAutomation.Pages;
using CrispAutomation.Support;
using NUnit.Framework;
using static CrispAutomation.Support.ExtensionMethods;

namespace CrispAutomation.Pages
{

   

    public class AddCompanyGroupPage : Support.Pages
    {
        public IWebDriver wdriver;

        public AddCompanyGroupPage(ISearchContext driver)
        {
            PageFactory.InitElements(driver, this);
            wdriver = (RemoteWebDriver)driver;
        }

        [FindsBy(How = How.XPath, Using = "//a[@class='au-target btn-floating btn-large waves-effect waves-light crisp-action-button-inner']//i[text()='add']")]
        public IWebElement CreateCompanyGroupButton;

        [FindsBy(How = How.XPath, Using = "//crisp-input-text[@label='Group Name']/div/label[text()='Group Name']")]
        public IWebElement GroupNameLabel;

        [FindsBy(How = How.XPath, Using = "//crisp-input-text[@label='Group Name']/div/input[@ref='input']")]
        public IWebElement GroupNameInput;       
        
        [FindsBy(How = How.XPath, Using = "//ul[@class='au-target collection has-header']/li[@class='au-target collection-item']//crisp-list-item//a//crisp-list-item-action//crisp-button//button[text()='Select'][@class='au-target waves-effect waves-light btn-flat']")]
        public IList<IWebElement> CompaniesList;     

        [FindsBy(How = How.XPath, Using = "//crisp-action-button[@class='au-target']//div//a[@class='au-target btn-floating btn-large waves-effect waves-light crisp-action-button-inner']//i[text()='save']")]
        public IWebElement SaveButton;

        [FindsBy(How = How.XPath, Using = "//crisp-card-actions[@class='au-target']//crisp-button[@click.delegate='cancel()']//button[text()='Back to Search'][@class='au-target waves-effect waves-light btn-flat']")]
        public IWebElement BackToSearchButton;

        [FindsBy(How = How.XPath,
            Using = "//crisp-card-content[@class='au-target']//crisp-input-text[@icon='search'][@label='Enter the name of a group to search for']//label[@class='au-target']")]
        public IWebElement SearchForCompanyGroup;

        [FindsBy(How = How.XPath, Using = "//crisp-card-content[@class='au-target']//crisp-input-text[@icon='search'][@label='Enter the name of a group to search for']//input")]
        public IWebElement EnterCompanyGroupForSerach;
        
        [FindsBy(How = How.XPath, Using = "//crisp-list//ul//li[@class='au-target collection-item']//crisp-list-item//a//div[@class='title']")]
        public IList<IWebElement> CompanyGroupResult;

        [FindsBy(How = How.XPath, Using = "//crisp-list//ul//li[contains(@class,'au-target collection-item')]/crisp-list-item//a//div[@class='secondary-content']//i")]
        public IList<IWebElement> EditCompanyGroupResultButton;   

        [FindsBy(How = How.XPath, Using = "//crisp-button[@click.delegate='discontinue()'][@class='au-target']//span//button[@class='au-target waves-effect waves-light btn-flat']")]
        public IWebElement DiscontinueGroup;

        [FindsBy(How = How.XPath, Using = "//crisp-footer[@class='au-target']//crisp-button[@class='au-target']//button[@class='au-target waves-effect waves-light btn'][text()='OK']")]
        public IWebElement OkButton;

        
        public void CompanyGroupDetails()
        {           
            WaitForElement(GroupNameLabel);
            GroupNameLabel.Click();
            WaitForElement(GroupNameInput);
            GroupNameInput.Click();
            GroupNameInput.Clear();
            GroupNameInput.SendKeys(RunId.Id);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            WaitForElements(CompaniesList);
            if(CompaniesList.Count>0)
            {
                for(int i=0; i < 2; i++)
                {
                    CloseSpinneronDiv();
                    CloseSpinneronPage();
                    WaitForElement(CompaniesList[i]);
                    CompaniesList[i].Click();
                    CloseSpinneronDiv();
                    CloseSpinneronPage();
                }
            }
            CloseSpinneronPage();           
            //Thread.Sleep(500);
            WaitForElement(SaveButton);
            SaveButton.Click();
            CloseSpinneronDiv();
            CloseSpinneronPage();
        }

        public void SearchCreatedCompanyGroup()
        {
            WaitForElement(BackToSearchButton);
            WaitForElementToClick(BackToSearchButton);
            BackToSearchButton.Click();
            //Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(500);
            WaitForElement(SearchForCompanyGroup);
            WaitForElementToClick(SearchForCompanyGroup);
            SearchForCompanyGroup.Click();
            WaitForElement(EnterCompanyGroupForSerach);
            EnterCompanyGroupForSerach.SendKeys(RunId.Id);
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
        }

        public void VerifyResults()
        {
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            WaitForElements(CompanyGroupResult);
            WaitForElement(CompanyGroupResult[0]);
            Assert.IsTrue(CompanyGroupResult[0].Displayed);
            var CompanyGroupResultsTitle = CompanyGroupResult[0].Text;
            //Assert.IsTrue(CompanyGroupResultsTitle.Contains(RunId.Id));
            CompanyGroupResult[0].Click();
            CloseSpinneronPage();
        }
        public void EditingCompanyGroup()
        {
            CloseSpinneronDiv();
            CloseSpinneronPage();
            WaitForElements(EditCompanyGroupResultButton);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            WaitForElement(EditCompanyGroupResultButton[0]);
            WaitForElementToClick(EditCompanyGroupResultButton[0]);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            EditCompanyGroupResultButton[0].Click();
            //Thread.Sleep(1000);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(1000);
            if (CompaniesList.Count > 0)
            {
                for (int i = 2; i < 4; i++)
                {
                    WaitForElement(CompaniesList[i]);
                    CompaniesList[i].Click();                 

                }
            }
            CloseSpinneronPage();           
            WaitForElement(SaveButton);
            SaveButton.Click();
            //Thread.Sleep(500);
            CloseSpinneronPage();
            CloseSpinneronDiv();
            //Thread.Sleep(500);
        }

        public void DiscontinueTheGroup()
        {
            WaitForElement(DiscontinueGroup);
            DiscontinueGroup.Click();
            WaitForElement(OkButton);
            OkButton.Click();
            //Thread.Sleep(1000);

        }
        public void VerifyDiscontinuedGroup()
        {
            WaitForElement(SearchForCompanyGroup);
            SearchForCompanyGroup.Click();
            WaitForElement(EnterCompanyGroupForSerach);
            //Thread.Sleep(1000);
            EnterCompanyGroupForSerach.SendKeys(RunId.Id);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(1000);
           // Assert.IsTrue(CompanyGroupResult == null);            
        }
    }
}


