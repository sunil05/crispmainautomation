﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms.VisualStyles;
using CrispAutomation.Features;
using CrispAutomation.Support;
using iTextSharp.text;
using Microsoft.Office.Interop.Excel;
using OpenQA.Selenium;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.PageObjects;

namespace CrispAutomation.Pages
{
   public class HTTPClient : Support.Pages
    {
        public IWebDriver wdriver;
        public HTTPClient(ISearchContext driver)
        {
            PageFactory.InitElements(driver, this);
            wdriver = (RemoteWebDriver)driver;
        }
        public void Download(string quoteId, string fileName, string folderName)
        {
            MainAsync(quoteId,fileName,folderName).GetAwaiter().GetResult();
        }
        //public void DownloadSubfolders(string quoteId, string folderName)
        //{
        //    GetTaskAsync(quoteId, folderName).GetAwaiter().GetResult();
        //}
        public async Task MainAsync(string quoteId, string fileName, string folderName)
        {
            var fileInfo = new FileInfo(ExtensionMethods.GetAbsolutePath($@"TestData\Documents\UIDocs\{fileName}"));
            if(fileInfo.Directory == null)
            {
                fileInfo.Directory.Create();
            }            
            Statics.UIPdf = fileInfo.ToString();
            var envrironmentApiServer = ConfigurationManager.AppSettings["ApiServerUrl"];
            var client = HttpClientHelpers.CreateClient("scott.williams@mdinsurance.co.uk", "Mdis@1234;");
            var folders = await client.GetAsync($"{envrironmentApiServer}/api/documentstore/3/{quoteId}/Folders").ToObject<FolderDetails[]>();
            var folder = folders.First(x => x.name == folderName);  
            var files = await client.GetAsync($"{envrironmentApiServer}/api/documentstore/3/{quoteId}/Files?folderId={folder.id}").ToObject<FileDetails[]>();
            var file = files.First(x => x.name == fileName);
           // var file = files.FirstOrDefault(x => x.name.Contains($"{fileName}"));         
            var data = await client.GetByteArrayAsync($"{envrironmentApiServer}/api/documentstore/3/{quoteId}/Files/{file.id}/Download");
            File.WriteAllBytes(Statics.UIPdf, data);
            log.Info($"Following Document is Downloaded from Crisp Application : {fileName}");    
            
        }
        public async Task GetTaskAsync(string quoteId, string folderName)
        {
            var envrironmentApiServer = ConfigurationManager.AppSettings["ApiServerUrl"];
            var client = HttpClientHelpers.CreateClient("scott.williams@mdinsurance.co.uk", "Mdis@1234;");
            var folders = await client.GetAsync($"{envrironmentApiServer}/api/documentstore/3/{quoteId}/Folders").ToObject<FolderDetails[]>();
            var folder = folders.First(x => x.name == "Building Control");            
        }
    }
    
    public class FileDetails
    {
        public string id { get; set; }
        public string name { get; set; }
        public object parentId { get; set; }
        public int fileType { get; set; }
        public object tags { get; set; }
        public int fileSize { get; set; }
        public string fileId { get; set; }
    }


    public class FolderDetails
    {
        public string id { get; set; }
        public string name { get; set; }
        public object parentId { get; set; }
        public int subFolders { get; set; }
        public object files { get; set; }
    }
    public static class HttpClientHelpers
    {
        public static Task<HttpResponseMessage> PostAsync(this HttpClient client, string url, object body)
        {
            return client.PostAsync(url, new StringContent(Newtonsoft.Json.JsonConvert.SerializeObject(body), Encoding.Default, "application/json"));
        }

        public static async Task<T> ToObject<T>(this Task<HttpResponseMessage> response)
        {
            return await (await response).ToObject<T>();
        }

        public static async Task<T> ToObject<T>(this HttpResponseMessage response)
        {
            var json = await response.Content.ReadAsStringAsync();
            return Newtonsoft.Json.JsonConvert.DeserializeObject<T>(json);
        }

        public static HttpClient CreateClient(string username, string password)
        {
            var client = new HttpClient();

            client.DefaultRequestHeaders.Authorization =
    new AuthenticationHeaderValue(
        "Basic",
        Convert.ToBase64String(
            System.Text.ASCIIEncoding.ASCII.GetBytes(
                string.Format("{0}:{1}", username, password))));

            return client;

        }
    }
}
