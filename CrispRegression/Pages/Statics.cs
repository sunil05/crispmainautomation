﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Build.Tasks;
using OfficeOpenXml;

namespace CrispAutomation.Features
{
    public static class Statics
    {
        public static ExcelPackage Excel { get; set; }
        public static string RoleName { get; set; }
        public static string EmpName { get; set; }    
        public static string ContactName { get; set; }
        public static string OrderNumber { get; set; }
        public static string OrderId { get; set; }
        public static string LeadId { get; set; }
        public static string PersonId { get; set; }
        public static int CurrentExcelRow { get; set; }
        public static string UIPdf { get; set; }
        public static string SiteRefNumber { get; set; }
        public static string ProductName { get; set; }
        public static string DownloadFolder { get; set; }
        public static string COIName { get; set; }
        public static int Filetype { get; set; }
        public static List<string> Plots { get; set; }
        public static List<string> ProductNameList { get; set; }
        public static List<string> ConstructionType { get; set; }
        public static List<string> BCProducts { get; set; }
        public static string CompanyName { get; set; }
        public static string QuoteNumber { get; set; }
        public static string quoteId { get; set; }
        public static List<string> ConditionsList { get; set; }
        public static string Username { get; set; }
        public static List<string> ExtranetConditions { get; set; }
        public static double CrispTotalFee { get; set; }
        public static double ExtranetFee { get; set; }
        public static double CrispFeeDue { get; set; }
        public static double CrispFeepaid { get; set; }
        public static double CrispFeeBalance { get; set; }
        public static double CrispEscrowDue { get; set; }
        public static double CrispEscrowPaid { get; set; }
        public static double CrispEscrowBalance { get; set; }
        public static double ExtranetTotalFeeDue { get; set; }
        public static double ExtranetTotalFeeOutstanding { get; set; }
        public static double ExtranetTotalFeeReceivedToDate { get; set; }
        public static double ExtranetEscrowFeeDue { get; set; }
        public static double ExtranetEscrowFeeOutstanding { get; set; }
        public static double ExtranetEscrowFeeReceivedToDate { get; set; }

        public static string PermissionUserId { get; set; }

        public static string CrispUserId { get; set; }

    }
    public static class Products
    {
        public static int Newhomes { get; set; } = 1;
        public static int SocialHousing { get; set; } = 2;
        public static int Commercial { get; set; } = 3;
        public static int SelfBuild { get; set; } = 4;
        public static int CompletedHousing { get; set; } = 5;
        public static int CIResidential { get; set; } = 6;
        public static int CICommercial { get; set; } = 7;
        public static int NewhomesHVS { get; set; } = 8;
        public static int SocialHousingHVS { get; set; } = 9;
        public static int CommercialHVS { get; set; } = 10;
        public static int CIResidentialHVS { get; set; } = 11;
        public static int CICOmmercialHVS { get; set; } = 12;
        public static int PrivateRental { get; set; } = 13;
        public static int ResidentialBC { get; set; } = 17;
        public static int CommercialBC { get; set; } = 18;
        public static int PrivateRentalHVS { get; set; } = 19;
        public static int RoadandSewerBonds { get; set; } = 20;
        public static int RoadBond { get; set; } = 22;
        public static int SewerBond { get; set; } = 23;
    }
}
