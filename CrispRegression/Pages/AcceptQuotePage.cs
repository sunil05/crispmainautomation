﻿using OpenQA.Selenium;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.PageObjects;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using CrispAutomation.Features;
using ExcelDataReader;
using OfficeOpenXml;
using CrispAutomation.Support;
using NUnit.Framework;

namespace CrispAutomation.Pages
{
    public class AcceptQuotePage :Support.Pages
    {
        public IWebDriver wdriver;
        public AcceptQuotePage(ISearchContext driver)
        {
            PageFactory.InitElements(driver, this);
            wdriver = (RemoteWebDriver)driver;
        }
                                                                
        [FindsBy(How = How.XPath, Using = "//ai-dialog-container//crisp-dialog//div[contains(@class,'modal-fixed-footer modal-full-bleed modal-wide modal-tall modal-has-header')]//crisp-header[@class='au-target']")]
        public IWebElement AcceptQuoteDialogueWindow;       

        //[FindsBy(How = How.CssSelector, Using = "crisp-card-content > div > crisp-input-file > div > div.btn")]
        //public IWebElement FileButton;
        
        [FindsBy(How = How.XPath, Using = "//crisp-input-file/div/div[1]/input")]
        public IWebElement QuoteAcceptenceDoc;

        [FindsBy(How = How.XPath, Using = "//crisp-input-date[@label='Date received']/div/label")]
        public IWebElement Datereceived;

        [FindsBy(How = How.XPath, Using = "//div[contains(@class,'picker--focused')]/div/div/div/div/div[3][@class='picker__footer']/button[text()='Today']")]
        public IWebElement ReceivedDateInput;

        [FindsBy(How = How.XPath, Using = "//crisp-footer/nav/div/crisp-footer-button[@click.delegate='tryComplete()'][@class='au-target']/button[text()='Complete'][@class='au-target waves-effect waves-light btn-flat']")]
        public IWebElement CompleteButton;

        [FindsBy(How = How.XPath, Using = "//crisp-stepper-step[contains(@class,'au-target current')]")]
        public IWebElement ProductsTab;

        [FindsBy(How = How.XPath, Using = "//crisp-wizard-step[@class='au-target active']//products-covers[@class='custom-element au-target']//div[@class='reset-recalculate-btn']")]
        public IWebElement ResetRecaluclateFees;

        [FindsBy(How = How.XPath, Using = "//crisp-footer-button[@click.delegate='goForward()'][@class='au-target']//button[text()='Next'][@class='au-target waves-effect waves-light btn-flat']")]
        public IWebElement AcceptQuoteNextButton;

        [FindsBy(How = How.XPath, Using = "//crisp-stepper-step[contains(@class,'au-target current')]")]
        public IWebElement ConditionsTab;
        
        [FindsBy(How = How.XPath, Using = "//crisp-wizard-step[@class='au-target active']//conditions[@class='custom-element au-target']//div[@class='table-title']//span[text()='Conditions']")]
        public IWebElement ConditionsTitle;

        [FindsBy(How = How.XPath, Using = "//crisp-wizard-step[@step-title='Conditions'][@class='au-target active']//conditions//crisp-table[@class='custom-element au-target custom-element']//span[text()='Conditions']")]
        public IWebElement ConditionsOptions;

        [FindsBy(How = How.XPath, Using = "//crisp-wizard-step[@step-title='Conditions'][@class='au-target active']//conditions//condition-list//crisp-table[@class='custom-element au-target custom-element']//div//table[@class='au-target']")]
        public IWebElement Conditionstable;

        [FindsBy(How = How.XPath, Using = "//crisp-wizard-step[@step-title='Conditions'][@class='au-target active']//conditions//condition-list//crisp-table[@class='custom-element au-target custom-element']//div//table[@class='au-target']/tbody/tr[@class='au-target']")]
        public IList<IWebElement> ConditionsRows;

        [FindsBy(How = How.XPath, Using = "//crisp-wizard-step[@step-title='Conditions'][@class='au-target active']//conditions//condition-list//crisp-table[@class='custom-element au-target custom-element']")]
        public IWebElement ConditionsEle;

        [FindsBy(How = How.XPath, Using = "//crisp-stepper-step[contains(@class,'au-target current')]")]
        public IWebElement FileReviewTab;

        [FindsBy(How = How.XPath, Using = "//crisp-wizard-step[@class='au-target active']//file-review[@class='custom-element au-target']//div[@class='card-title']//span[text()='File Review Summary']")]
        public IWebElement FileReviewTitle;

        [FindsBy(How = How.XPath, Using = "//crisp-stepper-step[contains(@class,'au-target current')]")]
        public IWebElement ConfirmTab;

        [FindsBy(How = How.XPath, Using = "//crisp-wizard-step[@class='au-target active']//confirm[@class='custom-element au-target']//div[@class='card-title']//span[contains(text(),'Confirm Quote Acceptance Details')]")]
        public IWebElement ConfirmPageTitle;

        [FindsBy(How = How.XPath, Using = "//crisp-stepper-step[contains(@class,'au-target current')]")]
        public IWebElement InternalRolesTab;

        [FindsBy(How = How.XPath, Using = "//crisp-wizard-step[@class='au-target active']//internal-roles[@class='custom-element au-target']//span[contains(text(),'Internal Roles')]")]
        public IWebElement InternalRoles;

        [FindsBy(How = How.XPath, Using = "//crisp-wizard-step[@step-title='internal Roles']//table//tbody[@class='collection']//tr[@class='au-target collection-item']//td[@class='au-target fixed-width error']//em[text()='Not selected']")]
        public IList<IWebElement> MandetoryRole;

        [FindsBy(How = How.XPath, Using = "//crisp-wizard-step[@step-title='internal Roles']//tbody[@class='collection']//tr[@class='au-target collection-item']//td[@class='au-target fixed-width error'][1]")]
        public IList<IWebElement> Role;

        [FindsBy(How = How.XPath, Using = "//ul[@ref='theList']//li[@class='au-target collection-item']//crisp-list-item//crisp-list-item-title//div//span[text()='Technical Administrator']")]
        public IList<IWebElement> TechAdminRole;

        //[FindsBy(How = How.XPath, Using = "//crisp-card-actions// div //crisp-button// span// button[@class='au-target waves-effect waves-light btn-flat'][text()='Cancel']")]
        //public IWebElement CancelButton;

        [FindsBy(How = How.XPath, Using = "//crisp-input-html[@class='au-target']//div//table//tbody//tr//td//p//a[@href='https://www.premierguarantee.co.uk/']//img")]
        public IList<IWebElement> PGGenerateQuote;

        [FindsBy(How = How.XPath, Using = "//crisp-stepper-step[contains(@class,'au-target last-step current')]")]
        public IWebElement GenerateQuoteAcceptanceTab;

        [FindsBy(How = How.XPath, Using = "//crisp-input-html[@class='au-target']//div//table//tbody//tr//td//h3//a[@href='https://www.labcwarranty.co.uk/']//img")]
        public IList<IWebElement> LABCGenerateQuote;

        [FindsBy(How = How.XPath, Using = "//div[@class='au-target input-field']//span[@ref='inputContainer']//span")]
        public IWebElement QuoteAcceptenceDocument;

        [FindsBy(How = How.XPath, Using = "//tr[@class='au-target collection-item']//td[contains(text(),'Quote Acceptance.pdf ')]")]
        public IWebElement QuoteAcceptenceDocumentInput;

        [FindsBy(How = How.XPath, Using = "//crisp-card-actions[@class='au-target']//div//crisp-button//button[text()='Select'][@class='au-target waves-effect waves-light btn-flat']")]
        public IWebElement SelectButton;

        [FindsBy(How = How.XPath, Using = "//div//crisp-header-title[@class='au-target'][1]/div[@class='title']")]
        public IWebElement OrderRefDetails;

        [FindsBy(How = How.XPath, Using = "//crisp-input-html[@class='au-target']//div//table//tbody//tr//td")]
        public IList<IWebElement> Correspondence;

        [FindsBy(How = How.XPath, Using = "//div[@class='modal-content']//crisp-wizard[@active-step.bind='activeStep']//crisp-stepper-step[contains(@class,'current')]//div[@title.bind='description'][contains(@class,'title')]")]
        public IWebElement StepTitle;

        //Accept Quote Product Page
        public void AcceptQuoteProductsPage()
        {
            try
            {
                WaitForElement(StepTitle);
                Assert.IsTrue(StepTitle.Text.Contains("PRODUCTS"), $"Failed to Load Products Page on Accept Quote");
                //Thread.Sleep(500);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                //Thread.Sleep(500);
                WaitForElement(ProductsTab);
                Assert.IsTrue(ProductsTab.Displayed);
                WaitForElement(ResetRecaluclateFees);
                Assert.IsTrue(ResetRecaluclateFees.Displayed);
                //Thread.Sleep(500);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                //Thread.Sleep(500);
                AcceptQuoteNextButtonMethod();
            }
            catch
            {
                wdriver.Navigate().Refresh();
                CloseSpinneronDiv();
                CloseSpinneronPage();
                //Thread.Sleep(500);
                WaitForElement(Dashboardpage.QuoteDecisionButton);
                //Thread.Sleep(500);
                Dashboardpage.QuoteDecisionButton.Click();
                //Thread.Sleep(500);
                WaitForElement(Dashboardpage.AcceptQuoteButton);
                //Thread.Sleep(500);
                Dashboardpage.AcceptQuoteButton.Click();
                //Thread.Sleep(500);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                //Thread.Sleep(500);
                WaitForElement(AcceptQuotePage.AcceptQuoteDialogueWindow);
                Assert.IsTrue(AcceptQuotePage.AcceptQuoteDialogueWindow.Displayed, "Failed to display Accept Quote Wizard");
                //Thread.Sleep(500);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                WaitForElement(StepTitle);
                Assert.IsTrue(StepTitle.Text.Contains("PRODUCTS"), $"Failed to Load Products Page on Accept Quote");
                //Thread.Sleep(500);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                //Thread.Sleep(500);
                WaitForElement(ProductsTab);
                Assert.IsTrue(ProductsTab.Displayed);
                WaitForElement(ResetRecaluclateFees);
                Assert.IsTrue(ResetRecaluclateFees.Displayed);
                //Thread.Sleep(500);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                //Thread.Sleep(500);
                AcceptQuoteNextButtonMethod();
            }
           
        }
        //Accept Quote Product Page
        public void AcceptQuoteConditionsPage()
        {
            WaitForElement(ConditionsTab);
            WaitForElement(StepTitle);
            Assert.IsTrue(StepTitle.Text.Contains("CONDITIONS"), $"Failed to Load Conditions Page on Accept Quote");
            Assert.IsTrue(ConditionsTab.Displayed);            
            WaitForElement(ConditionsOptions);            
            Assert.IsTrue(ConditionsOptions.Displayed);            
            WaitForElement(ConditionsEle);            
            Assert.IsTrue(ConditionsEle.Displayed);            
            WaitForElement(Conditionstable);            
            Assert.IsTrue(Conditionstable.Displayed);            
            if (SendQuotePage.ConditionsRows.Count > 0)
            {
                WaitForElements(ConditionsRows);
                Assert.IsTrue(ConditionsRows.Count >= 0);
                foreach (var eachrow in ConditionsRows)
                {
                    Assert.IsTrue(eachrow.Displayed);
                }
            }
            Console.WriteLine("conditions details verified");           
            AcceptQuoteNextButtonMethod();
        }
        //Accept Quote File Review Page 
        public void AcceptQuoteFileReviewPage()
        {
            WaitForElement(StepTitle);
            Assert.IsTrue(StepTitle.Text.Contains("FILE REVIEW"), $"Failed to Load File Review Page on Accept Quote");
            WaitForElement(FileReviewTab);
            Assert.IsTrue(FileReviewTab.Displayed);            
            WaitForElement(FileReviewTitle);
            Assert.IsTrue(FileReviewTitle.Displayed);            
            AcceptQuoteNextButtonMethod();
        }
        //Accept Quote Confirm page 
        public void AcceptQuoteConfirmPage()
        {
            WaitForElement(StepTitle);
            Assert.IsTrue(StepTitle.Text.Contains("CONFIRM"), $"Failed to Load Confirm Page on Accept Quote");
            WaitForElement(ConfirmTab);
            Assert.IsTrue(ConfirmTab.Displayed);          
            CloseSpinneronDiv();
            CloseSpinneronPage();
            WaitForElement(ConfirmPageTitle);
            Assert.IsTrue(ConfirmPageTitle.Displayed);
            ConfirmDetailsPage();            
            AcceptQuoteNextButtonMethod();
        }
        //Accept Quote Internal Roles Page 
        public void AcceptQuoteInternalRolesPage()
        {
            WaitForElement(StepTitle);
            Assert.IsTrue(StepTitle.Text.Contains("INTERNAL ROLES"), $"Failed to Load Internal Roles Page on Accept Quote");
            WaitForElement(InternalRolesTab);
            Assert.IsTrue(InternalRolesTab.Displayed);            
            WaitForElement(InternalRoles);
            Assert.IsTrue(InternalRoles.Displayed);            
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            if(MandetoryRole.Count > 0)
            {
                //Thread.Sleep(500);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                WaitForElements(MandetoryRole);
                WaitForElements(Role);
                Thread.Sleep(500);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                Thread.Sleep(1000);
                List<string> RoleNames = Role.Select(i => i.Text).ToList();
                CloseSpinneronDiv();
                CloseSpinneronPage();
                for (int i = 0; i < RoleNames.Count; i++)
                {
                    if (RoleNames[i].Contains("Builder"))
                    {
                        AddLABCQuotePage.SetBuilderRole();
                        //Thread.Sleep(500);
                        CloseCrispCard();
                        CloseChooseRolesCard();
                        CloseSpinneronDiv();
                        CloseSpinneronPage();
                        //Thread.Sleep(500);
                    }
                    if (RoleNames[i].Contains("Developer"))
                    {
                        AddLABCQuotePage.SetDeveloperRole();
                        //Thread.Sleep(500);
                        CloseCrispCard();
                        CloseChooseRolesCard();
                        CloseSpinneronDiv();
                        CloseSpinneronPage();
                        //Thread.Sleep(500);
                    }
                    if (RoleNames[i].Contains("Sales Account Manager"))
                    {
                        AddLABCQuotePage.SetSalesAccountManagerRole();
                        //Thread.Sleep(500);
                        CloseCrispCard();
                        CloseChooseRolesCard();
                        CloseSpinneronDiv();
                        CloseSpinneronPage();
                        //Thread.Sleep(500);
                    }
                    if (RoleNames[i].Contains("Structural Referral Administrator"))
                    {
                        AddLABCQuotePage.SetStructurralReferralAdminRole();
                        //Thread.Sleep(500);
                        CloseCrispCard();
                        CloseChooseRolesCard();
                        CloseSpinneronDiv();
                        CloseSpinneronPage();
                        //Thread.Sleep(500);
                    }
                    if (RoleNames[i].Contains("Design Review Administrator"))
                    {
                        AddLABCQuotePage.SetDesignReviewAdminRole();
                        //Thread.Sleep(500);
                        CloseCrispCard();
                        CloseChooseRolesCard();
                        CloseSpinneronDiv();
                        CloseSpinneronPage();
                        //Thread.Sleep(500);
                    }
                    if (RoleNames[i].Contains("Technical Administrator"))
                    {
                        AddLABCQuotePage.SetTechnicalAdministratorRole();
                        //Thread.Sleep(500);
                        CloseCrispCard();
                        CloseChooseRolesCard();
                        CloseSpinneronDiv();
                        CloseSpinneronPage();
                        //Thread.Sleep(500);
                    }
                    if (RoleNames[i].Contains("Building Control Provider"))
                    {
                        AddLABCQuotePage.SetBCProviderRole();
                        //Thread.Sleep(500);
                        CloseCrispCard();
                        CloseChooseRolesCard();
                        CloseSpinneronDiv();
                        CloseSpinneronPage();
                        //Thread.Sleep(500);
                    }
                    if (RoleNames[i].Contains("Hub Administrator"))
                    {
                        AddLABCQuotePage.SetHubAdministratorRole();
                        //Thread.Sleep(500);
                        CloseCrispCard();
                        CloseChooseRolesCard();
                        CloseSpinneronDiv();
                        CloseSpinneronPage();
                        //Thread.Sleep(500);
                    }

                    if (RoleNames[i].Contains("Land Owner"))
                    {
                        AddLABCQuotePage.SetLandOwnerRole();
                        //Thread.Sleep(500);
                        CloseCrispCard();
                        CloseChooseRolesCard();
                        CloseSpinneronDiv();
                        CloseSpinneronPage();
                        //Thread.Sleep(500);
                    }
                    if (RoleNames[i].Contains("Surveyor"))
                    {
                        AddLABCQuotePage.SetSurveyorRole();
                        //Thread.Sleep(500);
                        CloseCrispCard();
                        CloseChooseRolesCard();
                        CloseSpinneronDiv();
                        CloseSpinneronPage();
                        //Thread.Sleep(500);
                    }
                    if (RoleNames[i].Contains("Customer Service Account Handler"))
                    {
                        AddLABCQuotePage.SetCSURole();
                        //Thread.Sleep(500);
                        CloseCrispCard();
                        CloseChooseRolesCard();
                        CloseSpinneronDiv();
                        CloseSpinneronPage();
                        //Thread.Sleep(500);
                    }
                    if (RoleNames[i].Contains("Design Review Surveyor"))
                    {
                        AddLABCQuotePage.SetDesignReviewSurveyorRole();
                        //Thread.Sleep(500);
                        CloseCrispCard();
                        CloseChooseRolesCard();
                        CloseSpinneronDiv();
                        CloseSpinneronPage();
                        //Thread.Sleep(500);
                    }
                }
                if (Statics.ProductName.Contains("Social Housing") || Statics.ProductName.Contains("Social Housing - High Value"))
                {
                    if (RoleNames.Contains("Housing Association"))
                    {
                        AddLABCQuotePage.SetSocialHousingRole();
                        //Thread.Sleep(500);
                        CloseCrispCard();
                        CloseChooseRolesCard();
                        CloseSpinneronDiv();
                        CloseSpinneronPage();
                        //Thread.Sleep(500);
                    }
                }
                if (Statics.ProductName.Contains("Self Build"))
                {
                    if (RoleNames.Contains("Applicant (Self-build)"))
                    {
                        AddLABCQuotePage.SetSelfBuildRole();
                        //Thread.Sleep(500);
                        CloseCrispCard();
                        CloseChooseRolesCard();
                        CloseSpinneronDiv();
                        CloseSpinneronPage();
                        //Thread.Sleep(500);
                    }
                }
                if (Statics.ProductName.Contains("Private Rental") || Statics.ProductName.Contains("Private Rental - High Value"))
                {
                    if (RoleNames.Contains("PRS Company"))
                    {
                        AddLABCQuotePage.SetPRSCompanyRole();
                        //Thread.Sleep(500);
                        CloseCrispCard();
                        CloseChooseRolesCard();
                        CloseSpinneronDiv();
                        CloseSpinneronPage();
                        //Thread.Sleep(500);
                    }
                }
                if (Statics.ProductName.Contains("High Value"))
                {
                    if (RoleNames.Contains("Major Projects Manager"))
                    {
                        AddLABCQuotePage.SetMajorProjectsManagerRole();
                        //Thread.Sleep(500);
                        CloseCrispCard();
                        CloseChooseRolesCard();
                        CloseSpinneronDiv();
                        CloseSpinneronPage();
                        //Thread.Sleep(500);
                    }
                }
            }
            AcceptQuoteNextButtonMethod();
            //Thread.Sleep(500);           
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(500);
        }

        //Accept Quote Correspondence Page
        public void AcceptQuoteCorrespondencePage()
        {
            WaitForElement(StepTitle);
            Assert.IsTrue(StepTitle.Text.Contains("GENERATE QUOTE ACCEPTANCE"), $"Failed to Load Generate Quote Acceptance Page on Accept Quote Wizard");
            CloseSpinneronDiv();
            CloseSpinneronPage();
            WaitForElements(Correspondence);
            if (LABCGenerateQuote.Count > 0)
            {                
                CloseSpinneronDiv();                
                WaitForElement(LABCGenerateQuote[0]);                
                Assert.IsTrue(LABCGenerateQuote[0].Displayed);
                Console.WriteLine("LABC Generate  Quote details verified");
            }
            if (PGGenerateQuote.Count > 0)
            {                
                CloseSpinneronDiv();                
                WaitForElement(PGGenerateQuote[0]);                
                Assert.IsTrue(PGGenerateQuote[0].Displayed);
                Console.WriteLine("LABC Generate  Quote details verified");
            }       
        }
        public void AcceptQuoteNextButtonMethod()
        {
            //Thread.Sleep(500);
            CloseSpinneronPage();
            CloseSpinneronDiv();
            //Thread.Sleep(500);
            WaitForElement(AcceptQuoteNextButton);
            WaitForElementToClick(AcceptQuoteNextButton);
            AcceptQuoteNextButton.Click();
            //Thread.Sleep(500);
            CloseSpinneronPage();
            CloseSpinneronDiv();
            //Thread.Sleep(500);
        }
        public void CompleteButtonAcceptquoteWindow()
        {
            CloseSpinneronPage();
            CloseSpinneronDiv();            
            WaitForElement(CompleteButton);
            CompleteButton.Click();
            CloseSpinneronPage();
            CloseSpinneronDiv();
            CloseCrispDiv(1);            
        }
        public void OrderDetailsOnTheDashboard()
        {            
            CloseSpinneronDiv();
            CloseSpinneronPage();            
            WaitForElement(Dashboardpage.QuoteApplication);
            CloseSpinneronDiv();
            Assert.IsTrue(Dashboardpage.QuoteApplication.Displayed);            
            var orderStatus = Dashboardpage.OrderStatus.Text;
            if (orderStatus == "O")
            {
               Assert.IsTrue(orderStatus == "O");
            }
            else
            {
                WaitForElement(Dashboardpage.ReloadButton);
                Dashboardpage.ReloadButton.Click();
            }
           
            StoreOrderRefDetails();
        }
        //Accept Quote -Accept Details Page For PG and LABC products
        public void ConfirmDetailsPage()
        {            
            WaitForElement(QuoteAcceptenceDocument);
            QuoteAcceptenceDocument.Click();
            PageUtils.UploadNewFile();              
        }
        public void StoreOrderRefDetails()
        {
            WaitForElement(OrderRefDetails);
            var OrderRefNumber = OrderRefDetails.Text;
            ExtensionMethods.CreateSpreadsheet(OrderRefNumber);
            Statics.OrderNumber = OrderRefNumber;
            OrderGuidId();
        }

        public void OrderGuidId()
        {
            string currentURL =  wdriver.Url;
            string[] temp = currentURL.Split('/');
            var guid = temp[temp.Length - 1];
            Statics.OrderId = guid;
        }
        public void AcceptQuoteMain()
        {
            //Thread.Sleep(1000);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(1000);
            WaitForElement(Dashboardpage.QuoteDecisionButton);
            Dashboardpage.QuoteDecisionButton.Click();
            WaitForElement(Dashboardpage.AcceptQuoteButton);
            Dashboardpage.AcceptQuoteButton.Click();
            CloseSpinneronDiv();
            CloseSpinneronPage();
            WaitForElement(AcceptQuoteDialogueWindow);
            Assert.IsTrue(AcceptQuoteDialogueWindow.Displayed);
            CloseSpinneronDiv();
            AcceptQuoteProductsPage();
            AcceptQuoteConditionsPage();
            AcceptQuoteFileReviewPage();
            AcceptQuoteConfirmPage();
            AcceptQuoteInternalRolesPage();
            AcceptQuoteCorrespondencePage();
            CompleteButtonAcceptquoteWindow();
            OrderDetailsOnTheDashboard();
        }       
    }
}