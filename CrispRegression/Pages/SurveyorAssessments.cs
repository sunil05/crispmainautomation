﻿using CrispAutomation.Features;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CrispAutomation.Pages
{
    public class SurveyorAssessments : Support.Pages
    {
        public SurveyorAssessments(ISearchContext driver)
        {
            PageFactory.InitElements(driver, this);
        }
        public void SurveyorAssessment()
        {
            SurveyorLoginPage.SurveyorLoginMethod();

            //Any(x => x.Contains("High Value"))
            if (Statics.ConstructionType.Any(x => x.Contains("New Build")))
            {                
                SiteRiskAssessmentPage.SiteRiskAssessmentMain();
            }
            if (Statics.ConstructionType.Any(x => x.Contains("Conversion")))
            {
                
                RefurbishmentAssessmentPage.RefurbishmentAssessmentMain();
            }
            if(Statics.ProductNameList.Any(x => x.Contains("High Value"))|| Statics.BCProducts.Any(x=>x.Contains("Building Control")) || Statics.Plots.Count > 100)
            {                
                DesignReviewPage.DesignReviewMain();
            }
            //if (Statics.ProductName.Contains("High Value")||Statics.Plots.Count > 6)
            //{
            //    EngineerReviewPage.EngineerReviewMain();
            //}
            EngineerReviewPage.EngineerReviewMain();
            //if (Statics.ProductName.Contains("New Homes")) // Cover notwe
            //{
            //    CoverNotesPage.CoverNoteMain();
            //}            
            SiteInspectionPage.InspectionUpdateMain();
        }

        public void VerifySiteStatusBeforeAssessment()
        {
            if (Statics.ConstructionType.Any(x => x.Contains("New Build")) && Statics.ConstructionType.Any(x => x.Contains("Conversion")))
            {
                WaitForElementonSurveyor(SiteRiskAssessmentPage.SiteRiskAssessmentOption);
                Assert.IsTrue(SiteRiskAssessmentPage.SRAStatus.Text.Contains("Outstanding"));
                WaitForElementonSurveyor(RefurbishmentAssessmentPage.RefurbishmentAssessmentOption);
                Assert.IsTrue(RefurbishmentAssessmentPage.RAStatus.Text.Contains("Outstanding"));
            }else if (Statics.ConstructionType.Any(x => x.Contains("New Build")))
            {
                WaitForElementonSurveyor(SiteRiskAssessmentPage.SiteRiskAssessmentOption);
                Assert.IsTrue(SiteRiskAssessmentPage.SRAStatus.Text.Contains("Outstanding"));
                WaitForElementonSurveyor(RefurbishmentAssessmentPage.RefurbishmentAssessmentOption);
                Assert.IsTrue(RefurbishmentAssessmentPage.RAStatus.Text.Contains("Not required"));
            }           
            else if (Statics.ConstructionType.Any(x => x.Contains("Conversion")))
            {
                WaitForElementonSurveyor(RefurbishmentAssessmentPage.RefurbishmentAssessmentOption);
                Assert.IsTrue(RefurbishmentAssessmentPage.RAStatus.Text.Contains("Outstanding"));
                WaitForElementonSurveyor(SiteRiskAssessmentPage.SiteRiskAssessmentOption);
                Assert.IsTrue(SiteRiskAssessmentPage.SRAStatus.Text.Contains("Not required"));
            }           

            if (Statics.ProductNameList.Any(x => x.Contains("High Value")) || Statics.BCProducts.Any(x => x.Contains("Building Control")) || Statics.Plots.Count > 100)
            {
                WaitForElementonSurveyor(DesignReviewPage.DesignReviewOption);
                Assert.IsTrue(DesignReviewPage.DRStatus.Text.Contains("Outstanding"));
            }else
            {
                WaitForElementonSurveyor(DesignReviewPage.DesignReviewOption);
                Assert.IsTrue(DesignReviewPage.DRStatus.Text.Contains("Not required"));
            }

            if (Statics.ProductNameList.Any(x => x.Contains("High Value")) || Statics.Plots.Count > 6)
            {
              //  WaitForElementonSurveyor(EngineerReviewPage.EngineerReviewOption);
               // Assert.IsTrue(EngineerReviewPage.ERStatus.Text.Contains("Outstanding"));
            }
            else
            {
               // WaitForElementonSurveyor(EngineerReviewPage.EngineerReviewOption);
               // Assert.IsTrue(EngineerReviewPage.ERStatus.Text.Contains("Not required"));
            }
            WaitForElementonSurveyor(SiteInspectionPage.LastInspectionStatus);
            Assert.IsTrue(SiteInspectionPage.LastInspectionStatus.Text.Contains("No updates yet, please provide one."));

        }

        public void SubmitSurveyorAssessments()
        {
          //SurveyorLoginPage.SurveyorLoginMethod();

            //Any(x => x.Contains("High Value"))
            if (Statics.ConstructionType.Any(x => x.Contains("New Build")))
            {
                SiteRiskAssessmentPage.SubmitSiteRiskAssessmentMethod();
            }
            if (Statics.ConstructionType.Any(x => x.Contains("Conversion")))
            {

                RefurbishmentAssessmentPage.SubmitRefurbishmentAssessmentMethod();
            }
            if (Statics.ProductNameList.Any(x => x.Contains("High Value")) || Statics.BCProducts.Any(x => x.Contains("Building Control")) || Statics.Plots.Count > 100)
            {
                DesignReviewPage.SubmitDesignReviewAssessmentMethod();
            }
            //if (Statics.ProductName.Contains("High Value")||Statics.Plots.Count > 6)
            //{
            //    EngineerReviewPage.EngineerReviewMain();
            //}
            EngineerReviewPage.SubmitEngineerReviewAssessmentMethod();

            //if (Statics.ProductName.Contains("New Homes")) // Cover notwe
            //{
            //    CoverNotesPage.CoverNoteMain();
            //}

            SiteInspectionPage.InspectionUpdateMain();
        }
    }
}
