﻿using CrispAutomation.Support;
using OpenQA.Selenium;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.PageObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace CrispAutomation.Pages
{
    public class EditPersonDetailsPage : Support.Pages
    {
        public IWebDriver wdriver;

        public EditPersonDetailsPage(ISearchContext driver)
        {
            PageFactory.InitElements(driver, this);
            wdriver = (RemoteWebDriver)driver;
        }

        [FindsBy(How = How.XPath, Using = "//crisp-header-button[@click.delegate='edit()']//span//button")]
        public IWebElement EditPersonButton;

        [FindsBy(How = How.XPath, Using = "//LABEL[@class='au-target'][text()='Salutation']")]
        public IWebElement Salutation;

        [FindsBy(How = How.XPath, Using = "//crisp-input-autocomplete[@label='Salutation']/div/input")]
        public IWebElement SalutationFieldEdit;

        [FindsBy(How = How.XPath, Using = "//crisp-input-text[@label='First name']/div/input")]
        public IWebElement FirstNameFieldEdit;

        [FindsBy(How = How.XPath, Using = "//crisp-input-text[@label='Surname']/div/input")]
        public IWebElement SurNameFieldEdit;

        [FindsBy(How = How.XPath, Using = "//crisp-input-text[@label='Email']/div/input")]
        public IWebElement EmailFieldEdit;

        [FindsBy(How = How.XPath, Using = "//LABEL[@data-error.bind='error'][text()='Phone']")]
        public IWebElement PhoneNumber;

        [FindsBy(How = How.XPath, Using = "//crisp-input-text[@label='Phone']/div/input")]
        public IWebElement PhoneNumberEdit;

        [FindsBy(How = How.XPath, Using = "//LABEL[@data-error.bind='error'][text()='Mobile']")]
        public IWebElement MobileNumber;

        [FindsBy(How = How.XPath, Using = "//crisp-input-text[@label='Mobile']/div/input")]
        public IWebElement MobileNumberEdit;

        [FindsBy(How = How.XPath, Using = "//crisp-button[@enabled.bind='wizard.canGoForward'][@click.delegate='wizard.goForward()']/span/button[text()='Next']")]
        public IWebElement NextButton;
        
        [FindsBy(How = How.XPath, Using = "//ul//li//crisp-list-item//a//crisp-list-item-details[text()='Default address']")]
        public IWebElement DefaultAddress;

        [FindsBy(How = How.XPath, Using = "//crisp-action-button//div//a//i[text()='edit']")]
        public IWebElement EditAddress;

        [FindsBy(How = How.CssSelector, Using = "body > ai-dialog-container > div > div > crisp-dialog > div > div.modal-content > crisp-wizard > crisp-wizard-step.au-target.active > addresses > div > div > div > crisp-list > crisp-action-button > div > a > i")]
        public IWebElement AddressButton;

        [FindsBy(How = How.XPath, Using = "//label[@class='au-target'][@data-error='A valid outer postcode is required']")]
        public IWebElement PostcodeInput;

        [FindsBy(How = How.XPath, Using = "//crisp-input-text[@label='Enter postcode to search']/div/input")]
        public IWebElement PostcodeInputEnter;

        [FindsBy(How = How.XPath, Using = "//div[1]//div//crisp-button//span//button[text()='Search']")]
        public IWebElement SearchButton;

        [FindsBy(How = How.XPath, Using = "//crisp-card-content//div[@class='card-content']//div//crisp-picker[@label='Address']")]
        public IWebElement AddressDropdown;

        [FindsBy(How = How.XPath, Using = "//ul[@class='dropdown-content select-dropdown active']/li[1]")]
        public IWebElement AddressDropdownInput;

        [FindsBy(How = How.XPath, Using = "//ul[@class='dropdown-content select-dropdown active']/li[2]")]
        public IWebElement EditAddressDropdownInput;                

        [FindsBy(How = How.XPath, Using = "//button[text()='Enter Manually']")]
        public IWebElement EnterManuallyButton;

        [FindsBy(How = How.XPath, Using = "//LABEL[@data-error.bind='error'][text()='Address Line 1']")]
        public IWebElement AddressLine1;

        [FindsBy(How = How.XPath, Using = "//INPUT[@id='crisp-input-text-12']")]
        public IWebElement AddressLine1EnterValue;

        [FindsBy(How = How.XPath, Using = "//BUTTON[@ref='button'][text()='Confirm Selection']")]
        public IWebElement ConfirmSelection;

        [FindsBy(How = How.XPath, Using = "//crisp-card-header[@class='au-target']/div/span[text()='Address details']")]
        public IWebElement AddressDetails;

        [FindsBy(How = How.XPath, Using = "//LABEL[@data-error.bind='error'][text()='Facebook']")]
        public IWebElement FaceBookId;

        [FindsBy(How = How.XPath, Using = "//crisp-input-text[@label='Facebook']/div/input")]
        public IWebElement EditFacebookInput;

        [FindsBy(How = How.XPath, Using = "//crisp-input-text[@label='Google+']/div/input")]
        public IWebElement EditGooglePlusInput;

        [FindsBy(How = How.XPath, Using = "//crisp-input-text[@label='LinkedIn']/div/input")]
        public IWebElement EditLinkedInInput;

        [FindsBy(How = How.XPath, Using = "//crisp-input-text[@label='Twitter']/div/input")]
        public IWebElement EditTwitterInput;

        [FindsBy(How = How.CssSelector,
            Using =
                "body > ai-dialog-container > div > div > crisp-dialog > div > div.modal-content > crisp-wizard > crisp-wizard-step.au-target.active > social > div > div > div.crisp-row.height-40 > div > crisp-list > crisp-action-button > div > a > i")]
        public IWebElement AddWebsiteButton;

        [FindsBy(How = How.XPath,
            Using =
                "//crisp-input-text[@label='Website Address']/div/LABEL[@data-error.bind='error'][text()='Website Address']")]
        public IWebElement WebsiteAddress;

        [FindsBy(How = How.XPath, Using ="//crisp-input-text[@label='Website Address']/div/input[contains(@id,'crisp-input-text')][@ref='input']")]
        public IWebElement WebSiteInputField;

        [FindsBy(How = How.XPath,Using ="//ai-dialog-container//crisp-dialog//div[@class='modal-footer']//crisp-footer-button[@click.delegate='save()']//button[@class='au-target waves-effect waves-light btn-flat']")]
        public IWebElement AddButton;

        [FindsBy(How = How.XPath, Using = "//BUTTON[@ref='button'][text()='Save']")]
        public IWebElement SaveButton;

        [FindsBy(How = How.CssSelector,
            Using = "body > div.custom-element > router-view > crisp-header > nav > div > crisp-header-avatar > img")]
        public IWebElement Imageperson;

        [FindsBy(How = How.CssSelector, Using = "#crisp-picker-1 > div > input")]
        public IWebElement PhoneCountry;

        [FindsBy(How = How.CssSelector, Using = "#crisp-picker-1 > div > select")]
        public IWebElement PhoneCountryDropdown;

        [FindsBy(How = How.CssSelector, Using = "#crisp-picker-2 > div > input")]
        public IWebElement MobileCountry;

        [FindsBy(How = How.CssSelector, Using = "#crisp-picker-2 > div > select")]
        public IWebElement MobileCountryDropdown;

        [FindsBy(How = How.XPath, Using = "//crisp-button/span/button[text()='Enter Address Using Postcode']")]
        public IWebElement EnterAddressUsingPostcodeButton;

        [FindsBy(How = How.XPath, Using = "//crisp-button[1]//span//button[text()='Set Address'][@class='au-target waves-effect waves-light btn']")]
        public IWebElement SetAddressButton;

        public void EditPersonetails()
        {
            SalutationFieldEdit.Click();
            SalutationFieldEdit.Clear();
            SalutationFieldEdit.SendKeysAndtab("Mr");
            FirstNameFieldEdit.Click();
            FirstNameFieldEdit.Clear();
            FirstNameFieldEdit.SendKeys("Sunilkumar");
            SurNameFieldEdit.Click();
            SurNameFieldEdit.Clear();
            SurNameFieldEdit.SendKeys("Sunkishala");
            EmailFieldEdit.Click();
            EmailFieldEdit.Clear();
            EmailFieldEdit.SendKeys("crisp.dev.c@ext.crisp.co.uk");
            WaitForElement(PhoneNumberEdit);
            PhoneNumberEdit.Click();
            PhoneNumberEdit.Clear();
            PhoneNumberEdit.SendKeys("01493593721");
            WaitForElement(MobileNumberEdit);
            MobileNumberEdit.Click();
            MobileNumberEdit.Clear();
            MobileNumberEdit.SendKeys("07934233944");
            ClickNextButton();
        }
        public void ClickNextButton()
        {
            WaitForElement(EditPersonDetailsPage.NextButton);
            EditPersonDetailsPage.NextButton.Click();
        }
        public void EditAddressDetails()
        {
            DefaultAddress.Click();
            WaitForElement(EditAddress);
            EditAddress.Click();
            WaitForElement(EnterAddressUsingPostcodeButton);
            EnterAddressUsingPostcodeButton.Click();
            WaitForElement(PostcodeInputEnter);
            PostcodeInputEnter.Click();
            PostcodeInputEnter.Clear();
            PostcodeInputEnter.SendKeys("CH41 1AU");
            Thread.Sleep(500);
            WaitForElement(AddressDropdown);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            WaitForElementToClick(AddressDropdown);
            AddressDropdown.Click();
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            WaitForElement(EditAddressDropdownInput);
            WaitForElementToClick(EditAddressDropdownInput);
            EditAddressDropdownInput.Click();
            CloseSpinneronDiv();
            CloseSpinneronPage();
            WaitForElement(SetAddressButton);
            SetAddressButton.Click();
            CloseCrispCard();

        }
        public void AddNewAddressDetails()
        {
            WaitForElement(AddressButton);
            AddressButton.Click();
            WaitForElement(PostcodeInputEnter);
            PostcodeInputEnter.SendKeys("L1 0AA");
            WaitForElement(AddressDropdown);
            AddressDropdown.Click();
            WaitForElement(AddressDropdownInput);
            AddressDropdownInput.Click();
            WaitForElement(SetAddressButton);
            SetAddressButton.Click();
            CloseCrispCard();
        }
        public void EditSocialInfo()
        {
            EditFacebookInput.Click();
            EditFacebookInput.Clear();
            EditFacebookInput.SendKeys("https://www.facebook.com/sunilkumar");
            EditGooglePlusInput.Click();
            EditGooglePlusInput.Clear();
            EditGooglePlusInput.SendKeys("https://plus.google.com/sunilkumar");
            EditLinkedInInput.Click();
            EditLinkedInInput.Clear();
            EditLinkedInInput.SendKeys("https://www.linkedin.com/Sunilkumar");
            EditTwitterInput.Click();
            EditTwitterInput.Clear();
            EditTwitterInput.SendKeys("@SunilKumar");
            WaitForElement(AddWebsiteButton);
            AddWebsiteButton.Click();
            WaitForElement(WebsiteAddress);
            WebsiteAddress.Click();
            WebSiteInputField.SendKeys("www.mdis.com");
            WaitForElement(AddButton);
            AddButton.Click();
        }
        public void EditPersonDetailsMain()
        {
            EditPersonetails();
            WaitForElement(EditPersonDetailsPage.DefaultAddress);
            EditPersonDetailsPage.EditAddressDetails();
            EditAddressDetails();
            WaitForElement(EditPersonDetailsPage.NextButton);
            EditPersonDetailsPage.NextButton.Click();
            WaitForElement(EditPersonDetailsPage.EditFacebookInput);
            EditPersonDetailsPage.EditSocialInfo();
            WaitForElement(EditPersonDetailsPage.SaveButton);
            WaitForElement(EditPersonDetailsPage.SaveButton);
            EditPersonDetailsPage.SaveButton.Click();
            CloseSpinneronDiv();
            CloseSpinneronPage();
        }
    }
}