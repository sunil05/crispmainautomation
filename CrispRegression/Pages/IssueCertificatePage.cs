﻿using CrispAutomation.Features;
using CrispAutomation.Support;
using ExcelDataReader;
using Microsoft.Office.Interop.Excel;
using NUnit.Framework;
using OfficeOpenXml;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.PageObjects;
using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CrispAutomation.Pages
{
    public class IssueCertificatePage : Support.Pages
    {
        public IWebDriver wdriver;
        System.Data.DataTable excelTable;
        bool builderSecurityDocCondition = false;
        bool builderRegistrationFeeCondition = false;
        bool developerSecurityDocCondition = false;
        bool developerRegistrationFeeCondition = false;
        bool selfBuildIndemnityDocCondition = false;
        public IssueCertificatePage(ISearchContext driver)
        {
            PageFactory.InitElements(driver, this);
            wdriver = (RemoteWebDriver)driver;
        }

        //Pay the Fee Balance for issue the order 
        [FindsBy(How = How.XPath, Using = "//crisp-tabs//div//ul[@ref='tabHeader'][@class='au-target tabs highlight-background']//li[@class='au-target tab']//span//a[contains(text(),'Account')][contains(@class,'au-target highlight-colour')]")]
        public IWebElement AccountTab;

        [FindsBy(How = How.XPath, Using = "//div//account-summary[@summary.bind='summary']//dl/dt[text()='Fee balance']//following-sibling::dd[1]")]
        public IWebElement FeeBalance;

        [FindsBy(How = How.XPath, Using = "//div//account-summary[@summary.bind='summary']//dl/dt[text()='Escrow balance']//following-sibling::dd[1]")]
        public IWebElement EscrowBalance;

        [FindsBy(How = How.XPath, Using = "//crisp-card-content//div//crisp-input-text[@label='Reference/name']//div//label")]
        public IWebElement RefLabel;

        [FindsBy(How = How.XPath, Using = "//crisp-card-content//div//crisp-input-text[@label='Reference/name']//div//input")]
        public IWebElement RefInput;

        [FindsBy(How = How.XPath, Using = "//crisp-dialog/div/crisp-header/nav/div")]
        public IWebElement ICDiv;

        [FindsBy(How = How.XPath, Using = "//crisp-wizard-step//plots//crisp-card-content//crisp-input-object//span[@ref='inputContainer']")]
        public IWebElement SelectPlot;

        [FindsBy(How = How.XPath, Using = "//div[@class='card-content']//crisp-list//ul[@ref='theList']//li[@class='au-target collection-item']//crisp-list-item//a[@class='au-target row list-item-contents clickable key-item']")]
        public IList<IWebElement> SelectPlotInput;

        [FindsBy(How = How.XPath, Using = "//crisp-card-actions//crisp-button[@click.delegate='ok()']//button[@class='au-target waves-effect waves-light btn']")]
        public IWebElement SelectButton;

        [FindsBy(How = How.XPath, Using = "//crisp-footer//nav//div//crisp-footer-button[@click.delegate='goForward()']/button[@class='au-target waves-effect waves-light btn-flat'][text()='Next']")]
        public IWebElement ICNextButton;

        [FindsBy(How = How.XPath, Using = "//crisp-wizard-step[@step-title='File Review'][@class='au-target active']//file-review//div//span[text()='File Review Summary']")]
        public IWebElement FileReviewPage;

        [FindsBy(How = How.XPath, Using = "//crisp-tabs/div/ul/li/span/a[contains(text(),'Blocking (0)')]")]
        public IWebElement Blockers;

        [FindsBy(How = How.XPath, Using = "//crisp-card-content/div/div[contains(text(),'There are no companies or individuals with blocks')]")]
        public IWebElement NoBlockers;

        [FindsBy(How = How.XPath, Using = "//crisp-tabs/div/ul/li/span/a[contains(text(),'Conditions (0)')]")]
        public IWebElement Conditions;

        [FindsBy(How = How.XPath, Using = "//crisp-card-content/div/div[contains(text(),'There are no Conditions')]")]
        public IWebElement NoConditions;

        [FindsBy(How = How.XPath, Using = "//crisp-tabs/div/ul/li/span/a[contains(text(),'Fees (0)')]")]
        public IWebElement Fees;

        [FindsBy(How = How.XPath, Using = "//crisp-tabs/div/ul/li/span/a[contains(text(),'Defects (0)')]")]
        public IWebElement Defects;

        [FindsBy(How = How.XPath, Using = "//crisp-stepper-step[@class='au-target current']//div//div[@class='highlight-colour title au-target'][@title.bind='description']")]
        public IWebElement ConfirmPage;

        [FindsBy(How = How.XPath, Using = "//crisp-card//crisp-card-header//div//span[text()='Complete send Development Initial Certificate']")]
        public IWebElement ConfirmationonDevIC;

        //[FindsBy(How = How.XPath, Using = "//div//table//tbody//tr//td//p//a[@href='https://www.premierguarantee.co.uk/']//img")]
        //public IWebElement PGGenerateQuote;

        [FindsBy(How = How.XPath, Using = "//crisp-footer//nav//div//crisp-footer-button[@click.delegate='save()']/button[@class='au-target waves-effect waves-light btn-flat'][text()='Send Plot Initial Certificate']")]
        public IWebElement SendPlotICButton;

        [FindsBy(How = How.XPath, Using = "//crisp-footer//nav//div//crisp-footer-button[@click.delegate='save()']/button[@class='au-target waves-effect waves-light btn-flat'][text()='Send Development Initial Certificate']")]
        public IWebElement SendDevICButton;

        [FindsBy(How = How.XPath, Using = "//crisp-input-html[@class='au-target']//div//table//tbody//tr//td//h3//a[@href='https://www.labcwarranty.co.uk/']//img")]
        public IList<IWebElement> LABCGenerateQuote;

        [FindsBy(How = How.XPath, Using = "//crisp-input-html[@class='au-target']//div//table//tbody//tr//td//p//a[@href='https://www.premierguarantee.co.uk/']//img")]
        public IList<IWebElement> PGGenerateQuote;

        [FindsBy(How = How.XPath, Using = "//crisp-footer//nav//div//crisp-footer-button[@click.delegate='save()']/button[@class='au-target waves-effect waves-light btn-flat'][text()='Send Certificate of Insurance']")]
        public IWebElement SendCOIButton;

        // Clearing Conditions to issue COI 

        [FindsBy(How = How.XPath, Using = "//crisp-tabs//div//ul[@ref='tabHeader']/li[@class='au-target tab']//span//a[contains(text(),'Conditions')]")]
        public IWebElement ConditionsTab;

        [FindsBy(How = How.XPath, Using = "//condition-list[@class='custom-element au-target']//div[@class='au-target crisp-row table-body with-header']//table[@ref='table']//tbody//tr")]
        public IList<IWebElement> ConditionsRows;

        [FindsBy(How = How.XPath, Using = "//crisp-tabs//div//ul[@ref='tabHeader']/li[@class='au-target tab']//span//a[contains(text(),'Roles')]")]
        public IWebElement RolesTab;

        [FindsBy(How = How.XPath, Using = "//role-editor//crisp-list//div[@ref='theTable']//table//tbody[@class='collection']//tr[@class='au-target collection-item']//td")]
        public IList<IWebElement> RolesRows;

        [FindsBy(How = How.XPath, Using = "//role-editor//crisp-list//div[@ref='theTable']//table//tbody[@class='collection']//tr[@class='au-target collection-item']//td[contains(text(),'Builder')]/following-sibling::td[1]")]
        public IList<IWebElement> BuilderRole;

        [FindsBy(How = How.XPath, Using = "//role-editor//crisp-list//div[@ref='theTable']//table//tbody[@class='collection']//tr[@class='au-target collection-item']//td[contains(text(),'Developer')]/following-sibling::td[1]")]
        public IList<IWebElement> DeveloperRole;


        [FindsBy(How = How.XPath, Using = "//i[@class='au-target fa fa-search']")]
        public IWebElement SearchButton;

        [FindsBy(How = How.XPath, Using = "//a[@click.delegate='subItem.onClick()'][text()='Contacts']")]
        public IWebElement ContactOption;

        [FindsBy(How = How.XPath, Using = "//crisp-tabs//ul[@ref='tabHeader']//li[@class='au-target tab']//span//a[contains(text(),'PG Registration')]")]
        public IWebElement PGRegistrationTab;

        [FindsBy(How = How.XPath, Using = "//crisp-tabs//ul[@ref='tabHeader']//li[@class='au-target tab']//span//a[contains(text(),'LABC Registration')]")]
        public IWebElement LABCRegistrationTab;

        [FindsBy(How = How.XPath, Using = "//div[@ref='tabContainer']//ul[@ref='tabHeader']//li[@class='au-target tab']//span//a[contains(text(),'Reg Documents')]")]
        public IWebElement RegDocumentTab;

        [FindsBy(How = How.XPath, Using = "//security-documents//crisp-list[contains(@source.bind,'SecurityDocuments')]//crisp-input-bool[@label='Show Site Specific Documents']//label")]
        public IList<IWebElement> ShowSiteSpecificDocs;

        [FindsBy(How = How.XPath, Using = "//security-documents//crisp-list[contains(@source.bind,'SecurityDocuments')]//ul[@ref='theList']//li[@class='au-target collection-item']")]
        public IList<IWebElement> SecurityDocsList;

        [FindsBy(How = How.XPath, Using = "//crisp-card-actions//div//crisp-button//span//button[text()='Upload'][@class='au-target waves-effect waves-light btn-flat']")]
        public IWebElement UploadButton;

        [FindsBy(How = How.XPath, Using = "//ai-dialog-container[2]/div/div/crisp-dialog/div/crisp-header/nav/div")]
        public IWebElement UploadDiv;

        [FindsBy(How = How.XPath, Using = "//div[@class='modal-content']//crisp-input-object[@icon='file']//div[@class='au-target input-field']//span[@ref='inputContainer']")]
        public IWebElement RelatedFiles;

        [FindsBy(How = How.XPath, Using = "//button[text()='Upload New Files'][@class='au-target waves-effect waves-light btn-flat']")]
        public IWebElement UploadNewFilesButton;

        [FindsBy(How = How.XPath, Using = "//crisp-card-content//div//upload-documents-embed//crisp-input-file[@class='au-target']//div//div//input[@ref='fileInput']")]
        public IWebElement SendFileInput;

        [FindsBy(How = How.XPath, Using = "//crisp-input-file/div/div[1]/input")]
        public IWebElement UploadDoc;


        [FindsBy(How = How.XPath, Using = "//crisp-input-file[@label='Select file...']//div//div//input[@class='au-target']")]
        public IWebElement UploadPlotSchedule;

        [FindsBy(How = How.XPath, Using = "//tr//td//crisp-input-object[@class='au-target']//div[contains(@class,'au-target input-field')]//span[@ref='inputContainer']")]
        public IWebElement DocTypeDropdown1;

        [FindsBy(How = How.XPath, Using = "//crisp-card-content[@class='au-target']//div[@class='au-target list-title has-filter']//span[@class='filter-input-field']//input[@class='au-target']")]
        public IWebElement SearchDocumentType;

        [FindsBy(How = How.XPath, Using = "//ul[@class='au-target collection has-header has-filter']//li[@class='au-target collection-item']//crisp-list-item")]
        public IList<IWebElement> SearchDocumentTypeInputList;

        [FindsBy(How = How.XPath, Using = "//ul/li/span[text()='Site Location Plan']")]
        public IWebElement SiteLocationPlanType;

        [FindsBy(How = How.XPath, Using = "//table[@class='striped']//crisp-input-date/div/label[text()='Date Received']")]
        public IWebElement DateReceivedLabel;

        [FindsBy(How = How.XPath, Using = "//div[contains(@class,'picker--opened')]/div/div/div/div/div[@class='picker__footer']/button[text()='Today']")]
        public IWebElement DateReceivedInput;

        [FindsBy(How = How.XPath, Using = "//crisp-input-date[@label='Date Received']//div//label")]
        public IWebElement DateOnUploadDiv;

        [FindsBy(How = How.XPath, Using = "//crisp-button[@class='au-target']//span//button[text()='Select'][@class='au-target waves-effect waves-light btn']")]
        public IWebElement Select;

        [FindsBy(How = How.XPath, Using = "//button[text()='OK'][@class='au-target waves-effect waves-light btn-flat']")]
        public IWebElement OkButton;

        [FindsBy(How = How.XPath, Using = "//crisp-card-actions//div//crisp-button//span//button[text()='Review'][@class='au-target waves-effect waves-light btn-flat']")]
        public IWebElement ReviewButton;

        [FindsBy(How = How.XPath, Using = "//ai-dialog-container[2]/div/div/crisp-dialog/div")]
        public IWebElement ReviewDiv;

        [FindsBy(How = How.XPath, Using = "//crisp-picker[@label='Review Status']//div//input[@class='select-dropdown']")]
        public IWebElement ReviewStatusDropdown;

        [FindsBy(How = How.XPath, Using = "//ul[@class='dropdown-content select-dropdown active']//li//span[text()='Confirmed']")]
        public IWebElement ReviewStatusDropdownInput;

        [FindsBy(How = How.XPath, Using = "//crisp-card-actions//div//crisp-button//span//button[text()='Close'][@class='au-target waves-effect waves-light btn-flat']")]
        public IWebElement CloseButton;

        //Making Registration Fee for the Builder and Developer 
        [FindsBy(How = How.XPath, Using = "//div[@ref='tabContainer']//ul[@ref='tabHeader']//li[@class='au-target tab']//span//a[contains(text(),'Account')]")]
        public IWebElement RoleAccountTab;

        [FindsBy(How = How.XPath, Using = "//crisp-card-content//div[@class='card-content']//account-summary//dl//dt[text()='Reference/name']//following-sibling::dd[1]")]
        public IWebElement RegistrationRef;

        [FindsBy(How = How.XPath, Using = "//crisp-card-content//div[@class='card-content']//account-summary//dl//dt[text()='Fee balance']//following-sibling::dd[1]")]
        public IWebElement RegFeeBalance;

        [FindsBy(How = How.XPath, Using = "//crisp-card-content//div[@class='card-content']//account-summary//dl//dt[text()='Fee paid']//following-sibling::dd[1]")]
        public IWebElement RegFeePaid;

        [FindsBy(How = How.XPath, Using = "//crisp-card-content//div[@class='card-content']//crisp-button[@click.delegate='paymentToClient()']//button")]
        public IWebElement EnterPaymentButton;

        [FindsBy(How = How.XPath, Using = "//ai-dialog-container/div/div/crisp-dialog/div/crisp-header/nav/div")]
        public IWebElement MakePaymentDiv;

        // Additional Conditions 
        [FindsBy(How = How.XPath, Using = "//crisp-tabs//div//ul[@ref='tabHeader']/li[@class='au-target tab']//span//a[contains(text(),'Plot Schedule')]")]
        public IWebElement PlotScheduleTab;

        [FindsBy(How = How.XPath, Using = "//plot-schedule-order//crisp-header-button[@click.delegate='amendPlotSchedule()']//button[@class='au-target waves-effect waves-light btn-flat'][text()='Amend']")]
        public IWebElement AmendOrderButton;

        [FindsBy(How = How.XPath, Using = "//ai-dialog-container//crisp-dialog//div[contains(@class,'modal-fixed-footer modal-has-header modal-full-bleed modal-tall modal-wide')]//crisp-header[@class='au-target']")]
        public IWebElement AmendOrderDiv;

        [FindsBy(How = How.XPath, Using = "//crisp-wizard-step[@step-title='Plot Schedule']//crisp-header-button[@icon='download']//button[@class='au-target waves-effect waves-light btn-flat'][text()='Download Plot Schedule']")]
        public IWebElement DownloadPlotScheduleButton;

        [FindsBy(How = How.XPath, Using = "//crisp-wizard-step[@step-title='Plot Schedule']//crisp-header-button[@click.delegate='importPlots()']//button[@class='au-target waves-effect waves-light btn-flat'][text()='Import']")]
        public IWebElement ImportButton;

        [FindsBy(How = How.XPath, Using = "//crisp-button[@click.delegate='ok()'][@class='au-target']//span//button[text()='OK'][@class='au-target waves-effect waves-light btn-flat']")]
        public IWebElement UploadOkButton;

        [FindsBy(How = How.XPath, Using = "//crisp-button[@class='au-target'][@click.delegate='wizard.goForward()']//span//button[@class='au-target waves-effect waves-light btn-flat'][text()='Next']")]
        public IWebElement NextButton;

        [FindsBy(How = How.XPath, Using = "//crisp-button[@class='au-target'][@click.delegate='save()']//span//button[@class='au-target waves-effect waves-light btn-flat'][text()='Confirm']")]
        public IWebElement ConfirmButton;

        [FindsBy(How = How.XPath, Using = "//div[@class='modal-content']//span[@class='message'][contains(text(),'No QGU Referral is required to confirm these changes for this order amendment request. Would you like to manually trigger a QGU Referral?')]")]
        public IWebElement QGUReferralConfirmDiv;

        [FindsBy(How = How.XPath, Using = "//crisp-button[@class='au-target']//button[@class='au-target waves-effect waves-light btn'][text()='No']")]
        public IWebElement QGUReferralNoButton;


        [FindsBy(How = How.XPath, Using = "//crisp-input-radio/div/ul/li/label[contains(text(),'Override Conditions/Blocks & Send')]")]
        public IList<IWebElement> OverrideConditionsRadioButton;

        [FindsBy(How = How.XPath, Using = "//crisp-wizard-step[@class='au-target active']//complete//crisp-card//crisp-card-content//div//p[text()='There are no blocks on issuing the Final Notice for the selected plots']")]
        public IList<IWebElement> ConfirmDetails;

        [FindsBy(How = How.XPath, Using = "//crisp-input-textarea[@label='Override comments']//div//label")]
        public IWebElement CommentsLabel;

        [FindsBy(How = How.XPath, Using = "//crisp-input-textarea[@label='Override comments']//div//textarea")]
        public IWebElement CommentsInput;



        public static void ClearExistingFiles()
        {
            var fileInfo = new FileInfo(ExtensionMethods.GetAbsolutePath(@"TestData\Conditions"));
            DirectoryInfo downloadFolder = new DirectoryInfo($"{fileInfo}");

            foreach (DirectoryInfo dir in downloadFolder.EnumerateDirectories())
            {
                foreach (FileInfo file in dir.EnumerateFiles())
                {
                    file.Delete();
                }
            }
        }

        // Clearing Additional Conditions on LABC brand  - Clear Final Notice Condition On LABC Completed Housing Product and Property Owner condition on Self Build Product 
        public void LABCAdditionalConditions()
        {
            //Thread.Sleep(1000);
            if (Dashboardpage.DecisionButton.Count <= 0)
            {
                Dashboardpage.SelectOrder();
            }
            WaitForElement(ConditionsTab);
            ConditionsTab.Click();
            //Thread.Sleep(1500);
            CloseSpinneronPage();
            WaitForElements(ConditionsRows);
            if (ConditionsRows.Count > 0)
            {
                // Downloading Plot Schedule Data 
                //Thread.Sleep(1000);
                var finalNoticeCondition = ConditionsRows.Any(x => x.Text.Contains("Building Control Final Notice is required"));
                var propertyOwnerCondition = ConditionsRows.Any(x => x.Text.Contains("A property owner needs to be noted for the plot"));
                var constructionEndDateOnCHSite = ConditionsRows.Any(x => x.Text.Contains("This is a CH site, you need to input the construction end date (from App form or from confirmation from the client i.e a utility bill show the date they moved in) for the required plot, into the plot uploader, to calculate the COI dates"));
                var constructionEndDateOnCHSOBSite = ConditionsRows.Any(x => x.Text.Contains("This is a CH SOB site, you need to input the LA completion certificate date on the plot uploader, to calculate the COI dates."));
                // ClearExistingFiles();               
                ExtensionMethods.EmptyFolder(new DirectoryInfo(ExtensionMethods.GetAbsolutePath(@"TestData\Conditions")));
                ExtensionMethods.EmptyFolder(new DirectoryInfo($@"{Statics.DownloadFolder}"));
                if (finalNoticeCondition == true || propertyOwnerCondition == true || constructionEndDateOnCHSite == true || constructionEndDateOnCHSOBSite == true)
                {
                    WaitForElement(PlotScheduleTab);
                    PlotScheduleTab.Click();
                    //Thread.Sleep(1000);
                    CloseSpinneronDiv();
                    CloseSpinneronPage();
                    WaitForElement(AmendOrderButton);
                    AmendOrderButton.Click();
                    //Thread.Sleep(1000);
                    CloseSpinneronPage();
                    CloseSpinneronDiv();
                    WaitForElement(AmendOrderDiv);
                    Assert.IsTrue(AmendOrderDiv.Displayed);
                    WaitForElement(DownloadPlotScheduleButton);
                    //Thread.Sleep(1000);
                    CloseSpinneronPage();
                    CloseSpinneronDiv();
                    DownloadPlotScheduleButton.Click();
                    //Thread.Sleep(3000);
                    CloseSpinneronPage();
                    CloseSpinneronDiv();
                    //Thread.Sleep(1000);
                    //Download to following directory 
                    var directory = new DirectoryInfo($@"{Statics.DownloadFolder}");
                    //Thread.Sleep(1000);
                    var myFile = directory.GetFiles()
             .OrderByDescending(f => f.LastWriteTime)
             .First();
                    //Thread.Sleep(1000);

                    var filePath = new DirectoryInfo($@"{Statics.DownloadFolder}\{myFile}");
                    var excel = new ExcelPackage(myFile);

                    excelTable = ReadExcelPlotData(filePath.ToString());

                    //Read Excel Table 
                    var completedHousingRow = -1;
                    var selfBuildRow = -1;
                    var commercialsRow = -1;
                    var commercialsHVSRow = -1;
                    string ownerName = "SunilKumar";
                    //Thread.Sleep(1000);

                    if (Statics.ProductNameList.Any(o => o.Equals("Self Build")) || Statics.ProductNameList.Any(o => o.Equals("Commercial")) || Statics.ProductNameList.Any(o => o.Equals("Commercial - High Value")) || Statics.ProductNameList.Any(o => o.Equals("Completed Housing")))
                    {
                        if (Statics.ProductNameList.Any(o => o.Equals("Completed Housing")))
                        {
                            for (int i = 0; i < excelTable.Rows.Count; i++)
                            {
                                if (excelTable.Rows[i][1].ToString() == "Completed Housing")
                                {
                                    completedHousingRow = i;
                                    DateTime date = DateTime.Today; // will give the date for today
                                    string dateWithFormat = date.ToString("dd/MM/yyyy HH:mm");
                                    excel.Workbook.Worksheets["Plots"].Cells[$"Z{completedHousingRow + 2}"].Value = dateWithFormat;

                                }
                            }
                            if (completedHousingRow == -1)
                                throw new Exception("completedHousingRow Row Not Found");
                        }
                        if (Statics.ProductNameList.Any(o => o.Equals("Self Build")))
                        {
                            for (int i = 0; i < excelTable.Rows.Count; i++)
                            {
                                if (excelTable.Rows[i][1].ToString() == "Self Build")
                                {
                                    selfBuildRow = i;
                                    excel.Workbook.Worksheets["Plots"].Cells[$"S{selfBuildRow + 2}"].Value = ownerName;
                                }
                            }
                            if (selfBuildRow == -1)
                                throw new Exception("selfBuildRow Row Not Found");

                        }
                        if (Statics.ProductNameList.Any(o => o.Equals("Commercial")))
                        {
                            for (int i = 0; i < excelTable.Rows.Count; i++)
                            {
                                if (excelTable.Rows[i][1].ToString() == "Commercial")
                                {
                                    commercialsRow = i;
                                    excel.Workbook.Worksheets["Plots"].Cells[$"S{commercialsRow + 2}"].Value = ownerName;
                                }
                            }
                            if (commercialsRow == -1)
                                throw new Exception("Commercial Row Not Found");
                        }


                        if (Statics.ProductNameList.Any(o => o.Equals("Commercial - High Value")))
                        {
                            for (int i = 0; i < excelTable.Rows.Count; i++)
                            {
                                if (excelTable.Rows[i][1].ToString() == "Commercial - High Value")
                                {
                                    commercialsHVSRow = i;
                                    excel.Workbook.Worksheets["Plots"].Cells[$"S{commercialsHVSRow + 2}"].Value = ownerName;
                                }
                            }
                            if (commercialsHVSRow == -1)
                                throw new Exception("Commercial - High Value Row Not Found");
                        }
                        //string plotData = $@"\\tpgfile\NewCompany\IT\Department\QA Team\Current Projects\CRISP\CrispAutomation\CrispData\Conditions\{Statics.ProductName}\plots.xlsx";
                        var fileInfo = new FileInfo(ExtensionMethods.GetAbsolutePath($@"TestData\Conditions\plots.xlsx"));
                        string plotData = fileInfo.ToString();
                        excel.SaveAs(new FileInfo(plotData));
                        //Thread.Sleep(1000);
                        CloseSpinneronPage();
                        CloseSpinneronDiv();
                        WaitForElement(ImportButton);
                        ImportButton.Click();
                        CloseSpinneronDiv();
                        //Thread.Sleep(1000);
                        UploadPlotSchedule.SendKeys(plotData);
                        log.Info("File has been uploaded");
                        //Thread.Sleep(500);
                        //Thread.Sleep(500);
                        CloseSpinneronDiv();
                        CloseSpinneronPage();
                        WaitForElement(UploadOkButton);
                        UploadOkButton.Click();
                        CloseSpinneronDiv();
                        CloseSpinneronPage();
                        Thread.Sleep(500);
                        WaitForElement(ConfirmButton);
                        WaitForElementToClick(ConfirmButton);
                        ConfirmButton.Click();
                        Thread.Sleep(1000);
                        WaitForElement(QGUReferralConfirmDiv);
                        WaitForElementToClick(QGUReferralConfirmDiv);
                        WaitForElement(QGUReferralNoButton);
                        QGUReferralNoButton.Click();
                        CloseSpinneronDiv();
                        CloseSpinneronPage();
                    }
                }
            }
        }

        //Clear Final notice Contion 
        public void FinalNoticeCondition()
        {
            if (Dashboardpage.DecisionButton.Count <= 0)
            {
                Dashboardpage.SelectOrder();
            }
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            WaitForElement(ConditionsTab);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            WaitForElementToClick(ConditionsTab);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            ConditionsTab.Click();
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            //If there is BC on the product Sedn Final Notice Condition to be closed
            if (ConditionsRows.Count > 0)
            {
                var FinalNoticeCondition = ConditionsRows.Any(x => x.Text.Contains("Issue Building Control Final Notice"));
                if (FinalNoticeCondition == true)
                {
                    SendIntialNotice.SendIntialNoticeMethod();
                    SendIntialNotice.RespondtoIntialNoticeMethod();
                    FinalNoticePage.SendFinalNotice();
                }
            }
            //Thread.Sleep(500);
            CloseSpinneronPage();
            CloseSpinneronDiv();
            //Thread.Sleep(500);
            WaitForElement(ConditionsTab);
            //Thread.Sleep(500);
            CloseSpinneronPage();
            CloseSpinneronDiv();
            //Thread.Sleep(500);
            ConditionsTab.Click();
            //Thread.Sleep(1500);
            CloseSpinneronPage();
            if (ConditionsRows.Count > 0)
            {
                var IntialNoticeCondition = ConditionsRows.Any(x => x.Text.Contains("Please issue the Initial Notice"));
                if (IntialNoticeCondition == true)
                {
                    SendIntialNotice.SendIntialNoticeMethod();
                    SendIntialNotice.RespondtoIntialNoticeMethod();
                }
            }
        }
        // Clearing Additional Conditions on PG brand  - Clear Property Owner condition on Self Build Product 
        public void PGAdditionalConditions()
        {
            //Thread.Sleep(1000);
            if (Dashboardpage.DecisionButton.Count <= 0)
            {
                Dashboardpage.SelectOrder();
            }
            WaitForElement(ConditionsTab);
            ConditionsTab.Click();
            Thread.Sleep(500);
            CloseSpinneronPage();
            CloseSpinneronDiv();
            Thread.Sleep(500);
            var conditions = ConditionsRows.Count > 0;
            Thread.Sleep(500);
            CloseSpinneronPage();
            CloseSpinneronDiv();
            Thread.Sleep(500);
            if (conditions == true)
            {
                if (ConditionsRows.Count > 0)
                {
                    // Downloading Plot Schedule Data 
                    //Thread.Sleep(1000);
                    var propertyOwnerCondition = ConditionsRows.Any(x => x.Text.Contains("A property owner needs to be noted for the plot"));
                    if (propertyOwnerCondition == true)
                    {
                        WaitForElement(PlotScheduleTab);
                        PlotScheduleTab.Click();
                        //Thread.Sleep(1000);
                        CloseSpinneronPage();
                        WaitForElement(AmendOrderButton);
                        AmendOrderButton.Click();
                        Thread.Sleep(500);
                        CloseSpinneronPage();
                        CloseSpinneronDiv();
                        WaitForElement(AmendOrderDiv);
                        Assert.IsTrue(AmendOrderDiv.Displayed);
                        WaitForElement(DownloadPlotScheduleButton);
                        DownloadPlotScheduleButton.Click();
                        Thread.Sleep(1000);
                        CloseSpinneronPage();
                        CloseSpinneronDiv();
                        //Download to following directory 
                        var directory = new DirectoryInfo(Statics.DownloadFolder);
                        Thread.Sleep(500);
                        var myFile = directory.GetFiles()
                 .OrderByDescending(f => f.LastWriteTime)
                 .First();
                        Thread.Sleep(500);
                        var filePath = new DirectoryInfo($@"{Statics.DownloadFolder}\{myFile}");
                        var excel = new ExcelPackage(myFile);
                        excelTable = ReadExcelPlotData(filePath.ToString());

                        var selfBuildRow = -1;
                        var commercialsRow = -1;
                        var commercialsHVSRow = -1;
                        var ciCommercialsHVSRow = -1;
                        var ciCommercialsRow = -1;
                        string ownerName = "SunilKumar";
                        //open file and returns as Stream                   
                        if (Statics.ProductNameList.Any(o => o.Equals("Self Build")))
                        {
                            if (Statics.ProductNameList.Any(o => o.Equals("Self Build")))
                            {
                                for (int i = 0; i < excelTable.Rows.Count; i++)
                                {
                                    if (excelTable.Rows[i][1].ToString() == "Self Build")
                                    {
                                        selfBuildRow = i;
                                        excel.Workbook.Worksheets["Plots"].Cells[$"S{selfBuildRow + 2}"].Value = ownerName;
                                    }
                                }
                                if (selfBuildRow == -1)
                                    throw new Exception("selfBuildRow Row Not Found");
                            }
                        }
                        if (Statics.ProductNameList.Any(o => o.Equals("Commercial")))
                        {
                            for (int i = 0; i < excelTable.Rows.Count; i++)
                            {
                                if (excelTable.Rows[i][1].ToString() == "Commercial")
                                {
                                    commercialsRow = i;
                                    excel.Workbook.Worksheets["Plots"].Cells[$"S{commercialsRow + 2}"].Value = ownerName;
                                }
                            }
                            if (commercialsRow == -1)
                                throw new Exception("Commercial Row Not Found");
                        }
                        if (Statics.ProductNameList.Any(o => o.Equals("Commercial - High Value")))
                        {
                            for (int i = 0; i < excelTable.Rows.Count; i++)
                            {
                                if (excelTable.Rows[i][1].ToString() == "Commercial - High Value")
                                {
                                    commercialsHVSRow = i;
                                    excel.Workbook.Worksheets["Plots"].Cells[$"S{commercialsHVSRow + 2}"].Value = ownerName;
                                }
                            }
                            if (commercialsHVSRow == -1)
                                throw new Exception("Commercial - High Value Row Not Found");
                        }
                        if (Statics.ProductNameList.Any(o => o.Equals("Channel Islands Commercial - High Value")))
                        {
                            for (int i = 0; i < excelTable.Rows.Count; i++)
                            {
                                if (excelTable.Rows[i][1].ToString() == "Channel Islands Commercial - High Value")
                                {
                                    ciCommercialsHVSRow = i;
                                    excel.Workbook.Worksheets["Plots"].Cells[$"S{ciCommercialsHVSRow + 2}"].Value = ownerName;
                                }
                            }
                            if (ciCommercialsHVSRow == -1)
                                throw new Exception("Channel Island Commercial HVS Row Not Found");
                        }
                        if (Statics.ProductNameList.Any(o => o.Equals("Channel Islands Commercial")))
                        {
                            for (int i = 0; i < excelTable.Rows.Count; i++)
                            {
                                if (excelTable.Rows[i][1].ToString() == "Channel Islands Commercial")
                                {
                                    ciCommercialsRow = i;
                                    excel.Workbook.Worksheets["Plots"].Cells[$"S{ciCommercialsRow + 2}"].Value = ownerName;
                                }
                            }
                            if (ciCommercialsRow == -1)
                                throw new Exception("Channel Island Commercial Row Not Found");
                        }

                        var fileInfo = new FileInfo(ExtensionMethods.GetAbsolutePath($@"TestData\Conditions\plots.xlsx"));
                        string plotData = fileInfo.ToString();
                        excel.SaveAs(new FileInfo(plotData));
                        CloseSpinneronDiv();
                        CloseSpinneronPage();
                        Thread.Sleep(500);
                        WaitForElement(ImportButton);
                        ImportButton.Click();
                        CloseSpinneronDiv();
                        CloseSpinneronPage();
                        Thread.Sleep(500);
                        UploadPlotSchedule.SendKeys(plotData);
                        //SendKeys.SendWait(@"{Enter}");
                        log.Info("File has been uploaded");
                        //Thread.Sleep(500);                       
                        CloseSpinneronDiv();
                        CloseSpinneronPage();
                        WaitForElement(UploadOkButton);
                        UploadOkButton.Click();
                        //Thread.Sleep(500);
                        CloseSpinneronDiv();
                        CloseSpinneronPage();
                        Thread.Sleep(500);
                        WaitForElement(ConfirmButton);
                        CloseSpinneronDiv();
                        CloseSpinneronPage();
                        WaitForElementToClick(ConfirmButton);
                        CloseSpinneronDiv();
                        CloseSpinneronPage();
                        ConfirmButton.Click();
                        Thread.Sleep(500);
                        WaitForElement(QGUReferralConfirmDiv);
                        WaitForElement(QGUReferralNoButton);
                        QGUReferralNoButton.Click();
                        //Thread.Sleep(500);
                        CloseSpinneronDiv();
                        CloseSpinneronPage();
                        Thread.Sleep(500);
                    }
                }
            }
        }

        //Making Order Fee to clear the conditions to issue PLOTIC 

        public void PayFeeBalance()
        {
            //Thread.Sleep(1000);
            if (Dashboardpage.DecisionButton.Count <= 0)
            {
                Dashboardpage.SelectOrder();
            }
            //Thread.Sleep(1500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            WaitForElement(AccountTab);
            WaitForElementToClick(AccountTab);
            AccountTab.Click();
            //Thread.Sleep(1000);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            WaitForElement(FeeBalance);
            //Thread.Sleep(1000);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            var FeeBalanceValue = FeeBalance.Text.Replace("£", "");
            decimal actualFeeBal = Convert.ToDecimal(FeeBalanceValue);
            //Thread.Sleep(1000);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            WaitForElement(EscrowBalance);
            var EscrowBalanceValue = EscrowBalance.Text.Replace("£", "");
            decimal actualEscrowBal = Convert.ToDecimal(EscrowBalanceValue);
            //Thread.Sleep(1000);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(1000);
            if (actualFeeBal > 0 || actualEscrowBal > 0)
            {
                //Thread.Sleep(1000);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                WaitForElement(EnterPaymentButton);
                EnterPaymentButton.Click();
                //Thread.Sleep(1000);
                WaitForElement(MakePaymentDiv);
                Assert.IsTrue(MakePaymentDiv.Displayed);
                FinancesPage.MakeDirectPyament();
                //Thread.Sleep(1000);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                Thread.Sleep(500);
            }
        }


        //Upload security docs to builder and developer to clear the conditions on issue COI
        public void SecurityDocsUpload()
        {
            Thread.Sleep(500);
            CloseSpinneronPage();
            CloseSpinneronDiv();
            Thread.Sleep(500);
            if (Dashboardpage.DecisionButton.Count <= 0)
            {
                Dashboardpage.SelectOrder();
            }
            WaitForElement(ConditionsTab);
            WaitForElementToClick(ConditionsTab);
            ConditionsTab.Click();
            Thread.Sleep(500);
            CloseSpinneronPage();
            CloseSpinneronDiv();
            Thread.Sleep(500);
            var builderconditions = ConditionsRows.Count > 0;
            Thread.Sleep(500);
            CloseSpinneronPage();
            CloseSpinneronDiv();
            Thread.Sleep(500);
            if (builderconditions == true)
            {
                WaitForElements(ConditionsRows);
                builderSecurityDocCondition = ConditionsRows.Any(x => x.Text.Contains("Builder has Outstanding Security Documents"));
                builderRegistrationFeeCondition = ConditionsRows.Any(x => x.Text.Contains("Builder has outstanding Registration Fee"));
                selfBuildIndemnityDocCondition = ConditionsRows.Any(x => x.Text.Contains("The self build indemnity agreement for the builder is outstanding"));
                Thread.Sleep(1000);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                Thread.Sleep(1000);
                if (builderSecurityDocCondition == true || builderRegistrationFeeCondition == true || selfBuildIndemnityDocCondition == true)
                {
                    BuilderSecurityDocsandRegistrationFee();
                }
            }
            Thread.Sleep(500);
            CloseSpinneronPage();
            CloseSpinneronDiv();
            Thread.Sleep(500);
            if (ConditionsRows.Count <= 0)
            {
                Dashboardpage.SelectOrder();
            }
            WaitForElement(ConditionsTab);
            ConditionsTab.Click();
            Thread.Sleep(500);
            CloseSpinneronPage();
            CloseSpinneronDiv();
            Thread.Sleep(500);
            var devconditions = ConditionsRows.Count > 0;
            Thread.Sleep(500);
            CloseSpinneronPage();
            CloseSpinneronDiv();
            Thread.Sleep(500);
            if (devconditions == true)
            {
                developerSecurityDocCondition = ConditionsRows.Any(x => x.Text.Contains("Developer has Outstanding Security Documents"));
                developerRegistrationFeeCondition = ConditionsRows.Any(x => x.Text.Contains("Developer has outstanding Registration Fee"));
                Thread.Sleep(1000);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                Thread.Sleep(1000);
                if (developerSecurityDocCondition == true || developerRegistrationFeeCondition == true)
                {
                    DeveloperSecurityDocsAndRegistrationFee();
                }
            }
        }
        public void BuilderSecurityDocsandRegistrationFee()
        {
            AddLABCQuotePage.SelectBuilderRegistration();
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            if (Statics.OrderNumber.Contains("PL-PG"))
            {
                Thread.Sleep(500);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                WaitForElement(PGRegistrationTab);
                PGRegistrationTab.Click();
                Thread.Sleep(500);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                UploadDocument();
                //Thread.Sleep(1000);
                if (builderRegistrationFeeCondition == true)
                {
                    MakeRegistationFee();
                }
            }
            if (Statics.OrderNumber.Contains("PL-LABC"))
            {
                WaitForElement(LABCRegistrationTab);
                LABCRegistrationTab.Click();
                Thread.Sleep(500);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                UploadDocument();
                Thread.Sleep(500);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                if (builderRegistrationFeeCondition == true)
                {
                    MakeRegistationFee();
                }
            }
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
        }
        public void DeveloperSecurityDocsAndRegistrationFee()
        {
            AddLABCQuotePage.SelectDeveloperRegistration();
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            if (Statics.OrderNumber.Contains("PL-PG"))
            {
                WaitForElement(PGRegistrationTab);
                PGRegistrationTab.Click();
                Thread.Sleep(500);
                UploadDocument();
                Thread.Sleep(500);
                if (developerRegistrationFeeCondition == true)
                {
                    MakeRegistationFee();
                }
            }
            if (Statics.OrderNumber.Contains("PL-LABC"))
            {
                WaitForElement(LABCRegistrationTab);
                LABCRegistrationTab.Click();
                //Thread.Sleep(1000);
                UploadDocument();
                //Thread.Sleep(1000);
                if (developerRegistrationFeeCondition == true)
                {
                    MakeRegistationFee();
                }
            }
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(1000);
        }
        //Upload document method 
        public void UploadDocument()
        {
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            WaitForElement(RegDocumentTab);
            WaitForElementToClick(RegDocumentTab);
            RegDocumentTab.Click();
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            WaitForElements(ShowSiteSpecificDocs);
            if (ShowSiteSpecificDocs.Count > 0)
            {
                CloseSpinneronDiv();
                CloseSpinneronPage();
                Thread.Sleep(500);
                WaitForElement(ShowSiteSpecificDocs[0]);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                Thread.Sleep(500);
                WaitForLoadElementtobeclickable(ShowSiteSpecificDocs[0]);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                ShowSiteSpecificDocs[0].Click();
                Thread.Sleep(500);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                Thread.Sleep(500);
            }
            WaitForElements(SecurityDocsList);
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            if (SecurityDocsList.Count > 0)
            {
                int actualsecuritydocs = SecurityDocsList.Count;
                for (int i = 0; i < actualsecuritydocs; i++)
                {
                    Thread.Sleep(500);
                    CloseSpinneronRegPage();
                    CloseSpinneronDiv();
                    CloseSpinneronPage();
                    WaitForElement(SecurityDocsList[i]);
                    Thread.Sleep(500);
                    CloseSpinneronRegPage();
                    CloseSpinneronDiv();
                    CloseSpinneronPage();
                    WaitForElementToClick(SecurityDocsList[i]);
                    Thread.Sleep(500);
                    CloseSpinneronRegPage();
                    CloseSpinneronDiv();
                    CloseSpinneronPage();
                    SecurityDocsList[i].Click();
                    Thread.Sleep(500);
                    CloseSpinneronDiv();
                    CloseSpinneronPage();
                    WaitForElement(UploadButton);
                    UploadButton.Click();
                    WaitForElement(UploadDiv);
                    Assert.IsTrue(UploadDiv.Displayed);
                    Thread.Sleep(500);
                    CloseSpinneronDiv();
                    CloseSpinneronPage();
                    WaitForElement(RelatedFiles);
                    RelatedFiles.Click();
                    Thread.Sleep(500);
                    CloseSpinneronDiv();
                    CloseSpinneronPage();
                    WaitForElement(UploadNewFilesButton);
                    UploadNewFilesButton.Click();
                    //Upload Site location Plan 
                    Thread.Sleep(500);
                    CloseSpinneronDiv();
                    CloseSpinneronPage();
                    var fileInfo = new FileInfo(ExtensionMethods.GetAbsolutePath(@"TestData\QuotesData\CRISP install guide.docx"));
                    SendFileInput.SendKeys($"{fileInfo}");
                    Thread.Sleep(500);
                    CloseSpinneronDiv();
                    CloseSpinneronPage();
                    Thread.Sleep(500);
                    WaitForElement(Select);
                    WaitForElementToClick(Select);
                    Select.Click();
                    Thread.Sleep(500);
                    CloseSpinneronDiv();
                    CloseSpinneronPage();
                    Thread.Sleep(500);
                    WaitForElement(DateOnUploadDiv);
                    WaitForElementToClick(DateOnUploadDiv);
                    DateOnUploadDiv.Click();
                    Thread.Sleep(500);
                    CloseSpinneronDiv();
                    CloseSpinneronPage();
                    Thread.Sleep(500);
                    WaitForElement(DateReceivedInput);
                    WaitForElementToClick(DateReceivedInput);
                    DateReceivedInput.Click();
                    Thread.Sleep(500);
                    CloseSpinneronDiv();
                    CloseSpinneronPage();
                    WaitForElement(OkButton);
                    WaitForElementToClick(OkButton);
                    OkButton.Click();
                    CloseSpinneronDiv();
                    Thread.Sleep(500);
                    CloseSpinneronDiv();
                    CloseSpinneronPage();
                    Thread.Sleep(500);
                    WaitForElement(ReviewButton);
                    WaitForElementToClick(ReviewButton);
                    ReviewButton.Click();
                    Thread.Sleep(500);
                    CloseSpinneronDiv();
                    CloseSpinneronPage();
                    Thread.Sleep(500);
                    WaitForElement(ReviewDiv);
                    Assert.IsTrue(ReviewDiv.Displayed);
                    Thread.Sleep(500);
                    CloseSpinneronDiv();
                    CloseSpinneronPage();
                    Thread.Sleep(500);
                    WaitForElement(ReviewStatusDropdown);
                    ReviewStatusDropdown.Click();
                    Thread.Sleep(500);
                    CloseSpinneronDiv();
                    CloseSpinneronPage();
                    WaitForElement(ReviewStatusDropdownInput);
                    WaitForElementToClick(ReviewStatusDropdownInput);
                    ReviewStatusDropdownInput.Click();
                    Thread.Sleep(500);
                    CloseSpinneronDiv();
                    CloseSpinneronPage();
                    WaitForElement(OkButton);
                    WaitForElementToClick(OkButton);
                    OkButton.Click();
                    Thread.Sleep(500);
                    CloseSpinneronPage();
                    CloseSpinneronDiv();
                    Thread.Sleep(500);
                    WaitForElement(CloseButton);
                    WaitForElementToClick(CloseButton);
                    CloseButton.Click();
                    Thread.Sleep(500);
                    CloseSpinneronPage();
                    CloseSpinneronDiv();
                    CloseSpinneronRegPage();
                    Thread.Sleep(500);
                }
            }
        }


        //Pay Off registration fee to builder and developer to clear the conditions on issue COI
        public void RegistrationFee()
        {
            if (Dashboardpage.DecisionButton.Count <= 0)
            {
                Dashboardpage.SelectOrder();
            }
            WaitForElement(ConditionsTab);
            ConditionsTab.Click();
            //Thread.Sleep(1000);
            CloseSpinneronPage();
            CloseSpinneronDiv();
            //Thread.Sleep(1000);
            WaitForElements(ConditionsRows);
            //Thread.Sleep(1000);
            CloseSpinneronPage();
            CloseSpinneronDiv();
            //Thread.Sleep(1000);
            if (ConditionsRows.Count > 0)
            {
                var registrationFeeCondition = ConditionsRows.Any(x => x.Text.Contains("Builder has outstanding Registration Fee") || x.Text.Contains("Developer has outstanding Registration Fee"));
                if (registrationFeeCondition == true)
                {
                    string builderRole = "";
                    string developerRole = "";
                    WaitForElement(RolesTab);
                    RolesTab.Click();
                    WaitForElements(RolesRows);
                    if (BuilderRole.Count > 0)
                    {
                        builderRole = BuilderRole[0].Text;
                    }
                    if (DeveloperRole.Count > 0)
                    {
                        developerRole = DeveloperRole[0].Text;
                    }
                    //Thread.Sleep(1500);
                    CloseSpinneronDiv();
                    CloseSpinneronPage();
                    //Thread.Sleep(1500);
                    AddLABCQuotePage.SelectBuilderRegistration();
                    CloseSpinneronDiv();
                    CloseSpinneronPage();
                    if (Statics.OrderNumber.Contains("PL-PG"))
                    {
                        WaitForElement(PGRegistrationTab);
                        PGRegistrationTab.Click();
                        //Thread.Sleep(1000);
                        MakeRegistationFee();
                        //Thread.Sleep(1000);
                    }
                    if (Statics.OrderNumber.Contains("PL-LABC"))
                    {
                        WaitForElement(LABCRegistrationTab);
                        LABCRegistrationTab.Click();
                        //Thread.Sleep(1000);
                        MakeRegistationFee();
                        //Thread.Sleep(1000);
                    }
                }
            }
        }

        //Make Registration fee Method
        public void MakeRegistationFee()
        {
            //Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(500);
            WaitForElement(RoleAccountTab);
            WaitForElementToClick(RoleAccountTab);
            RoleAccountTab.Click();
            //Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(500);
            WaitForElement(RegistrationRef);
            var regRefNumber = RegistrationRef.Text;
            //Thread.Sleep(1000);
            WaitForElement(RegFeePaid);
            var FeePaidValue = RegFeePaid.Text.Replace("£", "");
            decimal actualRegFeePaid = Convert.ToDecimal(FeePaidValue);
            WaitForElement(RegFeeBalance);
            var RegBalanceValue = RegFeeBalance.Text.Replace("£", "");
            decimal actualRegFeeBal = Convert.ToDecimal(RegBalanceValue);
            if (actualRegFeeBal > 0)
            {
                WaitForElement(EnterPaymentButton);
                EnterPaymentButton.Click();
                //Thread.Sleep(1000);
                WaitForElement(MakePaymentDiv);
                Assert.IsTrue(MakePaymentDiv.Displayed);
                FinancesPage.MakeDirectPyament();
                CloseSpinneronPage();
            }
        }

        // Issue PlotIC method 
        public void IssuePlotIC()
        {
            if (Dashboardpage.DecisionButton.Count <= 0)
            {
                Dashboardpage.SelectOrder();
            }
            //Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(500);
            FinalNoticeCondition();
            //Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(500);
            try
            {
                WaitForElement(Dashboardpage.ActionsButton);
                //Thread.Sleep(500);
                Dashboardpage.ActionsButton.Click();
                //Thread.Sleep(500);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                //Thread.Sleep(500);
                WaitForElement(Dashboardpage.IssuePlotIC);
                Dashboardpage.IssuePlotIC.Click();
                Thread.Sleep(500);
                CloseSpinneronDiv();
                CloseSpinneronPage();
            }
            catch (Exception ex)
            {
                Console.WriteLine("Please check Warrenty Product and  PlotIc permissions , User may not have permissions to issue PlotIC, Please Apply Permissions Manually: {0}", ex);
            }
            WaitForElement(ICDiv);
            Assert.IsTrue(ICDiv.Displayed);
            WaitForElement(SelectPlot);
            SelectPlot.Click();
            WaitForElements(SelectPlotInput);
            if (SelectPlotInput.Count > 0)
            {
                foreach (var eachplot in SelectPlotInput)
                {
                    eachplot.Click();

                }
                //Thread.Sleep(500);
                CloseSpinneronDiv();
                WaitForElement(SelectButton);
                SelectButton.Click();
                //Thread.Sleep(500);
                CloseSpinneronDiv();
            }
            WaitForElement(ICNextButton);
            ICNextButton.Click();
            //File review Page 
            WaitForElement(FileReviewPage);
            Assert.IsTrue(FileReviewPage.Displayed);
            //Thread.Sleep(500);
            WaitForElement(Blockers);
            Assert.IsTrue(Blockers.Displayed);
            WaitForElement(NoBlockers);
            Assert.IsTrue(NoBlockers.Displayed);
            WaitForElement(Conditions);
            Assert.IsTrue(Conditions.Displayed);
            WaitForElement(Fees);
            Assert.IsTrue(Fees.Displayed);
            //Thread.Sleep(500);
            WaitForElement(ICNextButton);
            ICNextButton.Click();
            //Confirm Page 
            CloseSpinneronPage();
            CloseSpinneronDiv();
            //Thread.Sleep(1000);
            ConfirmDetailsPage();
            //Plot Intial Certificate Page
            WaitForElement(ICNextButton);
            ICNextButton.Click();
            //Thread.Sleep(1500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            if (LABCGenerateQuote.Count > 0)
            {
                //Thread.Sleep(500);
                CloseSpinneronDiv();
                //Thread.Sleep(500);
                WaitForElement(LABCGenerateQuote[0]);
                //Thread.Sleep(500);
                Assert.IsTrue(LABCGenerateQuote[0].Displayed);
                Console.WriteLine("LABC Generate  Quote details verified");
            }
            if (PGGenerateQuote.Count > 0)
            {
                //Thread.Sleep(500);
                CloseSpinneronDiv();
                //Thread.Sleep(500);
                WaitForElement(PGGenerateQuote[0]);
                //Thread.Sleep(500);
                Assert.IsTrue(PGGenerateQuote[0].Displayed);
                Console.WriteLine("LABC Generate  Quote details verified");
            }

            WaitForElement(SendPlotICButton);
            SendPlotICButton.Click();
            //Thread.Sleep(1500);
        }
        //Issue DevIC method
        public void IssueDevIC()
        {
            if (Dashboardpage.DecisionButton.Count <= 0)
            {
                Dashboardpage.CrispLoginMethod();
            }
            FinalNoticeCondition();
            //Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(500);
            WaitForElement(Dashboardpage.ActionsButton);
            WaitForElementToClick(Dashboardpage.ActionsButton);
            Dashboardpage.ActionsButton.Click();
            //Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(500);
            try
            {
                WaitForElement(Dashboardpage.IssueDevIC);
                WaitForElementToClick(Dashboardpage.IssueDevIC);
                Dashboardpage.IssueDevIC.Click();
            }
            catch (Exception)
            {
                throw new Exception(string.Format("Dev IC Option is not available on Actions Banner List."));
            }
            //Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(500);
            WaitForElement(ICDiv);
            Assert.IsTrue(ICDiv.Displayed);
            WaitForElement(FileReviewPage);
            Assert.IsTrue(FileReviewPage.Displayed);
            //Thread.Sleep(500);
            WaitForElement(Blockers);
            Assert.IsTrue(Blockers.Displayed);
            WaitForElement(NoBlockers);
            Assert.IsTrue(NoBlockers.Displayed);
            WaitForElement(Fees);
            Assert.IsTrue(Fees.Displayed);
            //Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(500);
            //Thread.Sleep(500);
            WaitForElement(Conditions);
            Assert.IsTrue(Conditions.Displayed);
            WaitForElement(ICNextButton);
            ICNextButton.Click();
            //Confirm Page           
            //Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(500);
            ConfirmDetailsPage();
            //Thread.Sleep(500);
            CloseSpinneronDiv();
            //Thread.Sleep(500);
            WaitForElement(ConfirmationonDevIC);
            Assert.IsTrue(ConfirmationonDevIC.Displayed);
            WaitForElement(ICNextButton);
            ICNextButton.Click();

            //Dev IC Page 
            //Thread.Sleep(1500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            if (LABCGenerateQuote.Count > 0)
            {
                //Thread.Sleep(500);
                CloseSpinneronDiv();
                //Thread.Sleep(500);
                WaitForElement(LABCGenerateQuote[0]);
                //Thread.Sleep(500);
                Assert.IsTrue(LABCGenerateQuote[0].Displayed);
                Console.WriteLine("LABC Generate  Quote details verified");
            }
            if (PGGenerateQuote.Count > 0)
            {
                //Thread.Sleep(500);
                CloseSpinneronDiv();
                //Thread.Sleep(500);
                WaitForElement(PGGenerateQuote[0]);
                //Thread.Sleep(500);
                Assert.IsTrue(PGGenerateQuote[0].Displayed);
                Console.WriteLine("LABC Generate  Quote details verified");
            }

            WaitForElement(SendDevICButton);
            SendDevICButton.Click();
            //Thread.Sleep(1500);
        }
        //Issue COI method 
        public void IssueCOI()
        {
            ConditionsPage.CloseConditions();
            FinalNoticeCondition();
            //Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            ConditionsPage.CloseConditions();
            //Thread.Sleep(500);
            COI();
        }

        public void COI()
        {
            var products = Statics.ProductNameList.Count;
            WaitForElement(Dashboardpage.ActionsButton);
            //Thread.Sleep(1000);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(1000);
            Dashboardpage.ActionsButton.Click();
            //Thread.Sleep(1000);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(1000);
            WaitForElement(Dashboardpage.IssueCOI);
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(1000);
            WaitForElementToClick(Dashboardpage.IssueCOI);
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(500);
            Dashboardpage.IssueCOI.Click();
            //Thread.Sleep(1000);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            WaitForElement(ICDiv);
            Assert.IsTrue(ICDiv.Displayed);
            WaitForElement(SelectPlot);
            SelectPlot.Click();
            WaitForElements(SelectPlotInput);
            if (SelectPlotInput.Count > 0)
            {
                foreach (var eachplot in SelectPlotInput)
                {
                    eachplot.Click();
                    //Thread.Sleep(500);
                    CloseSpinneronDiv();
                }
                WaitForElement(SelectButton);
                SelectButton.Click();
            }
            WaitForElement(ICNextButton);
            ICNextButton.Click();
            //File review Page 
            WaitForElement(FileReviewPage);
            Assert.IsTrue(FileReviewPage.Displayed);
            //Thread.Sleep(500);
            WaitForElement(Blockers);
            Assert.IsTrue(Blockers.Displayed);
            WaitForElement(NoBlockers);
            Assert.IsTrue(NoBlockers.Displayed);
            WaitForElement(Conditions);
            Assert.IsTrue(Conditions.Displayed);
            WaitForElement(Fees);
            Assert.IsTrue(Fees.Displayed);
            //Thread.Sleep(500);
            WaitForElement(Defects);
            Assert.IsTrue(Defects.Displayed);
            //Thread.Sleep(500);
            WaitForElement(ICNextButton);
            ICNextButton.Click();
            //Confirm Page
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(500);
            ConfirmDetailsPage();
            //Plot Intial Certificate Page
            WaitForElement(ICNextButton);
            ICNextButton.Click();
            //Correspondence Docs
            //Thread.Sleep(1500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            if (LABCGenerateQuote.Count > 0)
            {
                //Thread.Sleep(500);
                CloseSpinneronDiv();
                //Thread.Sleep(500);
                WaitForElement(LABCGenerateQuote[0]);
                //Thread.Sleep(500);
                Assert.IsTrue(LABCGenerateQuote[0].Displayed);
                Console.WriteLine("LABC Generate  Quote details verified");
            }
            if (PGGenerateQuote.Count > 0)
            {
                //Thread.Sleep(500);
                CloseSpinneronDiv();
                //Thread.Sleep(500);
                WaitForElement(PGGenerateQuote[0]);
                //Thread.Sleep(500);
                Assert.IsTrue(PGGenerateQuote[0].Displayed);
                Console.WriteLine("LABC Generate  Quote details verified");
            }

            WaitForElement(SendCOIButton);
            SendCOIButton.Click();
            //Thread.Sleep(1500);           

        }

        public void COIForExtranet()
        {
            var products = Statics.ProductNameList.Count;
            WaitForElement(Dashboardpage.ActionsButton);
            //Thread.Sleep(1000);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(1000);
            Dashboardpage.ActionsButton.Click();
            //Thread.Sleep(1000);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(1000);
            WaitForElement(Dashboardpage.IssueCOI);
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(1000);
            WaitForElementToClick(Dashboardpage.IssueCOI);
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(500);
            Dashboardpage.IssueCOI.Click();
            //Thread.Sleep(1000);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            WaitForElement(ICDiv);
            Assert.IsTrue(ICDiv.Displayed);
            WaitForElement(SelectPlot);
            SelectPlot.Click();
            WaitForElements(SelectPlotInput);
            if (SelectPlotInput.Count > 0)
            {
                WaitForLoadElement(SelectPlotInput[0]);
                SelectPlotInput[0].Click();
                Thread.Sleep(500);
                CloseSpinneronDiv();
                WaitForElement(SelectButton);
                SelectButton.Click();
            }
            WaitForElement(ICNextButton);
            ICNextButton.Click();
            //File review Page 
            WaitForElement(FileReviewPage);
            Assert.IsTrue(FileReviewPage.Displayed);
            //Thread.Sleep(500);
            WaitForElement(Blockers);
            Assert.IsTrue(Blockers.Displayed);
            WaitForElement(NoBlockers);
            Assert.IsTrue(NoBlockers.Displayed);
            WaitForElement(Conditions);
            Assert.IsTrue(Conditions.Displayed);
            WaitForElement(Fees);
            Assert.IsTrue(Fees.Displayed);
            //Thread.Sleep(500);
            WaitForElement(Defects);
            Assert.IsTrue(Defects.Displayed);
            //Thread.Sleep(500);
            WaitForElement(ICNextButton);
            ICNextButton.Click();
            //Confirm Page
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(500);
            ConfirmDetailsPage();
            //Plot Intial Certificate Page
            WaitForElement(ICNextButton);
            ICNextButton.Click();
            //Correspondence Docs
            //Thread.Sleep(1500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            if (LABCGenerateQuote.Count > 0)
            {
                //Thread.Sleep(500);
                CloseSpinneronDiv();
                //Thread.Sleep(500);
                WaitForElement(LABCGenerateQuote[0]);
                //Thread.Sleep(500);
                Assert.IsTrue(LABCGenerateQuote[0].Displayed);
                Console.WriteLine("LABC Generate  Quote details verified");
            }
            if (PGGenerateQuote.Count > 0)
            {
                //Thread.Sleep(500);
                CloseSpinneronDiv();
                //Thread.Sleep(500);
                WaitForElement(PGGenerateQuote[0]);
                //Thread.Sleep(500);
                Assert.IsTrue(PGGenerateQuote[0].Displayed);
                Console.WriteLine("LABC Generate  Quote details verified");
            }

            WaitForElement(SendCOIButton);
            SendCOIButton.Click();
            //Thread.Sleep(1500);           

        }
        public System.Data.DataTable ReadExcelPlotData(string plotData)
        {
            //open file and returns as Stream
            var result = new DataSet();
            using (var stream = File.Open(plotData, FileMode.Open, FileAccess.Read))
            using (var excelReader = ExcelReaderFactory.CreateOpenXmlReader(stream)) //.xlsx
            {
                //Createopenxmlreader via ExcelReaderFactory

                //Set the First Row as Column Name

                //Return as DataSet
                result = excelReader.AsDataSet(new ExcelDataSetConfiguration()
                {
                    ConfigureDataTable = (_) => new ExcelDataTableConfiguration()
                    {
                        UseHeaderRow = true
                    }
                });
            }
            //Get all the Tables
            DataTableCollection table = result.Tables;
            //Store it in DataTable
            System.Data.DataTable resultTable = table["Plots"];

            //Get The Warrenty Product List 

            var Productlist = resultTable.DefaultView
     .ToTable(true, "Warranty Product")
     .Rows
     .Cast<DataRow>()
     .Select(row => row["Warranty Product"])
     .ToList();
            Statics.ProductNameList = Productlist.Select(i => i.ToString()).ToList();
            return resultTable;
        }

        public void ConfirmDetailsPage()
        {
            //Thread.Sleep(1000);
            if (ConfirmDetails.Count > 0)
            {
                WaitForElements(ConfirmDetails);
                Assert.IsTrue(ConfirmDetails[0].Displayed);
            }
            if (OverrideConditionsRadioButton.Count > 0)
            {
                WaitForElement(OverrideConditionsRadioButton[0]);
                OverrideConditionsRadioButton[0].Click();
                WaitForElement(CommentsLabel);
                CommentsLabel.Click();
                WaitForElement(CommentsInput);
                CommentsInput.SendKeys("ConditionsOverrideConfirmation");
                //Thread.Sleep(500);
                CloseSpinneronDiv();
            }
        }

        public void Import()
        {
            for (int i = 0; i <= 1; i++)
            {
                WaitForElement(FinancesPage.TransactionsList[i]);
                FinancesPage.TransactionsList[i].Click();
                CloseSpinneronDiv();
                //Thread.Sleep(1500);
                WaitForElements(FinancesPage.Editbutton);
                FinancesPage.Editbutton[i].Click();
                WaitForElements(FinancesPage.AccountsList);
                //Thread.Sleep(1000);
                CloseSpinneronDiv();
                if (FinancesPage.AccountsList.Count > 0)
                {
                    WaitForElement(RefLabel);
                    RefLabel.Click();
                    WaitForElement(RefInput);
                    RefInput.Clear();
                    RefInput.SendKeys(Statics.OrderNumber);
                    //Thread.Sleep(1000);
                    CloseSpinneronDiv();
                    WaitForElement(FinancesPage.AccountsList[0]);
                    FinancesPage.AccountsList[0].Click();
                    WaitForElement(FinancesPage.MatchedAccount);
                    Assert.IsTrue(FinancesPage.MatchedAccount.Displayed);
                    WaitForElement(FinancesPage.OkButton);
                    FinancesPage.OkButton.Click();
                    //Thread.Sleep(1000);
                }
            }
            // BankStatementimportIBAAccount();
            // BankStatementimportEscrowAccount();
            //Thread.Sleep(1000);
            if (FinancesPage.checkBoxes.Count > 0)
            {
                WaitForElements(FinancesPage.checkBoxes);
                for (int i = 0; i < FinancesPage.checkBoxes.Count; i++)
                {
                    FinancesPage.checkBoxes[i].Click();

                }
                //Thread.Sleep(1000);
                CloseSpinneronDiv();
            }
        }
    }
}
