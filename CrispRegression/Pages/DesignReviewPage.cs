﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace CrispAutomation.Pages
{
    public class DesignReviewPage : Support.Pages
    {
        public IWebDriver wdriver;
        public DesignReviewPage(ISearchContext driver)
        {
            PageFactory.InitElements(driver, this);
        }
     
      //Desin Review Page Elements 
        
        [FindsBy(How = How.XPath, Using = "//div//a[@class='info-box bg-indigo hover-expand-effect']//div[text()='Design review']")]
        public IWebElement DesignReviewOption;

        [FindsBy(How = How.XPath, Using = "//div//a[@class='info-box bg-indigo hover-expand-effect']//div[@class='content']")]
        public IWebElement DRStatus;

        [FindsBy(How = How.XPath, Using = "//div//a[text()='Edit']")]
        public IWebElement EditButton;

        [FindsBy(How = How.XPath, Using = "//input[@id='Review_ExpectedStartDate']/following-sibling::input[@type='text']")]
        public IWebElement ExpectedStartDate;

        [FindsBy(How = How.XPath, Using = "//input[@id='Review_ExpectedEndDate']/following-sibling::input[@type='text']")]
        public IWebElement ExpectedEndDate;

        [FindsBy(How = How.XPath, Using = "//button[@data-id='Review_BuildingType'][@class='btn dropdown-toggle btn-default']")]
        public IWebElement BuildingTypeDropdown;

        [FindsBy(How = How.XPath, Using = "//div[contains(@class,'open')]//div[@class='dropdown-menu open']//ul[@class='dropdown-menu inner']//li")]
        public IList<IWebElement> BuildingTypeDropdownInput;

        [FindsBy(How = How.XPath, Using = "//input[@id='Review_InternalComments']")]
        public IWebElement InternalComment;        

        [FindsBy(How = How.XPath, Using = "//div[@id='savebar']//button[contains(@class,'btn btn-success btn-lg pull-right')][text()='Next']")]
        public IWebElement NextButton;

        [FindsBy(How = How.XPath, Using = "//div[@class='form-control']//label[contains(@for,'AcceptableRisk_true')]")]
        public IWebElement AcceptableRiskRadioButton;

        [FindsBy(How = How.XPath, Using = "//div[@id='page2']//div[@class='form-contents']//button[@type='submit'][contains(text(),'Save')]")]
        public IWebElement AssessmentSaveButton;

        [FindsBy(How = How.XPath, Using = "//div[@class='form-contents']//button[@type='submit'][contains(text(),'Submit')][contains(@class,'btn btn-success btn-lg pull-right')]")]
        public IWebElement SubmitButton;

        [FindsBy(How = How.XPath, Using = "//section[@class='content']//form[@method='post']//button[@type='submit'][contains(text(),'Send')][contains(@class,'btn btn-success btn-lg pull-right')]")]
        public IWebElement SendButton;

        [FindsBy(How = How.XPath, Using = "//h2[text()='Design Review Assessment']")]
        public IWebElement DesignReviewMainPage;

        //open DR 
        public void OpenDR()
        {
            WaitForElementonSurveyor(DesignReviewOption);            
            CloseSpinnerOnSurveyorPage();            
            DesignReviewOption.Click();
            Thread.Sleep(500);
            CloseSpinnerOnSurveyorPage();
            Thread.Sleep(500);
            Assert.IsTrue(EditButton.Displayed, "No permissions applied to the user to submit Design Review Assessment");
            WaitForElementonSurveyor(EditButton);
            EditButton.Click();            
            CloseSpinnerOnSurveyorPage();            
        }
        // Submit DR 

        public void SubmitDR()
        {

            Thread.Sleep(500);
            CloseSpinnerOnSurveyorDiv();
            CloseSpinnerOnSurveyorPage();
            WaitForElementonSurveyor(AcceptableRiskRadioButton);
            AcceptableRiskRadioButton.Click();            
            CloseSpinnerOnSurveyorPage();            
            WaitForElementonSurveyor(SubmitButton);
            SubmitButton.Click();
            Thread.Sleep(500);
            CloseSpinnerOnSurveyorDiv();
            CloseSpinnerOnSurveyorPage();
            //Thread.Sleep(500);
            WaitForElementonSurveyor(SendButton);
            SendButton.Click();            
            CloseSpinnerOnSurveyorPage();            
            WaitForElementonSurveyor(DesignReviewMainPage);
            Assert.IsTrue(DesignReviewMainPage.Displayed);
        }
        // Save DR 

        public void SaveDR()
        {
            Thread.Sleep(1000);
            CloseSpinnerOnSurveyorPage();            
            WaitForElementonSurveyor(AcceptableRiskRadioButton);
            AcceptableRiskRadioButton.Click();            
            CloseSpinnerOnSurveyorPage();            
            WaitForElementonSurveyor(SubmitButton);
            var SubmitButtonStatus = SubmitButton.GetAttribute("disabled");
            if (SubmitButtonStatus != null || SubmitButtonStatus == "true")
            {
                WaitForElementonSurveyor(AssessmentSaveButton);
                AssessmentSaveButton.Click();
                CloseSpinnerOnSurveyorDiv();
                CloseSpinnerOnSurveyorPage();
                WaitForElementonSurveyor(DesignReviewMainPage);
                Assert.IsTrue(DesignReviewMainPage.Displayed);
            }
            else
            {
                WaitForElementonSurveyor(SubmitButton);
                SubmitButton.Click();
                CloseSpinnerOnSurveyorDiv();
                CloseSpinnerOnSurveyorPage();
                WaitForElementonSurveyor(SendButton);
                SendButton.Click();
                CloseSpinnerOnSurveyorDiv();
                CloseSpinnerOnSurveyorPage();
                WaitForElementonSurveyor(DesignReviewMainPage);
                Assert.IsTrue(DesignReviewMainPage.Displayed);
            }
        }
    
        public void DesignReviewMain()
        {
            SurveyorLoginPage.SelectSite();
            Thread.Sleep(500);
            CloseSpinnerOnSurveyorPage();
            CloseSpinnerOnSurveyorDiv();
            Thread.Sleep(500);
            WaitForElementonSurveyor(DesignReviewOption);
            WaitForElementonSurveyor(DRStatus);
            var drStatus = DRStatus.Text.ToString();
            Thread.Sleep(500);
            CloseSpinnerOnSurveyorPage();
            CloseSpinnerOnSurveyorDiv();
            Thread.Sleep(500);
            if (drStatus.Contains("Outstanding"))
            {
                // SurveyorAssessments.VerifySiteStatusBeforeAssessment();
                OpenDR();
                DesignReviewAssessment();
                SurveyorDocs.CloseSurveyorDocuments();
                SubmitDR();
            }
        
        }

        public void DesignReviewAssessment()
        {
            CloseSpinnerOnSurveyorPage();            
            WaitForElementonSurveyor(ExpectedStartDate);
            //ExpectedStartDate.Click();
            //ExpectedStartDate.Clear();
            //ExpectedStartDate.SendKeys("06/07/2018");            
            //CloseSpinnerOnSurveyorPage();            
            //WaitForElementonSurveyor(ExpectedEndDate);
            //ExpectedEndDate.Click();
            //ExpectedEndDate.Clear();
            //ExpectedEndDate.SendKeys("07/07/2018");                   
            CloseSpinnerOnSurveyorPage();            
            WaitForElementonSurveyor(BuildingTypeDropdown);
            ScrollIntoView(BuildingTypeDropdown);
            Thread.Sleep(500);
            CloseSpinnerOnSurveyorPage();
            Thread.Sleep(500);
            BuildingTypeDropdown.Click();
            Thread.Sleep(500);
            CloseSpinnerOnSurveyorPage();
            Thread.Sleep(500);
            WaitForLoadElements(BuildingTypeDropdownInput);
            if (BuildingTypeDropdownInput.Count > 0)
            {
                int BuildingTypeInputCount = BuildingTypeDropdownInput.Count;
                for (int i = 0; i < BuildingTypeInputCount; i++)
                {
                    WaitForLoadElements(BuildingTypeDropdownInput);
                    BuildingTypeDropdownInput[i].Click();
                }
                BuildingTypeDropdown.SendKeys(Keys.Tab);
                Thread.Sleep(500);
                CloseSpinnerOnSurveyorPage();
                Thread.Sleep(500);
                WaitForElementonSurveyor(InternalComment);     
                WaitForLoadElementtobeclickable(InternalComment);                
                InternalComment.Click();
                InternalComment.Clear();
                InternalComment.SendKeys("Submitting Design Review");
            }           
            WaitForElementonSurveyor(NextButton);
            NextButton.Click();
            Thread.Sleep(500);
            CloseSpinnerOnSurveyorPage();            
        }
        public void SubmitDesignReviewAssessmentMethod()
        {          
            SurveyorLoginPage.SelectSite();
            Thread.Sleep(500);
            CloseSpinnerOnSurveyorPage();
            Thread.Sleep(500);  
            OpenDR();
            DesignReviewAssessment();
            SurveyorDocs.CloseSurveyorDocuments();
            SubmitDR();
        }
    }
}
