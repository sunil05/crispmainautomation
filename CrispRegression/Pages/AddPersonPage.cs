﻿using System.Collections.Generic;
using System.Threading;
using CrispAutomation.Support;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.PageObjects;

namespace CrispAutomation.Pages
{
    public class AddPersonPage : Support.Pages
    {
        public IWebDriver wdriver;
        public AddPersonPage(ISearchContext driver)
        {
            PageFactory.InitElements(driver, this);
            wdriver = (RemoteWebDriver)driver;
        }

        [FindsBy(How = How.XPath, Using = "//LABEL[@class='au-target'][text()='Salutation']")]
        public IWebElement Salutation;

        [FindsBy(How = How.XPath, Using = "//crisp-input-autocomplete[@label='Salutation']/div/input")]
        public IWebElement SalutationFieldEdit;

        [FindsBy(How = How.XPath, Using = "//LABEL[@data-error.bind='error'][text()='Phone']")]
        public IWebElement PhoneNumber;

        [FindsBy(How = How.XPath, Using = "//crisp-input-text[@label='Phone']//div[@class='input-field']//input")]
        public IWebElement PhoneNumberEdit;

        [FindsBy(How = How.XPath, Using = "//LABEL[@data-error.bind='error'][text()='Mobile']")]
        public IWebElement MobileNumber;

        [FindsBy(How = How.XPath, Using = "//crisp-input-text[@label='Mobile']//div[@class='input-field']//input")]
        public IWebElement MobileNumberEdit;

        [FindsBy(How = How.XPath, Using = "//div[@class='card-title']//span[text()='Sales & Marketing']")]
        public IWebElement SalesAndMarketingTitle;

        [FindsBy(How = How.XPath, Using = "//crisp-card-content[@class='au-target']//crisp-input-bool//label")]
        public IList<IWebElement> CheckBoxesList;

        [FindsBy(How = How.XPath,
            Using =
                "//crisp-button[@class='au-target'][@click.delegate='wizard.goForward()']/span/button[text()='Next']")]
        public IWebElement NextButton;

        [FindsBy(How = How.CssSelector,
            Using = "body > ai-dialog-container > div > div > crisp-dialog > div > div.modal-content > crisp-wizard > crisp-wizard-step.au-target.active > addresses > div > div > div > crisp-list > crisp-action-button > div > a > i")]
        public IWebElement AddressButton;

        [FindsBy(How = How.XPath,
            Using = "//crisp-input-text[@label='Enter postcode to search']/div[@class='input-field']/input")]
        public IWebElement PostcodeInput;

        [FindsBy(How = How.XPath,
            Using = "//crisp-card-content//div[@class='card-content']//div//crisp-picker[@label='Address']")]
        public IWebElement AddressDropdown;

        [FindsBy(How = How.XPath, Using = "//ul[@class='dropdown-content select-dropdown active']/li[1]")]
        public IWebElement AddressDropdownInput;

        [FindsBy(How = How.XPath, Using = "//crisp-button/span/button[text()='Enter Address Manually']")]
        public IWebElement EnterManuallyButton;

        [FindsBy(How = How.XPath, Using = "//crisp-input-text[@label='Address Line 1']//LABEL[text()='Address Line 1']")]
        public IWebElement AddressLine1;

        [FindsBy(How = How.XPath, Using = "//crisp-input-text[@label='Address Line 1']//div//input")]
        public IWebElement AddressLine1EnterValue;

        [FindsBy(How = How.XPath, Using = "//BUTTON[@ref='button'][text()='Confirm Selection']")]
        public IWebElement ConfirmSelection;

        [FindsBy(How = How.XPath, Using = "//crisp-card-header[@class='au-target']/div/span[text()='Address details']")]
        public IWebElement AddressDetails;

        [FindsBy(How = How.XPath, Using = "//LABEL[@data-error.bind='error'][text()='Facebook']")]
        public IWebElement FaceBookId;

        [FindsBy(How = How.XPath, Using = "//crisp-input-text[@label='Facebook']//div//input")]
        public IWebElement FacebookElementEnter;

        [FindsBy(How = How.CssSelector, Using = "body > ai-dialog-container > div > div > crisp-dialog > div > div.modal-content > crisp-wizard > crisp-wizard-step.au-target.active > social > div > div > div.crisp-row.height-40 > div > crisp-list > crisp-action-button > div > a > i")]
        public IWebElement AddWebsiteButton;

        [FindsBy(How = How.XPath, Using = "//crisp-input-text[@label='Website Address']/div/LABEL[@data-error.bind='error'][text()='Website Address']")]
        public IWebElement WebsiteAddress;

        [FindsBy(How = How.XPath, Using = "//crisp-input-text[@label='Website Address']/div/input[contains(@id,'crisp-input-text')][@ref='input']")]
        public IWebElement WebSiteInputField;

        [FindsBy(How = How.XPath, Using = "//crisp-footer-button[@class='au-target']//button[text()='Add']")]
        public IWebElement AddButton;

        [FindsBy(How = How.XPath, Using = "//crisp-list[@list-title='Websites']//ul//li[@class='au-target collection-item']//a")]
        public IList<IWebElement> WebSiteList;

        [FindsBy(How = How.XPath, Using = "//BUTTON[@ref='button'][text()='Save']")]
        public IWebElement SaveButton;

        [FindsBy(How = How.CssSelector,
            Using = "body > div.custom-element > router-view > crisp-header > nav > div > crisp-header-avatar > img")]
        public IWebElement Imageperson;

        [FindsBy(How = How.XPath, Using = "//crisp-picker[@selected.bind='phoneCountryCode & validate'][@class='au-target']//div//input[@class='select-dropdown']")]
        public IWebElement PhoneCountry;

        [FindsBy(How = How.XPath, Using = "//ul[@class='dropdown-content select-dropdown active']//li//span[text()='United Kingdom']")]
        public IWebElement PhoneCountryDropdown;

        [FindsBy(How = How.XPath, Using = "//crisp-picker[@selected.bind='mobileCountryCode & validate'][@class='au-target']//div//input[@class='select-dropdown']")]
        public IWebElement MobileCountry;

        [FindsBy(How = How.XPath, Using = "//ul[@class='dropdown-content select-dropdown active']//li//span[text()='United Kingdom']")]
        public IWebElement MobileCountryDropdown;

        [FindsBy(How = How.XPath, Using = "//crisp-button/span/button[text()='Enter Address Using Postcode']")]
        public IWebElement EnterAddressUsingPostcodeButton;

        [FindsBy(How = How.XPath, Using = "//crisp-button[1]/span/button[text()='Set Address'][@class='au-target waves-effect waves-light btn']")]
        public IWebElement SetAddressButton;

        [FindsBy(How = How.XPath, Using = "//ai-dialog-container[2]//crisp-dialog//div[contains(@class,'au-target modal dialog')]")]
        public IWebElement AddModalDialog;

        [FindsBy(How = How.XPath, Using = "//ai-dialog-container[2]//crisp-dialog//div[contains(@class,'au-target modal dialog')]")]
        public IWebElement AddModalDialog2;


        
        //Fill Details 

        public void FillDetails()
        {
            WaitForElement(Salutation);
            Salutation.Click();
            SalutationFieldEdit.SendKeysAndtab("Mr");
            wdriver.MoveToActiveAndSendKeys("Sunil", true);
            wdriver.MoveToActiveAndSendKeys("Sunkishala", true);
            wdriver.MoveToActiveAndSendKeys("crisp.dev.c@ext.crisp.co.uk", false);            
            WaitForElement(PhoneCountry);            
            PhoneCountry.Click();            
            WaitForElement(PhoneCountryDropdown);
            PhoneCountryDropdown.Click();            
            WaitForElement(PhoneNumber);            
            PhoneNumber.Click();
            WaitForElement(PhoneNumberEdit);
            PhoneNumberEdit.SendKeys("01483493725");
            WaitForElement(MobileCountry);
            MobileCountry.Click();            
            WaitForElement(MobileCountryDropdown);
            MobileCountryDropdown.Click();            
            WaitForElement(MobileNumber);
            MobileNumber.Click();
            WaitForElement(MobileNumberEdit);
            MobileNumberEdit.SendKeys("07834233955");
            WaitForElement(SalesAndMarketingTitle);
            Assert.IsTrue(SalesAndMarketingTitle.Displayed);
            WaitForElements(CheckBoxesList);
            for(int i=2; i < CheckBoxesList.Count; i++)
            {
                CheckBoxesList[i].Click();
            }            
            CloseSpinneronDiv();
            CloseSpinneronPage();            
            ClickNextButton();            
            CloseSpinneronDiv();
            CloseSpinneronPage();   
        }

        public void ClickNextButton()
        {
            
            WaitForElement(NextButton);
            NextButton.Click();
            
        }

        public void AddAddress()
        {
            AddressButton.Click();
            
            WaitForElement(PostcodeInput);
            PostcodeInput.Click();
            PostcodeInput.SendKeys("L1 0AA");
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            WaitForElement(EnterManuallyButton);
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            WaitForElementToClick(EnterManuallyButton);
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            EnterManuallyButton.Click();
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();           
            WaitForElement(AddressLine1);
            WaitForElementToClick(AddressLine1);
            AddressLine1.Click();
            AddressLine1EnterValue.SendKeysAndtab("21 Jamaica Street");
            wdriver.MoveToActiveAndSendKeys("Liverpool", true);
            wdriver.MoveToActiveAndSendKeys(" ", true);
            wdriver.MoveToActiveAndSendKeys("Merseyside", false);
            WaitForElement(SetAddressButton);
            WaitForElementToClick(SetAddressButton);
            SetAddressButton.Click();
            //Thread.Sleep(500);
            CloseCrispCard();

            // select address from dropdown list
            WaitForElement(AddressButton);
            AddressButton.Click();
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            WaitForElement(PostcodeInput);
            PostcodeInput.SendKeys("Ch41 1AU");
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            WaitForElement(AddressDropdown);
            AddressDropdown.Click();
            WaitForElement(AddressDropdownInput);
            AddressDropdownInput.Click();
            WaitForElement(SetAddressButton);
            SetAddressButton.Click();
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            CloseCrispCard();
            
            ClickNextButton();
        }

        public void AddSocialInfo()
        {
            FaceBookId.Click();
            FacebookElementEnter.SendKeysAndtab("https://www.facebook.com/sunil");
            wdriver.MoveToActiveAndSendKeys("https://plus.google.com/sunil", true);
            wdriver.MoveToActiveAndSendKeys("https://www.linkedin.com/Sunil", true);
            wdriver.MoveToActiveAndSendKeys("@Sunil", false);
            AddWebsiteButton.Click();
            
            WaitForElement(WebsiteAddress);
            WebsiteAddress.Click();
            WaitForElement(WebSiteInputField);
            WebSiteInputField.SendKeys("www.mdsitest.com");
            WaitForElement(AddButton);
            AddButton.Click();
            
            CloseCrispDiv(2);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            
            Assert.IsTrue(WebSiteList.Count > 0);

        }

        //Add Person Method 

        public void AddPersonMainMethod()
        {
            WaitForElement(Dashboardpage.PlusButton);
            Dashboardpage.PlusButton.Click();
            WaitForElement(Dashboardpage.PersonButton);
            Dashboardpage.PersonButton.Click();
            
            AdditionalMethodsPage.AddPerson();
            Dashboardpage.ContactGuidId();
        }

    }

}
