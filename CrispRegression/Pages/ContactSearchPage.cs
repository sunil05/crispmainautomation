﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;

namespace CrispAutomation.Pages
{
    public class ContactSearchPage : Support.Pages
    {
        public ContactSearchPage(ISearchContext driver)
        {
            PageFactory.InitElements(driver, this);
        }

        [FindsBy(How = How.XPath, Using = "//LABEL[@data-error.bind='error'][text()='Name']")]
        public IWebElement NameField;

        [FindsBy(How = How.XPath, Using = "//crisp-input-text[@class='au-target'][@icon='user']/div/input[@type='text']")]
        public IWebElement EnterName;

        [FindsBy(How = How.CssSelector,Using = "//contact-list//crisp-list[@ref='listElm']//ul[@ref='theList']//li[@class='au-target collection-item']//crisp-list-item//a//crisp-list-item-details//span//i[@class='fa fa-map-marker']//parent::span")]
        public IList<IWebElement> AddressDisplay;

        [FindsBy(How = How.CssSelector,Using = "//contact-list//crisp-list[@ref='listElm']//ul[@ref='theList']//li[@class='au-target collection-item']//crisp-list-item//a//crisp-list-item-details//span//i[@class='fa fa-mobile']//parent::span")]
        public IList<IWebElement> MobileDisplay;
        
        [FindsBy(How = How.CssSelector, Using = "//contact-list//crisp-list[@ref='listElm']//ul[@ref='theList']//li[@class='au-target collection-item']//crisp-list-item//a//crisp-list-item-details//span//i[@class='fa fa-phone']//parent::span")]
        public IList<IWebElement> PhoneDisplay;

        [FindsBy(How = How.CssSelector,Using = "//contact-list//crisp-list[@ref='listElm']//ul[@ref='theList']//li[@class='au-target collection-item']//crisp-list-item//a//crisp-list-item-details//span//i[@class='fa fa-envelope-o']//parent::span")]
        public IList<IWebElement> EmailDisplay;

        [FindsBy(How = How.CssSelector,Using = "//contact-list//crisp-list[@ref='listElm']//ul[@ref='theList']//li[@class='au-target collection-item']//crisp-list-item//a//crisp-list-item-title//div[@class='title']//span")]
        public IList<IWebElement> NameDisplay;

        [FindsBy(How = How.XPath, Using = "//crisp-list[@ref='listElm']//ul[@ref='theList']//li[@class='au-target collection-item']//div[@class='title']")]
        public IList<IWebElement> ContactList;

        public void SearchContactDetails()
        {           
            WaitForElements(NameDisplay);
            List<string> names = NameDisplay.Select(i => i.ToString()).ToList();           
            Assert.True(names.Contains("Mr Sunil Sunkishala"));
            WaitForElements(MobileDisplay);
            List<string> mobileNumber = MobileDisplay.Select(i => i.ToString()).ToList();            
            Assert.True(mobileNumber.Contains("07834233955"));
            WaitForElements(PhoneDisplay);
            List<string> phoneNumber = PhoneDisplay.Select(i => i.ToString()).ToList();
            Assert.True(phoneNumber.Contains("01483493725"));
            WaitForElements(EmailDisplay);
            List<string> emailID = EmailDisplay.Select(i => i.ToString()).ToList();
            Assert.True(emailID.Contains("crisp.dev.c@ext.crisp.co.uk"));
            WaitForElements(AddressDisplay);
            List<string> address = AddressDisplay.Select(i => i.ToString()).ToList();
            Assert.True(address.Contains("21 Jamaica Street, Liverpool, Merseyside, L1 0AA"));
            Console.WriteLine("Person Details Has Been Verified");
        }        
    }
}
