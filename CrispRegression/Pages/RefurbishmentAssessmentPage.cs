﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace CrispAutomation.Pages
{
    public class RefurbishmentAssessmentPage : Support.Pages
    {
          public IWebDriver wdriver;
        public RefurbishmentAssessmentPage(ISearchContext driver)
        {
            PageFactory.InitElements(driver, this);
        }

        [FindsBy(How = How.XPath, Using = "//div//a[@class='info-box bg-brown hover-expand-effect']//div[text()='Refurbishment assessment']")]
        public IWebElement RefurbishmentAssessmentOption;

        [FindsBy(How = How.XPath, Using = "//div//a[@class='info-box bg-brown hover-expand-effect']//div[@class='content']")]
        public IWebElement RAStatus;

        [FindsBy(How = How.XPath, Using = "//div[@class='alert alert-warning'][contains(text(),'Refurbishment assessment not yet submitted')]")]
        public IWebElement AlertStatus;

        [FindsBy(How = How.XPath, Using = "//section[@class='content']//div//h2[text()='Refurbishment Assessment']")]
        public IWebElement RefurbishmentSection;

        [FindsBy(How = How.XPath, Using = "//div/a[contains(@href,'/Sites/RiskAssessments/Edit/RefurbishmentAssessment')]")]
        public IWebElement EditButton;

        //Elements on Edit Site Risk Assessment Page

        [FindsBy(How = How.XPath, Using = "//div[@class='alert alert-info'][contains(text(),'Where new build elements also exist this assessment is for the existing structure and conversion elements only')]")]
        public IWebElement AlertStatusonEdit;

        //Elements on Site Overview Section 

        [FindsBy(How = How.XPath, Using = "//div[@class='input-group']//span/i[@class='material-icons']")]
        public IWebElement OriginalDateOfAssessment;

        [FindsBy(How = How.XPath, Using = "//div[@class='dtp-buttons']//button[@class='dtp-btn-ok btn btn-flat'][text()='OK']")]
        public IWebElement DateInput;

        [FindsBy(How = How.CssSelector, Using = "#Assessment_PeoplePresent")]
        public IWebElement PeoplePresent;

        //Elements on Site Exposure & Geo Technical Section 
        [FindsBy(How = How.XPath, Using = "//*[@id='editSra']/div[2]/div[3]/div/div/div[2]/div/div[1]/div[1]/div")]
        public IWebElement LocalExposure;

        [FindsBy(How = How.XPath, Using = "//div[@class='dropdown-menu open'] //ul[@class='dropdown-menu inner']//li//a//span[text()='Moderate']")]
        public IWebElement LocalExposureInput;

        [FindsBy(How = How.CssSelector, Using = "button[data-id = 'Assessment_GroundType'] > span.filter-option.pull-left")]
        public IWebElement GroundType;

        [FindsBy(How = How.XPath, Using = "//div[@class='dropdown-menu open'] //ul[@class='dropdown-menu inner']//li//a//span[text()='Rock']")]
        public IWebElement GroundTypeInput;

        [FindsBy(How = How.XPath, Using = "//form[@id='editSra']//div[contains(@class,'btn-group bootstrap-select')]//button[@data-toggle='dropdown']")]
        public IList<IWebElement> DropdownElementsonSra;

        [FindsBy(How = How.XPath, Using = "//div[contains(@class,'open')]//div[@class='dropdown-menu open']//ul[@class='dropdown-menu inner']//li[@data-original-index='1']")]
        public IWebElement DropDownInput;

        [FindsBy(How = How.XPath, Using = "//form[@id='editSra']//h2[contains(text(),'Site Exposure & Geo Technical')]")]
        public IWebElement SiteExposure;

        [FindsBy(How = How.XPath, Using = "//form[@id='editSra']//div[@class='form-control']//label[@class='form-label'][text()='Yes']")]
        public IList<IWebElement> RadioButtonsonSra;

        [FindsBy(How = How.XPath, Using = "//form[@id='editSra']//div[@class='form-control']//label[@class='form-label'][text()='No']")]
        public IList<IWebElement> NoOptionRadioButtonsonSra;


        [FindsBy(How = How.XPath, Using = "//form[@id='editSra']//div[@class='form-contents']//label[@for='Assessment_AreStageOfWorksAsSpecifiedInTheSiteApplication_false'][text()='No']")]
        public IWebElement RadioButtonsonStagesOfWorks;

        [FindsBy(How = How.XPath, Using = "//form[@id='editSra']//div[@class='row']//div[@class='header']//h2[contains(text(),'Assessment & Scores')]")]
        public IWebElement AssessmentandScores;        

        [FindsBy(How = How.XPath, Using = "//div[contains(@class,'br-theme-bootstrap-stars')]//div[@class='br-widget']/a[5]")]
        public IList<IWebElement> RatingStars;

        [FindsBy(How = How.XPath, Using = "//textarea[contains(@id,'Assessment_Comment')]")]
        public IList<IWebElement> RatingComment;

        [FindsBy(How = How.XPath, Using = "//div[@id='TechnicalConditions']/table/tfoot/tr/td/button[text()='Add conditions']")]
        public IWebElement AddTechConditionButton;

        [FindsBy(How = How.XPath, Using = "//div[@role='dialog']//button[text()='CUSTOM CONDITION']")]
        public IWebElement CustomConditionButton;

        [FindsBy(How = How.XPath, Using = "//div[@id='TechnicalConditions']//table//tbody//tr//td//div//div//div//button")]
        public IList<IWebElement> TechConditionsDropdowns;

        [FindsBy(How = How.XPath, Using = "//div[@id='TechnicalConditions']//table/tbody//tr//td//div[contains(@class,'open')]//div[@class='dropdown-menu open']//ul[@class='dropdown-menu inner']/li")]
        public IList<IWebElement> TechConditionsDropdownsInput;

        [FindsBy(How = How.XPath, Using = "//div[@id='savebar']/button[text()='Save']")]
        public IWebElement SaveButton;

        [FindsBy(How = How.XPath, Using = "//textarea[contains(@id,'Details')][@class='form-control']")]
        public IWebElement TechConditionsDetails;

        [FindsBy(How = How.XPath, Using = "//div[@class='alert alert-warning']")]
        public IWebElement SubmitAlert;

        [FindsBy(How = How.XPath, Using = "//button[text()='Submit assessment']")]
        public IWebElement SubmitAssessmentButton;

        [FindsBy(How = How.XPath, Using = "//form[@id='submitra']//div//button[text()='Acceptable Risk']")]
        public IWebElement AcceptableRiskButton;

        [FindsBy(How = How.XPath, Using = "//div[@class='alert alert-success'][contains(text(),'Refurbishment assessment submitted.')]")]
        public IWebElement SubmitSuccessAlert;

        [FindsBy(How = How.XPath, Using = "//input[@id='Assessment_DescriptionOfWorks']")]
        public IWebElement DescriptionOfWork;
        
        [FindsBy(How = How.XPath, Using = "//form[@id='editSra']//h2[contains(text(),'Conversion Overview')]")]
        public IWebElement ConversionOverviewDiv;

        [FindsBy(How = How.XPath, Using = "//form[@id='editSra']//div[contains(@class,'btn-group bootstrap-select')]//button[@data-toggle='dropdown'][@data-id='Assessment_SiteComplexity']")]
        public IWebElement SiteComplexityDropdown;

        [FindsBy(How = How.XPath, Using = "//div[@id='page2']//div[@class='form-control']//label[@for='Assessment_AcceptableRisk_true']")]
        public IWebElement AcceptableRiskRadioButton;
        
        [FindsBy(How = How.XPath, Using = "//div[@id='page2']//div[@class='form-contents']//button[@type='submit'][contains(text(),'Submit')]")]
        public IWebElement SubmitButton;

        [FindsBy(How = How.XPath, Using = "//div[@id='page2']//div[@class='form-contents']//button[@type='submit'][contains(text(),'Save')]")]
        public IWebElement AssessmentSaveButton;

        [FindsBy(How = How.XPath, Using = "//div[@id='savebar']//button[contains(@class,'btn btn-success btn-lg pull-right')][text()='Next']")]
        public IWebElement NextButton;

        [FindsBy(How = How.XPath, Using = "//section[@class='content']//form[@method='post']//button[@type='submit'][contains(text(),'Send')]")]
        public IWebElement SendButton;

        //open SRA 
        public void OpenRA()
        {
            Thread.Sleep(500);
            CloseSpinnerOnSurveyorPage();
            WaitForElementonSurveyor(RefurbishmentAssessmentOption);
            //Thread.Sleep(500);
            CloseSpinnerOnSurveyorPage();
            //Thread.Sleep(500);
            RefurbishmentAssessmentOption.Click();
            //Thread.Sleep(500);
            CloseSpinnerOnSurveyorPage();
            //Thread.Sleep(500);
            WaitForElementonSurveyor(RefurbishmentSection);
            Assert.IsTrue(RefurbishmentSection.Displayed, "Failed to send correspondence on Site Risk Assessment");
            Assert.IsTrue(EditButton.Displayed, "No permissions applied to the user to submit SiteRisk Assessment");
            //Thread.Sleep(1000);
            WaitForElementonSurveyor(EditButton);
            EditButton.Click();
            //Thread.Sleep(500);
            CloseSpinnerOnSurveyorPage();
            //Thread.Sleep(500);
        }
        // Submit SRA 

        public void SubmitRA()
        {
            Thread.Sleep(500);
            CloseSpinnerOnSurveyorPage();
            WaitForElementonSurveyor(AcceptableRiskRadioButton);
            AcceptableRiskRadioButton.Click();
            //Thread.Sleep(500);
            CloseSpinnerOnSurveyorPage();
            //Thread.Sleep(500);
            WaitForElementonSurveyor(SubmitButton);
            SubmitButton.Click();
            //Thread.Sleep(500);
            CloseSpinnerOnSurveyorPage();
            //Thread.Sleep(500);
            WaitForElementonSurveyor(SendButton);
            SendButton.Click();
            //Thread.Sleep(500);
            CloseSpinnerOnSurveyorPage();
            //Thread.Sleep(500);
            WaitForElementonSurveyor(RefurbishmentSection);
            Assert.IsTrue(RefurbishmentSection.Displayed, "Failed to send correspondence on Refurbishment Assessment");
        }

        // Save RA 
        public void SaveRA()
        {
            Thread.Sleep(500);
            CloseSpinnerOnSurveyorPage();
            WaitForElementonSurveyor(AcceptableRiskRadioButton);
            AcceptableRiskRadioButton.Click();
            //Thread.Sleep(500);
            CloseSpinnerOnSurveyorPage();
            //Thread.Sleep(500);
            WaitForElementonSurveyor(SubmitButton);
            var SubmitButtonStatus = SubmitButton.GetAttribute("disabled");
            if(SubmitButtonStatus != null || SubmitButtonStatus == "true")
            {
                WaitForElementonSurveyor(AssessmentSaveButton);
                AssessmentSaveButton.Click();
                //Thread.Sleep(500);
                CloseSpinnerOnSurveyorPage();
                //Thread.Sleep(500);
                WaitForElementonSurveyor(RefurbishmentSection);
                Assert.IsTrue(RefurbishmentSection.Displayed, "Failed to send correspondence on Refurbishment Assessment");
                
            }else
            {
                WaitForElementonSurveyor(SubmitButton);
                SubmitButton.Click();
                //Thread.Sleep(500);
                CloseSpinnerOnSurveyorPage();
                //Thread.Sleep(500);
                WaitForElementonSurveyor(SendButton);
                SendButton.Click();
                //Thread.Sleep(500);
                CloseSpinnerOnSurveyorPage();
                //Thread.Sleep(500);
                WaitForElementonSurveyor(RefurbishmentSection);
                Assert.IsTrue(RefurbishmentSection.Displayed, "Failed to send correspondence on Refurbishment Assessment");
            }
          
        }

        public void RefurbishmentAssessmentMain()
        {
            SurveyorLoginPage.SelectSite();
            // SurveyorAssessments.VerifySiteStatusBeforeAssessment();
            Thread.Sleep(500);
            CloseSpinnerOnSurveyorPage();
            CloseSpinnerOnSurveyorDiv();
            Thread.Sleep(500);
            WaitForElementonSurveyor(RefurbishmentAssessmentOption);
            WaitForElementonSurveyor(RAStatus);
            var raStatus = RAStatus.Text.ToString();
            Thread.Sleep(500);
            CloseSpinnerOnSurveyorPage();
            CloseSpinnerOnSurveyorDiv();
            Thread.Sleep(500);
            if (raStatus.Contains("Outstanding"))
            {
                OpenRA();
                //Thread.Sleep(500);
                CloseSpinnerOnSurveyorPage();
                //Thread.Sleep(500);
                WaitForElementonSurveyor(AlertStatusonEdit);
                Assert.IsTrue(AlertStatusonEdit.Displayed);
                EditRefurbishmentAssessmentSection();
                SurveyorDocs.CloseSurveyorDocuments();
                SubmitRA();
            }
        }


        public void EditRefurbishmentAssessmentSection()
        {
            SiteOverviewSection();            
            SraEditPage();
            AssessmentScoresSection();
            WaitForElement(AcceptableRiskRadioButton);
            AcceptableRiskRadioButton.Click();
            //Thread.Sleep(500);
            CloseSpinnerOnSurveyorPage();
            //Thread.Sleep(500);
        }

        public void EditRefurbishmentAssessmentWhenNoSelected()
        {
            SiteOverviewSection();
            SraEditPageWhenNoSelected();
            AssessmentScoresSection();
            WaitForElement(AcceptableRiskRadioButton);
            AcceptableRiskRadioButton.Click();
            //Thread.Sleep(500);
            CloseSpinnerOnSurveyorPage();
            //Thread.Sleep(500);
        }

        //form[@id="editSra"]//a[5]
        public void SiteOverviewSection()
        {
            WaitForElementonSurveyor(OriginalDateOfAssessment);
            OriginalDateOfAssessment.Click();
            //Thread.Sleep(500);            
            WaitForElementonSurveyor(DateInput);
            DateInput.Click();
            //Thread.Sleep(500);
            CloseSpinnerOnSurveyorPage();
            //Thread.Sleep(500);
            WaitForElementonSurveyor(PeoplePresent);
            PeoplePresent.Click();
            PeoplePresent.Clear();
            PeoplePresent.SendKeys("3");
        }
        public void SraEditPage()
        {
            // Create a javascript executor
            IJavaScriptExecutor js = Driver as IJavaScriptExecutor;
            // Run the javascript command 'scrollintoview on the element
            js.ExecuteScript("arguments[0].scrollIntoView(true);", ConversionOverviewDiv);

            WaitForLoadElements(RadioButtonsonSra);

            foreach (var RadioButton in RadioButtonsonSra)
            {
                RadioButton.Click();
            }
            //Thread.Sleep(500);
            CloseSpinnerOnSurveyorPage();
            //Thread.Sleep(500);
            //WaitForElementonSurveyor(RadioButtonsonStagesOfWorks);
            //RadioButtonsonStagesOfWorks.Click();           
            //WaitForLoadElements(DropdownElementsonSra);
            //for (int i = 0; i <31; i++)
            //{
            //    WaitForLoadElementtobeclickable(DropdownElementsonSra[i]);
            //    DropdownElementsonSra[i].SendKeys(Keys.Enter);
            //    //Thread.Sleep(500);
            //    WaitForElementonSurveyor(DropDownInput);
            //    DropDownInput.Click();
            //    DropdownElementsonSra[i].SendKeys(Keys.Tab);
            //}
            WaitForLoadElements(DropdownElementsonSra);
            if (DropdownElementsonSra.Count > 0)
            {
                for (int i = 0; i < DropdownElementsonSra.Count; i++)
                {
                    var element = DropdownElementsonSra[i];
                    bool isVisible = element.Displayed;


                    var javascriptCapableDriver = (IJavaScriptExecutor)Driver;
                    var visibleScript = "return $('[data-id=\"" + DropdownElementsonSra[i].GetAttribute("data-id") + "\"]').is(':visible');";

                    bool jQueryBelivesElementIsVisible = Convert.ToBoolean(javascriptCapableDriver.ExecuteScript(visibleScript));
                    bool elementIsVisible = isVisible && jQueryBelivesElementIsVisible;

                    if (elementIsVisible == true)
                    {
                        WaitForLoadElementtobeclickable(DropdownElementsonSra[i]);
                        DropdownElementsonSra[i].SendKeys(Keys.Enter);
                        //Thread.Sleep(500);
                        WaitForElementonSurveyor(DropDownInput);
                        DropDownInput.Click();
                        DropdownElementsonSra[i].SendKeys(Keys.Tab);
                    }
                }
            }
            //WaitForLoadElement(SiteComplexityDropdown);
            //SiteComplexityDropdown.Click();
            
            //WaitForElementonSurveyor(DropDownInput);
            //DropDownInput.Click();
            //Thread.Sleep(500);
            //WaitForElementonSurveyor(DescriptionOfWork);
            //DescriptionOfWork.Clear();
            //DescriptionOfWork.SendKeys("Conversion");
        }

        public void SraEditPageWhenNoSelected()
        {
            // Create a javascript executor
            IJavaScriptExecutor js = Driver as IJavaScriptExecutor;
            // Run the javascript command 'scrollintoview on the element
            js.ExecuteScript("arguments[0].scrollIntoView(true);", ConversionOverviewDiv);

            WaitForElements(RadioButtonsonSra);
            foreach (var RadioButton in NoOptionRadioButtonsonSra)
            {
                RadioButton.Click();
            }
            //Thread.Sleep(500);
            CloseSpinnerOnSurveyorPage();
            //Thread.Sleep(500);
            //WaitForElementonSurveyor(RadioButtonsonStagesOfWorks);
            //RadioButtonsonStagesOfWorks.Click();           
            //WaitForLoadElements(DropdownElementsonSra);
            //for (int i = 0; i <31; i++)
            //{
            //    WaitForLoadElementtobeclickable(DropdownElementsonSra[i]);
            //    DropdownElementsonSra[i].SendKeys(Keys.Enter);
            //    //Thread.Sleep(500);
            //    WaitForElementonSurveyor(DropDownInput);
            //    DropDownInput.Click();
            //    DropdownElementsonSra[i].SendKeys(Keys.Tab);
            //}
            WaitForLoadElements(DropdownElementsonSra);
            if (DropdownElementsonSra.Count > 0)
            {
                for (int i = 0; i < DropdownElementsonSra.Count; i++)
                {
                    var element = DropdownElementsonSra[i];
                    bool isVisible = element.Displayed;


                    var javascriptCapableDriver = (IJavaScriptExecutor)Driver;
                    var visibleScript = "return $('[data-id=\"" + DropdownElementsonSra[i].GetAttribute("data-id") + "\"]').is(':visible');";

                    bool jQueryBelivesElementIsVisible = Convert.ToBoolean(javascriptCapableDriver.ExecuteScript(visibleScript));
                    bool elementIsVisible = isVisible && jQueryBelivesElementIsVisible;

                    if (elementIsVisible == true)
                    {
                        WaitForLoadElementtobeclickable(DropdownElementsonSra[i]);
                        DropdownElementsonSra[i].SendKeys(Keys.Enter);
                        //Thread.Sleep(500);
                        WaitForElementonSurveyor(DropDownInput);
                        DropDownInput.Click();
                        DropdownElementsonSra[i].SendKeys(Keys.Tab);
                    }
                }
            }
            //WaitForLoadElement(SiteComplexityDropdown);
            //SiteComplexityDropdown.Click();
            
            //WaitForElementonSurveyor(DropDownInput);
            //DropDownInput.Click();
            //Thread.Sleep(500);
            //WaitForElementonSurveyor(DescriptionOfWork);
            //DescriptionOfWork.Clear();
            //DescriptionOfWork.SendKeys("Conversion");
        }


        public void AssessmentScoresSection()
        {
             WaitForLoadElements(RatingStars);
             IJavaScriptExecutor js = Driver as IJavaScriptExecutor;
            // Run the javascript command 'scrollintoview on the element
             //js.ExecuteScript("scroll(0,3500);");
             js.ExecuteScript("arguments[0].scrollIntoView(true);", AssessmentandScores);
            foreach (var eachRatingStar in RatingStars)
            {
                eachRatingStar.Click();
            }

            WaitForElements(RatingComment);
            foreach (var eachComment in RatingComment)
            {
                eachComment.Click();
                eachComment.Clear();
                eachComment.SendKeys("GoodRate");
            }
            WaitForElement(NextButton);
            NextButton.Click();
            //Thread.Sleep(500);
            CloseSpinnerOnSurveyorPage();
            //Thread.Sleep(500);
        }
        public void SubmitRefurbishmentAssessmentMethod()
        {
            SurveyorLoginPage.SurveyorLoginMethod();
            SurveyorLoginPage.SelectSite();
            WaitForElementonSurveyor(RefurbishmentAssessmentOption);
            //Thread.Sleep(500);
            CloseSpinnerOnSurveyorPage();
            //Thread.Sleep(500);
            RefurbishmentAssessmentOption.Click();
            //Thread.Sleep(500);
            CloseSpinnerOnSurveyorPage();
            //Thread.Sleep(500);
            WaitForElementonSurveyor(RefurbishmentSection);
            Assert.IsTrue(RefurbishmentSection.Displayed, "Failed to send correspondence on Site Risk Assessment");
            Assert.IsTrue(EditButton.Displayed, "No permissions applied to the user to submit SiteRisk Assessment");
            //Thread.Sleep(1000);
            WaitForElementonSurveyor(EditButton);
            EditButton.Click();
            //Thread.Sleep(500);
            CloseSpinnerOnSurveyorPage();
            //Thread.Sleep(500);
            //Thread.Sleep(500);
            CloseSpinnerOnSurveyorPage();
            //Thread.Sleep(500);
            WaitForElementonSurveyor(AlertStatusonEdit);
            Assert.IsTrue(AlertStatusonEdit.Displayed);
            EditRefurbishmentAssessmentSection();
            SurveyorDocs.CloseSurveyorDocuments();
            SubmitRA();         
        }
    }
}
