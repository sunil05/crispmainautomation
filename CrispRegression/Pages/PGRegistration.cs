﻿using CrispAutomation.Support;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace CrispAutomation.Pages
{
    public class PGRegistration : Support.Pages
    {
        public IWebDriver wdriver;


        public PGRegistration(ISearchContext driver)
        {
            PageFactory.InitElements(driver, this);
            wdriver = (RemoteWebDriver)driver;
        }
        [FindsBy(How = How.XPath, Using = "//crisp-tabs//ul[@ref='tabHeader']//li[@class='au-target tab']//span//a[contains(text(),'PG Registration')]")]
        public IWebElement PGRegistrationTab;

        [FindsBy(How = How.XPath, Using = "//button[@ref='button'][text()='Create Registration'][@class='au-target waves-effect waves-light btn-flat']")]
        public IList<IWebElement> CreateRegistrationButton;
        

        [FindsBy(How = How.XPath, Using = "//registration-view//crisp-header//nav//div//crisp-header-title//div[contains(text(),'Reference')]")]
        public IWebElement RegistrationReference;

        // Overview Section Elements 

        [FindsBy(How = How.XPath, Using = "//registration-view//crisp-tabs//ul[@ref='tabHeader']//li[@class='au-target tab']//span//a[contains(text(),'Overview')]")]
        public IWebElement OverviewTab;

        [FindsBy(How = How.XPath, Using = "//registration-view//crisp-tabs//div[@class='au-target tabcontainer']//crisp-card//crisp-card-content//div[@class='card-content']//dl[1]//dd[4]")]
        public IWebElement StandardFeesStatus;

        [FindsBy(How = How.XPath, Using = "//div[@ref='tabContainer']//crisp-card//crisp-card-actions//button[@class='au-target waves-effect waves-light btn-flat'] [text()='Toggle Non-Standard Fees']")]
        public IWebElement StandardFeesButton;

        [FindsBy(How = How.XPath, Using = "//div[@ref='tabContainer']//crisp-card//crisp-card-actions//button[@class='au-target waves-effect waves-light btn-flat'] [text()='Toggle Send Receipt Automation']")]
        public IWebElement SendReceiptAutomationButton;

        [FindsBy(How = How.XPath, Using = "//registration-view//crisp-tabs//div[@class='au-target tabcontainer']//crisp-card//crisp-card-content//div[@class='card-content']//dl[4]//dd")]
        public IWebElement SendAutomationReceiptStatus;

        [FindsBy(How = How.XPath, Using = "//div[@class='au-target modal dialog brand default modal-has-header']//div[text()='Send Receipt Automation']")]
        public IWebElement SendReceiptAutomationDiv;

        [FindsBy(How = How.XPath, Using = "//div[@class='au-target modal dialog brand default modal-has-header']//button[text()='confirm']")]
        public IWebElement SendReceiptAutomationConfirmButton;
        
        [FindsBy(How = How.XPath, Using = "//div[@class='au-target modal dialog brand default modal-has-header']//div[@class='modal-content']//span[@class='message']")]
        public IWebElement SendReceiptAutomationMessage;

        //Reg Documents Section Elements 

        [FindsBy(How = How.XPath, Using = "//registration-view//crisp-tabs//ul[@ref='tabHeader']//li[@class='au-target tab']//span//a[contains(text(),'Reg Documents')]")]
        public IWebElement RegDocumentTab;

        [FindsBy(How = How.XPath, Using = "//div[@ref='tabContainer']//security-documents//crisp-list[contains(@source.bind,'SecurityDocuments')]//crisp-action-button[@click.delegate='add()']//a//i")]
        public IList<IWebElement> AddSecurityDocButton;

        //[FindsBy(How = How.XPath, Using = "//div[@ref='tabContainer']//security-documents[@title='Additional Indemnities']//crisp-list[@source.bind='securityDocuments']//crisp-action-button[@click.delegate='add()']//a//i")]
        //public IList<IWebElement> AddSecurityDocsButton;

        [FindsBy(How = How.XPath, Using = "//crisp-dialog//div//crisp-header//div[text()='Add']")]
        public IWebElement AddDocsDiv;

        [FindsBy(How = How.XPath, Using = "//div[@class='modal-content']//crisp-picker[@label='Document Type']//div//input[@class='select-dropdown']")]
        public IWebElement DocumentTypeDropdown;

        [FindsBy(How = How.XPath, Using = "//ul[@class='dropdown-content select-dropdown active']//li//span")]
        public IList<IWebElement> DocumentTypeDropdownInput;

        [FindsBy(How = How.XPath, Using = "//crisp-input-bool[@label='Is Bespoke']//label")]
        public IWebElement IsBespoke;

        [FindsBy(How = How.XPath, Using = "//div[@class='modal-content']//crisp-input-object[@icon='file']//div//label[contains(text(),'Amended Version Upload')]")]
        public IWebElement UploadFileLabel;

        [FindsBy(How = How.XPath, Using = "//crisp-card-actions//crisp-button//button[text()='Upload New Files'][@class='au-target waves-effect waves-light btn-flat']")]
        public IWebElement UploadNewFilesButton;

        [FindsBy(How = How.XPath, Using = "//crisp-card//crisp-card-actions//crisp-button[@click.delegate='complete()']//button[@class='au-target waves-effect waves-light btn']")]
        public IWebElement UploadFileSelectButton;

        [FindsBy(How = How.XPath, Using = "//crisp-input-object//div//label[text()='Children']")]
        public IList<IWebElement> ChildrenLabel;

        [FindsBy(How = How.XPath, Using = "//contact-list//crisp-list//ul[@ref='theList']//li[@class='au-target collection-item']")]
        public IList<IWebElement> ChildrenList;

        [FindsBy(How = How.XPath, Using = "//div[@class='modal-content']//crisp-input-object//div//label[text()='Covered Documents']")]
        public IWebElement CoverDoclabel;

        [FindsBy(How = How.XPath, Using = "//crisp-card-content//crisp-list//span//input[@placeholder ='Enter filter text...']")]
        public IWebElement ChooseCoverDocLabel;

        [FindsBy(How = How.XPath, Using = "//div[@class='card-content']//crisp-list//ul[@ref='theList']//li[@class='au-target collection-item']")]
        public IList<IWebElement> CoverDocsList;

        [FindsBy(How = How.XPath, Using = "//crisp-picker[@label='Document Version']//div//input[@class='select-dropdown']")]
        public IWebElement DocVersionLabel;

        [FindsBy(How = How.XPath, Using = "//ul[@class='dropdown-content select-dropdown active']//li//span")]
        public IList<IWebElement> DocVersionDropdownInput;

        [FindsBy(How = How.XPath, Using = "//security-documents//crisp-list[contains(@source.bind,'SecurityDocuments')]//ul[@ref='theList']//li[@class='au-target collection-item']")]
        public IList<IWebElement> SecurityDocsList;

        [FindsBy(How = How.XPath, Using = "//security-documents//crisp-list[contains(@source.bind,'SecurityDocuments')]//ul[@ref='theList']//li[@class='au-target collection-item']//crisp-list-item//crisp-list-item-details//div[contains(text(),'Review Status')]")]
        public IList<IWebElement> SecurityDocsListReviewStatus;

        [FindsBy(How = How.XPath, Using = "//crisp-tabs//crisp-card//crisp-card-content/div[@class='card-content']//div//dl//dd[4]")]
        public IWebElement ReviewStatusValue;

        [FindsBy(How = How.XPath, Using = "//crisp-tabs//crisp-card//crisp-card-content/div[@class='card-content']//div//dl//dd[5]")]
        public IWebElement RejectReasonValue;

        [FindsBy(How = How.XPath, Using = "//crisp-tabs//crisp-card//crisp-card-content/div[@class='card-content']//div//dl//dd[8]")]
        public IWebElement ISBespokeValue;

        [FindsBy(How = How.XPath, Using = "//crisp-card-actions//div//crisp-button//span//button[text()='Upload']")]
        public IWebElement UploadButton;


        [FindsBy(How = How.XPath, Using = "//crisp-card-actions//div//crisp-button//span//button[text()='Edit']")]
        public IWebElement EditButton;

        [FindsBy(How = How.XPath, Using = "//ai-dialog-container[2]/div/div/crisp-dialog/div/crisp-header/nav/div")]
        public IWebElement EditDiv;

        [FindsBy(How = How.XPath, Using = "//ai-dialog-container[2]/div/div/crisp-dialog/div/crisp-header/nav/div")]
        public IWebElement UploadDiv;

        [FindsBy(How = How.XPath, Using = "//div[@class='modal-content']//crisp-input-object[@icon='file']//div[@class='au-target input-field']//span[@ref='inputContainer']")]
        public IWebElement RelatedFiles;
                

        [FindsBy(How = How.XPath, Using = "//crisp-card-content//div//upload-documents-embed//crisp-input-file[@class='au-target']//div//div//input[@ref='fileInput']")]
        public IWebElement SendFileInput;

        [FindsBy(How = How.XPath, Using = "//crisp-input-file/div/div[1]/input")]
        public IWebElement UploadDoc;

        [FindsBy(How = How.XPath, Using = "//crisp-card-actions//div//crisp-button//span//button[text()='Review']")]
        public IWebElement ReviewButton;

        [FindsBy(How = How.XPath, Using = "//ai-dialog-container[2]/div/div/crisp-dialog/div")]
        public IWebElement ReviewDiv;

        [FindsBy(How = How.XPath, Using = "//crisp-picker[@label='Review Status']//div//input[@class='select-dropdown']")]
        public IWebElement ReviewStatusDropdown;

        [FindsBy(How = How.XPath, Using = "//crisp-picker[@label='Rejection Reason']//div//input[@class='select-dropdown']")]
        public IWebElement RejectReasonDropdown;

        [FindsBy(How = How.XPath, Using = "//ul[@class='dropdown-content select-dropdown active']//li//span[text()='Not signed correctly']")]
        public IWebElement RejectReasonDropdownInput;

        [FindsBy(How = How.XPath, Using = "//crisp-input-textarea[@label='Rejection Reason Notes']//div//label")]
        public IWebElement RejectReasonNotes;

        [FindsBy(How = How.XPath, Using = "//crisp-input-textarea[@label='Rejection Reason Notes']//div//textarea")]
        public IWebElement RejectReasonNotesInput;


        [FindsBy(How = How.XPath, Using = "//ul[@class='dropdown-content select-dropdown active']//li//span[text()='Confirmed']")]
        public IWebElement ConfirmedStatusDropdownInput;

        [FindsBy(How = How.XPath, Using = "//ul[@class='dropdown-content select-dropdown active']//li//span[text()='Rejected']")]
        public IWebElement RejectsStatusDropdownInput;

        [FindsBy(How = How.XPath, Using = "//crisp-input-date[@label='Date Received']//div//label")]
        public IWebElement DateOnUploadDiv;


        //Conditions Section Elements 

        [FindsBy(How = How.XPath, Using = "//registration-view//crisp-tabs//ul[@ref='tabHeader']//li[@class='au-target tab']//span//a[contains(text(),'Conditions')]")]
        public IWebElement ConditionsTab;

        [FindsBy(How = How.XPath, Using = "//div[@ref='tabContainer']//crisp-list[@list-title='Conditions']")]
        public IWebElement ConditionsTitle;

        [FindsBy(How = How.XPath, Using = "//div[@ref='tabContainer']//crisp-list[@list-title='Conditions']//crisp-input-bool[@label='Include Closed?']")]
        public IWebElement IncludeClosedCheckbox;

        [FindsBy(How = How.XPath, Using = "//div[@ref='tabContainer']//crisp-list[@list-title='Conditions']//crisp-action-button[@click.delegate='addNewCondition()']//a//i")]
        public IWebElement AddConditionButton;

        [FindsBy(How = How.XPath, Using = "//ai-dialog-container//crisp-dialog//div[@class='au-target modal dialog brand default modal-has-header']//div[@class='nav-wrapper au-target'][text()='Add']")]
        public IWebElement AddConditionDiv;

        [FindsBy(How = How.XPath, Using = "//ai-dialog-container//crisp-dialog//div[@class='modal-content']//crisp-input-textarea[@label='Description']//label")]
        public IWebElement ConditionDescriptionLabel;

        [FindsBy(How = How.XPath, Using = "//ai-dialog-container//crisp-dialog//div[@class='modal-content']//crisp-input-textarea[@label='Description']//textarea")]
        public IWebElement ConditionDescriptionInput;

        [FindsBy(How = How.XPath, Using = "//ai-dialog-container//crisp-dialog//div[@class='modal-content']//crisp-input-object//div//span[@ref='inputContainer']//span")]
        public IWebElement ProcessLabel;

        [FindsBy(How = How.XPath, Using = "//crisp-card-content//div//crisp-list//ul//li[@class='au-target collection-item']")]
        public IList<IWebElement> ProcessList;

        [FindsBy(How = How.XPath, Using = "//crisp-card//crisp-card-actions//crisp-button[@click.delegate='ok()']//button[@class='au-target waves-effect waves-light btn']")]
        public IWebElement SelectButton;

        [FindsBy(How = How.XPath, Using = "//ai-dialog-container//crisp-dialog//div[@class='modal-footer']//crisp-button[@click.delegate='ok()']//button[@class='au-target waves-effect waves-light btn-flat']")]
        public IWebElement OkButton;

        [FindsBy(How = How.XPath, Using = "//crisp-list[@list-title='Conditions']//ul[@ref='theList']//li[@class='au-target collection-item']")]
        public IList<IWebElement> ConditionsList;

        [FindsBy(How = How.XPath, Using = "//div[@class='au-target side-nav right-aligned brand default']//crisp-footer//nav//crisp-footer-button[@click.delegate='amend()']//button[@class='au-target waves-effect waves-light btn-flat']")]
        public IWebElement ConditionAmendButton;

        [FindsBy(How = How.XPath, Using = "//ai-dialog-container//crisp-dialog//div[@class='au-target modal dialog brand default modal-has-header']//div[@class='nav-wrapper au-target'][text()='Edit']")]
        public IWebElement EditConditionDiv;

        [FindsBy(How = How.XPath, Using = "//ai-dialog-container//crisp-dialog//div[@class='modal-content']//crisp-input-textarea[@label='Amendment comments']//label")]
        public IWebElement AmendConditionCommentLabel;

        [FindsBy(How = How.XPath, Using = "//ai-dialog-container//crisp-dialog//div[@class='modal-content']//crisp-input-textarea[@label='Amendment comments']//textarea")]
        public IWebElement AmendConditionCommentInput;

        [FindsBy(How = How.XPath, Using = "//div[@class='au-target side-nav right-aligned brand default']//crisp-footer//nav//crisp-footer-button[@click.delegate='cancel()']//button[@class='au-target waves-effect waves-light btn-flat']")]
        public IWebElement ConditionCancelButton;

        [FindsBy(How = How.XPath, Using = "//div[@class='au-target side-nav right-aligned brand default']//crisp-footer//nav//crisp-footer-button[@click.delegate='close()']//button[@class='au-target waves-effect waves-light btn-flat']")]
        public IWebElement ConditionCloseButton;

        [FindsBy(How = How.XPath, Using = "//ai-dialog-container//crisp-dialog//div[@class='au-target modal dialog brand default modal-has-header']//div[@class='nav-wrapper au-target'][text()='Close Condition']")]
        public IWebElement CloseConditionDiv;

        [FindsBy(How = How.XPath, Using = "//ai-dialog-container//crisp-dialog//div[@class='modal-content']//crisp-input-textarea[@label='Closing comments']//label")]
        public IWebElement CloseConditionCommentLabel;

        [FindsBy(How = How.XPath, Using = "//ai-dialog-container//crisp-dialog//div[@class='modal-content']//crisp-input-textarea[@label='Closing comments']//textarea")]
        public IWebElement CloseConditionCommentInput;

        [FindsBy(How = How.XPath, Using = "//crisp-list[@list-title='Conditions']//ul[@ref='theList']//li[@class='au-target collection-item']//crisp-list-item//a//crisp-list-item-title//div[@class='title']//i")]
        public IList<IWebElement> ClosedConditionsList;

        [FindsBy(How = How.XPath, Using = "//div[@class='au-target side-nav right-aligned brand default']//crisp-footer//nav//crisp-footer-button[@click.delegate='reopen()']//button[@class='au-target waves-effect waves-light btn-flat']")]
        public IWebElement ConditionReopenButton;

        [FindsBy(How = How.XPath, Using = "//ai-dialog-container//crisp-dialog//div[@class='modal-content']//crisp-input-textarea[@label='Reopen Comments']//label")]
        public IWebElement ReopenCommentLabel;

        [FindsBy(How = How.XPath, Using = "//ai-dialog-container//crisp-dialog//div[@class='modal-content']//crisp-input-textarea[@label='Reopen Comments']//textarea")]
        public IWebElement ReopenCommentInput;

        //Terms Elements 

        [FindsBy(How = How.XPath, Using = "//registration-view//crisp-tabs//ul[@ref='tabHeader']//li[@class='au-target tab']//span//a[contains(text(),'Terms')]")]
        public IWebElement TermsTab;

        [FindsBy(How = How.XPath, Using = "//div[@ref='tabContainer']//crisp-list[@list-title='Terms']//crisp-action-button[@click.delegate='addNewTerm()']//a//i")]
        public IWebElement AddTermsButton;

        [FindsBy(How = How.XPath, Using = "//ai-dialog-container//crisp-dialog//div[contains(@class,'au-target modal dialog brand default')]//div[@class='nav-wrapper au-target']//div[text()='Add New Term']")]
        public IWebElement AddTermsDiv;

        [FindsBy(How = How.XPath, Using = "//div[@class='card-content']//crisp-input-date[@label='Start Date']//div//label")]
        public IWebElement StartDateLabel;

        [FindsBy(How = How.XPath, Using = "//div[contains(@class,'picker--opened')]//div[@class='picker__header']//select[@title='Select a year']")]
        public IWebElement SelectYearDropdown;

        [FindsBy(How = How.XPath, Using = "//div[contains(@class,'picker--opened')]//div[@class='picker__header']//select[@title='Select a year']//option")]
        public IList<IWebElement> SelectYearInput;

        [FindsBy(How = How.XPath, Using = "//div[contains(@class,'picker--opened')]//table[@class='picker__table']/tbody//tr//td//div[contains(@class,'day--infocus')][text()='3']")]
        public IWebElement SelectDate;

        [FindsBy(How = How.XPath, Using = "//div[contains(@class,'picker--focused')]//div[@class='picker__footer']/button[@class='btn-flat picker__today']")]
        public IWebElement StartDate;

        [FindsBy(How = How.XPath, Using = "//dl//dt[text()='End Date']")]
        public IWebElement EndDate;

        [FindsBy(How = How.XPath, Using = "//div[@class='modal-content']//crisp-card//crisp-card-content//dl//dd")]
        public IWebElement EndDateInput;

        [FindsBy(How = How.XPath, Using = "//crisp-button[@click.delegate='save()']//button[@class='au-target waves-effect waves-light btn-flat']")]
        public IWebElement SaveButton;

        [FindsBy(How = How.XPath, Using = "//crisp-footer-button[@click.delegate='save()']//button[@class='au-target waves-effect waves-light btn-flat']")]
        public IWebElement NotesSaveButton;

        [FindsBy(How = How.XPath, Using = "//crisp-button[@click.delegate='cancel()']//button[@class='au-target waves-effect waves-light btn-flat']")]
        public IWebElement CancelButton;

        [FindsBy(How = How.XPath, Using = "//crisp-list[@list-title='Terms']/ul[@ref='theList']//li[@class='au-target collection-item']//crisp-list-item//a//crisp-list-item-title//div")]
        public IList<IWebElement> TermsList;

        [FindsBy(How = How.XPath, Using = "//crisp-list[@list-title='Terms']/ul[@ref='theList']//li[@class='au-target collection-item']//crisp-list-item//a//crisp-list-item-title//div[contains(text(),'Active')]")]
        public IList<IWebElement> ActiveTerm;

        [FindsBy(How = How.XPath, Using = "//crisp-list[@list-title='Terms']/ul[@ref='theList']//li[@class='au-target collection-item']//crisp-list-item//a//crisp-list-item-title//div[contains(text(),'Prospective')]")]
        public IList<IWebElement> ProspectiveTerm;

        [FindsBy(How = How.XPath, Using = "//crisp-list[@list-title='Terms']/ul[@ref='theList']//li[@class='au-target collection-item active']//crisp-list-item//a//crisp-list-item-action//div[@class='secondary-content']//crisp-button")]
        public IWebElement EditTerm;

        [FindsBy(How = How.XPath, Using = "//ai-dialog-container//crisp-dialog//div[contains(@class,'au-target modal dialog brand default')]//div[@class='nav-wrapper au-target']//div[text()='Edit Term']")]
        public IWebElement EditTermsDiv;

        [FindsBy(How = How.XPath, Using = "//div[@class='modal-content']//crisp-input-currency[contains(@value.bind,'termFeeAdjustment')]//input")]
        public IWebElement AdjustmentFeeInput;

        [FindsBy(How = How.XPath, Using = "//div[@class='modal-content']//crisp-picker[@label='Adjustment Reason']//input")]
        public IWebElement AdjustmentReason;

        [FindsBy(How = How.XPath, Using = "//ul[@class='dropdown-content select-dropdown active']//li//span[text()='Fee Waived']")]
        public IWebElement AdjustmentReasonInputFee;

        [FindsBy(How = How.XPath, Using = "//ul[@class='dropdown-content select-dropdown active']//li//span[text()='Group Discount']")]
        public IWebElement AdjustmentReasonInputGroup;

        [FindsBy(How = How.XPath, Using = "//crisp-input-textarea[contains(@value.bind,'termFeeAdjustmentDescription')]//div//textarea")]
        public IWebElement AdjustmentDescription;

        [FindsBy(How = How.XPath, Using = "//crisp-card-actions//crisp-button[@click.delegate='addAdjustment()']//button[@class='au-target waves-effect waves-light btn-flat']")]
        public IWebElement AdjustmentButton;

        [FindsBy(How = How.XPath, Using = "//crisp-list[@list-title='Fee Adjustments']//ul[@ref='theList']//li[@class='au-target collection-item']//div[@class='title']")]
        public IList<IWebElement> FeeAdjustmentList;

        [FindsBy(How = How.XPath, Using = "//div[@class='modal-content']//crisp-input-date[@label='Start Date']//span[@class='error']")]
        public IWebElement TermErrorMessage;

        [FindsBy(How = How.XPath, Using = "//crisp-button[@click.delegate='cancelTerm()']//button[@class='au-target waves-effect waves-light btn-flat']")]
        public IWebElement CancelTermButton;

        [FindsBy(How = How.XPath, Using = "//crisp-input-object//div//label[text()='Cancellation Reason']")]
        public IWebElement CancellationReasonLabel;

        [FindsBy(How = How.XPath, Using = "//div[@class='card-content']//crisp-list//ul[@ref='theList']//li[@class='au-target collection-item']")]
        public IList<IWebElement> CancellationReasonLabelList;
        
        [FindsBy(How = How.XPath, Using = "//crisp-input-textarea[@label='Cancellation Notes']//div//label[text()='Cancellation Notes']")]
        public IWebElement CancellationNotesLabel;

        [FindsBy(How = How.XPath, Using = "//crisp-input-textarea[@label='Cancellation Notes']//div//textarea")]
        public IWebElement CancellationNotesInput;


        //Role Section Elements 

        [FindsBy(How = How.XPath, Using = "//registration-view//crisp-tabs//ul[@ref='tabHeader']//li[@class='au-target tab']//span//a[contains(text(),'Roles')]")]
        public IWebElement RolesTab;

        [FindsBy(How = How.XPath, Using = "//div[@ref='tabContainer']//crisp-list[@list-title='Roles']//crisp-action-button[@click.delegate='addNewRole()']//a//i")]
        public IWebElement AddRolesButton;

        [FindsBy(How = How.XPath, Using = "//ai-dialog-container//crisp-dialog//div[contains(@class,'au-target modal dialog brand default')]//div[@class='nav-wrapper au-target']//div[text()='Add Role']")]
        public IWebElement AddRoleDiv;

        [FindsBy(How = How.XPath, Using = "//div[@class='modal-content']//crisp-picker[@label='Role']//div//input[@value='Correspondence Recipient']")]
        public IWebElement RoleDropdown;

        [FindsBy(How = How.XPath, Using = "//ul[@class='dropdown-content select-dropdown active']//li[@class='selected']//span[text()='Correspondence Recipient']")]
        public IWebElement RoleDropdownInput;

        [FindsBy(How = How.XPath, Using = "//div[@class='modal-content']//div//crisp-input-object//div[@class='au-target input-field']//label[text()='Person']")]
        public IWebElement PersonLabel;

        [FindsBy(How = How.XPath, Using = "//contact-list//crisp-list[@ref='listElm']//ul[@ref='theList']//li[@class='au-target collection-item']")]
        public IList<IWebElement> PersonInput;

        [FindsBy(How = How.XPath, Using = "//div//crisp-list[@list-title='Roles']//table//tbody[@class='collection']//tr[contains(@class,'au-target collection-item')]")]
        public IList<IWebElement> RolesList;

        [FindsBy(How = How.XPath, Using = "//div//crisp-list[@list-title='Roles']//table//tbody[@class='collection']//tr[contains(@class,'au-target collection-item')]//td//crisp-button[@icon='trash']")]
        public IList<IWebElement> RoleDeleteButton;

        [FindsBy(How = How.XPath, Using = "//crisp-button//button[@class='au-target waves-effect waves-light btn'][text()='OK']")]
        public IWebElement DeleteOkButton;

        [FindsBy(How = How.XPath, Using = "//crisp-header//div//crisp-header-actions/crisp-header-button[@click.delegate='performFileReview()']//button[@class='au-target waves-effect waves-light btn-flat']")]
        public IList<IWebElement> ViewInformationButton;

        //Accounts Section Elements 

        [FindsBy(How = How.XPath, Using = "//registration-view//crisp-tabs//ul[@ref='tabHeader']//li[@class='au-target tab']//span//a[contains(text(),'Account')]")]
        public IWebElement AccountTab;

        [FindsBy(How = How.XPath, Using = "//crisp-card-content//div[@class='card-content']//account-summary//dl//dt[text()='Fee balance']//following-sibling::dd[1]")]
        public IWebElement FeeBalance;

        [FindsBy(How = How.XPath, Using = "//crisp-card-content//div[@class='card-content']//account-summary//dl//dt[text()='Fee paid']//following-sibling::dd[1]")]
        public IWebElement FeePaid;

        [FindsBy(How = How.XPath, Using = "//crisp-card-content//div[@class='card-content']//account-summary//dl//dt[text()='Escrow balance']//following-sibling::dd[1]")]
        public IWebElement EscrowBalance;

        [FindsBy(How = How.XPath, Using = "//crisp-card-content//div[@class='card-content']//account-summary//dl//dt[text()='Escrow paid']//following-sibling::dd[1]")]
        public IWebElement EscrowPaid;

        [FindsBy(How = How.XPath, Using = "//crisp-card-content//div[@class='card-content']//crisp-button[@click.delegate='paymentToClient()']//button")]
        public IWebElement EnterPaymentButton;

        [FindsBy(How = How.XPath, Using = "//crisp-card-content//div[@class='card-content']//crisp-button[@click.delegate='transfer()']//button")]
        public IWebElement TransferMoneyButton;

        [FindsBy(How = How.XPath, Using = "//div[@class='card-title']//span[contains(text(),'From')]//crisp-button[@class='right au-target']//span//button")]
        public IWebElement FromButton;

        [FindsBy(How = How.XPath, Using = "//div[@class='card-title']//span[contains(text(),'To')]//crisp-button[@class='right au-target']//span//button")]
        public IWebElement ToButton;

        [FindsBy(How = How.XPath, Using = "//ul//li//crisp-list-item//a//crisp-list-item-details//account-fees//table//tbody//tr[2]//td[3]")]
        public IList<IWebElement> TransferAccountsList;

        [FindsBy(How = How.XPath, Using = "//ul//li//crisp-list-item//a//crisp-list-item-details//account-fees//table//tbody//tr[2]//td[3]")]
        public IList<IWebElement> TransferAccountsList2;

        [FindsBy(How = How.XPath, Using = "//ul//li//crisp-list-item//a//crisp-list-item-details//account-fees//table//tbody//tr[2]//td[3]")]
        public IList<IWebElement> TransferToAccountsList;

        [FindsBy(How = How.XPath, Using = "//div[@class='modal-content']//crisp-input-text[@label='Comment']/div/label")]
        public IWebElement TransferCommentLabel;

        [FindsBy(How = How.XPath, Using = "//div[@class='modal-content']//crisp-input-text[@label='Comment']/div/input")]
        public IWebElement TransferCommentInput;

        [FindsBy(How = How.XPath, Using = "//crisp-button[@click.delegate='transfer()']//span//button[text()='Transfer'][@class='au-target waves-effect waves-light btn-flat']")]
        public IWebElement TransferButton;

        [FindsBy(How = How.XPath, Using = "//crisp-footer//crisp-button//span//button[text()='OK'][@class='au-target waves-effect waves-light btn']")]
        public IWebElement TransferOkButton;

        [FindsBy(How = How.XPath, Using = "//div[@id='toast-container']//div[text()='The transfer is complete']")]
        public IWebElement TransferComplete;

        [FindsBy(How = How.XPath, Using = "//crisp-card-content//div[@class='card-content']//crisp-button[@click.delegate='refundToClient()']//button")]
        public IWebElement RefundButton;

        [FindsBy(How = How.XPath, Using = "//ai-dialog-container/div/div/crisp-dialog/div/crisp-header/nav/div")]
        public IWebElement MakePaymentDiv;

        [FindsBy(How = How.XPath, Using = "//div/crisp-dialog[@class='au-target']/div[@class='au-target modal dialog brand default modal-fixed-footer modal-has-header']")]
        public IWebElement TransferMoneyDiv;

        [FindsBy(How = How.XPath, Using = "//ai-dialog-container/div/div/crisp-dialog/div/crisp-header/nav/div")]
        public IWebElement RefundDiv;

        [FindsBy(How = How.XPath, Using = "//crisp-input-currency[@value.bind='fromEscrowAmount & validate']/div/label")]
        public IWebElement FromEscrowAmountLabel;

        [FindsBy(How = How.XPath, Using = "//crisp-input-currency[@value.bind='fromEscrowAmount & validate']/div/input")]
        public IWebElement FromEscrowAmountInput;

        [FindsBy(How = How.XPath, Using = "//crisp-input-currency[@value.bind='fromFeeAmount & validate']/div/label")]
        public IWebElement FromFeeAmountLabel;

        [FindsBy(How = How.XPath, Using = "//crisp-input-currency[@value.bind='fromFeeAmount & validate']/div/input")]
        public IWebElement FromFeeAmountInput;

        //Tasks Section Elements 

        [FindsBy(How = How.XPath, Using = "//registration-view//crisp-tabs//ul[@ref='tabHeader']//li[@class='au-target tab']//span//a[contains(text(),'Tasks')]")]
        public IWebElement TaskTab;

        [FindsBy(How = How.XPath, Using = "//div[@ref='tabContainer']//crisp-action-button[@click.delegate='addNewTask()']//a//i")]
        public IWebElement AddTaskButton;

        [FindsBy(How = How.XPath, Using = "//ai-dialog-container//crisp-dialog//div[contains(@class,'au-target modal dialog brand default')]//div[@class='nav-wrapper au-target']//crisp-header-title//div[text()='Create Task']")]
        public IWebElement CreateTaskDiv;

        [FindsBy(How = How.XPath, Using = "//div[@class='modal-content']//div//crisp-picker[@label='Task Type']//input")]
        public IWebElement TaskTypeDropdown;

        [FindsBy(How = How.XPath, Using = "//ul[@class='dropdown-content select-dropdown active']//li//span[text()='Quote']")]
        public IWebElement TaskTypeDropdownInput;

        [FindsBy(How = How.XPath, Using = "//div[@class='modal-content']//div//crisp-picker[@label='Task Sub-Type']//input")]
        public IWebElement TaskSubTypeDropdown;

        [FindsBy(How = How.XPath, Using = "//ul[@class='dropdown-content select-dropdown active']//li//span[text()='Input Quote application']")]
        public IWebElement TaskSubTypeDropdownInput;

        [FindsBy(How = How.XPath, Using = "//crisp-input-date-time//crisp-input-date//div//label[text()='Date/Time']")]
        public IWebElement DateLabel;

        [FindsBy(How = How.XPath, Using = "//crisp-input-date-time//crisp-input-time//div//input[@ref='inputText']")]
        public IWebElement TimeLabel;

        [FindsBy(How = How.XPath, Using = "//ul[@class='picker__list']//li[@class='picker__list-item']")]
        public IList<IWebElement> TimeList;

        [FindsBy(How = How.XPath, Using = "//crisp-input-bool[@label='High Priority']//label")]
        public IWebElement HighPriorityLabel;

        [FindsBy(How = How.XPath, Using = "//crisp-input-textarea[contains(@label,'Notes')]//label[text()='Notes']")]
        public IWebElement NotesLabel;

        [FindsBy(How = How.XPath, Using = "//crisp-input-textarea[contains(@label,'Notes')]//textarea")]
        public IWebElement NotesInput;

        [FindsBy(How = How.XPath, Using = "//task-list//div[@class='card-content']//crisp-list[@list-title='Tasks']//ul//li[contains(@class,'au-target collection-item')]")]
        public IList<IWebElement> TaskList;

        [FindsBy(How = How.XPath, Using = "//task-view//crisp-card-content//div[@class='card-content']//h3")]
        public IWebElement TaskName;

        [FindsBy(How = How.XPath, Using = "//task-view//crisp-card-actions//crisp-button[@click.delegate='editTask()']//button[@class='au-target waves-effect waves-light btn-flat']")]
        public IWebElement EditTaskButton;

        [FindsBy(How = How.XPath, Using = "//ai-dialog-container//crisp-dialog//div[contains(@class,'au-target modal dialog brand default')]//div[@class='nav-wrapper au-target']//crisp-header-title//div[text()='Amend Task']")]
        public IWebElement AmendTaskDiv;

        [FindsBy(How = How.XPath, Using = "//crisp-input-textarea[contains(@label,'Amendment')]//label[text()='Amendment Notes']")]
        public IWebElement AmendNotesLabel;

        [FindsBy(How = How.XPath, Using = "//crisp-input-textarea[contains(@label,'Amendment')]//textarea")]
        public IWebElement AmendNotesInput;

        [FindsBy(How = How.XPath, Using = "//ul[@class='dropdown-content select-dropdown active']//li//span[text()='Enter New Quote Application']")]
        public IWebElement AmendTaskSubTypeDropdownInput;

        [FindsBy(How = How.XPath, Using = "//task-view//crisp-card-actions//crisp-button[@click.delegate='completeTask()']//button[@class='au-target waves-effect waves-light btn-flat']")]
        public IWebElement CompleteTaskButton;

        [FindsBy(How = How.XPath, Using = "//ai-dialog-container//crisp-dialog//div[contains(@class,'au-target modal dialog brand default')]//div[@class='nav-wrapper au-target']//crisp-header-title//div[text()='Complete Task']")]
        public IWebElement CompleteTaskDiv;

        [FindsBy(How = How.XPath, Using = "//div[@class='modal-content']//crisp-picker[@label='Completion Reason']//div//input")]
        public IWebElement CompletionReasonLabel;

        [FindsBy(How = How.XPath, Using = "//ul[@class='dropdown-content select-dropdown active']//li//span[text()='Successful']")]
        public IWebElement CompletionReasonInput;

        [FindsBy(How = How.XPath, Using = "//div[@class='modal-content']//crisp-input-textarea[@label='Completion Notes']//div//label")]
        public IWebElement CompletionNotesLabel;

        [FindsBy(How = How.XPath, Using = "//div[@class='modal-content']//crisp-input-textarea[@label='Completion Notes']//div//textarea")]
        public IWebElement CompletionNotesInput;

        [FindsBy(How = How.XPath, Using = "//crisp-button[@click.delegate='complete()']//button[@class='au-target waves-effect waves-light btn-flat']")]
        public IWebElement CompleteButton;

        [FindsBy(How = How.XPath, Using = "//task-view//crisp-card-actions//crisp-button[@click.delegate='cloneTask()']//button[@class='au-target waves-effect waves-light btn-flat']")]
        public IWebElement CloneTaskButton;

        [FindsBy(How = How.XPath, Using = "//ai-dialog-container//crisp-dialog//div[contains(@class,'au-target modal dialog brand default')]//div[@class='nav-wrapper au-target']//crisp-header-title//div[text()='Clone Task']")]
        public IWebElement CloneTaskDiv;

        [FindsBy(How = How.XPath, Using = "//crisp-card-actions//crisp-button[@click.delegate='showFilter()']//button[@class='au-target waves-effect waves-light btn-flat']")]
        public IWebElement ShowFilterButton;

        [FindsBy(How = How.XPath, Using = "//task-view//crisp-card-actions//crisp-button[@click.delegate='reopenTask()']//button[@class='au-target waves-effect waves-light btn-flat']")]
        public IList<IWebElement> ReopenTaskButton;

        [FindsBy(How = How.XPath, Using = "//ai-dialog-container//crisp-dialog//div[contains(@class,'au-target modal dialog brand default')]//div[@class='nav-wrapper au-target']//crisp-header-title//div[text()='Reopen Task']")]
        public IWebElement ReopenTaskDiv;

        [FindsBy(How = How.XPath, Using = "//div[@class='modal-content']//crisp-input-textarea[@label='Comments']//div//label")]
        public IWebElement ReopenTaskCommentLabel;

        [FindsBy(How = How.XPath, Using = "//div[@class='modal-content']//crisp-input-textarea[@label='Comments']//div//textarea")]
        public IWebElement ReopenTaskCommentInput;

        [FindsBy(How = How.XPath, Using = "//crisp-button[@click.delegate='reopen()']//button[@class='au-target waves-effect waves-light btn-flat']")]
        public IWebElement ReopenButton;

        [FindsBy(How = How.XPath, Using = "//crisp-card-content//div//crisp-input-object//div//span[@ref='inputContainer']//span")]
        public IList<IWebElement> FilterTypes;

        [FindsBy(How = How.XPath, Using = "//crisp-card-content//div//crisp-input-object//div//label[text()='Task Sub-Types']")]
        public IWebElement FilterSubTaskTypes;

        [FindsBy(How = How.XPath, Using = "//crisp-card-content//div[@class='card-content']//crisp-list[@class='au-target no-stripe']//ul[@ref='theList']//li[@class='au-target collection-item']")]
        public IList<IWebElement> ListTypes;

        [FindsBy(How = How.XPath, Using = "//crisp-card//crisp-card-actions//crisp-button[@click.delegate='doFilter()']//button[@class='au-target waves-effect waves-light btn-flat']")]
        public IWebElement DoFilterButton;

        [FindsBy(How = How.XPath, Using = "//crisp-card-content//div//crisp-input-bool[@value.two-way='includeClosed']//label")]
        public IWebElement IncludeClosed;

        //Corresponde Section Elements 

        [FindsBy(How = How.XPath, Using = "//registration-view//crisp-tabs//ul[@ref='tabHeader']//li[@class='au-target tab']//span//a[contains(text(),'Correspondence')]")]
        public IWebElement CorrespondenceTab;

        [FindsBy(How = How.XPath, Using = "//div[@ref='tabContainer']//crisp-action-button[@click.delegate='newBlankEmail()']//a//i")]
        public IWebElement AddCorrespondenceButton;

        [FindsBy(How = How.XPath, Using = "//ai-dialog-container//crisp-dialog//div[contains(@class,'au-target modal dialog brand')]//div[@class='modal-content']//correspondence-embed//div[@class='card-content']")]
        public IWebElement CorrespondenceDiv;

        [FindsBy(How = How.XPath, Using = "//correspondence-embed//crisp-card-content//correspondence-email//crisp-input-object//label[text()='Mailbox']")]
        public IWebElement MailBoxLabel;

        [FindsBy(How = How.XPath, Using = "//crisp-card-content//div[@class='card-content']//crisp-list[@class='au-target no-stripe']//ul[@ref='theList']//li[@class='au-target collection-item']")]
        public IList<IWebElement> MailBoxInput;

        [FindsBy(How = How.XPath, Using = "//correspondence-email//crisp-button[contains(@click.delegate,'selectEmailTo()')]//button")]
        public IWebElement EmailTo;

        [FindsBy(How = How.XPath, Using = "//contact-list//ul[@ref='theList']//li[@class='au-target collection-item']")]
        public IList<IWebElement> ForwardEmailList;        

        [FindsBy(How = How.XPath, Using = "//correspondence-embed//crisp-card-content//div//correspondence-email//crisp-input-object[@icon='file']//span[@ref='inputContainer']")]
        public IWebElement AttachmentLabel;

        [FindsBy(How = How.XPath, Using = "//div[@class='modal-footer']//crisp-footer-button//button[text()='Send'][@class='au-target waves-effect waves-light btn-flat']")]
        public IWebElement SendButton;

        [FindsBy(How = How.XPath, Using = "//crisp-button//button[@class='au-target waves-effect waves-light btn'][text()='OK']")]
        public IWebElement SendEmailOkButton;

        [FindsBy(How = How.XPath, Using = "//email-inbox-list//crisp-card//crisp-card-content//crisp-list[@title='Email Correspondence']//span[@click.delegate='refresh()']")]
        public IWebElement EmailRefreshButon;

        [FindsBy(How = How.XPath, Using = "//email-inbox-list//crisp-card//crisp-card-content//crisp-list[@title='Email Correspondence']//ul//li[contains(@class,'au-target collection-item')]//crisp-list-item//a")]
        public IList<IWebElement> CorrespondenceList;

        [FindsBy(How = How.XPath, Using = "//emails-inbox//view-email//crisp-card-actions//crisp-button[@click.delegate='showEmailActions()']//button")]
        public IWebElement ViewActionButon;

        [FindsBy(How = How.XPath, Using = "//crisp-card-content//crisp-list[@list-title='Email Actions']")]
        public IWebElement EmailActionCard;

        [FindsBy(How = How.XPath, Using = "//crisp-card-content//crisp-list[@list-title='Email Actions']//ul//li[@class='au-target collection-item']//a")]
        public IList<IWebElement> EmailActionList;
        
        [FindsBy(How = How.XPath, Using = "//crisp-card-content//crisp-list[@list-title='Email Actions']//ul//li[@class='au-target collection-item']//a//crisp-list-title[contains(text(),'Type of Action')]")]
        public IList<IWebElement> TypeofActionList;

        [FindsBy(How = How.XPath, Using = "//emails-inbox//view-email//crisp-card-actions//crisp-button[@click.delegate='doReply()']//button")]
        public IWebElement ReplayButon;

        [FindsBy(How = How.XPath, Using = "//emails-inbox//view-email//crisp-card-actions//crisp-button[@click.delegate='doReplyToAll()']//button")]
        public IWebElement ReplayAllButon;

        [FindsBy(How = How.XPath, Using = "//emails-inbox//view-email//crisp-card-actions//crisp-button[@click.delegate='doForward()']//button")]
        public IWebElement ForwardButon;

        [FindsBy(How = How.XPath, Using = "//emails-inbox//view-email//crisp-card-actions//crisp-button[@click.delegate='indexAttachments()']//button")]
        public IWebElement IndexAttachmentsButon;

        [FindsBy(How = How.XPath, Using = "//crisp-card//div//span[text()='Index Attachments']")]
        public IWebElement IndexAttachmentCard;

        [FindsBy(How = How.XPath, Using = "//crisp-card-content//div[@class='card-content']//view-email-attachments//table//tbody//tr")]
        public IList<IWebElement> AttachmentsList;

        [FindsBy(How = How.XPath, Using = "//crisp-card-content//div[@class='card-content']//view-email-attachments//table//tbody//tr//crisp-input-bool//label")]
        public IWebElement EmailAttachmentsLabel;

        [FindsBy(How = How.XPath, Using = "//crisp-card//crisp-card-content//div[@class='card-content']//view-document-details")]
        public IWebElement AttachmentDetailsCard;

        [FindsBy(How = How.XPath, Using = "//crisp-button[@click.delegate='save()']//button[@class='au-target waves-effect waves-light btn']")]
        public IWebElement AttachmentSaveButton;        

        //Files and Documents Section

        [FindsBy(How = How.XPath, Using = "//registration-view//crisp-tabs//ul[@ref='tabHeader']//li[@class='au-target tab']//span//a[contains(text(),'Files & Documents')]")]
        public IWebElement FilesDocsTab;

        [FindsBy(How = How.XPath, Using = "//registration-view//files-documents//view-folders//folders//crisp-list//ul//li[@class='au-target collection-item']//crisp-list-item//crisp-list-item-details//span")]
        public IList<IWebElement> DefaultFolders;

        [FindsBy(How = How.XPath, Using = "//registration-view//files-documents//crisp-header-button[@btn-title='Add folder']//button")]
        public IWebElement CreateFolderButton;

        [FindsBy(How = How.XPath, Using = "//view-folders//crisp-card//crisp-card-content//crisp-input-text[@label='Name']//label")]
        public IWebElement FolderLabel;

        [FindsBy(How = How.XPath, Using = "//view-folders//crisp-card//crisp-card-content//crisp-input-text[@label='Name']//input")]
        public IWebElement FolderInput;

        [FindsBy(How = How.XPath, Using = "//view-folders//crisp-card//crisp-card-content//crisp-button[@click.delegate='addFolder()']//button[@class='au-target waves-effect waves-light btn-flat']")]
        public IWebElement AddFolderButton;

        [FindsBy(How = How.XPath, Using = "//upload-documents-embed//crisp-input-file//div//div//input[@ref='fileInput']")]
        public IWebElement FileInput;

        [FindsBy(How = How.XPath, Using = "//table[@class='striped']//crisp-input-date/div/label[text()='Date Received']")]
        public IWebElement DateReceivedLabel;

        [FindsBy(How = How.XPath, Using = "//div[contains(@class,'picker--opened')]/div/div/div/div/div[@class='picker__footer']/button[text()='Today']")]
        public IWebElement DateReceivedInput;

        [FindsBy(How = How.XPath, Using = "//tr//td//crisp-input-object[@class='au-target']//div[contains(@class,'au-target input-field')]//span[@ref='inputContainer']")]
        public IWebElement DocTypeDropdown1;

        [FindsBy(How = How.XPath, Using = "//crisp-card-content[@class='au-target']//div[@class='au-target list-title has-filter']//span[@class='filter-input-field']//input[@class='au-target']")]
        public IWebElement SearchDocumentType;

        [FindsBy(How = How.XPath, Using = "//ul[@class='au-target collection has-header has-filter']//li[@class='au-target collection-item']//crisp-list-item")]
        public IList<IWebElement> SearchDocumentTypeInputList;

        [FindsBy(How = How.XPath, Using = "//tr//td//crisp-input-object[@class='au-target']//div[@class='au-target input-field']//span[@ref='inputContainer']")]
        public IWebElement DocTypeDropdown;

        [FindsBy(How = How.XPath, Using = "//ul[@class='dropdown-content select-dropdown active']//li/span[text()='Initial Notice']")]
        public IWebElement DocTypeDropdownInput;

        [FindsBy(How = How.XPath, Using = "//registration-view//files-documents//crisp-header-button[@click.delegate='toggleUpload()']//button")]
        public IWebElement UploadDocButton;

        [FindsBy(How = How.XPath, Using = "//view-documents//crisp-list[@list-title='Document']//div//table//tbody//tr[@class='au-target collection-item']")]
        public IList<IWebElement> DocsList;

        [FindsBy(How = How.XPath, Using = "//files-documents//view-folders//folders//crisp-breadcrumb[@class='custom-content au-target']//a[text()='Home']")]
        public IWebElement HomeFolder;

        //Notes Elements 

        [FindsBy(How = How.XPath, Using = "//registration-view//crisp-tabs//ul[@ref='tabHeader']//li[@class='au-target tab']//span//a[contains(text(),'Notes')]")]
        public IWebElement NotesTab;

        [FindsBy(How = How.XPath, Using = "//div[@ref='tabContainer']//crisp-action-button[@click.delegate='addNote()']//a//i")]
        public IWebElement AddNotesButton;

        [FindsBy(How = How.XPath, Using = "//ai-dialog-container//crisp-dialog//div[contains(@class,'au-target modal dialog brand default')]//div[@class='nav-wrapper au-target'][text()='Add Note']")]
        public IWebElement CreateNotesDiv;

        [FindsBy(How = How.XPath, Using = "//div[@class='modal-content']//crisp-picker[@label='Note Type']//input[@class='select-dropdown']")]
        public IWebElement NoteTypeDropdown;

        [FindsBy(How = How.XPath, Using = "//ul[@class='dropdown-content select-dropdown active']//li//span[text()='General Note']")]
        public IWebElement NoteTypeDropdownInput;

        [FindsBy(How = How.XPath, Using = "//crisp-input-html[@id='noteDetails']//div")]
        public IWebElement NotesDetailsInput;

        [FindsBy(How = How.XPath, Using = "//div[@class='nav-wrapper au-target'][text()='Create Task']")]
        public IWebElement CreateTaskDivFromNotes;

        [FindsBy(How = How.XPath, Using = "//crisp-button[@click.delegate='buttonClick(btn)']//button[@class='au-target waves-effect waves-light btn'][text()='Yes']")]
        public IWebElement YesButton;

        [FindsBy(How = How.XPath, Using = "//crisp-button[@click.delegate='buttonClick(btn)']//button[@class='au-target waves-effect waves-light btn'][text()='No']")]
        public IWebElement NoButton;

        [FindsBy(How = How.XPath, Using = "//crisp-list[@list-title='Notes']//ul[@ref='theList']//li[@class='au-target collection-item']")]
        public IList<IWebElement> NotesList;

        [FindsBy(How = How.XPath, Using = "//notes//div[@class='card-title']//span")]
        public IWebElement NoteName;

        [FindsBy(How = How.XPath, Using = "//notes//crisp-card-actions//crisp-button[@icon='trash']//button[text()='Delete']")]
        public IWebElement NoteDeleteButton;

        [FindsBy(How = How.XPath, Using = "//crisp-dialog//div//crisp-header//div[text()='Delete Note']")]
        public IWebElement DeleteNoteDiv;

        [FindsBy(How = How.XPath, Using = "//div[@class='modal-footer']//crisp-button//button[text()='Yes']")]
        public IWebElement DeleteYesButton;

        [FindsBy(How = How.XPath, Using = "//notes//crisp-card-actions//crisp-button//button[text()='Associated Task']")]
        public IList<IWebElement> AssociatedTaskButton;

        //Previous Year
        static DateTime expdate = DateTime.Today.AddYears(-1); // will give the date for today
        private string expirydate = expdate.ToString("dd/MM/yyyy HH:mm");

        //current year 
        static DateTime currentDate = DateTime.Today.AddYears(+1).AddDays(-1); // will give the date for today
        private string endDate = currentDate.ToString("dd/MM/yyyy");

        //current year 
        static DateTime prospectiveEndDate = DateTime.Today.AddYears(+2).AddDays(-1); // will give the date for today
        private string prospectiveEndDateInput = prospectiveEndDate.ToString("dd/MM/yyyy");

        //FutureData
        static DateTime date = DateTime.Today.AddDays(+1); // will give the date for today
        private string FutureDate = date.ToString("dd/MM/yyyy");

        public string DocumentTypeNames;

        public static int documentTypes { get; set; }

        public static int SecurityDocumentCount { get; set; }
       

        //Verify the Operations on Overview Section 
        public void OverviewSection()
        {
            WaitForElement(OverviewTab);
            OverviewTab.Click();
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(1000);

            //Verify the Sandered Fees Button operation
            WaitForElement(StandardFeesStatus);

            WaitForElement(StandardFeesButton);
            var status = StandardFeesStatus.Text;
            if (status == "Yes")
            {
                WaitForElement(StandardFeesButton);
                StandardFeesButton.Click();
                Thread.Sleep(500);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                Thread.Sleep(500);
                WaitForElement(StandardFeesStatus);
                status = StandardFeesStatus.Text;
                Assert.IsTrue(status == "No");
            
            }
            status = StandardFeesStatus.Text;
            if (status == "No")
            {
                WaitForElement(StandardFeesButton);
                StandardFeesButton.Click();
                Thread.Sleep(500);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                Thread.Sleep(500);
                WaitForElement(StandardFeesStatus);
                status = StandardFeesStatus.Text;               
                Assert.IsTrue(status == "Yes");
            }
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(1000);

            //Verify the Send Receipt Automation Button operation and verify no reciept has been sent out 
            WaitForElement(SendAutomationReceiptStatus);
            var AutomationReceiptResult = SendAutomationReceiptStatus.Text;
            WaitForElement(SendReceiptAutomationButton);

            if (SendAutomationReceiptStatus.Text == "Enabled")
            {
                WaitForElement(SendReceiptAutomationButton);
                SendReceiptAutomationButton.Click();
                CloseSpinneronDiv();
                CloseSpinneronPage();
                Thread.Sleep(1000);
                WaitForElement(SendReceiptAutomationDiv);
                Assert.IsTrue(SendReceiptAutomationDiv.Displayed);
                Assert.IsTrue(SendReceiptAutomationMessage.Text.Contains("Automatically send receipt once an external payment is received is currently enabled"));
                Assert.IsTrue(SendReceiptAutomationMessage.Text.Contains("Are you sure you want to disable automation?"));

                WaitForElement(SendReceiptAutomationConfirmButton);
                SendReceiptAutomationConfirmButton.Click();
                Thread.Sleep(1000);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                Thread.Sleep(1000);
                WaitForElement(SendAutomationReceiptStatus);
                Assert.IsTrue(SendAutomationReceiptStatus.Text == "Disabled");
            }
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(1000);
            if (SendAutomationReceiptStatus.Text == "Disabled")
            {
                WaitForElement(SendReceiptAutomationButton);
                SendReceiptAutomationButton.Click();
                CloseSpinneronDiv();
                CloseSpinneronPage();
                Thread.Sleep(1000);
                WaitForElement(SendReceiptAutomationDiv);
                Assert.IsTrue(SendReceiptAutomationDiv.Displayed);
                Assert.IsTrue(SendReceiptAutomationMessage.Text.Contains("Automatically send receipt once an external payment is received is currently disabled"));
                Assert.IsTrue(SendReceiptAutomationMessage.Text.Contains("Are you sure you want to enable automation?"));
                WaitForElement(SendReceiptAutomationConfirmButton);
                SendReceiptAutomationConfirmButton.Click();
                CloseSpinneronDiv();
                CloseSpinneronPage();
                Thread.Sleep(1000);
                WaitForElement(SendAutomationReceiptStatus);
                Assert.IsTrue(SendAutomationReceiptStatus.Text == "Enabled");
            }
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(1000);
        }
        //Verify the Operations on RegDocuments Section 
        public void RegDocumentsSection()
        {
            AddSecurityDocs();
            UploadSecurityDocument();
            EditSecurityDocument();
        }
        // Adding Security Documents 
        public void AddSecurityDocs()
        {
            WaitForElement(RegDocumentTab);
            RegDocumentTab.Click();
            Thread.Sleep(1000);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(1000);
            WaitForElements(AddSecurityDocButton);
            for (int i = 0; i < AddSecurityDocButton.Count; i++)
            {
                Thread.Sleep(500);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                WaitForElement(AddSecurityDocButton[i]);
                WaitForElementToClick(AddSecurityDocButton[i]);
                Thread.Sleep(500);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                AddSecurityDocButton[i].Click();
                Thread.Sleep(500);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                Thread.Sleep(500);
                WaitForElement(AddDocsDiv);
                Assert.IsTrue(AddDocsDiv.Displayed);
                Thread.Sleep(500);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                Thread.Sleep(500);
                WaitForElement(DocumentTypeDropdown);
                DocumentTypeDropdown.Click();
                WaitForElements(DocumentTypeDropdownInput);
                documentTypes = DocumentTypeDropdownInput.Count;
                for (int d = 0; d < documentTypes; d++)
                {
                    Thread.Sleep(500);
                    CloseSpinneronDiv();
                    CloseSpinneronPage();
                    Thread.Sleep(1000);
                    if (d != 0)
                    {
                        Thread.Sleep(500);
                        CloseSpinneronDiv();
                        CloseSpinneronPage();
                        WaitForElement(AddSecurityDocButton[i]);
                        WaitForElementToClick(AddSecurityDocButton[i]);
                        Thread.Sleep(500);
                        CloseSpinneronDiv();
                        CloseSpinneronPage();
                        AddSecurityDocButton[i].Click();
                        Thread.Sleep(500);
                        CloseSpinneronDiv();
                        CloseSpinneronPage();
                        Thread.Sleep(500);
                        WaitForElement(AddDocsDiv);
                        Assert.IsTrue(AddDocsDiv.Displayed);
                        Thread.Sleep(500);
                        CloseSpinneronDiv();
                        CloseSpinneronPage();
                        Thread.Sleep(500);
                        WaitForElement(DocumentTypeDropdown);
                        DocumentTypeDropdown.Click();
                        WaitForElements(DocumentTypeDropdownInput);
                    }
                    DocumentTypeNames = DocumentTypeDropdownInput.ElementAt(d).Text;

                    Thread.Sleep(500);
                    CloseSpinneronDiv();
                    CloseSpinneronPage();
                    var item = $"//ul[@class='dropdown-content select-dropdown active']//li[{d + 1}]//span";
                    Thread.Sleep(1500);
                    Driver.FindElement(By.XPath(item)).Click();                   
                    Thread.Sleep(500);
                    CloseSpinneronDiv();
                    CloseSpinneronPage();
                    Thread.Sleep(500);
                    WaitForElement(IsBespoke);
                    IsBespoke.Click();
                    Thread.Sleep(500);
                    CloseSpinneronDiv();
                    CloseSpinneronPage();
                    Thread.Sleep(500);
                    WaitForElement(UploadFileLabel);
                    UploadFileLabel.Click();
                    Thread.Sleep(500);
                    CloseSpinneronDiv();
                    CloseSpinneronPage();
                    Thread.Sleep(500);
                    UploadAmendedVersionFile();
                    Thread.Sleep(500);
                    CloseSpinneronDiv();
                    CloseSpinneronPage();
                    Thread.Sleep(500);
                    if (DocumentTypeNames == "CIA")
                    {
                        WaitForElement(DocVersionLabel);
                        DocVersionLabel.Click();
                        Thread.Sleep(500);
                        CloseSpinneronDiv();
                        CloseSpinneronPage();
                        Thread.Sleep(500);
                        WaitForElements(DocVersionDropdownInput);
                        DocVersionDropdownInput[0].Click();
                        Thread.Sleep(500);
                        CloseSpinneronDiv();
                        CloseSpinneronPage();
                        Thread.Sleep(500);
                    }
                   
                    if (DocumentTypeNames == "CIA - HVS")
                    {
                        DocumentTypeNames = DocumentTypeNames.Replace("CIA - HVS", "CIA");
                        Thread.Sleep(500);
                        CloseSpinneronDiv();
                        CloseSpinneronPage();
                        Thread.Sleep(500);
                    }

                    if (DocumentTypeNames == "ACIA - HVS")
                    {
                        DocumentTypeNames = DocumentTypeNames.Replace("ACIA - HVS", "ACIA");
                        Thread.Sleep(500);
                        CloseSpinneronDiv();
                        CloseSpinneronPage();
                        Thread.Sleep(500);
                    }
                    Thread.Sleep(500);
                    CloseSpinneronDiv();
                    CloseSpinneronPage();
                    Thread.Sleep(500);
                    if (DocumentTypeNames == "PCG" || DocumentTypeNames == "CCG")
                    {
                        Thread.Sleep(500);
                        CloseSpinneronDiv();
                        CloseSpinneronPage();
                        Thread.Sleep(500);
                    }
                    //else
                    //{
                    //    Thread.Sleep(500);
                    //    CloseSpinneronDiv();
                    //    CloseSpinneronPage();
                    //    Thread.Sleep(500);
                    //    WaitForElement(CoverDoclabel);
                    //    WaitForElementToClick(CoverDoclabel);
                    //    CloseSpinneronDiv();
                    //    CloseSpinneronPage();                       
                    //    CoverDoclabel.Click();
                    //    Thread.Sleep(500);
                    //    CloseSpinneronDiv();
                    //    CloseSpinneronPage();
                    //    Thread.Sleep(500);
                    //    WaitForElement(ChooseCoverDocLabel);
                    //    ChooseCoverDocLabel.Click();
                    //    ChooseCoverDocLabel.Clear();
                    //    ChooseCoverDocLabel.SendKeys(DocumentTypeNames);
                    //    Thread.Sleep(500);
                    //    CloseSpinneronDiv();
                    //    CloseSpinneronPage();
                    //    Thread.Sleep(500);                       
                    //    if (CoverDocsList.Count > 0)
                    //    {
                    //        WaitForElements(CoverDocsList);
                    //        CoverDocsList[0].Click();
                    //        Thread.Sleep(500);
                    //        CloseSpinneronDiv();
                    //        CloseSpinneronPage();
                    //        Thread.Sleep(500);
                    //    }
                    //    WaitForElement(SelectButton);
                    //    SelectButton.Click();
                    //    Thread.Sleep(500);
                    //    CloseSpinneronDiv();
                    //    CloseSpinneronPage();
                    //    Thread.Sleep(500);
                    //}


                    Thread.Sleep(500);
                    CloseSpinneronDiv();
                    CloseSpinneronPage();
                    Thread.Sleep(500);
                    if (ChildrenLabel.Count > 0)
                    {
                        CloseSpinneronDiv();
                        CloseSpinneronPage();
                        Thread.Sleep(500);
                        WaitForElement(ChildrenLabel[0]);
                        CloseSpinneronDiv();
                        CloseSpinneronPage();
                        Thread.Sleep(500);
                        ChildrenLabel[0].Click();
                        Thread.Sleep(500);
                        CloseSpinneronDiv();
                        CloseSpinneronPage();
                        Thread.Sleep(500);
                        if (ChildrenList.Count > 0)
                        {
                            CloseSpinneronDiv();
                            CloseSpinneronPage();
                            Thread.Sleep(500);
                            WaitForElement(ChildrenList[0]);
                            CloseSpinneronDiv();
                            CloseSpinneronPage();
                            Thread.Sleep(500);
                            ChildrenList[0].Click();
                            Thread.Sleep(500);
                            CloseSpinneronDiv();
                            CloseSpinneronPage();
                            Thread.Sleep(500);
                        }                      
                        CloseSpinneronDiv();
                        CloseSpinneronPage();
                        Thread.Sleep(500);
                        WaitForElement(SelectButton);
                        SelectButton.Click();
                        Thread.Sleep(500);
                        CloseSpinneronDiv();
                        CloseSpinneronPage();
                        Thread.Sleep(500);
                    }
                    Thread.Sleep(500);
                    CloseSpinneronDiv();
                    CloseSpinneronPage();
                    Thread.Sleep(500);
                    WaitForElement(OkButton);
                    WaitForElementToClick(OkButton);
                    OkButton.Click();
                    Thread.Sleep(1000);
                    CloseSpinneronDiv();
                    CloseSpinneronPage();
                    Thread.Sleep(1000);

                }
            }

        }
        

        //Uploading Reg Docs 
        //Upload document method 
        public void UploadSecurityDocument()
        {
            Thread.Sleep(1000);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            CloseSpinneronRegPage();
            Thread.Sleep(1000);
            WaitForElement(RegDocumentTab);
            WaitForElementToClick(RegDocumentTab);
            RegDocumentTab.Click();
            Thread.Sleep(1000);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            WaitForElements(SecurityDocsList);
            Thread.Sleep(1000);
            if (SecurityDocsList.Count > 0)
            {
                SecurityDocumentCount = SecurityDocsList.Count;
                //Verify Review Status for each security Documents           
                for (int i = 0; i < SecurityDocumentCount; i++)
                {
                    Thread.Sleep(1000);
                    CloseSpinneronDiv();
                    CloseSpinneronPage();
                    Thread.Sleep(1000);
                    SecurityDocsList[i].Click();
                    Thread.Sleep(1000);
                    CloseSpinneronDiv();
                    CloseSpinneronPage();
                    Thread.Sleep(1000);
                    WaitForElement(ReviewStatusValue);
                    Thread.Sleep(1000);
                    CloseSpinneronDiv();
                    CloseSpinneronPage();
                    Thread.Sleep(1000);
                   // Assert.IsTrue(ReviewStatusValue.Text.Contains("None"));
                    Thread.Sleep(1000);
                    CloseSpinneronDiv();
                    CloseSpinneronPage();
                    Thread.Sleep(1000);
                    WaitForElement(ISBespokeValue);
                    Thread.Sleep(1000);
                    CloseSpinneronDiv();
                    CloseSpinneronPage();
                    Thread.Sleep(1000);
                    Assert.IsTrue(ISBespokeValue.Text.Contains("Yes"));
                    WaitForElement(UploadButton);
                    UploadButton.Click();
                    WaitForElement(UploadDiv);
                    Assert.IsTrue(UploadDiv.Displayed);
                    Thread.Sleep(1000);
                    CloseSpinneronDiv();
                    WaitForElement(RelatedFiles);
                    RelatedFiles.Click();
                    Thread.Sleep(1000);
                    UploadAmendedVersionFile();                   
                    Thread.Sleep(1000);
                    CloseSpinneronDiv();
                    WaitForElement(DateOnUploadDiv);
                    DateOnUploadDiv.Click();
                    Thread.Sleep(1000);
                    WaitForElement(DateReceivedInput);
                    DateReceivedInput.Click();
                    Thread.Sleep(1000);
                    WaitForElement(OkButton);
                    OkButton.Click();
                    Thread.Sleep(1000);
                    CloseSpinneronDiv();
                    CloseSpinneronPage();
                    Thread.Sleep(1000);
                    WaitForElement(CancelButton);
                    CancelButton.Click();
                    Thread.Sleep(1000);
                    CloseSpinneronPage();
                    CloseSpinneronDiv();
                    Thread.Sleep(1000);
                }
            }
           
        }
        //Verify Review Status on Security Documents      
        public void VerifyReviewStatusSecurityDocuments()
        {
            VerifyRejectReviewStatus();
            VerifyConfirmReviewStatus();
        }

        public void VerifyRejectReviewStatus()
        {
            Thread.Sleep(1000);
            CloseSpinneronPage();
            CloseSpinneronDiv();
            Thread.Sleep(1000);
            WaitForElement(RegDocumentTab);
            RegDocumentTab.Click();
            Thread.Sleep(1000);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            WaitForElements(SecurityDocsList);
            Thread.Sleep(1000);
            if (SecurityDocsList.Count > 0)
            {
                WaitForElements(SecurityDocsListReviewStatus);
                foreach (var eachSecurityDocReviewStatus in SecurityDocsListReviewStatus)
                {
                    //Assert.IsTrue(eachSecurityDocReviewStatus.Text.Contains("Awaiting Review"));

                }
                SecurityDocumentCount = SecurityDocsList.Count;
                for (int i = 0; i < SecurityDocumentCount; i++)
                {
                    Thread.Sleep(500);
                    CloseSpinneronDiv();
                    CloseSpinneronPage();
                    Thread.Sleep(500);
                    SecurityDocsList[i].Click();
                    Thread.Sleep(500);
                    CloseSpinneronDiv();
                    CloseSpinneronPage();
                    Thread.Sleep(500);
                    WaitForElement(ReviewStatusValue);
                    Thread.Sleep(500);
                    CloseSpinneronDiv();
                    CloseSpinneronPage();
                    Thread.Sleep(500);
                    Assert.IsTrue(ReviewStatusValue.Text.Contains("Awaiting Review"));
                    CloseSpinneronDiv();
                    Thread.Sleep(500);
                    WaitForElement(ReviewButton);
                    ReviewButton.Click();
                    Thread.Sleep(500);
                    WaitForElement(ReviewDiv);
                    Assert.IsTrue(ReviewDiv.Displayed);
                    WaitForElement(ReviewStatusDropdown);
                    ReviewStatusDropdown.Click();
                    Thread.Sleep(500);
                    WaitForElement(RejectsStatusDropdownInput);
                    RejectsStatusDropdownInput.Click();
                    Thread.Sleep(500);
                    CloseSpinneronDiv();
                    CloseSpinneronPage();
                    Thread.Sleep(500);
                    WaitForElement(RejectReasonDropdown);
                    RejectReasonDropdown.Click();
                    Thread.Sleep(500);
                    CloseSpinneronDiv();
                    CloseSpinneronPage();
                    Thread.Sleep(500);
                    WaitForElement(RejectReasonDropdownInput);
                    RejectReasonDropdownInput.Click();
                    Thread.Sleep(500);
                    CloseSpinneronDiv();
                    CloseSpinneronPage();
                    Thread.Sleep(500);
                    WaitForElement(RejectReasonNotes);
                    RejectReasonNotes.Click();
                    WaitForElement(RejectReasonNotesInput);
                    RejectReasonNotesInput.SendKeys("TestRejectedStatus");
                    Thread.Sleep(500);
                    CloseSpinneronDiv();
                    CloseSpinneronPage();
                    Thread.Sleep(500);
                    WaitForElement(OkButton);
                    OkButton.Click();
                    Thread.Sleep(1000);
                    CloseSpinneronDiv();
                    CloseSpinneronPage();
                    Thread.Sleep(1000);
                    WaitForElement(ReviewStatusValue);
                    Thread.Sleep(500);
                    CloseSpinneronDiv();
                    CloseSpinneronPage();
                    Thread.Sleep(500);
                    Assert.IsTrue(ReviewStatusValue.Text.Contains("Rejected"));
                    Thread.Sleep(1000);
                    CloseSpinneronDiv();
                    CloseSpinneronPage();
                    Thread.Sleep(1000);
                    WaitForElement(RejectReasonValue);
                    Assert.IsTrue(RejectReasonValue.Text.Contains("Not signed correctly"));
                    Thread.Sleep(1000);
                    CloseSpinneronDiv();
                    CloseSpinneronPage();
                    Thread.Sleep(1000);
                    WaitForElement(CancelButton);
                    CancelButton.Click();
                    Thread.Sleep(1000);
                    CloseSpinneronDiv();
                    CloseSpinneronPage();
                    Thread.Sleep(1000);
                }
                //Verify Review Status for each security Documents
                WaitForElements(SecurityDocsListReviewStatus);
                foreach (var eachSecurityDocReviewStatus in SecurityDocsListReviewStatus)
                {
                    Assert.IsTrue(eachSecurityDocReviewStatus.Text.Contains("Rejected"));
                }
            }
        }
        //verify confirm Review Status 
        public void VerifyConfirmReviewStatus()
        {
            WaitForElement(RegDocumentTab);
            RegDocumentTab.Click();
            Thread.Sleep(1000);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            WaitForElements(SecurityDocsList);
            Thread.Sleep(1000);
            if (SecurityDocsList.Count > 0)
            {
                WaitForElements(SecurityDocsListReviewStatus);
                foreach (var eachSecurityDocReviewStatus in SecurityDocsListReviewStatus)
                {
                    Assert.IsTrue(eachSecurityDocReviewStatus.Text.Contains("Rejected"));

                }
                SecurityDocumentCount = SecurityDocsList.Count;
                for (int i = 0; i < SecurityDocumentCount; i++)
                {
                    Thread.Sleep(1000);
                    CloseSpinneronDiv();
                    CloseSpinneronPage();
                    Thread.Sleep(1000);
                    SecurityDocsList[i].Click();
                    Thread.Sleep(1000);
                    CloseSpinneronDiv();
                    CloseSpinneronPage();
                    Thread.Sleep(1000);
                    WaitForElement(ReviewStatusValue);
                    Thread.Sleep(1000);
                    CloseSpinneronDiv();
                    CloseSpinneronPage();
                    Thread.Sleep(1000);
                    Assert.IsTrue(ReviewStatusValue.Text.Contains("Rejected"));
                    CloseSpinneronDiv();
                    Thread.Sleep(1000);
                    WaitForElement(ReviewButton);
                    ReviewButton.Click();
                    Thread.Sleep(1000);
                    WaitForElement(ReviewDiv);
                    Assert.IsTrue(ReviewDiv.Displayed);
                    WaitForElement(ReviewStatusDropdown);
                    ReviewStatusDropdown.Click();
                    Thread.Sleep(500);
                    WaitForElement(ConfirmedStatusDropdownInput);
                    ConfirmedStatusDropdownInput.Click();
                    Thread.Sleep(500);
                    WaitForElement(OkButton);
                    OkButton.Click();
                    Thread.Sleep(1000);
                    CloseSpinneronDiv();
                    CloseSpinneronPage();
                    Thread.Sleep(1000);
                    WaitForElement(ReviewStatusValue);
                    Thread.Sleep(1000);
                    CloseSpinneronDiv();
                    CloseSpinneronPage();
                    Thread.Sleep(1000);
                    Assert.IsTrue(ReviewStatusValue.Text.Contains("Confirmed"));
                    Thread.Sleep(1000);
                    CloseSpinneronDiv();
                    CloseSpinneronPage();
                    Thread.Sleep(1000);
                    WaitForElement(CancelButton);
                    CancelButton.Click();
                    Thread.Sleep(1000);
                    CloseSpinneronDiv();
                    CloseSpinneronPage();
                    Thread.Sleep(1000);
                }
                //Verify Review Status for each security Documents
                WaitForElements(SecurityDocsListReviewStatus);
                foreach (var eachSecurityDocReviewStatus in SecurityDocsListReviewStatus)
                {
                    Assert.IsTrue(eachSecurityDocReviewStatus.Text.Contains("Confirmed"));
                }
            }
        }
      

            //Edit Security documents        
        public void EditSecurityDocument()
        {
            VerifyReviewStatusSecurityDocuments();
            WaitForElement(RegDocumentTab);
            RegDocumentTab.Click();
            Thread.Sleep(1000);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            WaitForElements(SecurityDocsList);
            Thread.Sleep(1000);
            if (SecurityDocsList.Count > 0)
            {
                SecurityDocumentCount = SecurityDocsList.Count;
                for (int i = 0; i < 6; i++)
                {
                    Thread.Sleep(1000);
                    CloseSpinneronDiv();
                    CloseSpinneronPage();
                    Thread.Sleep(1000);
                    SecurityDocsList[i].Click();
                    Thread.Sleep(1000);
                    CloseSpinneronDiv();
                    CloseSpinneronPage();
                    Thread.Sleep(1000);
                    WaitForElement(ReviewStatusValue);
                    Thread.Sleep(1000);
                    CloseSpinneronDiv();
                    CloseSpinneronPage();
                    Thread.Sleep(1000);
                    Assert.IsTrue(ReviewStatusValue.Text.Contains("Confirmed"));
                    Thread.Sleep(1000);
                    CloseSpinneronDiv();
                    CloseSpinneronPage();
                    Thread.Sleep(1000);                    
                    CloseSpinneronDiv();
                    CloseSpinneronPage();
                    Thread.Sleep(1000);
                    WaitForElement(ISBespokeValue);
                    Thread.Sleep(1000);
                    CloseSpinneronDiv();
                    CloseSpinneronPage();
                    Thread.Sleep(1000);
                    Assert.IsTrue(ISBespokeValue.Text.Contains("Yes"));
                    Thread.Sleep(1000);
                    CloseSpinneronDiv();
                    CloseSpinneronPage();
                    Thread.Sleep(1000);
                    WaitForElement(EditButton);
                    EditButton.Click();
                    WaitForElement(EditDiv);
                    Assert.IsTrue(EditDiv.Displayed);
                    Thread.Sleep(1000);
                    CloseSpinneronDiv();
                    WaitForElement(IsBespoke);
                    IsBespoke.Click();
                    Thread.Sleep(1000);
                    CloseSpinneronDiv();
                    Thread.Sleep(1000);
                    WaitForElement(OkButton);
                    OkButton.Click();
                    Thread.Sleep(1000);
                    CloseSpinneronDiv();
                    CloseSpinneronPage();
                    Thread.Sleep(1000);
                    WaitForElement(ISBespokeValue);
                    Thread.Sleep(1000);
                    CloseSpinneronDiv();
                    CloseSpinneronPage();
                    Thread.Sleep(1000);
                    Assert.IsTrue(ISBespokeValue.Text.Contains("No"));
                    Thread.Sleep(1000);
                    CloseSpinneronDiv();
                    CloseSpinneronPage();
                    Thread.Sleep(1000);                   
                    WaitForElement(CancelButton);
                    CancelButton.Click();
                    CloseSpinneronPage();
                    CloseSpinneronDiv();
                    Thread.Sleep(1000);
                }
            }
        }
        //Uploading New File with out index
        public void UploadAmendedVersionFile()
        {
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            WaitForElement(UploadNewFilesButton);
            UploadNewFilesButton.Click();
            Thread.Sleep(1000);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            var fileInfo = new FileInfo(ExtensionMethods.GetAbsolutePath(@"TestData\QuotesData\CRISP install guide.docx"));
            FileInput.SendKeys($"{fileInfo}");

           // FileInput.SendKeys(@"\\tpgfile\NewCompany\IT\Department\QA Team\Current Projects\CRISP\CrispAutomation\CrispData\QuotesData\CRISP install guide.docx");
            Thread.Sleep(1000);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(1000);
            //WaitForElement(DateReceivedLabel);
            //DateReceivedLabel.Click();
            
            //CloseSpinneronDiv();
            //CloseSpinneronPage();
            
            //WaitForElement(DateReceivedInput);
            //DateReceivedInput.Click();
            
            //CloseSpinneronDiv();
            //CloseSpinneronPage();
            
            WaitForElement(UploadFileSelectButton);
            UploadFileSelectButton.Click();
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
        }



        // Adding Additional Indemnities Documents 
        public void AddAdditionalIndemnitiesDocs()
        {
            WaitForElement(RegDocumentTab);
            RegDocumentTab.Click();
            Thread.Sleep(1000);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(1000);
            //WaitForElements();
            //AddSecurityDocs[0].Click();
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            WaitForElement(AddDocsDiv);
            Assert.IsTrue(AddDocsDiv.Displayed);
        }



        //Verify the Operations on Conditions Section 
        public void ConditionSection()
        {
            CreateCondition();
            EditCondition();
            CloseCondition();
            VerifyClosedConditions();
            ReopenClosedConditions();
        }

        public void CreateCondition()
        {
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            WaitForElement(ConditionsTab);
            ConditionsTab.Click();
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            WaitForElement(ConditionsTitle);
            Assert.IsTrue(ConditionsTitle.Displayed);
            Assert.IsTrue(ConditionsList.Count == 0);
            WaitForElement(AddConditionButton);
            AddConditionButton.Click();
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(1000);
            WaitForElement(AddConditionDiv);
            Assert.IsTrue(AddConditionDiv.Displayed);
            WaitForElement(ConditionDescriptionLabel);
            ConditionDescriptionLabel.Click();
            WaitForElement(ConditionDescriptionInput);
            ConditionDescriptionInput.Click();
            ConditionDescriptionInput.Clear();
            ConditionDescriptionInput.SendKeys("Test Conditions On Registartions ");
            WaitForElement(ProcessLabel);
            ProcessLabel.Click();
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(1000);
            WaitForElements(ProcessList);
            if (ProcessList.Count > 0)
            {
                foreach (var eachprocess in ProcessList)
                {
                    eachprocess.Click();
                }
            }
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(1000);
            WaitForElement(SelectButton);
            SelectButton.Click();
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            WaitForElement(OkButton);
            WaitForElementToClick(OkButton);
            OkButton.Click();
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(1000);
            WaitForElements(ConditionsList);
            Assert.IsTrue(ConditionsList.Count == 1);
        }

        public void EditCondition()
        {
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            string ConditonName = "Registration conditions";
            WaitForElements(ConditionsList);
            int actualListCount = ConditionsList.Count();
            var conditionItem = ConditionsList[0];
            WaitForElements(ConditionsList);
            WaitForElement(conditionItem);
            Thread.Sleep(500);
            conditionItem.Click();
            CloseSpinneronPage();
            CloseSpinneronDiv();
            Thread.Sleep(500);
            WaitForElement(ConditionAmendButton);
            Thread.Sleep(500);
            ConditionAmendButton.Click();
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            WaitForElement(EditConditionDiv);
            Assert.IsTrue(EditConditionDiv.Displayed);
            //Edit Condition Description
            WaitForElement(ConditionDescriptionInput);
            ConditionDescriptionInput.Click();
            ConditionDescriptionInput.Clear();
            ConditionDescriptionInput.SendKeys(ConditonName);
            //Add comment for Amendment
            WaitForElement(AmendConditionCommentLabel);
            AmendConditionCommentLabel.Click();
            WaitForElement(AmendConditionCommentInput);
            AmendConditionCommentInput.Click();
            AmendConditionCommentInput.Clear();
            AmendConditionCommentInput.SendKeys("ConditionAmendment");
            WaitForElement(OkButton);
            OkButton.Click();
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(1000);
            WaitForElement(ConditionCancelButton);
            ConditionCancelButton.Click();
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(1000);
            WaitForElements(ConditionsList);
            // Assert.IsTrue(ConditionsList.Any(x => x.Text == ConditonName));
            //Assert.IsTrue(ConditionsList.Count(x => x.Text == ConditonName) == 1);
        }

        //Close Conditons on Regestration section

        public void CloseCondition()
        {
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            WaitForElements(ConditionsList);
            int actualListCount = ConditionsList.Count();
            var conditionItem = ConditionsList[0];
            WaitForElements(ConditionsList);
            WaitForElement(conditionItem);
            Thread.Sleep(500);
            conditionItem.Click();
            CloseSpinneronPage();
            CloseSpinneronDiv();
            Thread.Sleep(500);
            WaitForElement(ConditionCloseButton);
            Thread.Sleep(500);
            ConditionCloseButton.Click();
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            WaitForElement(CloseConditionDiv);
            Assert.IsTrue(CloseConditionDiv.Displayed);
            //Edit Condition Description

            //Add comment for Close Conditions
            WaitForElement(CloseConditionCommentLabel);
            CloseConditionCommentLabel.Click();
            WaitForElement(CloseConditionCommentInput);
            CloseConditionCommentInput.Click();
            CloseConditionCommentInput.Clear();
            CloseConditionCommentInput.SendKeys("ConditionAmendment");
            WaitForElement(OkButton);
            OkButton.Click();
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(1000);
            WaitForElement(ConditionCancelButton);
            ConditionCancelButton.Click();
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(1000);
            Assert.IsTrue(ConditionsList.Count == 0);
        }

        public void VerifyClosedConditions()
        {
            WaitForElement(IncludeClosedCheckbox);
            if (!IncludeClosedCheckbox.Selected)
            {
                CloseSpinneronDiv();
                CloseSpinneronPage();
                Thread.Sleep(500);
                WaitForElement(IncludeClosedCheckbox);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                Thread.Sleep(500);
                WaitForElementToClick(IncludeClosedCheckbox);
                IncludeClosedCheckbox.Click();
                CloseSpinneronDiv();
                CloseSpinneronPage();
                Thread.Sleep(1000);
                WaitForElements(ConditionsList);
                Assert.IsTrue(ConditionsList.Count == 1);
                WaitForElements(ClosedConditionsList);
                Assert.IsTrue(ClosedConditionsList.Any(x => x.Text.Contains("Closed")));

            }
        }

        public void ReopenClosedConditions()
        {
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(1000);
            WaitForElements(ClosedConditionsList);
            ClosedConditionsList[0].Click();
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(1000);
            WaitForElement(ConditionReopenButton);
            ConditionReopenButton.Click();
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(1000);
            WaitForElement(CloseConditionDiv);
            Assert.IsTrue(CloseConditionDiv.Displayed);
            WaitForElement(ReopenCommentLabel);
            ReopenCommentLabel.Click();
            WaitForElement(ReopenCommentInput);
            ReopenCommentInput.Click();
            ReopenCommentInput.Clear();
            ReopenCommentInput.SendKeys("ReopenComment");
            WaitForElement(OkButton);
            OkButton.Click();
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(1000);
            WaitForElement(ConditionCancelButton);
            ConditionCancelButton.Click();
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            Assert.IsFalse(ClosedConditionsList.Any(x => x.Text.Contains("Closed")));
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Assert.IsTrue(ConditionsList.Count == 1);
        }
        //Verify the Terms Section 
        public void TermsSection()
        {
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            WaitForElement(TermsTab);
            TermsTab.Click();
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            WaitForElement(AddTermsButton);
            AddTermsButton.Click();
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            WaitForElement(AddTermsDiv);
            Assert.IsTrue(AddTermsDiv.Displayed);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            VerifyExpiredTerm();
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            VerifyActiveTerm();
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            VerifyProspectiveTerm();
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            VerifyAdditionalTerms();
        }
        //Verify expiry Term 
        public void VerifyExpiredTerm()
        {
            WaitForElement(TermErrorMessage);
            Assert.IsTrue(TermErrorMessage.Text.Contains("The term must start after 01/05/2012"));
            WaitForElement(StartDateLabel);
            StartDateLabel.Click();
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(1500);
            WaitForElement(SelectYearDropdown);
            SelectYearDropdown.Click();
            WaitForElements(SelectYearInput);
            if (SelectYearInput.Count > 0)
            {
                var ExpiryYear = SelectYearInput.FirstOrDefault(x => x.Text == "2011");
                if(ExpiryYear!=null)
                {
                    ExpiryYear.Click();
                }
                else
                {
                    var MinDisplayedYear = SelectYearInput.FirstOrDefault(x => x.Text == "2014");
                    MinDisplayedYear.Click();
                    CloseSpinneronDiv();
                    CloseSpinneronPage();
                    Thread.Sleep(500);
                    WaitForElement(SelectDate);
                    SelectDate.Click();
                    CloseSpinneronDiv();
                    CloseSpinneronPage();
                    Thread.Sleep(500);
                    WaitForElement(StartDateLabel);
                    StartDateLabel.Click();
                    CloseSpinneronDiv();
                    CloseSpinneronPage();
                    Thread.Sleep(1500);
                    WaitForElement(SelectYearDropdown);
                    SelectYearDropdown.Click();
                    WaitForElements(SelectYearInput);
                    var ExpiredYear = SelectYearInput.FirstOrDefault(x => x.Text == "2011");
                    if(ExpiredYear!=null)
                    {
                        ExpiredYear.Click();
                    }
                }              

            }
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            WaitForElement(SelectDate);
            SelectDate.Click();
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            WaitForElement(TermErrorMessage);
            Assert.IsTrue(TermErrorMessage.Text.Contains("The term must start after 01/05/2012"));
        }
     
        public void CreateActiveTerm()
        {
            WaitForElement(StartDateLabel);
            StartDateLabel.Click();
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            WaitForElement(StartDate);
            StartDate.Click();
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            WaitForElement(EndDate);
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            var endDateValue = EndDateInput.Text;
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            Assert.IsTrue(endDateValue == endDate);
            WaitForElement(SaveButton);
            SaveButton.Click();
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            WaitForElements(TermsList);
            if (TermsList.Count > 0)
            {
                Thread.Sleep(500);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                Thread.Sleep(500);
                var ActiveTerm = TermsList.FirstOrDefault(x => x.Text.Contains("Active"));
                Thread.Sleep(500);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                Thread.Sleep(500);
                Assert.IsTrue(ActiveTerm.Displayed);
            }
        }
        //Create and Verify Active Term 
        public void VerifyActiveTerm()
        {
            WaitForElement(StartDateLabel);
            StartDateLabel.Click();
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            WaitForElement(StartDate);
            StartDate.Click();
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            WaitForElement(EndDate);
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            var endDateValue = EndDateInput.Text;
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            Assert.IsTrue(endDateValue == endDate);
            WaitForElement(SaveButton);
            SaveButton.Click();
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            WaitForElements(TermsList);
            if (TermsList.Count > 0)
            {
                Thread.Sleep(500);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                Thread.Sleep(500);
                var ActiveTerm = TermsList.FirstOrDefault(x => x.Text.Contains("Active"));
                Thread.Sleep(500);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                Thread.Sleep(500);
                Assert.IsTrue(ActiveTerm.Displayed);
                EditActiveTermMethod();
                CloseSpinneronDiv();
                CloseSpinneronPage();
                Thread.Sleep(500);
            }

        }
        //Edit Active Term 
        public void EditActiveTermMethod()
        {
            var ActiveTerm = TermsList.FirstOrDefault(x => x.Text.Contains("Active"));
            Assert.IsTrue(ActiveTerm.Displayed);
            if (ActiveTerm != null)
            {
                ActiveTerm.Click();
                WaitForElement(EditTerm);
                EditTerm.Click();
                WaitForElement(EditTermsDiv);
                Assert.IsTrue(EditTermsDiv.Displayed);
                // Add Fee Waived Adjustments 
                WaitForElement(AdjustmentFeeInput);
                AdjustmentFeeInput.Click();
                AdjustmentFeeInput.Clear();
                AdjustmentFeeInput.SendKeys("1.00");
                WaitForElement(AdjustmentReason);
                AdjustmentReason.Click();
                CloseSpinneronDiv();
                CloseSpinneronPage();
                Thread.Sleep(500);
                WaitForElement(AdjustmentReasonInputFee);
                AdjustmentReasonInputFee.Click();
                CloseSpinneronDiv();
                CloseSpinneronPage();
                Thread.Sleep(500);
                WaitForElement(AdjustmentDescription);
                AdjustmentDescription.Click();
                AdjustmentDescription.Clear();
                AdjustmentDescription.SendKeys("Fee Waived Added");
                WaitForElement(AdjustmentButton);
                AdjustmentButton.Click();
                CloseSpinneronDiv();
                CloseSpinneronPage();
                Thread.Sleep(500);
                WaitForElements(FeeAdjustmentList);
                if (FeeAdjustmentList.Count > 0)
                {
                    WaitForElements(FeeAdjustmentList);
                    FeeAdjustmentList.Any(x => x.Text.Contains("Fee Waived"));
                }
                // Add Group Discount Adjustments 
                WaitForElement(AdjustmentFeeInput);
                AdjustmentFeeInput.Click();
                AdjustmentFeeInput.Clear();
                AdjustmentFeeInput.SendKeys("1.00");
                WaitForElement(AdjustmentReason);
                AdjustmentReason.Click();
                CloseSpinneronDiv();
                CloseSpinneronPage();
                Thread.Sleep(500);
                WaitForElement(AdjustmentReasonInputGroup);
                AdjustmentReasonInputGroup.Click();
                CloseSpinneronDiv();
                CloseSpinneronPage();
                Thread.Sleep(500);
                WaitForElement(AdjustmentDescription);
                AdjustmentDescription.Click();
                AdjustmentDescription.Clear();
                AdjustmentDescription.SendKeys("Fee Group Discount Added");
                WaitForElement(AdjustmentButton);
                AdjustmentButton.Click();
                CloseSpinneronDiv();
                CloseSpinneronPage();
                Thread.Sleep(500);
                WaitForElements(FeeAdjustmentList);
                if (FeeAdjustmentList.Count > 0)
                {
                    WaitForElements(FeeAdjustmentList);
                    FeeAdjustmentList.Any(x => x.Text.Contains("Group Discount"));
                }
                CloseSpinneronDiv();
                CloseSpinneronPage();
                Thread.Sleep(500);
                WaitForElement(SaveButton);
                SaveButton.Click();
                Thread.Sleep(1000);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                Thread.Sleep(500);
            }
        }

        //Edit Prospective Term 
        public void EditProspectiveTermMethod()
        {
            var ProspectiveTerm = TermsList.FirstOrDefault(x => x.Text.Contains("Prospective"));
            Assert.IsTrue(ProspectiveTerm.Displayed);
            if (ProspectiveTerm != null)
            {
                ProspectiveTerm.Click();
                WaitForElement(EditTerm);
                EditTerm.Click();
                WaitForElement(EditTermsDiv);
                Assert.IsTrue(EditTermsDiv.Displayed);
                // Add Fee Waived Adjustments 
                WaitForElement(AdjustmentFeeInput);
                AdjustmentFeeInput.Click();
                AdjustmentFeeInput.Clear();
                AdjustmentFeeInput.SendKeys("1.00");
                WaitForElement(AdjustmentReason);
                AdjustmentReason.Click();
                CloseSpinneronDiv();
                CloseSpinneronPage();
                Thread.Sleep(500);
                WaitForElement(AdjustmentReasonInputFee);
                AdjustmentReasonInputFee.Click();
                CloseSpinneronDiv();
                CloseSpinneronPage();
                Thread.Sleep(500);
                WaitForElement(AdjustmentDescription);
                AdjustmentDescription.Click();
                AdjustmentDescription.Clear();
                AdjustmentDescription.SendKeys("Fee Waived Added");
                WaitForElement(AdjustmentButton);
                AdjustmentButton.Click();
                CloseSpinneronDiv();
                CloseSpinneronPage();
                Thread.Sleep(500);
                WaitForElements(FeeAdjustmentList);
                if (FeeAdjustmentList.Count > 0)
                {
                    WaitForElements(FeeAdjustmentList);
                    FeeAdjustmentList.Any(x => x.Text.Contains("Fee Waived"));
                }
                // Add Group Discount Adjustments 
                WaitForElement(AdjustmentFeeInput);
                AdjustmentFeeInput.Click();
                AdjustmentFeeInput.Clear();
                AdjustmentFeeInput.SendKeys("1.00");
                WaitForElement(AdjustmentReason);
                AdjustmentReason.Click();
                CloseSpinneronDiv();
                CloseSpinneronPage();
                Thread.Sleep(500);
                WaitForElement(AdjustmentReasonInputGroup);
                AdjustmentReasonInputGroup.Click();
                CloseSpinneronDiv();
                CloseSpinneronPage();
                Thread.Sleep(500);
                WaitForElement(AdjustmentDescription);
                AdjustmentDescription.Click();
                AdjustmentDescription.Clear();
                AdjustmentDescription.SendKeys("Fee Group Discount Added");
                WaitForElement(AdjustmentButton);
                AdjustmentButton.Click();
                CloseSpinneronDiv();
                CloseSpinneronPage();
                Thread.Sleep(500);
                WaitForElements(FeeAdjustmentList);
                if (FeeAdjustmentList.Count > 0)
                {
                    WaitForElements(FeeAdjustmentList);
                    FeeAdjustmentList.Any(x => x.Text.Contains("Group Discount"));
                }
                CloseSpinneronDiv();
                CloseSpinneronPage();
                Thread.Sleep(500);
                WaitForElement(SaveButton);
                SaveButton.Click();
                Thread.Sleep(1000);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                Thread.Sleep(500);
            }
        }

        //Create and Verify Prospective Term
        public void VerifyProspectiveTerm()
        {
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            WaitForElement(AddTermsButton);
            AddTermsButton.Click();
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            WaitForElement(AddTermsDiv);
            Assert.IsTrue(AddTermsDiv.Displayed);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);

            WaitForElement(StartDateLabel);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);

            WaitForElement(EndDate);
            var endDateValue = EndDateInput.Text;
            Assert.IsTrue(endDateValue == prospectiveEndDateInput);

            WaitForElement(SaveButton);
            SaveButton.Click();
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            WaitForElements(TermsList);
            if (TermsList.Count > 0)
            {
                var ProspectiveTerm = TermsList.FirstOrDefault(x => x.Text.Contains("Prospective"));
                Assert.IsTrue(ProspectiveTerm.Displayed);
                EditProspectiveTermMethod();
            }
        }

        //Verify Additional Terms 
        public void VerifyAdditionalTerms()
        {
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            WaitForElement(AddTermsButton);
            AddTermsButton.Click();
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            WaitForElement(TermErrorMessage);
            Assert.IsTrue(TermErrorMessage.Text.Contains("Registration already has the maximum 1 prospective term"));
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            WaitForElement(StartDateLabel);
            StartDateLabel.Click();
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            WaitForElement(StartDate);
            StartDate.Click();
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            WaitForElement(TermErrorMessage);
            Assert.IsTrue(TermErrorMessage.Text.Contains("New term cannot fall within an existing term"));
            WaitForElement(CancelButton);
            CancelButton.Click();
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
        }
        public void CacelActiveTerm()
        {
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            WaitForElement(TermsTab);
            TermsTab.Click();
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
           
        }
        public void CancelTermMethod()
        {
            if (TermsList.Count > 0)
            {
                var activeTerm = TermsList.FirstOrDefault(x => x.Text.Contains("Active"));
                Assert.IsTrue(activeTerm.Displayed);
                activeTerm.Click();
                WaitForElement(CancelTermButton);
                CancelTermButton.Click();
                WaitForElement(CancellationReasonLabel);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                Thread.Sleep(500);
                if(CancellationReasonLabelList.Count>0)
                {
                    CancellationReasonLabelList[0].Click();
                }
                CloseSpinneronDiv();
                CloseSpinneronPage();
                Thread.Sleep(500);
                WaitForElement(CancellationNotesLabel);
                CancellationNotesLabel.Click();
                WaitForElement(CancellationNotesInput);
                CancellationNotesInput.SendKeys("Cancel The Term");
            }
        }
        //Verify Roles Section
        public void RolesSection()
        {
            CreateRole();
            DeleteRole();
            CreateRole();
        }

        //Create Role
        public void CreateRole()
        {

            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            WaitForElement(RolesTab);
            RolesTab.Click();
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            if (RolesList.Count == 0)
            {
                WaitForElement(AddRolesButton);
                AddRolesButton.Click();
                CloseSpinneronDiv();
                CloseSpinneronPage();
                Thread.Sleep(500);
                WaitForElement(AddRoleDiv);
                Assert.IsTrue(AddRoleDiv.Displayed);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                Thread.Sleep(500);
                WaitForElement(RoleDropdown);
                RoleDropdown.Click();
                CloseSpinneronDiv();
                CloseSpinneronPage();
                Thread.Sleep(500);
                WaitForElement(RoleDropdownInput);
                RoleDropdownInput.Click();
                CloseSpinneronDiv();
                CloseSpinneronPage();
                Thread.Sleep(500);
                WaitForElement(PersonLabel);
                PersonLabel.Click();
                CloseSpinneronDiv();
                CloseSpinneronPage();
                Thread.Sleep(500);
                WaitForElements(PersonInput);
                if (PersonInput.Count > 0)
                {
                    CloseSpinneronDiv();
                    CloseSpinneronPage();
                    Thread.Sleep(500);
                    PersonInput[0].Click();
                    CloseSpinneronDiv();
                    CloseSpinneronPage();
                    Thread.Sleep(500);
                }
                WaitForElement(SaveButton);
                SaveButton.Click();
                CloseSpinneronDiv();
                CloseSpinneronPage();
                Thread.Sleep(500);
                WaitForElements(RolesList);
                Assert.IsTrue(RolesList.Count == 1);

                //Verify View Information Button
                if (RolesList.Count > 0)
                {
                    Assert.IsTrue(ViewInformationButton.Count == 1);
                }

                CloseSpinneronDiv();
                CloseSpinneronPage();
                Thread.Sleep(500);

            }
        }

        //Delete Role
        public void DeleteRole()
        {
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            if (RolesList.Count > 0)
            {
                RolesList[0].Click();
                CloseSpinneronDiv();
                CloseSpinneronPage();
                Thread.Sleep(500);
                if (RoleDeleteButton.Count > 0)
                {
                    RoleDeleteButton[0].Click();
                    CloseSpinneronDiv();
                    CloseSpinneronPage();
                    Thread.Sleep(500);
                    WaitForElement(DeleteOkButton);
                    DeleteOkButton.Click();
                }
                CloseSpinneronDiv();
                CloseSpinneronPage();
                Thread.Sleep(500);
            }
            if (RolesList.Count == 0)
            {
                Assert.IsTrue(ViewInformationButton.Count == 0);
            }

            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
        }

        //Account Section 
        public void AccountSection()
        {
            EnterRegistationFee();
            TransferMoney();
            RefundClient();
        }

        //Make Registration fee Method
        public void EnterRegistationFee()
        {
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(1000);
            WaitForElement(AccountTab);
            AccountTab.Click();
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(1000);

            WaitForElement(FeePaid);
            var FeePaidValue = FeePaid.Text.Replace("£", "");
            decimal actualRegFeePaid = Convert.ToDecimal(FeePaidValue);
            WaitForElement(FeeBalance);
            var RegBalanceValue = FeeBalance.Text.Replace("£", "");
            decimal actualRegFeeBal = Convert.ToDecimal(RegBalanceValue);

            WaitForElement(EscrowPaid);
            var EscrowPaidValue = EscrowPaid.Text.Replace("£", "");
            decimal actualEscrowFeePaid = Convert.ToDecimal(EscrowPaidValue);
            WaitForElement(EscrowBalance);
            var EscrowBalanceValue = EscrowBalance.Text.Replace("£", "");
            decimal actualRegEscrowBal = Convert.ToDecimal(EscrowBalanceValue);

            if (actualRegFeeBal > actualRegFeePaid || actualRegEscrowBal > actualEscrowFeePaid)
            {
                WaitForElement(EnterPaymentButton);
                EnterPaymentButton.Click();
                Thread.Sleep(1000);
                WaitForElement(MakePaymentDiv);
                Assert.IsTrue(MakePaymentDiv.Displayed);
                FinancesPage.MakeDirectPyament();
                CloseSpinneronPage();
            }
            Thread.Sleep(1000);
        }
        //TransferMoney
        //Transfer Money to this account 
        public void TransferMoney()
        {
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(1000);
            WaitForElement(AccountTab);
            AccountTab.Click();
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(1000);
            //Check Fee Paid Values 
            WaitForElement(FeePaid);
            var FeePaidValue = FeePaid.Text.Replace("£", "");
            decimal actualRegFeePaid = Convert.ToDecimal(FeePaidValue);
            //check Escrow Paid Value 
            WaitForElement(EscrowPaid);
            var EscrowPaidValue = EscrowPaid.Text.Replace("£", "");
            decimal actualEscrowFeePaid = Convert.ToDecimal(EscrowPaidValue);

            if (actualEscrowFeePaid > 0 || actualRegFeePaid > 0)
            {
                WaitForElement(TransferMoneyButton);
                TransferMoneyButton.Click();
                CloseSpinneronDiv();
                CloseSpinneronPage();
                Thread.Sleep(1500);
                WaitForElement(TransferMoneyDiv);
                Assert.IsTrue(TransferMoneyDiv.Displayed);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                Thread.Sleep(1500);
                SelectToAccount();
                CloseSpinneronDiv();
                CloseSpinneronPage();
                Thread.Sleep(1500);

                //Perform TranferMoney operation 
                FinancesPage.CurrenyTransfer();
                Thread.Sleep(1000);
                WaitForElement(TransferCommentLabel);
                TransferCommentLabel.Click();
                WaitForElement(TransferCommentInput);
                TransferCommentInput.SendKeys("Transfering Amount");
                Thread.Sleep(1000);
                CloseSpinneronDiv();
                Thread.Sleep(1000);
                WaitForElement(TransferButton);
                TransferButton.Click();
                Thread.Sleep(1000);
                WaitForElement(TransferOkButton);
                TransferOkButton.Click();
                Thread.Sleep(1000);
                //WaitForElement(TransferComplete);
                //Assert.IsTrue(TransferComplete.Displayed);
                //Console.WriteLine("Amount has been transfered");
            }


        }
        //Select to Account and Transfer Money 
        public void SelectToAccount()
        {

            //Selecting TO List Accounts 
            WaitForElement(ToButton);
            ToButton.Click();
            CloseSpinneronDiv();
            Thread.Sleep(1500);
            WaitForElements(TransferToAccountsList);
            var toAccount = TransferToAccountsList
                .Select(tal => new { balance = Convert.ToDecimal(tal.Text.Replace("£", "")), element = tal })
                .Where(tal => tal.balance >= 0 || tal.balance < 0)
                .ToList();

            CloseSpinneronDiv();
            Thread.Sleep(1500);
            if (toAccount.Count > 0)
            {
                var toAccountImput = toAccount.First();
                Thread.Sleep(1500);
                CloseSpinneronDiv();
                Thread.Sleep(1000);
                toAccountImput.element.Click();
            }

            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(1000);
        }

        //Make Registration fee Method
        public void RefundClient()
        {
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(1000);
            WaitForElement(AccountTab);
            AccountTab.Click();
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(1000);


            WaitForElement(FeePaid);
            var FeePaidValue = FeePaid.Text.Replace("£", "");
            decimal actualRegFeePaid = Convert.ToDecimal(FeePaidValue);

            WaitForElement(EscrowPaid);
            var EscrowPaidValue = EscrowPaid.Text.Replace("£", "");
            decimal actualEscrowFeePaid = Convert.ToDecimal(EscrowPaidValue);

            if (actualRegFeePaid > 0 || actualEscrowFeePaid > 0)
            {
                WaitForElement(RefundButton);
                RefundButton.Click();
                Thread.Sleep(1000);
                WaitForElement(RefundDiv);
                Assert.IsTrue(RefundDiv.Displayed);
                RefundMoney();
                CloseSpinneronPage();
                WaitForElement(TransferCommentLabel);
                TransferCommentLabel.Click();
                WaitForElement(TransferCommentInput);
                TransferCommentInput.SendKeys("Transfering Amount");
                Thread.Sleep(1000);
                CloseSpinneronDiv();
                Thread.Sleep(1000);
                WaitForElement(TransferButton);
                TransferButton.Click();
                Thread.Sleep(1000);
                WaitForElement(TransferOkButton);
                TransferOkButton.Click();
                Thread.Sleep(1000);
                WaitForElement(TransferComplete);
                Assert.IsTrue(TransferComplete.Displayed);
                Console.WriteLine("Amount has been transfered");
            }

        }

        //Refund Money Operation on Refund Div
        public void RefundMoney()
        {
            //Retrieving Escrow Amount Paid Value 
            Thread.Sleep(1000);
            WaitForElement(EscrowPaid);
            var escrowAmountPaidValue = EscrowPaid.Text.Replace("£", "");
            decimal actualEscrowAmountPaidValue = (Convert.ToDecimal(escrowAmountPaidValue)) / 2;
            //Retrieving Fee Amount Paid Value 
            Thread.Sleep(1000);
            WaitForElement(FeePaid);
            var feeAmountPaidValue = FeePaid.Text.Replace("£", "");
            decimal actualFeeAmountPaidValue = (Convert.ToDecimal(feeAmountPaidValue)) / 2;

            if (actualFeeAmountPaidValue > 0)
            {

                //Enter the value on From Account Fee Amount field 
                WaitForElement(FromFeeAmountLabel);
                FromFeeAmountLabel.Click();
                WaitForElement(FromFeeAmountInput);
                FromFeeAmountInput.SendKeys(actualFeeAmountPaidValue.ToString(CultureInfo.InvariantCulture));
            }
            if (actualEscrowAmountPaidValue > 0)
            {
                //Enter the value on From Account Escrow Amount field 
                WaitForElement(FromEscrowAmountLabel);
                FromEscrowAmountLabel.Click();
                WaitForElement(FromEscrowAmountInput);
                FromEscrowAmountInput.SendKeys(actualEscrowAmountPaidValue.ToString(CultureInfo.InvariantCulture));
            }
        }

        //Task Operation 
        public void TaskSection()
        {
            CreateTask();
            EditTask();
            CloneTask();
            CompleteTask();
            FilterTask();
            ReopenTask();
        }

        //Creating a New Task
        public void CreateTask()
        {
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            WaitForElement(TaskTab);
            TaskTab.Click();
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            if (TaskList.Count == 1)
            {
                Thread.Sleep(500);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                Thread.Sleep(500);
                WaitForElement(TaskList[0]);
                Thread.Sleep(500);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                Thread.Sleep(500);
                WaitForElementToClick(TaskList[0]);
                Thread.Sleep(500);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                Thread.Sleep(500);
                TaskList[0].Click();
                CloseSpinneronDiv();
                CloseSpinneronPage();
                Thread.Sleep(500);
                WaitForElement(TaskName);
              //  Assert.IsTrue(TaskName.Text == "Registration - Identify Registration Correspondence Person");
            }
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            WaitForElement(AddTaskButton);
            AddTaskButton.Click();
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            WaitForElement(CreateTaskDiv);
            Assert.IsTrue(CreateTaskDiv.Displayed);
            WaitForElement(TaskTypeDropdown);
            TaskTypeDropdown.Click();
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            WaitForElement(TaskTypeDropdownInput);
            TaskTypeDropdownInput.Click();
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            WaitForElement(TaskSubTypeDropdown);
            TaskSubTypeDropdown.Click();
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            WaitForElement(TaskSubTypeDropdownInput);
            TaskSubTypeDropdownInput.Click();
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            WaitForElement(DateLabel);
            DateLabel.Click();
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            var DateInput = $"//div[contains(@class,'picker--opened')]//div[@class='picker__box']//tbody//tr//td//div[@aria-label='{FutureDate}']";
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            Driver.FindElement(By.XPath(DateInput)).Click();
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            WaitForElement(TimeLabel);
            TimeLabel.Click();
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            WaitForElements(TimeList);
            TimeList[2].Click();
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            WaitForElement(HighPriorityLabel);
            HighPriorityLabel.Click();
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            WaitForElement(NotesLabel);
            NotesLabel.Click();
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            WaitForElement(NotesInput);
            NotesInput.Click();
            NotesInput.Clear();
            NotesInput.SendKeys("AddingTask");
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            WaitForElement(SaveButton);
            SaveButton.Click();
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            WaitForElements(TaskList);
            Assert.IsTrue(TaskList.Count >= 1);
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(1000);
        }

        //Edit a Task
        public void EditTask()
        {
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            WaitForElement(TaskTab);
            TaskTab.Click();
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            WaitForElements(TaskList);
            if (TaskList.Count > 0)
            {
                Thread.Sleep(500);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                Thread.Sleep(500);
                WaitForElement(TaskList[0]);
                Thread.Sleep(500);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                Thread.Sleep(500);
                WaitForElementToClick(TaskList[0]);
                Thread.Sleep(500);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                Thread.Sleep(500);
                TaskList[0].Click();
                CloseSpinneronDiv();
                CloseSpinneronPage();
                Thread.Sleep(500);
                WaitForElement(TaskName);
                Assert.IsTrue(TaskName.Text == "Quote - Input Quote application");
                WaitForElement(EditTaskButton);
                EditTaskButton.Click();
                CloseSpinneronDiv();
                CloseSpinneronPage();
                Thread.Sleep(500);
                WaitForElement(AmendTaskDiv);
                Assert.IsTrue(AmendTaskDiv.Displayed);
                WaitForElement(TaskSubTypeDropdown);
                TaskSubTypeDropdown.Click();
                Thread.Sleep(500);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                Thread.Sleep(500);
                WaitForElement(AmendTaskSubTypeDropdownInput);
                AmendTaskSubTypeDropdownInput.Click();
                Thread.Sleep(500);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                Thread.Sleep(500);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                Thread.Sleep(500);
                WaitForElement(HighPriorityLabel);
                HighPriorityLabel.Click();
                Thread.Sleep(500);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                Thread.Sleep(500);
                WaitForElement(AmendNotesLabel);
                AmendNotesLabel.Click();
                Thread.Sleep(500);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                Thread.Sleep(500);
                WaitForElement(AmendNotesInput);
                AmendNotesInput.Click();
                AmendNotesInput.Clear();
                AmendNotesInput.SendKeys("AddingTask");
                Thread.Sleep(500);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                Thread.Sleep(500);
                WaitForElement(SaveButton);
                SaveButton.Click();
                Thread.Sleep(500);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                Thread.Sleep(500);
            }
            WaitForElements(TaskList);
            if (TaskList.Count > 0)
            {
                Thread.Sleep(500);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                Thread.Sleep(500);
                WaitForElement(TaskList[0]);
                Thread.Sleep(500);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                Thread.Sleep(500);
                WaitForElementToClick(TaskList[0]);
                Thread.Sleep(500);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                Thread.Sleep(500);
                TaskList[0].Click();
                CloseSpinneronDiv();
                CloseSpinneronPage();
                Thread.Sleep(500);
                WaitForElement(TaskName);
                Assert.IsTrue(TaskName.Text == "Quote - Enter New Quote Application");
            }
        }
        //Clone a Task
        public void CloneTask()
        {
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            WaitForElement(TaskTab);
            TaskTab.Click();
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            WaitForElements(TaskList);
            if (TaskList.Count > 0)
            {
                Thread.Sleep(500);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                Thread.Sleep(500);
                WaitForElement(TaskList[0]);
                Thread.Sleep(500);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                Thread.Sleep(500);
                WaitForElementToClick(TaskList[0]);
                Thread.Sleep(500);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                Thread.Sleep(500);
                TaskList[0].Click();
                CloseSpinneronDiv();
                CloseSpinneronPage();
                Thread.Sleep(500);
                WaitForElement(TaskName);
                Assert.IsTrue(TaskName.Text == "Quote - Enter New Quote Application");
                WaitForElement(CloneTaskButton);
                CloneTaskButton.Click();
                CloseSpinneronDiv();
                CloseSpinneronPage();
                Thread.Sleep(500);
                WaitForElement(CloneTaskDiv);
                Assert.IsTrue(CloneTaskDiv.Displayed);
                WaitForElement(TaskSubTypeDropdown);
                TaskSubTypeDropdown.Click();
                Thread.Sleep(500);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                Thread.Sleep(500);
                WaitForElement(TaskSubTypeDropdownInput);
                TaskSubTypeDropdownInput.Click();
                Thread.Sleep(500);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                Thread.Sleep(500);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                Thread.Sleep(500);
                WaitForElement(HighPriorityLabel);
                HighPriorityLabel.Click();
                Thread.Sleep(500);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                Thread.Sleep(500);
                WaitForElement(NotesLabel);
                NotesLabel.Click();
                Thread.Sleep(500);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                Thread.Sleep(500);
                WaitForElement(NotesInput);
                NotesInput.Click();
                NotesInput.Clear();
                NotesInput.SendKeys("AddingTask");
                Thread.Sleep(500);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                Thread.Sleep(500);
                WaitForElement(SaveButton);
                SaveButton.Click();
                Thread.Sleep(500);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                Thread.Sleep(500);
            }
        }
        //Complete Task
        public void CompleteTask()
        {
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            WaitForElement(TaskTab);
            TaskTab.Click();
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            WaitForElements(TaskList);
            if (TaskList.Count > 0)
            {
                int actualTaskList = TaskList.Count;
                for (int i = 0; i < actualTaskList; i++)
                {
                    Thread.Sleep(500);
                    CloseSpinneronDiv();
                    CloseSpinneronPage();
                    Thread.Sleep(1000);
                    WaitForElement(TaskList[0]);
                    var CompleteTaskItem = TaskList[0];
                    WaitForElement(CompleteTaskItem);
                    Thread.Sleep(500);
                    CloseSpinneronDiv();
                    CloseSpinneronPage();
                    Thread.Sleep(500);
                    WaitForElementToClick(CompleteTaskItem);
                    CompleteTaskItem.Click();
                    Thread.Sleep(500);
                    CloseSpinneronDiv();
                    CloseSpinneronPage();
                    Thread.Sleep(500);
                    WaitForElement(CompleteTaskButton);
                    CompleteTaskButton.Click();
                    Thread.Sleep(500);
                    CloseSpinneronDiv();
                    CloseSpinneronPage();
                    Thread.Sleep(500);
                    WaitForElement(CompleteTaskDiv);
                    Assert.IsTrue(CompleteTaskDiv.Displayed);
                    Thread.Sleep(500);
                    CloseSpinneronDiv();
                    CloseSpinneronPage();
                    Thread.Sleep(500);
                    WaitForElement(CompletionReasonLabel);
                    CompletionReasonLabel.Click();
                    Thread.Sleep(500);
                    CloseSpinneronDiv();
                    CloseSpinneronPage();
                    Thread.Sleep(500);
                    WaitForElement(CompletionReasonInput);
                    CompletionReasonInput.Click();
                    Thread.Sleep(500);
                    CloseSpinneronDiv();
                    CloseSpinneronPage();
                    Thread.Sleep(500);
                    WaitForElement(CompletionNotesLabel);
                    CompletionNotesLabel.Click();
                    Thread.Sleep(500);
                    CloseSpinneronDiv();
                    CloseSpinneronPage();
                    Thread.Sleep(500);
                    WaitForElement(CompletionNotesInput);
                    CompletionNotesInput.Click();
                    CompletionNotesInput.Clear();
                    CompletionNotesInput.SendKeys("TaskCompleted");
                    WaitForElement(CompleteButton);
                    CompleteButton.Click();
                    Thread.Sleep(500);
                    CloseSpinneronDiv();
                    CloseSpinneronPage();
                    Thread.Sleep(1000);
                }
            }
        }
        //Filter Task
        public void FilterTask()
        {
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            WaitForElement(TaskTab);
            TaskTab.Click();
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            WaitForElement(ShowFilterButton);
            ShowFilterButton.Click();
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            WaitForElements(FilterTypes);

            WaitForElement(IncludeClosed);
            IncludeClosed.Click();
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            WaitForElement(DoFilterButton);
            DoFilterButton.Click();
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            WaitForElements(TaskList);
            Assert.IsTrue(TaskList.Count >= 1);
        }

        //Reopen Task
        public void ReopenTask()
        {
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            WaitForElement(TaskTab);
            TaskTab.Click();
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            WaitForElements(TaskList);
            if (TaskList.Count > 0)
            {
                for (int i = 0; i < TaskList.Count; i++)
                {
                    Thread.Sleep(500);
                    CloseSpinneronDiv();
                    CloseSpinneronPage();
                    Thread.Sleep(500);
                    WaitForElement(TaskList[0]);
                    Thread.Sleep(500);
                    CloseSpinneronDiv();
                    CloseSpinneronPage();
                    Thread.Sleep(500);
                    WaitForElementToClick(TaskList[i]);
                    Thread.Sleep(500);
                    CloseSpinneronDiv();
                    CloseSpinneronPage();
                    Thread.Sleep(500);
                    TaskList[i].Click();
                    CloseSpinneronDiv();
                    CloseSpinneronPage();
                    Thread.Sleep(500);  
                    CloseSpinneronDiv();
                    CloseSpinneronPage();
                    Thread.Sleep(500);
                    if (ReopenTaskButton.Count > 0)
                    {
                        WaitForElement(ReopenTaskButton[0]);
                        ReopenTaskButton[0].Click();
                        Thread.Sleep(500);
                        CloseSpinneronDiv();
                        CloseSpinneronPage();
                        Thread.Sleep(500);
                        WaitForElement(ReopenTaskDiv);
                        Assert.IsTrue(ReopenTaskDiv.Displayed);
                        Thread.Sleep(500);
                        CloseSpinneronDiv();
                        CloseSpinneronPage();
                        Thread.Sleep(500);
                        WaitForElement(ReopenTaskCommentLabel);
                        ReopenTaskCommentLabel.Click();
                        Thread.Sleep(500);
                        CloseSpinneronDiv();
                        CloseSpinneronPage();
                        Thread.Sleep(500);
                        WaitForElement(ReopenTaskCommentInput);
                        ReopenTaskCommentInput.Click();
                        ReopenTaskCommentInput.Clear();
                        ReopenTaskCommentInput.SendKeys("Task Reopen");
                        WaitForElement(ReopenButton);
                        ReopenButton.Click();
                        Thread.Sleep(500);
                        CloseSpinneronDiv();
                        CloseSpinneronPage();
                        Thread.Sleep(1000);
                    }
                }
                Thread.Sleep(500);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                Thread.Sleep(500);
                WaitForElements(TaskList);
                Assert.IsTrue(TaskList.Count >= 1);
            }
        }

        //Correspondence Section 
        public void CorrespondenceSection()
        {
            CreateRole();
            SendCorrespondence();
            VerifyCorrespondenceButton();
        }
        //sending Corrspondence 
        public void SendCorrespondence()
        {

            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            WaitForElement(CorrespondenceTab);
            CorrespondenceTab.Click();
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            WaitForElement(AddCorrespondenceButton);
            AddCorrespondenceButton.Click();
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            WaitForElement(CorrespondenceDiv);
            Assert.IsTrue(CorrespondenceDiv.Displayed);
            WaitForElement(MailBoxLabel);
            MailBoxLabel.Click();
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            WaitForElements(MailBoxInput);
            if (MailBoxInput.Count > 0)
            {
                MailBoxInput[0].Click();
            }
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            WaitForElement(AttachmentLabel);
            AttachmentLabel.Click();
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            PageUtils.UploadNewFile();
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            WaitForElement(SendButton);
            SendButton.Click();
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            WaitForElement(SendEmailOkButton);
            SendEmailOkButton.Click();
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            WaitForElement(EmailRefreshButon);
            EmailRefreshButon.Click();
            Thread.Sleep(1000);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(1000);
            WaitForElements(CorrespondenceList);
            Assert.IsTrue(CorrespondenceList.Count >= 1);
            VerifySentAction();
        }
        //Verify All Buttons on Email Correspondence
        public void VerifyCorrespondenceButton()
        {           
            ReplayButton();
            ReplayAllButton();
            ForwardButton();
           // IndexAttachmentButton();
        }

        //view Actions
        public void ViewActions()
        {
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            WaitForElement(CorrespondenceTab);
            CorrespondenceTab.Click();
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            WaitForElements(CorrespondenceList);
            if(CorrespondenceList.Count >= 1)
            {
                Thread.Sleep(500);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                Thread.Sleep(500);
                CorrespondenceList[0].Click();
                Thread.Sleep(500);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                Thread.Sleep(500);
                WaitForElement(ViewActionButon);
                ViewActionButon.Click();
            }
        }
        //verify Sent Action 
        public void VerifySentAction()
        {

            ViewActions();
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            WaitForElement(EmailActionCard);
            Assert.IsTrue(EmailActionCard.Displayed);
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            if (EmailActionList.Count > 0)
            {
                WaitForElements(TypeofActionList);
                var TypeOfActionListTitle = TypeofActionList.FirstOrDefault(x => x.Text.Contains("Sent"));
                Assert.IsTrue(TypeOfActionListTitle.Displayed);
                WaitForElement(CancelButton);
                CancelButton.Click();
                Thread.Sleep(500);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                Thread.Sleep(500);
            }
        }
        //verify Replay Action 
        public void VerifyRepliedAction()
        {

            ViewActions();
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            WaitForElement(EmailActionCard);
            Assert.IsTrue(EmailActionCard.Displayed);
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            if (EmailActionList.Count > 0)
            {
                WaitForElements(TypeofActionList);
                var TypeOfActionListTitle = TypeofActionList.FirstOrDefault(x => x.Text.Contains("Replied"));
                Assert.IsTrue(TypeOfActionListTitle.Displayed);
                WaitForElement(CancelButton);
                CancelButton.Click();
                Thread.Sleep(500);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                Thread.Sleep(500);
            }
        }

        //verify Forward Action 
        public void VerifyForwardAction()
        {

            ViewActions();
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            WaitForElement(EmailActionCard);
            Assert.IsTrue(EmailActionCard.Displayed);
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            if (EmailActionList.Count > 0)
            {
                WaitForElements(TypeofActionList);
                var TypeOfActionListTitle = TypeofActionList.FirstOrDefault(x => x.Text.Contains("Forwarded"));
                Assert.IsTrue(TypeOfActionListTitle.Displayed);
                WaitForElement(CancelButton);
                CancelButton.Click();
                Thread.Sleep(500);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                Thread.Sleep(500);
            }
        }

        //Replay Email 
        public void ReplayCorrespondence()
        {
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            WaitForElement(CorrespondenceDiv);
            Assert.IsTrue(CorrespondenceDiv.Displayed);         
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);  
            WaitForElement(AttachmentLabel);
            AttachmentLabel.Click();
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            PageUtils.UploadNewFile();
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            WaitForElement(SendButton);
            SendButton.Click();
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);            
            
        }

        // Reply Correspondence        
        public void ReplayButton()
        {
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            WaitForElement(CorrespondenceTab);
            CorrespondenceTab.Click();
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            WaitForElements(CorrespondenceList);
            if (CorrespondenceList.Count >= 1)
            {
                Thread.Sleep(500);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                Thread.Sleep(500);
                CorrespondenceList[0].Click();
                Thread.Sleep(500);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                Thread.Sleep(500);
                WaitForElement(ReplayButon);
                ReplayButon.Click();
                Thread.Sleep(500);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                Thread.Sleep(500);
                ReplayCorrespondence();
            }
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            WaitForElement(EmailRefreshButon);
            EmailRefreshButon.Click();
            Thread.Sleep(1000);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(1000);
            WaitForElements(CorrespondenceList);
            Assert.IsTrue(CorrespondenceList.Count >= 1);
           // VerifyRepliedAction();
        }

        //Replay All Correspondence         
        public void ReplayAllButton()
        {
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            WaitForElement(CorrespondenceTab);
            CorrespondenceTab.Click();
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            WaitForElements(CorrespondenceList);
            if (CorrespondenceList.Count >= 1)
            {
                Thread.Sleep(500);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                Thread.Sleep(500);
                CorrespondenceList[0].Click();
                Thread.Sleep(500);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                Thread.Sleep(500);
                WaitForElement(ReplayAllButon);
                ReplayAllButon.Click();
                Thread.Sleep(500);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                Thread.Sleep(500);
                ReplayCorrespondence();
            }
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            WaitForElement(EmailRefreshButon);
            EmailRefreshButon.Click();
            Thread.Sleep(1000);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(1000);
            WaitForElements(CorrespondenceList);
            Assert.IsTrue(CorrespondenceList.Count >= 1);
          //  VerifyRepliedAction();
        }
        //Forward Email 
        public void ForwardCorrespondence()
        {
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            WaitForElement(CorrespondenceDiv);
            Assert.IsTrue(CorrespondenceDiv.Displayed);
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            WaitForElement(MailBoxLabel);
            MailBoxLabel.Click();
            CloseSpinneronDiv();
            CloseSpinneronPage();
            WaitForElements(MailBoxInput);
            if (MailBoxInput.Count > 0)
            {
                MailBoxInput[0].Click();
            }
            CloseSpinneronDiv();
            CloseSpinneronPage();
            WaitForElement(EmailTo);
            EmailTo.Click();
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            WaitForElements(ForwardEmailList);
           if(ForwardEmailList.Count>0)
            {
                ForwardEmailList[0].Click();
            }
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);        
            WaitForElement(SendButton);
            WaitForElementToClick(SendButton);
            SendButton.Click();
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
        }
        //Forward Correspondence         
        public void ForwardButton()
        {
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            WaitForElement(CorrespondenceTab);
            CorrespondenceTab.Click();
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            WaitForElements(CorrespondenceList);
            if (CorrespondenceList.Count >= 1)
            {
                Thread.Sleep(500);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                Thread.Sleep(500);
                CorrespondenceList[0].Click();
                Thread.Sleep(500);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                Thread.Sleep(500);
                WaitForElement(ForwardButon);
                ForwardButon.Click();
                Thread.Sleep(500);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                Thread.Sleep(500);
                ForwardCorrespondence();
            }
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            WaitForElement(EmailRefreshButon);
            EmailRefreshButon.Click();
            Thread.Sleep(1000);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(1000);
            WaitForElements(CorrespondenceList);
            Assert.IsTrue(CorrespondenceList.Count >= 1);
           // VerifyForwardAction();
        }


        //Index Attachments        
        public void IndexAttachmentButton()
        {
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            WaitForElement(CorrespondenceTab);
            CorrespondenceTab.Click();
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            WaitForElements(CorrespondenceList);
            if (CorrespondenceList.Count >= 1)
            {
                Thread.Sleep(500);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                Thread.Sleep(500);
                CorrespondenceList[0].Click();
                Thread.Sleep(500);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                Thread.Sleep(500);
                WaitForElement(IndexAttachmentsButon);
                IndexAttachmentsButon.Click();
                Thread.Sleep(500);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                Thread.Sleep(500);
                WaitForElement(IndexAttachmentCard);
                Assert.IsTrue(IndexAttachmentCard.Displayed);
                if(AttachmentsList.Count>0)
                {
                    Thread.Sleep(500);
                    CloseSpinneronDiv();
                    CloseSpinneronPage();
                    Thread.Sleep(500);
                    WaitForElement(EmailAttachmentsLabel);
                    EmailAttachmentsLabel.Click();
                    Thread.Sleep(500);
                    CloseSpinneronDiv();
                    CloseSpinneronPage();
                    Thread.Sleep(500);
                    WaitForElement(AttachmentDetailsCard);
                    Assert.IsTrue(AttachmentDetailsCard.Displayed);
                    Thread.Sleep(500);
                    CloseSpinneronDiv();
                    CloseSpinneronPage();
                    Thread.Sleep(500);
                    WaitForElement(CancelButton);
                    CancelButton.Click();
                }
                Thread.Sleep(500);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                Thread.Sleep(500);
            }

        }


            //Files and Documents Section 
        public void FilesDocsSection()
        {
            VerifyFolders();
            CreateFolder();
            UploadRegDocs();
            UploadNotIndexedDocs();

        }

        //Verify Default Folders
        public void VerifyFolders()
        {
            int[] n = { 0, 1, 2, 3, 4, 5 };
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            WaitForElement(FilesDocsTab);
            FilesDocsTab.Click();
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            WaitForElements(DefaultFolders);
            Assert.IsTrue(DefaultFolders.Count == 2);
            WaitForElements(DefaultFolders);
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            List<string> FolderNames = DefaultFolders.Select(i => i.Text).ToList();
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            Assert.IsTrue(FolderNames.Count == 2);
            Assert.IsTrue(FolderNames.Any(x=>x.Contains("Not Indexed")));
            Assert.IsTrue(FolderNames.Any(x => x.Contains("Registration")));
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
        }


        //Creating New Folder 
        public void CreateFolder()
        {
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            WaitForElement(FilesDocsTab);
            FilesDocsTab.Click();
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            WaitForElement(CreateFolderButton);
            CreateFolderButton.Click();
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            WaitForElement(FolderLabel);
            FolderLabel.Click();
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            WaitForElement(FolderInput);
            FolderInput.Click();
            FolderInput.Clear();
            FolderInput.SendKeys("TestFolder");
            WaitForElement(AddFolderButton);
            AddFolderButton.Click();
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            List<string> FolderNames = DefaultFolders.Select(i => i.Text).ToList();
            Thread.Sleep(1000);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(1000);
            Assert.IsTrue(FolderNames.Count == 3);
            Assert.IsTrue(FolderNames.Any(x => x.Contains("Not Indexed")));
            Assert.IsTrue(FolderNames.Any(x => x.Contains("Registration")));
            Assert.IsTrue(FolderNames.Any(x=>x.Contains("TestFolder")));
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
        }
        //upload Registration Document 
        public void UploadRegDocs()
        {
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            WaitForElement(FilesDocsTab);
            FilesDocsTab.Click();
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            UploadFile();
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            List<string> FolderNames = DefaultFolders.Select(i => i.Text).ToList();
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            Assert.IsTrue(FolderNames.Count == 3);
            Assert.IsTrue(FolderNames.Any(x => x.Contains("Not Indexed")));
            Assert.IsTrue(FolderNames.Any(x => x.Contains("Registration")));
            Assert.IsTrue(FolderNames.Any(x => x.Contains("TestFolder")));
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            var RegistrationFolder = DefaultFolders.FirstOrDefault(x => x.Text.Contains("Registration"));
            var NotIndexedFolder = DefaultFolders.FirstOrDefault(x => x.Text.Contains("Not Indexed"));
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            WaitForElement(RegistrationFolder);
            RegistrationFolder.Click();
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            WaitForElements(DocsList);
            int DocListCount = DocsList.Count;
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            Assert.IsTrue(DocListCount >= 0);
            WaitForElement(HomeFolder);
            HomeFolder.Click();
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
        }
        //upload Registration Document 
        public void UploadNotIndexedDocs()
        {
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            WaitForElement(FilesDocsTab);
            FilesDocsTab.Click();
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            UploadFileNoIndex();
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            List<string> FolderNames = DefaultFolders.Select(i => i.Text).ToList();
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            Assert.IsTrue(FolderNames.Count == 3);
            Assert.IsTrue(FolderNames.Any(x => x.Contains("Not Indexed")));
            Assert.IsTrue(FolderNames.Any(x => x.Contains("Registration")));
            Assert.IsTrue(FolderNames.Any(x => x.Contains("TestFolder")));
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            var RegistrationFolder = DefaultFolders.FirstOrDefault(x => x.Text.Contains("Registration"));
            var NotIndexedFolder = DefaultFolders.FirstOrDefault(x => x.Text.Contains("Not Indexed"));
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            WaitForElement(NotIndexedFolder);
            NotIndexedFolder.Click();
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            WaitForElements(DocsList);
            int DocListCount = DocsList.Count;
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            Assert.IsTrue(DocListCount >= 0);
            WaitForElement(HomeFolder);
            HomeFolder.Click();
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
        }
        //Uploading New File
        public void UploadFile()
        {
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            WaitForElement(UploadDocButton);
            UploadDocButton.Click();
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            var fileInfo = new FileInfo(ExtensionMethods.GetAbsolutePath(@"TestData\QuotesData\CRISP install guide.docx"));
            FileInput.SendKeys($"{fileInfo}");
           // FileInput.SendKeys(@"\\tpgfile\NewCompany\IT\Department\QA Team\Current Projects\CRISP\CrispAutomation\CrispData\QuotesData\CRISP install guide.docx");
            Thread.Sleep(1000);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(1000);
            //WaitForElement(DateReceivedLabel);
            //DateReceivedLabel.Click();
            //WaitForElement(DateReceivedInput);
            //DateReceivedInput.Click();
            
            //CloseSpinneronDiv();
            //CloseSpinneronPage();
            
            WaitForElement(DocTypeDropdown1);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(1000);
            DocTypeDropdown1.Click();
            Thread.Sleep(500);
            WaitForElement(SearchDocumentType);
            SearchDocumentType.Click();
            SearchDocumentType.Clear();
            SearchDocumentType.SendKeys("Site Location Plan");
            Thread.Sleep(500);
            SearchDocumentType.SendKeys(OpenQA.Selenium.Keys.Enter);
            Thread.Sleep(500);
            WaitForElements(SearchDocumentTypeInputList);
            if (SearchDocumentTypeInputList.Count >= 1)
            {
                SearchDocumentTypeInputList[0].Click();
                CloseSpinneronDiv();
                CloseSpinneronPage();
                Thread.Sleep(500);
            }
            Thread.Sleep(500);
            WaitForElement(SaveButton);
            SaveButton.Click();
            Thread.Sleep(1000);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
        }

        //Uploading New File with out index
        public void UploadFileNoIndex()
        {
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            WaitForElement(UploadDocButton);
            UploadDocButton.Click();
            Thread.Sleep(1500);
            var fileInfo = new FileInfo(ExtensionMethods.GetAbsolutePath(@"TestData\QuotesData\CRISP install guide.docx"));
            FileInput.SendKeys($"{fileInfo}");
           // FileInput.SendKeys(@"\\tpgfile\NewCompany\IT\Department\QA Team\Current Projects\CRISP\CrispAutomation\CrispData\QuotesData\CRISP install guide.docx");
            Thread.Sleep(1000);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            
            //WaitForElement(DateReceivedLabel);
            //DateReceivedLabel.Click();
            //WaitForElement(DateReceivedInput);
            //DateReceivedInput.Click();
            
            //CloseSpinneronDiv();
            //CloseSpinneronPage();
            
            WaitForElement(SaveButton);
            SaveButton.Click();
            Thread.Sleep(1000);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
        }

        //Notes Section 
        public void NotesSection()
        {
            CreateNote();
            CreateTaskviaNote();
            //AmendTaskviaNote();
            DeleteNote();

        }
        //Create a Note
        public void CreateNote()
        {
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            WaitForElement(NotesTab);
            NotesTab.Click();
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            WaitForElement(AddNotesButton);
            AddNotesButton.Click();
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            WaitForElement(CreateNotesDiv);
            Assert.IsTrue(CreateNotesDiv.Displayed);
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            WaitForElement(NoteTypeDropdown);
            NoteTypeDropdown.Click();
            WaitForElement(NoteTypeDropdownInput);
            NoteTypeDropdownInput.Click();
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            WaitForElement(NotesDetailsInput);
            NotesDetailsInput.SendKeys("Notes Created");
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            WaitForElement(NotesSaveButton);
            NotesSaveButton.Click();
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            WaitForElement(CreateTaskDivFromNotes);
            Assert.IsTrue(CreateTaskDivFromNotes.Displayed);
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            WaitForElement(NoButton);
            NoButton.Click();
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            WaitForElements(NotesList);
            if (NotesList.Count > 0)
            {
                int NotesListCount = NotesList.Count;
                Thread.Sleep(500);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                Thread.Sleep(500);
                Assert.IsTrue(NotesListCount == 1);
                Thread.Sleep(500);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                Thread.Sleep(500);
                NotesList[0].Click();
                Thread.Sleep(500);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                Thread.Sleep(500);
                WaitForElement(NoteName);
                Assert.IsTrue(NoteName.Text == "General Note");
                Thread.Sleep(500);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                Thread.Sleep(500);
                WaitForElement(NoteDeleteButton);
                Assert.IsTrue(NoteDeleteButton.Displayed);
                Assert.IsTrue(AssociatedTaskButton.Count == 0);
            }
        }

        //Creating Task via Note
        public void CreateTaskviaNote()
        {
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            WaitForElement(NotesTab);
            NotesTab.Click();
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            WaitForElement(AddNotesButton);
            AddNotesButton.Click();
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            WaitForElement(CreateNotesDiv);
            Assert.IsTrue(CreateNotesDiv.Displayed);
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            WaitForElement(NoteTypeDropdown);
            NoteTypeDropdown.Click();
            WaitForElement(NoteTypeDropdownInput);
            NoteTypeDropdownInput.Click();
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            WaitForElement(NotesDetailsInput);
            NotesDetailsInput.SendKeys("Notes Created");
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            WaitForElement(NotesSaveButton);
            NotesSaveButton.Click();
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            WaitForElement(CreateTaskDivFromNotes);
            Assert.IsTrue(CreateTaskDivFromNotes.Displayed);
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            WaitForElement(YesButton);
            YesButton.Click();
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            WaitForElement(CreateTaskDiv);
            Assert.IsTrue(CreateTaskDiv.Displayed);
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            WaitForElement(TaskTypeDropdown);
            TaskTypeDropdown.Click();
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            WaitForElement(TaskTypeDropdownInput);
            TaskTypeDropdownInput.Click();
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            WaitForElement(TaskSubTypeDropdown);
            TaskSubTypeDropdown.Click();
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            WaitForElement(TaskSubTypeDropdownInput);
            TaskSubTypeDropdownInput.Click();
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            WaitForElement(DateLabel);
            DateLabel.Click();
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            var DateInput = $"//div[contains(@class,'picker--opened')]//div[@class='picker__box']//tbody//tr//td//div[@aria-label='{FutureDate}']";
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            Driver.FindElement(By.XPath(DateInput)).Click();
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            WaitForElement(TimeLabel);
            TimeLabel.Click();
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            WaitForElements(TimeList);
            TimeList[2].Click();
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            WaitForElement(HighPriorityLabel);
            HighPriorityLabel.Click();
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            WaitForElement(NotesLabel);
            NotesLabel.Click();
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            WaitForElement(NotesInput);
            NotesInput.Click();
            NotesInput.Clear();
            NotesInput.SendKeys("AddingTask");
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            WaitForElement(SaveButton);
            SaveButton.Click();
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            if (NotesList.Count > 0)
            {
                int NotesListCount = NotesList.Count;
                Thread.Sleep(500);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                Thread.Sleep(500);
                Assert.IsTrue(NotesListCount == 2);
                Thread.Sleep(500);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                Thread.Sleep(500);
                WaitForElement(NotesList[0]);
                WaitForElementToClick(NotesList[0]);
                NotesList[0].Click();
                Thread.Sleep(500);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                Thread.Sleep(500);
                WaitForElement(NoteName);
                Assert.IsTrue(NoteName.Text == "General Note");
                Thread.Sleep(500);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                Thread.Sleep(500);
                WaitForElement(NoteDeleteButton);
                Assert.IsTrue(NoteDeleteButton.Displayed);
                Assert.IsTrue(AssociatedTaskButton.Count == 1);
            }
            //Verify the task has been created 
            VerifyTaskCreation();
        }

        //Verify the Task which is created via note
        public void VerifyTaskCreation()
        {
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            WaitForElement(TaskTab);
            TaskTab.Click();
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            WaitForElements(TaskList);
            if (TaskList.Count > 0)
            {
                WaitForElement(TaskList[0]);
                TaskList[0].Click();
                CloseSpinneronDiv();
                CloseSpinneronPage();
                Thread.Sleep(500);
                WaitForElement(TaskName);
                //Assert.IsTrue(TaskName.Text == "Quote - Input Quote application");
            }
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
        }

        //Creating Task via Note
        public void AmendTaskviaNote()
        {
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            WaitForElement(NotesTab);
            NotesTab.Click();
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            WaitForElements(NotesList);
            if (NotesList.Count > 0)
            {
                for (int i = 0; i < NotesList.Count; i++)
                {
                    int NotesListCount = NotesList.Count;
                    Thread.Sleep(500);
                    CloseSpinneronDiv();
                    CloseSpinneronPage();
                    Thread.Sleep(500);
                    Assert.IsTrue(NotesListCount == 2);
                    Thread.Sleep(500);
                    CloseSpinneronDiv();
                    CloseSpinneronPage();
                    Thread.Sleep(500);
                    NotesList[i].Click();
                    Thread.Sleep(500);
                    CloseSpinneronDiv();
                    CloseSpinneronPage();
                    Thread.Sleep(500);
                    WaitForElement(NoteName);
                    Assert.IsTrue(NoteName.Text == "General Note");
                    Thread.Sleep(500);
                    CloseSpinneronDiv();
                    CloseSpinneronPage();
                    Thread.Sleep(500);
                    WaitForElement(NoteDeleteButton);
                    Assert.IsTrue(NoteDeleteButton.Displayed);
                    if (AssociatedTaskButton.Count > 0)
                    {
                        Thread.Sleep(500);
                        CloseSpinneronDiv();
                        CloseSpinneronPage();
                        Thread.Sleep(500);
                        Assert.IsTrue(AssociatedTaskButton.Count == 1);
                        AssociatedTaskButton[0].Click();
                        Thread.Sleep(500);
                        CloseSpinneronDiv();
                        CloseSpinneronPage();
                        Thread.Sleep(500);
                        WaitForElement(AmendTaskDiv);
                        Assert.IsTrue(AmendTaskDiv.Displayed);
                        WaitForElement(TaskSubTypeDropdown);
                        TaskSubTypeDropdown.Click();
                        Thread.Sleep(500);
                        CloseSpinneronDiv();
                        CloseSpinneronPage();
                        Thread.Sleep(500);
                        WaitForElement(AmendTaskSubTypeDropdownInput);
                        AmendTaskSubTypeDropdownInput.Click();
                        Thread.Sleep(500);
                        CloseSpinneronDiv();
                        CloseSpinneronPage();
                        Thread.Sleep(500);
                        CloseSpinneronDiv();
                        CloseSpinneronPage();
                        Thread.Sleep(500);
                        WaitForElement(HighPriorityLabel);
                        HighPriorityLabel.Click();
                        Thread.Sleep(500);
                        CloseSpinneronDiv();
                        CloseSpinneronPage();
                        Thread.Sleep(500);
                        WaitForElement(AmendNotesLabel);
                        AmendNotesLabel.Click();
                        Thread.Sleep(500);
                        CloseSpinneronDiv();
                        CloseSpinneronPage();
                        Thread.Sleep(500);
                        WaitForElement(AmendNotesInput);
                        AmendNotesInput.Click();
                        AmendNotesInput.Clear();
                        AmendNotesInput.SendKeys("AddingTask");
                        Thread.Sleep(500);
                        CloseSpinneronDiv();
                        CloseSpinneronPage();
                        Thread.Sleep(500);
                        WaitForElement(SaveButton);
                        SaveButton.Click();
                        Thread.Sleep(500);
                        CloseSpinneronDiv();
                        CloseSpinneronPage();
                        Thread.Sleep(500);
                    }
                }
            }

            //Verify the task has been created 
            VerifyTaskAmendment();
        }
        //Verify the Task which is created via note
        public void VerifyTaskAmendment()
        {
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            WaitForElement(TaskTab);
            TaskTab.Click();
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            WaitForElements(TaskList);
            if (TaskList.Count > 0)
            {
                for (int i = 0; i < TaskList.Count; i++)
                {
                    TaskList[i].Click();
                    CloseSpinneronDiv();
                    CloseSpinneronPage();
                    Thread.Sleep(500);
                    WaitForElement(TaskName);
                    //Assert.IsTrue(TaskName.Text == "Quote - Enter New Quote Application");
                    //Assert.IsTrue(TaskName.Text == "Quote - Input Quote application");
                    CloseSpinneronDiv();
                    CloseSpinneronPage();
                    Thread.Sleep(500);
                }

            }

        }

        //Verify the deletion of a note
        public void DeleteNote()
        {
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            WaitForElement(NotesTab);
            NotesTab.Click();
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            WaitForElements(NotesList);
            if (NotesList.Count > 0)
            {
                int actualNotesList = NotesList.Count;
                for (int i = 0; i < actualNotesList; i++)
                {
                    Thread.Sleep(500);
                    CloseSpinneronDiv();
                    CloseSpinneronPage();
                    Thread.Sleep(500);
                    var DeleteNoteItem = NotesList[0];
                    Thread.Sleep(500);
                    CloseSpinneronDiv();
                    CloseSpinneronPage();
                    Thread.Sleep(500);
                    WaitForElement(DeleteNoteItem);
                    Thread.Sleep(500);
                    CloseSpinneronDiv();
                    CloseSpinneronPage();
                    Thread.Sleep(500);
                    DeleteNoteItem.Click();
                    Thread.Sleep(500);
                    CloseSpinneronDiv();
                    CloseSpinneronPage();
                    Thread.Sleep(500);
                    WaitForElement(NoteDeleteButton);
                    NoteDeleteButton.Click();
                    Thread.Sleep(500);
                    CloseSpinneronDiv();
                    CloseSpinneronPage();
                    Thread.Sleep(500);
                    WaitForElement(DeleteYesButton);
                    DeleteYesButton.Click();
                    Thread.Sleep(500);
                    CloseSpinneronDiv();
                    CloseSpinneronPage();
                    Thread.Sleep(500);
                }
            }

        }
    }
}

