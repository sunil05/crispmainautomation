﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CrispAutomation.Pages;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.PageObjects;
using System.Threading;
using TechTalk.SpecFlow.Plugins;

namespace CrispAutomation.Pages
{
    public class ManageCompanyListPage : Support.Pages
    {
        public IWebDriver wdriver;

        public ManageCompanyListPage(ISearchContext driver)
        {
            PageFactory.InitElements(driver, this);
            wdriver = (RemoteWebDriver)driver;
        }

        [FindsBy(How = How.XPath, Using = "//crisp-list[@list-title='Type of Company']//ul[@ref='theList']//li[contains(@class,'au-target collection-item')]")]
        public IList<IWebElement> TypeofCompanyRoleList;

        [FindsBy(How = How.XPath, Using = "//crisp-list[@list-title='Companies']//ul[@ref='theList']//li[contains(@class,'au-target collection-item')]")]
        public IList<IWebElement> SelectedCompanyRoleList;

        [FindsBy(How = How.XPath,
            Using = "//ul[@id='crisp-list-2']/li[4]/crisp-list-item/div/crisp-list-item-details")]
        public IWebElement CompaniesValue;

        [FindsBy(How = How.XPath, Using = "//crisp-list[@list-title='Type of Company']/ul[@ref='theList']/li[6]")]
        public IWebElement InsurerType;

        [FindsBy(How = How.XPath, Using = "//crisp-list-item//a//crisp-list-item-action//div//crisp-button//span//button[text()='Select']")]
        public IList<IWebElement> SelectButton;

        [FindsBy(How = How.XPath, Using = "//crisp-header-button[@icon='save'][@class='au-target']//span//button[@class='au-target waves-effect waves-light btn-flat']")]
        public IWebElement SaveButton;

        [FindsBy(How = How.XPath, Using = "//ul[@class='au-target collection has-header']//li[@class='au-target collection-item']//crisp-list-item[@class='au-target']//a//crisp-list-item-action//div//crisp-button[@icon='trash']//span//button[@class='au-target waves-effect waves-light btn-flat']")]
        public IList<IWebElement> RemoveButtons;

        public void VerifyDefaultCompanyList()
        {
            Assert.True(TypeofCompanyRoleList.Count > 0);
            foreach (var companytypes in TypeofCompanyRoleList)
            {
                Assert.True(companytypes.Displayed);
            }
            WaitForElement(InsurerType);
            InsurerType.Click();
            if (RemoveButtons.Count > 0)
            {
                for (int i = 0; i <= RemoveButtons.Count; i++)
                {
                    RemoveButtons[0].Click();
                    //Thread.Sleep(500);

                }
            }
            //Thread.Sleep(1000);
        }

        public void EditSelectedCompanyRole()
        {
            //Thread.Sleep(1000);
            WaitForElement(SaveButton);
            SaveButton.Click();
            //Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(500);
            WaitForElements(RemoveButtons);
            //  int actualremovebutton = RemoveButtons.Count();
            for (int i = 0; i <= RemoveButtons.Count(); i++)
            {
                WaitForElements(RemoveButtons);
                WaitForElementToClick(RemoveButtons[0]);
                RemoveButtons[0].Click();
                //Thread.Sleep(500);
            }
            WaitForElement(SaveButton);
            SaveButton.Click();
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(1500);
        }
    }
}

