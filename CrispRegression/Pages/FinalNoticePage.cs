﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using CrispAutomation.Features;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.PageObjects;
using System.Configuration;
using System.Web.UI.WebControls.Expressions;
using OpenQA.Selenium.Interactions;

namespace CrispAutomation.Pages
{
    public class FinalNoticePage : Support.Pages
    {
        public IWebDriver wdriver;
        public FinalNoticePage(ISearchContext driver)
        {
            PageFactory.InitElements(driver, this);
            wdriver = (RemoteWebDriver)driver;
        }

        [FindsBy(How = How.XPath, Using = "//a/span[text()='Sites']")]
        public IWebElement Sites;

        [FindsBy(How = How.CssSelector, Using = "div > div > input[placeholder='Search']")]
        public IWebElement Search;

        [FindsBy(How = How.XPath, Using = "//div/div/button[text()='Search']")]
        public IWebElement SearchButton;

        [FindsBy(How = How.XPath, Using = "//div/label[@for='allSites']")]
        public IWebElement AllSitesCheckbox;

        [FindsBy(How = How.XPath, Using = "//div/table/tbody/tr/th/a[1]")]
        public IWebElement OrderRef;

        [FindsBy(How = How.XPath, Using = "//aside[@id='leftsidebar']//div//div//ul//li//a//span[text()='Inspections and Updates']")]
        public IWebElement InspectionandUpdates;

        [FindsBy(How = How.XPath, Using = "//section[@class='content']//div[@class='card']//a[text()='Add']")]
        public IWebElement InspectionsAddButton;

        [FindsBy(How = How.XPath, Using = "//label[@for='InitialPage_TravelledToSite_true'][text()='Yes']")]
        public IWebElement TravelToSite;

        [FindsBy(How = How.XPath, Using = "//label[@for='InitialPage_SiteInspectionComplete_true'][text()='Yes']")]
        public IWebElement SiteInspectionComplete;

        [FindsBy(How = How.XPath, Using = "//form[@id='newSurveyorUpdateForm']//div[@data-crisp-visible-test='TravelledToSite == true']//div[@class='form-line error']//input[2]")]
        public IWebElement InspectionDate;

        [FindsBy(How = How.XPath, Using = "//input[@id='InitialPage_SiteVisitDetails_Minutes']")]
        public IWebElement SiteVisitDuration;

        [FindsBy(How = How.XPath, Using = "//div[@id='savebar']//button[contains(@class,'btn btn-success btn-lg pull-right')][text()='Next']")]
        public IWebElement NextButton;

        //Sign Off BC elements 

        [FindsBy(How = How.XPath, Using = "//div[@id='SummaryPage_Blocks']//label[contains(@for,'Inspected')]")]
        public IWebElement InspectedBlocks;

        [FindsBy(How = How.XPath, Using = "//div[@id='SummaryPage_Plots']//table//tbody//tr//td//label[contains(@for,'Inspected')]")]
        public IList<IWebElement> InspectedPlots;

        [FindsBy(How = How.XPath, Using = "//div[@id='SummaryPage_Plots']//table//tbody//tr//td//label[contains(@for,'SignOffWarranty')]")]
        public IList<IWebElement> SignOffWarranty;

        [FindsBy(How = How.XPath, Using = "//div[@id='SummaryPage_Plots']//table//tbody//tr//td//input[contains(@name,'SignOffBuildingControl')][@type='checkbox']")]
        public IList<IWebElement> SignoffBC;

        [FindsBy(How = How.XPath, Using = "//div[@id='SummaryPage_Plots']//table//tbody//tr//td//label[contains(@for,'SignOffBuildingControl')]")]
        public IList<IWebElement> SignoffBCLabel;

        [FindsBy(How = How.XPath, Using = "//div[@id='SummaryPage_Plots']//tbody//td//button[contains(@data-id,'UpdatedStageOfWorks')]")]
        public IWebElement UpdatedStagesOfWorks;

        [FindsBy(How = How.XPath, Using = "//div[@id='SummaryPage_Plots']//div[@class='dropdown-menu open'] //ul[@class='dropdown-menu inner']//li//a//span[text()='Pre-Handover']")]
        public IWebElement UpdatedStagesOfWorksInput;

        [FindsBy(How = How.XPath, Using = "//textarea[@name='SummaryPage.OrderDetails.InspectionComments[PreHandover].Comment']")]
        public IWebElement PGHandoverComment;

        [FindsBy(How = How.XPath, Using = "//textarea[@name='SummaryPage.OrderDetails.InspectionComments[PreHandover].FutureGuidance']")]
        public IWebElement PGHandoverFutureGuidance;

        [FindsBy(How = How.XPath, Using = "//h2[contains(text(),'Assessment & Scores')]")]
        public IWebElement AssessmentandScores;

        [FindsBy(How = How.XPath, Using = "//div[contains(@class,'br-theme-bootstrap-stars')]//div[@class='br-widget']/a[5]")]
        public IList<IWebElement> RatingStars;

        [FindsBy(How = How.XPath, Using = "//textarea[contains(@id,'_Comment)')]")]
        public IList<IWebElement> RatingComment;

        [FindsBy(How = How.XPath, Using = "//div[@class='btn-group bootstrap-select form-control']//button//span[text()='Nothing selected']")]
        public IList<IWebElement> SiteStatusDropdowns;

        [FindsBy(How = How.XPath, Using = "//div[contains(@class,'open')]//div[@class='dropdown-menu open']//ul[@class='dropdown-menu inner']//li[@data-original-index='1']")]
        public IWebElement SiteStatusDropdownInput;

        [FindsBy(How = How.XPath, Using = "//div[@class='alert alert-info']/button[text()='Save']")]
        public IWebElement SaveButton;

        [FindsBy(How = How.XPath, Using = "//button[@type='submit'][text()='Send']")]
        public IWebElement SendEmailButton;

        //Send Final Notice Elements 

        [FindsBy(How = How.CssSelector, Using = "span > i[class='au-target fa fa-search']")]
        public IWebElement SearchOption;

        [FindsBy(How = How.XPath, Using = "//A[@click.delegate='subItem.onClick()'][text()='Sites']")]
        public IWebElement SiteOption;

        [FindsBy(How = How.XPath, Using = "//crisp-input-bool[@label='Include quotes?']/label")]
        public IWebElement IncludeQuotes;

        [FindsBy(How = How.XPath, Using = "//crisp-input-bool[@label='Include leads?']/label")]
        public IWebElement IncludeLeads;

        [FindsBy(How = How.XPath, Using = "//crisp-input-bool[@label='Include orders?']/label")]
        public IWebElement IncludeOrders;

        [FindsBy(How = How.XPath, Using = "//crisp-input-bool[contains(@label,'Include')]/label")]
        public IList<IWebElement> IncludeCheckBoxes;

        [FindsBy(How = How.XPath, Using = "//crisp-input-text[@label='Reference']/div/label")]
        public IWebElement ReferenceLabel;

        [FindsBy(How = How.XPath, Using = "//crisp-input-text[@label='Reference']/div/input")]
        public IWebElement ReferenceInput;

        [FindsBy(How = How.XPath, Using = "//ul[@ref='theList']/li/crisp-list-item/div")]
        public IList<IWebElement> OrderList;

        [FindsBy(How = How.XPath, Using = "//ul[@ref='theList']//li[@class='au-target collection-item']//crisp-list-item//div//crisp-list-item-title[@class='au-target']//div")]
        public IList<IWebElement> OrderItems;

        public IWebElement OrderRefItem => OrderItems.FirstOrDefault(x => x.Text.Contains(Statics.OrderNumber));
        public IWebElement SendFinalNoticeOption => Dashboardpage.ActiveBannerList.FirstOrDefault(x => x.Text.Contains("SEND FINAL NOTICE"));

        // public IWebElement SendFinalNoticeButton => DashboardPage.ActiveBannerList.FirstOrDefault(x => x.Text.Contains("SEND FINAL NOTICE"));

        [FindsBy(How = How.XPath, Using = "//crisp-dialog[@class='au-target']//div[@class='au-target modal dialog brand pg modal-fixed-footer modal-has-header modal-full-bleed modal-tall modal-wide']")]
        public IWebElement FinalNoticeDialogue;

        [FindsBy(How = How.XPath, Using = "//crisp-wizard-step[@step-title='File Review']/file-review/crisp-card/crisp-card-content/div/div/div/file-review-results")]
        public IWebElement FileReviewSection;

        [FindsBy(How = How.XPath, Using = "//crisp-tabs/div/ul/li/span/a[contains(text(),'Blocking (0)')]")]
        public IWebElement Blockers;

        [FindsBy(How = How.XPath, Using = "//crisp-card-content/div/div[contains(text(),'There are no companies or individuals with blocks')]")]
        public IWebElement NoBlockers;

        [FindsBy(How = How.XPath, Using = "//crisp-tabs/div/ul/li/span/a[contains(text(),'Conditions (0)')]")]
        public IWebElement Conditions;

        [FindsBy(How = How.XPath, Using = "//crisp-card-content/div/div[contains(text(),'There are no Conditions')]")]
        public IWebElement NoConditions;

        [FindsBy(How = How.XPath, Using = "//crisp-tabs/div/ul/li/span/a[contains(text(),'Fees (0)')]")]
        public IWebElement Fees;

        [FindsBy(How = How.XPath, Using = "//crisp-tabs/div/ul/li/span/a[contains(text(),'Defects (0)')]")]
        public IWebElement Defects;

        [FindsBy(How = How.XPath, Using = "//crisp-footer-button[@class='au-target']/button[text()='Next']")]
        public IWebElement FinalNoticeNextButton;

        [FindsBy(How = How.XPath, Using = "//crisp-input-radio/div/ul/li/label[contains(text(),'Override Conditions/Blocks & Send')]")]
        public IList<IWebElement> OverrideConditionsRadioButton;

        [FindsBy(How = How.XPath, Using = "//crisp-wizard-step[@class='au-target active']//complete//crisp-card//crisp-card-content//div//p[text()='There are no blocks on issuing the Final Notice for the selected plots']")]
        public IList<IWebElement> ConfirmDetails;

        [FindsBy(How = How.XPath, Using = "//crisp-input-textarea[@label='Override comments']//div//label")]
        public IWebElement CommentsLabel;

        [FindsBy(How = How.XPath, Using = "//crisp-input-textarea[@label='Override comments']//div//textarea")]
        public IWebElement CommentsInput;

        [FindsBy(How = How.XPath, Using = "//div//correspondence-email//div//crisp-input-html//a[@href='https://www.premierguarantee.co.uk/']//img")]
        public IList<IWebElement> FNtoClient;

        [FindsBy(How = How.XPath, Using = "//div//correspondence-email//div//crisp-input-html//a[@href='https://www.premierguarantee.co.uk/']//img")]
        public IList<IWebElement> FNtoLA;

        [FindsBy(How = How.XPath, Using = "//crisp-wizard-step[@class='au-target active']//crisp-tabs/div/ul/li[@class='au-target tab']/span/a[contains(text(),'Email')]")]
        public IWebElement EmailTab;

        [FindsBy(How = How.XPath, Using = "//crisp-wizard-step[@class='au-target active']//correspondence-embed/crisp-tabs/div/div/div/crisp-card/crisp-card-content/div/correspondence-email/div/div[1]/crisp-input-bool/label[text()='Send email']")]
        public IWebElement SendEmailCheckBox;

        [FindsBy(How = How.XPath, Using = "//crisp-wizard-step[@class='au-target active']//crisp-tabs/div/ul/li[@class='au-target tab']/span/a[contains(text(),'Letter')]")]
        public IWebElement LetterTab;

        [FindsBy(How = How.XPath, Using = "//crisp-wizard-step[@class='au-target active']//crisp-input-bool/label[text()='Send letter']")]
        public IWebElement SendLetterCheckBox;

        [FindsBy(How = How.XPath, Using = "//crisp-footer[@class='au-target']//crisp-footer-button//button[text()='Send Final Notice']")]
        public IWebElement SendFinalNoticeButton;

        [FindsBy(How = How.XPath, Using = "//crisp-list[@list-title='Processes']//ul[@ref='theList']//li[contains(@class,'au-target collection-item')]//crisp-list-item-details//crisp-display-value//span[@class='au-target value']")]
        public IList<IWebElement> FinalNoticeProcessTitle;


        static DateTime date = DateTime.Today.AddDays(-1); // will give the date for previous day
        private string dateWithFormat = date.ToString("dd/MM/yyyy HH:mm");

        public void SignoffBuildingControl(string OrderRefNumber)
        {
            SiteInspectionPage.InspectionUpdateMain();
        }

        public void SignoffBuildingControlForPartialFinalNotice(string OrderRefNumber)
        {
            SiteInspectionPage.InspectionUpdateForPartialFinalNotice();
        }

        public void SelectFinalNotice()
        {
            //Thread.Sleep(1000);
            if (Dashboardpage.DecisionButton.Count <= 0)
            {
                Dashboardpage.CrispLoginMethod();
            }
            try
            {
                WaitForElementonSurveyor(Dashboardpage.ActionsButton);
                WaitForElementToClick(Dashboardpage.ActionsButton);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                Dashboardpage.ActionsButton.Click();
                Thread.Sleep(500);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                WaitForElement(SendFinalNoticeOption);
                WaitForElementToClick(SendFinalNoticeOption);
                SendFinalNoticeOption.Click();
                Thread.Sleep(500);
                CloseSpinneronDiv();
                CloseSpinneronPage();
            }
            catch (Exception ex)
            {
                Console.WriteLine("Please check Final Notice permissions , User may not have permissions to issue Final Notice,Please Apply Permissions Manually: {0}", ex);
            }

        }

        public void SendFinalNotice()
        {
            CloseSpinneronDiv();
            CloseSpinneronPage();
            SelectFinalNotice();
            CloseSpinneronDiv();
            CloseSpinneronPage();
            WaitForElement(FinalNoticeDialogue);
            Assert.IsTrue(FinalNoticeDialogue.Displayed);
            WaitForElement(FileReviewSection);
            Assert.IsTrue(FileReviewSection.Displayed);
            WaitForElement(Blockers);
            Assert.IsTrue(Blockers.Displayed, "Blockers are opened user can not send Final Notice");
            WaitForElement(NoBlockers);
            Assert.IsTrue(NoBlockers.Displayed, "Blockers are opened user can not send Final Notice");
            WaitForElement(Conditions);
            Assert.IsTrue(Conditions.Displayed, "Conditions are opened user can not send Final Notice");
            WaitForElement(Fees);
            Assert.IsTrue(Fees.Displayed, "Site Fees outstanding user can not send Final Notice");
            WaitForElement(Defects);
            Assert.IsTrue(Defects.Displayed, "Defects are opened user can not send Final Notice");
            NextButtonMethod();
            CloseSpinneronDiv();
            CloseSpinneronPage();
            ConfirmDetailsPage();
            CloseSpinneronDiv();
            CloseSpinneronPage();
            NextButtonMethod();
            WaitForElements(FNtoClient);
            Assert.IsTrue(FNtoClient.Count >= 1);
            NextButtonMethod();
            WaitForElements(FNtoLA);
            Assert.IsTrue(FNtoLA.Count >= 1);
            NextButtonMethod();
            WaitForElement(EmailTab);
            EmailTab.Click();
            WaitForElement(SendEmailCheckBox);
            if (!SendEmailCheckBox.Selected)
            {
                SendEmailCheckBox.Click();
            }
            WaitForElement(LetterTab);
            LetterTab.Click();
            WaitForElement(SendLetterCheckBox);
            if (!SendLetterCheckBox.Selected)
            {
                SendLetterCheckBox.Click();
            }
            WaitForElement(SendFinalNoticeButton);
            SendFinalNoticeButton.Click();
            CloseSpinneronDiv();
            CloseSpinneronPage();
        }

        public void ConfirmDetailsPage()
        {

            if (ConfirmDetails.Count > 0)
            {
                WaitForElements(ConfirmDetails);
                Assert.IsTrue(ConfirmDetails[0].Displayed);
            }
            if (OverrideConditionsRadioButton.Count > 0)
            {
                WaitForElement(OverrideConditionsRadioButton[0]);
                OverrideConditionsRadioButton[0].Click();
                WaitForElement(CommentsLabel);
                CommentsLabel.Click();
                WaitForElement(CommentsInput);
                CommentsInput.SendKeys("ConditionsOverrideConfirmation");

                CloseSpinneronDiv();
            }
        }
        public void NextButtonMethod()
        {
            WaitForElement(FinalNoticeNextButton);
            FinalNoticeNextButton.Click();

        }


        public void AssessmentScoresSection()
        {
            IJavaScriptExecutor js = Driver as IJavaScriptExecutor;
            js.ExecuteScript("arguments[0].scrollIntoView(true);", AssessmentandScores);
            foreach (var eachRatingStar in RatingStars)
            {
                eachRatingStar.Click();
            }

            WaitForElements(RatingComment);
            foreach (var eachComment in RatingComment)
            {
                eachComment.Click();
                eachComment.Clear();
                eachComment.SendKeys("GoodRate");
            }
        }
        public void PartialFinalNoticeStatus()
        {
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(1000);
            WaitForElements(FinalNoticeProcessTitle);
            if (FinalNoticeProcessTitle.Count > 0)
            {
                for (int i = 0; i < FinalNoticeProcessTitle.Count; i++)
                {
                    Assert.IsTrue(FinalNoticeProcessTitle.Any(x => x.Text.Contains("Partial Final Notice")));
                }
            }

        }
        public void FinalNoticeStatus()
        {
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(1000);
            WaitForElements(FinalNoticeProcessTitle);
            if (FinalNoticeProcessTitle.Count > 0)
            {
                for (int i = 0; i < FinalNoticeProcessTitle.Count; i++)
                {
                    Assert.IsTrue(FinalNoticeProcessTitle.Any(x => x.Text.Contains("Final Notice")));
                }
            }

        }

    }
}
