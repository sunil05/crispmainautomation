﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using CrispAutomation.Features;
using CrispAutomation.Support;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.PageObjects;
using static CrispAutomation.Support.ExtensionMethods;
using System.Configuration;

namespace CrispAutomation.Pages
{
    public class AdditionalMethodsPage : Support.Pages
    {
        public IWebDriver wdriver;

        public AdditionalMethodsPage(ISearchContext driver)
        {
            PageFactory.InitElements(driver, this);
        }
        [FindsBy(How = How.XPath, Using = "//crisp-button[@click.delegate='additionalButtonClicked(btn)'][@class='au-target']//button[text()='Add Person']")]
        public IWebElement AddPersonButton;

        [FindsBy(How = How.XPath, Using = "//crisp-button[@click.delegate='additionalButtonClicked(btn)'][@class='au-target']//button[text()='Add Company']")]
        public IWebElement AddCompanyButton;

        [FindsBy(How = How.XPath, Using = "//crisp-button[@click.delegate='additionalButtonClicked(btn)'][@class='au-target']//button[text()='Add Employee']")]
        public IWebElement AddEmployeeButton;
        [FindsBy(How = How.XPath, Using = "//crisp-button[@class='au-target'][@click.delegate='wizard.goForward()']/span/button[text()='Next']")]
        public IWebElement NextButton;

        [FindsBy(How = How.XPath, Using = "//crisp-button[@class='au-target']//button[@ref='button'][text()='Save']")]
        public IWebElement SaveButton;    

        [FindsBy(How = How.CssSelector,
            Using = "body > ai-dialog-container > div > div > crisp-dialog > div > div.modal-content > crisp-wizard > crisp-wizard-step.au-target.active > addresses > div > div > div > crisp-list > crisp-action-button > div > a > i")]
        public IWebElement AddressButton;

        [FindsBy(How = How.XPath, Using = "//crisp-input-text[@label='Enter postcode to search']//div[@class='input-field']//label[@class='au-target active']")]
        public IWebElement PostcodeLabel;

        [FindsBy(How = How.XPath, Using = "//crisp-input-text[@label='Enter postcode to search']//div[@class='input-field']//label[contains(@class,'au-target')]//following-sibling::input")]
        public IWebElement PostcodeInput;

        [FindsBy(How = How.XPath, Using = "//crisp-picker[@label='Address']//div//input[@class='select-dropdown']")]
        public IWebElement AddressDropdown;

        [FindsBy(How = How.XPath, Using = "//ul[@class='dropdown-content select-dropdown active']//li//span[text()='M D Insurance Services Ltd, 2 Shore Lines Building, Shore Road, Merseyside, CH41 1AU']")]
        public IWebElement AddressDropdownInput;

        [FindsBy(How = How.XPath, Using = "//LABEL[@data-error.bind='error'][text()='Address Line 1']")]
        public IWebElement AddressLine1;

        [FindsBy(How = How.XPath, Using = "//crisp-input-text[@label='Address Line 1']//div//input")]
        public IWebElement AddressLine1EnterValue;

        [FindsBy(How = How.XPath, Using = "//crisp-button[1]/span/button[text()='Set Address']")]
        public IWebElement SetAddressButton;
        //Add Person Elements 

        [FindsBy(How = How.XPath, Using = "//LABEL[@class='au-target'][text()='Salutation']")]
        public IWebElement Salutation;

        [FindsBy(How = How.XPath, Using = "//crisp-input-autocomplete[@label='Salutation']/div/input")]
        public IWebElement SalutationFieldEdit;

        [FindsBy(How = How.XPath, Using = "//crisp-input-text[@label='First name']//div//label")]
        public IWebElement PersonFirstName;

        [FindsBy(How = How.XPath, Using = "//crisp-input-text[@label='First name']//div//input")]
        public IWebElement PersonFirstNameInput;

        [FindsBy(How = How.XPath, Using = "//crisp-input-text[@label='Surname']//div//label")]
        public IWebElement PersonSurName;

        [FindsBy(How = How.XPath, Using = "//crisp-input-text[@label='Surname']//div//input")]
        public IWebElement PersonSurNameInput;

        [FindsBy(How = How.XPath, Using = "//crisp-input-text[@label='Email']//div//label")]
        public IWebElement PersonEmail;

        [FindsBy(How = How.XPath, Using = "//crisp-input-text[@label='Email']//div//input")]
        public IWebElement PersonEmailInput;

        [FindsBy(How = How.XPath, Using = "//LABEL[@data-error.bind='error'][text()='Phone']")]
        public IWebElement PhoneNumber;

        [FindsBy(How = How.XPath, Using = "//crisp-input-text[@label='Phone']//div[@class='input-field']//input")]
        public IWebElement PhoneNumberEdit;

        [FindsBy(How = How.XPath, Using = "//LABEL[@data-error.bind='error'][text()='Mobile']")]
        public IWebElement MobileNumber;

        [FindsBy(How = How.XPath, Using = "//crisp-input-text[@label='Mobile']//div[@class='input-field']//input")]
        public IWebElement MobileNumberEdit;

        [FindsBy(How = How.XPath, Using = "//crisp-button/span/button[text()='Enter Address Manually']")]
        public IWebElement EnterManuallyButton;

        [FindsBy(How = How.XPath, Using = "//BUTTON[@ref='button'][text()='Confirm Selection']")]
        public IWebElement ConfirmSelection;

        [FindsBy(How = How.XPath, Using = "//crisp-card-header[@class='au-target']/div/span[text()='Address details']")]
        public IWebElement AddressDetails;

        [FindsBy(How = How.XPath, Using = "//LABEL[@data-error.bind='error'][text()='Facebook']")]
        public IWebElement FaceBookId;

        [FindsBy(How = How.XPath, Using = "//crisp-input-text[@label='Facebook']//div//input")]
        public IWebElement FacebookElementEnter;

        [FindsBy(How = How.CssSelector, Using = "body > ai-dialog-container > div > div > crisp-dialog > div > div.modal-content > crisp-wizard > crisp-wizard-step.au-target.active > social > div > div > div.crisp-row.height-40 > div > crisp-list > crisp-action-button > div > a > i")]
        public IWebElement AddWebsiteButton;

        [FindsBy(How = How.XPath, Using = "//crisp-input-text[@label='Website Address']/div/LABEL[@data-error.bind='error'][text()='Website Address']")]
        public IWebElement WebsiteAddress;

        [FindsBy(How = How.XPath, Using = "//crisp-input-text[@label='Website Address']/div/input[contains(@id,'crisp-input-text')][@ref='input']")]
        public IWebElement WebSiteInputField;

        [FindsBy(How = How.XPath, Using = "//crisp-footer-button[@class='au-target']//button[text()='Add']")]
        public IWebElement AddButton;

        [FindsBy(How = How.CssSelector,
            Using = "body > div.custom-element > router-view > crisp-header > nav > div > crisp-header-avatar > img")]
        public IWebElement Imageperson;

        [FindsBy(How = How.XPath, Using = "//crisp-picker[@selected.bind='phoneCountryCode & validate'][@class='au-target']//div//input[@class='select-dropdown']")]
        public IWebElement PhoneCountry;

        [FindsBy(How = How.XPath, Using = "//ul[@class='dropdown-content select-dropdown active']//li//span[text()='United Kingdom']")]
        public IWebElement PhoneCountryDropdown;

        [FindsBy(How = How.XPath, Using = "//crisp-picker[@selected.bind='mobileCountryCode & validate'][@class='au-target']//div//input[@class='select-dropdown']")]
        public IWebElement MobileCountry;

        [FindsBy(How = How.XPath, Using = "//ul[@class='dropdown-content select-dropdown active']//li//span[text()='United Kingdom']")]
        public IWebElement MobileCountryDropdown;

        [FindsBy(How = How.XPath, Using = "//crisp-button/span/button[text()='Enter Address Using Postcode']")]
        public IWebElement EnterAddressUsingPostcodeButton;

        //Add Company Elements 

        [FindsBy(How = How.XPath, Using = "//crisp-wizard-step[@class='au-target active']//company-name//crisp-input-text//label[text()='Enter the name of the company you wish to create']")]
        public IWebElement CompanyNameLabel;

        [FindsBy(How = How.XPath, Using = "//crisp-wizard-step[@class='au-target active']//company-name//crisp-input-text[@label='Enter the name of the company you wish to create']//div//input")]
        public IWebElement CompanyNameInput;

        [FindsBy(How = How.XPath, Using = "//crisp-input-number[@label='Year of registration']//div//label[text()='Year of registration']")]
        public IWebElement YearOFRegistration;

        [FindsBy(How = How.XPath, Using = "//crisp-input-number[@label='Year of registration']//div//input")]
        public IWebElement YearOFRegistrationInput;

        [FindsBy(How = How.XPath, Using = "//crisp-list[@list-title='Results of Search from Existing Companies in CRISP']//ul//li[@class='au-target collection-item']")]
        public IList<IWebElement> ExistingCompanyList;

        [FindsBy(How = How.XPath, Using = "//crisp-list[@list-title='Companies House matches']//ul//li[@class='au-target collection-item']")]
        public IList<IWebElement> ExistingCompanyListFromCompaniesHouse;

        [FindsBy(How = How.XPath, Using = "//button[@ref='button'][text()='No Existing Match Found']")]
        public IWebElement NoExistingMatchFound;

        [FindsBy(How = How.XPath, Using = "//button[@ref='button'][text()='No Match Found']")]
        public IWebElement NoMatchFound;

        [FindsBy(How = How.XPath, Using = "//crisp-input-text[@label='Name']/div/LABEL[@data-error.bind='error'][text()='Name']")]
        public IWebElement CompanyName;

        [FindsBy(How = How.XPath, Using = "//crisp-input-text[@label='Name']/div/input[@ref='input'][@type='text']")]
        public IWebElement ManualCompanyNameInput;

        [FindsBy(How = How.XPath, Using = "//LABEL[@data-error.bind='error'][text()='Year of registration']")]
        public IWebElement YearOfRegistration;

        [FindsBy(How = How.XPath, Using = "//crisp-input-number[@label='Year of registration']//input")]
        public IWebElement EnterYearOfRegistration;

        [FindsBy(How = How.XPath, Using = "//crisp-input-text[@label='Registration number (e.g. AB123456 or 12345678)']//div//label")]
        public IWebElement RegistartionNumber;

        [FindsBy(How = How.XPath, Using = "//crisp-input-text[@label='Registration number (e.g. AB123456 or 12345678)']//div//input")]
        public IWebElement EnterRegistrationNumber;

        [FindsBy(How = How.XPath, Using = "//crisp-picker[@label='Legal form type']/div/div/input")]
        public IWebElement LegalFormType;

        [FindsBy(How = How.XPath, Using = "//ul[@class='dropdown-content select-dropdown active']/li/span[text()='Limited Company - LTD']")]
        public IWebElement LegalFormTypeDropdown;

        [FindsBy(How = How.XPath, Using = "//crisp-card/crisp-card-content/div/crisp-input-bool/label[text()='Is a VIP?']")]
        public IWebElement VIPCheckBox;

        [FindsBy(How = How.XPath, Using = "//crisp-card/crisp-card-content/div/crisp-input-bool/label[text()='Has enhanced SH insolvency endorsement?']")]
        public IWebElement EndorseMentCheckBox;

        [FindsBy(How = How.XPath, Using = "//crisp-button[@enabled.bind='canGoForward'][@click.delegate='goForward()'][@title='Move to the next step of creating a company']/span/button[@ref='button'][text()='Next'][@class='au-target waves-effect waves-light btn-flat']")]
        public IWebElement NextButtonOnCompany;

        [FindsBy(How = How.XPath, Using = "//crisp-wizard-step[@step-title='Sales & Marketing']//sales-and-marketing//div[@class='card-title']//span[text()='Sales & Marketing']")]
        public IWebElement SalesAndMarketingTitle;

        [FindsBy(How = How.XPath, Using = "//crisp-wizard-step[@step-title='Sales & Marketing']//sales-and-marketing//crisp-card-content[@class='au-target']//crisp-input-bool//label")]
        public IList<IWebElement> CallsandEmailsList;

        [FindsBy(How = How.XPath, Using = "//crisp-action-button[@icon='add'][@click.delegate='addOffice()']/div/a/i[text()='add']")]
        public IWebElement AddOfficeButton;

        [FindsBy(How = How.XPath, Using = "//crisp-action-button[@icon='add'][@click.delegate='addEmployee()']/div/a/i")]
        public IWebElement addEmployeeButtononCompanyWizard;

        [FindsBy(How = How.XPath, Using = "//div[@class='modal-content']/crisp-input-text[@label='Name']/div/label[text()='Name']")]
        public IWebElement OfficeName;

        [FindsBy(How = How.XPath, Using = "//div[@class='modal-content']/crisp-input-text[@label='Name']/div/input[@ref='input'][@type='text']")]
        public IWebElement OfficeNameInput;

        [FindsBy(How = How.XPath, Using = "//crisp-input-object//div//label[text()='Address']")]
        public IWebElement OfficeAdress;

        [FindsBy(How = How.XPath, Using = "//crisp-input-bool[@label='Office set as registered address.']//label")]
        public IWebElement RegisteredAddress;

        [FindsBy(How = How.XPath, Using = "//crisp-input-bool[@label='Office set as default address.']//label")]
        public IWebElement DefaultAddress;

        [FindsBy(How = How.XPath, Using = "//crisp-input-text[@label='Email']/div/label[text()='Email']")]
        public IWebElement Email;

        [FindsBy(How = How.XPath, Using = "//crisp-input-text[@label='Email']/div/input[@ref='input']")]
        public IWebElement EmailInput;

        [FindsBy(How = How.XPath, Using = "//crisp-picker[@selected.bind='countryCode & validate'][@class='au-target']//div//input[@class='select-dropdown']")]
        public IWebElement PhoneCountryoncompanywizard;

        [FindsBy(How = How.XPath, Using = "//crisp-input-text[@label='${label} Number']//div//label[text()='Phone Number']")]
        public IWebElement PhoneNumberoncompanywizard;

        [FindsBy(How = How.XPath, Using = "//crisp-input-text[contains(@value.bind,'phoneNumber')]/div[@class='input-field']/input[@ref='input'][@type='text'][contains(@id,'crisp-input-text')]")]
        public IWebElement PhoneNumberInput;

        [FindsBy(How = How.XPath, Using = "//crisp-input-text[contains(@label,'Number')]/div/label[text()='Phone Number']")]
        public IWebElement EmpPhone;

        [FindsBy(How = How.XPath, Using = "//crisp-input-text[contains(@label,'Number')]/div/label[text()='Mobile Number']")]
        public IWebElement EmpMobile;

        [FindsBy(How = How.XPath, Using = "//div/div[2]/div/crisp-input-text[contains(@value.bind,'phoneNumber')]/div[@class='input-field']/input[@ref='input'][@type='text'][contains(@id,'crisp-input-text')]")]
        public IWebElement EmpMobileInput;

        [FindsBy(How = How.XPath, Using = "//div/div[1]/div/crisp-input-text[contains(@value.bind,'phoneNumber')]/div[@class='input-field']/input[@ref='input'][@type='text'][contains(@id,'crisp-input-text')]")]
        public IWebElement EmpPhoneInput;

        [FindsBy(How = How.XPath, Using = "//crisp-input-number[@label='Phone Extension']/div/label[text()='Phone Extension']")]
        public IWebElement EmpPhoneExtension;

        [FindsBy(How = How.XPath, Using = "//crisp-input-number[@label='Phone Extension']/div/input")]
        public IWebElement EmpPhoneExtensionInput;

        [FindsBy(How = How.XPath, Using = "//LABEL[@data-error.bind='error'][text()='Premier Guarantee account manager']")]
        public IWebElement PGAccountManager;

        [FindsBy(How = How.XPath, Using = "//crisp-card-content//ul//li[@class='au-target collection-item']")]
        public IList<IWebElement> PGAccountPerson;

        [FindsBy(How = How.XPath, Using = "//LABEL[@data-error.bind='error'][text()='LABC account manager']")]
        public IWebElement LABCAccountManager;

        [FindsBy(How = How.XPath, Using = "//crisp-card-content//ul//li[@class='au-target collection-item']")]
        public IList<IWebElement> LABCAccountPerson;

        [FindsBy(How = How.XPath, Using = "//LABEL[@data-error.bind='error'][text()='CSU account manager']")]
        public IWebElement CSUAccountManager;

        [FindsBy(How = How.XPath, Using = "//crisp-card-content//ul//li[@class='au-target collection-item']")]
        public IList<IWebElement> CSUAccountPerson;

        [FindsBy(How = How.XPath, Using = "//crisp-button[@enabled.bind='isOfficeDataValid']/span/button[@class='au-target waves-effect waves-light btn']")]
        public IWebElement AddOfficeDataButton;

        [FindsBy(How = How.XPath, Using = "//crisp-input-bool[contains(@label,'Opt')]//label")]
        public IList<IWebElement> OptCalls;

        [FindsBy(How = How.XPath, Using = "//crisp-input-bool[contains(@label,'TPS')]//label")]
        public IWebElement NotedOnTPS;

        //Employee Elements

        [FindsBy(How = How.XPath, Using = "//crisp-action-button[@class='au-target'][@click.delegate='addEmployee()']//a//i[text()='add']")]
        public IWebElement AddEmpButtonOnCompanyWizard;

        [FindsBy(How = How.XPath, Using = "//LABEL[@class='au-target'][text()='Title']")]
        public IWebElement EmpTitleLabel;

        [FindsBy(How = How.XPath, Using = "//crisp-input-autocomplete[@label='Title']/div/input")]
        public IWebElement EmpTitleInput;

        [FindsBy(How = How.XPath, Using = "//crisp-input-text[@label='First name']//LABEL[@class='au-target'][text()='First name']")]
        public IWebElement EmpFirstNameLabel;

        [FindsBy(How = How.XPath, Using = "//crisp-input-text[@label='First name']//input")]
        public IWebElement EmpFirstNameInput;

        [FindsBy(How = How.XPath, Using = "//crisp-input-text[@label='Surname']//LABEL[@class='au-target'][text()='Surname']")]
        public IWebElement EmpSurNameLabel;

        [FindsBy(How = How.XPath, Using = "//crisp-input-text[@label='Surname']//input")]
        public IWebElement EmpSurNameInput;

        [FindsBy(How = How.XPath, Using = "//crisp-input-text[@label='Suffix']//LABEL[@class='au-target'][text()='Suffix']")]
        public IWebElement EmpSuffixLabel;

        [FindsBy(How = How.XPath, Using = "//crisp-input-text[@label='Suffix']//input")]
        public IWebElement EmpSuffixInput;

        [FindsBy(How = How.XPath, Using = "//crisp-input-text[@label='Email']//LABEL[@class='au-target'][text()='Email']")]
        public IWebElement EmpEmailLabel;

        [FindsBy(How = How.XPath, Using = "//crisp-input-text[@label='Email']//input")]
        public IWebElement EmpEmailidInput;

        [FindsBy(How = How.XPath, Using = "//crisp-input-bool[@label='Company Director']//LABEL[@class='au-target'][text()='Company Director']")]
        public IWebElement EmpCompanyDirector;

        [FindsBy(How = How.XPath, Using = "//crisp-input-bool[@label='Purchasing Decision Maker']//LABEL[@class='au-target'][text()='Purchasing Decision Maker']")]
        public IWebElement EmpPurchasingDecisionMaker;

        [FindsBy(How = How.XPath, Using = "//crisp-input-text[@label='Job title']//div//label")]
        public IWebElement EmpJobTitle;

        [FindsBy(How = How.XPath, Using = "//crisp-input-text[@label='Job title']//div//input")]
        public IWebElement EmpJobTitleInput;

        [FindsBy(How = How.XPath, Using = "//label[@click.delegate='labelClicked()'][@data-error='Primary Office is invalid']")]
        public IWebElement EmpPrimaryOffice;

        [FindsBy(How = How.XPath, Using = "//crisp-list[@source.two-way='offices']//ul//li[@click.delegate='clicked(item)'][@class='au-target collection-item']")]
        public IWebElement EmpChooseOffice;

        [FindsBy(How = How.XPath, Using = "//crisp-button[@enabled.bind='hasSelection']/span/button")]
        public IWebElement SelectButton;

        [FindsBy(How = How.XPath, Using = "//crisp-button[@click.delegate='save()']/span//button[@ref='button'][text()='Add']")]
        public IWebElement Add;

        [FindsBy(How = How.XPath, Using = "//crisp-input-object[@icon.bind='icon']//label[text()='Offices']//parent::div//i[@class='fa fa-plus add action au-target']")]
        public IWebElement Offices;

        [FindsBy(How = How.XPath, Using = "//div[1]/div/crisp-picker[@selected.bind='countryCode & validate']/div/div/input")]
        public IWebElement EmpPhoneCountry;

        [FindsBy(How = How.XPath, Using = "//div[2]/div/crisp-picker[@selected.bind='countryCode & validate']/div/div/input")]
        public IWebElement EmpMobileCountry;

        [FindsBy(How = How.XPath, Using = "//crisp-list[@list-title='Results of Search from Existing Companies in CRISP']/ul[@ref='theList']/li[2]")]
        public IWebElement ExistingCompaniesFromCrispDB;   

        [FindsBy(How = How.XPath, Using = "//crisp-wizard[@errors.bind='saveErrors']/div/crisp-button/span/button")]
        public IWebElement ErrorDisplayed;

        [FindsBy(How = How.XPath, Using = "//div[@class='title']/div[@class='au-target']")]
        public IWebElement SelectCompanyFromCompaniesHouse;

        [FindsBy(How = How.XPath, Using = "//ul[@class='au-target collection has-header has-filter']//li[@class='au-target collection-item active']")]
        public IList<IWebElement> EmpList;

        [FindsBy(How = How.XPath, Using = "//crisp-list[@list-title='Employees']/ul[@ref='theList']/li[2]")]
        public IWebElement SingleEmp;

        [FindsBy(How = How.CssSelector, Using = "crisp-card-actions > div > crisp-action-button > div > a > i")]
        public IWebElement EmpEdit;

        [FindsBy(How = How.XPath, Using = "//crisp-input-autocomplete[@label='Title']/div/label")]
        public IWebElement SalutationLabel;

        [FindsBy(How = How.XPath, Using = "//crisp-input-autocomplete[@label='Title']/div/input")]
        public IWebElement SalutationInput;

        [FindsBy(How = How.XPath, Using = "//crisp-input-autocomplete[@label='Title']/div/span")]
        public IWebElement SalutationRequired;

        [FindsBy(How = How.XPath, Using = "//crisp-button[@click.delegate='save()']//button[text()='Add'][@class='au-target waves-effect waves-light btn']")]
        public IWebElement AddEmp;

        [FindsBy(How = How.XPath, Using = "//crisp-button[@title='Save the company you are creating']//BUTTON[@ref='button'][text()='Save'][@class='au-target waves-effect waves-light btn']")]
        public IWebElement CompanySaveButton;


        public void AddPerson()
        {
            FillDetails();
            AddAddressDetails();
            AddSocialInfo();
            WaitForElement(SaveButton);
            SaveButton.Click();
            CloseSpinneronDiv();
            CloseSpinneronDiv();
        }
     
        //Add Company Elememnts 
        public void AddCompany()
        {
            //Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            WaitForElement(CompanyNameLabel);
            WaitForElementToClick(CompanyNameLabel);
            CompanyNameLabel.Click();
            WaitForElement(CompanyNameInput);
            CompanyNameInput.Click();
            CompanyNameInput.Clear();
            var TestCompany = $"Test{RunId.Id}";
            Statics.RoleName = TestCompany;
            CompanyNameInput.SendKeys(Statics.RoleName);
            CloseSpinneronDiv();
            if (ExistingCompanyList.Count > 0)
            {
                ExistingCompanyList[0].Click();

                CloseSpinneronDiv();
            }
            else
            {
                WaitForElement(NoExistingMatchFound);
                NoExistingMatchFound.Click();

                CloseSpinneronDiv();
                if (ExistingCompanyListFromCompaniesHouse.Count > 0)
                {
                    ExistingCompanyListFromCompaniesHouse[0].Click();
                    CloseSpinneronDiv();
                    WaitForElement(NextButton);
                    NextButton.Click();
                    CloseSpinneronDiv();
                    WaitForElement(NextButton);
                    NextButton.Click();
                    CloseSpinneronDiv();
                    WaitForElement(NextButton);
                    NextButton.Click();
                    CloseSpinneronDiv();
                    WaitForElement(NextButton);
                    NextButton.Click();
                    CloseSpinneronDiv();
                }
                else
                {
                    WaitForElement(NoMatchFound);
                    NoMatchFound.Click();
                    FillCompanyDetails();
                    CloseSpinneronDiv();
                    AddOfficeDetails();
                    CloseSpinneronDiv();
                    AddEmpDetails();
                    CloseSpinneronDiv();
                }

                WaitForElement(CompanySaveButton);
                CompanySaveButton.Click();
            }
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
        }
        //Add Employee Emlements 
        public void AddEmpMethod()
        {
            var TestEmp = $"Emp{RunId.Id}";
            Statics.EmpName = TestEmp;
            WaitForElement(EmpTitleLabel);
            EmpTitleLabel.Click();
            EmpTitleInput.SendKeys("Mr");
            WaitForElement(EmpFirstNameLabel);
            EmpFirstNameLabel.Click();
            EmpFirstNameInput.SendKeys(Statics.EmpName);
            WaitForElement(EmpSurNameLabel);
            EmpSurNameLabel.Click();
            WaitForElement(EmpSurNameInput);
            EmpSurNameInput.SendKeys("Kumar");
            WaitForElement(EmpSuffixLabel);
            EmpSuffixLabel.Click();
            WaitForElement(EmpSuffixInput);
            EmpSuffixInput.SendKeys("k");
            WaitForElement(EmpEmailLabel);
            EmpEmailLabel.Click();
            WaitForElement(EmpEmailidInput);
            EmpEmailidInput.SendKeys(ConfigurationManager.AppSettings["Email"]);
            EmpCompanyDirector.Click();
            EmpPurchasingDecisionMaker.Click();
            WaitForElement(EmpJobTitle);
            EmpJobTitle.Click();
            WaitForElement(EmpJobTitleInput);
            EmpJobTitleInput.SendKeys("Automation Test Engineer");
            //Thread.Sleep(500);
            AddCompanyPage.SalesAndMarketingDetails();
            WaitForElement(EmpPhone);
            EmpPhone.Click();
            EmpPhoneInput.SendKeys("01483493726");
            EmpPhoneCountry.Click();
            WaitForElement(PhoneCountryDropdown);
            PhoneCountryDropdown.Click();
            WaitForElement(EmpPhoneExtension);
            EmpPhoneExtension.Click();
            WaitForElement(EmpPhoneExtensionInput);
            EmpPhoneExtensionInput.SendKeys("752");
            WaitForElement(EmpMobile);
            EmpMobile.Click();
            WaitForElement(EmpMobileInput);
            EmpMobileInput.SendKeys("07764233944");
            WaitForElement(EmpMobileCountry);
            EmpMobileCountry.Click();
            WaitForElement(MobileCountryDropdown);
            MobileCountryDropdown.Click();
            WaitForElement(EmpPrimaryOffice);
            EmpPrimaryOffice.Click();
            WaitForElement(EmpChooseOffice);
            EmpChooseOffice.Click();
            WaitForElement(Offices);
            Offices.Click();
            WaitForElement(EmpChooseOffice);
            EmpChooseOffice.Click();
            WaitForElement(SelectButton);
            SelectButton.Click();
            WaitForElement(AddEmp);
            AddEmp.Click();
            CloseSpinneronDiv();
        }
        public void ClickNextButton()
        {
            CloseSpinneronDiv();
            WaitForElement(NextButton);
            NextButton.Click();
            CloseSpinneronDiv();
        }
        // Add Adress To person 
        public void AddAddressDetails()
        {
            WaitForElement(AddressButton);
            AddressButton.Click();
            CloseSpinneronDiv();
            WaitForElement(PostcodeLabel);
            PostcodeLabel.Click();
            WaitForElement(PostcodeInput);
            PostcodeInput.SendKeys("Ch41 1AU");
            CloseSpinneronDiv();
            WaitForElement(AddressDropdown);
            AddressDropdown.Click();
            WaitForElement(AddressDropdownInput);
            AddressDropdownInput.Click();
            WaitForElement(SetAddressButton);
            SetAddressButton.Click();
            CloseSpinneronDiv();
            ClickNextButton();
        }
        //Add Person Fill Details
        public void FillDetails()
        {
            var TestPerson = $"Test{RunId.Id}";
            Statics.ContactName = TestPerson;
            WaitForElement(Salutation);
            Salutation.Click();
            SalutationFieldEdit.SendKeysAndtab("Mr");
            WaitForElement(PersonFirstName);
            PersonFirstName.Click();
            WaitForElement(PersonFirstNameInput);
            PersonFirstNameInput.SendKeys(Statics.ContactName);
            WaitForElement(PersonSurName);
            PersonSurName.Click();
            WaitForElement(PersonSurNameInput);
            PersonSurNameInput.SendKeys("Sunkishala");
            WaitForElement(PersonEmail);
            PersonEmail.Click();
            WaitForElement(PersonEmailInput);
            PersonEmailInput.SendKeys(ConfigurationManager.AppSettings["Email"]);
            WaitForElement(PhoneCountry);
            PhoneCountry.Click();
            WaitForElement(PhoneCountryDropdown);
            PhoneCountryDropdown.Click();
            WaitForElement(PhoneNumber);
            PhoneNumber.Click();
            WaitForElement(PhoneNumberEdit);
            PhoneNumberEdit.SendKeys("01483493721");
            WaitForElement(MobileCountry);
            MobileCountry.Click();
            WaitForElement(MobileCountryDropdown);
            MobileCountryDropdown.Click();
            WaitForElement(MobileNumber);
            MobileNumber.Click();
            WaitForElement(MobileNumberEdit);
            MobileNumberEdit.SendKeys("07834295534");
            CloseSpinneronDiv();
            ClickNextButton();
        }
        //Add Person Social Info Details 
        public void AddSocialInfo()
        {
            //FaceBookId.Click();
            //FacebookElementEnter.SendKeysAndtab("https://www.facebook.com/sunil");
            //wdriver.MoveToActiveAndSendKeys("https://plus.google.com/sunil", true);
            //wdriver.MoveToActiveAndSendKeys("https://www.linkedin.com/sunil", true);
            //wdriver.MoveToActiveAndSendKeys("@sunil", false);
            AddWebsiteButton.Click();

            WaitForElement(WebsiteAddress);
            WebsiteAddress.Click();
            WaitForElement(WebSiteInputField);
            WebSiteInputField.SendKeys("www.mdistest.com");
            WaitForElement(AddButton);
            AddButton.Click();
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);

        }

        //Add Company Details 
        public void FillCompanyDetails()
        {
            var TestCompany = $"Test{RunId.Id}";
            Statics.RoleName = TestCompany;
            WaitForElement(CompanyName);
            CompanyName.Click();
            WaitForElement(ManualCompanyNameInput);
            ManualCompanyNameInput.Clear();
            ManualCompanyNameInput.SendKeys(Statics.RoleName);
            // ManualCompanyNameInput.SendKeys("MDIS Test Company");
            WaitForElement(YearOFRegistration);
            YearOFRegistration.Click();
            WaitForElement(YearOFRegistrationInput);
            YearOFRegistrationInput.SendKeys("2017");
            WaitForElement(RegistartionNumber);
            RegistartionNumber.Click();
            WaitForElement(EnterRegistrationNumber);
            EnterRegistrationNumber.SendKeys("AB123456");
            WaitForElement(LegalFormType);
            LegalFormType.Click();
            WaitForElement(LegalFormTypeDropdown);
            LegalFormTypeDropdown.Click();
            WaitForElement(VIPCheckBox);
            VIPCheckBox.Click();
            WaitForElement(EndorseMentCheckBox);
            EndorseMentCheckBox.Click();
            WaitForElement(FaceBookId);
            FaceBookId.Click();
            //FacebookElementEnter.SendKeysAndtab("https://www.facebook.com/mdis");
            //wdriver.MoveToActiveAndSendKeys("https://plus.google.com/mdis", true);
            //wdriver.MoveToActiveAndSendKeys("https://www.linkedin.com/sunil", true);
            //wdriver.MoveToActiveAndSendKeys("@mdis", false);
            //WaitForElement(AddWebsiteButton);
            //AddWebsiteButton.Click();
            //WaitForElement(WebsiteAddress);
            //WebsiteAddress.Click();
            //WaitForElement(WebSiteInputField);
            //WebSiteInputField.SendKeys("www.mdistest.com");
            //WaitForElement(AddButton);
            //AddButton.Click();

            WaitForElement(NextButtonOnCompany);
            NextButtonOnCompany.Click();
        }
        public void SalesAndMarketingDetails()
        {
            WaitForElements(OptCalls);
            foreach (var eachCheckbox in OptCalls)
            {
                eachCheckbox.Click();
            }

            WaitForElement(NotedOnTPS);
            NotedOnTPS.Click();

            CloseSpinneronDiv();
            CloseSpinneronPage();

        }

        //AddOffice Details ON Company 
        public void AddOfficeDetails()
        {
            //Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(500);
            WaitForElement(AddOfficeButton);
            AddOfficeButton.Click();
            //Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(500);
            WaitForElement(OfficeName);
            OfficeName.Click();
            OfficeNameInput.SendKeys("MDIS Office");
            WaitForElement(OfficeAdress);
            OfficeAdress.Click();
            //Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(500);
            WaitForElement(PostcodeLabel);
            WaitForElementToClick(PostcodeLabel);
            PostcodeLabel.Click();
            //Thread.Sleep(500);
            WaitForElement(PostcodeInput);
            PostcodeInput.Click();
            PostcodeInput.Clear();
            PostcodeInput.SendKeys("Ch41 1AU");
            //Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(500);
            WaitForElement(AddressDropdown);
            AddressDropdown.Click();
            WaitForElement(AddressDropdownInput);
            AddressDropdownInput.Click();
            WaitForElement(SetAddressButton);
            SetAddressButton.Click();
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            WaitForElement(RegisteredAddress);
            WaitForLoadElementtobeclickable(RegisteredAddress);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            RegisteredAddress.Click();
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            WaitForElement(DefaultAddress);
            WaitForLoadElementtobeclickable(DefaultAddress);
            DefaultAddress.Click();
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            WaitForElement(Email);
            WaitForLoadElementtobeclickable(Email);
            Email.Click();
            EmailInput.SendKeys("mdistest@mdinsurance.co.uk");

            WaitForElement(PhoneCountryoncompanywizard);
            string phoneCountry = PhoneCountryoncompanywizard.GetAttribute("value");
            if (phoneCountry != "United Kingdom")
            {
                WaitForElement(PhoneCountryoncompanywizard);
                PhoneCountryoncompanywizard.Click();
                WaitForElement(PhoneCountryDropdown);
                PhoneCountryDropdown.Click();
                //Thread.Sleep(500);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                //Thread.Sleep(500);
            }
            WaitForElement(PhoneNumberoncompanywizard);
            PhoneNumberoncompanywizard.Click();
            WaitForElement(PhoneNumberInput);
            PhoneNumberInput.SendKeys("01483493725");
            WaitForElement(PGAccountManager);
            SalesAndMarketingDetails();
            //PGAccountManager.Click();
            //
            //WaitForElements(PGAccountPerson);
            //
            //PGAccountPerson[0].Click();
            //
            //CloseSpinneronDiv();
            //WaitForElement(LABCAccountManager);
            //
            //LABCAccountManager.Click();
            //
            //CloseSpinneronDiv();
            //WaitForElements(LABCAccountPerson);
            //
            //LABCAccountPerson[0].Click();
            //
            //CloseSpinneronDiv();
            //WaitForElement(CSUAccountManager);
            //
            //CSUAccountManager.Click();
            //
            //WaitForElements(CSUAccountPerson);
            //
            //CSUAccountPerson[0].Click();
            //
            //CloseSpinneronDiv();
            WaitForElement(AddOfficeDataButton);
            AddOfficeDataButton.Click();
            //Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(500);
            WaitForElement(NextButtonOnCompany);
            NextButtonOnCompany.Click();
            //Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(500);
        }
        //Adding Employee on Company 

        public void AddEmpDetails()
        {
            var TestEmp = $"Test{RunId.Id}";
            Statics.EmpName = TestEmp;
            WaitForElement(AddEmpButtonOnCompanyWizard);
            AddEmpButtonOnCompanyWizard.Click();

            WaitForElement(EmpTitleLabel);
            EmpTitleLabel.Click();

            WaitForElement(EmpTitleInput);
            EmpTitleInput.SendKeys("Mr");

            WaitForElement(EmpFirstNameLabel);
            EmpFirstNameLabel.Click();
            WaitForElement(EmpFirstNameInput);
            EmpFirstNameInput.SendKeys(Statics.EmpName);

            WaitForElement(EmpSurNameLabel);
            EmpSurNameLabel.Click();
            WaitForElement(EmpSurNameInput);
            EmpSurNameInput.SendKeys("Kumar");

            WaitForElement(EmpSuffixLabel);
            EmpSuffixLabel.Click();
            WaitForElement(EmpSuffixInput);
            EmpSuffixInput.SendKeys("k");

            WaitForElement(EmpEmailLabel);
            EmpEmailLabel.Click();
            WaitForElement(EmpEmailidInput);
            EmpEmailidInput.SendKeys(ConfigurationManager.AppSettings["Email"]);
            WaitForElement(EmpCompanyDirector);
            WaitForElementToClick(EmpCompanyDirector);
            EmpCompanyDirector.Click();
            WaitForElement(EmpPurchasingDecisionMaker);
            WaitForElementToClick(EmpPurchasingDecisionMaker);
            EmpPurchasingDecisionMaker.Click();
            WaitForElement(EmpJobTitle);
            WaitForElementToClick(EmpJobTitle);
            EmpJobTitle.Click();
            WaitForElement(EmpJobTitleInput);
            EmpJobTitleInput.SendKeys("Automation Test Engineer");

            WaitForElement(EmpPhone);
            EmpPhone.Click();
            EmpPhoneInput.SendKeys("01483493726");
            WaitForElement(EmpPhoneCountry);
            string phoneCountry = EmpPhoneCountry.GetAttribute("value");
            if (phoneCountry != "United Kingdom")
            {
                WaitForElement(EmpPhoneCountry);
                EmpPhoneCountry.Click();

                WaitForElement(PhoneCountryDropdown);

                PhoneCountryDropdown.Click();
            }

            WaitForElement(EmpPhoneExtension);
            EmpPhoneExtension.Click();
            WaitForElement(EmpPhoneExtensionInput);
            EmpPhoneExtensionInput.SendKeys("752");
            WaitForElement(EmpMobile);
            EmpMobile.Click();
            WaitForElement(EmpMobileInput);
            EmpMobileInput.SendKeys("07764233944");
            WaitForElement(EmpMobileCountry);
            string mobileCountry = EmpMobileCountry.GetAttribute("value");
            if (mobileCountry != "United Kingdom")
            {
                WaitForElement(EmpMobileCountry);
                EmpMobileCountry.Click();

                WaitForElement(MobileCountryDropdown);

                MobileCountryDropdown.Click();
            }
            WaitForElement(EmpPrimaryOffice);
            EmpPrimaryOffice.Click();
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            WaitForElement(EmpChooseOffice);
            EmpChooseOffice.Click();
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            WaitForElement(Offices);
            Offices.Click();
            CloseSpinneronDiv();
            CloseSpinneronPage();
            WaitForElement(EmpChooseOffice);
            EmpChooseOffice.Click();
            WaitForElement(SelectButton);
            SelectButton.Click();
            //Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            WaitForElement(AddEmp);
            AddEmp.Click();
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(500);
            WaitForElement(NextButtonOnCompany);
            WaitForElementToClick(NextButtonOnCompany);
            NextButtonOnCompany.Click();
            //Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(500);
        }

    }
}

