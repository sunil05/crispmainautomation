﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CrispAutomation.Pages
{
    public class RescindProcessPage : Support.Pages
    {
        public RescindProcessPage(ISearchContext driver)
        {
            PageFactory.InitElements(driver, this);
        }
        [FindsBy(How = How.XPath, Using = "//crisp-dialog//div[contains(@class,'au-target modal dialog')]//crisp-header[@text='Rescind Process']")]
        public IWebElement RescindProcessDiv;

        [FindsBy(How = How.XPath, Using = "//div[@class='modal-content']//crisp-input-object//label[text()='Process']")]
        public IWebElement ProcessLabel;

        [FindsBy(How = How.XPath, Using = "//div[@class='modal-content']//crisp-input-object//label[text()='Plots']//parent::div//following-sibling::span//span")]
        public IWebElement PlotsLabel;

        [FindsBy(How = How.XPath, Using = "//ul[@class='au-target collection']//li[@class='au-target collection-item']//crisp-list-item//a[@class='au-target row list-item-contents clickable']//crisp-list-item-title[@class='au-target']//div[@class='title']//span")]
        public IList<IWebElement> ProcessList;

        [FindsBy(How = How.XPath, Using = "//ul[contains(@class,'au-target collection')]//li[@class='au-target collection-item']//crisp-list-item//a[@class='au-target row list-item-contents clickable key-item']")]
        public IList<IWebElement> PlotsList;

        [FindsBy(How = How.XPath, Using = "//crisp-button[@click.delegate='ok()']//span//button[@class='au-target waves-effect waves-light btn']")]
        public IWebElement SelectButton;

        [FindsBy(How = How.XPath, Using = "//crisp-input-radio[@label='Client Confirmation']//div[@class='au-target']//ul//li[@class='au-target']//label")]
        public IList<IWebElement> ClientConfirmation;

        [FindsBy(How = How.XPath, Using = "//crisp-input-textarea[@label='Notes']//div//label[@class='au-target']")]
        public IWebElement NotesLabel;

        [FindsBy(How = How.XPath, Using = "//crisp-input-textarea[@label='Notes']//div//textarea")]
        public IWebElement NotesInput;

        [FindsBy(How = How.XPath, Using = "//crisp-footer[@class='au-target']//crisp-footer-button[@icon='save']//button[@class='au-target waves-effect waves-light btn-flat']")]
        public IWebElement RescindButton;

        public IWebElement PlotICProcess => ProcessList.FirstOrDefault(x => x.Text.Contains("Plot IC"));
        public IWebElement DevICProcess => ProcessList.FirstOrDefault(x => x.Text.Contains("Development IC"));
        public IWebElement CoverNoteProcess => ProcessList.FirstOrDefault(x => x.Text.Contains("Cover Note"));
        public IWebElement COAProcess => ProcessList.FirstOrDefault(x => x.Text.Contains("COA"));       
        public IWebElement COIProcess => ProcessList.FirstOrDefault(x => x.Text.Contains("COI"));
        public IWebElement InitialNoticeProcess => ProcessList.FirstOrDefault(x => x.Text.Contains("Initial Notice"));
        public IWebElement FinalNoticeProcess => ProcessList.FirstOrDefault(x => x.Text.Contains("Final Notice"));
        public IWebElement BCSignOffProcess => ProcessList.FirstOrDefault(x => x.Text.Contains("Building Control Sign-Off"));

        public void OpenRescindProcessDiv()
        {
            WaitForElement(Dashboardpage.ActionsButton);
            Dashboardpage.ActionsButton.Click();
            CloseSpinneronDiv();
            CloseSpinneronPage();
            WaitForElement(Dashboardpage.RescindProcess);
            WaitForElementToClick(Dashboardpage.RescindProcess);
            Dashboardpage.RescindProcess.Click();
            WaitForElement(RescindProcessDiv);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Assert.IsTrue(RescindProcessDiv.Displayed);
        }


        public void RescindProcess()
        {
            CloseSpinneronDiv();
            CloseSpinneronPage();
            WaitForElement(PlotsLabel);
            PlotsLabel.Click();
            CloseSpinneronDiv();
            CloseSpinneronPage();
            WaitForElements(PlotsList);
            if (PlotsList.Count > 0)
            {
                foreach (var eachplot in PlotsList)
                {
                    eachplot.Click();
                }
                WaitForElement(SelectButton);
                WaitForElementToClick(SelectButton);
                SelectButton.Click();
                CloseCrispCard();
                CloseSpinneronDiv();
                CloseSpinneronPage();
            }
            WaitForElements(ClientConfirmation);
            if(ClientConfirmation.Count>0)
            {
                ClientConfirmation[0].Click();
            }
            WaitForElement(NotesLabel);
            NotesLabel.Click();
            WaitForElement(NotesInput);
            NotesInput.SendKeys("Test");
            WaitForElement(RescindButton);
            RescindButton.Click();
            CloseSpinneronDiv();
            CloseSpinneronPage();
        }

        public void RescindCOI()
        {
            OpenRescindProcessDiv();
            WaitForElement(ProcessLabel);
            ProcessLabel.Click();
            WaitForElements(ProcessList);
            WaitForElement(COIProcess);
            COIProcess.Click();
            CloseCrispCard();
            CloseSpinneronDiv();
            CloseSpinneronPage();
            RescindProcess();
            CloseSpinneronDiv();
            CloseSpinneronPage();
        }

        public void RescindPlotIC()
        {
            OpenRescindProcessDiv();
            WaitForElement(ProcessLabel);
            ProcessLabel.Click();
            WaitForElements(ProcessList);
            WaitForElement(PlotICProcess);
            PlotICProcess.Click();
            CloseCrispCard();
            CloseSpinneronDiv();
            CloseSpinneronPage();
            RescindProcess();
            CloseSpinneronDiv();
            CloseSpinneronPage();
        }
        public void RescindDevIC()
        {
            OpenRescindProcessDiv();
            WaitForElement(ProcessLabel);
            ProcessLabel.Click();
            WaitForElements(ProcessList);
            WaitForElement(DevICProcess);
            DevICProcess.Click();
            CloseCrispCard();
            CloseSpinneronDiv();
            CloseSpinneronPage();
            RescindProcess();
            CloseSpinneronDiv();
            CloseSpinneronPage();
        }

        public void RescindBCSignOff()
        {
            OpenRescindProcessDiv();
            WaitForElement(ProcessLabel);
            ProcessLabel.Click();
            WaitForElements(ProcessList);
            WaitForElement(BCSignOffProcess);
            BCSignOffProcess.Click();
            CloseCrispCard();
            CloseSpinneronDiv();
            CloseSpinneronPage();
            RescindProcess();
            CloseSpinneronDiv();
            CloseSpinneronPage();
        }

        public void RescindFinalNotice()
        {
            OpenRescindProcessDiv();
            WaitForElement(ProcessLabel);
            ProcessLabel.Click();
            WaitForElements(ProcessList);
            WaitForElement(FinalNoticeProcess);
            FinalNoticeProcess.Click();
            CloseCrispCard();
            CloseSpinneronDiv();
            CloseSpinneronPage();
            RescindProcess();
            CloseSpinneronDiv();
            CloseSpinneronPage();
        }
        public void RescindIntialNotice()
        {
            OpenRescindProcessDiv();
            CloseSpinneronDiv();
            CloseSpinneronPage();
            WaitForElement(ProcessLabel);
            ProcessLabel.Click();
            CloseSpinneronDiv();
            CloseSpinneronPage();
            WaitForElements(ProcessList);
            WaitForElement(InitialNoticeProcess);
            InitialNoticeProcess.Click();
            CloseCrispCard();
            CloseSpinneronDiv();
            CloseSpinneronPage();
            WaitForElements(ClientConfirmation);
            if (ClientConfirmation.Count > 0)
            {
                ClientConfirmation[0].Click();
            }
            WaitForElement(NotesLabel);
            NotesLabel.Click();
            WaitForElement(NotesInput);
            NotesInput.SendKeys("Test");
            WaitForElement(RescindButton);
            RescindButton.Click();           
            CloseSpinneronDiv();
            CloseSpinneronPage();
        }
        public void RescindCOA()
        {
            OpenRescindProcessDiv();
            CloseSpinneronDiv();
            CloseSpinneronPage();
            WaitForElement(ProcessLabel);
            ProcessLabel.Click();
            WaitForElements(ProcessList);
            WaitForElement(COAProcess);
            COAProcess.Click();
            CloseCrispCard();
            CloseSpinneronDiv();
            CloseSpinneronPage();
            RescindProcess();
            CloseSpinneronDiv();
            CloseSpinneronPage();
        }
        public void RescindCoverNote()
        {
            OpenRescindProcessDiv();
            CloseSpinneronDiv();
            CloseSpinneronPage();
            WaitForElement(ProcessLabel);
            ProcessLabel.Click();
            WaitForElements(ProcessList);
            WaitForElement(CoverNoteProcess);
            CoverNoteProcess.Click();
            CloseCrispCard();
            CloseSpinneronDiv();
            CloseSpinneronPage();
            RescindProcess();
            CloseSpinneronDiv();
            CloseSpinneronPage();
        }
    }
}
