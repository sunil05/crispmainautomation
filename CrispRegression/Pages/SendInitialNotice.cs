﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Remote;
using System.Threading;
using NUnit.Framework;
using System.Windows.Forms;
using System.IO;
using CrispAutomation.Support;

namespace CrispAutomation.Pages
{
    public class SendInitialNotice : Support.Pages
    {
        public IWebDriver wdriver;
        public SendInitialNotice(ISearchContext driver)
        {
            PageFactory.InitElements(driver, this);
            wdriver = (RemoteWebDriver)driver;
        }

        [FindsBy(How = How.XPath, Using = "//crisp-tabs[@class='au-target']/div/ul/li/span/a[contains(text(),'Files & Documents')]")]
        public IWebElement FilesandDocsTab;

        [FindsBy(How = How.XPath, Using = "//crisp-header-button[@btn-title='Upload'][@class='au-target']/span/button[@class='au-target waves-effect waves-light btn-flat']")]
        public IWebElement UploadButton;

        [FindsBy(How = How.XPath, Using = "//div[@class='card-content']//upload-documents-embed//crisp-input-file//div[@class='btn']")]
        public IWebElement FileButton;

        [FindsBy(How = How.XPath, Using = "//crisp-input-file[@class='au-target']//div//div//input[@placeholder='Multiple File Upload']")]
        public IWebElement FileInput;

        [FindsBy(How = How.XPath, Using = "//crisp-card-content//div//upload-documents-embed//crisp-input-file[@class='au-target']//div//div//input[@ref='fileInput']")]
        public IWebElement SendFileInput;

        [FindsBy(How = How.XPath, Using = "//crisp-input-file/div/div[1]/input")]
        public IWebElement UploadDoc;

        [FindsBy(How = How.XPath, Using = "//tr//td//crisp-input-object[@class='au-target']//div[contains(@class,'au-target input-field')]//span[@ref='inputContainer']")]
        public IWebElement DocTypeDropdown1;

        [FindsBy(How = How.XPath, Using = "//crisp-card-content[@class='au-target']//div[@class='au-target list-title has-filter']//span[@class='filter-input-field']//input[@class='au-target']")]
        public IWebElement SearchDocumentType;

        [FindsBy(How = How.XPath, Using = "//ul[@class='au-target collection has-header has-filter']//li[@class='au-target collection-item']//crisp-list-item")]
        public IList<IWebElement> SearchDocumentTypeInputList;

        [FindsBy(How = How.XPath, Using = "//ul/li/span[text()='Site Location Plan']")]
        public IWebElement SiteLocationPlanType;

        [FindsBy(How = How.XPath, Using = "//ul[@class='dropdown-content select-dropdown active']//li/span[text()='Drainage Plan']")]
        public IWebElement SurfaceStormAndFoulDrainageType;

        [FindsBy(How = How.XPath, Using = "//crisp-input-date/div/label[text()='Date Received']")]
        public IWebElement DateReceivedLabel;

        [FindsBy(How = How.XPath, Using = "//div[contains(@class,'picker--opened')]/div/div/div/div/div[@class='picker__footer']/button[text()='Today']")]
        public IWebElement DateReceivedInput;

        [FindsBy(How = How.XPath, Using = "//crisp-button[@class='au-target']//span//button[text()='Save'][@class='au-target waves-effect waves-light btn-flat']")]
        public IWebElement SaveButton;

        //public IWebElement AcceptQuoteButton => QuoteDecisionList.FirstOrDefault(x => x.Text.Contains("ACCEPT QUOTE"));

        public IWebElement SendInitialNoticeButton => Dashboardpage.ActiveBannerList.FirstOrDefault(x => x.Text.Contains("SEND INITIAL NOTICE"));
          
        //Intial Notice QS Page Elements 
        [FindsBy(How = How.XPath, Using = "//crisp-dialog[@class='au-target']//div[contains(@class,'au-target modal dialog')]//crisp-header[@class='au-target']")]
        public IList<IWebElement> SendIntialNoticeDialogueWizard;        

        [FindsBy(How = How.XPath, Using = "//crisp-input-radio[@label='Is connection being made to the sewer?']/div/ul/li/label[text()='Yes'][@class='au-target']")]
        public IWebElement IsConnectiontotheSewer;

        [FindsBy(How = How.XPath, Using = "//crisp-input-radio[@label='Are we obliged to consult with the fire authority?']/div/ul/li/label[text()='Yes'][@class='au-target']")]
        public IWebElement ObligedFireAuthority;

        [FindsBy(How = How.XPath, Using = "//crisp-input-radio[@label='Has the client provided a response to the following planning requirements?']/div/ul/li/label[text()='Yes'][@class='au-target']")]
        public IWebElement PlanningRequirement;

        [FindsBy(How = How.XPath, Using = "//div[@class='au-target']//crisp-input-radio[contains(@label,'Regulation')]/div/ul/li/label[text()='Applicable']")]
        public IWebElement RegulationRadioButton;

        [FindsBy(How = How.XPath, Using = "//div[@class='au-target']//crisp-input-radio[contains(@label,'Schedule 1 Part M optional requirement M4(2)')]/div/ul/li/label[text()='Applicable']")]
        public IWebElement AccessbleRadioButton;

        [FindsBy(How = How.XPath, Using = "//div[@class='au-target']//crisp-input-radio[contains(@label,'Schedule 1 Part M optional requirement M4(3)')]/div/ul/li/label[text()='Applicable']")]
        public IWebElement WheelChairRadioButton;

        [FindsBy(How = How.XPath, Using = "//div[@class='au-target']//crisp-input-radio[contains(@label,'A statement that planning permission has not yet been granted for the work')]/div/ul/li/label[text()='Applicable']")]
        public IWebElement PermissionRadioButton;

        [FindsBy(How = How.XPath, Using = "//crisp-footer-button[@class='au-target']//button[text()='Next']")]
        public IWebElement InitialNoticeNextButton;

        //Intial Notice File Review Page Elements 

        [FindsBy(How = How.XPath, Using = "//crisp-tabs/div/ul/li/span/a[contains(text(),'Blocking (0)')]")]
        public IWebElement Blockers;

        [FindsBy(How = How.XPath, Using = "//crisp-card-content/div/div[contains(text(),'There are no companies or individuals with blocks')]")]
        public IWebElement NoBlockers;

        [FindsBy(How = How.XPath, Using = "//crisp-tabs/div/ul/li/span/a[contains(text(),'Conditions (0)')]")]
        public IWebElement Conditions;

        [FindsBy(How = How.XPath, Using = "//crisp-card-content/div/div[contains(text(),'There are no Conditions')]")]
        public IWebElement NoConditions;

        [FindsBy(How = How.XPath, Using = "//crisp-card-header/div/span[text()='Complete send Initial Notice']")]
        public IWebElement ConfirmDetailsPage;

        [FindsBy(How = How.XPath, Using = "//div//table//tbody//tr//td//p//a[@href='https://www.premierguarantee.co.uk/']//img")]
        public IList<IWebElement> PGGenerateQuote;


        [FindsBy(How = How.XPath, Using = "//crisp-footer-button[@class='au-target']//button[text()='Send Initial Notice']")]
        public IWebElement CompleteSednIntialNoticeButton;

        //Respond to InitialNotice 

        [FindsBy(How = How.CssSelector, Using = "crisp-header > nav > div > crisp-header-avatar")]
        public IWebElement QuoteApplication;

        [FindsBy(How = How.XPath, Using = "//crisp-header-button[@class='au-target']//span//button[text()='Reload'][@class='au-target waves-effect waves-light btn-flat']")]
        public IList<IWebElement> ReloadButton;
        public IWebElement RespondToInitialNotice => Dashboardpage.ActiveBannerList.FirstOrDefault(x => x.Text.Contains("RESPOND TO INITIAL NOTICE"));
         

        [FindsBy(How = How.XPath, Using = "//crisp-list-item-title/div/span[text()='Accepted']")]
        public IWebElement ResponseTypeAccepted;

        [FindsBy(How = How.XPath, Using = "//div/crisp-header/nav/div")]
        public IWebElement InitialNoticeResponseAcceptanceWizard;

        public void SelectIntialNotice()
        {
            try
            {
                //Thread.Sleep(500);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                Thread.Sleep(500);
                WaitForElement(Dashboardpage.ActionsButton);
                Dashboardpage.ActionsButton.Click();
                //Thread.Sleep(500);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                Thread.Sleep(500);
                WaitForElement(SendIntialNotice.SendInitialNoticeButton);
                SendIntialNotice.SendInitialNoticeButton.Click();
                CloseSpinneronDiv();
                CloseSpinneronPage();
                //Thread.Sleep(500);
            }
            catch(Exception ex)
            {
                Console.WriteLine("Please check Intial Notice permissions , User may not have permissions to issue Intial Notice,Please Apply Permissions Manually: {0}", ex);
            }            
        }
        

        public void FilesandDocumentsDetails()
        {
            //Upload Site location Plan 
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(500);
            var fileInfo = new FileInfo(ExtensionMethods.GetAbsolutePath(@"TestData\QuotesData\CRISP install guide.docx"));
            SendFileInput.SendKeys($"{fileInfo}");
            CloseSpinneronDiv();
            CloseSpinneronPage();        
            WaitForElement(DocTypeDropdown1);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            DocTypeDropdown1.Click();
            CloseSpinneronDiv();
            CloseSpinneronPage();
            WaitForElement(SearchDocumentType);
            SearchDocumentType.Click();
            SearchDocumentType.Clear();
            SearchDocumentType.SendKeys("Site Location Plan");
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            SearchDocumentType.SendKeys(OpenQA.Selenium.Keys.Enter);
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            WaitForElements(SearchDocumentTypeInputList);
            if(SearchDocumentTypeInputList.Count>=1)
            {
                WaitForElement(SearchDocumentTypeInputList[0]);
                SearchDocumentTypeInputList[0].Click();
                CloseSpinneronDiv();
                CloseSpinneronPage();
                Thread.Sleep(500);
            }
            WaitForElement(DateReceivedLabel);
            DateReceivedLabel.Click();
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            WaitForElement(DateReceivedInput);
            DateReceivedInput.Click();
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            WaitForElement(SaveButton);
            WaitForElementToClick(SaveButton);
            SaveButton.Click();
            //Upload  Surface Storm And Foul Drainage Layout
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            WaitForElement(UploadButton);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            WaitForElementToClick(UploadButton);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            UploadButton.Click();
            CloseSpinneronDiv();
            CloseSpinneronPage();          
            WaitForElement(FileButton);                      
            SendFileInput.SendKeys($"{fileInfo}");
            //SendFileInput.SendKeys(@"\\tpgfile\NewCompany\IT\Department\QA Team\Current Projects\CRISP\CrispAutomation\CrispData\QuotesData\CRISP install guide.docx");           
            CloseSpinneronDiv();
            CloseSpinneronPage();           
            WaitForElement(DocTypeDropdown1);           
            CloseSpinneronDiv();
            CloseSpinneronPage();          
            DocTypeDropdown1.Click();           
            CloseSpinneronDiv();
            CloseSpinneronPage();          
            WaitForElement(SearchDocumentType);
            SearchDocumentType.Click();
            SearchDocumentType.Clear();
            SearchDocumentType.SendKeys("Drainage Plan");
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            SearchDocumentType.SendKeys(OpenQA.Selenium.Keys.Enter);
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            WaitForElements(SearchDocumentTypeInputList);
            if (SearchDocumentTypeInputList.Count >= 1)
            {
                SearchDocumentTypeInputList[0].Click();
                CloseSpinneronDiv();
                CloseSpinneronPage();
                Thread.Sleep(500);
            }
            WaitForElement(DateReceivedLabel);
            DateReceivedLabel.Click();
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            WaitForElement(DateReceivedInput);
            DateReceivedInput.Click();           
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            WaitForElement(SaveButton);
            WaitForElementToClick(SaveButton);
            SaveButton.Click();
        }

        public void IntialNoticeQS()
        {
            CloseSpinneronDiv();
            CloseSpinneronPage();
            WaitForElement(IsConnectiontotheSewer);
            IsConnectiontotheSewer.Click();
            WaitForElement(ObligedFireAuthority);
            ObligedFireAuthority.Click();
            WaitForElement(PlanningRequirement);
            PlanningRequirement.Click();
            CloseSpinneronDiv();
            CloseSpinneronPage();
            WaitForElement(RegulationRadioButton);
            RegulationRadioButton.Click();
            WaitForElement(AccessbleRadioButton);
            AccessbleRadioButton.Click();
            WaitForElement(WheelChairRadioButton);
            WheelChairRadioButton.Click();
            WaitForElement(PermissionRadioButton);
            PermissionRadioButton.Click();
            CloseSpinneronDiv();
            CloseSpinneronPage();
        }

        public void FileReviewDetails()
        {
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Assert.IsTrue(Blockers.Displayed);
            Assert.IsTrue(NoBlockers.Displayed);
            WaitForElement(Conditions);
            Assert.IsTrue(Conditions.Displayed);
            CloseSpinneronDiv();
            CloseSpinneronPage();
        }

        public void ConfirmDetails()
        {
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Assert.IsTrue(ConfirmDetailsPage.Displayed);
            CloseSpinneronDiv();
            CloseSpinneronPage();
        }

        public void InToClientDetails()
        {
            CloseSpinneronDiv();
            CloseSpinneronPage();
            WaitForElements(PGGenerateQuote);
            Assert.True(PGGenerateQuote[0].Displayed);
            CloseSpinneronDiv();
            CloseSpinneronPage();
        }

        public void InToLADetails()
        {
            CloseSpinneronDiv();
            CloseSpinneronPage();
            WaitForElements(PGGenerateQuote);
            Assert.True(PGGenerateQuote[1].Displayed);
            CloseSpinneronDiv();
            CloseSpinneronPage();
        }
        public void SendINNextButton()
        {
            WaitForElement(InitialNoticeNextButton);
            WaitForElementToClick(InitialNoticeNextButton);
            InitialNoticeNextButton.Click();
        }
        public void SendIntialNoticeMethod()
        {
            WaitForElement(FilesandDocsTab);
            FilesandDocsTab.Click();
            Thread.Sleep(500);
            CloseSpinneronDiv();
            WaitForElement(UploadButton);
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronDiv();
            UploadButton.Click();
            Thread.Sleep(500);
            CloseSpinneronDiv();
            WaitForElement(FileButton);
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronDiv();
            FilesandDocumentsDetails();
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            log.Info($"Required Document are uploaded Successfully to sent Intial Notice");
            log.Info("Sending Intial Notice");
            //Thread.Sleep(500);
            if (SendIntialNotice.ReloadButton.Count > 0)
            {
                WaitForElements(SendIntialNotice.ReloadButton);
                SendIntialNotice.ReloadButton[0].Click();
                Thread.Sleep(500);
                CloseSpinneronDiv();
                CloseSpinneronPage();
            }
            else
            {
                Driver.Navigate().Refresh();
                Thread.Sleep(500);
                CloseSpinneronDiv();
                CloseSpinneronPage();
            }           
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            WaitForElement(Dashboardpage.ActionsButton);
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Dashboardpage.ActionsButton.Click();
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();          
            WaitForElement(SendInitialNoticeButton);
            WaitForElementToClick(SendInitialNoticeButton);
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            SendInitialNoticeButton.Click();
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();            
            WaitForElement(SendIntialNoticeDialogueWizard[0]);
            Assert.IsTrue(SendIntialNoticeDialogueWizard[0].Displayed);
            log.Info($"Sending Intial Notice");
            CloseSpinneronDiv();
            CloseSpinneronPage();
            IntialNoticeQS();
            SendINNextButton();
            CloseSpinneronDiv();
            CloseSpinneronPage();
            FileReviewDetails();
            SendINNextButton();
            CloseSpinneronDiv();
            CloseSpinneronPage();
            ConfirmDetails();
            SendINNextButton();
            CloseSpinneronDiv();
            CloseSpinneronPage();
            InToClientDetails();
            SendINNextButton();
            CloseSpinneronDiv();
            CloseSpinneronPage();
            InToLADetails();
            WaitForElement(SendIntialNotice.CompleteSednIntialNoticeButton);
            SendIntialNotice.CompleteSednIntialNoticeButton.Click();           
            CloseSpinneronDiv();
            CloseSpinneronPage();          
            WaitForElement(RespondIntialNotice.BuildingControlTab);         
            CloseSpinneronDiv();
            CloseSpinneronPage();          
            WaitForElementToClick(RespondIntialNotice.BuildingControlTab);   
            CloseSpinneronDiv();
            CloseSpinneronPage();        
            RespondIntialNotice.BuildingControlTab.Click();          
            CloseSpinneronDiv();
            CloseSpinneronPage();           
            WaitForElement(RespondIntialNotice.Processes);
            RespondIntialNotice.Processes.Click();         
            CloseSpinneronDiv();
            CloseSpinneronPage();           
            RespondIntialNotice.IntialNoticeStatusSent();
            log.Info($"Intial Notice Sent SuccessFully");           
        }

        public void RespondtoIntialNoticeMethod()
        {             
            CloseSpinneronDiv();
            CloseSpinneronPage();            
            WaitForElement(Dashboardpage.ActionsButton);
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Dashboardpage.ActionsButton.Click();          
            CloseSpinneronDiv();
            CloseSpinneronPage();
            WaitForElement(SendIntialNotice.RespondToInitialNotice);
            WaitForElementToClick(SendIntialNotice.RespondToInitialNotice);
            SendIntialNotice.RespondToInitialNotice.Click();           
            CloseSpinneronDiv();
            CloseSpinneronPage();
            WaitForElement(SendIntialNotice.ResponseTypeAccepted);
            SendIntialNotice.ResponseTypeAccepted.Click();          
            CloseSpinneronDiv();
            CloseSpinneronPage();
            WaitForElement(RespondIntialNotice.FormalAcceptanceButton);
            RespondIntialNotice.FormalAcceptanceButton.Click();           
            CloseSpinneronDiv();
            CloseSpinneronPage();
            WaitForElement(RespondIntialNotice.AcceptanceIntialNoticeResponseNextButton);
            RespondIntialNotice.AcceptanceIntialNoticeResponseNextButton.Click();
            RespondIntialNotice.FileReviewDetails();           
            CloseSpinneronDiv();
            CloseSpinneronPage();
            WaitForElement(RespondIntialNotice.AcceptanceIntialNoticeResponseNextButton);
            RespondIntialNotice.AcceptanceIntialNoticeResponseNextButton.Click();
            RespondIntialNotice.ConfirmDetails();           
            CloseSpinneronDiv();
            CloseSpinneronPage();
            WaitForElement(RespondIntialNotice.AcceptanceIntialNoticeResponseNextButton);
            RespondIntialNotice.AcceptanceIntialNoticeResponseNextButton.Click();           
            CloseSpinneronDiv();
            CloseSpinneronPage();
            RespondIntialNotice.IntialNoticeAcceptanceDetails();          
            CloseSpinneronDiv();
            CloseSpinneronPage();
            WaitForElement(RespondIntialNotice.SendIntialNoticeResponseAcceptanceButton);
            //Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            RespondIntialNotice.SendIntialNoticeResponseAcceptanceButton.Click();
            //Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            WaitForElement(RespondIntialNotice.BuildingControlTab);
            //Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            RespondIntialNotice.BuildingControlTab.Click();
            //Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            WaitForElement(RespondIntialNotice.Processes);
            //Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            RespondIntialNotice.Processes.Click();
            //Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            RespondIntialNotice.IntialNoticeStatusDetails();
        }
    }
}
