﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using CrispAutomation.Support;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;

namespace CrispAutomation.Pages
{
    public class PageUtils : Support.Pages
    {
        public IWebDriver wdriver;
        public PageUtils(ISearchContext driver)
        {
            PageFactory.InitElements(driver, this);
            wdriver = (RemoteWebDriver)driver;
        }


        public void SelectFromDropdown(IWebElement ele, string option)
        {
            SelectElement sel = new SelectElement(ele);
            sel.SelectByText(option);
        }

        [FindsBy(How = How.XPath, Using = "//crisp-button//button[text()='Upload New Files'][@class='au-target waves-effect waves-light btn-flat']")]
        public IWebElement UploadNewFilesButton;

        [FindsBy(How = How.XPath, Using = "//upload-documents-embed//crisp-input-file//div//div//input[@ref='fileInput']")]
        public IWebElement FileInput;

        [FindsBy(How = How.XPath, Using = "//crisp-input-date/div/label[text()='Date Received']")]
        public IWebElement DateReceivedLabel;

        [FindsBy(How = How.XPath, Using = "//div[contains(@class,'picker--opened')]/div/div/div/div/div[@class='picker__footer']/button[text()='Today']")]
        public IWebElement DateReceivedInput;

        [FindsBy(How = How.XPath, Using = "//tr//td//crisp-input-object[@class='au-target']//div[contains(@class,'au-target input-field')]//span[@ref='inputContainer']")]
        public IWebElement DocTypeDropdown1;

        [FindsBy(How = How.XPath, Using = "//crisp-card-content[@class='au-target']//div[@class='au-target list-title has-filter']//span[@class='filter-input-field']//input[@class='au-target']")]
        public IWebElement SearchDocumentType;

        [FindsBy(How = How.XPath, Using = "//ul[@class='au-target collection has-header has-filter']//li[@class='au-target collection-item']//crisp-list-item")]
        public IList<IWebElement> SearchDocumentTypeInputList;

        [FindsBy(How = How.XPath, Using = "//tr//td//crisp-input-object[@class='au-target']//div[@class='au-target input-field']//span[@ref='inputContainer']")]
        public IWebElement DocTypeDropdown;

        [FindsBy(How = How.XPath, Using = "//ul[@class='dropdown-content select-dropdown active']//li/span[text()='Initial Notice']")]
        public IWebElement DocTypeDropdownInput;

        [FindsBy(How = How.XPath, Using = "//crisp-card-actions//crisp-button[@click.delegate='complete()']//button[@class='au-target waves-effect waves-light btn'][text()='Select']")]
        public IWebElement UploadFileSelectButton;

        

        public void UploadNewFile()
        {
            //Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(500);
            WaitForElement(UploadNewFilesButton);
            WaitForElementToClick(UploadNewFilesButton);
            UploadNewFilesButton.Click();
            //Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();           
            var fileInfo = new FileInfo(ExtensionMethods.GetAbsolutePath(@"TestData\QuotesData\CRISP install guide.docx"));
            //Thread.Sleep(500);
            FileInput.SendKeys($"{fileInfo}");
            //FileInput.SendKeys(@"\\tpgfile\NewCompany\IT\Department\QA Team\Current Projects\CRISP\CrispAutomation\CrispData\QuotesData\CRISP install guide.docx");
            //Thread.Sleep(1000);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);   
            WaitForElement(UploadFileSelectButton);
            WaitForElementToClick(UploadFileSelectButton);
            UploadFileSelectButton.Click();
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
        }
    }
}
