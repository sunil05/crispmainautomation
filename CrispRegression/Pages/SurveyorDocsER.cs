﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.PageObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace CrispAutomation.Pages
{
    public class SurveyorDocsER : Support.Pages
    {
        public IWebDriver wdriver;
        public SurveyorDocsER(ISearchContext driver)
        {
            PageFactory.InitElements(driver, this);
            wdriver = (RemoteWebDriver)driver;
        }
        //Opend Documents on ER 
        public void OpenSurveyorDocsER()
        {
            WaitForElement(EngineerReviewPage.ERStatus);
            if (EngineerReviewPage.ERStatus.Text.Contains("Outstanding"))
            {
                EngineerReviewPage.OpenER();
                EngineerReviewPage.EngineerReviewAssessment();
                SurveyorDocs.OpenDocuments();
                EngineerReviewPage.SubmitER();
            }else
            {
                EngineerReviewPage.OpenER();
                EngineerReviewPage.EngineerReviewAssessment();
                SurveyorDocs.OpenDocuments();
                EngineerReviewPage.SubmitER();
            }

        }
        //Close Documents on ER
        public void CloseSurveyorDocsER()
        {
            WaitForElement(EngineerReviewPage.ERStatus);
            if (EngineerReviewPage.ERStatus.Text.Contains("Outstanding"))
            {
                CloseSpinnerOnSurveyorDiv();
                CloseSpinnerOnSurveyorPage();
                Thread.Sleep(500);
                EngineerReviewPage.OpenER();
                CloseSpinnerOnSurveyorDiv();
                CloseSpinnerOnSurveyorPage();
                Thread.Sleep(500);
                WaitForElementonSurveyor(EngineerReviewPage.NextButton);
                EngineerReviewPage.NextButton.Click();
                CloseDocsonER();
                EngineerReviewPage.SubmitER();
            }
            else
            {
                EngineerReviewPage.OpenER();
                //Thread.Sleep(500);
                CloseSpinnerOnSurveyorPage();
                //Thread.Sleep(500);
                //Thread.Sleep(500);
                CloseSpinnerOnSurveyorPage();
                //Thread.Sleep(500);
                WaitForElementonSurveyor(EngineerReviewPage.NextButton);
                EngineerReviewPage.NextButton.Click();
                CloseDocsonER();
                EngineerReviewPage.SubmitER();
            }
        }

        //Close Documents on ER
        public void NotClosingSurveyorDocsER()
        {
            WaitForElement(EngineerReviewPage.ERStatus);
            if (EngineerReviewPage.ERStatus.Text.Contains("Outstanding") || EngineerReviewPage.ERStatus.Text.Contains("Not required"))
            {
                EngineerReviewPage.OpenER();
                //Thread.Sleep(500);
                CloseSpinnerOnSurveyorPage();
                //Thread.Sleep(500);            
                WaitForElementonSurveyor(EngineerReviewPage.NextButton);
                EngineerReviewPage.NextButton.Click();
                NotCloseDocsonER();
                EngineerReviewPage.SaveER();
            }
            else
            {
                EngineerReviewPage.OpenER();
                //Thread.Sleep(500);
                CloseSpinnerOnSurveyorPage();
                //Thread.Sleep(500);
                //Thread.Sleep(500);
                CloseSpinnerOnSurveyorPage();
                //Thread.Sleep(500);
                WaitForElementonSurveyor(EngineerReviewPage.NextButton);
                EngineerReviewPage.NextButton.Click();
                NotCloseDocsonER();
                EngineerReviewPage.SaveER();
            }
        }

        //Verify Closed Documents on ER 
        public void VerifyClosedSurveyorDocsOnER()
        {
            EngineerReviewPage.OpenER();
            //Thread.Sleep(500);
            CloseSpinnerOnSurveyorPage();
            //Thread.Sleep(500);
            WaitForElementonSurveyor(EngineerReviewPage.NextButton);
            EngineerReviewPage.NextButton.Click();
            SurveyorDocs.ShowClosedDocuments();
            EngineerReviewPage.SubmitER();
        }

        //Close Docs on ER
        public void CloseDocsonER()
        {
            WaitForLoadElements(SurveyorDocs.DocsTabs);
            if (SurveyorDocs.DocsTabs.Count > 0)
            {
                // if it is open on Site Inspection or SRA Close all documents 
                for (int i = 0; i < SurveyorDocs.DocsTabs.Count; i++)
                {
                    var TabStatus = SurveyorDocs.DocsTabs[i].GetAttribute("aria-expanded");
                    Assert.IsTrue(TabStatus == "false");

                    if (SurveyorDocs.RaisedDesignDocPartOf.Contains("Engineering Review"))
                    {
                        SurveyorDocs.CloseDesignDocuments(i);
                    }
                    else
                    {
                        SurveyorDocs.NotClosingDesignDocuments(i);
                    }

                    if (SurveyorDocs.RaisedDesignDocPartOf.Contains("Engineering Review"))
                    {
                        SurveyorDocs.CloseDesignItems(i);
                    }
                    else
                    {
                        SurveyorDocs.NotClosingDesignItems(i);
                    }
                    if (SurveyorDocs.RaisedDesignDocPartOf.Contains("Inspection") || SurveyorDocs.RaisedDesignDocPartOf.Contains("Engineering Review"))
                    {
                        SurveyorDocs.CloseRiskItems(i);
                    }
                    else
                    {
                        SurveyorDocs.NotClosingRiskItems(i);
                    }

                    if (SurveyorDocs.RaisedDesignDocPartOf.Contains("Inspection") || SurveyorDocs.RaisedDesignDocPartOf.Contains("Engineering Review"))
                    {
                        SurveyorDocs.CloseCompletionDocs(i);
                    }
                    else
                    {
                        SurveyorDocs.NotClosingCompletionDocuments(i);
                    }
                }
            }
        }

        //Not Close Docs on ER
        public void NotCloseDocsonER()
        {
            WaitForLoadElements(SurveyorDocs.DocsTabs);
            if (SurveyorDocs.DocsTabs.Count > 0)
            {
                for (int i = 0; i < SurveyorDocs.DocsTabs.Count; i++)
                {
                    var TabStatus = SurveyorDocs.DocsTabs[i].GetAttribute("aria-expanded");
                    Assert.IsTrue(TabStatus == "false");
                    if (SurveyorDocs.RaisedDesignDocPartOf.Contains("Design Review") || SurveyorDocs.RaisedDesignDocPartOf.Contains("Refurbishment Assessment") || SurveyorDocs.RaisedDesignDocPartOf.Contains("Site Risk Assessment")|| SurveyorDocs.RaisedDesignDocPartOf.Contains("Inspection"))
                    {
                        SurveyorDocs.NotClosingDesignDocuments(i);
                    }
                    if (SurveyorDocs.RaisedDesignDocPartOf.Contains("Design Review") || SurveyorDocs.RaisedDesignDocPartOf.Contains("Refurbishment Assessment") || SurveyorDocs.RaisedDesignDocPartOf.Contains("Site Risk Assessment"))
                    {
                        SurveyorDocs.NotClosingCompletionDocuments(i);
                    }

                    if (SurveyorDocs.RaisedDesignDocPartOf.Contains("Design Review") || SurveyorDocs.RaisedDesignDocPartOf.Contains("Refurbishment Assessment") || SurveyorDocs.RaisedDesignDocPartOf.Contains("Site Risk Assessment") || SurveyorDocs.RaisedDesignDocPartOf.Contains("Inspection"))
                    {
                        SurveyorDocs.NotClosingDesignItems(i);
                    }
                    if (SurveyorDocs.RaisedDesignDocPartOf.Contains("Design Review") || SurveyorDocs.RaisedDesignDocPartOf.Contains("Refurbishment Assessment") || SurveyorDocs.RaisedDesignDocPartOf.Contains("Site Risk Assessment"))
                    {
                        SurveyorDocs.NotClosingRiskItems(i);
                    }
                }
            }

        }
    }
}
