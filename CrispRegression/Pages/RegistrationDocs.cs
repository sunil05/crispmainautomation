﻿using CrispAutomation.Features;
using CrispAutomation.Support;
using iTextSharp.text.pdf;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace CrispAutomation.Pages
{
    public class RegistrationDocs : Support.Pages
    {
        public IWebDriver wdriver;

        public RegistrationDocs(ISearchContext driver)
        {
            PageFactory.InitElements(driver, this);
            wdriver = (RemoteWebDriver)driver;
        }
        [FindsBy(How = How.XPath, Using = "//registration-view//crisp-tabs//ul[@ref='tabHeader']//li[@class='au-target tab']//span//a[contains(text(),'Reg Documents')]")]
        public IWebElement RegDocumentTab;

        [FindsBy(How = How.XPath, Using = "//div[@ref='tabContainer']//security-documents//crisp-list[contains(@source.bind,'SecurityDocuments')]//crisp-action-button[@click.delegate='add()']//a//i")]
        public IList<IWebElement> AddSecurityDocButton;

        //[FindsBy(How = How.XPath, Using = "//div[@ref='tabContainer']//security-documents[@title='Additional Indemnities']//crisp-list[@source.bind='securityDocuments']//crisp-action-button[@click.delegate='add()']//a//i")]
        //public IList<IWebElement> AddSecurityDocsButton;

        [FindsBy(How = How.XPath, Using = "//div[@ref='tabContainer']//security-documents//crisp-list[contains(@source.bind,'SecurityDocuments')]//crisp-action-button[@click.delegate='add()']//div[@class='au-target fixed-action-btn active']//a//i")]
        public IList<IWebElement> AddSecurityDocButtons;

        [FindsBy(How = How.XPath, Using = "//crisp-dialog//div//crisp-header//div[text()='Add']")]
        public IWebElement AddDocsDiv;

        [FindsBy(How = How.XPath, Using = "//div[@class='modal-content']//crisp-picker[@label='Document Type']//div//input[@class='select-dropdown']")]
        public IWebElement DocumentTypeDropdown;

        [FindsBy(How = How.XPath, Using = "//ul[@class='dropdown-content select-dropdown active']//li//span")]
        public IList<IWebElement> DocumentTypeDropdownInput;

        [FindsBy(How = How.XPath, Using = "//crisp-input-bool[@label='Is Bespoke']//label")]
        public IWebElement IsBespoke;

        [FindsBy(How = How.XPath, Using = "//div[@class='modal-content']//crisp-input-object[@icon='file']//div//label[contains(text(),'Amended Version Upload')]")]
        public IWebElement UploadFileLabel;

        [FindsBy(How = How.XPath, Using = "//crisp-card-actions//crisp-button//button[text()='Upload New Files']")]
        public IWebElement UploadNewFilesButton;

        [FindsBy(How = How.XPath, Using = "//crisp-card//crisp-card-actions//crisp-button[@click.delegate='complete()']//button[@class='au-target waves-effect waves-light btn']")]
        public IWebElement UploadFileSelectButton;

        [FindsBy(How = How.XPath, Using = "//crisp-input-object//div//label[text()='Children']")]
        public IList<IWebElement> ChildrenLabel;

        [FindsBy(How = How.XPath, Using = "//contact-list//crisp-list//ul[@ref='theList']//li[@class='au-target collection-item']")]
        public IList<IWebElement> ChildrenList;

        [FindsBy(How = How.XPath, Using = "//div[@class='modal-content']//crisp-input-object//div//label[text()='Covered Documents']")]
        public IWebElement CoverDoclabel;

        [FindsBy(How = How.XPath, Using = "//crisp-card-content//crisp-list//span//input[@placeholder ='Enter filter text...']")]
        public IWebElement ChooseCoverDocLabel;

        [FindsBy(How = How.XPath, Using = "//div[@class='card-content']//crisp-list//ul[@ref='theList']//li[@class='au-target collection-item']")]
        public IList<IWebElement> CoverDocsList;

        [FindsBy(How = How.XPath, Using = "//crisp-card//crisp-card-actions//crisp-button[@click.delegate='ok()']//button[@class='au-target waves-effect waves-light btn']")]
        public IWebElement SelectButton;

        [FindsBy(How = How.XPath, Using = "//ai-dialog-container//crisp-dialog//div[@class='modal-footer']//crisp-button[@click.delegate='ok()']//button[@class='au-target waves-effect waves-light btn-flat']")]
        public IWebElement OkButton;

        [FindsBy(How = How.XPath, Using = "//crisp-header-actions//crisp-header-button[@click.delegate='performFileReview()']//span//button[@class='au-target waves-effect waves-light btn-flat']")]
        public IWebElement ViewInformation;

        [FindsBy(How = How.XPath, Using = "//ai-dialog-container//crisp-dialog//crisp-wizard[@wizard-title='File Review']//h2")]
        public IWebElement FileReviewWizard;

        [FindsBy(How = How.XPath, Using = "//crisp-footer//crisp-footer-button[@click.delegate='goForward()']//button[@class='au-target waves-effect waves-light btn-flat']")]
        public IWebElement FileReviewNextButton;

        [FindsBy(How = How.XPath, Using = "//crisp-wizard/div/crisp-stepper-step[contains(@class,'current')]//div[@class='highlight-colour title au-target'][@title.bind='description']")]
        public IWebElement StepTitle;

        [FindsBy(How = How.XPath, Using = "//crisp-wizard-step[@step-title='Create Chase Email']//correspondence-email//crisp-input-object//crisp-download-file//a")]
        public IList<IWebElement> DownloadFile;

        [FindsBy(How = How.XPath, Using = "//crisp-footer//crisp-footer-button[@click.delegate='sendChaseEmail()']//button[@class='au-target waves-effect waves-light btn-flat']")]
        public IWebElement SendChaseEmailButton;

        //Files and Documents Section

        [FindsBy(How = How.XPath, Using = "//registration-view//crisp-tabs//ul[@ref='tabHeader']//li[@class='au-target tab']//span//a[contains(text(),'Files & Documents')]")]
        public IWebElement FilesDocsTab;

        [FindsBy(How = How.XPath, Using = "//registration-view//files-documents//view-folders//folders//crisp-list//ul//li[@class='au-target collection-item']//crisp-list-item//crisp-list-item-details//span")]
        public IList<IWebElement> DefaultFolders;

        [FindsBy(How = How.XPath, Using = "//registration-view//files-documents//crisp-header-button[@btn-title='Add folder']//button")]
        public IWebElement CreateFolderButton;

        [FindsBy(How = How.XPath, Using = "//view-folders//crisp-card//crisp-card-content//crisp-input-text[@label='Name']//label")]
        public IWebElement FolderLabel;

        [FindsBy(How = How.XPath, Using = "//view-folders//crisp-card//crisp-card-content//crisp-input-text[@label='Name']//input")]
        public IWebElement FolderInput;

        [FindsBy(How = How.XPath, Using = "//view-folders//crisp-card//crisp-card-content//crisp-button[@click.delegate='addFolder()']//button[@class='au-target waves-effect waves-light btn-flat']")]
        public IWebElement AddFolderButton;

        [FindsBy(How = How.XPath, Using = "//upload-documents-embed//crisp-input-file//div//div//input[@ref='fileInput']")]
        public IWebElement FileInput;

        [FindsBy(How = How.XPath, Using = "//table[@class='striped']//crisp-input-date/div/label[text()='Date Received']")]
        public IWebElement DateReceivedLabel;

        [FindsBy(How = How.XPath, Using = "//div[contains(@class,'picker--opened')]/div/div/div/div/div[@class='picker__footer']/button[text()='Today']")]
        public IWebElement DateReceivedInput;

        [FindsBy(How = How.XPath, Using = "//tr//td//crisp-input-object[@class='au-target']//div[contains(@class,'au-target input-field')]//span[@ref='inputContainer']")]
        public IWebElement DocTypeDropdown1;

        [FindsBy(How = How.XPath, Using = "//crisp-card-content[@class='au-target']//div[@class='au-target list-title has-filter']//span[@class='filter-input-field']//input[@class='au-target']")]
        public IWebElement SearchDocumentType;

        [FindsBy(How = How.XPath, Using = "//ul[@class='au-target collection has-header has-filter']//li[@class='au-target collection-item']//crisp-list-item")]
        public IList<IWebElement> SearchDocumentTypeInputList;

        [FindsBy(How = How.XPath, Using = "//tr//td//crisp-input-object[@class='au-target']//div[@class='au-target input-field']//span[@ref='inputContainer']")]
        public IWebElement DocTypeDropdown;

        [FindsBy(How = How.XPath, Using = "//ul[@class='dropdown-content select-dropdown active']//li/span[text()='Initial Notice']")]
        public IWebElement DocTypeDropdownInput;

        [FindsBy(How = How.XPath, Using = "//registration-view//files-documents//crisp-header-button[@click.delegate='toggleUpload()']//button")]
        public IWebElement UploadDocButton;

        [FindsBy(How = How.XPath, Using = "//view-documents//crisp-list[@list-title='Document']//div//table//tbody//tr[@class='au-target collection-item']")]
        public IList<IWebElement> DocsList;

        [FindsBy(How = How.XPath, Using = "//files-documents//view-folders//folders//crisp-breadcrumb[@class='custom-content au-target']//a[text()='Home']")]
        public IWebElement HomeFolder;

        [FindsBy(How = How.XPath, Using = "//div[@class='crisp-list-table-content au-target']//table//tbody//tr[@class='au-target collection-item']//td//crisp-file-icon//parent::td")]
        public IList<IWebElement> FileIconList;

        [FindsBy(How = How.XPath, Using = "//router-view[@name='default']//crisp-header//div[@class='nav-wrapper au-target']//crisp-header-avatar/following-sibling::crisp-header-title[@class='au-target'][1]//div[@class='title']")]
        public IWebElement RegContactName;

        [FindsBy(How = How.XPath, Using = "//registration-view//crisp-header//crisp-header-title[3]//div[@class='title']")]
        public IWebElement RegDetails;

        //Site Specfid Reg Documents 
        [FindsBy(How = How.XPath, Using = "//crisp-tabs//div//ul[@ref='tabHeader']/li[@class='au-target tab']//span//a[contains(text(),'Conditions')]")]
        public IWebElement ConditionsTab;

        [FindsBy(How = How.XPath, Using = "//condition-list[@class='custom-element au-target']//div[@class='au-target crisp-row table-body with-header']//table[@ref='table']//tbody//tr")]
        public IList<IWebElement> ConditionsRows;

        [FindsBy(How = How.XPath, Using = "//crisp-tabs//div//ul[@ref='tabHeader']/li[@class='au-target tab']//span//a[contains(text(),'Roles')]")]
        public IWebElement RolesTab;

        [FindsBy(How = How.XPath, Using = "//role-editor//crisp-list//div[@ref='theTable']//table//tbody[@class='collection']//tr[@class='au-target collection-item']//td")]
        public IList<IWebElement> RolesRows;

        [FindsBy(How = How.XPath, Using = "//role-editor//crisp-list//div[@ref='theTable']//table//tbody[@class='collection']//tr[@class='au-target collection-item']//td[contains(text(),'Builder')]/following-sibling::td[1]")]
        public IList<IWebElement> BuilderRole;

        [FindsBy(How = How.XPath, Using = "//role-editor//crisp-list//div[@ref='theTable']//table//tbody[@class='collection']//tr[@class='au-target collection-item']//td[contains(text(),'Developer')]/following-sibling::td[1]")]
        public IList<IWebElement> DeveloperRole;

        [FindsBy(How = How.XPath, Using = "//crisp-card-content//crisp-card-actions//crisp-button[@click.delegate='viewContact(selectedRole)']//button[@class='au-target waves-effect waves-light btn-flat']")]
        public IWebElement ViewButton;

        [FindsBy(How = How.XPath, Using = "//crisp-header[@class='au-target']//nav[@class='highlight-background highlight-colour']//crisp-header-title[@class='au-target'][1]//div[@class='title']")]
        public IWebElement ContactTitle;        

        [FindsBy(How = How.XPath, Using = "//i[@class='au-target fa fa-search']")]
        public IWebElement SearchButton;

        [FindsBy(How = How.XPath, Using = "//a[@click.delegate='subItem.onClick()'][text()='Contacts']")]
        public IWebElement ContactOption;

        [FindsBy(How = How.XPath, Using = "//crisp-tabs//ul[@ref='tabHeader']//li[@class='au-target tab']//span//a[contains(text(),'PG Registration')]")]
        public IWebElement PGRegistrationTab;

        [FindsBy(How = How.XPath, Using = "//crisp-tabs//ul[@ref='tabHeader']//li[@class='au-target tab']//span//a[contains(text(),'LABC Registration')]")]
        public IWebElement LABCRegistrationTab;

        [FindsBy(How = How.XPath, Using = "//security-documents//crisp-list[contains(@source.bind,'SecurityDocuments')]//crisp-input-bool[@label='Show Site Specific Documents']//label")]
        public IList<IWebElement> ShowSiteSpecificDocs;

        [FindsBy(How = How.XPath, Using = "//security-documents//crisp-list[contains(@source.bind,'SecurityDocuments')]//ul[@ref='theList']//li[@class='au-target collection-item']")]
        public IList<IWebElement> SecurityDocsList;

        public string builderRole { get; set; }
        public string developerRole { get; set; }

        public string DocumentTypeNames;
        public static int documentTypes { get; set; }
        public static int SecurityDocumentCount { get; set; }
        public static string RegID { get; set; }
        public static string RegFolderID { get; set; }
        public static string RegDocID { get; set; }
        public static IList<string> RegDocName { get; set; }
        public static string RegNumber { get; set; }

        public static void CloseSpinneronRegPage()
        {
            //var item = By.XPath("//ai-dialog-overlay[@class='active']//following-sibling::ai-dialog-container[@class='active']")
            var wait = new WebDriverWait(Driver, TimeSpan.FromSeconds(120));
            var element = By.XPath("//ai-dialog-overlay[@class='active']//following-sibling::ai-dialog-container[@class='active']");
            var loadingElement = By.XPath("//ai-dialog-overlay[@class='active']//following-sibling::ai-dialog-container[@class='active']//div[@class='modal-content']//h4[@class='heading'][contains(text(),'Loading')]");
            wait.Until(ExpectedConditions.InvisibilityOfElementLocated(element));
            wait.Until(ExpectedConditions.InvisibilityOfElementLocated(loadingElement));
            //Thread.Sleep(100);
        }


        //Add SecurityDocs to verify documents 
        public void AddSecurityDocsForVerification()
        {
            WaitForElement(RegDocumentTab);
            RegDocumentTab.Click();
            RegNumber = RegDetails.Text;
            //Thread.Sleep(1000);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(1000);
            WaitForElements(AddSecurityDocButton);
            var addSecurityDocButtons = AddSecurityDocButton.Count;
            for (int i = 0; i < addSecurityDocButtons; i++)
            {              
                CloseSpinneronDiv();
                CloseSpinneronPage();
                Thread.Sleep(500);
                WaitForElement(AddSecurityDocButton[i]);
                WaitForElementToClick(AddSecurityDocButton[i]);                
                CloseSpinneronDiv();
                CloseSpinneronPage();
                Thread.Sleep(500);
                AddSecurityDocButton[i].Click();
                //Thread.Sleep(500);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                //Thread.Sleep(500);
                WaitForElement(AddDocsDiv);
                Assert.IsTrue(AddDocsDiv.Displayed);
                //Thread.Sleep(500);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                //Thread.Sleep(500);
                WaitForElement(DocumentTypeDropdown);
                DocumentTypeDropdown.Click();
                WaitForElements(DocumentTypeDropdownInput);
                documentTypes = DocumentTypeDropdownInput.Count;
                for (int d = 0; d < documentTypes; d++)
                {
                    Thread.Sleep(500);
                    CloseSpinneronDiv();
                    CloseSpinneronPage();                  
                    Thread.Sleep(500);
                    if (d != 0)
                    {
                        WaitForElement(AddSecurityDocButton[i]);
                        AddSecurityDocButton[i].Click();
                        //Thread.Sleep(500);
                        CloseSpinneronDiv();
                        CloseSpinneronPage();
                        //Thread.Sleep(500);
                        WaitForElement(AddDocsDiv);
                        Assert.IsTrue(AddDocsDiv.Displayed);
                        //Thread.Sleep(500);
                        CloseSpinneronDiv();
                        CloseSpinneronPage();
                        //Thread.Sleep(500);
                        WaitForElement(DocumentTypeDropdown);
                        DocumentTypeDropdown.Click();
                        WaitForElements(DocumentTypeDropdownInput);
                    }
                    DocumentTypeNames = DocumentTypeDropdownInput.ElementAt(d).Text;
                    //Thread.Sleep(1500);
                    var item = $"//ul[@class='dropdown-content select-dropdown active']//li[{d + 1}]//span";
                    //Thread.Sleep(1500);
                    Driver.FindElement(By.XPath(item)).Click();
                    //Thread.Sleep(500);
                    CloseSpinneronDiv();
                    CloseSpinneronPage();
                    Thread.Sleep(500);
                    if (ChildrenLabel.Count > 0)
                    {
                        WaitForElement(ChildrenLabel[0]);
                        ChildrenLabel[0].Click();
                        //Thread.Sleep(500);
                        CloseSpinneronDiv();
                        CloseSpinneronPage();
                        WaitForLoadElements(ChildrenList);
                        //Thread.Sleep(500);
                        if (ChildrenList.Count > 0)
                        {
                            //Thread.Sleep(500);
                            CloseSpinneronDiv();
                            CloseSpinneronPage();
                            //Thread.Sleep(500);
                            WaitForElement(ChildrenList[0]);
                            WaitForElementToClick(ChildrenList[0]);
                            ChildrenList[0].Click();
                            //Thread.Sleep(500);                         
                            CloseSpinneronDiv();
                            CloseSpinneronPage();
                            Thread.Sleep(500);
                        }
                        WaitForElement(SelectButton);
                        SelectButton.Click();
                        Thread.Sleep(500);
                        CloseCrispCard();
                        CloseSpinneronDiv();
                        CloseSpinneronPage();
                        Thread.Sleep(500);
                    }
                    WaitForElement(OkButton);
                    WaitForElementToClick(OkButton);
                    CloseSpinneronDiv();
                    CloseSpinneronPage();
                    Thread.Sleep(500);
                    OkButton.Click();
                    Thread.Sleep(500);
                    CloseSpinneronDiv();
                    CloseSpinneronPage();
                    CloseSpinneronRegPage();
                    Thread.Sleep(500);
                }
            }
        }
        public void SiteSpecificRegDocs()
        {
            //Thread.Sleep(1000);
            if (ConditionsRows.Count <= 0)
            {
                Dashboardpage.SelectOrder();
            }
            WaitForElement(ConditionsTab);
            ConditionsTab.Click();
            //Thread.Sleep(1500);
            CloseSpinneronPage();
            WaitForElements(ConditionsRows);
            if (ConditionsRows.Count > 0)
            {
                //Thread.Sleep(1000);
                var securityDocCondition = ConditionsRows.Any(x => x.Text.Contains("The self build indemnity agreement for the builder is outstanding"));
                if (securityDocCondition == true)
                {
                  
                    WaitForElement(RolesTab);
                    RolesTab.Click();
                    WaitForElements(RolesRows);
                    if (BuilderRole.Count > 0)
                    {
                        builderRole = BuilderRole[0].Text;
                    }
                    
                    //Thread.Sleep(1000);
                    CloseSpinneronDiv();
                    CloseSpinneronPage();
                    //Thread.Sleep(1000);
                    if (builderRole != null)
                    {
                        WaitForElement(BuilderRole[0]);
                        WaitForElementToClick(BuilderRole[0]);
                        BuilderRole[0].Click();
                        //Thread.Sleep(500);
                        CloseSpinneronDiv();
                        CloseSpinneronPage();
                        //Thread.Sleep(500);
                        WaitForElement(ViewButton);
                        WaitForElementToClick(ViewButton);
                        ViewButton.Click();
                        WaitForElement(ContactTitle);
                        var contactTitletext = ContactTitle.Text;
                        Assert.AreEqual(contactTitletext, builderRole);
                        if (Statics.OrderNumber.Contains("PL-PG"))
                        {
                            WaitForElement(PGRegistrationTab);
                            PGRegistrationTab.Click();
                            //Thread.Sleep(1000);
                            SiteSpecificDocs();
                            //Thread.Sleep(1000);
                        }
                        if (Statics.OrderNumber.Contains("PL-LABC"))
                        {
                            WaitForElement(LABCRegistrationTab);
                            LABCRegistrationTab.Click();
                            //Thread.Sleep(1000);
                            SiteSpecificDocs();
                            //Thread.Sleep(1000);
                        }
                        CloseSpinneronDiv();
                        CloseSpinneronPage();
                        //Thread.Sleep(1000);
                    }
                }
            }
        }
        public void SiteSpecificDocs()
        {
            WaitForElement(RegDocumentTab);
            RegDocumentTab.Click();
            //Thread.Sleep(1000);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            if (ShowSiteSpecificDocs.Count > 0)
            {
                WaitForElement(ShowSiteSpecificDocs[0]);
                ShowSiteSpecificDocs[0].Click();
                Thread.Sleep(500);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                //Thread.Sleep(1000);
            }
            Assert.IsTrue(SecurityDocsList.Count > 0);
            RegNumber = RegDetails.Text;
        }


        public void DownloadDocsOnFileReview()
        {
            ExtensionMethods.EmptyFolder(new DirectoryInfo(ExtensionMethods.GetAbsolutePath(@"TestData\Downloads")));
            WaitForElement(ViewInformation);
            if (ViewInformation.Enabled)
            {
                //Thread.Sleep(1000);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                //Thread.Sleep(1000);
                WaitForElement(ViewInformation);
                WaitForElementToClick(ViewInformation);
                ViewInformation.Click();
                //Thread.Sleep(500);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                //Thread.Sleep(500);
                WaitForElement(FileReviewWizard);
                Assert.IsTrue(FileReviewWizard.Displayed);
                //Thread.Sleep(500);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                //Thread.Sleep(500);
                WaitForElement(StepTitle);
                var StepTitleName = StepTitle.Text;
                if (StepTitleName.Contains("PROCESS / PLOTS"))
                {
                    Assert.IsTrue(StepTitle.Text.Contains("PROCESS / PLOTS"));
                    WaitForElement(FileReviewNextButton);
                    WaitForElementToClick(FileReviewNextButton);
                    FileReviewNextButton.Click();
                    //Thread.Sleep(500);
                    CloseSpinneronDiv();
                    CloseSpinneronPage();
                    //Thread.Sleep(500);
                }
                WaitForElement(StepTitle);
                StepTitleName = StepTitle.Text;
                if (StepTitleName.Contains("O/S INFORMATION"))
                {
                    Assert.IsTrue(StepTitle.Text.Contains("O/S INFORMATION"));
                    WaitForElement(FileReviewNextButton);
                    WaitForElementToClick(FileReviewNextButton);
                    FileReviewNextButton.Click();
                    //Thread.Sleep(500);
                    CloseSpinneronDiv();
                    CloseSpinneronPage();
                    //Thread.Sleep(500);
                }
                WaitForElement(StepTitle);
                StepTitleName = StepTitle.Text;
                if (StepTitleName.Contains("CREATE CHASE EMAIL"))
                {
                    Assert.IsTrue(StepTitle.Text.Contains("CREATE CHASE EMAIL"));
                    WaitForElements(DownloadFile);
                    if (DownloadFile.Count > 0)
                    {
                        var downloadFiles = DownloadFile.Count;
                        for (int i=0; i<downloadFiles;i++)
                        {
                            RegDocName = DownloadFile.Select(d=>d.Text.ToString()).ToList();
                            WaitForElement(DownloadFile[i]);
                            WaitForElementToClick(DownloadFile[i]);
                            DownloadFile[i].Click();
                            //Thread.Sleep(500);
                            CloseSpinneronDiv();
                            CloseSpinneronPage();
                            //Thread.Sleep(500);
                            RegistrationDocComparision();
                        }
                    }
                    //Thread.Sleep(500);
                    CloseSpinneronDiv();
                    CloseSpinneronPage();
                    //Thread.Sleep(500);
                    WaitForElement(SendChaseEmailButton);
                    WaitForElementToClick(SendChaseEmailButton);
                    SendChaseEmailButton.Click();
                    //Thread.Sleep(500);
                    CloseSpinneronDiv();
                    CloseSpinneronPage();
                    //Thread.Sleep(500);
                }
            }
        }
        public void RegistrationDocComparision()
        {
            var actualfileInfo = "";
            //Thread.Sleep(1000);
            CloseSpinneronPage();
            CloseSpinneronDiv();
            //Thread.Sleep(1000);
            var directory = new DirectoryInfo($@"{Statics.DownloadFolder}");
            //Thread.Sleep(1000);
            var myFile = directory.GetFiles()
     .OrderByDescending(f => f.LastWriteTime)
     .First();
            string renameMyFile = myFile.ToString();
            string contactName = RegContactName.Text;
            string actualdocName = renameMyFile.Replace($" - {contactName}.pdf","");
            //string actualdocName = RegDocName.FirstOrDefault(d=>d.Contains(newFileName));
            var UIRegPDFPath = new FileInfo(ExtensionMethods.GetAbsolutePath($@"{Statics.DownloadFolder}\{myFile}"));
            if (RegNumber.Contains("Reference REG-PG"))
            {
               actualfileInfo = new FileInfo(ExtensionMethods.GetAbsolutePath($@"TestData\RegDocuments\PG\{actualdocName}.pdf")).ToString();

            }
            if (RegNumber.Contains("Reference REG-LABC"))
            {
                actualfileInfo = new FileInfo(ExtensionMethods.GetAbsolutePath($@"TestData\RegDocuments\LABC\{actualdocName}.pdf")).ToString();

            }
            string UIRegDocument = UIRegPDFPath.ToString();
            string ActualRegDocument = actualfileInfo;
            Console.WriteLine("Started Comparing Document On: " + actualdocName);
           // log.Info("Started Comparing Document On: " + actualdocName);
            DocumentComparisionPage.CompareTwoPDF(UIRegDocument, ActualRegDocument);
            Console.WriteLine("Finished Comparing Document On : " + actualdocName);
           // log.Info("Finished Comparing Document On: " + actualdocName);

            //public void DownloadRegDocFromUI()
            //{
            //    WaitForElement(FilesDocsTab);
            //    FilesDocsTab.Click();
            //    WaitForElements(DefaultFolders);
            //    if(DefaultFolders.Any(x=>x.Text.Contains("Registration")))
            //    {
            //        var Regfolder = DefaultFolders.First(x => x.Text.Contains("Registration"));
            //        Regfolder.Click();
            //        WaitForElements(FileIconList);
            //        if(FileIconList.Count>0)
            //        {
            //            List<String> FileName = FileIconList.Select(i => i.Text.ToString()).ToList();
            //            var actualFileName = FileName.First(x => x.Contains("Builder Counter Indemnity Agreement v2"));
            //            //Thread.Sleep(500);
            //            var downloadButton = $"//div[@class='crisp-list-table-content au-target']//table//tbody//tr[@class='au-target collection-item']//td//crisp-file-icon//parent::td[contains(text(),'{actualFileName}')]//parent::tr//td//crisp-download-file//crisp-button[@icon='download']";
            //            var ClickDownload = Driver.FindElement(By.XPath(downloadButton));
            //            ClickDownload.Click();
            //        }
            //    }
            //}
            //public void Download()
            //{
            //    GetPGRegistrationIdForPerson().GetAwaiter().GetResult();
            //    GetDocumentFolderId().GetAwaiter().GetResult();
            //    GetRegDocumentDetails().GetAwaiter().GetResult();
            //    DownloadDocuments().GetAwaiter().GetResult();
            //}

            //public async Task GetPGRegistrationIdForPerson()
            //{
            //    string personId = "6a25eaed-4dc5-4468-a9be-a98300f464d2";
            //    string brand = "1";
            //    var envrironmentApiServer = ConfigurationManager.AppSettings["ApiServerUrl"];
            //    var client = HttpClientHelpers.CreateClient("scott.williams@mdinsurance.co.uk", "Mdis@1234;");
            //    var RegistrationID = await client.GetAsync($"{envrironmentApiServer}/api/registration/registrationSummaryForPerson/{personId}");
            //    var response = await RegistrationID.Content.ReadAsStringAsync();
            //    var data = JArray.Parse(response) as JArray;
            //    dynamic registrations = data;       

            //    foreach (var registration in registrations)
            //    {
            //        if (registration.registration.brand == brand)
            //        {
            //            Console.WriteLine("The ID is" + registration.registration.id);
            //            RegID = registration.registration.id;
            //        }

            //    }

            //}    

            //public async Task GetDocumentFolderId()
            //{          
            //    var envrironmentApiServer = ConfigurationManager.AppSettings["ApiServerUrl"];
            //    var client = HttpClientHelpers.CreateClient("scott.williams@mdinsurance.co.uk", "Mdis@1234;");
            //    string body = string.Format("{{\"entityType\":14,\"entityId\":\"{0}\",\"start\":0,\"count\":250,\"sortBy\":\"name\",\"sortDirection\":\"asc\"}}", RegID.ToString());
            //    var response = await client.PostAsync($"{envrironmentApiServer}/api/documentfolder/search", new StringContent(body, Encoding.Default, "application/json"));
            //    string responseContent = await response.Content.ReadAsStringAsync();
            //     var data = JObject.Parse(responseContent);
            //    dynamic Folders = data;
            //    foreach (var folder in Folders.data)
            //    {
            //        if (folder.name == "Registration")
            //        {
            //            Console.WriteLine("The ID is" + folder.id);
            //            RegFolderID = folder.id;
            //        }
            //    }           
            //}

            //public async Task GetRegDocumentDetails()
            //{
            //    var envrironmentApiServer = ConfigurationManager.AppSettings["ApiServerUrl"];
            //    var client = HttpClientHelpers.CreateClient("scott.williams@mdinsurance.co.uk", "Mdis@1234;");
            //    string body = string.Format("{{\"parentFolderId\":\"{0}\",\"fileType\":0,\"entityType\":0,\"fileName\":\"\",\"originalFileName\":\"\",\"tags\":\"\",\"batchId\":\"\",\"includeDiscontinued\":false,\"internalOnly\":false,\"externalOnly\":false,\"filterByParentFolder\":true,\"start\":0,\"count\":30,\"sortBy\":\"DateCreated\",\"sortDirection\":\"desc\"}}", RegFolderID);
            //    var response = await client.PostAsync($"{envrironmentApiServer}/api/document/search", new StringContent(body, Encoding.Default, "application/json"));
            //    string responseContent = await response.Content.ReadAsStringAsync();
            //    var data = JObject.Parse(responseContent);
            //    dynamic Documents = data;
            //    foreach (var document in Documents.data)
            //    {
            //        if (document.fileName == "Builder Counter Indemnity Agreement v2 - Mr Test53d8ec8c-53a9-48cc-aec4-89f20b34e34a Sunkishala.pdf")
            //        {
            //            Console.WriteLine("The ID is" + document.fileId);
            //            RegDocID = document.fileId;
            //            RegDocName = document.fileName;
            //        }
            //    }
            //}

            //public async Task DownloadDocuments()
            //{
            //    var envrironmentApiServer = ConfigurationManager.AppSettings["ApiServerUrl"];
            //    var client = HttpClientHelpers.CreateClient("scott.williams@mdinsurance.co.uk", "Mdis@1234;");
            //    string body = string.Format("");
            //    var response = await client.PostAsync($"{envrironmentApiServer}/api/file/{RegDocID}/{RegDocName}", new StringContent("application/json"));
            //    string responseContent = await response.Content.ReadAsStringAsync();
            //    var data = JObject.Parse(responseContent);     

            //}
        }
    }
}
