﻿using CrispAutomation.Features;
using CrispAutomation.Support;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.PageObjects;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CrispAutomation.Pages
{
    public class AddLABCQuotePage : Support.Pages
    {
        public IWebDriver wdriver;

        public AddLABCQuotePage(ISearchContext driver)
        {
            PageFactory.InitElements(driver, this);
            wdriver = (RemoteWebDriver)driver;
        }

        [FindsBy(How = How.XPath, Using = "//crisp-dialog[@class='au-target']//div//crisp-header[@class='au-target']//nav")]
        public IWebElement AddNewQuoteDiv;

        //Key Site Details Page Elements

        [FindsBy(How = How.XPath, Using = "//crisp-input-radio[@label='Brand']//div[@class='au-target']//ul//li//label[text()='LABC Warranty']")]
        public IList<IWebElement> LABCRadioButton;

        [FindsBy(How = How.XPath, Using = "//crisp-content//crisp-input-object//div//label[text()='Quote Recipient']")]
        public IWebElement QuoteRecipientLabel;

        [FindsBy(How = How.XPath, Using = "//crisp-input-object/div[@class='au-target input-field']/span[@ref='inputContainer']/span/div/span[@class='au-target']")]
        public IList<IWebElement> QuoteRecipientInput;

        [FindsBy(How = How.XPath, Using = "//contact-list/crisp-list[@ref='listElm']/ul[@ref='theList']/li[@class='au-target collection-item']//crisp-list-item//a[contains(@class,'au-target row list-item-contents clickable hasAvatar')]")]
        public IList<IWebElement> ChooseBuilder;

        [FindsBy(How = How.CssSelector, Using = "crisp-input-text[label = 'Search'] > div > label")]
        public IWebElement SearchLabel;

        [FindsBy(How = How.CssSelector, Using = "crisp-input-text[label = 'Search'] > div > input")]
        public IWebElement SearchInput;

        [FindsBy(How = How.XPath, Using = "//crisp-list[@ref='listElm']//ul//li[@class='au-target collection-item']//crisp-list-item[@class='au-target']//a[contains(@class,'au-target row list-item-contents clickable hasAvatar')]")]
        public IList<IWebElement> QuoteRecipientList;   

        [FindsBy(How = How.XPath, Using = "//crisp-list[@ref='listElm']//ul//li[@class='au-target collection-item']//crisp-list-item[@class='au-target']//a[contains(@class,'au-target row list-item-contents clickable hasAvatar')]//crisp-list-item-title//span//small")]
        public IList<IWebElement> QuoteRecipienttitle;

        [FindsBy(How = How.XPath, Using = "//crisp-input-object//div//label[contains(text(),'Office')]")]
        public IList<IWebElement> QuoteRecipientOfficeLabel;

        [FindsBy(How = How.XPath, Using = "//ul//li[@class='au-target collection-item']//crisp-list-item/a")]
        public IList<IWebElement> QuoteRecipientOfficeInput;

        [FindsBy(How = How.XPath, Using = "//crisp-content/crisp-input-object//div//label[text()='Site Address']")]
        public IWebElement SiteAddressLabel;

        [FindsBy(How = How.XPath, Using = "//crisp-content/crisp-input-object//div//span//view-address")]
        public IList<IWebElement> SiteAddressInput;

        [FindsBy(How = How.XPath, Using = "//crisp-button/span/button[text()='Enter Address Using Postcode'][@class='au-target waves-effect waves-light btn-flat']")]
        public IWebElement EnterAddressUsingPostcodeButton;

        [FindsBy(How = How.CssSelector, Using = "crisp-input-text[label='Enter postcode to search'] > div > label")]
        public IWebElement PostcodeSearchLabel;

        [FindsBy(How = How.CssSelector, Using = "crisp-input-text[label='Enter postcode to search'] > div > input")]
        public IWebElement PostcodeInput;

        [FindsBy(How = How.XPath, Using = "//crisp-picker[@label='Address']/div/div/input[@class='select-dropdown']")]
        public IWebElement AddressDropdown;

        [FindsBy(How = How.XPath, Using = "//ul[@class='dropdown-content select-dropdown active']/li[1]")]
        public IWebElement AddressDropdownInput;

        [FindsBy(How = How.XPath, Using = "//crisp-button/span/button[@class='au-target waves-effect waves-light btn'][text()='Set Address']")]
        public IWebElement SetAddressButton;

        [FindsBy(How = How.XPath, Using = "//crisp-picker[@label='Insurance Agreement Group']//div[@class='au-target input-field']//div[@class='select-wrapper au-target']")]
        public IList<IWebElement> InsuranceGroup;

        [FindsBy(How = How.XPath, Using = "//ul[@class='dropdown-content select-dropdown active']//li//span")]
        public IList<IWebElement> InsuranceGroupInput;

        [FindsBy(How = How.XPath, Using = "//crisp-input-object[@label='Quote Application Documents']//span[contains(@class,'au-target input-container multi-item')]//span")]
        public IWebElement AddQuoteAppDocs;

        [FindsBy(How = How.XPath, Using = "//crisp-dialog//crisp-header//div[text()='Upload document']")]
        public IWebElement UploadDocWizard;        

        [FindsBy(How = How.XPath, Using = "//crisp-input-file[@label='Select file...']//div//div//input[@class='au-target']")]
        public IWebElement UploadDoc;

        [FindsBy(How = How.XPath, Using = "//crisp-button[@click.delegate='ok()'][@class='au-target']//span//button[text()='OK'][@class='au-target waves-effect waves-light btn-flat']")]
        public IWebElement UploadOkButton;

        [FindsBy(How = How.XPath, Using = "//crisp-button[@class='au-target'][@click.delegate='wizard.goForward()']//span//button[@class='au-target waves-effect waves-light btn-flat'][text()='Next']")]
        public IWebElement NextButton;

        [FindsBy(How = How.XPath, Using = "//div[@class='modal-content']//crisp-wizard[@active-step.bind='activeStep']//crisp-stepper-step[contains(@class,'current')]//div[@title.bind='description'][contains(@class,'title')]")]
        public IWebElement StepTitle;

        //Oter Site Details Page Elements 

        [FindsBy(How = How.XPath, Using = "//crisp-input-date/div/label[text()='Construction start date']")]
        public IWebElement ConstructionStartDateLabel;

        [FindsBy(How = How.XPath, Using = "//div[contains(@class,'picker--focused')]/div/div/div/div/div[2]/table[@class='picker__table']/tbody//tr//td/div[text()='10']")]
        public IWebElement ConstructionStartDateInput;

        [FindsBy(How = How.XPath, Using = "//crisp-input-date[@label='Construction end date']/div/label")]
        public IWebElement ConstructionEndDateLabel;

        [FindsBy(How = How.XPath, Using = "//div[contains(@class,'picker--focused')]/div/div/div/div/div[2]/table[@class='picker__table']/tbody//tr//td/div[contains(@class,'day--infocus')][text()='28']")]
        public IWebElement ConstructionEndDateInput;

        [FindsBy(How = How.CssSelector, Using = "div:nth-child(1) > crisp-input-number:nth-child(3) > div > label")]
        public IWebElement NumberOffStoreysAboveGround;

        [FindsBy(How = How.CssSelector, Using = "div:nth-child(1) > crisp-input-number:nth-child(3) > div > input")]
        public IWebElement NumberOffStoreysAboveGroundInput;

        [FindsBy(How = How.CssSelector, Using = "div:nth-child(1) > crisp-input-number:nth-child(4) > div > label")]
        public IWebElement NumberOffStoreysBelowGround;

        [FindsBy(How = How.CssSelector, Using = "div:nth-child(1) > crisp-input-number:nth-child(4) > div > input")]
        public IWebElement NumberOffStoreysBelowGroundInput;

        [FindsBy(How = How.XPath, Using = "//crisp-input-radio[@label='Any innovative construction methods?']/div/ul/li/label[text()='No']")]
        public IWebElement InnovativeMethodsRadioButton;

        [FindsBy(How = How.XPath, Using = "//crisp-input-radio[@label='Is site in administration?']/div/ul/li/label[text()='No']")]
        public IWebElement SiteAdminRadioButton;

        //Plot Schedule Page Elements 

        [FindsBy(How = How.XPath, Using = "//crisp-header-button[@click.delegate='importPlots()']//span//button[@class='au-target waves-effect waves-light btn-flat']")]
        public IWebElement ImportPlotScheduleButton;

        [FindsBy(How = How.XPath, Using = "//div[@ref='theTable']//tbody//tr[@class='au-target collection-item']//td[2]")]
        public IWebElement WarrentyProduct;

        [FindsBy(How = How.XPath, Using = "//crisp-wizard-step[@step-title='Plot Schedule']//tbody//tr[@class='au-target collection-item']/td[3]")]
        public IWebElement BCProduct;

        [FindsBy(How = How.XPath, Using = "//crisp-wizard-step[@step-title='Plot Schedule']//tbody//tr[@class='au-target collection-item']/td[4]")]
        public IWebElement UnitType;

        [FindsBy(How = How.XPath, Using = "//crisp-wizard-step[@step-title='Plot Schedule']//tbody//tr[@class='au-target collection-item']/td[5]")]
        public IWebElement StagesOfWorks;

        //Product Details Page Elements

        [FindsBy(How = How.XPath, Using = "//div//crisp-input-object[@class='au-target']//div//label[text()='Product version']")]
        public IList<IWebElement> ProductVersion;

        [FindsBy(How = How.XPath, Using = "//crisp-wizard-step[@step-title='Products']//ul//li//crisp-list-item//div[@class='crisp-row']//span//dl//span//dd[contains(text(),'Aviva')]")]
        public IWebElement AvivaInsurer;

        [FindsBy(How = How.XPath, Using = "//crisp-input-radio[@label='Cover length']//div//ul//li//label[text()='10']")]
        public IList<IWebElement> CoverLengthList;

        [FindsBy(How = How.XPath, Using = "//crisp-list/ul[@class='au-target collection has-header has-filter']//li[@class='au-target collection-item']")]
        public IList<IWebElement> ChooseProductVersionList;

        [FindsBy(How = How.XPath, Using = "//crisp-input-radio[@label='Are the works contracts under seal?']/div/ul/li/label[text()='No']")]
        public IList<IWebElement> ContractsUnderSeal;

        [FindsBy(How = How.XPath,Using = "//crisp-input-currency[@label='Contract cost']//div//label[text()='Contract cost'][@class='au-target']")]
        public IList<IWebElement> ContractCostLabel;

        [FindsBy(How = How.XPath, Using = "//crisp-input-currency[@label='Contract cost']//div//input")]
        public IList<IWebElement> ContractCostInput;

        //Choose core and Services List
     
        [FindsBy(How = How.XPath, Using = "//crisp-wizard-step[@step-title='Products']//crisp-card-content[@class='au-target']//crisp-input-object//div[@class='au-target input-field']//label[text()='Optional Covers']//parent::div//i[@click.delegate='addClicked()'][@class='fa fa-plus add action au-target']")]
        public IList<IWebElement> OptionalCovers;

        [FindsBy(How = How.XPath, Using = "//crisp-card[@class='au-target']//crisp-list//ul[@ref='theList']//li[@class='au-target collection-item']//a[@class='au-target row list-item-contents clickable']")]
        public IList<IWebElement> ChooseCoversList;       

        [FindsBy(How = How.XPath, Using = "//crisp-card-actions//div//crisp-button[@click.delegate='ok()']//span//button[@class='au-target waves-effect waves-light btn']")]
        public IList<IWebElement> SelectButton;

        //Additional Questions Page Elements

        [FindsBy(How = How.XPath, Using = "//crisp-input-radio[@label='Are any units attached or structurally connected to any other structure not included within this application?']/div/ul/li/label[text()='No']")]
        public IList<IWebElement> NoOfUnitsAttachedRadioButton;

        [FindsBy(How = How.XPath, Using = "//crisp-input-radio[@label='Does the site include Curtain Walling/Glazing?']/div/ul/li/label[text()='No']")]
        public IList<IWebElement> WallingandGlazingRadioButton;

        [FindsBy(How = How.XPath, Using = "//crisp-wizard-step[@step-title='Additional Questions']//div//h3[text()='Conversions / Refurbishments']")]
        public IList<IWebElement> ConversionsAndRefurbishments;

        [FindsBy(How = How.XPath, Using = "//crisp-input-text[@label='Description of Works']//div//label")]
        public IWebElement DescriptionOfWorkLabel;

        [FindsBy(How = How.XPath, Using = "//crisp-input-text[@label='Description of Works']//div//input")]
        public IWebElement DescriptionOfWorkInput;

        [FindsBy(How = How.XPath, Using = "//div//crisp-input-year[@label='What was the approximate year of original build?']//div//label")]
        public IWebElement OrginalBuildLabel;

        [FindsBy(How = How.XPath, Using = "//div//crisp-input-year[@label='What was the approximate year of original build?']//div//input")]
        public IWebElement OrginalBuildInput;

        [FindsBy(How = How.XPath, Using = "//crisp-picker[@label='What was the previous use of the building?']//div/input")]
        public IWebElement PreviousBuildLabel;

        [FindsBy(How = How.XPath, Using = "//ul[@class='dropdown-content select-dropdown active']/li/span[text()='Residential']")]
        public IWebElement PreviousBuildInput;

        [FindsBy(How = How.XPath, Using = " //crisp-picker[@label='What is the listed status of the building?']//div/input")]
        public IWebElement StatusOfBuildLabel;

        [FindsBy(How = How.XPath, Using = "//ul[@class='dropdown-content select-dropdown active']/li/span[text()='Grade II']")]
        public IWebElement StatusOfBuildInput;

        [FindsBy(How = How.XPath, Using = "//crisp-input-radio[@label='Is the site in a conservation area?']//label[text()='No']")]
        public IWebElement ConversionArea;

        [FindsBy(How = How.XPath, Using = "//crisp-input-radio[@label='Have any other surveys or tests been carried out on the existing structure?']//label[text()='No']")]
        public IWebElement ExistingStructureSurvey;

        [FindsBy(How = How.XPath, Using = "//crisp-input-radio[@label='Has a condition survey been carried out?']//label[text()='No']")]
        public IWebElement ConditionSurvey;

        [FindsBy(How = How.XPath, Using = "//crisp-input-radio[@label='Has the Developer / Builder had experience in conversion or refurbishment projects?']//label[text()='No']")]
        public IWebElement BuilderExperienceInConversion;

        [FindsBy(How = How.XPath, Using = "//crisp-input-radio[@label='Does the development contain any barn conversions?']//label[text()='No']")]
        public IWebElement BarnConversion;

        [FindsBy(How = How.XPath, Using = "//crisp-wizard-step//crisp-content//div//h3[text()='Self Build']")]
        public IList<IWebElement> SelfBuildAddtionalQuetions;

        [FindsBy(How = How.XPath, Using = "//crisp-input-radio[contains(@label,'Is an architect involved in this project?')]/div/ul/li/label[text()='No']")]
        public IWebElement IsArchitectOpen;

        [FindsBy(How = How.XPath, Using = "//crisp-input-radio[contains(@label,'Has the home owner built')]/div/ul/li/label[text()='No']")]
        public IWebElement HomeOwnerBuilt;

        [FindsBy(How = How.XPath, Using = "//crisp-input-radio[contains(@label,'Is sole place of residence?')]/div/ul/li/label[text()='No']")]
        public IWebElement SolePlaceOFResidence;

        [FindsBy(How = How.XPath, Using = "//crisp-input-number[@label='Number of Stage Payments']//div//label[text()='Number of Stage Payments']")]
        public IWebElement StagePaymentLabel;

        [FindsBy(How = How.XPath, Using = "//crisp-input-number[@label='Number of Stage Payments']//div//input")]
        public IWebElement StagePaymentInput;

        [FindsBy(How = How.XPath, Using = "//crisp-wizard-step//crisp-content//div//h3[text()='Completed Housing']")]
        public IList<IWebElement> CompletedHousingAdditionalQuetions;

        [FindsBy(How = How.XPath, Using = "//crisp-wizard-step[@step-title='Additional Questions']//crisp-content//div//crisp-input-textarea[contains(@label,'Reason why a structural warranty was not previously put in place')]//div//label")]
        public IWebElement StructuralWarrentyReasonLabel;

        [FindsBy(How = How.XPath, Using = "//crisp-wizard-step[@step-title='Additional Questions']//crisp-content//div//crisp-input-textarea[contains(@label,'Reason why a structural warranty was not previously put in place')]//div//textarea")]
        public IWebElement StructuralWarrentyReasonInput;

        [FindsBy(How = How.XPath, Using = "//crisp-input-number[@label='Loss of Gross Profit - Amount']/div/label")]
        public IList<IWebElement> LossOfGrossProfit;

        [FindsBy(How = How.XPath, Using = "//crisp-input-number[@label='Loss of Gross Profit - Amount']/div/input")]
        public IWebElement LossOfGrossProfitInput;

        [FindsBy(How = How.XPath, Using = "//crisp-input-number[@label='Loss of Rent Receivable - Amount']/div/label")]
        public IList<IWebElement> LossOfRentReceivable;

        [FindsBy(How = How.XPath, Using = "//crisp-input-number[@label='Loss of Rent Receivable - Amount']/div/input")]
        public IWebElement LossOfRentReceivableInput;

        [FindsBy(How = How.XPath, Using = "//crisp-input-number[@label='Loss of Rent Payable - Amount']/div/label")]
        public IList<IWebElement> LossOfRentPayable;

        [FindsBy(How = How.XPath, Using = "//crisp-input-number[@label='Loss of Rent Payable - Amount']/div/input")]
        public IWebElement LossOfRentPayableInput;

        [FindsBy(How = How.XPath, Using = "//crisp-input-number[@label='Loss of Gross Profit - Number of years']/div/label")]
        public IList<IWebElement> LossOfGrossProfitYears;

        [FindsBy(How = How.XPath, Using = "//crisp-input-number[@label='Loss of Gross Profit - Number of years']/div/input")]
        public IWebElement LossOfGrossProfitYearsInput;

        [FindsBy(How = How.XPath, Using = "//crisp-input-number[@label='Loss of Rent Receivable - Number of years']/div/label")]
        public IList<IWebElement> LossOfRentReceivableYears;

        [FindsBy(How = How.XPath, Using = "//crisp-input-number[@label='Loss of Rent Receivable - Number of years']/div/input")]
        public IWebElement LossOfRentReceivableYearsInput;

        [FindsBy(How = How.XPath, Using = "//crisp-input-number[@label='Loss of Rent Payable - Number of years']/div/label")]
        public IList<IWebElement> LossOfRentPayableYears;

        [FindsBy(How = How.XPath, Using = "//crisp-input-number[@label='Loss of Rent Payable - Number of years']/div/input")]
        public IWebElement LossOfRentPayableYearsInput;

        [FindsBy(How = How.XPath, Using = "//crisp-input-number[@label='Duration in months atkins required on site?']/div/label")]
        public IList<IWebElement> AtkinsDurationLabel;

        [FindsBy(How = How.XPath, Using = "//crisp-input-number[@label='Duration in months atkins required on site?']/div/label[@class='au-target active']/following-sibling::input")]
        public IWebElement AtkinsDurationInput;

        //Roles Page Elements    

        [FindsBy(How = How.XPath, Using = "//crisp-wizard-step[@step-title='Roles']//div[@class='crisp-list-table-content au-target']//tbody[@class='collection']")]
        public IWebElement RolesTable;

        [FindsBy(How = How.XPath, Using = "//crisp-wizard-step[contains(@step-title,'Roles')]//role-editor//div//crisp-list//crisp-loader[@class='au-target aurelia-hide']//following-sibling::div[@class='au-target list-title has-filter']//crisp-header-button[@click.delegate='editRoles()']//span//button[@class='au-target waves-effect waves-light btn-flat']")]
        public IWebElement EditRoleButton;

        [FindsBy(How = How.XPath, Using = "//crisp-card-actions// div //crisp-button// span// button[@class='au-target waves-effect waves-light btn-flat'][text()='Cancel']")]
        public IWebElement CancelButton;

        [FindsBy(How = How.XPath, Using = "//crisp-card-content[@class='au-target']//ul[@ref='theList']//li[contains(@class,'au-target collection-item')]//crisp-list-item//a[contains(@class,'au-target row list-item-contents clickable')]//crisp-list-item-title//div//span[text()='Builder']")]
        public IList<IWebElement> BuilderRole;

        [FindsBy(How = How.XPath, Using = "//crisp-card-content[@class='au-target']//ul[@ref='theList']//li[@class='au-target collection-item']//crisp-list-item//a[contains(@class,'au-target row list-item-contents clickable')]//crisp-list-item-title//div//span[text()='Structural Referral Administrator']")]
        public IList<IWebElement> StructuralRefAdminRole;

        [FindsBy(How = How.XPath, Using = "//crisp-card-content[@class='au-target']//ul[@ref='theList']//li[@class='au-target collection-item']//crisp-list-item//a[contains(@class,'au-target row list-item-contents clickable')]//crisp-list-item-title//div//span[text()='Sales Account Manager']")]
        public IList<IWebElement> SalesAccountMaangerRole;

        [FindsBy(How = How.XPath, Using = "//crisp-card-content[@class='au-target']//ul[@ref='theList']//li[@class='au-target collection-item']//crisp-list-item//a[contains(@class,'au-target row list-item-contents clickable')]//crisp-list-item-title//div//span[text()='Hub Administrator']")]
        public IList<IWebElement> HubAdministratorRole;

        [FindsBy(How = How.XPath, Using = "//crisp-card-content[@class='au-target']//ul[@ref='theList']//li[@class='au-target collection-item']//crisp-list-item//a[contains(@class,'au-target row list-item-contents clickable')]//crisp-list-item-title//div//span[text()='Design Review Administrator']")]
        public IList<IWebElement> DesignReviewAdminRole;

        [FindsBy(How = How.XPath, Using = "//crisp-card-content[@class='au-target']//ul[@ref='theList']//li[@class='au-target collection-item']//crisp-list-item//a[contains(@class,'au-target row list-item-contents clickable')]//crisp-list-item-title//div//span[text()='Applicant (Self-build)']")]
        public IList<IWebElement> SelfBuildApplicantRole;

        [FindsBy(How = How.XPath, Using = "//crisp-card-content[@class='au-target']//ul[@ref='theList']//li[@class='au-target collection-item']//crisp-list-item//a[contains(@class,'au-target row list-item-contents clickable')]//crisp-list-item-title//div//span[text()='Housing Association']")]
        public IList<IWebElement> HousingAssociationRole;

        [FindsBy(How = How.XPath, Using = "//crisp-card-content[@class='au-target']//ul[@ref='theList']//li[@class='au-target collection-item']//crisp-list-item//a[contains(@class,'au-target row list-item-contents clickable')]//crisp-list-item-title//div//span[text()='PRS Company']")]
        public IList<IWebElement> PRSCompanyRole;

        [FindsBy(How = How.XPath, Using = "//crisp-card-content[@class='au-target']//ul[@ref='theList']//li[@class='au-target collection-item']//crisp-list-item//a[contains(@class,'au-target row list-item-contents clickable')]//crisp-list-item-title//div//span[text()='Major Projects Manager']")]
        public IList<IWebElement> MajorProjectManagerRole;

        [FindsBy(How = How.XPath, Using = "//crisp-card-content[@class='au-target']//ul[@ref='theList']//li[@class='au-target collection-item']//crisp-list-item//a[contains(@class,'au-target row list-item-contents clickable')]//crisp-list-item-title//div//span[text()='Design Review Surveyor']")]
        public IList<IWebElement> DesignReviewSurveyorRole;

        [FindsBy(How = How.XPath, Using = "//crisp-card-content[@class='au-target']//ul[@ref='theList']//li[@class='au-target collection-item']//crisp-list-item//a[contains(@class,'au-target row list-item-contents clickable')]//crisp-list-item-title//div//span[text()='Developer']")]
        public IList<IWebElement> DeveloperRole;

        [FindsBy(How = How.XPath, Using = "//crisp-card-content[@class='au-target']//ul[@ref='theList']//li[@class='au-target collection-item']//crisp-list-item//a[contains(@class,'au-target row list-item-contents clickable')]//crisp-list-item-title//div//span[text()='Technical Administrator']")]
        public IList<IWebElement> TechAdminRole;

        [FindsBy(How = How.XPath, Using = "//crisp-wizard-step[@step-title='Roles']//tbody[@class='collection']//tr[@class='au-target collection-item']//td[@class='au-target fixed-width error']//em[text()='Not selected']")]
        public IList<IWebElement> MandetoryRole;

        [FindsBy(How = How.XPath, Using = "//crisp-wizard-step[@step-title='Roles']//tbody[@class='collection']//tr[@class='au-target collection-item']//td[@class='au-target fixed-width error'][1]")]
        public IList<IWebElement> Roles;       

        [FindsBy(How = How.XPath, Using = "//crisp-card-content[@class='au-target']//ul[@ref='theList']//li[@class='au-target collection-item']//crisp-list-item//a[contains(@class,'au-target row list-item-contents clickable')]//crisp-list-item-title//div//span[text()='Land Owner']")]
        public IList<IWebElement> LandOwnerRole;

        [FindsBy(How = How.XPath, Using = "//crisp-card-content[@class='au-target']//ul[@ref='theList']//li[@class='au-target collection-item']//crisp-list-item//a[contains(@class,'au-target row list-item-contents clickable')]//crisp-list-item-title//div//span[text()='Surveyor']")]
        public IList<IWebElement> SurveyorRole;

        [FindsBy(How = How.XPath, Using = "//crisp-card-content[@class='au-target']//ul[@ref='theList']//li[@class='au-target collection-item']//crisp-list-item//a[contains(@class,'au-target row list-item-contents clickable')]//crisp-list-item-title//div//span[text()='Customer Service Account Handler']")]
        public IList<IWebElement> CSURole;

        [FindsBy(How = How.XPath, Using = "//crisp-card-content[@class='au-target']//ul[@ref='theList']//li[@class='au-target collection-item']//crisp-list-item//a[contains(@class,'au-target row list-item-contents clickable')]//crisp-list-item-title//div//span[text()='Building Control Provider']")]
        public IList<IWebElement> BuildingControlProviderRole;

        //Company List 
        [FindsBy(How = How.XPath, Using = "//crisp-list[@ref='listElm']//ul[@ref='theList']//li[@class='au-target collection-item']//crisp-list-item//a[contains(@class,'au-target row list-item-contents clickable hasAvatar')]")]
        public IList<IWebElement> CompanyList;
                                           
        [FindsBy(How = How.XPath, Using = "//crisp-list[@ref='listElm']//ul[@ref='theList']//li[@class='au-target collection-item']//crisp-list-item//a[contains(@class,'au-target row list-item-contents clickable hasAvatar')]")]
        public IList<IWebElement> EmpList;

        [FindsBy(How = How.XPath, Using = "//crisp-card-content[@class='au-target']//span/input[@type='text']")]
        public IWebElement SearchRoleLabel;

        [FindsBy(How = How.XPath, Using = "//crisp-card-content/div/div/div/crisp-input-text/div/label[text()='Search']")]
        public IWebElement SearchEmployeeLabel;

        [FindsBy(How = How.XPath, Using = "//crisp-card-content/div/div/div/crisp-input-text/div/input")]
        public IWebElement SearchEmployeeInput;

        //Declaration Page Elements 

        [FindsBy(How = How.XPath, Using = "//crisp-input-radio[@label='Applicant has confirmed that all details are correct']/div/ul/li/label[text()='Yes']")]
        public IWebElement ApplicationConfirmationRadiobutton;

        [FindsBy(How = How.XPath, Using = "//crisp-wizard-step[@step-title='Declaration']//crisp-input-radio/div/ul/li/label[text()='No']")]
        public IList<IWebElement> DeclarationPageRadioButtons;

        [FindsBy(How = How.XPath, Using = "//crisp-footer/nav/div/crisp-button[@class='au-target']//span//button[text()='Save'][@class='au-target waves-effect waves-light btn-flat']")]
        public IWebElement SaveButton;

        [FindsBy(How = How.XPath, Using = "//crisp-footer/nav/div/crisp-button[@class='au-target']/span/button[text()='Submit'][@class='au-target waves-effect waves-light btn']")]
        public IWebElement SubmitButton;

        //[FindsBy(How = How.XPath, Using = "//crisp-button[@class='au-target']/span/button[text()='Ok']")]
        //public IWebElement OkButton;

        [FindsBy(How = How.XPath, Using = "//div//crisp-header-title[@class='au-target'][1]/div[@class='title']")]
        public IWebElement SiteRefDetails;

        //Additional Buttons 

        [FindsBy(How = How.XPath, Using = "//crisp-button[@click.delegate='additionalButtonClicked(btn)'][@class='au-target']//button[text()='Add Person'][@class='au-target waves-effect waves-light btn-flat']")]
        public IWebElement AddPersonButton;

        [FindsBy(How = How.XPath, Using = "//crisp-button[@click.delegate='additionalButtonClicked(btn)'][@class='au-target']//button[text()='Add Company'][@class='au-target waves-effect waves-light btn-flat']")]
        public IWebElement AddCompanyButton;

        [FindsBy(How = How.XPath, Using = "//crisp-button[@click.delegate='additionalButtonClicked(btn)'][@class='au-target']//button[text()='Add employee'][@class='au-target waves-effect waves-light btn-flat']")]
        public IList<IWebElement> AddEmployeeButton;

        //Site Address Elements 

        [FindsBy(How = How.XPath, Using = "//div[@class='card-content']//crisp-input-text[@label='Address Line 1']//div//label[@class='au-target']")]
        public IWebElement AddressLine1Label;

        [FindsBy(How = How.XPath, Using = "//div[@class='card-content']//crisp-input-text[@label='Address Line 1']//div//input")]
        public IWebElement AddressLine1Input;

        [FindsBy(How = How.XPath, Using = "//div[@class='card-content']//crisp-input-text[@label='Town']//div//label[@class='au-target']")]
        public IWebElement TownLabel;

        [FindsBy(How = How.XPath, Using = "//div[@class='card-content']//crisp-input-text[@label='Town']//div//input")]
        public IWebElement TownInput;

        [FindsBy(How = How.XPath, Using = "//div[@class='card-content']//crisp-input-text[@label='Postcode']//div//label[@class='au-target']")]
        public IWebElement SitePostcodeLabel;

        [FindsBy(How = How.XPath, Using = "//div[@class='card-content']//crisp-input-text[@label='Postcode']//div//input")]
        public IWebElement SitePostcodeInput;

        //Local Autority email address 

        [FindsBy(How = How.XPath, Using = "//crisp-tabs//div//ul[@ref='tabHeader']/li[@class='au-target tab']//span//a[contains(text(),'Conditions')]")]
        public IWebElement ConditionsTab;

        [FindsBy(How = How.XPath, Using = "//condition-list[@class='custom-element au-target']//div[@class='au-target crisp-row table-body with-header']//table[@ref='table']//tbody//tr")]
        public IList<IWebElement> ConditionsRows;

        [FindsBy(How = How.XPath, Using = "//crisp-tabs//div//ul[@ref='tabHeader']/li[@class='au-target tab']//span//a[contains(text(),'Roles')]")]
        public IWebElement RolesTab;
       

        [FindsBy(How = How.XPath, Using = "//div[@class='card-content']//crisp-card-actions//crisp-button[@click.delegate='viewContact(selectedRole)']//span//button[@class='au-target waves-effect waves-light btn-flat']")]
        public IWebElement ViewCompanyButton;

        //Edit Role Page 
        [FindsBy(How = How.XPath, Using = "//role-editor//crisp-list//div[@ref='theTable']//table//tbody[@class='collection']//tr[@class='au-target collection-item']//td//parent::tr")]
        public IList<IWebElement> EditRoles;
        
        [FindsBy(How = How.XPath, Using = "//role-editor//crisp-list//div[@ref='theTable']//table//tbody[@class='collection']//tr[@class='au-target collection-item']//td[contains(text(),'Builder')]//parent::tr")]
        public IList<IWebElement> EditBuilderRole;

        [FindsBy(How = How.XPath, Using = "//role-editor//crisp-list//div[@ref='theTable']//table//tbody[@class='collection']//tr[@class='au-target collection-item']//td[contains(text(),'Developer')]//parent::tr")]
        public IList<IWebElement> EditDeveloperRole;



        public void UATSiteAddress()
        {
            CloseSpinneronDiv();
            WaitForElement(AddressLine1Label);
            AddressLine1Label.Click();
            if (LABCRadioButton[0].Selected == true)
            {

                WaitForElement(AddressLine1Input);
                AddressLine1Input.SendKeys("LABC Site 1");
            }
            else
            {
                WaitForElement(AddressLine1Input);
                AddressLine1Input.SendKeys("PG Site 1");
            }
            WaitForElement(TownLabel);
            TownLabel.Click();
            WaitForElement(TownInput);
            TownInput.SendKeys("Test Town");
            WaitForElement(SitePostcodeLabel);
            SitePostcodeLabel.Click();
            WaitForElement(SitePostcodeInput);
            SitePostcodeInput.SendKeys("L1");
            CloseSpinneronDiv();
            CloseSpinneronPage();
        }
        // Adding Key Site Details for LABC quote 
        public void KeySiteDetailsPage()
        {
            WaitForElement(StepTitle);
            Assert.IsTrue(StepTitle.Text.Contains("KEY SITE DETAILS"), $"Failed to Display {StepTitle.Text}");
            SelectLABCBrand();
            QuoteRecipientDetails();
            SiteAddressDetails();
            InsurerGroupDetails();
            QuoteApplicationDocumentsDetails();
            WaitForElement(NextButton);
            NextButton.Click();
            CloseSpinneronDiv();
            CloseSpinneronPage();
        }
        // Adding Key Site Details On LABC quote For Extranet
        public void KeySiteDetailsPageOnExtranet()
        {
            WaitForElement(StepTitle);
            Assert.IsTrue(StepTitle.Text.Contains("KEY SITE DETAILS"), $"Failed to Display {StepTitle.Text}");
            SelectLABCBrand();
            QuoteRecipientDetailsOnExtranet();
            SiteAddressDetails();
            InsurerGroupDetails();
            QuoteApplicationDocumentsDetails();
            WaitForElement(NextButton);
            NextButton.Click();
            CloseSpinneronDiv();
            CloseSpinneronPage();
        }
        public void KeySiteDetailsWithMigratedData()
        {
            WaitForElement(StepTitle);
            Assert.IsTrue(StepTitle.Text.Contains("KEY SITE DETAILS"), $"Failed to Display {StepTitle.Text}");
            SelectLABCBrand();
            QuoteRecipientDetailsWithMigratedCompany();
            SiteAddressDetails();
            InsurerGroupDetails();
            QuoteApplicationDocumentsDetails();
            WaitForElement(NextButton);
            NextButton.Click();
            CloseSpinneronDiv();
            CloseSpinneronPage();
        }
        public void SelectLABCBrand()
        {
            WaitForElements(LABCRadioButton);
            WaitForLoadElements(LABCRadioButton);
            if (LABCRadioButton.Count > 0)
            {
                CloseSpinneronDiv();
                CloseSpinneronPage();
                WaitForElement(LABCRadioButton[0]);
                WaitForElementToClick(LABCRadioButton[0]);
                LABCRadioButton[0].Click();
                CloseSpinneronDiv();
                CloseSpinneronPage();

            }
            CloseSpinneronDiv();
            CloseSpinneronPage();
        }

        public void QuoteRecipientDetails()
        {
            CloseSpinneronDiv();
            CloseSpinneronPage();
            if (QuoteRecipientInput.Count == 0)
            {
                WaitForElement(QuoteRecipientLabel);
                WaitForElementToClick(QuoteRecipientLabel);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                QuoteRecipientLabel.Click();
                CloseSpinneronDiv();
                CloseSpinneronPage();
                WaitForElement(SearchLabel);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                WaitForElementToClick(SearchLabel);
                SearchLabel.Click();
                WaitForElement(SearchInput);
                string quoteRecipientName = ConfigurationManager.AppSettings["QuoteRecipient"];
                for (int i = 0; i < quoteRecipientName.Length; i++)
                {
                    SearchInput.SendKeys(quoteRecipientName[i].ToString());
                    Thread.Sleep(200);
                }
                //Thread.Sleep(500);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                //Thread.Sleep(500);
                if (QuoteRecipientList.Count > 0)
                {                    
                    CloseSpinneronDiv();
                    CloseSpinneronPage();
                    Thread.Sleep(500);
                    var quoteRecipientTitle = QuoteRecipienttitle.FirstOrDefault(x => x.Text.Contains(ConfigurationManager.AppSettings["QuoteRecipient"]));
                    CloseSpinneronDiv();
                    CloseSpinneronPage();
                    Thread.Sleep(500);
                    if (quoteRecipientTitle != null)
                    {
                        WaitForElement(quoteRecipientTitle);
                        CloseSpinneronDiv();
                        quoteRecipientTitle.Click();
                        Thread.Sleep(500);
                        CloseCrispCard();
                        CloseSpinneronDiv();
                        CloseSpinneronPage();
                        //Thread.Sleep(500);
                    }
                    else if(QuoteRecipientList.Count>0)
                    {
                        WaitForElement(QuoteRecipientList[0]);
                        QuoteRecipientList[0].Click();
                        //Thread.Sleep(500);
                        CloseCrispCard();
                        CloseSpinneronDiv();
                        CloseSpinneronPage();
                        //Thread.Sleep(500);
                    }else
                    {
                        WaitForElement(AddPersonButton);
                        AddPersonButton.Click();
                        AdditionalMethodsPage.AddPerson();
                        Thread.Sleep(500);
                        CloseCrispCard();
                        CloseSpinneronPage();
                        CloseSpinneronDiv();
                    }
                }
                else
                {
                    WaitForElement(AddPersonButton);
                    AddPersonButton.Click();
                    AdditionalMethodsPage.AddPerson();
                    Thread.Sleep(500);
                    CloseCrispCard();
                    CloseSpinneronPage();
                    CloseSpinneronDiv();
                    //Thread.Sleep(500);
                }
            }
            Thread.Sleep(500);
            CloseCrispCard();
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            if (QuoteRecipientOfficeLabel.Count > 0)
            {
                CloseSpinneronDiv();
                CloseSpinneronPage();
                WaitForElement(QuoteRecipientOfficeLabel[0]);
                QuoteRecipientOfficeLabel[0].Click();
                CloseSpinneronDiv();
                CloseSpinneronPage();
                WaitForElements(QuoteRecipientOfficeInput);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                if (QuoteRecipientOfficeInput.Count > 0)
                {
                    QuoteRecipientOfficeInput[0].Click();
                    CloseSpinneronDiv();
                    CloseSpinneronPage();
                    CloseCrispCard();
                }
            }
            CloseSpinneronDiv();
            CloseSpinneronPage();
        }
        public void QuoteRecipientDetailsWithMigratedCompany()
        {
            CloseSpinneronDiv();
            CloseSpinneronPage();
            if (QuoteRecipientInput.Count == 0)
            {
                WaitForElement(QuoteRecipientLabel);
                WaitForElementToClick(QuoteRecipientLabel);
                QuoteRecipientLabel.Click();
                CloseSpinneronDiv();
                CloseSpinneronPage();
                WaitForElement(SearchLabel);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                WaitForElementToClick(SearchLabel);
                SearchLabel.Click();
                WaitForElement(SearchInput);
                string quoteRecipientName = ConfigurationManager.AppSettings["MigratedCompany"];
                for (int i = 0; i < quoteRecipientName.Length; i++)
                {
                    SearchInput.SendKeys(quoteRecipientName[i].ToString());
                    Thread.Sleep(200);
                }
                Thread.Sleep(500);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                if (QuoteRecipientList.Count > 0)
                {
                    WaitForElements(QuoteRecipientList);
                    Thread.Sleep(500);
                    CloseSpinneronDiv();
                    CloseSpinneronPage();
                    var  quoteRecipientMigratedTitle = QuoteRecipienttitle.FirstOrDefault(x => x.Text.Contains(ConfigurationManager.AppSettings["MigratedCompany"]));
                    Thread.Sleep(500);
                    CloseSpinneronDiv();
                    CloseSpinneronPage();
                    //WaitForElements(QuoteRecipienttitle);                    
                    if (quoteRecipientMigratedTitle != null)
                    {
                        WaitForElement(quoteRecipientMigratedTitle);
                        CloseSpinneronDiv();
                        quoteRecipientMigratedTitle.Click();
                        //Thread.Sleep(500);
                        CloseCrispCard();
                        CloseSpinneronDiv();
                        CloseSpinneronPage();
                        //Thread.Sleep(500);
                    }
                    else 
                    {
                        WaitForElement(QuoteRecipientList[0]);
                        WaitForElementToClick(QuoteRecipientList[0]);
                        QuoteRecipientList[0].Click();
                        Thread.Sleep(500);
                        CloseCrispCard();
                        CloseSpinneronDiv();
                        CloseSpinneronPage();
                        //Thread.Sleep(500);
                    }
                }
                else
                {
                    WaitForElement(AddPersonButton);
                    AddPersonButton.Click();
                    AdditionalMethodsPage.AddPerson();
                    //Thread.Sleep(500);
                    CloseCrispCard();
                    CloseSpinneronPage();
                    CloseSpinneronDiv();
                    //Thread.Sleep(500);
                }
            }
            Thread.Sleep(1000);
            CloseCrispCard();
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            if (QuoteRecipientOfficeLabel.Count > 0)
            {
                CloseSpinneronDiv();
                CloseSpinneronPage();
                QuoteRecipientOfficeLabel[0].Click();
                CloseSpinneronDiv();
                CloseSpinneronPage();
                WaitForElements(QuoteRecipientOfficeInput);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                if (QuoteRecipientOfficeInput.Count > 0)
                {
                    QuoteRecipientOfficeInput[0].Click();

                }
            }
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
        }

        public void QuoteRecipientDetailsOnExtranet()
        {
            CloseSpinneronDiv();
            CloseSpinneronPage();
            if (QuoteRecipientInput.Count == 0)
            {
                WaitForElement(QuoteRecipientLabel);
                WaitForElementToClick(QuoteRecipientLabel);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                QuoteRecipientLabel.Click();
                CloseSpinneronDiv();
                CloseSpinneronPage();
                WaitForElement(SearchLabel);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                WaitForElementToClick(SearchLabel);
                SearchLabel.Click();
                WaitForElement(SearchInput);
                string quoteRecipientName = ConfigurationManager.AppSettings["ExtranetCompany"];
                for (int i = 0; i < quoteRecipientName.Length; i++)
                {
                    SearchInput.SendKeys(quoteRecipientName[i].ToString());
                    Thread.Sleep(200);
                }
                //Thread.Sleep(500);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                Thread.Sleep(500);
                if (QuoteRecipientList.Count > 0)
                {
                     WaitForElements(QuoteRecipientList);
                    //Thread.Sleep(500);                  
                    CloseSpinneronDiv();
                    CloseSpinneronPage();
                    Thread.Sleep(500);
                    var quoteRecipientExtranetTitle = QuoteRecipienttitle.FirstOrDefault(x => x.Text.Contains(ConfigurationManager.AppSettings["ExtranetCompany"]));
                    CloseSpinneronDiv();
                    CloseSpinneronPage();
                    Thread.Sleep(500);
                    if (quoteRecipientExtranetTitle != null)
                    {
                        WaitForElement(quoteRecipientExtranetTitle);
                        CloseSpinneronDiv();
                        quoteRecipientExtranetTitle.Click();
                        Thread.Sleep(500);
                        CloseCrispCard();
                        CloseSpinneronDiv();
                        CloseSpinneronPage();
                        //Thread.Sleep(500);
                    }
                    else
                    {
                        WaitForElement(QuoteRecipientList[0]);
                        QuoteRecipientList[0].Click();
                        //Thread.Sleep(500);
                        CloseCrispCard();
                        CloseSpinneronDiv();
                        CloseSpinneronPage();
                        //Thread.Sleep(500);
                    }
                }
                //else
                //{
                //    WaitForElement(AddPersonButton);
                //    AddPersonButton.Click();
                //    AdditionalMethodsPage.AddPerson();
                //    Thread.Sleep(500);
                //    CloseCrispCard();
                //    CloseSpinneronPage();
                //    CloseSpinneronDiv();
                //    //Thread.Sleep(500);
                //}
            }
            Thread.Sleep(1000);
            CloseCrispCard();
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            if (QuoteRecipientOfficeLabel.Count > 0)
            {
                CloseSpinneronDiv();
                CloseSpinneronPage();
                WaitForElement(QuoteRecipientOfficeLabel[0]);
                QuoteRecipientOfficeLabel[0].Click();
                CloseSpinneronDiv();
                CloseSpinneronPage();
                WaitForElements(QuoteRecipientOfficeInput);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                if (QuoteRecipientOfficeInput.Count > 0)
                {
                    QuoteRecipientOfficeInput[0].Click();
                    CloseSpinneronDiv();
                    CloseSpinneronPage();
                    CloseCrispCard();
                }
            }
            CloseSpinneronDiv();
            CloseSpinneronPage();
        }
        public void SiteAddressDetails()
        {
            if (SiteAddressInput.Count == 0)
            {
                //Thread.Sleep(500);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                Thread.Sleep(500);
                WaitForElement(SiteAddressLabel);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                WaitForElementToClick(SiteAddressLabel);
                SiteAddressLabel.Click();
                //Thread.Sleep(500);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                //Thread.Sleep(500);
                // UATSiteAddress();
                WaitForElement(EnterAddressUsingPostcodeButton);
                EnterAddressUsingPostcodeButton.Click();
                WaitForElement(PostcodeSearchLabel);
                PostcodeSearchLabel.Click();
                WaitForElement(PostcodeInput);
                PostcodeInput.SendKeys("CH41 1AU");
                WaitForElement(AddressDropdown);
                AddressDropdown.Click();
                WaitForElement(AddressDropdownInput);
                AddressDropdownInput.Click();
                WaitForElement(SetAddressButton);
                SetAddressButton.Click();
                CloseCrispCard();
                CloseSpinneronDiv();
                CloseSpinneronPage();
            }


            if (Statics.ProductNameList.Any(x => x.Contains("High Value")))
            {
                if (InsuranceGroup.Count > 0)
                {
                    //Thread.Sleep(500);
                    CloseSpinneronPage();
                    CloseSpinneronDiv();
                    //Thread.Sleep(500);
                    WaitForElement(InsuranceGroup[0]);
                    WaitForElementToClick(InsuranceGroup[0]);
                    InsuranceGroup[0].Click();
                    CloseSpinneronDiv();
                    WaitForElements(InsuranceGroupInput);
                    var insuranceGroup = InsuranceGroupInput.FirstOrDefault(o => o.Text.Contains("Aviva"));
                    if (insuranceGroup.Displayed)
                    {
                        insuranceGroup.Click();
                    }
                    CloseSpinneronDiv();
                }
            }
            else
            {
                if (InsuranceGroup.Count > 0)
                {
                    //Thread.Sleep(500);
                    CloseSpinneronPage();
                    CloseSpinneronDiv();
                    //Thread.Sleep(500);
                    WaitForElement(InsuranceGroup[0]);
                    WaitForElementToClick(InsuranceGroup[0]);
                    InsuranceGroup[0].Click();
                    CloseSpinneronDiv();
                    WaitForElements(InsuranceGroupInput);
                    var insuranceGroup = InsuranceGroupInput.FirstOrDefault(o => o.Text.Contains("AmTrust"));
                    if (insuranceGroup.Displayed)
                    {
                        insuranceGroup.Click();
                    }
                    CloseSpinneronDiv();
                }
            }
            CloseSpinneronDiv();
            CloseSpinneronPage();
        }
        public void InsurerGroupDetails()
        {
            CloseSpinneronDiv();
            CloseSpinneronPage();
            if (Statics.ProductNameList.Any(x => x.Contains("High Value")))
            {
                if (InsuranceGroup.Count > 0)
                {
                    //Thread.Sleep(500);
                    CloseSpinneronPage();
                    CloseSpinneronDiv();
                    //Thread.Sleep(500);
                    WaitForElement(InsuranceGroup[0]);
                    WaitForElementToClick(InsuranceGroup[0]);
                    InsuranceGroup[0].Click();
                    CloseSpinneronDiv();
                    WaitForElements(InsuranceGroupInput);
                    var insuranceGroup = InsuranceGroupInput.FirstOrDefault(o => o.Text.Contains("Aviva"));
                    if (insuranceGroup.Displayed)
                    {
                        insuranceGroup.Click();
                    }
                    CloseSpinneronDiv();
                }
            }
            else
            {
                if (InsuranceGroup.Count > 0)
                {

                    //Thread.Sleep(500);
                    CloseSpinneronPage();
                    CloseSpinneronDiv();
                    //Thread.Sleep(500);
                    WaitForElement(InsuranceGroup[0]);
                    WaitForElementToClick(InsuranceGroup[0]);
                    InsuranceGroup[0].Click();
                    CloseSpinneronDiv();
                    WaitForElements(InsuranceGroupInput);
                    var insuranceGroup = InsuranceGroupInput.FirstOrDefault(o => o.Text.Contains("AmTrust"));
                    if (insuranceGroup.Displayed)
                    {
                        insuranceGroup.Click();
                    }
                    CloseSpinneronDiv();
                }
            }
        }
        public void QuoteApplicationDocumentsDetails()
        {
            CloseSpinneronDiv();
            CloseSpinneronPage();
            WaitForElement(AddQuoteAppDocs);
            WaitForElementToClick(AddQuoteAppDocs);
            AddQuoteAppDocs.Click();
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(1000);
            WaitForElement(UploadDocWizard);
            Assert.IsTrue(UploadDocWizard.Displayed);
            var fileInfo = new FileInfo(ExtensionMethods.GetAbsolutePath(@"TestData\QuotesData\CRISP install guide.docx"));
            UploadDoc.SendKeys(fileInfo.ToString());
            CloseSpinneronDiv();
            WaitForElement(UploadOkButton);
            UploadOkButton.Click();
            CloseSpinneronDiv();
            CloseSpinneronPage();
        }

        //Other Site Details 
        public void OtherSiteDetailsPage()
        {
            WaitForElement(AddLABCQuotePage.StepTitle);
            Assert.IsTrue(AddLABCQuotePage.StepTitle.Text.Contains("OTHER SITE DETAILS"), $"Failed to Display {StepTitle.Text}");
            WaitForElement(ConstructionStartDateLabel);
            ConstructionStartDateLabel.Click();
            WaitForElement(ConstructionStartDateInput);
            WaitForElementToClick(ConstructionStartDateInput);
            ConstructionStartDateInput.Click();
            CloseSpinneronDiv();
            WaitForElement(ConstructionEndDateLabel);
            ConstructionEndDateLabel.Click();
            WaitForElement(ConstructionEndDateInput);
            WaitForElementToClick(ConstructionEndDateInput);
            ConstructionEndDateInput.Click();
            CloseSpinneronDiv();
            WaitForElement(NumberOffStoreysAboveGround);
            NumberOffStoreysAboveGround.Click();
            WaitForElement(NumberOffStoreysAboveGroundInput);
            NumberOffStoreysAboveGroundInput.SendKeys("2");
            WaitForElement(NumberOffStoreysBelowGround);
            NumberOffStoreysBelowGround.Click();
            WaitForElement(NumberOffStoreysBelowGroundInput);
            NumberOffStoreysBelowGroundInput.SendKeys("1");
            WaitForElement(InnovativeMethodsRadioButton);
            InnovativeMethodsRadioButton.Click();
            WaitForElement(SiteAdminRadioButton);
            SiteAdminRadioButton.Click();
            WaitForElement(NextButton);
            NextButton.Click();
        }

        //LABC Create Quote - Importing Plot Schedule Data for all products
        public void PlotSchedulePage(string plotdata)
        {
            ExtensionMethods.ReadExcelPlotData(plotdata);
            WaitForElement(AddLABCQuotePage.StepTitle);
            Assert.IsTrue(AddLABCQuotePage.StepTitle.Text.Contains("PLOT SCHEDULE"));
            try
            {
                WaitForElement(ImportPlotScheduleButton);
                ImportPlotScheduleButton.Click();
                CloseSpinneronDiv();
                UploadDoc.SendKeys(plotdata);
                log.Info("File has been uploaded");

            }
            catch (Exception e)
            {
                log.Info(e);
                Console.WriteLine("element not found upload folder path");
            }

            CloseSpinneronDiv();
            CloseSpinneronPage();
            WaitForElement(UploadOkButton);
            WaitForElementToClick(UploadOkButton);
            UploadOkButton.Click();
            WaitForElement(WarrentyProduct);
            var productName = WarrentyProduct.Text;
            Statics.ProductName = productName;
            CloseSpinneronDiv();
            WaitForElement(NextButton);
            NextButton.Click();
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(500);
        }

        //LABC Create Quote - Product Details Page
        public void ProductsDetailsPage()
        {
            //Thread.Sleep(500);
            WaitForElement(AddLABCQuotePage.StepTitle);
            Assert.IsTrue(AddLABCQuotePage.StepTitle.Text.Contains("PRODUCTS"), $"Failed to Display {StepTitle.Text}");
            CloseSpinneronDiv();
            CloseSpinneronPage();
            if (ProductVersion.Count > 0)
            {
                WaitForElements(ProductVersion);
                for (int i = 0; i < ProductVersion.Count; i++)
                {
                    WaitForElement(ProductVersion[i]);
                    ProductVersion[i].Click();
                    CloseSpinneronDiv();
                    CloseSpinneronPage();
                    WaitForElements(ChooseProductVersionList);
                    if (ChooseProductVersionList.Count > 0)
                    {
                        WaitForElements(ChooseProductVersionList);
                        ChooseProductVersionList[0].Click();
                        CloseSpinneronDiv();
                        CloseCrispCard();
                    }
                }
            }
            CloseSpinneronDiv();
            CloseSpinneronPage();
            if (CoverLengthList.Count > 0)
            {
                WaitForElements(CoverLengthList);
                foreach (var coverlenghthinput in CoverLengthList)
                {
                    if (coverlenghthinput.Selected == false)
                    {
                        coverlenghthinput.Click();
                    }
                }
            }
            CloseSpinneronDiv();
            CloseSpinneronPage();
            if (ContractsUnderSeal.Count > 0)
            {
                foreach (var eachcontractunderseal in ContractsUnderSeal)
                {
                    if (eachcontractunderseal.Selected == false)
                    {
                        eachcontractunderseal.Click();
                    }
                    CloseSpinneronDiv();
                }
            }
            CloseSpinneronDiv();
            CloseSpinneronPage();
            if (OptionalCovers.Count > 0)
            {
                WaitForElements(OptionalCovers);
                foreach (var addcover in OptionalCovers)
                {
                    //Thread.Sleep(500);
                    CloseSpinneronDiv();
                    CloseSpinneronPage();
                    //Thread.Sleep(500);
                    WaitForElement(addcover);
                    addcover.Click();
                    Thread.Sleep(500);
                    CloseSpinneronDiv();
                    CloseSpinneronPage();
                    if (ChooseCoversList.Count > 0)
                    {
                        //Choose Core Services List
                        foreach (var eachcover in ChooseCoversList)
                        {
                            WaitForElement(eachcover);
                            eachcover.Click();
                            Thread.Sleep(500);
                            CloseSpinneronDiv();
                            CloseSpinneronPage();
                        }
                        //Thread.Sleep(500);
                        CloseSpinneronDiv();
                        CloseSpinneronPage();
                        if (SelectButton.Count > 0)
                        {
                            WaitForElements(SelectButton);
                            SelectButton[0].Click();
                            CloseCrispCard();
                        }
                        Thread.Sleep(500);
                        CloseSpinneronDiv();
                        CloseSpinneronPage();
                        Thread.Sleep(500);
                    }
                }
            }
            CloseSpinneronDiv();
            CloseSpinneronPage();
            if (Statics.ProductNameList.Any(x => x.Contains("Social Housing")) || Statics.ProductNameList.Any(x => x.Contains("Social Housing - High Value")) || Statics.ProductNameList.Any(x => x.Contains("Private Rental")) || Statics.ProductNameList.Any(x => x.Contains("Private Rental - High Value")))
            {
                if (ContractCostInput.Count > 0)
                {
                    foreach (var eachcontractcost in ContractCostInput)
                    {
                        //Thread.Sleep(500);
                        CloseSpinneronDiv();
                        CloseSpinneronPage();
                        //Thread.Sleep(500);
                        WaitForElement(ContractCostLabel[0]);
                        ContractCostLabel[0].Click();
                        eachcontractcost.Clear();
                        eachcontractcost.SendKeys("120000");
                        //Thread.Sleep(500);
                        CloseSpinneronDiv();
                        CloseSpinneronPage();
                    }
                }
            }
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            WaitForElement(NextButton);
            NextButton.Click();
            CloseSpinneronDiv();
            CloseSpinneronPage();

        }

        //Additional Quoetions Page
        public void AdditionalQuestionsPage()
        {

            WaitForElement(AddLABCQuotePage.StepTitle);
            Assert.IsTrue(AddLABCQuotePage.StepTitle.Text.Contains("ADDITIONAL QUESTIONS"), $"Failed to Display {StepTitle.Text}");
            if (NoOfUnitsAttachedRadioButton.Count > 0)
            {
                foreach (var eachunit in NoOfUnitsAttachedRadioButton)
                {
                    eachunit.Click();
                    CloseSpinneronDiv();
                }
            }

            if (WallingandGlazingRadioButton.Count > 0)
            {
                foreach (var eachunit in WallingandGlazingRadioButton)
                {
                    eachunit.Click();
                    CloseSpinneronDiv();
                }
            }

            if (AtkinsDurationLabel.Count > 0)
            {
                WaitForElement(AtkinsDurationLabel[0]);
                AtkinsDurationLabel[0].Click();
                WaitForElement(AtkinsDurationInput);
                AtkinsDurationInput.Click();
                AtkinsDurationInput.Clear();
                AtkinsDurationInput.SendKeys("12");
            }


            if (ConversionsAndRefurbishments.Count > 0)
            {
                if (DescriptionOfWorkLabel.Displayed)
                {
                    WaitForElement(DescriptionOfWorkLabel);
                    DescriptionOfWorkLabel.Click();
                    WaitForElement(DescriptionOfWorkInput);
                    DescriptionOfWorkInput.Click();
                    DescriptionOfWorkInput.Clear();
                    DescriptionOfWorkInput.SendKeys("Conversion");
                }

                if (OrginalBuildLabel.Displayed)
                {
                    WaitForElement(OrginalBuildLabel);
                    OrginalBuildLabel.Click();
                    WaitForElement(OrginalBuildInput);
                    OrginalBuildInput.Click();
                    OrginalBuildInput.Clear();
                    OrginalBuildInput.SendKeys("2016");
                }

                if (PreviousBuildLabel.Displayed)
                {
                    WaitForElement(PreviousBuildLabel);
                    PreviousBuildLabel.Click();
                    WaitForElement(PreviousBuildInput);
                    PreviousBuildInput.Click();
                }

                if (StatusOfBuildLabel.Displayed)
                {
                    WaitForElement(StatusOfBuildLabel);
                    StatusOfBuildLabel.Click();
                    WaitForElement(StatusOfBuildInput);
                    StatusOfBuildInput.Click();
                }

                if (ConversionArea.Displayed)
                {
                    WaitForElement(ConversionArea);
                    ConversionArea.Click();
                }
                if (ExistingStructureSurvey.Displayed)
                {
                    WaitForElement(ExistingStructureSurvey);
                    ExistingStructureSurvey.Click();
                }
                if (ConditionSurvey.Displayed)
                {
                    WaitForElement(ConditionSurvey);
                    ConditionSurvey.Click();
                }
                if (BuilderExperienceInConversion.Displayed)
                {
                    WaitForElement(BuilderExperienceInConversion);
                    BuilderExperienceInConversion.Click();
                }
                if (BarnConversion.Displayed)
                {
                    WaitForElement(BarnConversion);
                    BarnConversion.Click();
                }


                CloseSpinneronDiv();
            }
            //Selfbuild AdditionalQuotions
            if (SelfBuildAddtionalQuetions.Count > 0)
            {
                WaitForElement(IsArchitectOpen);
                IsArchitectOpen.Click();
                WaitForElement(HomeOwnerBuilt);
                HomeOwnerBuilt.Click();
                WaitForElement(SolePlaceOFResidence);
                SolePlaceOFResidence.Click();
                WaitForElement(StagePaymentLabel);
                StagePaymentLabel.Click();
                WaitForElement(StagePaymentInput);
                StagePaymentInput.Clear();
                StagePaymentInput.SendKeys("12");

            }
            //Completed Houing Additional Quetions
            if (CompletedHousingAdditionalQuetions.Count > 0)
            {
                WaitForElement(StructuralWarrentyReasonLabel);
                StructuralWarrentyReasonLabel.Click();
                WaitForElement(StructuralWarrentyReasonInput);
                StructuralWarrentyReasonInput.SendKeys("There was no availability");

            }
            //Commercials Conversion Addtional Questions 
            if (LossOfGrossProfit.Count > 0)
            {
                WaitForElements(LossOfGrossProfit);
                LossOfGrossProfit[0].Click();
                WaitForElement(LossOfGrossProfitInput);
                LossOfGrossProfitInput.SendKeys("20000");
            }
            if (LossOfRentReceivable.Count > 0)
            {
                WaitForElements(LossOfRentReceivable);
                LossOfRentReceivable[0].Click();
                WaitForElement(LossOfRentReceivableInput);
                LossOfRentReceivableInput.SendKeys("10000");
            }
            if (LossOfRentPayable.Count > 0)
            {
                WaitForElements(LossOfRentPayable);
                LossOfRentPayable[0].Click();
                WaitForElement(LossOfRentPayableInput);
                LossOfRentPayableInput.SendKeys("10000");
            }
            if (LossOfGrossProfitYears.Count > 0)
            {
                WaitForElements(LossOfGrossProfitYears);
                LossOfGrossProfitYears[0].Click();
                WaitForElement(LossOfGrossProfitYearsInput);
                LossOfGrossProfitYearsInput.SendKeys("5");
            }
            if (LossOfRentReceivableYears.Count > 0)
            {
                WaitForElements(LossOfRentReceivableYears);
                LossOfRentReceivableYears[0].Click();
                WaitForElement(LossOfRentReceivableYearsInput);
                LossOfRentReceivableYearsInput.SendKeys("3");
            }
            if (LossOfRentPayableYears.Count > 0)
            {
                WaitForElements(LossOfRentPayableYears);
                LossOfRentPayableYears[0].Click();
                WaitForElement(LossOfRentPayableYearsInput);
                LossOfRentPayableYearsInput.SendKeys("2");
            }
            WaitForElement(NextButton);
            NextButton.Click();
            CloseSpinneronDiv();
        }

        //Create Quote - Declaration Details Page
        public void DeclarationDetailsPage()
        {
            CloseSpinneronDiv();
            WaitForElement(AddLABCQuotePage.StepTitle);
            Assert.IsTrue(AddLABCQuotePage.StepTitle.Text.Contains("DECLARATION"), $"Failed to Display {StepTitle.Text}");
            WaitForElements(DeclarationPageRadioButtons);
            foreach (var eachradiobutton in DeclarationPageRadioButtons)
            {
                eachradiobutton.Click();
            }
            WaitForElement(ApplicationConfirmationRadiobutton);
            ApplicationConfirmationRadiobutton.Click();

        }

        //LABC Create Quote - Roles Details Page
        public void RolesDetails()
        {

            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            WaitForElement(AddLABCQuotePage.StepTitle);
            Assert.IsTrue(AddLABCQuotePage.StepTitle.Text.Contains("ROLES"), $"Failed to Display {StepTitle.Text}");
            WaitForElement(RolesTable);
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            if (MandetoryRole.Count > 0)
            {
                Thread.Sleep(500);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                WaitForElements(MandetoryRole);
                WaitForElements(Roles);
                Thread.Sleep(500);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                List<string> RoleNames = Roles.Select(i => i.Text).ToList();
                CloseSpinneronDiv();
                CloseSpinneronPage();
                for (int i = 0; i < RoleNames.Count; i++)
                {

                    if (RoleNames[i].Contains("Builder"))
                    {
                        SetBuilderRole();
                        Thread.Sleep(500);
                        CloseCrispCard();
                        CloseChooseRolesCard();
                        CloseSpinneronDiv();
                        CloseSpinneronPage();
                        Thread.Sleep(500);
                    }
                    if (RoleNames[i].Contains("Developer"))
                    {
                        SetDeveloperRole();
                        Thread.Sleep(500);
                        CloseCrispCard();
                        CloseChooseRolesCard();
                        CloseSpinneronDiv();
                        CloseSpinneronPage();
                        Thread.Sleep(500);
                    }
                    if (RoleNames[i].Contains("Sales Account Manager"))
                    {
                        SetSalesAccountManagerRole();
                        Thread.Sleep(500);
                        CloseCrispCard();
                        CloseChooseRolesCard();
                        CloseSpinneronDiv();
                        CloseSpinneronPage();
                        Thread.Sleep(1000);
                    }
                    if (RoleNames[i].Contains("Structural Referral Administrator"))
                    {
                        SetStructurralReferralAdminRole();
                        Thread.Sleep(500);
                        CloseCrispCard();
                        CloseChooseRolesCard();
                        CloseSpinneronDiv();
                        CloseSpinneronPage();
                        Thread.Sleep(500);
                    }
                    if (RoleNames[i].Contains("Design Review Administrator"))
                    {
                        SetDesignReviewAdminRole();
                        Thread.Sleep(500);
                        CloseCrispCard();
                        CloseChooseRolesCard();
                        CloseSpinneronDiv();
                        CloseSpinneronPage();
                        Thread.Sleep(500);
                    }
                    if (RoleNames[i].Contains("Technical Administrator"))
                    {
                        SetTechnicalAdministratorRole();
                        Thread.Sleep(500);
                        CloseCrispCard();
                        CloseChooseRolesCard();
                        CloseSpinneronDiv();
                        CloseSpinneronPage();
                        Thread.Sleep(500);
                    }
                    if (RoleNames[i].Contains("Building Control Provider"))
                    {
                        SetBCProviderRole();
                        Thread.Sleep(500);
                        CloseCrispCard();
                        CloseChooseRolesCard();
                        CloseSpinneronDiv();
                        CloseSpinneronPage();
                        Thread.Sleep(500);
                    }
                    if (RoleNames[i].Contains("Surveyor"))
                    {
                        SetSurveyorRole();
                        Thread.Sleep(500);
                        CloseCrispCard();
                        CloseChooseRolesCard();
                        CloseSpinneronDiv();
                        CloseSpinneronPage();
                        Thread.Sleep(500);
                    }
                    if (RoleNames[i].Contains("Design Review Surveyor"))
                    {
                        SetDesignReviewSurveyorRole();
                        Thread.Sleep(500);
                        CloseCrispCard();
                        CloseChooseRolesCard();
                        CloseSpinneronDiv();
                        CloseSpinneronPage();
                        Thread.Sleep(500);
                    }
                    if (RoleNames[i].Contains("Hub Administrator"))
                    {
                        SetHubAdministratorRole();
                        Thread.Sleep(500);
                        CloseCrispCard();
                        CloseChooseRolesCard();
                        CloseSpinneronDiv();
                        CloseSpinneronPage();
                        Thread.Sleep(500);
                    }
                }
                SetLandOwnerRole();
                Thread.Sleep(500);
                CloseCrispCard();
                CloseSpinneronDiv();
                CloseSpinneronPage();
                Thread.Sleep(500);
                if (Statics.ProductNameList.Any(o => o.Equals("Social Housing")) || Statics.ProductNameList.Any(o => o.Equals("Social Housing - High Value")))
                {
                    SetSocialHousingRole();
                    Thread.Sleep(500);
                    CloseCrispCard();
                    CloseChooseRolesCard();
                    CloseSpinneronDiv();
                    CloseSpinneronPage();
                    Thread.Sleep(500);
                }
                if (Statics.ProductNameList.Any(o => o.Contains("Self Build")))
                {
                    SetSelfBuildRole();
                    Thread.Sleep(500);
                    CloseCrispCard();
                    CloseChooseRolesCard();
                    CloseSpinneronDiv();
                    CloseSpinneronPage();
                    Thread.Sleep(500);
                }

                if (Statics.ProductNameList.Any(o => o.Contains("Private Rental")) || Statics.ProductNameList.Any(o => o.Contains("Private Rental - High Value")))
                {
                    SetPRSCompanyRole();
                    Thread.Sleep(500);
                    CloseCrispCard();
                    CloseChooseRolesCard();
                    CloseSpinneronDiv();
                    CloseSpinneronPage();
                    Thread.Sleep(500);
                }
            }
            CloseSpinneronDiv();
            CloseSpinneronPage();
            CloseCrispCard();
            Thread.Sleep(500);
            WaitForElement(NextButton);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            CloseCrispCard();
            Thread.Sleep(500);
            WaitForElementToClick(NextButton);
            NextButton.Click();
            CloseSpinneronDiv();
            CloseSpinneronPage();

        }
        //LABC Create Quote - Roles Details Page
        public void RolesDetailsWithMigratedData()
        {

            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            WaitForElement(AddLABCQuotePage.StepTitle);
            Assert.IsTrue(AddLABCQuotePage.StepTitle.Text.Contains("ROLES"), $"Failed to Display {StepTitle.Text}");
            WaitForElement(RolesTable);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            if (MandetoryRole.Count > 0)
            {
                Thread.Sleep(500);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                WaitForElements(MandetoryRole);
                WaitForElements(Roles);
                Thread.Sleep(500);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                List<string> RoleNames = Roles.Select(i => i.Text).ToList();
                CloseSpinneronDiv();
                CloseSpinneronPage();
                for (int i = 0; i < RoleNames.Count; i++)
                {
                    if (RoleNames[i].Contains("Builder"))
                    {
                        SetBuilderWithMigratedCompany();
                        Thread.Sleep(500);
                        CloseCrispCard();
                        CloseSpinneronDiv();
                        CloseSpinneronPage();
                        Thread.Sleep(500);
                    }
                    if (RoleNames[i].Contains("Developer"))
                    {
                        SetDeveloperWithMigratedCompany();
                        Thread.Sleep(500);
                        CloseCrispCard();
                        CloseSpinneronDiv();
                        CloseSpinneronPage();
                        Thread.Sleep(500);

                    }
                    if (RoleNames[i].Contains("Sales Account Manager"))
                    {
                        SetSalesAccountManagerRole();
                        Thread.Sleep(500);
                        CloseCrispCard();
                        CloseSpinneronDiv();
                        CloseSpinneronPage();
                        Thread.Sleep(500);
                    }
                    if (RoleNames[i].Contains("Structural Referral Administrator"))
                    {
                        SetStructurralReferralAdminRole();
                        Thread.Sleep(500);
                        CloseCrispCard();
                        CloseSpinneronDiv();
                        CloseSpinneronPage();
                        Thread.Sleep(500);
                    }
                    if (RoleNames[i].Contains("Design Review Administrator"))
                    {
                        SetDesignReviewAdminRole();
                        Thread.Sleep(500);
                        CloseCrispCard();
                        CloseSpinneronDiv();
                        CloseSpinneronPage();
                        Thread.Sleep(500);
                    }
                    if (RoleNames[i].Contains("Technical Administrator"))
                    {
                        SetTechnicalAdministratorRole();
                        Thread.Sleep(500);
                        CloseCrispCard();
                        CloseSpinneronDiv();
                        CloseSpinneronPage();
                        Thread.Sleep(500);
                    }

                    if (RoleNames[i].Contains("Building Control Provider"))
                    {
                        SetBCProviderRole();
                        Thread.Sleep(500);
                        CloseCrispCard();
                        CloseSpinneronDiv();
                        CloseSpinneronPage();
                        Thread.Sleep(500);
                    }
                    if (RoleNames[i].Contains("Surveyor"))
                    {
                        SetSurveyorRole();
                        Thread.Sleep(500);
                        CloseCrispCard();
                        CloseSpinneronDiv();
                        CloseSpinneronPage();
                        Thread.Sleep(500);
                    }

                    if (RoleNames[i].Contains("Design Review Surveyor"))
                    {
                        SetDesignReviewSurveyorRole();
                        Thread.Sleep(500);
                        CloseCrispCard();
                        CloseSpinneronDiv();
                        CloseSpinneronPage();
                        Thread.Sleep(500);
                    }
                    if (RoleNames[i].Contains("Hub Administrator"))
                    {
                        SetHubAdministratorRole();
                        Thread.Sleep(500);
                        CloseCrispCard();
                        CloseSpinneronDiv();
                        CloseSpinneronPage();
                        Thread.Sleep(500);
                    }

                }
                SetLandOwnerRole();
                Thread.Sleep(500);
                CloseCrispCard();
                CloseSpinneronDiv();
                CloseSpinneronPage();
                Thread.Sleep(500);
                if (Statics.ProductNameList.Any(o => o.Equals("Social Housing")) || Statics.ProductNameList.Any(o => o.Equals("Social Housing - High Value")))
                {
                    SetSocialHousingRole();
                    Thread.Sleep(500);
                    CloseCrispCard();
                    CloseSpinneronDiv();
                    CloseSpinneronPage();
                    Thread.Sleep(500);
                }
                if (Statics.ProductNameList.Any(o => o.Contains("Self Build")))
                {
                    SetSelfBuildRole();
                    Thread.Sleep(500);
                    CloseCrispCard();
                    CloseSpinneronDiv();
                    CloseSpinneronPage();
                    Thread.Sleep(500);
                }
                if (Statics.ProductNameList.Any(o => o.Contains("Private Rental")) || Statics.ProductNameList.Any(o => o.Contains("Private Rental - High Value")))
                {
                    SetPRSCompanyRole();
                    Thread.Sleep(500);
                    CloseCrispCard();
                    CloseSpinneronDiv();
                    CloseSpinneronPage();
                    Thread.Sleep(500);
                }
            }
            CloseSpinneronDiv();
            CloseSpinneronPage();
            CloseCrispCard();
            Thread.Sleep(1000);
            WaitForElement(NextButton);
            WaitForElementToClick(NextButton);
            NextButton.Click();
            CloseSpinneronDiv();
            CloseSpinneronPage();

        }

        // LABC Create Quote -   Roles Details For Social Housing Product
        public void SetSocialHousingRole()
        {
            Thread.Sleep(500);
            CloseCrispCard();
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            WaitForElement(EditRoleButton);
            WaitForElementToClick(EditRoleButton);
            EditRoleButton.Click();
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            WaitForElement(SearchRoleLabel);
            SearchRoleLabel.Click();
            SearchRoleLabel.Clear();
            SearchRoleLabel.SendKeys("Housing Association");
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            WaitForElements(HousingAssociationRole);
            if (HousingAssociationRole.Count > 0)
            {
                string housingassociation = "//crisp-card-content[@class='au-target']//ul[@ref='theList']//li[@class='au-target collection-item']//crisp-list-item//a[contains(@class,'au-target row list-item-contents clickable')]//crisp-list-item-title//div//span[text()='Housing Association']";
                var housingAssociationRole = Driver.FindElement(By.XPath(housingassociation));
                WaitForElement(housingAssociationRole);
                housingAssociationRole.Click();
                //WaitForElements(HousingAssociationRole);                
                //HousingAssociationRole[0].Click();
                Thread.Sleep(500);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                Thread.Sleep(500);
                WaitForElements(SelectButton);
                if (SelectButton.Count > 0)
                {
                    WaitForElements(SelectButton);
                    SelectButton[0].Click();
                    Thread.Sleep(500);
                    CloseSpinneronDiv();
                    CloseSpinneronPage();
                    Thread.Sleep(500);
                    if (ChooseBuilder.Count > 0)
                    {
                        WaitForElement(SearchLabel);
                        SearchLabel.Click();
                        if (Statics.RoleName != null)
                        {

                            WaitForElement(SearchInput);
                            SearchInput.Clear();
                            SearchInput.SendKeys(Statics.RoleName);
                            Thread.Sleep(500);
                            CloseSpinneronDiv();
                            CloseSpinneronPage();
                            Thread.Sleep(500);
                            if (ChooseBuilder.Count > 0)
                            {
                                WaitForElement(ChooseBuilder[0]);
                                Thread.Sleep(500);
                                CloseSpinneronDiv();
                                CloseSpinneronPage();
                                Thread.Sleep(500);
                                ChooseBuilder[0].Click();
                                CloseSpinneronDiv();
                                Thread.Sleep(500);
                            }
                            else
                            {
                                WaitForElement(AddCompanyButton);
                                WaitForElementToClick(AddCompanyButton);
                                AddCompanyButton.Click();
                                AdditionalMethodsPage.AddCompany();
                            }
                        }
                        else
                        {
                            WaitForElement(AddCompanyButton);
                            WaitForElementToClick(AddCompanyButton);
                            AddCompanyButton.Click();
                            AdditionalMethodsPage.AddCompany();
                        }
                    }
                    else
                    {
                        WaitForElement(AddCompanyButton);
                        WaitForElementToClick(AddCompanyButton);
                        AddCompanyButton.Click();
                        AdditionalMethodsPage.AddCompany();
                    }
                }
            }
            else
            {
                WaitForElement(CancelButton);
                CancelButton.Click();
                Thread.Sleep(500);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                Thread.Sleep(500);
            }
        }
        // LABC Create Quote -   Roles Details For SelfBuild Product
        public void SetSelfBuildRole()
        {
            Thread.Sleep(500);
            CloseCrispCard();
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            WaitForElement(EditRoleButton);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            WaitForElementToClick(EditRoleButton);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            EditRoleButton.Click();
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            WaitForElement(SearchRoleLabel);
            SearchRoleLabel.Click();
            SearchRoleLabel.Clear();
            SearchRoleLabel.SendKeys("Applicant (Self-build)");
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            WaitForElements(SelfBuildApplicantRole);
            if (SelfBuildApplicantRole.Count > 0)
            {
                string selfBuild = "//crisp-card-content[@class='au-target']//ul[@ref='theList']//li[@class='au-target collection-item']//crisp-list-item//a[contains(@class,'au-target row list-item-contents clickable')]//crisp-list-item-title//div//span[text()='Applicant (Self-build)']";
                var selfBuildRole = Driver.FindElement(By.XPath(selfBuild));
                selfBuildRole.Click();
                //WaitForElements(SelfBuildApplicantRole);
                //WaitForElementToClick(SelfBuildApplicantRole[0]);
                //SelfBuildApplicantRole[0].Click();
                Thread.Sleep(500);
                CloseSpinneronDiv();
                WaitForElements(SelectButton);
                if (SelectButton.Count > 0)
                {
                    CloseSpinneronDiv();
                    WaitForElements(SelectButton);
                    SelectButton[0].Click();
                    CloseSpinneronDiv();
                    WaitForElements(CompanyList);                  
                    CloseSpinneronDiv();
                    CloseSpinneronPage();
                    Thread.Sleep(500);
                    WaitForElement(AddCompanyButton);
                    WaitForElementToClick(AddCompanyButton);
                    AddCompanyButton.Click();
                    AdditionalMethodsPage.AddCompany();
                    Thread.Sleep(1000);                  
                    CloseSpinneronPage();
                    CloseSpinneronDiv();
                    Thread.Sleep(500);
                    WaitForElements(EmpList);
                    if (EmpList.Count > 0)
                    {
                        CloseSpinneronDiv();
                        WaitForElement(EmpList[0]);
                        Thread.Sleep(500);
                        CloseSpinneronDiv();
                        CloseSpinneronPage();
                        Thread.Sleep(500);
                        WaitForElementToClick(EmpList[0]);
                        EmpList[0].Click();
                        CloseSpinneronDiv();
                    }
                }
                else
                {
                    WaitForElement(CancelButton);
                    CancelButton.Click();
                    CloseSpinneronDiv();
                    CloseSpinneronPage();
                    Thread.Sleep(500);
                }
            }
            else
            {
                WaitForElement(CancelButton);
                CancelButton.Click();
                CloseSpinneronDiv();
                CloseSpinneronPage();
            }
        }

        public void SetMajorProjectsManagerRole()
        {
            Thread.Sleep(500);
            CloseCrispCard();
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            WaitForElement(EditRoleButton);
            WaitForElementToClick(EditRoleButton);
            EditRoleButton.Click();
            WaitForElement(SearchRoleLabel);
            SearchRoleLabel.Click();
            SearchRoleLabel.Clear();
            SearchRoleLabel.SendKeys("Major Projects Manager");
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            WaitForElements(MajorProjectManagerRole);
            if (MajorProjectManagerRole.Count > 0)
            {
                string majorProject = "//crisp-card-content[@class='au-target']//ul[@ref='theList']//li[@class='au-target collection-item']//crisp-list-item//a[contains(@class,'au-target row list-item-contents clickable')]//crisp-list-item-title//div//span[text()='Major Projects Manager']";
                var majorProjectRole = Driver.FindElement(By.XPath(majorProject));
                majorProjectRole.Click();
                //CloseSpinneronDiv();
                //WaitForElements(MajorProjectManagerRole);
                //MajorProjectManagerRole[0].Click();
                CloseSpinneronDiv();
                CloseSpinneronPage();
                WaitForElements(SelectButton);
                if (SelectButton.Count > 0)
                {
                    CloseSpinneronDiv();
                    WaitForElements(SelectButton);
                    SelectButton[0].Click();
                    CloseSpinneronDiv();
                    WaitForElements(CompanyList);
                    CloseSpinneronDiv();
                    if (CompanyList.Count > 0)
                    {
                        WaitForElement(CompanyList[0]);
                        WaitForElementToClick(CompanyList[0]);
                        CloseSpinneronDiv();
                        CompanyList[0].Click();
                        Thread.Sleep(500);
                        CloseSpinneronDiv();
                        CloseSpinneronPage();
                        Thread.Sleep(500);
                        WaitForElement(SearchEmployeeLabel);
                        //
                        //SearchEmployeeLabel.Click();
                        //
                        //WaitForElement(SearchEmployeeInput);
                        //SearchEmployeeInput.SendKeys("Ms Clare Hebden-Rawson");
                        //
                        //
                        CloseSpinneronDiv();
                        CloseSpinneronPage();
                        Thread.Sleep(500);
                        WaitForElements(EmpList);
                        if (EmpList.Count > 0)
                        {
                            CloseSpinneronDiv();
                            WaitForElement(EmpList[0]);
                            Thread.Sleep(500);
                            CloseSpinneronDiv();
                            CloseSpinneronPage();
                            Thread.Sleep(500);                           
                            WaitForElementToClick(EmpList[0]);
                            EmpList[0].Click();
                            CloseSpinneronDiv();
                        }
                        else
                        {
                            Thread.Sleep(500);
                            CloseSpinneronDiv();
                            CloseSpinneronPage();
                            Thread.Sleep(500);
                            WaitForElement(EmpList[0]);
                            Thread.Sleep(500);
                            CloseSpinneronDiv();
                            CloseSpinneronPage();
                            Thread.Sleep(500);                          
                            WaitForElementToClick(EmpList[0]);
                            EmpList[0].Click();
                            CloseSpinneronDiv();
                        }
                    }
                }
                else
                {
                    WaitForElement(CancelButton);
                    CancelButton.Click();
                    CloseSpinneronDiv();
                    CloseSpinneronPage();
                }
            }
            else
            {
                WaitForElement(CancelButton);
                CancelButton.Click();
                CloseSpinneronDiv();
                CloseSpinneronPage();
            }
        }

        // LABC Create Quote -   Roles Details For PrivateRental
        public void SetPRSCompanyRole()
        {
            Thread.Sleep(500);
            CloseCrispCard();
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            WaitForElement(EditRoleButton);
            WaitForElementToClick(EditRoleButton);
            EditRoleButton.Click();
            WaitForElement(SearchRoleLabel);
            SearchRoleLabel.Click();
            SearchRoleLabel.Clear();
            SearchRoleLabel.SendKeys("PRS Company");
            Thread.Sleep(500);
            CloseSpinneronDiv();
            WaitForElements(PRSCompanyRole);
            if (PRSCompanyRole.Count > 0)
            {
                Thread.Sleep(500);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                string prsCompany = "//crisp-card-content[@class='au-target']//ul[@ref='theList']//li[@class='au-target collection-item']//crisp-list-item//a[contains(@class,'au-target row list-item-contents clickable')]//crisp-list-item-title//div//span[text()='PRS Company']";
                var prsCompanyRole = Driver.FindElement(By.XPath(prsCompany));
                WaitForElement(prsCompanyRole);
                prsCompanyRole.Click();
                CloseSpinneronDiv();
                CloseSpinneronPage();
                Thread.Sleep(500);
                WaitForElements(SelectButton);
                if (SelectButton.Count > 0)
                {
                    WaitForElements(SelectButton);
                    SelectButton[0].Click();
                    Thread.Sleep(500);
                    CloseSpinneronDiv();
                    CloseSpinneronPage();
                    if (ChooseBuilder.Count > 0)
                    {
                        WaitForElement(SearchLabel);
                        SearchLabel.Click();
                        if (Statics.RoleName != null)
                        {
                            WaitForElement(SearchInput);
                            SearchInput.Clear();
                            SearchInput.SendKeys(Statics.RoleName);
                            Thread.Sleep(500);
                            CloseSpinneronDiv();
                            CloseSpinneronPage();
                            Thread.Sleep(500);
                            if (ChooseBuilder.Count > 0)
                            {
                                WaitForElement(ChooseBuilder[0]);
                                CloseSpinneronDiv();
                                ChooseBuilder[0].Click();
                                CloseSpinneronDiv();
                            }
                            else
                            {
                                WaitForElement(AddCompanyButton);
                                WaitForElementToClick(AddCompanyButton);
                                AddCompanyButton.Click();
                                AdditionalMethodsPage.AddCompany();
                            }
                        }
                        else
                        {
                            WaitForElement(AddCompanyButton);
                            WaitForElementToClick(AddCompanyButton);
                            AddCompanyButton.Click();
                            AdditionalMethodsPage.AddCompany();
                        }
                    }
                    else
                    {
                        WaitForElement(AddCompanyButton);
                        WaitForElementToClick(AddCompanyButton);
                        AddCompanyButton.Click();
                        AdditionalMethodsPage.AddCompany();
                    }
                }
            }
            else
            {
                WaitForElement(CancelButton);
                CancelButton.Click();
                CloseSpinneronDiv();
                CloseSpinneronPage();
            }
        }
        //set Builder Role 
        public void SetBuilderRole()
        {
            Thread.Sleep(500);
            CloseCrispCard();
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            WaitForElement(EditRoleButton);
            WaitForElementToClick(EditRoleButton);
            EditRoleButton.Click();
            WaitForElement(SearchRoleLabel);
            SearchRoleLabel.Click();
            SearchRoleLabel.Clear();
            SearchRoleLabel.SendKeys("Builder");
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            WaitForElements(BuilderRole);
            if (BuilderRole.Count > 0)
            {
                Thread.Sleep(500);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                string builder = "//crisp-card-content[@class='au-target']//ul[@ref='theList']//li[contains(@class,'au-target collection-item')]//crisp-list-item//a[contains(@class,'au-target row list-item-contents clickable')]//crisp-list-item-title//div//span[text()='Builder']";
                var builderRole = Driver.FindElement(By.XPath(builder));
                WaitForElement(builderRole);
                builderRole.Click();
                CloseSpinneronDiv();
                WaitForElements(SelectButton);
                if (SelectButton.Count > 0)
                {
                    WaitForElements(SelectButton);
                    SelectButton[0].Click();
                    Thread.Sleep(500);
                    CloseSpinneronDiv();
                    CloseSpinneronPage();
                    Thread.Sleep(500);
                    if (ChooseBuilder.Count > 0)
                    {
                        WaitForElement(SearchLabel);
                        SearchLabel.Click();
                        if (Statics.RoleName != null)
                        {
                            WaitForElement(SearchInput);
                            SearchInput.Clear();
                            SearchInput.SendKeys(Statics.RoleName);
                            Thread.Sleep(500);
                            CloseSpinneronDiv();
                            CloseSpinneronPage();
                            Thread.Sleep(500);
                            if (ChooseBuilder.Count > 0)
                            {
                                WaitForElement(ChooseBuilder[0]);
                                CloseSpinneronDiv();
                                ChooseBuilder[0].Click();
                                CloseSpinneronDiv();
                            }
                            else
                            {
                                CloseSpinneronDiv();
                                WaitForElement(AddCompanyButton);
                                WaitForElementToClick(AddCompanyButton);
                                AddCompanyButton.Click();
                                AdditionalMethodsPage.AddCompany();
                            }
                        }
                        else
                        {
                            CloseSpinneronDiv();
                            WaitForElement(AddCompanyButton);
                            WaitForElementToClick(AddCompanyButton);
                            AddCompanyButton.Click();
                            AdditionalMethodsPage.AddCompany();
                        }
                    }
                    else
                    {
                        CloseSpinneronDiv();
                        WaitForElement(AddCompanyButton);
                        WaitForElementToClick(AddCompanyButton);
                        AddCompanyButton.Click();
                        AdditionalMethodsPage.AddCompany();
                    }
                }
            }
            else
            {
                WaitForElement(CancelButton);
                CancelButton.Click();
                CloseSpinneronDiv();
                CloseSpinneronPage();
            }
        }
        //Set Builder Role With Migrated Company
        public void SetBuilderWithMigratedCompany()
        {
            string migratedCompany = ConfigurationManager.AppSettings["MigratedCompany"];
            Thread.Sleep(500);
            CloseCrispCard();
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            WaitForElement(EditRoleButton);
            WaitForElementToClick(EditRoleButton);
            EditRoleButton.Click();
            WaitForElement(SearchRoleLabel);
            SearchRoleLabel.Click();
            SearchRoleLabel.Clear();
            SearchRoleLabel.SendKeys("Builder");
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            WaitForElements(BuilderRole);
            if (BuilderRole.Count > 0)
            {
                Thread.Sleep(500);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                string builder = "//crisp-card-content[@class='au-target']//ul[@ref='theList']//li[contains(@class,'au-target collection-item')]//crisp-list-item//a[contains(@class,'au-target row list-item-contents clickable')]//crisp-list-item-title//div//span[text()='Builder']";
                var builderRole = Driver.FindElement(By.XPath(builder));
                WaitForElement(builderRole);
                builderRole.Click();
                CloseSpinneronDiv();
                CloseSpinneronDiv();
                WaitForElements(SelectButton);
                if (SelectButton.Count > 0)
                {
                    WaitForElements(SelectButton);
                    SelectButton[0].Click();
                    Thread.Sleep(500);
                    CloseSpinneronDiv();
                    CloseSpinneronPage();
                    Thread.Sleep(500);
                    WaitForElement(SearchLabel);
                    if (ChooseBuilder.Count > 0)
                    {
                        WaitForElement(SearchLabel);
                        SearchLabel.Click();
                        if (migratedCompany != null)
                        {
                            WaitForElement(SearchInput);
                            SearchInput.Clear();
                            Thread.Sleep(500);
                            CloseSpinneronDiv();
                            CloseSpinneronPage();
                            Thread.Sleep(500);

                            for (int i = 0; i < migratedCompany.Length; i++)
                            {
                                SearchInput.SendKeys(migratedCompany[i].ToString());
                                Thread.Sleep(200);
                            }
                            Thread.Sleep(500);
                            CloseSpinneronDiv();
                            CloseSpinneronPage();
                            Thread.Sleep(500);
                            if (ChooseBuilder.Count > 0)
                            {
                                WaitForElement(ChooseBuilder[0]);
                                CloseSpinneronDiv();
                                ChooseBuilder[0].Click();
                                CloseSpinneronDiv();
                            }
                            else
                            {
                                CloseSpinneronDiv();
                                WaitForElement(AddCompanyButton);
                                WaitForElementToClick(AddCompanyButton);
                                AddCompanyButton.Click();
                                AdditionalMethodsPage.AddCompany();
                            }
                        }
                        else
                        {
                            CloseSpinneronDiv();
                            WaitForElement(AddCompanyButton);
                            WaitForElementToClick(AddCompanyButton);
                            AddCompanyButton.Click();
                            AdditionalMethodsPage.AddCompany();
                        }
                    }
                    else
                    {
                        CloseSpinneronDiv();
                        WaitForElement(AddCompanyButton);
                        WaitForElementToClick(AddCompanyButton);
                        AddCompanyButton.Click();
                        AdditionalMethodsPage.AddCompany();
                    }
                }
            }
            else
            {
                WaitForElement(CancelButton);
                CancelButton.Click();
                CloseSpinneronDiv();
                CloseSpinneronPage();
            }
        }
        //Set Developer Role With Migrated Company
        public void SetDeveloperWithMigratedCompany()
        {
            string migratedCompany = ConfigurationManager.AppSettings["MigratedCompany"];
            Thread.Sleep(500);
            CloseCrispCard();
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            WaitForElement(EditRoleButton);
            Thread.Sleep(500);
            CloseCrispCard();
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            WaitForElementToClick(EditRoleButton);
            EditRoleButton.Click();
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            WaitForElement(SearchRoleLabel);
            SearchRoleLabel.Click();
            SearchRoleLabel.Clear();
            SearchRoleLabel.SendKeys("Developer");
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            WaitForElements(DeveloperRole);
            if (DeveloperRole.Count > 0)
            {
                Thread.Sleep(500);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                string developer = "//crisp-card-content[@class='au-target']//ul[@ref='theList']//li[@class='au-target collection-item']//crisp-list-item//a[contains(@class,'au-target row list-item-contents clickable')]//crisp-list-item-title//div//span[text()='Developer']";
                var developerRole = Driver.FindElement(By.XPath(developer));
                WaitForElement(developerRole);
                developerRole.Click();
                //WaitForElement(DeveloperRole[0]);
                //WaitForElementToClick(DeveloperRole[0]);
                //CloseSpinneronDiv();
                //DeveloperRole[0].Click();
                CloseSpinneronDiv();
                Thread.Sleep(500);
                CloseSpinneronDiv();
                WaitForElements(SelectButton);
                if (SelectButton.Count > 0)
                {
                    WaitForElements(SelectButton);
                    SelectButton[0].Click();
                    Thread.Sleep(500);
                    CloseSpinneronDiv();
                    CloseSpinneronPage();
                    Thread.Sleep(500);
                    WaitForElement(SearchLabel);
                    if (ChooseBuilder.Count > 0)
                    {
                        WaitForElement(SearchLabel);
                        SearchLabel.Click();
                        if (migratedCompany != null)
                        {
                            WaitForElement(SearchInput);
                            SearchInput.Clear();
                            Thread.Sleep(500);
                            CloseSpinneronDiv();
                            CloseSpinneronPage();
                            Thread.Sleep(500);
                            for (int i = 0; i < migratedCompany.Length; i++)
                            {
                                SearchInput.SendKeys(migratedCompany[i].ToString());
                                Thread.Sleep(200);
                            }
                            Thread.Sleep(500);
                            CloseSpinneronDiv();
                            CloseSpinneronPage();
                            Thread.Sleep(500);
                            if (ChooseBuilder.Count > 0)
                            {
                                WaitForElement(ChooseBuilder[0]);
                                CloseSpinneronDiv();
                                ChooseBuilder[0].Click();
                                CloseSpinneronDiv();
                            }
                            else
                            {
                                CloseSpinneronDiv();
                                WaitForElement(AddCompanyButton);
                                WaitForElementToClick(AddCompanyButton);
                                AddCompanyButton.Click();
                                AdditionalMethodsPage.AddCompany();
                            }
                        }
                        else
                        {
                            CloseSpinneronDiv();
                            WaitForElement(AddCompanyButton);
                            WaitForElementToClick(AddCompanyButton);
                            AddCompanyButton.Click();
                            AdditionalMethodsPage.AddCompany();
                        }
                    }
                    else
                    {
                        CloseSpinneronDiv();
                        WaitForElement(AddCompanyButton);
                        WaitForElementToClick(AddCompanyButton);
                        AddCompanyButton.Click();
                        AdditionalMethodsPage.AddCompany();
                    }
                }
            }
            else
            {
                WaitForElement(CancelButton);
                CancelButton.Click();
                CloseSpinneronDiv();
                CloseSpinneronPage();
            }
        }
        //Set Developer Role
        public void SetDeveloperRole()
        {
            Thread.Sleep(500);
            CloseCrispCard();
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            WaitForElement(EditRoleButton);
            WaitForElementToClick(EditRoleButton);
            EditRoleButton.Click();
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            WaitForElement(SearchRoleLabel);
            SearchRoleLabel.Click();
            SearchRoleLabel.Clear();
            SearchRoleLabel.SendKeys("Developer");
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            WaitForElements(DeveloperRole);
            if (DeveloperRole.Count > 0)
            {
                string developer = "//crisp-card-content[@class='au-target']//ul[@ref='theList']//li[@class='au-target collection-item']//crisp-list-item//a[contains(@class,'au-target row list-item-contents clickable')]//crisp-list-item-title//div//span[text()='Developer']";
                var developerRole = Driver.FindElement(By.XPath(developer));
                WaitForElement(developerRole);
                developerRole.Click();
                CloseSpinneronDiv();
                Thread.Sleep(500);
                CloseSpinneronDiv();
                WaitForElements(SelectButton);
                if (SelectButton.Count > 0)
                {
                    WaitForElements(SelectButton);
                    SelectButton[0].Click();
                    Thread.Sleep(500);
                    CloseSpinneronDiv();
                    CloseSpinneronPage();
                    Thread.Sleep(500);
                    if (ChooseBuilder.Count > 0)
                    {
                        WaitForElement(SearchLabel);
                        SearchLabel.Click();
                        if (Statics.RoleName != null)
                        {
                            WaitForElement(SearchInput);
                            SearchInput.Clear();
                            SearchInput.SendKeys(Statics.RoleName);
                            Thread.Sleep(500);
                            CloseSpinneronDiv();
                            CloseSpinneronPage();
                            Thread.Sleep(500);
                            if (ChooseBuilder.Count > 0)
                            {
                                WaitForElement(ChooseBuilder[0]);
                                CloseSpinneronDiv();
                                ChooseBuilder[0].Click();
                                CloseSpinneronDiv();
                            }
                            else
                            {
                                CloseSpinneronDiv();
                                WaitForElement(AddCompanyButton);
                                WaitForElementToClick(AddCompanyButton);
                                AddCompanyButton.Click();
                                AdditionalMethodsPage.AddCompany();
                            }
                        }
                        else
                        {
                            CloseSpinneronDiv();
                            WaitForElement(AddCompanyButton);
                            WaitForElementToClick(AddCompanyButton);
                            AddCompanyButton.Click();
                            AdditionalMethodsPage.AddCompany();
                        }
                    }
                    else
                    {
                        CloseSpinneronDiv();
                        WaitForElement(AddCompanyButton);
                        WaitForElementToClick(AddCompanyButton);
                        AddCompanyButton.Click();
                        AdditionalMethodsPage.AddCompany();
                    }
                }
            }
            else
            {
                WaitForElement(CancelButton);
                CancelButton.Click();
                CloseSpinneronDiv();
                CloseSpinneronPage();
            }
        }
        //Set Sales Account Manager Role 
        public void SetSalesAccountManagerRole()
        {
            Thread.Sleep(500);
            CloseCrispCard();
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            WaitForElement(EditRoleButton);
            WaitForElementToClick(EditRoleButton);
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            EditRoleButton.Click();
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            WaitForElement(SearchRoleLabel);
            SearchRoleLabel.Click();
            SearchRoleLabel.Clear();
            SearchRoleLabel.SendKeys("Sales Account Manager");
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            WaitForElements(SalesAccountMaangerRole);
            if (SalesAccountMaangerRole.Count > 0)
            {
                CloseSpinneronDiv();
                CloseSpinneronPage();
                Thread.Sleep(500);
                string salesAccountManager = "//crisp-card-content[@class='au-target']//ul[@ref='theList']//li[@class='au-target collection-item']//crisp-list-item//a[contains(@class,'au-target row list-item-contents clickable')]//crisp-list-item-title//div//span[text()='Sales Account Manager']";
                var salesAccountManagerRole = Driver.FindElement(By.XPath(salesAccountManager));
                WaitForElement(salesAccountManagerRole);
                salesAccountManagerRole.Click();
                CloseSpinneronDiv();
                Thread.Sleep(500);
                CloseSpinneronDiv();
                WaitForElements(SelectButton);
                if (SelectButton.Count > 0)
                {
                    CloseSpinneronDiv();
                    WaitForElements(SelectButton);
                    SelectButton[0].Click();
                    CloseSpinneronDiv();
                    WaitForElements(CompanyList);
                    CloseSpinneronDiv();
                    if (CompanyList.Count > 0)
                    {
                        WaitForElement(CompanyList[0]);
                        WaitForElementToClick(CompanyList[0]);
                        CloseSpinneronDiv();
                        CompanyList[0].Click();
                        Thread.Sleep(500);
                        CloseSpinneronDiv();
                        CloseSpinneronPage();
                        Thread.Sleep(500);                       
                        WaitForElements(EmpList);
                        if (EmpList.Count > 0)
                        {
                            CloseSpinneronDiv();
                            WaitForElement(EmpList[0]);
                            Thread.Sleep(500);
                            CloseSpinneronDiv();
                            CloseSpinneronPage();
                            Thread.Sleep(500);                           
                            WaitForElementToClick(EmpList[0]);
                            EmpList[0].Click();
                            CloseSpinneronDiv();
                        }
                        else
                        {
                            Thread.Sleep(500);
                            CloseSpinneronDiv();
                            CloseSpinneronPage();
                            Thread.Sleep(500);
                            WaitForElement(EmpList[0]);
                            Thread.Sleep(500);
                            CloseSpinneronDiv();
                            CloseSpinneronPage();
                            Thread.Sleep(500);                            
                            WaitForElementToClick(EmpList[0]);
                            EmpList[0].Click();
                            Thread.Sleep(500);
                            CloseSpinneronDiv();
                            CloseSpinneronPage();
                            Thread.Sleep(500);
                        }
                    }
                }
                else
                {
                    WaitForElement(CancelButton);
                    CancelButton.Click();
                    CloseSpinneronDiv();
                    CloseSpinneronPage();
                }
            }
            else

            {
                WaitForElement(CancelButton);
                CancelButton.Click();
                CloseSpinneronDiv();
                CloseSpinneronPage();
            }
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
        }
        //Set Sales Account Manager Role 
        public void SetHubAdministratorRole()
        {
            Thread.Sleep(500);
            CloseCrispCard();
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            WaitForElement(EditRoleButton);
            WaitForElementToClick(EditRoleButton);
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            EditRoleButton.Click();
            WaitForElement(SearchRoleLabel);
            SearchRoleLabel.Click();
            SearchRoleLabel.Clear();
            SearchRoleLabel.SendKeys("Hub Administrator");
            CloseSpinneronDiv();
            Thread.Sleep(500);
            WaitForElements(HubAdministratorRole);
            if (HubAdministratorRole.Count > 0)
            {
                CloseSpinneronDiv();
                CloseSpinneronPage();
                Thread.Sleep(500);
                string hubAdminRole = "//crisp-card-content[@class='au-target']//ul[@ref='theList']//li[@class='au-target collection-item']//crisp-list-item//a[contains(@class,'au-target row list-item-contents clickable')]//crisp-list-item-title//div//span[text()='Hub Administrator']";
                var hubAdministratorRole = Driver.FindElement(By.XPath(hubAdminRole));
                WaitForElement(hubAdministratorRole);
                hubAdministratorRole.Click();
                Thread.Sleep(500);
                CloseSpinneronDiv();
                WaitForElements(SelectButton);
                if (SelectButton.Count > 0)
                {
                    CloseSpinneronDiv();
                    WaitForElements(SelectButton);
                    SelectButton[0].Click();
                    CloseSpinneronDiv();
                    WaitForElements(CompanyList);
                    CloseSpinneronDiv();
                    if (CompanyList.Count > 0)
                    {
                        WaitForElement(CompanyList[0]);
                        WaitForElementToClick(CompanyList[0]);
                        CloseSpinneronDiv();
                        CompanyList[0].Click();
                        Thread.Sleep(500);
                        CloseSpinneronDiv();
                        CloseSpinneronPage();
                        Thread.Sleep(500);
                        //WaitForElement(SearchEmployeeLabel);
                        //
                        //SearchEmployeeLabel.Click();
                        //
                        //WaitForElement(SearchEmployeeInput);
                        //SearchEmployeeInput.SendKeys("Ms Clare Hebden-Rawson");
                        //
                        //
                        CloseSpinneronDiv();
                        CloseSpinneronPage();
                        Thread.Sleep(500);
                        WaitForElements(EmpList);
                        if (EmpList.Count > 0)
                        {
                            CloseSpinneronDiv();
                            WaitForElement(EmpList[0]);
                            Thread.Sleep(500);
                            CloseSpinneronDiv();
                            CloseSpinneronPage();
                            Thread.Sleep(500);                           
                            WaitForElementToClick(EmpList[0]);
                            EmpList[0].Click();
                            CloseSpinneronDiv();
                        }
                        else
                        {
                            Thread.Sleep(500);
                            CloseSpinneronDiv();
                            CloseSpinneronPage();
                            Thread.Sleep(500);
                            WaitForElement(EmpList[0]);
                            Thread.Sleep(500);
                            CloseSpinneronDiv();
                            CloseSpinneronPage();
                            Thread.Sleep(500);                         
                            WaitForElementToClick(EmpList[0]);
                            EmpList[0].Click();
                            CloseSpinneronDiv();
                        }
                    }
                }
                else
                {
                    WaitForElement(CancelButton);
                    CancelButton.Click();
                    CloseSpinneronDiv();
                    CloseSpinneronPage();
                }
            }
            else

            {
                WaitForElement(CancelButton);
                CancelButton.Click();
                CloseSpinneronDiv();
                CloseSpinneronPage();
            }
        }


        //Set Structurral Referral Administrator Role 
        public void SetStructurralReferralAdminRole()
        {

            Thread.Sleep(500);
            CloseCrispCard();
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            WaitForElement(EditRoleButton);
            WaitForElementToClick(EditRoleButton);
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            EditRoleButton.Click();
            CloseSpinneronDiv();
            CloseSpinneronPage();
            WaitForElement(SearchRoleLabel);
            SearchRoleLabel.Click();
            SearchRoleLabel.Clear();
            SearchRoleLabel.SendKeys("Structural Referral Administrator");
            CloseSpinneronDiv();
            CloseSpinneronPage();
            WaitForElements(StructuralRefAdminRole);
            if (StructuralRefAdminRole.Count > 0)
            {
                CloseSpinneronDiv();
                CloseSpinneronPage();
                Thread.Sleep(500);
                string structuralRefAdmin = "//crisp-card-content[@class='au-target']//ul[@ref='theList']//li[@class='au-target collection-item']//crisp-list-item//a[contains(@class,'au-target row list-item-contents clickable')]//crisp-list-item-title//div//span[text()='Structural Referral Administrator']";
                var structuralRefAdminRole = Driver.FindElement(By.XPath(structuralRefAdmin));
                WaitForElement(structuralRefAdminRole);
                structuralRefAdminRole.Click();
                CloseSpinneronDiv();
                CloseSpinneronPage();
                Thread.Sleep(500);
                WaitForElements(SelectButton);
                if (SelectButton.Count > 0)
                {
                    CloseSpinneronDiv();
                    CloseSpinneronPage();
                    WaitForElements(SelectButton);
                    SelectButton[0].Click();
                    CloseSpinneronDiv();
                    CloseSpinneronPage();
                    WaitForElements(CompanyList);
                    CloseSpinneronDiv();
                    CloseSpinneronPage();
                    if (CompanyList.Count > 0)
                    {
                        CloseSpinneronDiv();
                        CloseSpinneronPage();
                        WaitForElement(CompanyList[0]);
                        CloseSpinneronDiv();
                        CompanyList[0].Click();
                        Thread.Sleep(500);
                        CloseSpinneronDiv();
                        CloseSpinneronPage();
                        Thread.Sleep(500);
                        //WaitForElement(SearchEmployeeLabel);                        
                        //SearchEmployeeLabel.Click();                        
                        //CloseSpinneronDiv();
                        //CloseSpinneronPage();
                        //WaitForElement(SearchEmployeeInput);
                        //SearchEmployeeInput.SendKeys("Ms Clare Hebden-Rawson");                        
                        WaitForElements(EmpList);
                        if (EmpList.Count > 0)
                        {
                            Thread.Sleep(500);
                            CloseSpinneronDiv();
                            CloseSpinneronPage();
                            Thread.Sleep(500);
                            WaitForElement(EmpList[0]);
                            CloseSpinneronDiv();
                            CloseSpinneronPage();
                            WaitForElementToClick(EmpList[0]);
                            CloseSpinneronDiv();
                            CloseSpinneronPage();
                            EmpList[0].Click();
                            CloseSpinneronDiv();
                        }
                        else
                        {
                            Thread.Sleep(500);
                            CloseSpinneronDiv();
                            CloseSpinneronPage();
                            Thread.Sleep(500);
                            WaitForElement(EmpList[0]);
                            Thread.Sleep(500);
                            CloseSpinneronDiv();
                            CloseSpinneronPage();
                            Thread.Sleep(500);
                            WaitForElementToClick(EmpList[0]);
                            EmpList[0].Click();
                            CloseSpinneronDiv();
                        }
                    }
                }
                else

                {
                    WaitForElement(CancelButton);
                    CancelButton.Click();
                    CloseSpinneronDiv();
                    CloseSpinneronPage();
                }
            }
        }
        //Set Sales Account Manager Role 
        public void SetTechnicalAdministratorRole()
        {
            Thread.Sleep(500);
            CloseCrispCard();
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            WaitForElement(EditRoleButton);
            WaitForElementToClick(EditRoleButton);
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            EditRoleButton.Click();
            CloseSpinneronDiv();
            CloseSpinneronPage();
            WaitForElement(SearchRoleLabel);
            SearchRoleLabel.Click();
            SearchRoleLabel.Clear();
            SearchRoleLabel.SendKeys("Technical Administrator");
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            WaitForElements(TechAdminRole);
            if (TechAdminRole.Count > 0)
            {
                CloseSpinneronDiv();
                CloseSpinneronPage();
                Thread.Sleep(500);
                string techAdminRole = "//crisp-card-content[@class='au-target']//ul[@ref='theList']//li[@class='au-target collection-item']//crisp-list-item//a[contains(@class,'au-target row list-item-contents clickable')]//crisp-list-item-title//div//span[text()='Technical Administrator']";
                var techAdministratorRole = Driver.FindElement(By.XPath(techAdminRole));
                WaitForElement(techAdministratorRole);
                techAdministratorRole.Click();
                CloseSpinneronDiv();
                CloseSpinneronPage();
                WaitForElements(SelectButton);
                if (SelectButton.Count > 0)
                {
                    CloseSpinneronDiv();
                    CloseSpinneronPage();
                    WaitForElements(SelectButton);
                    SelectButton[0].Click();
                    CloseSpinneronDiv();
                    CloseSpinneronPage();
                    WaitForElements(CompanyList);
                    CloseSpinneronDiv();
                    CloseSpinneronDiv();
                    CloseSpinneronPage();
                    if (CompanyList.Count > 0)
                    {
                        CloseSpinneronDiv();
                        CloseSpinneronPage();
                        WaitForElement(CompanyList[0]);
                        CloseSpinneronDiv();
                        CompanyList[0].Click();
                        Thread.Sleep(500);
                        CloseSpinneronDiv();
                        CloseSpinneronPage();
                        Thread.Sleep(500);
                        //WaitForElement(SearchEmployeeLabel);                        
                        //SearchEmployeeLabel.Click();                        
                        //CloseSpinneronDiv();
                        //CloseSpinneronPage();                        
                        //WaitForElement(SearchEmployeeInput);
                        //SearchEmployeeInput.SendKeys("Ms Clare Hebden-Rawson");                        
                        CloseSpinneronDiv();
                        CloseSpinneronPage();
                        WaitForElements(EmpList);
                        if (EmpList.Count > 0)
                        {
                            CloseSpinneronDiv();
                            CloseSpinneronPage();
                            WaitForElement(EmpList[0]);
                            Thread.Sleep(500);
                            CloseSpinneronDiv();
                            CloseSpinneronPage();
                            Thread.Sleep(500);                         
                            WaitForElementToClick(EmpList[0]);
                            EmpList[0].Click();
                            CloseSpinneronDiv();
                        }
                        else
                        {
                            Thread.Sleep(500);
                            CloseSpinneronDiv();
                            CloseSpinneronPage();
                            Thread.Sleep(500);
                            WaitForElement(EmpList[0]);
                            WaitForElementToClick(EmpList[0]);
                            EmpList[0].Click();
                            CloseSpinneronDiv();

                        }
                    }
                }
            }
            else

            {
                WaitForElement(CancelButton);
                CancelButton.Click();
                CloseSpinneronDiv();
                CloseSpinneronPage();
            }
        }

        //set design review admin role 
        public void SetDesignReviewAdminRole()
        {
            Thread.Sleep(500);
            CloseCrispCard();
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            WaitForElement(EditRoleButton);
            WaitForElementToClick(EditRoleButton);
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            EditRoleButton.Click();
            CloseSpinneronDiv();
            CloseSpinneronPage();
            WaitForElement(SearchRoleLabel);
            SearchRoleLabel.Click();
            SearchRoleLabel.Clear();
            SearchRoleLabel.SendKeys("Design Review Administrator");
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            WaitForElements(DesignReviewAdminRole);
            if (DesignReviewAdminRole.Count > 0)
            {
                CloseSpinneronDiv();
                CloseSpinneronPage();
                Thread.Sleep(500);
                string designReviewAdmin = "//crisp-card-content[@class='au-target']//ul[@ref='theList']//li[@class='au-target collection-item']//crisp-list-item//a[contains(@class,'au-target row list-item-contents clickable')]//crisp-list-item-title//div//span[text()='Design Review Administrator']";
                var designReviewAdminRole = Driver.FindElement(By.XPath(designReviewAdmin));
                WaitForElement(designReviewAdminRole);
                designReviewAdminRole.Click();
                CloseSpinneronDiv();
                CloseSpinneronPage();
                WaitForElements(SelectButton);
                if (SelectButton.Count > 0)
                {
                    CloseSpinneronDiv();
                    CloseSpinneronPage();
                    WaitForElements(SelectButton);
                    SelectButton[0].Click();
                    CloseSpinneronDiv();
                    CloseSpinneronPage();
                    WaitForElements(CompanyList);
                    CloseSpinneronDiv();
                    CloseSpinneronPage();
                    if (CompanyList.Count > 0)
                    {
                        CloseSpinneronDiv();
                        CloseSpinneronPage();
                        WaitForElement(CompanyList[0]);
                        CloseSpinneronDiv();
                        CompanyList[0].Click();
                        Thread.Sleep(500);
                        CloseSpinneronDiv();
                        CloseSpinneronPage();
                        Thread.Sleep(500);
                        //WaitForElement(SearchEmployeeLabel);                        
                        //SearchEmployeeLabel.Click();                        
                        //CloseSpinneronDiv();
                        //CloseSpinneronPage();                        
                        //WaitForElement(SearchEmployeeInput);
                        //SearchEmployeeInput.SendKeys("Ms Clare Hebden-Rawson");                        
                        CloseSpinneronDiv();
                        CloseSpinneronPage();
                        WaitForElements(EmpList);
                        if (EmpList.Count > 0)
                        {
                            CloseSpinneronDiv();
                            CloseSpinneronPage();
                            WaitForElement(EmpList[0]);
                            Thread.Sleep(500);
                            CloseSpinneronDiv();
                            CloseSpinneronPage();
                            Thread.Sleep(500);                          
                            WaitForElementToClick(EmpList[0]);
                            EmpList[0].Click();
                            CloseSpinneronDiv();
                        }
                        else
                        {
                            Thread.Sleep(500);
                            CloseSpinneronDiv();
                            CloseSpinneronPage();
                            Thread.Sleep(500);
                            WaitForElement(EmpList[0]);
                            Thread.Sleep(500);
                            CloseSpinneronDiv();
                            CloseSpinneronPage();
                            Thread.Sleep(500);                          
                            WaitForElementToClick(EmpList[0]);
                            EmpList[0].Click();
                            CloseSpinneronDiv();
                        }
                    }
                }
                else
                {
                    WaitForElement(CancelButton);
                    CancelButton.Click();
                    CloseSpinneronDiv();
                    CloseSpinneronPage();
                }
            }
        }
        //set Land Owner Role 
        public void SetLandOwnerRole()
        {
            Thread.Sleep(500);
            CloseCrispCard();
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            WaitForElement(EditRoleButton);
            WaitForElementToClick(EditRoleButton);
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            EditRoleButton.Click();
            Thread.Sleep(500);
            CloseSpinneronDiv();
            WaitForElement(SearchRoleLabel);
            SearchRoleLabel.Click();
            SearchRoleLabel.Clear();
            SearchRoleLabel.SendKeys("Land Owner");
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            WaitForElements(LandOwnerRole);
            if (LandOwnerRole.Count > 0)
            {
                Thread.Sleep(500);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                Thread.Sleep(500);
                string landOwner = "//crisp-card-content[@class='au-target']//ul[@ref='theList']//li[@class='au-target collection-item']//crisp-list-item//a[contains(@class,'au-target row list-item-contents clickable')]//crisp-list-item-title//div//span[text()='Land Owner']";
                var landOwnerRole = Driver.FindElement(By.XPath(landOwner));
                WaitForElement(landOwnerRole);
                landOwnerRole.Click();
                Thread.Sleep(500);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                Thread.Sleep(500);
                Assert.IsTrue(SelectButton.Count > 0);
                WaitForElements(SelectButton);
                if (SelectButton.Count > 0)
                {
                    WaitForElements(SelectButton);
                    WaitForElementToClick(SelectButton[0]);
                    SelectButton[0].Click();
                    Thread.Sleep(500);
                    CloseSpinneronDiv();
                    CloseSpinneronPage();
                    Thread.Sleep(500);
                    if (ChooseBuilder.Count > 0)
                    {
                        WaitForElement(SearchLabel);
                        SearchLabel.Click();
                        if (Statics.RoleName != null)
                        {
                            WaitForElement(SearchInput);
                            SearchInput.Clear();
                            SearchInput.SendKeys(Statics.RoleName);
                            Thread.Sleep(500);
                            CloseSpinneronDiv();
                            CloseSpinneronPage();
                            Thread.Sleep(500);
                            WaitForElements(ChooseBuilder);
                            if (ChooseBuilder.Count > 0)
                            {
                                WaitForElement(ChooseBuilder[0]);
                                CloseSpinneronDiv();
                                ChooseBuilder[0].Click();
                                CloseSpinneronDiv();
                            }
                            else
                            {
                                CloseSpinneronDiv();
                                WaitForElement(AddCompanyButton);
                                WaitForElementToClick(AddCompanyButton);
                                AddCompanyButton.Click();
                                AdditionalMethodsPage.AddCompany();
                            }
                        }
                        else
                        {
                            CloseSpinneronDiv();
                            WaitForElement(AddCompanyButton);
                            WaitForElementToClick(AddCompanyButton);
                            AddCompanyButton.Click();
                            AdditionalMethodsPage.AddCompany();
                        }
                    }
                    else
                    {
                        CloseSpinneronDiv();
                        WaitForElement(AddCompanyButton);
                        WaitForElementToClick(AddCompanyButton);
                        AddCompanyButton.Click();
                        AdditionalMethodsPage.AddCompany();
                    }

                }
            }
            else
            {
                WaitForElement(CancelButton);
                CancelButton.Click();
                CloseSpinneronDiv();
                CloseSpinneronPage();
            }
        }

        //Set Building Control Provider Role 
        public void SetBCProviderRole()
        {
            Thread.Sleep(500);
            CloseCrispCard();
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            WaitForElement(EditRoleButton);
            WaitForElementToClick(EditRoleButton);
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            EditRoleButton.Click();
            CloseSpinneronDiv();
            CloseSpinneronPage();
            WaitForElement(SearchRoleLabel);
            SearchRoleLabel.Click();
            SearchRoleLabel.Clear();
            SearchRoleLabel.SendKeys("Building Control Provider");
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            WaitForElements(BuildingControlProviderRole);
            if (BuildingControlProviderRole.Count > 0)
            {
                CloseSpinneronDiv();
                CloseSpinneronPage();
                Thread.Sleep(500);
                string bcProvider = "//crisp-card-content[@class='au-target']//ul[@ref='theList']//li[@class='au-target collection-item']//crisp-list-item//a[contains(@class,'au-target row list-item-contents clickable')]//crisp-list-item-title//div//span[text()='Building Control Provider']";
                var bcProviderRole = Driver.FindElement(By.XPath(bcProvider));
                WaitForElement(bcProviderRole);
                bcProviderRole.Click();
                CloseSpinneronDiv();
                Thread.Sleep(500);
                CloseSpinneronDiv();
                WaitForElements(SelectButton);
                if (SelectButton.Count > 0)
                {
                    WaitForElements(SelectButton);
                    SelectButton[0].Click();
                    Thread.Sleep(500);
                    CloseSpinneronDiv();
                    CloseSpinneronPage();
                    Thread.Sleep(500);
                    if (ChooseBuilder.Count > 0)
                    {
                        WaitForElement(SearchLabel);
                        SearchLabel.Click();
                        if (Statics.RoleName != null)
                        {
                            WaitForElement(SearchInput);
                            SearchInput.Clear();
                            SearchInput.SendKeys(Statics.RoleName);
                            Thread.Sleep(500);
                            CloseSpinneronDiv();
                            CloseSpinneronPage();
                            Thread.Sleep(500);
                            if (ChooseBuilder.Count > 0)
                            {
                                WaitForElement(ChooseBuilder[0]);
                                CloseSpinneronDiv();
                                ChooseBuilder[0].Click();
                                CloseSpinneronDiv();
                            }
                            else
                            {
                                CloseSpinneronDiv();
                                WaitForElement(AddCompanyButton);
                                WaitForElementToClick(AddCompanyButton);
                                AddCompanyButton.Click();
                                AdditionalMethodsPage.AddCompany();
                            }
                        }
                        else
                        {
                            CloseSpinneronDiv();
                            WaitForElement(AddCompanyButton);
                            WaitForElementToClick(AddCompanyButton);
                            AddCompanyButton.Click();
                            AdditionalMethodsPage.AddCompany();
                        }
                    }
                    else
                    {
                        CloseSpinneronDiv();
                        WaitForElement(AddCompanyButton);
                        WaitForElementToClick(AddCompanyButton);
                        AddCompanyButton.Click();
                        AdditionalMethodsPage.AddCompany();
                    }
                }
            }
            else
            {
                WaitForElement(CancelButton);
                CancelButton.Click();
                CloseSpinneronDiv();
                CloseSpinneronPage();
            }
        }
        public void SetDesignReviewSurveyorRole()
        {
            Thread.Sleep(500);
            CloseCrispCard();
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            WaitForElement(EditRoleButton);
            WaitForElementToClick(EditRoleButton);
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            EditRoleButton.Click();
            WaitForElement(SearchRoleLabel);
            SearchRoleLabel.Click();
            SearchRoleLabel.Clear();
            SearchRoleLabel.SendKeys("Design Review Surveyor");
            CloseSpinneronDiv();
            Thread.Sleep(500);
            WaitForElements(DesignReviewSurveyorRole);
            if (DesignReviewSurveyorRole.Count > 0)
            {
                CloseSpinneronDiv();
                CloseSpinneronPage();
                Thread.Sleep(500);
                string designReviewSurveyor = "//crisp-card-content[@class='au-target']//ul[@ref='theList']//li[@class='au-target collection-item']//crisp-list-item//a[contains(@class,'au-target row list-item-contents clickable')]//crisp-list-item-title//div//span[text()='Design Review Surveyor']";
                var designReviewSurveyorRole = Driver.FindElement(By.XPath(designReviewSurveyor));
                WaitForElement(designReviewSurveyorRole);
                designReviewSurveyorRole.Click();

                CloseSpinneronDiv();
                CloseSpinneronPage();
                WaitForElements(SelectButton);
                if (SelectButton.Count > 0)
                {
                    CloseSpinneronDiv();
                    CloseSpinneronPage();
                    WaitForElements(SelectButton);
                    SelectButton[0].Click();
                    CloseSpinneronDiv();
                    CloseSpinneronPage();
                    WaitForElements(CompanyList);
                    CloseSpinneronDiv();
                    CloseSpinneronPage();
                    if (CompanyList.Count > 0)
                    {
                        CloseSpinneronDiv();
                        CloseSpinneronPage();
                        WaitForElement(CompanyList[0]);
                        CloseSpinneronDiv();
                        CompanyList[0].Click();
                        Thread.Sleep(500);
                        CloseSpinneronDiv();
                        CloseSpinneronPage();
                        Thread.Sleep(500);
                        //WaitForElement(SearchEmployeeLabel);                        
                        //SearchEmployeeLabel.Click();                        
                        //CloseSpinneronDiv();
                        //CloseSpinneronPage();                        
                        //WaitForElement(SearchEmployeeInput);
                        //SearchEmployeeInput.SendKeys("Ms Clare Hebden-Rawson");                        
                        CloseSpinneronDiv();
                        CloseSpinneronPage();
                        WaitForElements(EmpList);
                        if (EmpList.Count > 0)
                        {
                            CloseSpinneronDiv();
                            WaitForElement(EmpList[0]);
                            Thread.Sleep(500);
                            CloseSpinneronDiv();
                            CloseSpinneronPage();
                            Thread.Sleep(500);                       
                            WaitForElementToClick(EmpList[0]);
                            EmpList[0].Click();
                            CloseSpinneronDiv();
                        }
                        else
                        {
                            Thread.Sleep(500);
                            CloseSpinneronDiv();
                            CloseSpinneronPage();
                            Thread.Sleep(500);
                            WaitForElement(EmpList[0]);
                            Thread.Sleep(500);
                            CloseSpinneronDiv();
                            CloseSpinneronPage();
                            Thread.Sleep(500);                           
                            WaitForElementToClick(EmpList[0]);
                            EmpList[0].Click();
                            CloseSpinneronDiv();
                        }
                    }
                }
            }
            else

            {
                WaitForElement(CancelButton);
                CancelButton.Click();
                CloseSpinneronDiv();
                CloseSpinneronPage();
            }
        }


        //Set Surveyor Role
        public void SetSurveyorRole()
        {
            Thread.Sleep(500);
            CloseCrispCard();
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            WaitForElement(EditRoleButton);
            WaitForElementToClick(EditRoleButton);
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            EditRoleButton.Click();
            CloseSpinneronDiv();
            CloseSpinneronPage();
            WaitForElement(SearchRoleLabel);
            SearchRoleLabel.Click();
            SearchRoleLabel.Clear();
            SearchRoleLabel.SendKeys("Surveyor");
            CloseSpinneronDiv();
            CloseSpinneronPage();
            if (SurveyorRole.Count > 0)
            {
                CloseSpinneronDiv();
                CloseSpinneronPage();
                Thread.Sleep(500);
                string surveyor = "//crisp-card-content[@class='au-target']//ul[@ref='theList']//li[@class='au-target collection-item']//crisp-list-item//a[contains(@class,'au-target row list-item-contents clickable')]//crisp-list-item-title//div//span[text()='Surveyor']";
                var surveyorRole = Driver.FindElement(By.XPath(surveyor));
                WaitForElement(surveyorRole);
                surveyorRole.Click();
                CloseSpinneronDiv();
                CloseSpinneronPage();
                WaitForElements(SelectButton);
                if (SelectButton.Count > 0)
                {
                    CloseSpinneronDiv();
                    CloseSpinneronPage();
                    WaitForElements(SelectButton);
                    SelectButton[0].Click();
                    CloseSpinneronDiv();
                    CloseSpinneronPage();
                    WaitForElements(CompanyList);
                    CloseSpinneronDiv();
                    CloseSpinneronDiv();
                    CloseSpinneronPage();
                    if (CompanyList.Count > 0)
                    {
                        CloseSpinneronDiv();
                        CloseSpinneronPage();
                        WaitForElement(CompanyList[0]);
                        CloseSpinneronDiv();
                        CompanyList[0].Click();
                        Thread.Sleep(500);
                        CloseSpinneronDiv();
                        CloseSpinneronPage();
                        Thread.Sleep(500);
                        //WaitForElement(SearchEmployeeLabel);                        
                        //SearchEmployeeLabel.Click();                        
                        //CloseSpinneronDiv();
                        //CloseSpinneronPage();                        
                        //WaitForElement(SearchEmployeeInput);
                        //SearchEmployeeInput.SendKeys("Ms Clare Hebden-Rawson");
                        CloseSpinneronDiv();
                        CloseSpinneronPage();
                        WaitForElements(EmpList);
                        if (EmpList.Count > 0)
                        {
                            CloseSpinneronDiv();
                            WaitForElement(EmpList[0]);
                            Thread.Sleep(500);
                            CloseSpinneronDiv();
                            CloseSpinneronPage();
                            Thread.Sleep(500);                                                      
                            WaitForElementToClick(EmpList[0]);
                            EmpList[0].Click();
                            CloseSpinneronDiv();
                        }
                        else
                        {
                            Thread.Sleep(500);
                            CloseSpinneronDiv();
                            CloseSpinneronPage();
                            Thread.Sleep(500);
                            WaitForElement(EmpList[0]);
                            WaitForElementToClick(EmpList[0]);
                            EmpList[0].Click();
                            CloseSpinneronDiv();
                        }
                    }
                }
            }
            else

            {
                WaitForElement(CancelButton);
                CancelButton.Click();
                CloseSpinneronDiv();
                CloseSpinneronPage();
            }
        }

        //Set Surveyor Role
        public void SetCSURole()
        {
            Thread.Sleep(500);
            CloseCrispCard();
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            WaitForElement(EditRoleButton);
            WaitForElementToClick(EditRoleButton);
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            EditRoleButton.Click();
            CloseSpinneronDiv();
            CloseSpinneronPage();
            WaitForElement(SearchRoleLabel);
            SearchRoleLabel.Click();
            SearchRoleLabel.Clear();
            SearchRoleLabel.SendKeys("Customer Service Account Handler");
            CloseSpinneronDiv();
            CloseSpinneronPage();
            WaitForElements(CSURole);
            if (CSURole.Count > 0)
            {
                CloseSpinneronDiv();
                CloseSpinneronPage();
                Thread.Sleep(500);
                string csu = "//crisp-card-content[@class='au-target']//ul[@ref='theList']//li[@class='au-target collection-item']//crisp-list-item//a[contains(@class,'au-target row list-item-contents clickable')]//crisp-list-item-title//div//span[text()='Customer Service Account Handler']";
                var csuRole = Driver.FindElement(By.XPath(csu));
                WaitForElement(csuRole);
                csuRole.Click();
                CloseSpinneronDiv();
                CloseSpinneronPage();
                WaitForElements(SelectButton);
                if (SelectButton.Count > 0)
                {
                    CloseSpinneronDiv();
                    CloseSpinneronPage();
                    WaitForElements(SelectButton);
                    SelectButton[0].Click();
                    CloseSpinneronDiv();
                    CloseSpinneronPage();
                    WaitForElements(CompanyList);
                    CloseSpinneronDiv();
                    CloseSpinneronDiv();
                    CloseSpinneronPage();
                    if (CompanyList.Count > 0)
                    {
                        CloseSpinneronDiv();
                        CloseSpinneronPage();
                        WaitForElement(CompanyList[0]);
                        CloseSpinneronDiv();
                        CompanyList[0].Click();
                        Thread.Sleep(500);
                        CloseSpinneronDiv();
                        CloseSpinneronPage();
                        Thread.Sleep(500);
                        //WaitForElement(SearchEmployeeLabel);                        
                        //SearchEmployeeLabel.Click();                        
                        //CloseSpinneronDiv();
                        //CloseSpinneronPage();                        
                        //WaitForElement(SearchEmployeeInput);
                        //SearchEmployeeInput.SendKeys("Ms Clare Hebden-Rawson");
                        CloseSpinneronDiv();
                        CloseSpinneronPage();
                        WaitForElements(EmpList);
                        if (EmpList.Count > 0)
                        {
                            CloseSpinneronDiv();
                            WaitForElement(EmpList[0]);
                            Thread.Sleep(500);
                            CloseSpinneronDiv();
                            CloseSpinneronPage();
                            Thread.Sleep(500);                         
                            WaitForElementToClick(EmpList[0]);
                            EmpList[0].Click();
                            CloseSpinneronDiv();
                        }
                        else
                        {
                            Thread.Sleep(500);
                            CloseSpinneronDiv();
                            CloseSpinneronPage();
                            Thread.Sleep(500);
                            WaitForElement(EmpList[0]);
                            Thread.Sleep(500);
                            CloseSpinneronDiv();
                            CloseSpinneronPage();
                            Thread.Sleep(500);                            
                            WaitForElementToClick(EmpList[0]);
                            EmpList[0].Click();
                            CloseSpinneronDiv();
                        }
                    }
                }
            }
            else

            {
                WaitForElement(CancelButton);
                CancelButton.Click();
                CloseSpinneronDiv();
                CloseSpinneronPage();
            }
        }
        

        public void SelectBuilderRegistration()
        {
            WaitForElement(RolesTab);
            RolesTab.Click();
            Thread.Sleep(500);
            CloseSpinneronPage();
            CloseSpinneronDiv();
            Thread.Sleep(500);
            WaitForElements(EditRoles);
            WaitForElementToClick(EditRoles[0]);
            Thread.Sleep(500);
            CloseSpinneronPage();
            CloseSpinneronDiv();
            Thread.Sleep(500);
            if (EditBuilderRole.Count > 0)
            {
                WaitForElementToClick(EditBuilderRole[0]);
                EditBuilderRole[0].Click();
                Thread.Sleep(500);
                CloseSpinneronPage();
                CloseSpinneronDiv();
                Thread.Sleep(500);
                WaitForElement(ViewCompanyButton);
                ViewCompanyButton.Click();
                Thread.Sleep(500);
                CloseSpinneronPage();
                CloseSpinneronDiv();
                Thread.Sleep(500);
            }
        }
        public void SelectDeveloperRegistration()
        {
            Thread.Sleep(1000);
            if (ConditionsRows.Count <= 0)
            {
                Dashboardpage.SelectOrder();
            }
            Thread.Sleep(500);
            CloseSpinneronPage();
            CloseSpinneronDiv();
            Thread.Sleep(500);
            WaitForElement(RolesTab);
            RolesTab.Click();
            Thread.Sleep(500);
            CloseSpinneronPage();
            CloseSpinneronDiv();
            Thread.Sleep(500);
            WaitForElements(EditRoles);
            WaitForElementToClick(EditRoles[0]);
            Thread.Sleep(500);
            CloseSpinneronPage();
            CloseSpinneronDiv();
            Thread.Sleep(500);
            if (EditDeveloperRole.Count > 0)
            {
                WaitForElementToClick(EditDeveloperRole[0]);
                EditDeveloperRole[0].Click();
                Thread.Sleep(500);
                CloseSpinneronPage();
                CloseSpinneronDiv();
                Thread.Sleep(500);
                WaitForElement(ViewCompanyButton);
                ViewCompanyButton.Click();
                Thread.Sleep(500);
                CloseSpinneronPage();
                CloseSpinneronDiv();
                Thread.Sleep(500);
            }
        }
        public void SubmitLABCQuote(string masterPlotData)
        {
            ExtensionMethods.ReadExcelPlotData(masterPlotData);
            CloseSpinneronPage();
            CloseSpinneronDiv();
            Thread.Sleep(2000);
            WaitForElement(Dashboardpage.PlusButton);
            Dashboardpage.PlusButton.Click();
            WaitForElement(Dashboardpage.QuoteOption);
            Dashboardpage.QuoteOption.Click();
            WaitForElement(AddNewQuoteDiv);
            CloseSpinneronPage();
            CloseSpinneronDiv();
            KeySiteDetailsPage();
            OtherSiteDetailsPage();
            PlotSchedulePage(masterPlotData);
            ProductsDetailsPage();
            AdditionalQuestionsPage();
            RolesDetails();
            DeclarationDetailsPage();
            WaitForElement(SaveButton);
            SaveButton.Click();
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(2000);
            WaitForElement(SubmitButton);
            SubmitButton.Click();
            Thread.Sleep(2000);
            CloseSpinneronPage();
            CloseSpinneronDiv();
            Thread.Sleep(2000);
            WaitForElement(Dashboardpage.QuoteApplicationStatusSubmitted);
            Assert.IsTrue(Dashboardpage.QuoteApplicationStatusSubmitted.Displayed);
        }
        public void SubmitLABCQuoteForExtranet(string masterPlotData)
        {
            ExtensionMethods.ReadExcelPlotData(masterPlotData);
            CloseSpinneronPage();
            CloseSpinneronDiv();
            Thread.Sleep(2000);
            WaitForElement(Dashboardpage.PlusButton);
            Dashboardpage.PlusButton.Click();
            WaitForElement(Dashboardpage.QuoteOption);
            Dashboardpage.QuoteOption.Click();
            WaitForElement(AddNewQuoteDiv);
            CloseSpinneronPage();
            CloseSpinneronDiv();
            KeySiteDetailsPageOnExtranet();
            OtherSiteDetailsPage();
            PlotSchedulePage(masterPlotData);
            ProductsDetailsPage();
            AdditionalQuestionsPage();
            RolesDetails();
            DeclarationDetailsPage();
            WaitForElement(SaveButton);
            SaveButton.Click();
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(2000);
            WaitForElement(SubmitButton);
            SubmitButton.Click();
            Thread.Sleep(2000);
            CloseSpinneronPage();
            CloseSpinneronDiv();
            Thread.Sleep(2000);
            WaitForElement(Dashboardpage.QuoteApplicationStatusSubmitted);
            Assert.IsTrue(Dashboardpage.QuoteApplicationStatusSubmitted.Displayed);
        }

        //
        // Adding Key Site Details for LABC quote 
        public void KeySiteDetailsSelectSompoCanopiusInsurer()
        {
            WaitForElement(StepTitle);
            Assert.IsTrue(StepTitle.Text.Contains("KEY SITE DETAILS"), $"Failed to Display {StepTitle.Text}");
            SelectLABCBrand();
            QuoteRecipientDetails();
            SiteAddressDetails();
            CloseSpinneronDiv();
            if (Statics.ProductNameList.Any(x => x.Contains("High Value")))
            {
                if (InsuranceGroup.Count > 0)
                {
                    AddPGQuotePage.SelectSompoCanopiusInsurer();
                }
            }
            else
            {
                if (InsuranceGroup.Count > 0)
                {
                    Thread.Sleep(500);
                    CloseSpinneronPage();
                    CloseSpinneronDiv();
                    Thread.Sleep(500);
                    WaitForElement(InsuranceGroup[0]);
                    WaitForElementToClick(InsuranceGroup[0]);
                    InsuranceGroup[0].Click();
                    CloseSpinneronDiv();
                    WaitForElements(InsuranceGroupInput);
                    var insuranceGroup = InsuranceGroupInput.FirstOrDefault(o => o.Text.Contains("AmTrust"));
                    if (insuranceGroup.Displayed)
                    {
                        insuranceGroup.Click();
                    }
                    CloseSpinneronDiv();
                }
            }
            CloseSpinneronDiv();
            CloseSpinneronPage();
            QuoteApplicationDocumentsDetails();
            WaitForElement(NextButton);
            NextButton.Click();
            CloseSpinneronDiv();
            CloseSpinneronPage();
        }
        public void KeySiteDetailsSelectAXAHVSsInsurer()
        {
            WaitForElement(StepTitle);
            Assert.IsTrue(StepTitle.Text.Contains("KEY SITE DETAILS"), $"Failed to Display {StepTitle.Text}");
            SelectLABCBrand();
            QuoteRecipientDetails();
            SiteAddressDetails();
            CloseSpinneronDiv();
            if (Statics.ProductNameList.Any(x => x.Contains("High Value")))
            {
                if (InsuranceGroup.Count > 0)
                {
                    AddPGQuotePage.SelectAXAHVSInsurer();
                }
            }
            else
            {
                if (InsuranceGroup.Count > 0)
                {
                    Thread.Sleep(500);
                    CloseSpinneronPage();
                    CloseSpinneronDiv();
                    Thread.Sleep(500);
                    WaitForElement(InsuranceGroup[0]);
                    WaitForElementToClick(InsuranceGroup[0]);
                    InsuranceGroup[0].Click();
                    CloseSpinneronDiv();
                    WaitForElements(InsuranceGroupInput);
                    var insuranceGroup = InsuranceGroupInput.FirstOrDefault(o => o.Text.Contains("AmTrust"));
                    if (insuranceGroup.Displayed)
                    {
                        insuranceGroup.Click();
                    }
                    CloseSpinneronDiv();
                }
            }
            CloseSpinneronDiv();
            CloseSpinneronPage();
            QuoteApplicationDocumentsDetails();
            WaitForElement(NextButton);
            NextButton.Click();
            CloseSpinneronDiv();
            CloseSpinneronPage();
        }

    }
}

