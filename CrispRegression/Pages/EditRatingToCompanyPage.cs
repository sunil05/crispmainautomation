﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.PageObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace CrispAutomation.Pages
{
    public class EditRatingToCompanyPage : Support.Pages
    {
        public IWebDriver wdriver;

        public EditRatingToCompanyPage(ISearchContext driver)
        {
            PageFactory.InitElements(driver, this);
            wdriver = (RemoteWebDriver)driver;
        }


        [FindsBy(How = How.XPath, Using = "//crisp-tabs/div/ul/li[@class='au-target tab']/span/a[contains(text(),'Rating')]")]
        public IWebElement RatingOption;

        [FindsBy(How = How.XPath, Using = "//crisp-header-button//span//button[text()='Edit Rating']")]
        public IWebElement EditRating;

        [FindsBy(How = How.XPath, Using = "//crisp-footer[@class='au-target']//crisp-button[@class='au-target'][@click.delegate='wizard.goForward()']//button[text()='Next']")]
        public IWebElement NextButton;

        [FindsBy(How = How.XPath, Using = "//crisp-header-button[2]/span/button[text()='Add Rating']")]
        public IWebElement AddRatingButton;

        [FindsBy(How = How.XPath, Using = "//crisp-dialog[@class='au-target']//div//crisp-header[@class='au-target']")]
        public IWebElement RatingDialogue;

        //Details Page Elements 

        [FindsBy(How = How.XPath, Using = "//crisp-stepper-step[1]//div[@class='highlight-colour title au-target']")]
        public IWebElement DetailsOption;

        [FindsBy(How = How.CssSelector, Using = "crisp-picker[label='Principal Occupation'] input[type='text']")]
        public IWebElement OccupationDrodpDown;

        [FindsBy(How = How.XPath, Using = "//ul[@class='dropdown-content select-dropdown active']//li//span[text()='Building Contractor']")]
        public IWebElement OccupationDrodpDownInput;

        [FindsBy(How = How.CssSelector, Using = "crisp-picker[label='Company Segment'] input[type='text']")]
        public IWebElement CompanySegmentDrodpDown;

        [FindsBy(How = How.XPath, Using = "//ul[@class='dropdown-content select-dropdown active']//li//span[text()='Medium']")]
        public IWebElement CompanySegmentDrodpDownInput;

        //Warranty Providers Page Elements 

        [FindsBy(How = How.XPath, Using = "//crisp-stepper-step[2]//div[@class='highlight-colour title au-target']")]
        public IWebElement WarrentyProvider;

        [FindsBy(How = How.CssSelector, Using = "crisp-card-content > div > crisp-input-bool:nth-child(1) > label")]
        public IWebElement NHBCCheckbox;

        [FindsBy(How = How.CssSelector, Using = "crisp-input-year > div > label")]
        public IWebElement NHBCRatingYear;

        [FindsBy(How = How.CssSelector, Using = "crisp-input-year > div > input")]
        public IWebElement NHBCRatingYearInput;

        [FindsBy(How = How.CssSelector, Using = "crisp-picker[label='Rate'] [value='A1']")]
        public IWebElement NHBCRateDropDown;

        [FindsBy(How = How.XPath, Using = "//ul[@class='dropdown-content select-dropdown active']//li//span[text()='A2']")]
        public IWebElement NHBCRateDropDownInput;

        //Director details 
        [FindsBy(How = How.XPath, Using = "//crisp-stepper-step[3]//div[@class='highlight-colour title au-target']")]
        public IWebElement DirectorsOption;

        [FindsBy(How = How.XPath, Using = "//crisp-wizard-step[@step-title='Directors']//crisp-card[@class='au-target']//crisp-list[@source.bind='companyDirectors']//ul//li[contains(@class,'au-target collection-item')]")]
        public IList<IWebElement> DirectorsList;

        [FindsBy(How = How.XPath, Using = "//div[@class='modal-content']//crisp-input-year[@value.two-way='yearsOfExperience & validate']//div//label")]
        public IWebElement YearsOfExpLabel;

        [FindsBy(How = How.XPath, Using = "//div[@class='modal-content']//crisp-input-year[@value.two-way='yearsOfExperience & validate']//div//input")]
        public IWebElement YearsOfExpInput;

        [FindsBy(How = How.XPath, Using = "//div[@class='modal-content']//crisp-input-text[@value.two-way='comment & validate']//div//label")]
        public IWebElement CommentsOfDirectorLabel;

        [FindsBy(How = How.XPath, Using = "//div[@class='modal-content']//crisp-input-text[@value.two-way='comment & validate']//div//input")]
        public IWebElement CommentsOfDirectorInput;

        //Developements Page Elements 

        [FindsBy(How = How.XPath, Using = "//crisp-stepper-step[3]//div[@class='highlight-colour title au-target']")]
        public IWebElement Developments;

        [FindsBy(How = How.XPath, Using = "//crisp-dialog[@class='au-target']//crisp-header[@text='Previous Development']")]
        public IWebElement PreviousDevDiv;

        [FindsBy(How = How.XPath, Using = "//crisp-wizard-step[@step-title='Developments'][@class='au-target active']//crisp-action-button[@click.delegate='addPreviousDevelopment()'][@class='au-target']//div//a//i")]
        public IWebElement PreDevsPlusButton;

        [FindsBy(How = How.CssSelector, Using = "crisp-input-text[label='Address'] > div > label")]
        public IWebElement PreDevsAddress;

        [FindsBy(How = How.XPath, Using = "//crisp-wizard-step[@step-title='Developments'][@class='au-target active']//crisp-list[@list-title='Previous Developments']//ul//li[@class='au-target collection-item']")]
        public IList<IWebElement> PreDevList;

        [FindsBy(How = How.CssSelector, Using = "crisp-input-text[label='Address'] > div > input")]
        public IWebElement PreDevsAddressInput;

        [FindsBy(How = How.CssSelector, Using = "crisp-input-currency[label='Reconstruction Cost'] > div > label")]
        public IWebElement ReconstructionCost;

        [FindsBy(How = How.CssSelector, Using = "crisp-input-currency[label='Reconstruction Cost'] > div > input")]
        public IWebElement ReconstructionCostInput;

        [FindsBy(How = How.CssSelector, Using = "crisp-input-number[label='Units'] > div > label")]
        public IWebElement Unitslabel;

        [FindsBy(How = How.CssSelector, Using = "crisp-input-number[label='Units'] > div > input")]
        public IWebElement UnitsInput;

        [FindsBy(How = How.CssSelector, Using = "crisp-picker[label='Warranty Provider']")]
        public IWebElement WarrentyProviderDropdown;

        [FindsBy(How = How.XPath, Using = "//ul[@class='dropdown-content select-dropdown active']/li/span[text()='LABC Warranty']")]
        public IWebElement WarrentyProviderDropdownInput;

        [FindsBy(How = How.CssSelector, Using = "crisp-picker[label='Construction Type']")]
        public IWebElement ConstructionTypeDropdown;

        [FindsBy(How = How.XPath, Using = "//ul[@class='dropdown-content select-dropdown active']/li/span[text()='Conversion']")]
        public IWebElement ConstructionTypeDropdownInput;

        [FindsBy(How = How.XPath, Using = "//ul[@class='dropdown-content select-dropdown active']/li/span[text()='Conversion']")]
        public IWebElement ConstructionTypeDropdownInputConversion;

        [FindsBy(How = How.XPath, Using = "//crisp-footer-button[@class='au-target'][@click.delegate='ok()']//button[text()='Ok']")]
        public IWebElement OkButton;

        [FindsBy(How = How.XPath, Using = "//crisp-wizard-step[@step-title='Developments']//crisp-list//ul//li//crisp-list-item//crisp-button[@click.delegate='removePreviousDevelopment(item)']//button")]
        public IList<IWebElement> RemoveDevButton;


        //Prospective Business Details Page Elements 

        [FindsBy(How = How.XPath, Using = "//crisp-wizard-step[@step-title='Prospective Business'][@class='au-target active']//crisp-action-button[@click.delegate='addUnitsConstructedByYear()'][@class='au-target']//div//a//i")]
        public IWebElement AddProspectiveBusinessButton;

        [FindsBy(How = How.XPath, Using = "//crisp-wizard-step[@step-title='Prospective Business'][@class='au-target active']//crisp-list//ul//li[@class='au-target collection-item']")]
        public IList<IWebElement> ProspectiveBusinessList;

        [FindsBy(How = How.XPath, Using = "//crisp-dialog[@class='au-target']//crisp-header[@text='Prospective Business']")]
        public IWebElement ProspectiveBusinessDiv;

        [FindsBy(How = How.XPath, Using = "//crisp-stepper-step[4]//div[@class='highlight-colour title au-target']")]
        public IWebElement ProspectiveBusiness;

        [FindsBy(How = How.CssSelector, Using = "crisp-input-currency > div > label")]
        public IWebElement LandBankHeld;

        [FindsBy(How = How.CssSelector, Using = "crisp-input-currency > div > input")]
        public IWebElement LandBankHeldInput;

        [FindsBy(How = How.XPath, Using = "//div[@class='modal-content']//crisp-input-year[@label='Year'][@value.bind='year & validate']//div[@class='input-field']//label[contains(@class,'au-target')]")]
        public IWebElement ConstructedHistoricYear;

        [FindsBy(How = How.XPath, Using = "//div[@class='modal-content']//crisp-input-year[@label='Year'][@value.bind='year & validate']//input")]
        public IWebElement ConstructedHistoricYearInput;

        [FindsBy(How = How.XPath, Using = "//crisp-input-number[@label='Units Constructed']//label[text()='Units Constructed']")]
        public IWebElement ConstructedHistoricUnits;

        [FindsBy(How = How.XPath, Using = "//crisp-input-number[@label='Units Constructed']//input")]
        public IWebElement ConstructedHistoricUnitsInput;

        [FindsBy(How = How.XPath, Using = "//crisp-wizard-step[@step-title='Prospective Business']//crisp-list//ul//li//crisp-list-item//crisp-button[@click.delegate='removeUnitsConstructedByYear(item)']//button")]
        public IWebElement RemoveProspectiveBusinessButton;

        //Claims History Page Elements 

        [FindsBy(How = How.XPath, Using = "//crisp-stepper-step[5]//div[@class='highlight-colour title au-target']")]
        public IWebElement ClaimHistory;

        [FindsBy(How = How.XPath, Using = "//crisp-wizard-step[@step-title='Claims History'][@class='au-target active']//crisp-list//ul//li[@class='au-target collection-item']")]
        public IList<IWebElement> ClaimHistoryList;

        [FindsBy(How = How.XPath, Using = "//crisp-dialog[@class='au-target']//div//crisp-header[@text='Historic Claim']")]
        public IWebElement ClaimHistoryDiv;

        [FindsBy(How = How.XPath, Using = "//crisp-wizard-step[@step-title='Claims History'][@class='au-target active']//crisp-input-bool//label")]
        public IWebElement AssociatedClaims;

        [FindsBy(How = How.XPath, Using = "//crisp-wizard-step[@step-title='Claims History'][@class='au-target active']//crisp-action-button[@click.delegate='addClaimsHistory()']//div//a/i")]
        public IWebElement AddHistoricClaimsButton;

        [FindsBy(How = How.CssSelector, Using = "div.modal-content > crisp-input-date.au-target > div > label")]
        public IWebElement ClaimsDate;

        [FindsBy(How = How.XPath, Using = "//div[@class='picker picker--focused picker--opened']/div/div/div/div/div/button[@class='btn-flat picker__today']")]
        public IWebElement ClaimsDateInput;

        [FindsBy(How = How.XPath, Using = "//crisp-dialog[@class='au-target']//crisp-input-currency[@label='Value']//label")]
        public IWebElement ClaimsValue;

        [FindsBy(How = How.XPath, Using = "//crisp-dialog[@class='au-target']//crisp-input-currency[@label='Value']//input")]
        public IWebElement ClaimsValueInput;

        [FindsBy(How = How.XPath, Using = "//crisp-dialog[@class='au-target']//crisp-input-textarea[@label='Comments']//label")]
        public IWebElement ClaimsComments;

        [FindsBy(How = How.XPath, Using = "//crisp-dialog[@class='au-target']//crisp-input-textarea[@label='Comments']//textarea")]
        public IWebElement ClaimsCommentsInput;

        [FindsBy(How = How.XPath, Using = "//crisp-footer[@class='au-target']//crisp-button[@click.delegate='save()'][@class='au-target']//button[text()='Save']")]
        public IWebElement ConfirmRatingButton;

        [FindsBy(How = How.XPath, Using = "//crisp-card-header/div/span[text()='Summary']")]
        public IWebElement RatingSummaryDisplayed;

        [FindsBy(How = How.CssSelector, Using = "body > div.custom-element > router-view > crisp-header > nav > div > crisp-header-avatar > img")]
        public IWebElement Imageperson;

        [FindsBy(How = How.CssSelector, Using = "body > ai-dialog-container > div > div > crisp-dialog > div")]
        public IWebElement RatingPersonDialogueWindow;

        [FindsBy(How = How.XPath, Using = "//crisp-wizard-step[@step-title='Claims History']//crisp-list//ul//li//crisp-list-item//crisp-button[@click.delegate='removeClaimsHistory(item)']//button")]
        public IWebElement RemoveClaimsHistoryButton;

        //Scores Page Elements 

        [FindsBy(How = How.XPath, Using = "//crisp-stepper-step[5]//div[@class='highlight-colour title au-target']")]
        public IWebElement ScoresOption;

        [FindsBy(How = How.XPath, Using = "//crisp-picker[@label='Financial Score']//div//input[@class='select-dropdown']")]
        public IWebElement FinancialScoresDropdown;

        [FindsBy(How = How.XPath, Using = "//ul[@class='dropdown-content select-dropdown active']/li/span[text()='2']")]
        public IWebElement FinancialScoresDropdownInput;

        [FindsBy(How = How.XPath, Using = "//crisp-picker[@label='Land Bank Score']//div//input[@class='select-dropdown']")]
        public IWebElement LandBankScoresDropdown;

        [FindsBy(How = How.XPath, Using = "//ul[@class='dropdown-content select-dropdown active']/li/span[text()='2']")]
        public IWebElement LandBankScoresDropdownInput;

        [FindsBy(How = How.XPath, Using = "//crisp-picker[@label='Units Score']//div//input[@class='select-dropdown']")]
        public IWebElement UnitsScoresDropdown;

        [FindsBy(How = How.XPath, Using = "//ul[@class='dropdown-content select-dropdown active']/li/span[text()='2']")]
        public IWebElement UnitsScoresDropdownInput;

        [FindsBy(How = How.XPath, Using = "//crisp-picker[@label='Years Trading Score']//div//input[@class='select-dropdown']")]
        public IWebElement TradingScoresDropdown;

        [FindsBy(How = How.XPath, Using = "//ul[@class='dropdown-content select-dropdown active']/li/span[text()='2']")]
        public IWebElement TradingScoresDropdownInput;

        [FindsBy(How = How.XPath, Using = "//crisp-picker[@label='Company Structure Score']//div//input[@class='select-dropdown']")]
        public IWebElement CompanyStructureScoresDropdown;

        [FindsBy(How = How.XPath, Using = "//ul[@class='dropdown-content select-dropdown active']/li/span[text()='2']")]
        public IWebElement CompanyStructureScoresDropdownInput;

        [FindsBy(How = How.XPath, Using = "//crisp-display-value[@value.bind='totalScore']//span[@class='au-target value']")]
        public IWebElement TotalScrores;

        [FindsBy(How = How.CssSelector, Using = "crisp-picker[label='Actual Technical Rating'] >div>div>input[type=text].select-dropdown")]
        public IWebElement TechgRatingDropdown;

        [FindsBy(How = How.XPath, Using = "//ul[@class='dropdown-content select-dropdown active']/li/span[text()='2']")]
        public IWebElement TechgRatingDropdownInput;

        [FindsBy(How = How.CssSelector, Using = "crisp-input-textarea[label='Technical Rating Comment']>div>label")]
        public IWebElement TechRatingComment;

        [FindsBy(How = How.CssSelector, Using = "crisp-input-textarea[label='Technical Rating Comment']>div>textarea")]
        public IWebElement TechRatingCommentText;

        //Group Details 

        [FindsBy(How = How.XPath, Using = "//crisp-stepper-step[7]//div[@class='highlight-colour title au-target']")]
        public IWebElement GroupOption;

        [FindsBy(How = How.XPath, Using = "//crisp-dialog/div/div/div/div/crisp-tabs/div/ul/li/span/a[contains(text(),'Group')]")]
        public IWebElement Group;

        [FindsBy(How = How.CssSelector, Using = "div.crisp-col.width-20 > crisp-button > span > button")]
        public IWebElement GroupPlusButton;

        [FindsBy(How = How.CssSelector, Using = "crisp-input-text[label='Group Name'] > div > label")]
        public IWebElement GroupNameLabel;

        [FindsBy(How = How.CssSelector, Using = "crisp-input-text[label='Group Name'] > div > input")]
        public IWebElement GroupNameInput;

        [FindsBy(How = How.XPath, Using = "//ul/li[5]/crisp-list-item/div/crisp-list-item-action/div/crisp-button/span/button")]
        public IWebElement SelectCompany1;

        [FindsBy(How = How.XPath, Using = "//ul/li[6]/crisp-list-item/div/crisp-list-item-action/div/crisp-button/span/button")]
        public IWebElement SelectCompany2;

        [FindsBy(How = How.CssSelector, Using = "company-group-create > div > div:nth-child(1) > crisp-card > crisp-card-actions > div > crisp-button > span > button")]
        public IWebElement GroupSaveButton;

        [FindsBy(How = How.XPath, Using = "//crisp-card-actions/div/crisp-button[2]/span/button[text()='Remove from Company Group']")]
        public IWebElement RemoveGroup;



        public void EditDetailsPage()
        {
            WaitForElement(OccupationDrodpDown);
            OccupationDrodpDown.Click();
            WaitForElement(OccupationDrodpDownInput);
            OccupationDrodpDownInput.Click();
            WaitForElement(CompanySegmentDrodpDown);
            CompanySegmentDrodpDown.Click();
            WaitForElement(CompanySegmentDrodpDownInput);
            CompanySegmentDrodpDownInput.Click();
            WaitForElement(NextButton);
            NextButton.Click();

        }
        public void WarrantyDetails(string editnhbcratingyear)
        {
            
            NHBCRatingYearInput.Click();
            NHBCRatingYearInput.Clear();
            NHBCRatingYearInput.SendKeys(editnhbcratingyear);
            WaitForElement(NHBCRateDropDown);
            NHBCRateDropDown.Click();
            WaitForElement(NHBCRateDropDownInput);
            NHBCRateDropDownInput.Click();
            
            WaitForElement(NextButton);
            NextButton.Click();

        }
        public void EditDirectorDetails()
        {
            WaitForElements(DirectorsList);
            int actualDirectorsCount = DirectorsList.Count();
            for (int i = 0; i < actualDirectorsCount; i++)
            {
                CloseSpinneronDiv();
                CloseSpinneronPage();
                Thread.Sleep(500);
                WaitForElement(DirectorsList[i]);
                var eachDirector = DirectorsList[i];
                CloseSpinneronDiv();
                CloseSpinneronPage();
                Thread.Sleep(500);
                eachDirector.Click();
                CloseSpinneronDiv();
                CloseSpinneronPage();
                Thread.Sleep(500);
                WaitForElement(YearsOfExpLabel);
                YearsOfExpLabel.Click();
                WaitForElement(YearsOfExpInput);
                YearsOfExpInput.Click();
                YearsOfExpInput.Clear();
                YearsOfExpInput.SendKeys("7");
                WaitForElement(CommentsOfDirectorLabel);
                CommentsOfDirectorLabel.Click();
                WaitForElement(CommentsOfDirectorInput);
                CommentsOfDirectorInput.Click();
                CommentsOfDirectorInput.Clear();
                CommentsOfDirectorInput.SendKeys("TeamWork is Very Important");
                WaitForElement(OkButton);
                OkButton.Click();
                CloseSpinneronDiv();
                CloseSpinneronPage();
                Thread.Sleep(500);
            }
            WaitForElement(NextButton);
            NextButton.Click();
        }
       
        public void EditDevelopmentDetails()
        {

            WaitForElements(PreDevList);
            if (PreDevList.Count > 0)
            {
                // int actualPreDevelopments = PreDevList.Count();
                foreach (var eachPreDev in PreDevList)
                {
                    CloseSpinneronDiv();
                    CloseSpinneronPage();
                    Thread.Sleep(500);
                    WaitForElement(eachPreDev);
                    CloseSpinneronDiv();
                    CloseSpinneronPage();
                    Thread.Sleep(500);
                    eachPreDev.Click();
                    WaitForElement(PreviousDevDiv);
                    WaitForElement(PreDevsAddress);
                    PreDevsAddress.Click();
                    WaitForElement(PreDevsAddressInput);
                    PreDevsAddressInput.Clear();
                    PreDevsAddressInput.SendKeys("Manchester");
                    WaitForElement(ReconstructionCost);
                    ReconstructionCost.Click();
                    WaitForElement(ReconstructionCostInput);
                    ReconstructionCostInput.Clear();
                    ReconstructionCostInput.SendKeys("10000");
                    WaitForElement(Unitslabel);
                    Unitslabel.Click();
                    WaitForElement(UnitsInput);
                    UnitsInput.Clear();
                    UnitsInput.SendKeys("3");
                    WaitForElement(WarrentyProviderDropdown);
                    WarrentyProviderDropdown.Click();
                    WaitForElement(WarrentyProviderDropdownInput);
                    WarrentyProviderDropdownInput.Click();
                    WaitForElement(ConstructionTypeDropdown);
                    ConstructionTypeDropdown.Click();
                    WaitForElement(ConstructionTypeDropdownInput);
                    ConstructionTypeDropdownInput.Click();
                    CloseSpinneronDiv();
                    CloseSpinneronPage();
                    Thread.Sleep(500);
                    WaitForElement(OkButton);
                    OkButton.Click();
                    CloseSpinneronDiv();
                    CloseSpinneronPage();
                    Thread.Sleep(500);
                }
            }

            WaitForElements(RemoveDevButton);
            int actualListCount = RemoveDevButton.Count();

            for (int i = 0; i < actualListCount; i++)
            {
                var conditionItem = RemoveDevButton[0];
                conditionItem.Click();
                

            }

        }
        public void NewDevelopmentDetails()
        {
            WaitForElement(PreDevsPlusButton);
            PreDevsPlusButton.Click();
            WaitForElement(PreviousDevDiv);
            WaitForElement(PreDevsAddress);
            PreDevsAddress.Click();
            PreDevsAddressInput.SendKeys("London");
            WaitForElement(ReconstructionCost);
            ReconstructionCost.Click();
            WaitForElement(ReconstructionCostInput);
            ReconstructionCostInput.SendKeys("40000");
            WaitForElement(Unitslabel);
            Unitslabel.Click();
            WaitForElement(UnitsInput);
            UnitsInput.SendKeys("4");
            WaitForElement(WarrentyProviderDropdown);
            WarrentyProviderDropdown.Click();
            WaitForElement(WarrentyProviderDropdownInput);
            WarrentyProviderDropdownInput.Click();
            WaitForElement(ConstructionTypeDropdown);
            ConstructionTypeDropdown.Click();
            WaitForElement(ConstructionTypeDropdownInput);
            ConstructionTypeDropdownInput.Click();
            
            WaitForElement(OkButton);
            OkButton.Click();
            
            WaitForElement(NextButton);
            NextButton.Click();
        }

        public void EditProspectiveBusinessDetails()
        {
            WaitForElement(LandBankHeld);
            LandBankHeld.Click();
            WaitForElement(LandBankHeldInput);
            LandBankHeldInput.Click();
            LandBankHeldInput.Clear();
            LandBankHeldInput.SendKeys("30000");
            if (ProspectiveBusinessList.Count>0)
            {
                foreach (var eachprospectivebusiness in ProspectiveBusinessList)
                {
                    eachprospectivebusiness.Click();
                    WaitForElement(ProspectiveBusinessDiv);
                    WaitForElement(ConstructedHistoricYear);
                    ConstructedHistoricYear.Click();
                    WaitForElement(ConstructedHistoricYearInput);
                    ConstructedHistoricYearInput.Click();
                    ConstructedHistoricYearInput.Clear();
                    ConstructedHistoricYearInput.SendKeys("2016");
                    WaitForElement(ConstructedHistoricUnits);
                    ConstructedHistoricUnits.Click();
                    WaitForElement(ConstructedHistoricUnitsInput);
                    ConstructedHistoricUnitsInput.SendKeys("5");
                    WaitForElement(OkButton);
                    OkButton.Click();
                }
               
            }
            
            WaitForElement(RemoveProspectiveBusinessButton);
            RemoveProspectiveBusinessButton.Click();
            

        }

        public void NewProspectiveBusinessDetails()
        {

            WaitForElement(LandBankHeld);
            LandBankHeld.Click();
            WaitForElement(LandBankHeldInput);
            LandBankHeldInput.Click();
            LandBankHeldInput.Clear();
            LandBankHeldInput.SendKeys("25000");
            WaitForElement(AddProspectiveBusinessButton);
            AddProspectiveBusinessButton.Click();
            WaitForElement(ProspectiveBusinessDiv);
            WaitForElement(ConstructedHistoricYear);
            ConstructedHistoricYear.Click();
            WaitForElement(ConstructedHistoricYearInput);
            ConstructedHistoricYearInput.SendKeys("2017");
            WaitForElement(ConstructedHistoricUnits);
            ConstructedHistoricUnits.Click();
            WaitForElement(ConstructedHistoricUnitsInput);
            ConstructedHistoricUnitsInput.SendKeys("6");
            WaitForElement(OkButton);
            OkButton.Click();
            
            WaitForElement(NextButton);
            NextButton.Click();

        }
        public void EditClaimsHistoryDetails()
        {
            if (ClaimHistoryList.Count>0)
            {
                foreach (var eachclaimhistory in ClaimHistoryList)
                {
                    eachclaimhistory.Click();
                    WaitForElement(ClaimHistoryDiv);
                    WaitForElement(ClaimsDate);
                    ClaimsDate.Click();
                    
                    WaitForElement(ClaimsDateInput);
                    ClaimsDateInput.Click();
                    
                    WaitForElement(ClaimsValue);
                    ClaimsValue.Click();
                    ClaimsValueInput.SendKeys("25000");
                    WaitForElement(ClaimsComments);
                    ClaimsComments.Click();
                    WaitForElement(ClaimsCommentsInput);
                    ClaimsCommentsInput.SendKeys("Positive Claims");
                    WaitForElement(OkButton);
                    OkButton.Click();
                }
               
            }
            WaitForElement(RemoveClaimsHistoryButton);
            RemoveClaimsHistoryButton.Click();
            


        }
        public void NewClaimsHistoryDetails()
        {
            WaitForElement(AddHistoricClaimsButton);
            AddHistoricClaimsButton.Click();
            WaitForElement(ClaimHistoryDiv);
            WaitForElement(ClaimsDate);
            ClaimsDate.Click();
            
            WaitForElement(ClaimsDateInput);
            ClaimsDateInput.Click();
            
            WaitForElement(ClaimsValue);
            ClaimsValue.Click();
            ClaimsValueInput.SendKeys("35000");
            WaitForElement(ClaimsComments);
            ClaimsComments.Click();
            WaitForElement(ClaimsCommentsInput);
            ClaimsCommentsInput.SendKeys("Good");
            WaitForElement(OkButton);
            OkButton.Click();
            WaitForElement(NextButton);
            NextButton.Click();
            
        }
        public void EditScoresDetails()
        {
            WaitForElement(FinancialScoresDropdown);
            
            FinancialScoresDropdown.Click();
            WaitForElement(FinancialScoresDropdownInput);
            
            FinancialScoresDropdownInput.Click();
            WaitForElement(LandBankScoresDropdown);
            LandBankScoresDropdown.Click();
            WaitForElement(LandBankScoresDropdownInput);
            
            LandBankScoresDropdownInput.Click();
            WaitForElement(UnitsScoresDropdown);
            UnitsScoresDropdown.Click();
            WaitForElement(UnitsScoresDropdownInput);
            
            UnitsScoresDropdownInput.Click();
            WaitForElement(TradingScoresDropdown);
            TradingScoresDropdown.Click();
            WaitForElement(TradingScoresDropdownInput);
            
            TradingScoresDropdownInput.Click();
            WaitForElement(CompanyStructureScoresDropdown);
            CompanyStructureScoresDropdown.Click();
            WaitForElement(CompanyStructureScoresDropdownInput);
            
            CompanyStructureScoresDropdownInput.Click();
            WaitForElement(TotalScrores);
            var totalScoreValue = TotalScrores.Text;
            Assert.AreEqual("10", totalScoreValue);
            WaitForElement(TechgRatingDropdown);
            TechgRatingDropdown.Click();
            WaitForElement(TechgRatingDropdownInput);
            TechgRatingDropdownInput.Click();
            WaitForElement(TechRatingComment);
            TechRatingComment.Click();
            WaitForElement(TechRatingCommentText);
            TechRatingCommentText.Click();
            TechRatingCommentText.Clear();
            TechRatingCommentText.SendKeys("RatingDetailsEdited");
            
        }

        //public void EditGroupDetails(string groupname)
        //{
        //        //Thread.Sleep(1500);
        //        GroupPlusButton.Click();
        //        //Thread.Sleep(500);
        //        GroupNameLabel.Click();
        //        //Thread.Sleep(1000);
        //        GroupNameInput.Clear();
        //        GroupNameInput.SendKeys(groupname);
        //        //Thread.Sleep(1000);
        //        SelectCompany1.Click();
        //        SelectCompany2.Click();
        //        GroupSaveButton.Click();
        //        //Thread.Sleep(1000);
        //}
    }
}
