﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using CrispAutomation.Features;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.PageObjects;

namespace CrispAutomation.Pages
{
   public class ActivitiesPage : Support.Pages
    {
        public IWebDriver wdriver;
        public ActivitiesPage(ISearchContext driver)
        {
            PageFactory.InitElements(driver, this);
            wdriver = (RemoteWebDriver)driver;
        }

        [FindsBy(How = How.XPath, Using = "//crisp-tabs[@class='au-target']//ul[@ref='tabHeader']//li//a[contains(text(),'Activities')]")]
        public IWebElement ActivitiesTab;

        [FindsBy(How = How.XPath, Using = "//activities[@class='custom-element au-target']//crisp-card-content[@class='au-target']//crisp-list[@list-title='Activities']//div[@class='au-target list-title']//div")]
        public IWebElement ActivitiesTitle;

        [FindsBy(How = How.XPath,Using = "//crisp-list[@list-title='Activities']//ul[@ref='theList']//li[contains(@class,'au-target collection-item')]")]
        public IList<IWebElement> ActivitiesRows;

        [FindsBy(How = How.XPath, Using = "//crisp-list[@list-title='Activities']//ul[@ref='theList']//li[@class='au-target collection-item']//crisp-list-item//crisp-list-item-title[@class='au-target']//span")]
        public  IList<IWebElement> ActivitiesList;
        
        //Activity Card Content 
        [FindsBy(How = How.XPath, Using = "//crisp-list-item-title[@class='au-target']//div[@class='crisp-row']//div[contains(@class,'overflow-fade')]//span[1]")]
        public IWebElement ActivityDateCreated;

        [FindsBy(How = How.XPath, Using = "//crisp-card[@class='au-target']//crisp-card-content[@class='au-target']//div//dl//dt[text()='Correspondence Status']")]
        public IWebElement CorrespondenceStatus;

        [FindsBy(How = How.XPath, Using = "//crisp-card[@class='au-target']//crisp-card-content[@class='au-target']//div//dl//dt[text()='Priority']")]
        public IWebElement Priority;

        [FindsBy(How = How.XPath, Using = "//crisp-card[@class='au-target']//crisp-card-content[@class='au-target']//div//dl//dt[text()='Stage']")]
        public IWebElement Stage;

        [FindsBy(How = How.XPath, Using = "//crisp-card[@class='au-target']//crisp-card-content[@class='au-target']//div//dl//dt[text()='Date Created']")]
        public IWebElement DateCreated;

        [FindsBy(How = How.XPath, Using = "//crisp-card[@class='au-target']//crisp-card-content[@class='au-target']//div//dl//dt[text()='Created By']")]
        public IWebElement CreatedBy;

        [FindsBy(How = How.XPath, Using = "//crisp-card[@class='au-target']//crisp-card-content[@class='au-target']//div//dl//dt[text()='Department']")]
        public IWebElement Department;

        [FindsBy(How = How.XPath, Using = "//crisp-card[@class='au-target']//crisp-card-content[@class='au-target']//div//dl//dt[text()='Notes']")]
        public IWebElement Notes;

        [FindsBy(How = How.XPath, Using = "//crisp-card[@class='au-target']//crisp-card-content[@class='au-target']//div//dl//dd")]
        public IList<IWebElement> LeadActivityValueList;

        [FindsBy(How = How.XPath, Using = "//crisp-card-actions[@class='au-target']//crisp-button[@btn-title='Promote']//button")]
        public IWebElement PromoteButton;

        [FindsBy(How = How.XPath, Using = "//crisp-dialog//div[@class='au-target modal dialog brand default modal-has-header']//crisp-header[@class='au-target']")]
        public IWebElement PromoteActivityWizard;

        [FindsBy(How = How.XPath, Using = "//crisp-button[@class='au-target']//span/button[text()='Yes']")]
        public IWebElement PromoteActivityYesButton;

        [FindsBy(How = How.XPath, Using = "//crisp-card-actions[@class='au-target']//crisp-button[@btn-title='Relegate']//button")]
        public IWebElement RelegateButton;

        [FindsBy(How = How.XPath, Using = "//div//crisp-header-title[@class='au-target'][1]/div[@class='title']")]
        public IWebElement OrderRefDetails;

        [FindsBy(How = How.XPath, Using = "//crisp-card[@class='au-target']//crisp-card-content[@class='au-target']//div//dl//dd")]
        public IList<IWebElement> QuoteActivityValueList;

        //Allocating Email Activity 

        [FindsBy(How = How.XPath, Using = "//crisp-nav-bar[@class='au-target']/a[@title='Index mail']//span/i")]
        public IWebElement IndexMail;

        [FindsBy(How = How.XPath, Using = "//crisp-input-object//div[@class='au-target input-field']//label[text()='Please select the mailbox to view']")]
        public IWebElement MailboxLabel;
        
        [FindsBy(How = How.XPath, Using = "//crisp-card[@class='au-target']//crisp-card-header[@class='au-target']//div//span[text()='Choose Mailbox']")]
        public IWebElement ChooseMailBoxDiv;

        [FindsBy(How = How.XPath, Using = "//crisp-card-content[@class='au-target']//ul[@class='au-target collection has-header has-filter']//li[@class='au-target collection-item']")]
        public IList<IWebElement> EmailList;

        [FindsBy(How = How.XPath, Using = "//crisp-list//ul[@class='au-target collection has-header']/li[@class='au-target collection-item']")]
        public IList<IWebElement> SharedMaiLboxList;

        [FindsBy(How = How.XPath, Using = "//ul[@class='au-target collection has-header']/li[@class='au-target collection-item active']//crisp-list-item//a//crisp-list-item-action//div/crisp-input-bool//label")]
        public IWebElement SharedMaiLboxCheckBox;

        [FindsBy(How = How.XPath, Using = "//crisp-card-actions[@class='au-target']//crisp-button[@click.delegate='indexSingleEmail(email)']//button[@class='au-target waves-effect waves-light btn'][text()='Index This Email']")]
        public IWebElement IndexThisEmailButton;     

        [FindsBy(How = How.XPath, Using = "//crisp-dialog//div//crisp-header[@class='au-target']//div//crisp-header-title//div[text()='Index Email']")]
        public IWebElement IndexEmaiLDiv;
        
        [FindsBy(How = How.XPath, Using = "//div[@class='modal-content']//crisp-input-object[@class='au-target']//div//span[@ref='inputContainer']/span[@class='au-target']")]
        public IWebElement SelectEntityToIndexEmail;

        [FindsBy(How = How.XPath, Using = "//crisp-card//crisp-card-header[@class='au-target']//div//span[text()='Select Entities']")]
        public IWebElement SelectEntityDiv;

        [FindsBy(How = How.XPath, Using = "//crisp-card-content[@class='au-target']//div//crisp-input-text[@label='Search']//div//label")]
        public IWebElement SearchLabel;

        [FindsBy(How = How.XPath, Using = "//crisp-card-content[@class='au-target']//div//crisp-input-text[@label='Search']//div//input")]
        public IWebElement SearchInput;

        [FindsBy(How = How.XPath, Using = "//crisp-list//ul[@class='au-target collection']//li[@class='au-target collection-item']")]
        public IList<IWebElement> EntityList;

        [FindsBy(How = How.XPath, Using = "//crisp-button//button[text()='Select'][@class='au-target waves-effect waves-light btn']")]
        public IWebElement SelectButton;
        
        [FindsBy(How = How.XPath, Using = "//crisp-button//button[text()='Index'][@class='au-target waves-effect waves-light btn-flat']")]
        public IWebElement IndexButton;

        [FindsBy(How = How.XPath, Using = "//crisp-input-radio[@label='Notification Assigning Options']//div//ul//li")]
        public IList<IWebElement> AsigneeList;
        

        public void AllocateEmailMethod()
        {
            WaitForElement(IndexMail);
            IndexMail.Click();          
            WaitForElement(MailboxLabel);
            MailboxLabel.Click();          
            CloseSpinneronDiv();
            CloseSpinneronPage();
            WaitForElement(ChooseMailBoxDiv);
            WaitForElements(EmailList);
            if (EmailList.Count > 0)
            {
                WaitForElements(EmailList);
                EmailList[2].Click();              
                CloseSpinneronDiv();
                CloseSpinneronPage();
                Thread.Sleep(500);
            }           
            CloseSpinneronDiv();
            CloseSpinneronPage();
            if (SharedMaiLboxList.Count > 0)
            {
                WaitForElement(SharedMaiLboxList[0]);
                SharedMaiLboxList[0].Click();             
                CloseSpinneronDiv();
                CloseSpinneronPage();
                WaitForElement(SharedMaiLboxCheckBox);
                SharedMaiLboxCheckBox.Click();
                WaitForElement(IndexThisEmailButton);
                IndexThisEmailButton.Click();
                WaitForElement(IndexEmaiLDiv);
                WaitForElement(SelectEntityToIndexEmail);
                SelectEntityToIndexEmail.Click();
                WaitForElement(SelectEntityDiv);
                WaitForElement(SearchLabel);
                SearchLabel.Click();
                WaitForElement(SearchInput);
                SearchInput.Clear();
                SearchInput.SendKeys(Statics.SiteRefNumber);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                WaitForElements(EntityList);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                if (EntityList.Count > 0)
                {
                    WaitForElement(EntityList[0]);
                    EntityList[0].Click();
                }
                CloseSpinneronDiv();
                CloseSpinneronPage();
                Thread.Sleep(500);
                WaitForElement(SelectButton);
                SelectButton.Click();
                CloseSpinneronDiv();
                CloseSpinneronPage();
                Thread.Sleep(500);
                if(AsigneeList.Count>0)
                {
                    WaitForElement(AsigneeList[0]);
                    AsigneeList[0].Click();
                    CloseSpinneronDiv();
                    CloseSpinneronPage();
                    Thread.Sleep(500);                  
                }
                WaitForElement(IndexButton);
                IndexButton.Click();
                CloseSpinneronDiv();
                CloseSpinneronPage();
                Thread.Sleep(500);           
                AddLeadPage.CheckEmailCorrespondenceOnLead();
            }           
        }


        public DateTime thisDay = DateTime.Today;

        public void StoreOrderRefDetails()
        {
            WaitForElement(OrderRefDetails);
            var OrderRefNumber = OrderRefDetails.Text;
         //ExtensionMethods.CreateSpreadsheet(OrderRefNumber);
            Statics.OrderNumber = OrderRefNumber;
        }


        public void VerfiyActivities()
        {
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            WaitForElement(ActivitiesTab);
            ActivitiesTab.Click();
            WaitForElement(ActivitiesTitle);          
            Assert.IsTrue(ActivitiesTitle.Text.Contains("Activities"));
            WaitForElements(ActivitiesList);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
        }
        public void LeadCreatedActivity()
        {
            var activityDateCreated = (ActivityDateCreated.Text).TrimEnd(':',' ');
            WaitForElement(Priority);
            Assert.IsTrue(Priority.Displayed);
            WaitForElement(Stage);
            Assert.IsTrue(Stage.Displayed);
            WaitForElement(DateCreated);
            Assert.IsTrue(DateCreated.Displayed);
            WaitForElement(CreatedBy);
            Assert.IsTrue(CreatedBy.Displayed);
            WaitForElement(Department);
            Assert.IsTrue(Department.Displayed);
            WaitForElement(Notes);
            Assert.IsTrue(Notes.Displayed);
            WaitForElements(LeadActivityValueList);
            Assert.IsTrue(ActivitiesPage.LeadActivityValueList[0].Text == "Milestone");
            Assert.IsTrue(ActivitiesPage.LeadActivityValueList[1].Text == "Lead");
            Assert.IsTrue(ActivitiesPage.LeadActivityValueList[2].Text == activityDateCreated);
            //Assert.IsTrue(ActivitiesPage.LeadActivityValueList[3].Text == "Brian Hare");
           // Assert.IsTrue(ActivitiesPage.LeadActivityValueList[4].Text == "Test Team");
            Assert.IsTrue(ActivitiesPage.LeadActivityValueList[5].Text == "New lead was manually created");
          
        }
        public void LeadShortListedActivity()
        {
            var activityDateCreated = (ActivityDateCreated.Text).TrimEnd(':', ' ');
            WaitForElement(Priority);
            Assert.IsTrue(Priority.Displayed);
            WaitForElement(Stage);
            Assert.IsTrue(Stage.Displayed);
            WaitForElement(DateCreated);
            Assert.IsTrue(DateCreated.Displayed);
            WaitForElement(CreatedBy);
            Assert.IsTrue(CreatedBy.Displayed);
            WaitForElement(Department);
            Assert.IsTrue(Department.Displayed);
            WaitForElement(Notes);
            Assert.IsTrue(Notes.Displayed);
            WaitForElements(LeadActivityValueList);
            Assert.IsTrue(ActivitiesPage.LeadActivityValueList[0].Text == "Major");
            Assert.IsTrue(ActivitiesPage.LeadActivityValueList[1].Text == "Opportunity");
            Assert.IsTrue(ActivitiesPage.LeadActivityValueList[2].Text == activityDateCreated);
           // Assert.IsTrue(ActivitiesPage.LeadActivityValueList[3].Text == "Brian Hare");
           // Assert.IsTrue(ActivitiesPage.LeadActivityValueList[4].Text == "Test Team");
            Assert.IsTrue(ActivitiesPage.LeadActivityValueList[5].Text == "A new sales opportunity has been created");                     
          
        }

        public void LeadEmailIndexActivity()
        {
            var activityDateCreated = (ActivityDateCreated.Text).TrimEnd(':', ' ');
            WaitForElement(Priority);
            Assert.IsTrue(Priority.Displayed);
            WaitForElement(Stage);
            Assert.IsTrue(Stage.Displayed);
            WaitForElement(DateCreated);
            Assert.IsTrue(DateCreated.Displayed);
            WaitForElement(CreatedBy);
            Assert.IsTrue(CreatedBy.Displayed);
            WaitForElement(Department);
            Assert.IsTrue(Department.Displayed);
            WaitForElement(Notes);
            Assert.IsTrue(Notes.Displayed);
            WaitForElements(LeadActivityValueList);
            Assert.IsTrue(ActivitiesPage.LeadActivityValueList[1].Text == "Minor");
            Assert.IsTrue(ActivitiesPage.LeadActivityValueList[2].Text == "Lead");
            Assert.IsTrue(ActivitiesPage.LeadActivityValueList[3].Text == activityDateCreated);
            Assert.IsTrue(ActivitiesPage.LeadActivityValueList[4].Text.Contains("Microsoft Outlook"));
           // Assert.IsTrue(ActivitiesPage.LeadActivityValueList[5].Text == "Test Team");
            Assert.IsTrue(ActivitiesPage.LeadActivityValueList[6].Text.Contains("Email received from Microsoft Outlook"));
          
        }

        public void PromoteLeadActivity()
        {
            WaitForElements(ActivitiesRows);
            var activitiesRowsCount = ActivitiesRows.Count;
            WaitForElement(PromoteButton);
            PromoteButton.Click();
            WaitForElement(PromoteActivityWizard);
            WaitForElement(PromoteActivityYesButton);
            PromoteActivityYesButton.Click();
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            var actualactivitiesRowscount = activitiesRowsCount + 1;          
            WaitForElements(ActivitiesRows);          
            Assert.IsTrue(actualactivitiesRowscount == ActivitiesRows.Count);
            Assert.IsTrue(ActivitiesPage.ActivitiesList.Count >= 2);
            Assert.IsTrue(ActivitiesPage.ActivitiesList.Any(x => x.Text == "New lead was manually created"));
            Assert.IsTrue(ActivitiesPage.ActivitiesList.Count(x => x.Text == "New lead was manually created") == 2);
            Assert.IsTrue(ActivitiesPage.ActivitiesList.Any(x => x.Text == "New Lead Created - "));
            Assert.IsTrue(ActivitiesPage.ActivitiesList.Count(x => x.Text == "New Lead Created - ") == 2);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
        }

        public void RelegateLeadActivity()
        {
            WaitForElements(ActivitiesRows);
            var activitiesRowsCount = ActivitiesRows.Count;
            WaitForElement(RelegateButton);
            RelegateButton.Click();
            WaitForElement(PromoteActivityWizard);
            WaitForElement(PromoteActivityYesButton);
            PromoteActivityYesButton.Click();
            Thread.Sleep(1000);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(1000);
            var actualactivitiesRowscount = activitiesRowsCount - 1;
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            WaitForElements(ActivitiesRows);
            Assert.IsTrue(actualactivitiesRowscount == ActivitiesRows.Count);
            Assert.IsTrue(ActivitiesPage.ActivitiesList.Count >= 1);
            Assert.IsTrue(ActivitiesPage.ActivitiesList.Any(x => x.Text == "New lead was manually created"));
            Assert.IsTrue(ActivitiesPage.ActivitiesList.Count(x => x.Text == "New lead was manually created") == 1);
            Assert.IsTrue(ActivitiesPage.ActivitiesList.Any(x => x.Text == "New Lead Created - "));
            Assert.IsTrue(ActivitiesPage.ActivitiesList.Count(x => x.Text == "New Lead Created - ") == 1);
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
        }
        public void QuoteCreatedActivity()
        {
            var activityDateCreated = (ActivityDateCreated.Text).TrimEnd(':', ' ');
            WaitForElement(Priority);
            Assert.IsTrue(Priority.Displayed);
            WaitForElement(Stage);
            Assert.IsTrue(Stage.Displayed);
            WaitForElement(DateCreated);
            Assert.IsTrue(DateCreated.Displayed);
            WaitForElement(CreatedBy);
            Assert.IsTrue(CreatedBy.Displayed);
            WaitForElement(Department);
            Assert.IsTrue(Department.Displayed);
            WaitForElement(Notes);
            Assert.IsTrue(Notes.Displayed);
            WaitForElements(QuoteActivityValueList);
            Assert.IsTrue(ActivitiesPage.QuoteActivityValueList[0].Text == "Major");
            Assert.IsTrue(ActivitiesPage.QuoteActivityValueList[1].Text == "Quote");
            Assert.IsTrue(ActivitiesPage.QuoteActivityValueList[2].Text == activityDateCreated);
           // Assert.IsTrue(ActivitiesPage.QuoteActivityValueList[3].Text == "Brian Hare");
           // Assert.IsTrue(ActivitiesPage.QuoteActivityValueList[4].Text == "Test Team");
            Assert.IsTrue(ActivitiesPage.QuoteActivityValueList[5].Text == "A quote application was created");
          
        }
        public void QuoteSubmittedActivity()
        {
            var activityDateCreated = (ActivityDateCreated.Text).TrimEnd(':', ' ');
            WaitForElement(Priority);
            Assert.IsTrue(Priority.Displayed);
            WaitForElement(Stage);
            Assert.IsTrue(Stage.Displayed);
            WaitForElement(DateCreated);
            Assert.IsTrue(DateCreated.Displayed);
            WaitForElement(CreatedBy);
            Assert.IsTrue(CreatedBy.Displayed);
            WaitForElement(Department);
            Assert.IsTrue(Department.Displayed);
            WaitForElement(Notes);
            Assert.IsTrue(Notes.Displayed);
            WaitForElements(QuoteActivityValueList);
            Assert.IsTrue(ActivitiesPage.QuoteActivityValueList[0].Text == "Major");
            Assert.IsTrue(ActivitiesPage.QuoteActivityValueList[1].Text == "Quote");
            Assert.IsTrue(ActivitiesPage.QuoteActivityValueList[2].Text == activityDateCreated);
           // Assert.IsTrue(ActivitiesPage.QuoteActivityValueList[3].Text == "Brian Hare");
          //  Assert.IsTrue(ActivitiesPage.QuoteActivityValueList[4].Text == "Test Team");
            Assert.IsTrue(ActivitiesPage.QuoteActivityValueList[5].Text == "The complete quote application was submitted for QGU review.");
          
        }

        public void QuoteSentActivity()
        {
            var activityDateCreated = (ActivityDateCreated.Text).TrimEnd(':', ' ');
            WaitForElement(CorrespondenceStatus);
            Assert.IsTrue(CorrespondenceStatus.Displayed);
            WaitForElement(Priority);
            Assert.IsTrue(Priority.Displayed);
            WaitForElement(Stage);
            Assert.IsTrue(Stage.Displayed);
            WaitForElement(DateCreated);
            Assert.IsTrue(DateCreated.Displayed);
            WaitForElement(CreatedBy);
            Assert.IsTrue(CreatedBy.Displayed);
            WaitForElement(Department);
            Assert.IsTrue(Department.Displayed);
            WaitForElement(Notes);
            Assert.IsTrue(Notes.Displayed);
            WaitForElements(QuoteActivityValueList);
           // Assert.IsTrue(ActivitiesPage.LeadActivityValueList[0].Text == "Pending");
            Assert.IsTrue(ActivitiesPage.QuoteActivityValueList[1].Text == "Milestone");
            Assert.IsTrue(ActivitiesPage.QuoteActivityValueList[2].Text == "Quote");
            Assert.IsTrue(ActivitiesPage.QuoteActivityValueList[3].Text == activityDateCreated);
           // Assert.IsTrue(ActivitiesPage.QuoteActivityValueList[4].Text == "Brian Hare");
           // Assert.IsTrue(ActivitiesPage.QuoteActivityValueList[5].Text == "Test Team");
            Assert.IsTrue(ActivitiesPage.QuoteActivityValueList[6].Text == "A quotation was sent to the client");
          
        }
        public void QuoteAcceptActivity()
        {
            var activityDateCreated = (ActivityDateCreated.Text).TrimEnd(':', ' ');
            WaitForElement(CorrespondenceStatus);
            Assert.IsTrue(CorrespondenceStatus.Displayed);
            WaitForElement(Priority);
            Assert.IsTrue(Priority.Displayed);
            WaitForElement(Stage);
            Assert.IsTrue(Stage.Displayed);
            WaitForElement(DateCreated);
            Assert.IsTrue(DateCreated.Displayed);
            WaitForElement(CreatedBy);
            Assert.IsTrue(CreatedBy.Displayed);
            WaitForElement(Department);
            Assert.IsTrue(Department.Displayed);
            WaitForElement(Notes);
            Assert.IsTrue(Notes.Displayed);
            WaitForElements(QuoteActivityValueList);
           // Assert.IsTrue(ActivitiesPage.LeadActivityValueList[0].Text == "Pending");
            Assert.IsTrue(ActivitiesPage.QuoteActivityValueList[1].Text == "Milestone");
            Assert.IsTrue(ActivitiesPage.QuoteActivityValueList[2].Text == "Order");
            Assert.IsTrue(ActivitiesPage.QuoteActivityValueList[3].Text == activityDateCreated);
           // Assert.IsTrue(ActivitiesPage.QuoteActivityValueList[4].Text == "Brian Hare");
           // Assert.IsTrue(ActivitiesPage.QuoteActivityValueList[5].Text == "Test Team");
            Assert.IsTrue(ActivitiesPage.LeadActivityValueList[6].Text == "The quotation was accepted by the client");
          
        }

        public void PromoteQuoteActivity()
        {
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            WaitForElements(ActivitiesRows);
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            var activitiesRowsCount = ActivitiesRows.Count;
            WaitForElement(PromoteButton);
            PromoteButton.Click();
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            WaitForElement(PromoteActivityWizard);
            WaitForElement(PromoteActivityYesButton);
            PromoteActivityYesButton.Click();
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            var actualactivitiesRowscount = activitiesRowsCount + 1;          
            WaitForElements(ActivitiesRows);
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            Assert.IsTrue(actualactivitiesRowscount == ActivitiesRows.Count);
            Assert.IsTrue(ActivitiesPage.ActivitiesList.Count >= 2);
            Assert.IsTrue(ActivitiesPage.ActivitiesList.Any(x => x.Text == "Quote record created - "));
            Assert.IsTrue(ActivitiesPage.ActivitiesList.Count(x => x.Text == "Quote record created - ") == 2);
            Assert.IsTrue(ActivitiesPage.ActivitiesList.Any(x => x.Text == "A quote application was created"));
            Assert.IsTrue(ActivitiesPage.ActivitiesList.Count(x => x.Text == "A quote application was created") == 2);
        }
        public void RelegateQuoteActivity()
        {
            WaitForElements(ActivitiesRows);
            var activitiesRowsCount = ActivitiesRows.Count;
            WaitForElement(RelegateButton);
            RelegateButton.Click();
            WaitForElement(PromoteActivityWizard);
            WaitForElement(PromoteActivityYesButton);
            PromoteActivityYesButton.Click();           
            CloseSpinneronDiv();
            var actualactivitiesRowscount = activitiesRowsCount - 1;         
            WaitForElements(ActivitiesRows);
            Assert.IsTrue(actualactivitiesRowscount == ActivitiesRows.Count);
            Assert.IsTrue(ActivitiesPage.ActivitiesList.Count >= 1);
            Assert.IsTrue(ActivitiesPage.ActivitiesList.Any(x => x.Text == "Quote record created - "));
            Assert.IsTrue(ActivitiesPage.ActivitiesList.Count(x => x.Text == "Quote record created - ") == 1);
            Assert.IsTrue(ActivitiesPage.ActivitiesList.Any(x => x.Text == "A quote application was created"));
            Assert.IsTrue(ActivitiesPage.ActivitiesList.Count(x => x.Text == "A quote application was created") == 1);
        }
    }
}
