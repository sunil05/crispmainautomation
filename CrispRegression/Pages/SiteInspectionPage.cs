﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using OpenQA.Selenium.Support.PageObjects;
using NUnit.Framework;

namespace CrispAutomation.Pages
{
    public class SiteInspectionPage : Support.Pages
    {
        public IWebDriver wdriver;

        public SiteInspectionPage(ISearchContext driver)
        {
            PageFactory.InitElements(driver, this);
        }
        [FindsBy(How = How.XPath, Using = "//a/span[text()='Sites']")]
        public IWebElement Sites;

        [FindsBy(How = How.CssSelector, Using = "div > div > input[placeholder='Search']")]
        public IWebElement Search;

        [FindsBy(How = How.XPath, Using = "//div/div/button[text()='Search']")]
        public IWebElement SearchButton;

        [FindsBy(How = How.XPath, Using = "//div/label[@for='allSites']")]
        public IWebElement AllSitesCheckbox;

        [FindsBy(How = How.XPath, Using = "//div/table/tbody/tr/th/a[1]")]
        public IWebElement OrderRef;

        [FindsBy(How = How.XPath, Using = "//div[@class='info-box bg-pink']//a[contains(text(),'update')]")]
        public IWebElement InspectionandUpdates;

        [FindsBy(How = How.XPath, Using = "//div[@class='info-box bg-pink']//div[@class='content']//div[text()='Last Inspection']//following-sibling::p")]
        public IWebElement LastInspectionStatus;

        [FindsBy(How = How.XPath, Using = "//section[@class='content']//div[@class='card']//a[text()='Add']")]
        public IWebElement InspectionsAddButton;

        [FindsBy(How = How.XPath, Using = "//label[@for='InitialPage_TravelledToSite_true'][text()='Yes']")]
        public IWebElement TravelToSite;

        [FindsBy(How = How.XPath, Using = "//label[@for='InitialPage_TravelledToSite_true'][text()='No']")]
        public IWebElement TravelToSiteNo;

        [FindsBy(How = How.XPath, Using = "//label[@for='InitialPage_SiteInspectionComplete_true'][text()='Yes']")]
        public IWebElement SiteInspectionComplete;

        [FindsBy(How = How.XPath, Using = "//form[@id='newSurveyorUpdateForm']//div[@data-crisp-visible-test='TravelledToSite == true']//div[@class='form-line error']//input[2]")]
        public IWebElement InspectionDate;

        [FindsBy(How = How.XPath, Using = "//input[@id='InitialPage_SiteVisitDetails_Minutes']")]
        public IWebElement SiteVisitDuration;

        [FindsBy(How = How.XPath, Using = "//div//button[text()='Next']")]
        public IWebElement NextButton;

        //Sign Off BC elements 

        [FindsBy(How = How.XPath, Using = "//div[@id='SummaryPage_Blocks']//label[contains(@for,'Inspected')]")]
        public IWebElement InspectedBlocks;

        [FindsBy(How = How.XPath, Using = "//div[@id='SummaryPage_Plots']//table//tbody//tr//td//label[contains(@for,'Inspected')]")]
        public IList<IWebElement> InspectedPlots;

        [FindsBy(How = How.XPath, Using = "//div[@id='SummaryPage_Plots']//table//tbody//tr//td//label[contains(@for,'SignOffWarranty')]")]
        public IList<IWebElement> SignOffWarranty;

        [FindsBy(How = How.XPath, Using = "//div[@id='SummaryPage_Plots']//table//tbody//tr//td//input[contains(@name,'SignOffBuildingControl')][@type='checkbox']")]
        public IList<IWebElement> SignoffBC;

        [FindsBy(How = How.XPath, Using = "//div[@id='SummaryPage_Plots']//table//tbody//tr//td//label[contains(@for,'SignOffBuildingControl')]")]
        public IList<IWebElement> SignoffBCLabel;

        [FindsBy(How = How.XPath, Using = "//div[@id='SummaryPage_Plots']//tbody//td//button[contains(@data-id,'UpdatedStageOfWorks')]")]
        public IWebElement UpdatedStagesOfWorks;

        [FindsBy(How = How.XPath, Using = "//div[@id='SummaryPage_Plots']//div[@class='dropdown-menu open'] //ul[@class='dropdown-menu inner']//li//a//span[text()='Pre-Handover']")]
        public IWebElement UpdatedStagesOfWorksInput;

        [FindsBy(How = How.XPath, Using = "//textarea[@name='SummaryPage.OrderDetails.InspectionComments[PreHandover].Comment']")]
        public IWebElement PGHandoverComment;

        [FindsBy(How = How.XPath, Using = "//textarea[@name='SummaryPage.OrderDetails.InspectionComments[PreHandover].FutureGuidance']")]
        public IWebElement PGHandoverFutureGuidance;

        [FindsBy(How = How.XPath, Using = "//div[@class='form-contents']//div[@class='header']//h2[text()='QIS Questions']//parent::div//parent::div//div[@id='AssessmentPage_Indicators']")]
        public IList<IWebElement> QISQuestions;

        [FindsBy(How = How.XPath, Using = "//div[@id='AssessmentPage_Indicators']//table//tbody//tr//td//div[contains(@class,'form-group')]//div[contains(@class,'question-selectbox')]//button[@title='Nothing selected']")]
        public IList<IWebElement> QISQuestionsDropdowns;

        [FindsBy(How = How.XPath, Using = "//div[contains(@class,'open')]//div[@class='dropdown-menu open']//ul[@class='dropdown-menu inner']//li[@data-original-index='3']")]
        public IWebElement QISQuestionsDropdownInputs;


        [FindsBy(How = How.XPath, Using = "//h2[contains(text(),'Assessment & Scores')]")]
        public IWebElement AssessmentandScores;

        [FindsBy(How = How.XPath, Using = "//div[contains(@class,'br-theme-bootstrap-stars')]//div[@class='br-widget']/a[5]")]
        public IList<IWebElement> RatingStars;

        [FindsBy(How = How.XPath, Using = "//textarea[contains(@id,'_Comment)')]")]
        public IList<IWebElement> RatingComment;

        [FindsBy(How = How.XPath, Using = "//div[@class='btn-group bootstrap-select form-control']//button//span[text()='Nothing selected']")]
        public IList<IWebElement> SiteStatusDropdowns;


        [FindsBy(How = How.XPath, Using = "//div[@class='btn-group bootstrap-select form-control']//button[@data-id='AssessmentPage_FollowUp_FollowUpType']//span[text()='Nothing selected']")]
        public IList<IWebElement> FollowUpType;
        

        [FindsBy(How = How.XPath, Using = "//div[contains(@class,'open')]//div[@class='dropdown-menu open']//ul[@class='dropdown-menu inner']//li[@data-original-index='1']")]
        public IWebElement SiteStatusDropdownInput;
        

        [FindsBy(How = How.XPath, Using = "//div//button[text()='Save']")]
        public IWebElement SaveButton;

        [FindsBy(How = How.XPath, Using = "//button[@type='submit'][text()='Send']")]
        public IWebElement SendEmailButton;

        [FindsBy(How = How.XPath, Using = "//table[@id='defectsTable']//tr[@role='row'][@class='odd']")]
        public IList<IWebElement> DefectsList;

        [FindsBy(How = How.XPath, Using = "//div[@id='SummaryPage_Plots']//table//tbody//tr//div//p[text()='Cannot be signed off since it has open defects']")]
        public IWebElement OpenDefects;

        static DateTime date = DateTime.Today.AddDays(-1); // will give the date for previous day
        private string dateWithFormat = date.ToString("dd/MM/yyyy HH:mm");

        //div[@class='alert alert-danger validation-summary-errors']//ul//li
        [FindsBy(How = How.XPath, Using = "//div[@class='alert alert-danger validation-summary-errors']//ul//li")]
        public IList<IWebElement> ErrorsList;

        //open SiteInspection 
        public void OpenSiteInspection()
        {
            Thread.Sleep(500);
            CloseSpinnerOnSurveyorDiv();
            CloseSpinnerOnSurveyorPage();
            Thread.Sleep(500);
            WaitForElementonSurveyor(InspectionandUpdates);
            InspectionandUpdates.Click();
            //WaitForElementonSurveyor(InspectionsAddButton);
            //InspectionsAddButton.Click();
            WaitForElementonSurveyor(TravelToSite);
            TravelToSite.Click();
            Thread.Sleep(500);
            CloseSpinnerOnSurveyorDiv();
            CloseSpinnerOnSurveyorPage();
            Thread.Sleep(500);
            WaitForLoadElement(InspectionDate);
            Thread.Sleep(500);
            CloseSpinnerOnSurveyorDiv();
            CloseSpinnerOnSurveyorPage();
            Thread.Sleep(500);
            InspectionDate.SendKeys(dateWithFormat);
            WaitForLoadElement(SiteVisitDuration);
            SiteVisitDuration.SendKeys("300");
            Thread.Sleep(500);
            CloseSpinnerOnSurveyorDiv();
            CloseSpinnerOnSurveyorPage();
            Thread.Sleep(500);
            WaitForLoadElement(SiteInspectionComplete);
            SiteInspectionComplete.Click();
            Thread.Sleep(500);
            CloseSpinnerOnSurveyorDiv();
            CloseSpinnerOnSurveyorPage();
            Thread.Sleep(500);
        }
        //Save Site Inspection
        public void SaveSiteInspection()
        {
            Thread.Sleep(500);
            CloseSpinnerOnSurveyorDiv();
            CloseSpinnerOnSurveyorPage();
            Thread.Sleep(500);
            WaitForElementonSurveyor(SaveButton);
            SaveButton.Click();
            Thread.Sleep(500);
            CloseSpinnerOnSurveyorDiv();
            CloseSpinnerOnSurveyorPage();
            Thread.Sleep(500);
            if (ErrorsList.Count>0)
            {
                CatchError();
            }
            WaitForElementonSurveyor(SendEmailButton);
            SendEmailButton.Click();
            Thread.Sleep(500);
            CloseSpinnerOnSurveyorDiv();
            CloseSpinnerOnSurveyorPage();
            Thread.Sleep(500);
            if (ErrorsList.Count > 0)
            {
                CatchError();
            }
        }
        public void CatchError()
        {
            try
            {   
               IList<string> errors = ErrorsList.Select(i => i.Text.ToString()).ToList();
                string error = ErrorsList[0].Text;
                Assert.IsTrue(error==null);
            }
            catch(Exception error)
            {
                throw error;
            }
        }
        //Sign off Building Control 
        public void InspectionUpdateMain()
        {
            SurveyorLoginPage.SelectSite();
            WaitForElementonSurveyor(SiteRiskAssessmentPage.SRAStatus);
            var siteRiskAssessment = SiteRiskAssessmentPage.SRAStatus.Text;
            WaitForElementonSurveyor(RefurbishmentAssessmentPage.RAStatus);
            var refurbishmentAssessment = RefurbishmentAssessmentPage.RAStatus.Text;
            WaitForElementonSurveyor(DesignReviewPage.DRStatus);
            var designReviewAssessment = DesignReviewPage.DRStatus.Text;
            if (siteRiskAssessment.Contains("Outstanding"))
            {
                SiteRiskAssessmentPage.SiteRiskAssessmentMain();
            }
            if (refurbishmentAssessment.Contains("Outstanding"))
            {
                RefurbishmentAssessmentPage.RefurbishmentAssessmentMain();
            }
            if (designReviewAssessment.Contains("Outstanding"))
            {
                DesignReviewPage.DesignReviewMain();
            }
            SurveyorLoginPage.SelectSite();
            WaitForElementonSurveyor(EngineerReviewPage.ERStatus);
            var engineerReviewAssessment = EngineerReviewPage.ERStatus.Text;
            if (engineerReviewAssessment.Contains("Outstanding"))
            {
                EngineerReviewPage.EngineerReviewMain();
            }
            SurveyorLoginPage.SelectSite();
            OpenSiteInspection();
            SurveyorDocs.CloseOpenDocs();
            Thread.Sleep(500);
            CloseSpinnerOnSurveyorDiv();
            CloseSpinnerOnSurveyorPage();
            Thread.Sleep(500);
            WaitForLoadElement(NextButton);
            NextButton.Click();
            Thread.Sleep(500);
            CloseSpinnerOnSurveyorDiv();
            CloseSpinnerOnSurveyorPage();
            Thread.Sleep(500);
            SignOffPlots();
            Thread.Sleep(500);
            CloseSpinnerOnSurveyorDiv();
            CloseSpinnerOnSurveyorPage();
            Thread.Sleep(500);
            WaitForLoadElement(NextButton);
            NextButton.Click();
            Thread.Sleep(500);
            CloseSpinnerOnSurveyorDiv();
            CloseSpinnerOnSurveyorPage();
            Thread.Sleep(500);
            QISQuestionsDetails();
            Thread.Sleep(500);
            CloseSpinnerOnSurveyorDiv();
            CloseSpinnerOnSurveyorPage();
            Thread.Sleep(500);
            AssessmentScoresSection();
            Thread.Sleep(500);
            CloseSpinnerOnSurveyorDiv();
            CloseSpinnerOnSurveyorPage();
            Thread.Sleep(500);
            WaitForLoadElements(SiteStatusDropdowns);
            Thread.Sleep(500);
            CloseSpinnerOnSurveyorDiv();
            CloseSpinnerOnSurveyorPage();
            Thread.Sleep(500);
            int siteStatusDropdowns = SiteStatusDropdowns.Count;
            Thread.Sleep(500);
            CloseSpinnerOnSurveyorDiv();
            CloseSpinnerOnSurveyorPage();
            Thread.Sleep(500);
            for (int i = 0; i <= SiteStatusDropdowns.Count; i++)
            {
                Thread.Sleep(500);
                CloseSpinnerOnSurveyorDiv();
                CloseSpinnerOnSurveyorPage();
                Thread.Sleep(500);
                IJavaScriptExecutor js = Driver as IJavaScriptExecutor;
                js.ExecuteScript("scroll(0,300);");
                WaitForElementonSurveyor(SiteStatusDropdowns[0]);
                SiteStatusDropdowns[0].Click();
                Thread.Sleep(500);
                WaitForElementonSurveyor(SiteStatusDropdownInput);
                SiteStatusDropdownInput.Click();
                Thread.Sleep(500);
                CloseSpinnerOnSurveyorDiv();
                CloseSpinnerOnSurveyorPage();
                Thread.Sleep(500);
            }
            Thread.Sleep(500);
            CloseSpinnerOnSurveyorDiv();
            CloseSpinnerOnSurveyorPage();
            Thread.Sleep(500);
            if (FollowUpType.Count>0)
            {
                Thread.Sleep(500);
                CloseSpinnerOnSurveyorDiv();
                CloseSpinnerOnSurveyorPage();
                Thread.Sleep(500);
                IJavaScriptExecutor js = Driver as IJavaScriptExecutor;
                js.ExecuteScript("scroll(0,300);");
                WaitForElementonSurveyor(FollowUpType[0]);
                FollowUpType[0].Click();
                Thread.Sleep(500);
                WaitForElementonSurveyor(SiteStatusDropdownInput);
                SiteStatusDropdownInput.Click();
                Thread.Sleep(500);
                CloseSpinnerOnSurveyorDiv();
                CloseSpinnerOnSurveyorPage();
                Thread.Sleep(500);
            }

            SaveSiteInspection();
        }
        public void InspectionUpdateForPartialFinalNotice()
        {
            SurveyorLoginPage.SelectSite();
            WaitForElementonSurveyor(SiteRiskAssessmentPage.SRAStatus);
            var siteRiskAssessment = SiteRiskAssessmentPage.SRAStatus.Text;
            WaitForElementonSurveyor(RefurbishmentAssessmentPage.RAStatus);
            var refurbishmentAssessment = RefurbishmentAssessmentPage.RAStatus.Text;
            WaitForElementonSurveyor(DesignReviewPage.DRStatus);
            var designReviewAssessment = DesignReviewPage.DRStatus.Text;
            if (siteRiskAssessment.Contains("Outstanding"))
            {
                SiteRiskAssessmentPage.SiteRiskAssessmentMain();
            }
            if (refurbishmentAssessment.Contains("Outstanding"))
            {
                RefurbishmentAssessmentPage.RefurbishmentAssessmentMain();
            }
            if (designReviewAssessment.Contains("Outstanding"))
            {
                DesignReviewPage.DesignReviewMain();
            }
            SurveyorLoginPage.SelectSite();
            WaitForElementonSurveyor(EngineerReviewPage.ERStatus);
            var engineerReviewAssessment = EngineerReviewPage.ERStatus.Text;
            if (engineerReviewAssessment.Contains("Outstanding"))
            {
                EngineerReviewPage.EngineerReviewMain();
            }
            SurveyorLoginPage.SelectSite();
            OpenSiteInspection();
            SurveyorDocs.CloseOpenDocs();
            Thread.Sleep(500);
            CloseSpinnerOnSurveyorDiv();
            CloseSpinnerOnSurveyorPage();
            Thread.Sleep(500);
            WaitForLoadElement(NextButton);
            NextButton.Click();
            Thread.Sleep(500);
            CloseSpinnerOnSurveyorDiv();
            CloseSpinnerOnSurveyorPage();
            Thread.Sleep(500);
            SignOffPartialPlots();
            Thread.Sleep(500);
            CloseSpinnerOnSurveyorDiv();
            CloseSpinnerOnSurveyorPage();
            Thread.Sleep(500);
            WaitForLoadElement(NextButton);
            NextButton.Click();
            Thread.Sleep(500);
            CloseSpinnerOnSurveyorDiv();
            CloseSpinnerOnSurveyorPage();
            Thread.Sleep(500);
            QISQuestionsDetails();
            Thread.Sleep(500);
            CloseSpinnerOnSurveyorDiv();
            CloseSpinnerOnSurveyorPage();
            Thread.Sleep(500);
            AssessmentScoresSection();
            Thread.Sleep(500);
            CloseSpinnerOnSurveyorDiv();
            CloseSpinnerOnSurveyorPage();
            Thread.Sleep(500);
            WaitForLoadElements(SiteStatusDropdowns);
            Thread.Sleep(500);
            CloseSpinnerOnSurveyorDiv();
            CloseSpinnerOnSurveyorPage();
            Thread.Sleep(500);
            int siteStatusDropdowns = SiteStatusDropdowns.Count;
            Thread.Sleep(500);
            CloseSpinnerOnSurveyorDiv();
            CloseSpinnerOnSurveyorPage();
            Thread.Sleep(500);
            for (int i = 0; i <= SiteStatusDropdowns.Count; i++)
            {
                Thread.Sleep(500);
                CloseSpinnerOnSurveyorDiv();
                CloseSpinnerOnSurveyorPage();
                Thread.Sleep(500);
                IJavaScriptExecutor js = Driver as IJavaScriptExecutor;
                js.ExecuteScript("scroll(0,300);");
                WaitForElementonSurveyor(SiteStatusDropdowns[0]);
                SiteStatusDropdowns[0].Click();
                Thread.Sleep(500);
                WaitForElementonSurveyor(SiteStatusDropdownInput);
                SiteStatusDropdownInput.Click();
                Thread.Sleep(500);
                CloseSpinnerOnSurveyorDiv();
                CloseSpinnerOnSurveyorPage();
                Thread.Sleep(500);
            }
            Thread.Sleep(500);
            CloseSpinnerOnSurveyorDiv();
            CloseSpinnerOnSurveyorPage();
            Thread.Sleep(500);
            if (FollowUpType.Count > 0)
            {
                Thread.Sleep(500);
                CloseSpinnerOnSurveyorDiv();
                CloseSpinnerOnSurveyorPage();
                Thread.Sleep(500);
                IJavaScriptExecutor js = Driver as IJavaScriptExecutor;
                js.ExecuteScript("scroll(0,300);");
                WaitForElementonSurveyor(FollowUpType[0]);
                FollowUpType[0].Click();
                Thread.Sleep(500);
                WaitForElementonSurveyor(SiteStatusDropdownInput);
                SiteStatusDropdownInput.Click();
                Thread.Sleep(500);
                CloseSpinnerOnSurveyorDiv();
                CloseSpinnerOnSurveyorPage();
                Thread.Sleep(500);
            }

            SaveSiteInspection();
        }
        public void QISQuestionsDetails()
        {
            Thread.Sleep(500);
            CloseSpinnerOnSurveyorDiv();
            CloseSpinnerOnSurveyorPage();
            Thread.Sleep(500);
            if (QISQuestions.Count>0)
            {
                WaitForLoadElements(QISQuestions);
                //Thread.Sleep(1000);
                for (int i = 0; i <= QISQuestionsDropdowns.Count; i++)
                {
                    IJavaScriptExecutor js = Driver as IJavaScriptExecutor;
                    js.ExecuteScript("scroll(0,300);");
                    Thread.Sleep(500);
                    CloseSpinnerOnSurveyorDiv();
                    CloseSpinnerOnSurveyorPage();
                    Thread.Sleep(500);              
                    QISQuestionsDropdowns[0].Click();
                    Thread.Sleep(500);
                    CloseSpinnerOnSurveyorDiv();
                    CloseSpinnerOnSurveyorPage();
                    Thread.Sleep(500);
                    WaitForElementonSurveyor(QISQuestionsDropdownInputs);
                    QISQuestionsDropdownInputs.Click();
                    Thread.Sleep(500);
                    CloseSpinnerOnSurveyorDiv();
                    CloseSpinnerOnSurveyorPage();
                    Thread.Sleep(500);
                }
            }
        }

        public void SignOffPlots()
        {
            for (int i = 0; i < InspectedPlots.Count; i++)
            {
                if (InspectedPlots[i].Enabled == true)
                {
                    if (InspectedPlots[i].Selected == false)
                    {
                        InspectedPlots[i].Click();

                        if (SurveyorDocs.Defects == 0)
                        {
                            if (SignoffBC.Count > 0)
                            {
                                SelectSignOffBCCheckBoxes(i);
                                CloseSpinnerOnSurveyorDiv();
                                CloseSpinnerOnSurveyorPage();
                            }
                            if (SignOffWarranty.Count > 0)
                            {
                                SelectSignOffWarrentyCheckBoxes(i);
                                CloseSpinnerOnSurveyorDiv();
                                CloseSpinnerOnSurveyorPage();
                            }
                        }
                        else
                        {
                            WaitForElementonSurveyor(OpenDefects);
                            Assert.IsTrue(OpenDefects.Displayed);
                        }
                        Thread.Sleep(500);
                        CloseSpinnerOnSurveyorDiv();
                        CloseSpinnerOnSurveyorPage();
                        Thread.Sleep(500);
                    }
                }
            }
            Thread.Sleep(500);
            CloseSpinnerOnSurveyorDiv();
            CloseSpinnerOnSurveyorPage();
            Thread.Sleep(500);
            //WaitForLoadElement(UpdatedStagesOfWorks);
            //WaitForElementToClick(UpdatedStagesOfWorks);
            //UpdatedStagesOfWorks.Click();
            ////Thread.Sleep(500);
            //CloseSpinnerOnSurveyorDiv();
            //CloseSpinnerOnSurveyorPage();
            ////Thread.Sleep(500);
            //WaitForLoadElement(UpdatedStagesOfWorksInput);
            //UpdatedStagesOfWorksInput.Click();
            ////Thread.Sleep(500);
            //CloseSpinnerOnSurveyorDiv();
            //CloseSpinnerOnSurveyorPage();
            ////Thread.Sleep(500);
            WaitForLoadElement(PGHandoverComment);
            PGHandoverComment.SendKeys("Signing Off Building Control");
            WaitForLoadElement(PGHandoverFutureGuidance);
            PGHandoverFutureGuidance.SendKeys("FutureGuidance of Building Control");
        }

        public void SignOffPartialPlots()
        {
            for (int i = 0; i < InspectedPlots.Count-1; i++)
            {
                if (InspectedPlots[i].Enabled == true)
                {
                    if (InspectedPlots[i].Selected == false)
                    {
                        InspectedPlots[i].Click();

                        if (SurveyorDocs.Defects == 0)
                        {
                            if (SignoffBC.Count > 0)
                            {
                                SelectSignOffBCCheckBoxes(i);
                                CloseSpinnerOnSurveyorDiv();
                                CloseSpinnerOnSurveyorPage();
                            }
                            if (SignOffWarranty.Count > 0)
                            {
                                SelectSignOffWarrentyCheckBoxes(i);
                                CloseSpinnerOnSurveyorDiv();
                                CloseSpinnerOnSurveyorPage();
                            }
                        }
                        else
                        {
                            WaitForElementonSurveyor(OpenDefects);
                            Assert.IsTrue(OpenDefects.Displayed);
                        }
                        Thread.Sleep(500);
                        CloseSpinnerOnSurveyorDiv();
                        CloseSpinnerOnSurveyorPage();
                        Thread.Sleep(500);
                    }
                }
            }
            Thread.Sleep(500);
            CloseSpinnerOnSurveyorDiv();
            CloseSpinnerOnSurveyorPage();
            Thread.Sleep(500);
            //WaitForLoadElement(UpdatedStagesOfWorks);
            //WaitForElementToClick(UpdatedStagesOfWorks);
            //UpdatedStagesOfWorks.Click();
            ////Thread.Sleep(500);
            //CloseSpinnerOnSurveyorDiv();
            //CloseSpinnerOnSurveyorPage();
            ////Thread.Sleep(500);
            //WaitForLoadElement(UpdatedStagesOfWorksInput);
            //UpdatedStagesOfWorksInput.Click();
            ////Thread.Sleep(500);
            //CloseSpinnerOnSurveyorDiv();
            //CloseSpinnerOnSurveyorPage();
            ////Thread.Sleep(500);
            WaitForLoadElement(PGHandoverComment);
            PGHandoverComment.SendKeys("Signing Off Building Control");
            WaitForLoadElement(PGHandoverFutureGuidance);
            PGHandoverFutureGuidance.SendKeys("FutureGuidance of Building Control");
        }
        public void SelectSignOffBCCheckBoxes(int i)
        {
            if (SignoffBCLabel[i].Enabled == true)
            {
                if (SignoffBCLabel[i].Selected == false)
                {
                    SignoffBCLabel[i].Click();
                    Thread.Sleep(500);
                    CloseSpinnerOnSurveyorDiv();
                    CloseSpinnerOnSurveyorPage();
                    Thread.Sleep(500);
                }
            }

        }
        public void SelectSignOffWarrentyCheckBoxes(int i)
        {
            if (SignOffWarranty[i].Enabled == true)
            {
                if (SignOffWarranty[i].Selected == false)
                {
                    SignOffWarranty[i].Click();
                    Thread.Sleep(500);
                    CloseSpinnerOnSurveyorDiv();
                    CloseSpinnerOnSurveyorPage();
                    Thread.Sleep(500);
                }
            }
        }
        public void AssessmentScoresSection()
        {
            IJavaScriptExecutor js = Driver as IJavaScriptExecutor;
            js.ExecuteScript("arguments[0].scrollIntoView(true);", AssessmentandScores);
            foreach (var eachRatingStar in RatingStars)
            {
                eachRatingStar.Click();
            }

            WaitForElements(RatingComment);
            foreach (var eachComment in RatingComment)
            {
                eachComment.Click();
                eachComment.Clear();
                eachComment.SendKeys("GoodRate");
            }
        }
        public void SubmitSiteInspectionMethod()
        {
            SurveyorLoginPage.SelectSite();

            SiteInspectionPage.InspectionUpdateMain();
        }
    }
}
