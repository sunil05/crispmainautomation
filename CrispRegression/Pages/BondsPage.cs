﻿using CrispAutomation.Support;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.PageObjects;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace CrispAutomation.Pages
{
    public class BondsPage : Support.Pages
    {
        public IWebDriver wdriver;
        public BondsPage(ISearchContext driver)
        {
            PageFactory.InitElements(driver, this);
            wdriver = (RemoteWebDriver)driver;
        }

        [FindsBy(How = How.XPath, Using = "//crisp-action-button[@click.delegate='addBond()']//div//a//i")]
        public IWebElement AddBondButton;

        [FindsBy(How = How.XPath, Using = "//crisp-dialog//div[contains(@class,'au-target modal dialog')]/crisp-header//nav//div[text()='Add new bond']")]
        public IWebElement BondDialogueWindow;

        [FindsBy(How = How.XPath, Using = "//crisp-input-radio[@label='Product']//ul//li[contains(@id,'crisp-input-radio')]//label[contains(@for,'crisp-input-radio-group')][contains(text(),'Road Bond')]")]
        public IList<IWebElement> RoadBondProducts;

        [FindsBy(How = How.XPath, Using = "//crisp-input-radio[@label='Product']//ul//li[contains(@id,'crisp-input-radio')]//label[contains(@for,'crisp-input-radio-group')][contains(text(),'Sewer Bond')]")]
        public IList<IWebElement> SewerBondProducts;

        [FindsBy(How = How.XPath, Using = "//crisp-input-radio[@label='Cover']//ul//li[contains(@id,'crisp-input-radio')]//label[contains(@for,'crisp-input-radio-group')]")]
        public IList<IWebElement> CoverList;

        [FindsBy(How = How.XPath, Using = "//crisp-input-object//div[@class='au-target input-field']//label[text()='Site Address']")]
        public IWebElement SiteAddressLabel;

        [FindsBy(How = How.XPath, Using = "//crisp-input-object//div[@class='au-target input-field']//label[text()='Applicant'][@class='au-target active']")]
        public IList<IWebElement> ApplicantField;

        [FindsBy(How = How.XPath, Using = "//crisp-input-object//div[@class='au-target input-field']//label[text()='Applicant']")]
        public IWebElement ApplicantLabel;

        [FindsBy(How = How.CssSelector, Using = "crisp-input-text[label = 'Search'] > div > label")]
        public IWebElement SearchLabel;

        [FindsBy(How = How.CssSelector, Using = "crisp-input-text[label = 'Search'] > div > input")]
        public IWebElement SearchInput;

        [FindsBy(How = How.XPath, Using = "//contact-list/crisp-list[@ref='listElm']/ul[@ref='theList']/li[@class='au-target collection-item']//a[@class='au-target row list-item-contents clickable hasAvatar']")]
        public IList<IWebElement> Applicant;

        [FindsBy(How = How.XPath, Using = "//crisp-input-object//div[@class='au-target input-field']//label[text()='Beneficiary'][@class='au-target active']")]
        public IList<IWebElement> BeneficiaryField;

        [FindsBy(How = How.XPath, Using = "//crisp-input-object//div[@class='au-target input-field']//label[text()='Beneficiary']")]
        public IWebElement BeneficiaryLabel;

        [FindsBy(How = How.XPath, Using = "//contact-list/crisp-list[@ref='listElm']/ul[@ref='theList']/li[@class='au-target collection-item']//a[@class='au-target row list-item-contents clickable hasAvatar']")]
        public IList<IWebElement> Beneficiary;

        [FindsBy(How = How.XPath, Using = "//crisp-input-object//div[@class='au-target input-field']//label[text()='Bond recipient'][contains(@class,'au-target')]//parent::div//span[contains(@class,'au-target input-container')]")]
        public IList<IWebElement> BondRecipientField;

        [FindsBy(How = How.XPath, Using = "//crisp-wizard-step[@step-title='Bond details']//div//crisp-card//crisp-card-content//crisp-input-object//div[@class='au-target input-field']//label[text()='Bond recipient']")]
        public IWebElement BondRecipientLabel;

        [FindsBy(How = How.XPath, Using = "//crisp-list[@ref='listElm']//ul[@ref='theList']//li[@class='au-target collection-item']//crisp-list-item//a[contains(@class,'clickable hasAvatar')]")]
        public IList<IWebElement> BondRecipientList;

        [FindsBy(How = How.XPath, Using = "//crisp-footer//nav//div//crisp-footer-button[@click.delegate='goForward()']//button[@class='au-target waves-effect waves-light btn-flat']")]
        public IWebElement BondNextButton;

        //Bond Parts Page Elements

        [FindsBy(How = How.XPath, Using = "//crisp-wizard-step[@step-title='Bond parts']//crisp-input-date[@label='Anticipated release date']//div[@class='au-target input-field']//label[text()='Anticipated release date']")]
        public IWebElement ReleaseDate;

        [FindsBy(How = How.XPath, Using = "//div[contains(@class,'picker--opened')]//div[@class='picker__box']//div[@class='picker__footer']//button[contains(@class,'picker__today')]")]
        public IWebElement ReleaseDateInput;

        [FindsBy(How = How.XPath, Using = "//crisp-wizard-step[@step-title='Bond parts']//crisp-input-currency[@label='Remaining amount']//label[text()='Remaining amount']")]
        public IWebElement RemainingAmountLabel;

        [FindsBy(How = How.XPath, Using = "//crisp-wizard-step[@step-title='Bond parts']//crisp-input-currency[@label='Remaining amount']//input")]
        public IWebElement RemainingAmountInput;

        //Other Details Page 

        [FindsBy(How = How.XPath, Using = "//crisp-wizard-step[@step-title='Other details']//crisp-input-text[@label='Description of works']//div//label")]
        public IWebElement DescriptionOfWork;

        [FindsBy(How = How.XPath, Using = "//crisp-wizard-step[@step-title='Other details']//crisp-input-text[@label='Description of works']//div//input")]
        public IWebElement DescriptionOfWorkInput;

        [FindsBy(How = How.XPath, Using = "//crisp-wizard-step[@step-title='Other details']//crisp-input-textarea[@label='Notes']//div//label")]
        public IWebElement NotesLabel;

        [FindsBy(How = How.XPath, Using = "//crisp-wizard-step[@step-title='Other details']//crisp-input-textarea[@label='Notes']//div//textarea")]
        public IWebElement Notesinput;

        [FindsBy(How = How.XPath, Using = "//crisp-wizard-step[@step-title='Other details']//crisp-input-bool[@label='Has been signed?']//label")]
        public IWebElement HasbeenSignedCheckBox;

        [FindsBy(How = How.XPath, Using = "//crisp-wizard-step[@step-title='Other details']//crisp-input-currency//div[@class='input-field']//label")]
        public IWebElement StartValueLabel;

        [FindsBy(How = How.XPath, Using = "//crisp-wizard-step[@step-title='Other details']//crisp-input-currency//div[@class='input-field']//input")]
        public IWebElement StartValueInput;

        [FindsBy(How = How.XPath, Using = "//crisp-wizard-step[@step-title='Other details']//crisp-slider//input")]
        public IWebElement Slider;

        [FindsBy(How = How.XPath, Using = "//crisp-wizard-step[@step-title='Other details']//crisp-card-content//crisp-input-number[@label='Number of units serviced']//label")]
        public IWebElement UnitesServicedLabel;

        [FindsBy(How = How.XPath, Using = "//crisp-wizard-step[@step-title='Other details']//crisp-card-content//crisp-input-number[@label='Number of units serviced']//input")]
        public IWebElement UnitesServicedInput;

        [FindsBy(How = How.XPath, Using = "//crisp-wizard-step[@step-title='Other details']//crisp-card-content//crisp-input-date[@label='Anticipated release date']//label")]
        public IWebElement AnticipatedReleaseDateLabel;

        [FindsBy(How = How.XPath, Using = "//div[contains(@class,'picker--opened')]//div[@class='picker__box']//div[@class='picker__footer']//button[contains(@class,'picker__today')]")]
        public IWebElement AnticipatedReleaseDateInput;

        [FindsBy(How = How.XPath, Using = "//crisp-wizard-step[@step-title='Other details']//crisp-card-content//crisp-input-number[@label='Maintenance period']//label")]
        public IWebElement MaintenancePeriodLabel;

        [FindsBy(How = How.XPath, Using = "//crisp-wizard-step[@step-title='Other details']//crisp-card-content//crisp-input-number[@label='Maintenance period']//input")]
        public IWebElement MaintenancePeriodInput;

        [FindsBy(How = How.XPath, Using = "//crisp-footer//nav//div//crisp-footer-button[@click.delegate='ok()']//button[@class='au-target waves-effect waves-light btn-flat']")]
        public IWebElement BondOkButton;

        //Site Address Elements 

        [FindsBy(How = How.XPath, Using = "//div[@class='card-content']//crisp-input-text[@label='Address Line 1']//div//label[@class='au-target']")]
        public IWebElement AddressLine1Label;

        [FindsBy(How = How.XPath, Using = "//div[@class='card-content']//crisp-input-text[@label='Address Line 1']//div//input")]
        public IWebElement AddressLine1Input;

        [FindsBy(How = How.XPath, Using = "//div[@class='card-content']//crisp-input-text[@label='Town']//div//label[@class='au-target']")]
        public IWebElement TownLabel;

        [FindsBy(How = How.XPath, Using = "//div[@class='card-content']//crisp-input-text[@label='Town']//div//input")]
        public IWebElement TownInput;

        [FindsBy(How = How.XPath, Using = "//div[@class='card-content']//crisp-input-text[@label='Postcode']//div//label[@class='au-target']")]
        public IWebElement SitePostcodeLabel;

        [FindsBy(How = How.XPath, Using = "//div[@class='card-content']//crisp-input-text[@label='Postcode']//div//input")]
        public IWebElement SitePostcodeInput;

        [FindsBy(How = How.XPath, Using = "//crisp-button/span/button[@class='au-target waves-effect waves-light btn'][text()='Set Address']")]
        public IWebElement SetAddressButton;

        [FindsBy(How = How.XPath, Using = "//crisp-header-button[@click.delegate='showSiteDetails()']//span//button[@class='au-target waves-effect waves-light btn-flat']")]
        public IWebElement SiteDetailsButton;


        //Conditions Checking 
        [FindsBy(How = How.XPath, Using = "//crisp-input-object//div[@class='au-target input-field']//label[text()='Applicant'][@class='au-target active']//following-sibling::i[@click.delegate='clear()']")]
        public IList<IWebElement> ApplicantClear;

        [FindsBy(How = How.XPath, Using = "//crisp-input-object//div[@class='au-target input-field']//label[text()='Beneficiary'][@class='au-target active']//following-sibling::i[@click.delegate='clear()']")]
        public IList<IWebElement> BeneficiaryClear;

        [FindsBy(How = How.XPath, Using = "//crisp-list[@list-title='Conditions']//ul//li[@class='au-target collection-item']//div[@class='title']")]
        public IList<IWebElement> ConditionsTitles;

        [FindsBy(How = How.XPath, Using = "//crisp-header[@text.bind='bond.reference']//crisp-header-actions//crisp-header-button[@click.delegate='edit()']//button")]
        public IWebElement BondEditButton;

        [FindsBy(How = How.XPath, Using = "//crisp-header[@text.bind='bond.reference']//crisp-header-actions//crisp-header-button[@click.delegate='chase()']//button")]
        public IWebElement BondChaseButton;

        [FindsBy(How = How.XPath, Using = "//div[@class='modal-content']//correspondence-embed")]
        public IWebElement BondChaseDialogueWindow;

        [FindsBy(How = How.XPath, Using = "//crisp-button[@click.delegate='send()']//button")]
        public IWebElement BondChaseSendButton;

        [FindsBy(How = How.XPath, Using = "//crisp-list[@list-title='Bonds']//ul/li[@class='au-target collection-item']//crisp-list-item//a//crisp-list-item-title//div[@class='title']//small")]
        public IWebElement BondStatus;

        //Accepting and Rejecting Bond Elements 


        [FindsBy(How = How.XPath, Using = "//crisp-header-button[@click.delegate='makeDecision()']//span//button[@class='au-target waves-effect waves-light btn-flat']")]
        public IWebElement MakeDecesionButton;

        [FindsBy(How = How.XPath, Using = "//crisp-wizard//crisp-wizard-step[@step-title='Bond']//crisp-input-radio//div//ul//li//label[text()='Accept']")]
        public IWebElement AcceptBondButton;

        [FindsBy(How = How.XPath, Using = "//crisp-wizard//crisp-wizard-step[@step-title='Bond']//crisp-input-radio//div//ul//li//label[text()='Reject']")]
        public IWebElement RejectBondButton;

        [FindsBy(How = How.XPath, Using = "//crisp-wizard//crisp-wizard-step[@step-title='Decision details']//crisp-input-file[@label='Bond file']//div//input[@ref='fileInput']")]
        public IWebElement BondFileInput;

        [FindsBy(How = How.XPath, Using = "//crisp-wizard//crisp-wizard-step[@step-title='Decision details']//crisp-input-object//label[text()='Signed by']")]
        public IWebElement SignedByLabel;

        [FindsBy(How = How.XPath, Using = "//crisp-card-content//contact-list//crisp-list[@ref='listElm']//ul[@ref='theList']//li[@class='au-target collection-item']")]
        public IList<IWebElement> ContactList;

        [FindsBy(How = How.XPath, Using = "//crisp-wizard//crisp-wizard-step[@step-title='Decision details']//crisp-picker[@label='Insurance Agreement Group']//div//input")]
        public IWebElement InsuranceAgreementLabel;

        [FindsBy(How = How.XPath, Using = "//ul[@class='dropdown-content select-dropdown active']//li//span[text()='AmTrust']")]
        public IWebElement AmtrustAgreement;

        [FindsBy(How = How.XPath, Using = "//crisp-wizard-step[@step-title='Decision details']//crisp-card-content//crisp-input-radio[@label='Insurance agreement']//ul//li[@class='au-target']//label")]
        public IList<IWebElement> InsuranceAgreement;

        [FindsBy(How = How.XPath, Using = "//crisp-wizard-step[@step-title='Send']//correspondence-embed//crisp-tabs//div//correspondence-email//div//crisp-input-html")]
        public IWebElement CorrespondenceEmail;

        [FindsBy(How = How.XPath, Using = "//crisp-footer-button[@click.delegate='send()']//button")]
        public IWebElement BondAcceptSendButton;

        [FindsBy(How = How.XPath, Using = "//crisp-wizard-step[@step-title='Decision details']//crisp-card//crisp-card-content//div[@class='card-content']//crisp-input-bool[@value.bind='selectedRejectionReasons & validate']//label")]
        public IList<IWebElement> BondRejectionReasons;

        [FindsBy(How = How.XPath, Using = " //crisp-wizard-step[@step-title='Decision details']//crisp-input-textarea[@label='Comments']//label")]
        public IWebElement RejectionCommentLabel;

        [FindsBy(How = How.XPath, Using = "//crisp-wizard-step[@step-title='Decision details']//crisp-input-textarea[@label='Comments']//textarea")]
        public IWebElement RejectionCommentInput;

        [FindsBy(How = How.XPath, Using = "//crisp-wizard-step[@step-title='Decision details']//crisp-input-textarea[@label='Other reason comments']//label")]
        public IWebElement RejectionOtherReasonCommentLabel;

        [FindsBy(How = How.XPath, Using = "//crisp-wizard-step[@step-title='Decision details']//crisp-input-textarea[@label='Other reason comments']//textarea")]
        public IWebElement RejectionOtherReasonCommentInput;
        //Complete Part Elements

        [FindsBy(How = How.XPath, Using = "//crisp-header-button[@click.delegate='completePart()']//span//button[@class='au-target waves-effect waves-light btn-flat']")]
        public IWebElement CompletePartButton;

        [FindsBy(How = How.XPath, Using = "//crisp-footer-button[@click.delegate='send()']//button")]
        public IWebElement BondCompleteSendButton;

        public void CreateRoadBond()
        {
            WaitForElement(AddBondButton);
            AddBondButton.Click();
            //Thread.Sleep(1000);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(1000);
            WaitForElement(BondDialogueWindow);
            Assert.IsTrue(BondDialogueWindow.Displayed);
            RoadBondsDetailsPage();
            VerifyBondSubmittedStatus();
        }

        public void CreateSewerBond()
        {
            WaitForElement(AddBondButton);
            AddBondButton.Click();
            //Thread.Sleep(1000);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(1000);
            WaitForElement(BondDialogueWindow);
            Assert.IsTrue(BondDialogueWindow.Displayed);
            SewerBondsDetailsPage();
            VerifyBondSubmittedStatus();
        }

        public void CreateConditionsOnRoadBond()
        {
            WaitForElement(AddBondButton);
            AddBondButton.Click();
            //Thread.Sleep(1000);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(1000);
            WaitForElement(BondDialogueWindow);
            Assert.IsTrue(BondDialogueWindow.Displayed);
            RoadBondConditionsOnDetails();
            VerifyBondInCompleteStatus();
        }

        public void RoadBondsDetailsPage()
        {
            WaitForElements(RoadBondProducts);
            if (RoadBondProducts.Count > 0)
            {
                //Thread.Sleep(1000);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                //Thread.Sleep(1000);
                int BondProductsList = RoadBondProducts.Count;
                for (int i = 0; i < BondProductsList; i++)
                {
                    if (i != 0)
                    {
                        WaitForElement(SiteDetailsButton);
                        SiteDetailsButton.Click();
                        WaitForElement(AddBondButton);
                        AddBondButton.Click();
                        //Thread.Sleep(1000);
                        CloseSpinneronDiv();
                        CloseSpinneronPage();
                        //Thread.Sleep(1000);
                        WaitForElement(BondDialogueWindow);
                        Assert.IsTrue(BondDialogueWindow.Displayed);
                        WaitForElement(RoadBondProducts[i]);
                        RoadBondProducts[i].Click();
                        //Thread.Sleep(1000);
                        CloseSpinneronDiv();
                        CloseSpinneronPage();
                        //Thread.Sleep(1000);
                        if (CoverList.Count > 0)
                        {
                            int CoversList = CoverList.Count;
                            for (int j = 0; j < CoversList; j++)
                            {
                                if (j != 0)
                                {
                                    WaitForElement(SiteDetailsButton);
                                    SiteDetailsButton.Click();
                                    WaitForElement(AddBondButton);
                                    AddBondButton.Click();
                                    //Thread.Sleep(1000);
                                    CloseSpinneronDiv();
                                    CloseSpinneronPage();
                                    //Thread.Sleep(1000);
                                    WaitForElement(BondDialogueWindow);
                                    Assert.IsTrue(BondDialogueWindow.Displayed);
                                    WaitForElement(RoadBondProducts[i]);
                                    RoadBondProducts[i].Click();
                                    //Thread.Sleep(1000);
                                    CloseSpinneronDiv();
                                    CloseSpinneronPage();
                                    //Thread.Sleep(1000);
                                    WaitForElement(CoverList[j]);
                                    CoverList[j].Click();
                                    //Thread.Sleep(1000);
                                    CloseSpinneronDiv();
                                    CloseSpinneronPage();
                                    //Thread.Sleep(1000);
                                    BondSiteAddress();
                                    DetailsPage();
                                    BondPartsPage();
                                    OtherDetails();
                                    //Thread.Sleep(1000);
                                    CloseSpinneronDiv();
                                    CloseSpinneronPage();
                                    //Thread.Sleep(1000);
                                }
                                else
                                {
                                    WaitForElement(CoverList[j]);
                                    CoverList[j].Click();
                                    //Thread.Sleep(1000);
                                    CloseSpinneronDiv();
                                    CloseSpinneronPage();
                                    //Thread.Sleep(1000);
                                    BondSiteAddress();
                                    DetailsPage();
                                    BondPartsPage();
                                    OtherDetails();
                                    //Thread.Sleep(1000);
                                    CloseSpinneronDiv();
                                    CloseSpinneronPage();
                                    //Thread.Sleep(1000);
                                }
                            }
                        }
                    }
                    else
                    {
                        WaitForElement(RoadBondProducts[i]);
                        RoadBondProducts[i].Click();
                        //Thread.Sleep(1000);
                        CloseSpinneronDiv();
                        CloseSpinneronPage();
                        //Thread.Sleep(1000);
                        if (CoverList.Count > 0)
                        {
                            int CoversList = CoverList.Count;
                            for (int j = 0; j < CoversList; j++)
                            {
                                if (j != 0)
                                {
                                    WaitForElement(SiteDetailsButton);
                                    SiteDetailsButton.Click();
                                    WaitForElement(AddBondButton);
                                    AddBondButton.Click();
                                    //Thread.Sleep(1000);
                                    CloseSpinneronDiv();
                                    CloseSpinneronPage();
                                    //Thread.Sleep(1000);
                                    WaitForElement(BondDialogueWindow);
                                    Assert.IsTrue(BondDialogueWindow.Displayed);
                                    WaitForElement(RoadBondProducts[i]);
                                    RoadBondProducts[i].Click();
                                    //Thread.Sleep(1000);
                                    CloseSpinneronDiv();
                                    CloseSpinneronPage();
                                    //Thread.Sleep(1000);
                                    WaitForElement(CoverList[j]);
                                    CoverList[j].Click();
                                    //Thread.Sleep(1000);
                                    CloseSpinneronDiv();
                                    CloseSpinneronPage();
                                    //Thread.Sleep(1000);
                                    BondSiteAddress();
                                    DetailsPage();
                                    BondPartsPage();
                                    OtherDetails();
                                    //Thread.Sleep(1000);
                                    CloseSpinneronDiv();
                                    CloseSpinneronPage();
                                    //Thread.Sleep(1000);
                                }
                                else
                                {
                                    WaitForElement(CoverList[j]);
                                    CoverList[j].Click();
                                    //Thread.Sleep(1000);
                                    CloseSpinneronDiv();
                                    CloseSpinneronPage();
                                    //Thread.Sleep(1000);
                                    BondSiteAddress();
                                    DetailsPage();
                                    BondPartsPage();
                                    OtherDetails();
                                    //Thread.Sleep(1000);
                                    CloseSpinneronDiv();
                                    CloseSpinneronPage();
                                    //Thread.Sleep(1000);
                                }
                            }
                        }
                    }
                }
            }
        }

        public void SewerBondsDetailsPage()
        {
            WaitForElements(SewerBondProducts);
            if (SewerBondProducts.Count > 0)
            {
                //Thread.Sleep(1000);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                //Thread.Sleep(1000);
                int BondProductsList = SewerBondProducts.Count;
                for (int i = 0; i < BondProductsList; i++)
                {
                    if (i != 0)
                    {
                        WaitForElement(SiteDetailsButton);
                        SiteDetailsButton.Click();
                        WaitForElement(AddBondButton);
                        AddBondButton.Click();
                        //Thread.Sleep(1000);
                        CloseSpinneronDiv();
                        CloseSpinneronPage();
                        //Thread.Sleep(1000);
                        WaitForElement(BondDialogueWindow);
                        Assert.IsTrue(BondDialogueWindow.Displayed);
                        WaitForElement(SewerBondProducts[i]);
                        SewerBondProducts[i].Click();
                        //Thread.Sleep(1000);
                        CloseSpinneronDiv();
                        CloseSpinneronPage();
                        //Thread.Sleep(1000);
                        if (CoverList.Count > 0)
                        {
                            int CoversList = CoverList.Count;
                            for (int j = 0; j < CoversList; j++)
                            {
                                if (j != 0)
                                {
                                    WaitForElement(SiteDetailsButton);
                                    SiteDetailsButton.Click();
                                    WaitForElement(AddBondButton);
                                    AddBondButton.Click();
                                    //Thread.Sleep(1000);
                                    CloseSpinneronDiv();
                                    CloseSpinneronPage();
                                    //Thread.Sleep(1000);
                                    WaitForElement(BondDialogueWindow);
                                    Assert.IsTrue(BondDialogueWindow.Displayed);
                                    WaitForElement(SewerBondProducts[i]);
                                    SewerBondProducts[i].Click();
                                    //Thread.Sleep(1000);
                                    CloseSpinneronDiv();
                                    CloseSpinneronPage();
                                    //Thread.Sleep(1000);
                                    WaitForElement(CoverList[j]);
                                    CoverList[j].Click();
                                    //Thread.Sleep(1000);
                                    CloseSpinneronDiv();
                                    CloseSpinneronPage();
                                    //Thread.Sleep(1000);
                                    BondSiteAddress();
                                    DetailsPage();
                                    OtherDetails();
                                    //Thread.Sleep(1000);
                                    CloseSpinneronDiv();
                                    CloseSpinneronPage();
                                    //Thread.Sleep(1000);
                                }
                                else
                                {
                                    WaitForElement(CoverList[j]);
                                    CoverList[j].Click();
                                    //Thread.Sleep(1000);
                                    CloseSpinneronDiv();
                                    CloseSpinneronPage();
                                    //Thread.Sleep(1000);
                                    BondSiteAddress();
                                    DetailsPage();
                                    OtherDetails();
                                    //Thread.Sleep(1000);
                                    CloseSpinneronDiv();
                                    CloseSpinneronPage();
                                    //Thread.Sleep(1000);
                                }
                            }
                        }
                    }
                    else
                    {
                        WaitForElement(SewerBondProducts[i]);
                        SewerBondProducts[i].Click();
                        //Thread.Sleep(1000);
                        CloseSpinneronDiv();
                        CloseSpinneronPage();
                        //Thread.Sleep(1000);
                        if (CoverList.Count > 0)
                        {
                            int CoversList = CoverList.Count;
                            for (int j = 0; j < CoversList; j++)
                            {
                                if (j != 0)
                                {
                                    WaitForElement(SiteDetailsButton);
                                    SiteDetailsButton.Click();
                                    WaitForElement(AddBondButton);
                                    AddBondButton.Click();
                                    //Thread.Sleep(1000);
                                    CloseSpinneronDiv();
                                    CloseSpinneronPage();
                                    //Thread.Sleep(1000);
                                    WaitForElement(BondDialogueWindow);
                                    Assert.IsTrue(BondDialogueWindow.Displayed);
                                    WaitForElement(SewerBondProducts[i]);
                                    SewerBondProducts[i].Click();
                                    //Thread.Sleep(1000);
                                    CloseSpinneronDiv();
                                    CloseSpinneronPage();
                                    //Thread.Sleep(1000);
                                    WaitForElement(CoverList[j]);
                                    CoverList[j].Click();
                                    //Thread.Sleep(1000);
                                    CloseSpinneronDiv();
                                    CloseSpinneronPage();
                                    //Thread.Sleep(1000);
                                    BondSiteAddress();
                                    DetailsPage();
                                    OtherDetails();
                                    //Thread.Sleep(1000);
                                    CloseSpinneronDiv();
                                    CloseSpinneronPage();
                                    //Thread.Sleep(1000);
                                }
                                else
                                {
                                    WaitForElement(CoverList[j]);
                                    CoverList[j].Click();
                                    //Thread.Sleep(1000);
                                    CloseSpinneronDiv();
                                    CloseSpinneronPage();
                                    //Thread.Sleep(1000);
                                    BondSiteAddress();
                                    DetailsPage();
                                    OtherDetails();
                                    //Thread.Sleep(1000);
                                    CloseSpinneronDiv();
                                    CloseSpinneronPage();
                                    //Thread.Sleep(1000);
                                }
                            }
                        }
                    }
                }
            }
        }


        public void DetailsPage()
        {            
            if (ApplicantField.Count == 0)
            {
                WaitForElement(ApplicantLabel);
                ApplicantLabel.Click();
                WaitForElement(SearchLabel);
                SearchLabel.Click();
                //Thread.Sleep(500);
                WaitForElement(SearchInput);
                SearchInput.Clear();
                SearchInput.SendKeys("Aberdeen ");
                //Thread.Sleep(500);
                SearchInput.SendKeys("City ");
                //Thread.Sleep(500);
                SearchInput.SendKeys("Council");
                //Thread.Sleep(500);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                Thread.Sleep(500);
                WaitForElements(Applicant);
                if (Applicant.Count > 0)
                {
                    CloseSpinneronDiv();
                    Thread.Sleep(500);
                    WaitForElement(Applicant[0]);
                    WaitForElementToClick(Applicant[0]);
                    Applicant[0].Click();
                    Thread.Sleep(500);
                    CloseSpinneronDiv();
                    CloseSpinneronPage();
                    Thread.Sleep(500);
                }
            }

            if (BeneficiaryField.Count == 0)
            {
                WaitForElement(BeneficiaryLabel);
                BeneficiaryLabel.Click();
                WaitForElement(SearchLabel);
                SearchLabel.Click();
                //Thread.Sleep(500);
                WaitForElement(SearchInput);
                SearchInput.Clear();
                SearchInput.SendKeys("Aberdeen ");
                //Thread.Sleep(500);
                SearchInput.SendKeys("City ");
                //Thread.Sleep(500);
                SearchInput.SendKeys("Council");
                //Thread.Sleep(500);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                //Thread.Sleep(500);
                WaitForElements(Beneficiary);
                if (Beneficiary.Count > 0)
                {
                    CloseSpinneronDiv();
                    Thread.Sleep(500);
                    WaitForElement(Beneficiary[0]);
                    WaitForElementToClick(Beneficiary[0]);
                    Beneficiary[0].Click();
                    Thread.Sleep(500);                  
                    CloseSpinneronDiv();
                    CloseSpinneronPage();
                    Thread.Sleep(500);
                }
            }
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);           
            if (BondRecipientField.Count > 0)
            {             
                WaitForElement(BondRecipientLabel);
                WaitForElementToClick(BondRecipientLabel);
                BondRecipientLabel.Click();
                Thread.Sleep(500);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                //Thread.Sleep(500);
               // WaitForElements(BondRecipientList);
                if (BondRecipientList.Count > 0)
                {
                    CloseSpinneronDiv();
                    Thread.Sleep(500);
                    WaitForElement(BondRecipientList[0]);
                    WaitForElementToClick(BondRecipientList[0]);                   
                    BondRecipientList[0].Click();
                    Thread.Sleep(500);                   
                    CloseSpinneronDiv();
                    CloseSpinneronPage();                   
                }
            }
            WaitForElement(BondNextButton);
            BondNextButton.Click();
        }


        public void BondPartsPage()
        {
            //Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(500);
            WaitForElement(ReleaseDate);
            ReleaseDate.Click();
            //Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(500);
            WaitForElement(ReleaseDateInput);
            ReleaseDateInput.Click();
            //Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(500);
            WaitForElement(RemainingAmountLabel);
            RemainingAmountLabel.Click();
            WaitForElement(RemainingAmountInput);            
            RemainingAmountInput.Clear();
            RemainingAmountInput.SendKeys("1000");
            //Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(500);
            WaitForElement(BondNextButton);
            BondNextButton.Click();
            //Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(500);
        }
        public void OtherDetails()
        {
            WaitForElement(DescriptionOfWork);
            DescriptionOfWork.Click();
            //Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(500);
            WaitForElement(DescriptionOfWorkInput);
            DescriptionOfWorkInput.SendKeys("Test");
            //Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(500);
            WaitForElement(NotesLabel);
            NotesLabel.Click();
            //Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(500);
            WaitForElement(Notesinput);
            Notesinput.SendKeys("Test");
            WaitForElement(HasbeenSignedCheckBox);
            HasbeenSignedCheckBox.Click();
            //Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(500);
            WaitForElement(StartValueLabel);
            StartValueLabel.Click();
            WaitForElement(StartValueInput);
            StartValueInput.SendKeys("1000");
            //Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(500);
            WaitForElement(Slider);
            Slider.Click();
            //Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(500);
            WaitForElement(UnitesServicedLabel);
            UnitesServicedLabel.Click();
            WaitForElement(UnitesServicedInput);
            UnitesServicedInput.SendKeys("2");
            //Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(500);
            WaitForElement(AnticipatedReleaseDateLabel);
            AnticipatedReleaseDateLabel.Click();
            //Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(500);
            WaitForElement(AnticipatedReleaseDateInput);
            AnticipatedReleaseDateInput.Click();
            //Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(500);
            WaitForElement(BondOkButton);
            BondOkButton.Click();
            //Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(500);

        }


        public void BondSiteAddress()
        {
            WaitForElement(SiteAddressLabel);
            WaitForElementToClick(SiteAddressLabel);
            SiteAddressLabel.Click();
            //Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(500);
            WaitForElement(AddressLine1Label);
            AddressLine1Label.Click();
            //Thread.Sleep(1000);
            WaitForElement(AddressLine1Input);
            AddressLine1Input.SendKeys("Site 1");
            WaitForElement(TownLabel);
            TownLabel.Click();
            //Thread.Sleep(1000);
            WaitForElement(TownInput);
            TownInput.SendKeys("Test Town");
            WaitForElement(SitePostcodeLabel);
            SitePostcodeLabel.Click();
            //Thread.Sleep(1000);
            WaitForElement(SitePostcodeInput);
            SitePostcodeInput.SendKeys("L1");
            //Thread.Sleep(1000);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(1000);
            WaitForElement(SetAddressButton);
            SetAddressButton.Click();
            //Thread.Sleep(1000);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(1000);
        }


        //Create Conditions from Details Page

        public void RoadBondConditionsOnDetails()
        {
            WaitForElements(RoadBondProducts);
            if (RoadBondProducts.Count > 0)
            {
                //Thread.Sleep(1000);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                //Thread.Sleep(1000);
                int BondProductsList = RoadBondProducts.Count;
                for (int i = 0; i < 1; i++)
                {
                    if (i != 0)
                    {
                        WaitForElement(SiteDetailsButton);
                        SiteDetailsButton.Click();
                        WaitForElement(AddBondButton);
                        AddBondButton.Click();
                        //Thread.Sleep(1000);
                        CloseSpinneronDiv();
                        CloseSpinneronPage();
                        //Thread.Sleep(1000);
                        WaitForElement(BondDialogueWindow);
                        Assert.IsTrue(BondDialogueWindow.Displayed);
                        WaitForElement(RoadBondProducts[0]);
                        RoadBondProducts[0].Click();
                        //Thread.Sleep(1000);
                        CloseSpinneronDiv();
                        CloseSpinneronPage();
                        Thread.Sleep(500);
                        if (CoverList.Count > 0)
                        {
                            int CoversList = CoverList.Count;
                            WaitForElement(CoverList[0]);
                            CoverList[0].Click();
                            //Thread.Sleep(1000);
                            CloseSpinneronDiv();
                            CloseSpinneronPage();
                            //Thread.Sleep(1000);
                            ConditionsOnDetailsPage();
                            BondPartsPage();
                            ConditionsOnOtherDetails();
                            //Thread.Sleep(1000);
                            CloseSpinneronDiv();
                            CloseSpinneronPage();
                            //Thread.Sleep(1000);
                        }
                    }
                    else
                    {
                        WaitForElement(RoadBondProducts[0]);
                        RoadBondProducts[0].Click();
                        //Thread.Sleep(1000);
                        CloseSpinneronDiv();
                        CloseSpinneronPage();
                        //Thread.Sleep(1000);
                        if (CoverList.Count > 0)
                        {
                            int CoversList = CoverList.Count;
                            WaitForElement(CoverList[0]);
                            CoverList[0].Click();
                            //Thread.Sleep(1000);
                            CloseSpinneronDiv();
                            CloseSpinneronPage();
                            //Thread.Sleep(1000);
                            ConditionsOnDetailsPage();
                            BondPartsPage();
                            ConditionsOnOtherDetails();
                            //Thread.Sleep(1000);
                            CloseSpinneronDiv();
                            CloseSpinneronPage();
                            //Thread.Sleep(1000);
                        }
                    }
                }
            }
        }

        //Verify Conditions 
        public void VerifyConditionsOnBond()
        {

            WaitForElements(ConditionsTitles);
            if(ConditionsTitles.Count>0)
            {
                Assert.IsTrue(ConditionsTitles.Any(x => x.Text == "Bond must be signed correctly"));
                Assert.IsTrue(ConditionsTitles.Any(x => x.Text == "Confirmation of the expected bond completion date"));
                Assert.IsTrue(ConditionsTitles.Any(x => x.Text == "Confirmation of the value of works to be covered by the bond"));
                Assert.IsTrue(ConditionsTitles.Any(x => x.Text == "Confirmation of the company completing the works covered under the bond"));
                Assert.IsTrue(ConditionsTitles.Any(x => x.Text == "Confirmation of the Description of Works to be covered by the bond"));
               //Assert.IsTrue(ConditionsTitles.Any(x => x.Text == "Confirmation of the authority benefitting from the works covered under the bond"));
            }
        }

        public void ChaseBondConditions()
        {
            //Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(500);
            WaitForElement(BondChaseButton);
            BondChaseButton.Click();
            //Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(500);
            WaitForElement(BondChaseDialogueWindow);
            Assert.IsTrue(BondChaseDialogueWindow.Displayed);
            //Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(500);
            WaitForElement(BondChaseSendButton);
            BondChaseSendButton.Click();
            //Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(500);
        }

        public void CloseBondConditions()
        {
            //Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(500);
            WaitForElement(BondEditButton);
            BondEditButton.Click();
            //Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(500);
            DetailsPage();
            BondPartsPage();
            OtherDetails();
        }



        public void ConditionsOnDetailsPage()
        {

            BondSiteAddress();
            if (ApplicantField.Count != 0)
            {
               if(ApplicantClear.Count>0)
                {
                    WaitForElement(ApplicantClear[0]);
                    ApplicantClear[0].Click();
                    //Thread.Sleep(500);
                    CloseSpinneronDiv();
                    CloseSpinneronPage();
                    //Thread.Sleep(500);
                }
            }

            if (BeneficiaryField.Count != 0)
            {
                if (BeneficiaryClear.Count > 0)
                {
                    WaitForElement(BeneficiaryClear[0]);
                    BeneficiaryClear[0].Click();
                    Thread.Sleep(500);
                    CloseSpinneronDiv();
                    CloseSpinneronPage();
                    //Thread.Sleep(500);
                }
            }

            if (BondRecipientField.Count > 0)
            {
                WaitForElement(BondRecipientLabel);
                BondRecipientLabel.Click();
                //Thread.Sleep(500);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                //Thread.Sleep(500);
                WaitForElements(BondRecipientList);
                if (BondRecipientList.Count > 0)
                {
                    WaitForElement(BondRecipientList[0]);
                    CloseSpinneronDiv();
                    //Thread.Sleep(500);
                    BondRecipientList[0].Click();
                    Thread.Sleep(500);
                    CloseSpinneronDiv();
                    CloseSpinneronPage();
                    //Thread.Sleep(500);
                }
            }
            WaitForElement(BondNextButton);
            BondNextButton.Click();
        }
        public void ConditionsOnOtherDetails()
        {
            WaitForElement(DescriptionOfWork);
            DescriptionOfWork.Click();
            //Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(500);
            WaitForElement(DescriptionOfWorkInput);
            DescriptionOfWorkInput.Clear();
            //Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(500);
            WaitForElement(NotesLabel);
            NotesLabel.Click();
            //Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(500);
            WaitForElement(Notesinput);
            Notesinput.Clear();
            //WaitForElement(HasbeenSignedCheckBox);
            //HasbeenSignedCheckBox.Click();
            //Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(500);
            WaitForElement(StartValueLabel);
            StartValueLabel.Click();
            WaitForElement(StartValueInput);
            StartValueInput.Clear();
            //Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(500);
            //WaitForElement(Slider);
            //Slider.Click();
            
            //CloseSpinneronDiv();
            //CloseSpinneronPage();
            
            WaitForElement(UnitesServicedLabel);
            UnitesServicedLabel.Click();
            WaitForElement(UnitesServicedInput);
            UnitesServicedInput.SendKeys("2");
            //Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(500);
            //WaitForElement(AnticipatedReleaseDateLabel);
            //AnticipatedReleaseDateLabel.Click();
            
            //CloseSpinneronDiv();
            //CloseSpinneronPage();
            
            //WaitForElement(AnticipatedReleaseDateInput);
            //AnticipatedReleaseDateInput.Click();
            
            //CloseSpinneronDiv();
            //CloseSpinneronPage();
            
            WaitForElement(BondOkButton);
            BondOkButton.Click();
            //Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(500);

        }
        public void VerifyBondInCompleteStatus()
        {
            //Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(500);
            WaitForElement(SiteDetailsButton);
            SiteDetailsButton.Click();
            //Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(500);
            WaitForElement(BondStatus);
            Assert.IsTrue(BondStatus.Text.Contains("Application incomplete"));
            BondStatus.Click();
            //Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(500);
        }
        public void VerifyBondSubmittedStatus()
        {
            WaitForElement(SiteDetailsButton);
            SiteDetailsButton.Click();
            //Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(500);
            WaitForElement(BondStatus);
            Assert.IsTrue(BondStatus.Text.Contains("Application submitted"));
            BondStatus.Click();
            //Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(500);
        }

        public void VerifyBondRejectedStatus()
        {
            WaitForElement(SiteDetailsButton);
            SiteDetailsButton.Click();
            //Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(500);
            WaitForElement(BondStatus);
            Assert.IsTrue(BondStatus.Text.Contains("Rejected"));
            BondStatus.Click();
            //Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(500);
        }




        public void AcceptBond()
        {
            
            //Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(500);
            WaitForElement(MakeDecesionButton);
            MakeDecesionButton.Click();
            //Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(500);
            WaitForElement(AcceptBondButton);
            AcceptBondButton.Click();
            //Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(500);
            WaitForElement(BondNextButton);
            BondNextButton.Click();
            //Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(500);           
            var fileInfo = new FileInfo(ExtensionMethods.GetAbsolutePath(@"TestData\QuotesData\CRISP install guide.docx"));
            BondFileInput.SendKeys($"{fileInfo}");

            //BondFileInput.SendKeys(@"\\tpgfile\NewCompany\IT\Department\QA Team\Current Projects\CRISP\CrispAutomation\CrispData\QuotesData\CRISP install guide.docx");
            //Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(500);
            WaitForElement(SignedByLabel);
            SignedByLabel.Click();
            //Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(500);
            WaitForElements(ContactList);
            if(ContactList.Count>0)
            {
                ContactList[0].Click();
                CloseSpinneronDiv();
                CloseSpinneronPage();
            }
            //Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            WaitForElement(InsuranceAgreementLabel);
            InsuranceAgreementLabel.Click();
            //Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(500);
            WaitForElement(AmtrustAgreement);
            AmtrustAgreement.Click();
            //Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(500);
            WaitForElements(InsuranceAgreement);
            InsuranceAgreement[0].Click();
            //Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(500);
            WaitForElement(BondNextButton);
            BondNextButton.Click();
            //Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(500);
            WaitForElement(CorrespondenceEmail);
            Assert.IsTrue(CorrespondenceEmail.Displayed);
            //Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(500);
            WaitForElement(BondAcceptSendButton);
            BondAcceptSendButton.Click();
            //Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(500);
            CompletePart();
        }
        public void CompletePart()
        {

            //Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(500);
            WaitForElement(CompletePartButton);
            CompletePartButton.Click();
            //Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(500);
            WaitForElement(BondNextButton);
            BondNextButton.Click();
            //Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(500);
            WaitForElement(CorrespondenceEmail);
            Assert.IsTrue(CorrespondenceEmail.Displayed);
            //Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(500);
            WaitForElement(BondCompleteSendButton);
            BondCompleteSendButton.Click();
            //Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(500);
        }

        public void RejectingBond()
        {
            //Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(500);
            WaitForElement(MakeDecesionButton);
            MakeDecesionButton.Click();
            //Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(500);
            WaitForElement(RejectBondButton);
            RejectBondButton.Click();
            //Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(500);
            WaitForElement(BondNextButton);
            BondNextButton.Click();
            //Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(500);
            WaitForElements(BondRejectionReasons);
            foreach(var eachReason in BondRejectionReasons)
            {
                eachReason.Click();
            }
            //Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(500);
            WaitForElement(RejectionCommentLabel);
            RejectionCommentLabel.Click();
            WaitForElement(RejectionCommentInput);
            RejectionCommentInput.SendKeys("Test Rejecting");
            //Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(500);
            WaitForElement(RejectionOtherReasonCommentLabel);
            RejectionOtherReasonCommentLabel.Click();
            WaitForElement(RejectionOtherReasonCommentInput);
            RejectionOtherReasonCommentInput.SendKeys("Test Rejecting");
            //Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(500);
            WaitForElement(BondNextButton);
            BondNextButton.Click();
            //Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(500);
            WaitForElement(CorrespondenceEmail);
            Assert.IsTrue(CorrespondenceEmail.Displayed);
            //Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(500);
            WaitForElement(BondAcceptSendButton);
            BondAcceptSendButton.Click();
            //Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(500);
        }

        public void CreateSingleRoadBond()
        {            
            WaitForElement(AddBondButton);
            AddBondButton.Click();
            //Thread.Sleep(1000);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(1000);
            Assert.IsTrue(BondDialogueWindow.Displayed);
            WaitForElements(RoadBondProducts);
            //Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(500);
            int BondProductsList = RoadBondProducts.Count;
            WaitForElements(RoadBondProducts);
            RoadBondProducts[0].Click();
            //Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(500);
            int CoversList = CoverList.Count;
            WaitForElements(CoverList);
            WaitForElement(CoverList[0]);
            CoverList[0].Click();
            //Thread.Sleep(1000);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(1000);
            BondSiteAddress();
            DetailsPage();
            BondPartsPage();
            OtherDetails();
            //Thread.Sleep(1000);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(1000);        
        }
        public void CreateSingleSewerBond()
        {
            //Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(500);
            WaitForElement(SiteDetailsButton);
            SiteDetailsButton.Click();
            WaitForElement(AddBondButton);
            AddBondButton.Click();
            //Thread.Sleep(1000);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(1000);
            Assert.IsTrue(BondDialogueWindow.Displayed);
            WaitForElements(RoadBondProducts);
            //Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(500);
            int BondProductsList = SewerBondProducts.Count;
            WaitForElements(SewerBondProducts);
            SewerBondProducts[0].Click();
            //Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(500);
            int CoversList = CoverList.Count;
            WaitForElements(CoverList);
            WaitForElement(CoverList[0]);
            CoverList[0].Click();
            //Thread.Sleep(1000);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(1000);
            BondSiteAddress();
            DetailsPage();          
            OtherDetails();
            //Thread.Sleep(1000);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(1000);
        }
    }
}
