﻿using CrispAutomation.Features;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace CrispAutomation.Pages
{
    public class SiteRiskAssessmentPage : Support.Pages
    {
        public IWebDriver wdriver;
        public SiteRiskAssessmentPage(ISearchContext driver)
        {
            PageFactory.InitElements(driver, this);
        }

        [FindsBy(How = How.XPath, Using = "//div//a[@class='info-box bg-light-green hover-expand-effect']//div[text()='Site risk assessment']")]
        public IWebElement SiteRiskAssessmentOption;

        [FindsBy(How = How.XPath, Using = "//div//a[@class='info-box bg-light-green hover-expand-effect']//div[@class='content']")]
        public IWebElement SRAStatus;


        [FindsBy(How = How.XPath, Using = "//section[@class='content']//div//h2[text()='Site Risk Assessment']")]
        public IWebElement SiteRiskSection;

        [FindsBy(How = How.XPath, Using = "//div/a[contains(@href,'/Sites/RiskAssessments/Edit/SiteRiskAssessment')]")]
        public IWebElement EditButton;

        //Elements on Edit Site Risk Assessment Page

        [FindsBy(How = How.XPath, Using = "//div[@class='alert alert-info'][contains(text(),'Where conversion elements also exist this assessment is for the new build elements only.')]")]
        public IWebElement AlertInfoOnEdit;

        //Elements on Site Overview Section 

        [FindsBy(How = How.XPath, Using = "//div[@class='input-group']//span/i[@class='material-icons']")]
        public IWebElement OriginalDateOfAssessment;

        [FindsBy(How = How.XPath, Using = "//div[@class='dtp-buttons']//button[@class='dtp-btn-ok btn btn-flat'][text()='OK']")]
        public IWebElement DateInput;

        [FindsBy(How = How.CssSelector, Using = "#Assessment_PeoplePresent")]
        public IWebElement PeoplePresent;

        //Elements on Site Exposure & Geo Technical Section 

        [FindsBy(How = How.XPath, Using = "//div//h2[contains(text(),'Site Exposure')]")]
        public IWebElement SiteExposureDiv;

        [FindsBy(How = How.CssSelector, Using = "button[data-id = 'Assessment_LocalWeatherExposure'] >span[class='filter-option pull-left']")]
        public IWebElement LocalExposure;

        [FindsBy(How = How.XPath, Using = "//div[@class='dropdown-menu open'] //ul[@class='dropdown-menu inner']//li//a//span[text()='Moderate']")]
        public IWebElement LocalExposureInput;

        [FindsBy(How = How.CssSelector, Using = "button[data-id = 'Assessment_GroundType']>span[class='filter-option pull-left']")]
        public IWebElement GroundType;

        [FindsBy(How = How.XPath, Using = "//div[@class='dropdown-menu open'] //ul[@class='dropdown-menu inner']//li//a//span[text()='Rock']")]
        public IWebElement GroundTypeInput;

        [FindsBy(How = How.CssSelector, Using = "button[data-id = 'Assessment_DrivewaysAndPaving']>span[class='filter-option pull-left']")]
        public IWebElement DrivewaysAndPaving;

        [FindsBy(How = How.XPath, Using = "//div[@class='dropdown-menu open'] //ul[@class='dropdown-menu inner']//li//a//span[text()='Concrete Paving']")]
        public IWebElement DrivewaysAndPavingInput;

        [FindsBy(How = How.CssSelector, Using = "button[data-id = 'Assessment_TreeState']>span[class='filter-option pull-left']")]
        public IWebElement TreeState;

        [FindsBy(How = How.XPath, Using = "//div[@class='dropdown-menu open'] //ul[@class='dropdown-menu inner']//li//a//span[text()='Rock']")]
        public IWebElement TreeStateInput;

        //All Elements on Site Risk Assessment Page 

        [FindsBy(How = How.XPath, Using = "//form[@id='editSra']//div[contains(@class,'btn-group bootstrap-select')]//button[@data-toggle='dropdown']")]
        public IList<IWebElement> DropdownElementsonSra;

        [FindsBy(How = How.XPath, Using = "//div[contains(@class,'open')]//div[@class='dropdown-menu open']//ul[@class='dropdown-menu inner']//li[@data-original-index='1']")]
        public IWebElement DropDownInput;

        [FindsBy(How = How.XPath, Using = "//form[@id='editSra']//h2[contains(text(),'Site Exposure & Geo Technical')]")]
        public IWebElement SiteExposure;

        [FindsBy(How = How.XPath, Using = "//form[@id='editSra']//div[@class='form-control']//label[@class='form-label'][text()='Yes']")]
        public IList<IWebElement> RadioButtonsonSra;

        [FindsBy(How = How.XPath, Using = "//form[@id='editSra']//div[@class='form-control']//label[@class='form-label'][text()='No']")]
        public IList<IWebElement> NoOptionRadioButtonsonSra;

        [FindsBy(How = How.XPath, Using = "//form[@id='editSra']//div[@class='form-contents']//label[@for='Assessment_AreStageOfWorksAsSpecifiedInTheSiteApplication_false'][text()='No']")]
        public IWebElement RadioButtonsonStagesOfWorks;

        [FindsBy(How = How.XPath, Using = "//div[contains(@class,'br-theme-bootstrap-stars')]//div[@class='br-widget']/a[5]")]
        public IList<IWebElement> RatingStars;

        [FindsBy(How = How.XPath, Using = "//textarea[contains(@id,'Assessment_Comment')]")]
        public IList<IWebElement> RatingComment;

        [FindsBy(How = How.XPath, Using = "//div[@id='page2']//div[@class='form-contents']//button[@type='submit'][contains(text(),'Save')]")]
        public IWebElement SaveButton;

        [FindsBy(How = How.XPath, Using = "//div[@id='page2']//div[@class='form-contents']//button[@type='submit'][contains(text(),'Submit')]")]
        public IWebElement SubmitButton;

        [FindsBy(How = How.XPath, Using = "//div[@id='savebar']//button[contains(@class,'btn btn-success btn-lg pull-right')][text()='Next']")]
        public IWebElement NextButton;

        [FindsBy(How = How.XPath, Using = "//section[@class='content']//form[@method='post']//button[@type='submit'][contains(text(),'Send')]")]
        public IWebElement SendButton;

        [FindsBy(How = How.XPath, Using = "//div[@id='TechnicalConditions']/table/tfoot/tr/td/button[text()='Add conditions']")]
        public IWebElement AddTechConditionButton;

        [FindsBy(How = How.XPath, Using = "//div[@role='dialog']//button[text()='CUSTOM CONDITION']")]
        public IWebElement CustomConditionButton;

        [FindsBy(How = How.XPath, Using = "//div[@id='TechnicalConditions']//table//tbody//tr//td//div//div//div//button")]
        public IList<IWebElement> TechConditionsDropdowns;

        [FindsBy(How = How.XPath, Using = "//div[@id='TechnicalConditions']//table/tbody//tr//td//div[contains(@class,'open')]//div[@class='dropdown-menu open']//ul[@class='dropdown-menu inner']/li")]
        public IList<IWebElement> TechConditionsDropdownsInput;


        //Page 2 Elements Site Risk Assessment 

        [FindsBy(How = How.XPath, Using = "//div[@id='page2']//h2[contains(text(),'Design Documents')]")]
        public IWebElement DesignDocuments;

        [FindsBy(How = How.XPath, Using = "//div[@id='page2']//h2[contains(text(),'Completion Documents')]")]
        public IWebElement CompletionDocuments;

        [FindsBy(How = How.XPath, Using = "//div[@id='page2']//h2[contains(text(),'Design Items')]")]
        public IWebElement DesignItems;

        [FindsBy(How = How.XPath, Using = "//div[@id='page2']//h2[contains(text(),'Risk Items')]")]
        public IWebElement RiskItems;


        [FindsBy(How = How.XPath, Using = "//textarea[contains(@id,'Details')][@class='form-control']")]
        public IWebElement TechConditionsDetails;

        [FindsBy(How = How.XPath, Using = "//div[@class='alert alert-warning']")]
        public IWebElement SubmitAlert;

        [FindsBy(How = How.XPath, Using = "//button[text()='Submit assessment']")]
        public IWebElement SubmitAssessmentButton;

        [FindsBy(How = How.XPath, Using = "//div[@id='page2']//div[@class='form-control']//label[@for='Assessment_AcceptableRisk_true']")]
        public IWebElement AcceptableRiskRadioButton;

        [FindsBy(How = How.XPath, Using = "//div[@class='alert alert-success'][contains(text(),'Site risk assessment submitted')]")]
        public IWebElement SubmitSuccessAlert;

        [FindsBy(How = How.XPath, Using = "//div//crisp-header-title[@class='au-target'][1]/div[@class='title']")]
        public IWebElement OrderRefDetails;

        [FindsBy(How = How.XPath, Using = "//form[@id='editSra']//div[@class='row']//div[@class='header']//h2[contains(text(),'Assessment & Scores')]")]
        public IWebElement AssessmentandScores;

        [FindsBy(How = How.XPath, Using = "//form[@id='editSra']//div[contains(@class,'btn-group bootstrap-select')]//button[@data-toggle='dropdown'][@data-id='Assessment_SiteComplexity']")]
        public IWebElement SiteComplexityDropdown;

        [FindsBy(How = How.XPath, Using = "//section[@class='content']//div//h2[text()='Refurbishment Assessment']")]
        public IWebElement RefurbishmentSection;

        [FindsBy(How = How.XPath, Using = "//div[@id='page2']//div[@class='form-contents']//button[@type='submit'][contains(text(),'Save')]")]
        public IWebElement AssessmentSaveButton;


        //open SRA 
        public void OpenSRA()
        {
            Thread.Sleep(500);
            CloseSpinnerOnSurveyorPage();
            CloseSpinnerOnSurveyorDiv();
            //Thread.Sleep(500);
            WaitForElementonSurveyor(SiteRiskAssessmentOption);
            WaitForElementonSurveyor(SRAStatus);
            var sraStatus = SRAStatus.Text.ToString();           
            Thread.Sleep(500);
            CloseSpinnerOnSurveyorPage();
            //Thread.Sleep(500);
            WaitForElementonSurveyor(SiteRiskAssessmentOption);
            SiteRiskAssessmentOption.Click();
            //Thread.Sleep(500);
            CloseSpinnerOnSurveyorPage();
            //Thread.Sleep(500);
            WaitForElementonSurveyor(SiteRiskSection);
            Assert.IsTrue(SiteRiskSection.Displayed, "Failed to send correspondence on Site Risk Assessment");
            Assert.IsTrue(EditButton.Displayed, "No permissions applied to the user to submit SiteRisk Assessment");
            //Thread.Sleep(1000);
            WaitForElementonSurveyor(EditButton);
            EditButton.Click();
            //Thread.Sleep(500);
            CloseSpinnerOnSurveyorPage();
            CloseSpinnerOnSurveyorDiv();
            //Thread.Sleep(500);
        }
        // Submit SRA 

        public void SubmitSRA()
        {
            Thread.Sleep(500);
            CloseSpinnerOnSurveyorPage();
            CloseSpinnerOnSurveyorDiv();
            Thread.Sleep(500);
            WaitForElementonSurveyor(AcceptableRiskRadioButton);
            AcceptableRiskRadioButton.Click();
            Thread.Sleep(500);
            CloseSpinnerOnSurveyorPage();
            CloseSpinnerOnSurveyorDiv();
            //Thread.Sleep(500);
            WaitForElementonSurveyor(SubmitButton);
            SubmitButton.Click();
            //Thread.Sleep(500);
            CloseSpinnerOnSurveyorPage();
            CloseSpinnerOnSurveyorDiv();
            //Thread.Sleep(500);
            WaitForElementonSurveyor(SendButton);
            SendButton.Click();
            //Thread.Sleep(500);
            CloseSpinnerOnSurveyorPage();
            CloseSpinnerOnSurveyorDiv();
            //Thread.Sleep(500);
            WaitForElementonSurveyor(SiteRiskSection);
            Assert.IsTrue(SiteRiskSection.Displayed, "Failed to send correspondence on Site Risk Assessment");
        }

        //Save SRA
        public void SaveSRA()
        {
            Thread.Sleep(500);
            CloseSpinnerOnSurveyorPage();
            WaitForElementonSurveyor(AcceptableRiskRadioButton);
            AcceptableRiskRadioButton.Click();
            Thread.Sleep(500);
            CloseSpinnerOnSurveyorPage();
            //Thread.Sleep(500);
            WaitForElementonSurveyor(SubmitButton);
            var SubmitButtonStatus = SubmitButton.GetAttribute("disabled");
            if (SubmitButtonStatus != null || SubmitButtonStatus == "true")
            {
                WaitForElementonSurveyor(AssessmentSaveButton);
                AssessmentSaveButton.Click();
                //Thread.Sleep(500);
                CloseSpinnerOnSurveyorPage();
                Thread.Sleep(500);
                WaitForElementonSurveyor(SiteRiskSection);
                Assert.IsTrue(SiteRiskSection.Displayed, "Failed to send correspondence on Site Risk Assessment");
               
            }
            else
            {
                WaitForElementonSurveyor(SubmitButton);
                SubmitButton.Click();                
                //Thread.Sleep(500);
                CloseSpinnerOnSurveyorPage();
                //Thread.Sleep(500);
                WaitForElementonSurveyor(SendButton);
                SendButton.Click();             
                //Thread.Sleep(500);
                CloseSpinnerOnSurveyorPage();
                Thread.Sleep(500);
                WaitForElement(SiteRiskSection);
                Assert.IsTrue(SiteRiskSection.Displayed, "Failed to send correspondence on Site Risk Assessment");
            }
        }
        public void SiteRiskAssessmentMain()
        {
            SurveyorLoginPage.SelectSite();            
           // SurveyorAssessments.VerifySiteStatusBeforeAssessment();          
            Thread.Sleep(500);
            CloseSpinnerOnSurveyorPage();
            CloseSpinnerOnSurveyorDiv();
            Thread.Sleep(500);
            WaitForElementonSurveyor(SiteRiskAssessmentOption);
            WaitForElementonSurveyor(SRAStatus);
            var sraStatus = SRAStatus.Text.ToString();
            Thread.Sleep(500);
            CloseSpinnerOnSurveyorPage();
            CloseSpinnerOnSurveyorDiv();
            Thread.Sleep(500);
            if (sraStatus.Contains("Outstanding"))
            {
                OpenSRA();
                //Thread.Sleep(500);
                CloseSpinnerOnSurveyorPage();
                //Thread.Sleep(500);
                WaitForElementonSurveyor(AlertInfoOnEdit);
                Assert.IsTrue(AlertInfoOnEdit.Displayed);
                EditSiteRiSkAssessmentSection();
                //Thread.Sleep(500);
                CloseSpinnerOnSurveyorPage();
                //Thread.Sleep(500);
                SubmitSRA();
            }
            
        }
        public void EditSiteRiSkAssessmentSection()
        {
            SiteOverviewSection();
            SraEditPage();
            AssessmentScoresSection();
            SurveyorDocs.CloseSurveyorDocuments();
            Thread.Sleep(500);
            CloseSpinnerOnSurveyorPage();
            Thread.Sleep(500); ;
        }

        public void EditSRAWhenNoSelected()
        {
            SiteOverviewSection();
            SraEditPageWhenNoSelected();
            AssessmentScoresSection();
            //Thread.Sleep(500);
            CloseSpinnerOnSurveyorPage();
            //Thread.Sleep(500); ;
        }

        //form[@id="editSra"]//a[5]
        public void SiteOverviewSection()
        {
            WaitForElementonSurveyor(OriginalDateOfAssessment);
            // Scrollup();
            //Thread.Sleep(500);
            OriginalDateOfAssessment.Click();
            //Thread.Sleep(500);
            WaitForElementonSurveyor(DateInput);
            DateInput.Click();
            //Thread.Sleep(500);
            WaitForElementonSurveyor(PeoplePresent);
            PeoplePresent.Click();
            PeoplePresent.Clear();
            PeoplePresent.SendKeys("3");
            WaitForElements(RadioButtonsonSra);
        }
       

        public void SraEditPage()
        {
            WaitForElements(RadioButtonsonSra);
            foreach (var RadioButton in RadioButtonsonSra)
            {
                RadioButton.Click();

            }
            //Thread.Sleep(500);
            CloseSpinnerOnSurveyorPage();
            //Thread.Sleep(500);
            // WaitForElementonSurveyor(RadioButtonsonStagesOfWorks);
            //ScrollIntoView(RadioButtonsonStagesOfWorks);
            // RadioButtonsonStagesOfWorks.Click();
            WaitForLoadElements(DropdownElementsonSra);
            if (DropdownElementsonSra.Count > 0)
            {
                for (int i = 0; i < DropdownElementsonSra.Count; i++)
                {
                    var element = DropdownElementsonSra[i];
                    bool isVisible = element.Displayed;


                    var javascriptCapableDriver = (IJavaScriptExecutor)Driver;
                    var visibleScript = "return $('[data-id=\"" + DropdownElementsonSra[i].GetAttribute("data-id") + "\"]').is(':visible');";

                    bool jQueryBelivesElementIsVisible = Convert.ToBoolean(javascriptCapableDriver.ExecuteScript(visibleScript));
                    bool elementIsVisible = isVisible && jQueryBelivesElementIsVisible;

                    if (elementIsVisible == true)
                    {
                        WaitForLoadElementtobeclickable(DropdownElementsonSra[i]);
                        DropdownElementsonSra[i].SendKeys(Keys.Enter);
                        //Thread.Sleep(500);
                        WaitForElementonSurveyor(DropDownInput);
                        DropDownInput.Click();
                        DropdownElementsonSra[i].SendKeys(Keys.Tab);
                    }
                }
            }
            //Thread.Sleep(500);
            ////WaitForLoadElement(SiteComplexityDropdown);
            ////SiteComplexityDropdown.Click();
            
            //WaitForElementonSurveyor(DropDownInput);
            //DropDownInput.Click();
            

        }
        public void SraEditPageWhenNoSelected()
        {
            WaitForElements(RadioButtonsonSra);
            foreach (var RadioButton in NoOptionRadioButtonsonSra)
            {
                RadioButton.Click();
            }
            //Thread.Sleep(500);
            CloseSpinnerOnSurveyorPage();
            //Thread.Sleep(500);
            // WaitForElementonSurveyor(RadioButtonsonStagesOfWorks);
            //ScrollIntoView(RadioButtonsonStagesOfWorks);
            // RadioButtonsonStagesOfWorks.Click();       

            WaitForLoadElements(DropdownElementsonSra);
            if (DropdownElementsonSra.Count > 0)
            {
                for (int i = 0; i < DropdownElementsonSra.Count; i++)
                {
                    var element = DropdownElementsonSra[i];
                    bool isVisible = element.Displayed;


                    var javascriptCapableDriver = (IJavaScriptExecutor)Driver;
                    var visibleScript = "return $('[data-id=\"" + DropdownElementsonSra[i].GetAttribute("data-id") + "\"]').is(':visible');";

                    bool jQueryBelivesElementIsVisible = Convert.ToBoolean(javascriptCapableDriver.ExecuteScript(visibleScript));
                    bool elementIsVisible = isVisible && jQueryBelivesElementIsVisible;

                    if (elementIsVisible == true)
                    {
                        WaitForLoadElementtobeclickable(DropdownElementsonSra[i]);
                        DropdownElementsonSra[i].SendKeys(Keys.Enter);
                        //Thread.Sleep(500);
                        WaitForElementonSurveyor(DropDownInput);
                        DropDownInput.Click();
                        DropdownElementsonSra[i].SendKeys(Keys.Tab);
                    }
                }
            }
            //Thread.Sleep(500);
            //WaitForLoadElement(SiteComplexityDropdown);
            //SiteComplexityDropdown.Click();
            
            //WaitForElementonSurveyor(DropDownInput);
            //DropDownInput.Click();
            

        }

        public void AssessmentScoresSection()
        {
            IJavaScriptExecutor js = Driver as IJavaScriptExecutor;
            js.ExecuteScript("arguments[0].scrollIntoView(true);", AssessmentandScores);
            // Run the javascript command 'scrollintoview on the element
            // js.ExecuteScript("scroll(0,2500);");
            // WaitForLoadElements(RatingStars);
            foreach (var eachRatingStar in RatingStars)
            {
                eachRatingStar.Click();
            }

            WaitForElements(RatingComment);
            foreach (var eachComment in RatingComment)
            {
                eachComment.Click();
                eachComment.Clear();
                eachComment.SendKeys("GoodRate");
            }
            WaitForElementonSurveyor(NextButton);
            NextButton.Click();
            //Thread.Sleep(500);
            CloseSpinnerOnSurveyorPage();
            //Thread.Sleep(500);

        }
        
        public void SubmitSiteRiskAssessmentMethod()
        {
            SurveyorLoginPage.SurveyorLoginMethod();
            SurveyorLoginPage.SelectSite();
            WaitForElementonSurveyor(SiteRiskAssessmentPage.SiteRiskAssessmentOption);
            //Thread.Sleep(500);
            CloseSpinnerOnSurveyorPage();
            //Thread.Sleep(500);
            SiteRiskAssessmentPage.SiteRiskAssessmentOption.Click();
            //Thread.Sleep(500);
            CloseSpinnerOnSurveyorPage();
            //Thread.Sleep(500);
            WaitForElementonSurveyor(SiteRiskAssessmentPage.SiteRiskSection);
            Assert.IsTrue(SiteRiskAssessmentPage.SiteRiskSection.Displayed);
            Assert.IsTrue(SiteRiskAssessmentPage.EditButton.Displayed, "No permissions applied to the user to submit SiteRisk Assessment");
            WaitForElementonSurveyor(SiteRiskAssessmentPage.EditButton);
            SiteRiskAssessmentPage.EditButton.Click();
            //Thread.Sleep(500);
            CloseSpinnerOnSurveyorPage();
            //Thread.Sleep(500);
            WaitForElementonSurveyor(SiteRiskAssessmentPage.AlertInfoOnEdit);
            Assert.IsTrue(SiteRiskAssessmentPage.AlertInfoOnEdit.Displayed);
            SiteRiskAssessmentPage.EditSiteRiSkAssessmentSection();
            //Thread.Sleep(500);
            CloseSpinnerOnSurveyorPage();
            //Thread.Sleep(500);
            WaitForElementonSurveyor(SiteRiskAssessmentPage.AcceptableRiskRadioButton);
            SiteRiskAssessmentPage.AcceptableRiskRadioButton.Click();
            //Thread.Sleep(500);
            CloseSpinnerOnSurveyorPage();
            //Thread.Sleep(500);
            WaitForElementonSurveyor(SiteRiskAssessmentPage.SubmitButton);
            SiteRiskAssessmentPage.SubmitButton.Click();
            //Thread.Sleep(500);
            CloseSpinnerOnSurveyorPage();
            //Thread.Sleep(500);
            WaitForElementonSurveyor(SiteRiskAssessmentPage.SendButton);
            SiteRiskAssessmentPage.SendButton.Click();
            //Thread.Sleep(500);
            CloseSpinnerOnSurveyorPage();
            //Thread.Sleep(500);
            WaitForElementonSurveyor(SiteRiskAssessmentPage.SiteRiskSection);
            Assert.IsTrue(SiteRiskAssessmentPage.SiteRiskSection.Displayed, "Failed to send correspondence on Site Risk Assessment");

        }

    }
}
