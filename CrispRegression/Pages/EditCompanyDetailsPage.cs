﻿using CrispAutomation.Support;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.PageObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace CrispAutomation.Pages
{
    public class EditCompanyDetailsPage : Support.Pages
    {
        public IWebDriver wdriver;
        public EditCompanyDetailsPage(ISearchContext driver)
        {
            PageFactory.InitElements(driver, this);
            wdriver = (RemoteWebDriver)driver;
        }

        [FindsBy(How = How.XPath, Using = "//crisp-input-text[@label='Name']/div/LABEL[@data-error.bind='error'][text()='Name']")]
        public IWebElement CompanyName;

        [FindsBy(How = How.XPath, Using = "//crisp-header-actions//crisp-header-button[@click.delegate='edit()']//span//button[@class='au-target waves-effect waves-light btn-flat']")]
        public IWebElement EditCompany;

        [FindsBy(How = How.XPath, Using = "//crisp-input-text[@label='Name']/div/input[@ref='input'][@type='text']")]
        public IWebElement CompanyNameInput;

        [FindsBy(How = How.XPath, Using = "//LABEL[@data-error.bind='error'][text()='Year of registration']")]
        public IWebElement YearOfRegistration;

        [FindsBy(How = How.XPath, Using = "//crisp-input-number[@label='Year of registration']/div/input[@ref='input']")]
        public IWebElement EnterYearOfRegistration;

        [FindsBy(How = How.XPath, Using = "//crisp-input-text[@label='Registration number (e.g. AB123456 or 12345678)']/div/input")]
        public IWebElement EnterRegistrationNumber;

        [FindsBy(How = How.XPath, Using = "//crisp-picker[@label='Legal form type']/div/div/input[@class='select-dropdown']")]
        public IWebElement LegalFormType;

        [FindsBy(How = How.XPath, Using = "//ul[@class='dropdown-content select-dropdown active']//li//span[text()='Public Limited Company  - PLC']")]
        public IWebElement LegalFormTypeDropdown;

        [FindsBy(How = How.XPath, Using = "//crisp-picker[@label='Client segmentation type']/div/div/input")]
        public IWebElement ClientSegmentation;

        [FindsBy(How = How.XPath, Using = "//ul[@class='dropdown-content select-dropdown active']/li[2]/span")]
        public IWebElement ClientSegmentationDropdown;

        [FindsBy(How = How.XPath, Using = "/html/body/ai-dialog-container/div/div/crisp-dialog/div/div[1]/crisp-wizard/crisp-wizard-step[3]/company-details/div/div[1]/crisp-card/crisp-card-content/div/crisp-input-bool/label")]
        public IWebElement VIPCheckBox;

        [FindsBy(How = How.XPath, Using = "//LABEL[@data-error.bind='error'][text()='Facebook']")]
        public IWebElement FaceBookId;

        [FindsBy(How = How.XPath, Using = "//crisp-input-text[@label='Facebook']/div/input[@ref='input']")]
        public IWebElement FacebookElementEnter;

        [FindsBy(How = How.XPath, Using = "//crisp-input-text[@label='Google+']/div/input[@ref='input']")]
        public IWebElement GooglePlusEnter;

        [FindsBy(How = How.XPath, Using = "//crisp-input-text[@label='LinkedIn']/div/input[@ref='input']")]
        public IWebElement LinkdedinEnter;

        [FindsBy(How = How.XPath, Using = "//crisp-input-text[@label='Twitter']/div/input[@ref='input']")]
        public IWebElement TwitterEnter;

        [FindsBy(How = How.XPath, Using = "//crisp-action-button[@icon='add'][@click.delegate='addWebsite()']/div/a/i[text()='add']")]
        public IWebElement AddWebsiteButton;

        [FindsBy(How = How.XPath, Using = "//crisp-input-text[@label='Website Address']/div/LABEL[@data-error.bind='error'][text()='Website Address']")]
        public IWebElement WebsiteAddress;

        [FindsBy(How = How.XPath, Using = "//crisp-input-text[@label='Website Address']/div/input[contains(@id,'crisp-input-text')][@ref='input']")]
        public IWebElement WebSiteInputField;

        [FindsBy(How = How.CssSelector, Using = "body > ai-dialog-container:nth-child(4) > div > div > crisp-dialog > div > div.modal-footer > crisp-footer > nav > div > crisp-footer-button:nth-child(1) > button")]
        public IWebElement AddButton;

        [FindsBy(How = How.XPath, Using = "//crisp-button[@enabled.bind='canGoForward'][@click.delegate='goForward()']/span/button[@ref='button'][text()='Next']")]
        public IWebElement NextButton;

        [FindsBy(How = How.XPath, Using = "//crisp-input-bool[contains(@label,'Opt')]//label")]
        public IList<IWebElement> OptCalls;

        [FindsBy(How = How.XPath, Using = "//crisp-input-bool[contains(@label,'TPS')]//label")]
        public IWebElement NotedOnTPS;

        [FindsBy(How = How.XPath, Using = "//crisp-action-button[@icon='add'][@click.delegate='addOffice()']/div/a/i[text()='add']")]
        public IWebElement AddOfficeButton;

        [FindsBy(How = How.XPath, Using = "//crisp-action-button[@icon='add'][@click.delegate='addEmployee()']/div/a/i")]
        public IWebElement AddEmployeeButton;

        [FindsBy(How = How.XPath, Using = "//div[@class='modal-content']/crisp-input-text[@label='Name']/div/label[text()='Name']")]
        public IWebElement OfficeName;

        [FindsBy(How = How.XPath, Using = "//div[@class='modal-content']/crisp-input-text[@label='Name']/div/input[@ref='input'][@type='text']")]
        public IWebElement OfficeNameInput;
     
        [FindsBy(How = How.CssSelector, Using = "body > ai-dialog-container:nth-child(4) > div > div > crisp-dialog > div > div.modal-content > crisp-input-object:nth-child(2) > div > span > span")]
        public IWebElement EditExistingOfficeAdress;

        [FindsBy(How = How.CssSelector, Using = "body > ai-dialog-container:nth-child(4) > div > div > crisp-dialog > div > div.modal-content > crisp-input-object:nth-child(2) > div > label")]
        public IWebElement OfficeAdress;

        [FindsBy(How = How.XPath, Using = "//label[@class='au-target'][@data-error='A valid outer postcode is required']")]
        public IWebElement PostcodeSearch;

        [FindsBy(How = How.XPath, Using = "//crisp-input-text[@label='Postcode']/div/label")]
        public IList<IWebElement> EditPostcodeLabel;

        [FindsBy(How = How.XPath, Using = "//crisp-input-text[@label='Postcode']/div/input")]
        public IWebElement EditPostcodeInput;

        [FindsBy(How = How.XPath,
        Using = "//crisp-input-text[@label='Enter postcode to search']/div[@class='input-field']/input")]
        public IWebElement PostcodeInput;

        [FindsBy(How = How.XPath, Using = "//crisp-input-text[@label='Enter postcode to search']/div/input")]
        public IWebElement NewPostcodeInput;

        //crisp-input-text[@label='Address Line 1']/div/label[text()='Address Line 1']


        [FindsBy(How = How.XPath, Using = "//crisp-input-text[@label='Address Line 1']/div/label[text()='Address Line 1']")]
        public IList<IWebElement> AddressLine1EditLabel;

        [FindsBy(How = How.XPath, Using = "//crisp-input-text[@label='Address Line 1']/div/input[@ref='input']")]
        public IWebElement AddressLine1Edit;

        [FindsBy(How = How.XPath, Using = "//crisp-input-text[@label='Address Line 2']/div/label[text()='Address Line 2']")]
        public IList<IWebElement> AddressLine2EditLabel;

        [FindsBy(How = How.XPath, Using = "//crisp-input-text[@label='Address Line 2']/div/input[@ref='input']")]
        public IWebElement AddressLine2Edit;

        [FindsBy(How = How.XPath, Using = "//crisp-input-text[@label='Address Line 3']/div/label[text()='Address Line 3']")]
        public IList<IWebElement> AddressLine3EditLabel;

        [FindsBy(How = How.XPath, Using = "//crisp-input-text[@label='Address Line 3']/div/input[@ref='input']")]
        public IWebElement AddressLine3Edit;

        [FindsBy(How = How.XPath, Using = "//crisp-input-text[@label='Town']/div/label[text()='Town']")]
        public IList<IWebElement> TownEditLabel;

        [FindsBy(How = How.XPath, Using = "//crisp-input-text[@label='Town']/div/input[@ref='input']")]
        public IWebElement TownEdit;

        [FindsBy(How = How.XPath, Using = "//crisp-button[@click.delegate='confirmManualAddressSelection()']/span/BUTTON[@ref='button']")]
        public IWebElement ConfirmSelection;

        [FindsBy(How = How.CssSelector, Using = "body > ai-dialog-container:nth-child(4) > div > div > crisp-dialog > div > div.modal-content > crisp-input-bool > label")]
        public IWebElement RegisteredAddress;

        [FindsBy(How = How.XPath, Using = "//crisp-card-header[@class='au-target']/div/span[text()='Address details']")]
        public IWebElement AddressDetails;

        [FindsBy(How = How.CssSelector, Using = "body > ai-dialog-container:nth-child(4) > div > div > crisp-dialog > div > div.modal-content > crisp-input-object:nth-child(2) > div > label")]
        public IWebElement OfficeAddress;

        [FindsBy(How = How.XPath,
            Using = "//input[@type='text'][@class='select-dropdown'][@value='Labc Warranty, 2 Shore Lines Building, Shore Road, Merseyside, CH41 1AU']")]
        public IWebElement AddressDropdown;

        [FindsBy(How = How.XPath, Using = "//ul[@class='dropdown-content select-dropdown active']/li[2]")]
        public IWebElement AddressDropdownInput;

        [FindsBy(How = How.XPath, Using = "//crisp-input-text[@label='Email']/div/label[text()='Email']")]
        public IWebElement Email;

        [FindsBy(How = How.XPath, Using = "//crisp-input-text[@label='Email']/div/label")]
        public IList<IWebElement> EmailLabel;

        [FindsBy(How = How.XPath, Using = "//crisp-input-text[@label='Email']/div/input[@ref='input']")]
        public IWebElement EmailInput;

        [FindsBy(How = How.XPath, Using = "//crisp-input-text[@label='${label} Number']/div/label[text()='Phone Number']")]
        public IList<IWebElement> PhoneNumber;

        [FindsBy(How = How.XPath, Using = "//crisp-input-text[contains(@value.bind,'phoneNumber')]/div[@class='input-field']/input[@ref='input'][@type='text'][contains(@id,'crisp-input-text')]")]
        public IWebElement PhoneNumberInput;

        [FindsBy(How = How.XPath, Using = "//LABEL[@data-error.bind='error'][text()='Phone']")]
        public IWebElement PhoneExtension;

        [FindsBy(How = How.XPath, Using = "//*[@id='crisp - input - text - 35']")]
        public IWebElement PhoneExtensionInput;

        [FindsBy(How = How.XPath, Using = "//LABEL[@data-error.bind='error'][text()='Premier Guarantee account manager']")]
        public IWebElement PGAccountManager;

        [FindsBy(How = How.XPath, Using = "//crisp-list-item-avatar/img")]
        public IWebElement PGAccountPerson;

        [FindsBy(How = How.XPath, Using = "//LABEL[@data-error.bind='error'][text()='LABC account manager']")]
        public IWebElement LABCAccountManager;

        [FindsBy(How = How.XPath, Using = "//crisp-list-item-avatar/img")]
        public IWebElement LABCAccountPerson;

        [FindsBy(How = How.XPath, Using = "//crisp-button[@enabled.bind='isOfficeDataValid']/span/button[@class='au-target waves-effect waves-light btn']")]
        public IWebElement AddOfficeDataButton;

        [FindsBy(How = How.XPath, Using = "//LABEL[@class='au-target'][text()='Title']")]
        public IWebElement Title;

        [FindsBy(How = How.XPath, Using = "//LABEL[@class='au-target'][text()='Company Director']")]
        public IWebElement CompanyDirector;

        [FindsBy(How = How.XPath, Using = "//LABEL[@class='au-target'][text()='Purchasing Decision Maker']")]
        public IWebElement PurchasingDecisionMaker;

        [FindsBy(How = How.XPath, Using = "//crisp-input-text[@label='Job title']/div/label")]
        public IWebElement JobTitle;

        [FindsBy(How = How.XPath, Using = "//label[@click.delegate='labelClicked()'][@data-error='Primary Office is invalid']")]
        public IWebElement PrimaryOffice;

        [FindsBy(How = How.XPath, Using = "//li[@click.delegate='clicked(item)']/crisp-list-item")]
        public IWebElement ChooseOffice;

        [FindsBy(How = How.XPath, Using = "//crisp-button[@enabled.bind='hasSelection']/span/button")]
        public IWebElement SelectButton;

        [FindsBy(How = How.XPath, Using = "//crisp-button[@click.delegate='save()']/span//button[@ref='button']")]
        public IWebElement Add;

        [FindsBy(How = How.XPath, Using = "//i[@class='fa fa-plus add action au-target']")]
        public IWebElement Offices;

        [FindsBy(How = How.XPath, Using = "//BUTTON[@ref='button'][text()='Save']")]
        public IWebElement SaveButton;

        [FindsBy(How = How.CssSelector, Using = "body > div.custom-element > router-view > crisp-header > nav > div > crisp-header-avatar > img")]
        public IWebElement Imageperson;

        [FindsBy(How = How.XPath, Using = "//crisp-picker[@label='${label} Country code']/div/div/input")]
        public IWebElement PhoneCountry;

        [FindsBy(How = How.XPath, Using = "//div[1]/div/crisp-picker[@selected.bind='countryCode & validate']/div/div/input")]
        public IWebElement EmpPhoneCountry;

        [FindsBy(How = How.XPath, Using = "//ul[@class='dropdown-content select-dropdown active']/li/span[text()='United Kingdom']")]
        public IWebElement PhoneCountryDropdown;

        [FindsBy(How = How.XPath, Using = "//div[2]/div/crisp-picker[@selected.bind='countryCode & validate']/div/div/input")]
        public IWebElement EmpMobileCountry;

        [FindsBy(How = How.XPath, Using = "//ul[@class='dropdown-content select-dropdown active']/li/span[text()='United Kingdom']")]
        public IWebElement MobileCountryDropdown;

        [FindsBy(How = How.XPath, Using = "//LABEL[@class='au-target'][text()='Title']")]
        public IWebElement EmpTitleLabel;

        [FindsBy(How = How.XPath, Using = "//crisp-input-autocomplete[@label='Title']/div/input")]
        public IWebElement EmpTitleInput;

        [FindsBy(How = How.XPath, Using = "//crisp-input-text[@label='First name']//LABEL[@class='au-target'][text()='First name']")]
        public IWebElement EmpFirstNameLabel;

        [FindsBy(How = How.XPath, Using = "//crisp-input-text[@label='First name']//input")]
        public IWebElement EmpFirstNameInput;

        [FindsBy(How = How.XPath, Using = "//crisp-input-text[@label='Surname']//LABEL[@class='au-target'][text()='Surname']")]
        public IWebElement EmpSurNameLabel;

        [FindsBy(How = How.XPath, Using = "//crisp-input-text[@label='Surname']//input")]
        public IWebElement EmpSurNameInput;

        [FindsBy(How = How.XPath, Using = "//crisp-input-text[@label='Suffix']//LABEL[@class='au-target'][text()='Suffix']")]
        public IWebElement EmpSuffixLabel;

        [FindsBy(How = How.XPath, Using = "//crisp-input-text[@label='Suffix']//input")]
        public IWebElement EmpSuffixInput;

        [FindsBy(How = How.XPath, Using = "//crisp-input-text[@label='Email']//LABEL[@class='au-target'][text()='Email']")]
        public IWebElement EmpEmailLabel;

        [FindsBy(How = How.XPath, Using = "//crisp-input-text[@label='Email']//input")]
        public IWebElement EmpEmailidInput;

        [FindsBy(How = How.XPath, Using = "//crisp-input-bool[@label='Company Director']//LABEL[@class='au-target'][text()='Company Director']")]
        public IWebElement EmpCompanyDirector;

        [FindsBy(How = How.XPath, Using = "//crisp-input-bool[@label='Purchasing Decision Maker']//LABEL[@class='au-target'][text()='Purchasing Decision Maker']")]
        public IWebElement EmpPurchasingDecisionMaker;

        [FindsBy(How = How.XPath, Using = "//crisp-input-text[@label='Job title']//div//label")]
        public IWebElement EmpJobTitle;

        [FindsBy(How = How.XPath, Using = "//crisp-input-text[@label='Job title']//div//input")]
        public IWebElement EmpJobTitleInput;


        [FindsBy(How = How.XPath, Using = "//crisp-input-text[contains(@label,'Number')]/div/label[text()='Phone Number']")]
        public IWebElement EmpPhone;

        [FindsBy(How = How.XPath, Using = "//crisp-list[@list-title='Results of Search from Existing Companies in CRISP']/ul[@ref='theList']/li[2]")]
        public IWebElement ExistingCompaniesFromCrispDB;

        [FindsBy(How = How.XPath, Using = "//crisp-list[@list-title='Companies House matches']/ul/li[2]")]
        public IWebElement SelectCompanyFromCompaniesHouse;

        [FindsBy(How = How.XPath, Using = "//div[@class='title']/div[@class='au-target']")]
        public IWebElement SelectCompanyFromCompaniesHouseNotStriked;

        [FindsBy(How = How.XPath, Using = "//crisp-list[@list-title='Offices']//ul[@ref='theList']//li[@class='au-target collection-item']//crisp-list-item//a")]
        public IWebElement EditOfficeAddress;

        [FindsBy(How = How.CssSelector, Using = "crisp-card > crisp-card-actions > div > crisp-action-button > div > a > i")]
        public IWebElement EditOfficeAddressButton;
     
        [FindsBy(How = How.XPath, Using = "//button[text()='Enter Address Manually']")]
        public IWebElement EnterManuallyButton;

        [FindsBy(How = How.XPath, Using = "//LABEL[@data-error.bind='error'][text()='Address Line 1']")]
        public IWebElement AddressLine1;

        [FindsBy(How = How.XPath, Using = "//crisp-input-text[@label='Address Line 1']/div/input[@ref='input']")]
        public IWebElement AddressLine1EnterValue;

        [FindsBy(How = How.XPath, Using = "//crisp-tabs/div/ul/li[@class='au-target tab']/span/a[contains(text(),'Employees')]")]
        public IWebElement EmployeesOption;

        [FindsBy(How = How.XPath, Using = "//crisp-list[@list-title='Employees']/ul[@ref='theList']//li[@class='au-target collection-item']//crisp-list-item//a")]
        public IWebElement EditEmployees;

        [FindsBy(How = How.CssSelector, Using = "crisp-card > crisp-card-actions > div > crisp-action-button > div > a > i")]
        public IWebElement EditEmployeeButton;

        [FindsBy(How = How.XPath, Using = "//crisp-input-autocomplete[@label='Title']/div/input")]
        public IWebElement EditEmpTitle;

        [FindsBy(How = How.XPath, Using = "//crisp-input-text[@label='First name']/div/input")]
        public IWebElement EditEmpFirstName;

        [FindsBy(How = How.XPath, Using = "//crisp-input-text[@label='Surname']/div/input")]
        public IWebElement EditEmpSurName;

        [FindsBy(How = How.XPath, Using = "//crisp-input-text[@label='Suffix']/div/input")]
        public IWebElement EditEmpSuffix;

        [FindsBy(How = How.XPath, Using = "//crisp-input-text[@label='Email']/div/input")]
        public IWebElement EditEmpEmail;


        [FindsBy(How = How.XPath, Using = "//div/div[1]/div/crisp-input-text[contains(@value.bind,'phoneNumber')]/div[@class='input-field']/input[@ref='input'][@type='text'][contains(@id,'crisp-input-text')]")]
        public IWebElement EmpPhoneInput;


        [FindsBy(How = How.XPath, Using = "//crisp-input-number[@label='Phone Extension']/div/label[text()='Phone Extension']")]
        public IWebElement EmpPhoneExtension;

        [FindsBy(How = How.XPath, Using = "//crisp-input-number[@label='Phone Extension']/div/input")]
        public IWebElement EmpPhoneExtensionInput;

        [FindsBy(How = How.XPath, Using = "//LABEL[@data-error.bind='error'][text()='CSU account manager']")]
        public IWebElement CSUAccountManager;

        [FindsBy(How = How.XPath, Using = "//crisp-list-item-avatar/img")]
        public IWebElement CSUAccountPerson;

        [FindsBy(How = How.XPath, Using = "//crisp-button[1]/span/button[text()='Set Address']")]
        public IWebElement SetAddressButton;

        [FindsBy(How = How.XPath, Using = "//crisp-button/span/button[text()='Enter Address Using Postcode']")]
        public IWebElement EnterAddressUsingPostcodeButton;

        [FindsBy(How = How.CssSelector, Using = "body > ai-dialog-container:nth-child(4) > div > div > crisp-dialog > div > div.modal-content > crisp-input-bool:nth-child(4) > label")]
        public IWebElement DefaultAddress;

        [FindsBy(How = How.XPath, Using = "//crisp-input-text[contains(@label,'Number')]/div/label[text()='Mobile Number']")]
        public IWebElement EmpMobile;

        [FindsBy(How = How.XPath, Using = "//div/div[2]/div/crisp-input-text[contains(@value.bind,'phoneNumber')]/div[@class='input-field']/input[@ref='input'][@type='text'][contains(@id,'crisp-input-text')]")]
        public IWebElement EmpMobileInput;

        [FindsBy(How = How.XPath, Using = "//label[@click.delegate='labelClicked()'][@data-error='Primary Office is invalid']")]
        public IWebElement EmpPrimaryOffice;

        [FindsBy(How = How.XPath, Using = "//crisp-list[@source.two-way='offices']//ul//li[@click.delegate='clicked(item)'][@class='au-target collection-item']")]
        public IWebElement EmpChooseOffice;

        [FindsBy(How = How.XPath, Using = "//crisp-button[@click.delegate='save()']//button[text()='Add']")]
        public IWebElement AddEmp;
        //Edit Company Main Method 
        public void EditCompanyMain()
        {
            FillCompanyDetails();
            EditOfficeDetails();
            WaitForElement(SaveButton);
            SaveButton.Click();
        }

        public void FillCompanyDetails()
        {
            WaitForElement(CompanyNameInput);
            CompanyNameInput.Click();
            CompanyNameInput.Clear();
            CompanyNameInput.SendKeys("MDISTestCompany");
            EnterYearOfRegistration.Click();
            EnterYearOfRegistration.Clear();
            EnterYearOfRegistration.SendKeys("2018");
            EnterRegistrationNumber.Click();
            EnterRegistrationNumber.Clear();
            EnterRegistrationNumber.SendKeys("AB456123");
            WaitForElement(LegalFormType);
            LegalFormType.Click();
            WaitForElement(LegalFormTypeDropdown);
            LegalFormTypeDropdown.Click();
            WaitForElement(ClientSegmentation);
            ClientSegmentation.Click();
            WaitForElement(ClientSegmentationDropdown);
            ClientSegmentationDropdown.Click();
            FacebookElementEnter.Click();
            FacebookElementEnter.Clear();
            FacebookElementEnter.SendKeys("https://www.facebook.com/MdisTest");
            GooglePlusEnter.Click();
            GooglePlusEnter.Clear();
            GooglePlusEnter.SendKeys("https://plus.google.com/MdisTest");
            LinkdedinEnter.Click();
            LinkdedinEnter.Clear();
            LinkdedinEnter.SendKeys("https://www.linkedin.com/MdisTest");
            TwitterEnter.Click();
            TwitterEnter.Clear();
            TwitterEnter.SendKeys("@MDIsTest");
            WaitForElement(AddWebsiteButton);
            AddWebsiteButton.Click();
            WaitForElement(WebsiteAddress);
            WebsiteAddress.Click();
            WaitForElement(WebSiteInputField);
            WebSiteInputField.SendKeys("www.MdisTest.com");
            WaitForElement(AddButton);
            AddButton.Click();

            WaitForElement(NextButton);
            NextButton.Click();
        }
        public void EditSalesAndMarketingDetails()
        {
            WaitForElements(OptCalls);
            foreach (var eachCheckbox in OptCalls)
            {
                eachCheckbox.Click();
            }

            WaitForElement(NotedOnTPS);
            NotedOnTPS.Click();

            CloseSpinneronDiv();
            CloseSpinneronPage();

        }
        public void EditOfficeDetails()
        {
            OfficeNameInput.Click();
            OfficeNameInput.Clear();
            OfficeNameInput.SendKeys("MDIS Test Office");

            CloseSpinneronDiv();
            CloseSpinneronPage();

            WaitForElement(EditExistingOfficeAdress);
            EditExistingOfficeAdress.Click();

            CloseSpinneronDiv();
            CloseSpinneronPage();

            if (EditPostcodeLabel.Count > 0)
            {
                EditPostcodeLabel[0].Click();
            }
            EditPostcodeInput.Click();
            EditPostcodeInput.Clear();
            EditPostcodeInput.SendKeys("CH41 1AU");

            CloseSpinneronDiv();
            CloseSpinneronPage();

            if (AddressLine1EditLabel.Count > 0)
            {
                AddressLine1EditLabel[0].Click();
            }
            AddressLine1Edit.Click();
            AddressLine1Edit.Clear();
            AddressLine1Edit.SendKeys("Labc Warranty");

            CloseSpinneronDiv();
            CloseSpinneronPage();

            if (AddressLine2EditLabel.Count > 0)
            {
                AddressLine2EditLabel[0].Click();
            }
            AddressLine2Edit.Click();
            AddressLine2Edit.Clear();
            AddressLine2Edit.SendKeys("2 Shore Lines Building");

            CloseSpinneronDiv();
            CloseSpinneronPage();

            if (AddressLine3EditLabel.Count > 0)
            {
                AddressLine3EditLabel[0].Click();
            }
            AddressLine3Edit.Click();
            AddressLine3Edit.Clear();
            AddressLine3Edit.SendKeys("Shore Road");

            CloseSpinneronDiv();
            CloseSpinneronPage();

            if (TownEditLabel.Count > 0)
            {
                TownEditLabel[0].Click();
            }
            TownEdit.Click();
            TownEdit.Clear();
            TownEdit.SendKeys("Merseyside");

            WaitForElement(SetAddressButton);
            SetAddressButton.Click();
            //Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            if (EmailLabel.Count > 0)
            {
                WaitForElement(EmailLabel[0]);
                WaitForElementToClick(EmailLabel[0]);
                EmailLabel[0].Click();
            }
            EmailInput.Click();
            EmailInput.Clear();
            EmailInput.SendKeys("MDISTestEmail@mdinsurance.co.uk");
            WaitForElement(PhoneCountry);
            PhoneCountry.Click();
            WaitForElement(PhoneCountryDropdown);
            PhoneCountryDropdown.Click();

            if (PhoneNumber.Count > 0)
            {
                PhoneNumber[0].Click();
            }
            PhoneNumberInput.Clear();
            PhoneNumberInput.SendKeys("01483493725");

            CloseSpinneronDiv();
            CloseSpinneronPage();

            WaitForElement(AddOfficeDataButton);
            AddOfficeDataButton.Click();

            CloseSpinneronDiv();
            CloseSpinneronPage();

        }
        public void AddNewOfficeDetails()
        {
            WaitForElement(OfficeName);
            OfficeName.Click();
            OfficeNameInput.SendKeys("MDIS Test Office");
            WaitForElement(OfficeAddress);
            OfficeAddress.Click();
            WaitForElement(PostcodeInput);
            PostcodeInput.SendKeys("CH41 1AU");
            CloseSpinneronDiv();
            WaitForElement(AddressDropdown);
            AddressDropdown.Click();
            WaitForElement(AddressDropdownInput);
            AddressDropdownInput.Click();
            WaitForElement(SetAddressButton);
            SetAddressButton.Click();
            CloseSpinneronPage();
            CloseSpinneronDiv();
            Thread.Sleep(500);
            WaitForElement(RegisteredAddress);
            RegisteredAddress.Click();
            CloseSpinneronPage();
            CloseSpinneronDiv();
            Thread.Sleep(500);
            WaitForElement(DefaultAddress);
            DefaultAddress.Click();
            WaitForElement(Email);
            Email.Click();
            WaitForElement(EmailInput);
            EmailInput.SendKeys("MdisTest@mdinsurance.co.uk ");
            WaitForElement(PhoneCountry);
            WaitForElementToClick(PhoneCountry);
            PhoneCountry.Click();
            WaitForElement(PhoneCountryDropdown);
            PhoneCountryDropdown.Click();

            if (PhoneNumber.Count > 0)
            {
                PhoneNumber[0].Click();
            }
            WaitForElement(PhoneNumberInput);
            PhoneNumberInput.SendKeys("01483493735");
            WaitForElement(PGAccountManager);
            PGAccountManager.Click();
            //Thread.Sleep(500);
            WaitForElement(PGAccountPerson);
            WaitForElementToClick(PGAccountPerson);
            CloseSpinneronDiv();
            PGAccountPerson.Click();
            Thread.Sleep(500);
            CloseCrispCard();
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(500);
            WaitForElement(LABCAccountManager);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            WaitForElementToClick(LABCAccountManager);
            LABCAccountManager.Click();
            //Thread.Sleep(500);           
            CloseSpinneronDiv();
            WaitForElement(LABCAccountPerson);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            LABCAccountPerson.Click();
            CloseCrispCard();
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(500);
            WaitForElement(CSUAccountManager);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            CSUAccountManager.Click();
            //Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            WaitForElement(CSUAccountPerson);
            CloseSpinneronDiv();
            //Thread.Sleep(500);
            CSUAccountPerson.Click();
            CloseSpinneronDiv();
            CloseSpinneronPage();
            CloseCrispCard();
            Thread.Sleep(500);
            EditSalesAndMarketingDetails();
            CloseSpinneronDiv();
            CloseSpinneronPage();
            WaitForElement(AddOfficeDataButton);
            AddOfficeDataButton.Click();
            CloseSpinneronDiv();
            WaitForElement(NextButton);
            NextButton.Click();
        }
        public void EditEmpDetails()
        {
            EditEmployees.Click();
            WaitForElement(EditEmployeeButton);
            EditEmployeeButton.Click();
            WaitForElement(EditEmpTitle);
            EditEmpTitle.Click();
            EditEmpTitle.Clear();
            EditEmpTitle.SendKeys("Mr");
            EditEmpFirstName.Click();
            EditEmpFirstName.Clear();
            EditEmpFirstName.SendKeys("Sunil");
            EditEmpSurName.Click();
            EditEmpSurName.Clear();
            EditEmpSurName.SendKeys("Kumar");
            EditEmpSuffix.Click();
            EditEmpSuffix.Clear();
            EditEmpSuffix.SendKeys("K");
            EditEmpEmail.Click();
            EditEmpEmail.Clear();
            EditEmpEmail.SendKeys("sunil@mdinsurance.co.uk");

            WaitForElement(EmpPhone);
            EmpPhone.Click();
            EmpPhoneInput.Clear();
            EmpPhoneInput.SendKeys("01483493755");
            WaitForElement(EmpPhoneCountry);
            WaitForElementToClick(EmpPhoneCountry);
            EmpPhoneCountry.Click();

            WaitForElement(PhoneCountryDropdown);
            WaitForElementToClick(PhoneCountryDropdown);

            PhoneCountryDropdown.Click();

            WaitForElement(EmpPhoneExtension);
            EmpPhoneExtension.Click();
            WaitForElement(EmpPhoneExtensionInput);
            EmpPhoneExtensionInput.Clear();
            EmpPhoneExtensionInput.SendKeys("253");
            WaitForElement(EmpMobile);
            EmpMobile.Click();
            WaitForElement(EmpMobileInput);
            EmpMobileInput.Clear();
            EmpMobileInput.SendKeys("07834233955");
            WaitForElement(EmpMobileCountry);

            EmpMobileCountry.Click();

            WaitForElement(MobileCountryDropdown);

            MobileCountryDropdown.Click();


            WaitForElement(SaveButton);
            SaveButton.Click();

        }
        public void NewEmpDetails()
        {

            WaitForElement(EmpTitleLabel);
            EmpTitleLabel.Click();

            EmpTitleInput.SendKeys("Mr");

            WaitForElement(EmpFirstNameLabel);
            EmpFirstNameLabel.Click();
            EmpFirstNameInput.SendKeys("Sunil");

            WaitForElement(EmpSurNameLabel);
            EmpSurNameLabel.Click();
            WaitForElement(EmpSurNameInput);
            EmpSurNameInput.SendKeys("Kumar");

            WaitForElement(EmpSuffixLabel);
            EmpSuffixLabel.Click();
            WaitForElement(EmpSuffixInput);
            EmpSuffixInput.SendKeys("k");

            WaitForElement(EmpEmailLabel);
            EmpEmailLabel.Click();
            WaitForElement(EmpEmailidInput);
            EmpEmailidInput.SendKeys("sunil.kumar@mdinsurance.co.uk");

            EmpCompanyDirector.Click();
            EmpPurchasingDecisionMaker.Click();
            WaitForElement(EmpJobTitle);
            EmpJobTitle.Click();
            WaitForElement(EmpJobTitleInput);
            EmpJobTitleInput.SendKeys("Automation Test Engineer");

            WaitForElement(EmpPhone);
            EmpPhone.Click();
            EmpPhoneInput.SendKeys("01483493726");
            EmpPhoneCountry.Click();

            WaitForElement(PhoneCountryDropdown);

            PhoneCountryDropdown.Click();

            WaitForElement(EmpPhoneExtension);
            EmpPhoneExtension.Click();
            WaitForElement(EmpPhoneExtensionInput);
            EmpPhoneExtensionInput.SendKeys("752");
            WaitForElement(EmpMobile);
            EmpMobile.Click();
            WaitForElement(EmpMobileInput);
            EmpMobileInput.SendKeys("07764233944");
            WaitForElement(EmpMobileCountry);
            EmpMobileCountry.Click();

            WaitForElement(MobileCountryDropdown);

            MobileCountryDropdown.Click();

            WaitForElement(EmpPrimaryOffice);
            EmpPrimaryOffice.Click();

            WaitForElement(EmpChooseOffice);
            EmpChooseOffice.Click();

            WaitForElement(Offices);
            Offices.Click();

            WaitForElement(EmpChooseOffice);
            EmpChooseOffice.Click();

            WaitForElement(SelectButton);
            SelectButton.Click();
            //Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            WaitForElement(AddEmp);
            AddEmp.Click();

            CloseSpinneronDiv();
        }
    }
}

