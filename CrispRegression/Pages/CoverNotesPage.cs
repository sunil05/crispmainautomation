﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace CrispAutomation.Pages
{
     public class CoverNotesPage : Support.Pages
     {
        public IWebDriver wdriver;
        public CoverNotesPage(ISearchContext driver)
        {
            PageFactory.InitElements(driver, this);
        }
        [FindsBy(How = How.XPath, Using = "//a/span[text()='Cover Notes']")]
        public IWebElement CoverNotes;

        [FindsBy(How = How.XPath, Using = "//h2[contains(text(),'Cover Note Batches')]//a[text()='CREATE NEW COVER NOTE']")]
        public IWebElement CreateCoverNoteButton;

        [FindsBy(How = How.XPath, Using = "//h2[contains(text(),'Cover Note Batches')]")]
        public IWebElement CoverNoteBatches;

        [FindsBy(How = How.XPath, Using = "//h2[contains(text(),'Produce Cover Notes')]")]
        public IWebElement ProduceCoverNotes;

        [FindsBy(How = How.XPath, Using = "//table[@id='coverNotesTable']//tbody//tr")]
        public IList<IWebElement> CoverNotesPlots;

        [FindsBy(How = How.XPath, Using = "//table[@id='coverNotesTable']//tbody//tr//label[contains(@for,'AddCoverNotes')]")]
        public IList<IWebElement> CoverNotePlotsLabel;

        [FindsBy(How = How.XPath, Using = "//button[@type='submit'][contains(@class,'btnProduce')][text()='Produce']")]
        public IWebElement ProduceButton;

        [FindsBy(How = How.XPath, Using = "//section[@class='content']//div[@class='alert alert-danger validation-summary-errors']//ul//li")]
        public IList<IWebElement> ValidationErrors;

        public void CoverNoteMain()
        {            
            SurveyorLoginPage.SelectSite();
            VerifyCoverNoteErrors();
            CreatingCoverNote();
        }

      public void VerifyCoverNoteErrors()
      {
            WaitForElement(CoverNotes);
            CoverNotes.Click();
            
            CloseSpinnerOnSurveyorPage();
            
            WaitForElement(CoverNoteBatches);
            Assert.IsTrue(CoverNoteBatches.Displayed);
            WaitForElement(CreateCoverNoteButton);
            CreateCoverNoteButton.Click();
            WaitForElement(ProduceCoverNotes);
            Assert.IsTrue(ProduceCoverNotes.Displayed);
            WaitForElements(CoverNotesPlots);
            if(CoverNotesPlots.Count>0)
            {
                WaitForElements(CoverNotePlotsLabel);
                if(CoverNotePlotsLabel.Count>0)
                {
                    for(int i=0; i < CoverNotePlotsLabel.Count; i++)
                    {
                        CoverNotePlotsLabel[i].Click();
                    }
                    
                    CloseSpinnerOnSurveyorPage();
                    
                }
            }
            WaitForElement(ProduceButton);
            ProduceButton.Click();
            
            CloseSpinnerOnSurveyorPage();
            

            if(ValidationErrors.Count>0 )
            {
                ValidationErrors.Any(x => x.Text.Contains("CoverNotes can't be produced for the selected plots."));
                ValidationErrors.Any(x => x.Text.Contains("Payment validation failed due to not enough money available for site."));
            }
       }


        public void CreatingCoverNote()
        {
            Dashboardpage.CrispLoginMethod();           
            IssueCertificatePage.PayFeeBalance();
            Driver.Navigate().GoToUrl(ConfigurationManager.AppSettings["SurveyorUrl"]);
            
            CloseSpinnerOnSurveyorPage();
                     
            SurveyorLoginPage.SelectSite();
            VerifyCoverNoteErrors();
        }
     }
}
