﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.PageObjects;
using System;
using System.Collections.Generic;
using System.Diagnostics.Eventing.Reader;
using System.Linq;
using System.Security.Policy;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace CrispAutomation.Pages
{
    public class AddLeadPage : Support.Pages
    {
        public IWebDriver wdriver;

        public AddLeadPage(ISearchContext driver)
        {
            PageFactory.InitElements(driver, this);
            wdriver = (RemoteWebDriver)driver;
        }

        [FindsBy(How = How.XPath, Using = "//crisp-input-radio[@label='Brand']/div/ul/li/label[text()='Premier Guarantee']")]
        public IWebElement PGRadioButton;

        [FindsBy(How = How.XPath, Using = "//crisp-input-radio[@label='Brand']/div/ul/li/label[text()='LABC Warranty']")]
        public IWebElement LABCRadioButton;

        [FindsBy(How = How.CssSelector, Using = "crisp-input-object:nth-child(2) > div > label")]
        public IWebElement QuoteRecipientLabel;

        [FindsBy(How = How.CssSelector, Using = "li:nth-child(7) > crisp-list-item > div")]
        public IWebElement QuoteRecipient;

        [FindsBy(How = How.CssSelector, Using = "crisp-input-object:nth-child(3) > div > label")]
        public IWebElement QuoteRecipientOffice;

        [FindsBy(How = How.XPath, Using = "//crisp-list[@ref='list']/ul[@ref='theList']/li[2]")]
        public IWebElement ChooseQuoteRecipientOffice;

        [FindsBy(How = How.XPath, Using = "//crisp-input-object/div/label[text()='Site Address']")]
        public IWebElement AddressLabel;

        [FindsBy(How = How.XPath, Using = "//crisp-button/span/button[text()='Search']")]
        public IWebElement SearchButton;

        [FindsBy(How = How.XPath, Using = "//crisp-button/span/button[text()='Enter Address Using Postcode']")]
        public IWebElement EnterAddressUsingPostcodeButton;

        [FindsBy(How = How.CssSelector, Using = "crisp-input-text > div > label")]
        public IWebElement PostcodeSearchLabel;

        [FindsBy(How = How.CssSelector, Using = "crisp-input-text > div > input")]
        public IWebElement PostcodeInput;

        [FindsBy(How = How.XPath, Using = "//crisp-picker[@label='Address']/div/div/input[@class='select-dropdown']")]
        public IWebElement AddressDropdown;

        [FindsBy(How = How.XPath, Using = "//ul[@class='dropdown-content select-dropdown active']/li[1]")]
        public IWebElement AddressDropdownInput;

        [FindsBy(How = How.XPath, Using = "//crisp-button[1]/span/button[text()='Confirm Selection']")]
        public IWebElement ConfirmSelectionButton;

        [FindsBy(How = How.XPath, Using = "//crisp-button[1]/span/button[text()='Set Address']")]
        public IWebElement SetAddressButton;

        [FindsBy(How = How.XPath, Using = "//crisp-button[@click.delegate='wizard.goForward()']/span/button[text()='Next'][@class='au-target waves-effect waves-light btn-flat']")]
        public IWebElement NextButton;

        [FindsBy(How = How.CssSelector, Using = "crisp-action-button > div > a > i")]
        public IWebElement AddProductButton;

        [FindsBy(How = How.XPath, Using = "//ul[@ref='theList'][@class='au-target collection has-header has-filter']//li[contains(@class,'collection-item')]")]
        public IList<IWebElement> ProductList;

        [FindsBy(How = How.XPath, Using = "//ul[@ref='theList'][@class='au-target collection has-header has-filter']//li[contains(@class,'collection-item')]//crisp-list-item//a//crisp-list-item-title//div//span[contains(text(),'High Value')]")]
        public IList<IWebElement> HVProductList;

        [FindsBy(How = How.CssSelector, Using = "crisp-card-actions > div > crisp-button > span > button")]
        public IWebElement SelectButton;

        //Plots List 
        [FindsBy(How = How.XPath, Using = "//crisp-input-number[@label='Plots']//div//input")]
        public IList<IWebElement> PlotsInputList;

        //SalesPrice List 
        [FindsBy(How = How.XPath, Using = "//crisp-input-currency[@label='Estimate Sales Price']//div//input")]
        public IList<IWebElement> SalesPriceInputList;

        //ReconstructionCost List 
        [FindsBy(How = How.XPath, Using = "//crisp-input-currency[@label='Reconstruction Cost']//div//input")]
        public IList<IWebElement> ReconstructionInputList;

        //Number of Apartments List 
        [FindsBy(How = How.XPath, Using = "//crisp-input-number[@label='Number of Apartments']//div//input")]
        public IList<IWebElement> NumberOfApartmentsList;

        //CoverLength List 
        [FindsBy(How = How.XPath, Using = "//crisp-input-radio[@label='Cover length']//div//ul//li//label[text()='10']")]
        public IList<IWebElement> CoverLengthList;

        //CheckBoxes List 
        [FindsBy(How = How.XPath, Using = "//div[contains(@class,'au-target left width-49')]//crisp-input-bool//label")]
        public IList<IWebElement> CheckBoxesListLabel;

        [FindsBy(How = How.XPath, Using = "//crisp-wizard-step[@step-title='Products']//crisp-content//div//crisp-card//crisp-card-content//div//crisp-input-currency[@label='Contract cost'][@class='au-target']//div//label")]
        public IList<IWebElement> ContractCostLabels;

        [FindsBy(How = How.XPath, Using = "//crisp-wizard-step[@step-title='Products']//crisp-content//div//crisp-card//crisp-card-content//div//crisp-input-currency[@label='Contract cost'][@class='au-target']//div//label[@class='au-target active']//following-sibling::input")]
        public IList<IWebElement> ContractCostInput;        

        [FindsBy(How = How.CssSelector, Using = "div.au-target.list-title.has-filter > div > crisp-header-button > span > button")]
        public IWebElement EditRoleButton;

        [FindsBy(How = How.XPath, Using = "//table[@class='au-target']/tbody/tr[2]//td[2]/em[text()='Not selected']")]
        public IWebElement BuildernotSelected;

        [FindsBy(How = How.XPath, Using = "//ul[@ref='theList']/li/crisp-list-item/a/crisp-list-item-title/div/span[text()='Builder']")]
        public IWebElement BuilderRole;

        [FindsBy(How = How.XPath, Using = "//ul[@ref='theList']/li/crisp-list-item/a/crisp-list-item-title/div/span[text()='Developer']")]
        public IWebElement DeveloperRole;

        [FindsBy(How = How.XPath, Using = "//ul[@ref='theList']/li/crisp-list-item/a/crisp-list-item-title/div/span[text()='Sales Account Manager']")]
        public IWebElement SalesAccountManagerRole;

        [FindsBy(How = How.XPath, Using = "//crisp-list[@ref='listElm']//ul//li[@class='au-target collection-item']")]
        public IList<IWebElement> ChooseContactList;

        [FindsBy(How = How.XPath, Using = "//crisp-list[@ref='listElm']//ul//li[@class='au-target collection-item']")]
        public IList<IWebElement> ChooseEmployeeList;

        //crisp-list[@ref='listElm']//ul//li[@class='au-target collection-item']

        [FindsBy(How = How.CssSelector, Using = "crisp-input-text[label = 'Search'] > div > label")]
        public IWebElement SearchLabel;

        [FindsBy(How = How.CssSelector, Using = "crisp-input-text[label = 'Search'] > div > input")]
        public IWebElement SearchInput;

        [FindsBy(How = How.XPath, Using = "//contact-list/crisp-list[@ref='listElm']/ul[@ref='theList']/li[2]")]
        public IWebElement ChooseBuilder;

        [FindsBy(How = How.CssSelector, Using = "div:nth-child(1) > crisp-input-textarea > div > label")]
        public IWebElement DescriptionOfWorkLabel;

        [FindsBy(How = How.CssSelector, Using = "div:nth-child(1) > crisp-input-textarea > div > textarea")]
        public IWebElement DescriptionOfWorkInput;

        [FindsBy(How = How.XPath, Using = "//crisp-input-date[@label='Estimated construction start date']/div/label[text()='Estimated construction start date']")]
        public IWebElement ConstructionStartDateLabel;

        [FindsBy(How = How.XPath, Using = "//div[contains(@class,'picker--focused')]/div/div/div/div/div[2]/table[@class='picker__table']/tbody//tr//td/div[text()='10']")]
        public IWebElement ConstructionStartDateInput;

        [FindsBy(How = How.XPath, Using = "//crisp-input-date[@label='Estimated end date']/div/label[text()='Estimated end date']")]
        public IWebElement ConstructionEndDateLabel;

        [FindsBy(How = How.XPath, Using = "//div[contains(@class,'picker--focused')]/div/div/div/div/div[2]/table[@class='picker__table']/tbody//tr//td/div[contains(@class,'day--infocus')][text()='28']")]
        public IWebElement ConstructionEndDateInput;

        [FindsBy(How = How.XPath, Using = "//crisp-input-date[@label='Quote decision date']/div/label[text()='Quote decision date']")]
        public IWebElement DecesionDateLabel;

        [FindsBy(How = How.XPath, Using = "//div[contains(@class,'picker--focused')]/div/div/div/div/div[2]/table[@class='picker__table']/tbody//tr//td/div[contains(@class,'day--infocus')][text()='28']")]
        public IWebElement DecesionDateInput;

        [FindsBy(How = How.CssSelector, Using = "div:nth-child(3) > crisp-input-number:nth-child(1) > div > label")]
        public IWebElement NumberOffStoreysAboveGround;

        [FindsBy(How = How.CssSelector, Using = "div:nth-child(3) > crisp-input-number:nth-child(1) > div > input")]
        public IWebElement NumberOffStoreysAboveGroundInput;

        [FindsBy(How = How.CssSelector, Using = "div:nth-child(3) > crisp-input-number:nth-child(2) > div > label")]
        public IWebElement NumberOffStoreysBelowGround;

        [FindsBy(How = How.CssSelector, Using = "div:nth-child(3) > crisp-input-number:nth-child(2) > div > input")]
        public IWebElement NumberOffStoreysBelowGroundInput;

        [FindsBy(How = How.CssSelector, Using = "div:nth-child(3) > crisp-input-number:nth-child(3) > div > label")]
        public IWebElement TotalFloors;

        [FindsBy(How = How.CssSelector, Using = "div:nth-child(3) > crisp-input-number:nth-child(3) > div > input")]
        public IWebElement TotalFloorsInput;

        [FindsBy(How = How.XPath, Using = "//crisp-picker[@label='Current Planning Stage']/div/div/input")]
        public IWebElement CurrentPlanningStageDropdown;

        [FindsBy(How = How.XPath, Using = "//ul[@class='dropdown-content select-dropdown active']/li[3]/span[text()='Contract']")]
        public IWebElement CurrentPlanningStageDropdownInput;

        [FindsBy(How = How.XPath, Using = "//crisp-input-bool[@label='Includes conversion elements']/label")]
        public IWebElement ConversionElement;

        [FindsBy(How = How.XPath, Using = "//crisp-footer/nav/div/crisp-button[2]/span/button[text()='Save']")]
        public IWebElement SaveButton;

        [FindsBy(How = How.XPath, Using = "//ai-dialog-container[2]/div/div/crisp-dialog/div")]
        public IWebElement RatingDialogueWindow;

        [FindsBy(How = How.CssSelector, Using = "crisp-picker[label='Principal Occupation'] input[type='text']")]
        public IWebElement OccupationDrodpDown;

        [FindsBy(How = How.XPath, Using = "//ul[@class='dropdown-content select-dropdown active']/li[10]/span")]
        public IWebElement OccupationDrodpDownInput;

        [FindsBy(How = How.CssSelector, Using = "crisp-picker[label='Company Segment'] input[type='text']")]
        public IWebElement CompanySegmentDrodpDown;

        [FindsBy(How = How.XPath, Using = "//ul[@class='dropdown-content select-dropdown active']/li[3]/span")]
        public IWebElement CompanySegmentDownInput;

        [FindsBy(How = How.CssSelector, Using = "crisp-input-textarea[label='Technical Rating Comment']>div>label")]
        public IWebElement TechRatingComment;

        [FindsBy(How = How.CssSelector, Using = "crisp-input-textarea[label='Technical Rating Comment']>div>textarea")]
        public IWebElement TechRatingCommentText;

        [FindsBy(How = How.XPath, Using = "//crisp-footer/nav/div/crisp-footer-button[2]/button[text()='Save Rating']")]
        public IWebElement SaveRatingButton;

        //QuoteRecipientList
        
        [FindsBy(How = How.XPath, Using = "//crisp-card-content/div/div/div/crisp-input-text/div/label[text()='Search']")]
        public IWebElement SearchEmployeeLabel;

        [FindsBy(How = How.XPath, Using = "//crisp-card-content/div/div/div/crisp-input-text/div/input")]
        public IWebElement SearchEmployeeInput;

        [FindsBy(How = How.XPath, Using = "//crisp-list[@ref='listElm']//ul//li[@class='au-target collection-item']//crisp-list-item//a[contains(@class,'au-target row list-item-contents clickable hasAvatar')]//crisp-list-item-avatar[@title='Private individual']//img")]
        public IList<IWebElement> QuoteRecipientList;

        [FindsBy(How = How.XPath, Using = "//contact-list[@name.bind='search']/crisp-list[@source.bind='loadData']//crisp-loader[@class='au-target aurelia-hide']")]
        public IWebElement CrispLoaderHide;        

        [FindsBy(How = How.XPath, Using = "//crisp-dialog/div[@class='au-target modal dialog brand labc modal-has-header']")]
        public IWebElement NoRatingDialogueWindow;

        [FindsBy(How = How.XPath, Using = "//crisp-button[1]/span/button[text()='Yes']")]
        public IWebElement YesButton;

        [FindsBy(How = How.XPath, Using = "//crisp-picker[@label='Lead Site Rating'] // div//input[@class='select-dropdown']")]
        public IWebElement SiteRatingDropdown;

        [FindsBy(How = How.XPath, Using = "//ul[@class='dropdown-content select-dropdown active']/li[1]/span")]
        public IWebElement SiteRatingDropdownInput;

        //Company List 
        [FindsBy(How = How.XPath, Using = "//crisp-list[@ref='listElm']//ul[@ref='theList']//li[@class='au-target collection-item']")]
        public IList<IWebElement> CompanyList;
        
        //Employee List 
        [FindsBy(How = How.XPath, Using = "//ul[@ref='theList']//li//crisp-list-item")]
        public IList<IWebElement> EmpList;

        [FindsBy(How = How.CssSelector, Using = "crisp-card > crisp-card-actions > div > crisp-button:nth-child(1) > span > button")]
        public IWebElement ChooseCompanyButton;
        
        [FindsBy(How = How.XPath, Using = "//crisp-card-content[@class='au-target']//span/input[@type='text']")]
        public IWebElement SearchRoleLabel;

        [FindsBy(How = How.XPath, Using = "//crisp-button[@click.delegate='additionalButtonClicked(btn)'][@class='au-target']//button[text()='Add Person']")]
        public IWebElement AddPersonButton;

        [FindsBy(How = How.XPath, Using = "//crisp-button[@click.delegate='additionalButtonClicked(btn)'][@class='au-target']//button[text()='Add Company']")]
        public IWebElement AddCompanyButton;

        [FindsBy(How = How.XPath, Using = "//crisp-button[@click.delegate='additionalButtonClicked(btn)'][@class='au-target']//button[text()='Add Employee']")]
        public IWebElement AddEmployeeButton;

        [FindsBy(How = How.XPath, Using = "//crisp-wizard-step[@step-title='Roles']//tbody[@class='collection']//tr[@class='au-target collection-item']//td[@class='au-target fixed-width error']//em[text()='Not selected']")]
        public IList<IWebElement> MandetoryRole;

        [FindsBy(How = How.XPath, Using = "//crisp-wizard-step[@step-title='Roles']//div[@class='crisp-list-table-content au-target']//tbody[@class='collection']")]
        public IWebElement RolesTable;

        [FindsBy(How = How.XPath, Using = "//crisp-wizard-step[@step-title='Roles']//tbody[@class='collection']//tr[@class='au-target collection-item']//td[@class='au-target fixed-width error'][1]")]
        public IList<IWebElement> Roles;

        //Check Email Correspondence

        [FindsBy(How = How.XPath, Using = "//crisp-tabs[@ref='tabs']//div//ul/li[@class='au-target tab']//a[contains(text(),'Correspondence')]")]
        public IWebElement CorrespondenceTab;

        [FindsBy(How = How.XPath, Using = "//crisp-list[@title='Email Correspondence']//ul//li//crisp-list-item//a")]
        public IList<IWebElement> EmailList;

        [FindsBy(How = How.XPath, Using = "//view-email//crisp-card//crisp-card-content")]
        public IWebElement ViewEmail;

      
        public void PGDevelopmentInformationDetails()
        {
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            WaitForElement(PGRadioButton);
            WaitForElementToClick(PGRadioButton);
            PGRadioButton.Click();
            CloseSpinneronDiv();
            CloseSpinneronPage();
            DevelopmentInformation();
            ClickNextButton();
        }

        public void LABCDevelopmentInformationDetails()
        {
            CloseSpinneronDiv();
            CloseSpinneronPage();
            WaitForElement(LABCRadioButton);
            WaitForElementToClick(LABCRadioButton);
            LABCRadioButton.Click();
            CloseSpinneronDiv();
            CloseSpinneronPage();
            DevelopmentInformation();
            ClickNextButton();
        }
        public void DevelopmentInformation()
        {
            WaitForElement(QuoteRecipientLabel);
            QuoteRecipientLabel.Click();
            WaitForElement(SearchEmployeeLabel);    
            CloseSpinneronDiv();
            WaitForElements(QuoteRecipientList);
            if (QuoteRecipientList.Count > 0)
            {
                WaitForElement(QuoteRecipientList[0]);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                Thread.Sleep(500);
                WaitForElementToClick(QuoteRecipientList[0]);
                CloseSpinneronDiv();
                CloseSpinneronPage();               
                QuoteRecipientList[0].Click();               
                CloseCrispCard();
                CloseSpinneronDiv();
                CloseSpinneronPage();               
            }        
            CloseSpinneronPage();
            CloseSpinneronDiv();
            Thread.Sleep(500);
            WaitForElement(AddressLabel);
            WaitForElementToClick(AddressLabel);
            AddressLabel.Click();
            WaitForElement(EnterAddressUsingPostcodeButton);
            EnterAddressUsingPostcodeButton.Click();
            WaitForElement(PostcodeSearchLabel);
            PostcodeSearchLabel.Click();
            WaitForElement(PostcodeInput);
            PostcodeInput.SendKeys("CH41 5EY");
            WaitForElement(AddressDropdown);
            AddressDropdown.Click();
            WaitForElement(AddressDropdownInput);
            AddressDropdownInput.Click();
            WaitForElement(SetAddressButton);
            SetAddressButton.Click();
            CloseSpinneronDiv();
            CloseSpinneronPage();
            CloseCrispCard();
            CloseSpinneronDiv();
            CloseSpinneronPage();
           
        }
        public void LVSProductsDetails()
        {
            WaitForElement(AddProductButton);
            WaitForElementToClick(AddProductButton);
            AddProductButton.Click();
            CloseSpinneronDiv();
            WaitForElements(ProductList);
            //LABC Products List
            foreach (var eachproduct in ProductList)
            {
                eachproduct.Click();
            }
            SelectButton.Click();
            CloseSpinneronDiv();
            ProductsDetails();
            ClickNextButton();
        }
        public void HVSProductsDetails()
        {
            WaitForElement(AddProductButton);
            AddProductButton.Click();
            WaitForElements(ProductList);
            WaitForElement(ProductList[0]);
            WaitForElement(HVProductList[0]);
            //LABC and Pg HV Products List
            foreach (var eachproduct in HVProductList)
            {
                eachproduct.Click();
            }
            WaitForElement(SelectButton);
            SelectButton.Click();
            CloseSpinneronDiv();
            ProductsDetails();
            ClickNextButton();
        }

        public void ProductsDetails()
        {          
            WaitForElements(PlotsInputList);
            //Plots Input List
            foreach (var eachplotinput in PlotsInputList)
            {
                eachplotinput.SendKeys("1");

            }
            WaitForElements(NumberOfApartmentsList);
            //ApartmentList
            foreach (var eachapartment in NumberOfApartmentsList)
            {
                eachapartment.SendKeys("1");

            }
            WaitForElements(SalesPriceInputList);
            //Salesprice Input Cost
            foreach (var eachsalespriceinput in SalesPriceInputList)
            {
                eachsalespriceinput.SendKeys("500000");

            }
            WaitForElements(ReconstructionInputList);
            //Reconstruction Cost Input list
            foreach (var reconstructioninput in ReconstructionInputList)
            {
                reconstructioninput.SendKeys("250000");
            }
            //Thread.Sleep(500);
            WaitForElements(CoverLengthList);
            //CoverLength List
            foreach (var coverlenghthinput in CoverLengthList)
            {
                coverlenghthinput.Click();

            }
            if (CheckBoxesListLabel.Count > 0)
            {
                WaitForElements(CheckBoxesListLabel);
                //Checkboxes list 
                foreach (var eachchebox in CheckBoxesListLabel)
                {
                    if (eachchebox.Enabled == true)
                    {
                        eachchebox.Click();
                    }
                }
            }
            else
            {
                Assert.IsTrue(CheckBoxesListLabel.Count > 0, "Sub Covers are not displayed on the product");
            }
            if (ContractCostLabels.Count > 0)
            {
                WaitForElements(ContractCostLabels);
                for (int i = 0; i < ContractCostLabels.Count; i++)
                {
                    WaitForElement(ContractCostLabels[i]);
                    ContractCostLabels[i].Click();
                    CloseSpinneronDiv();
                    CloseSpinneronPage();
                    Thread.Sleep(500);
                    WaitForElement(ContractCostInput[i]);
                    ContractCostInput[i].SendKeys("1000");
                    CloseSpinneronDiv();
                    CloseSpinneronPage();
                    Thread.Sleep(500);
                }
            }
        }
        public void RolesDetails()
        {
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            WaitForElement(RolesTable);
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            if (MandetoryRole.Count > 0)
            {
                Thread.Sleep(500);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                WaitForElements(MandetoryRole);
                WaitForElements(Roles);
                Thread.Sleep(500);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                List<string> RoleNames = Roles.Select(i => i.Text).ToList();
                CloseSpinneronDiv();
                CloseSpinneronPage();
                for (int i = 0; i < RoleNames.Count; i++)
                {

                    if (RoleNames[i].Contains("Builder"))
                    {
                        AddLABCQuotePage.SetBuilderRole();
                        Thread.Sleep(500);
                        CloseCrispCard();
                        CloseChooseRolesCard();
                        CloseSpinneronDiv();
                        CloseSpinneronPage();
                        Thread.Sleep(500);
                    }
                    if (RoleNames[i].Contains("Developer"))
                    {
                        AddLABCQuotePage.SetDeveloperRole();
                        Thread.Sleep(500);
                        CloseCrispCard();
                        CloseChooseRolesCard();
                        CloseSpinneronDiv();
                        CloseSpinneronPage();
                        Thread.Sleep(500);
                    }
                    if (RoleNames[i].Contains("Sales Account Manager"))
                    {
                        AddLABCQuotePage.SetSalesAccountManagerRole();
                        Thread.Sleep(500);
                        CloseCrispCard();
                        CloseChooseRolesCard();
                        CloseSpinneronDiv();
                        CloseSpinneronPage();
                        Thread.Sleep(500);
                    }
                    if (RoleNames[i].Contains("Structural Referral Administrator"))
                    {
                        AddLABCQuotePage.SetStructurralReferralAdminRole();
                        Thread.Sleep(500);
                        CloseCrispCard();
                        CloseSpinneronDiv();
                        CloseSpinneronPage();
                        Thread.Sleep(500);
                    }
                    if (RoleNames[i].Contains("Design Review Administrator"))
                    {
                        AddLABCQuotePage.SetDesignReviewAdminRole();
                        Thread.Sleep(500);
                        CloseCrispCard();
                        CloseChooseRolesCard();
                        CloseSpinneronDiv();
                        CloseSpinneronPage();
                        Thread.Sleep(500);
                    }
                    if (RoleNames[i].Contains("Technical Administrator"))
                    {
                        AddLABCQuotePage.SetTechnicalAdministratorRole();
                        Thread.Sleep(500);
                        CloseCrispCard();
                        CloseChooseRolesCard();
                        CloseSpinneronDiv();
                        CloseSpinneronPage();
                        Thread.Sleep(500);
                    }
                    if (RoleNames[i].Contains("Building Control Provider"))
                    {
                        AddLABCQuotePage.SetBCProviderRole();
                        Thread.Sleep(500);
                        CloseCrispCard();
                        CloseChooseRolesCard();
                        CloseSpinneronDiv();
                        CloseSpinneronPage();
                        Thread.Sleep(500);
                    }
                    if (RoleNames[i].Contains("Surveyor"))
                    {
                        AddLABCQuotePage.SetSurveyorRole();
                        Thread.Sleep(500);
                        CloseCrispCard();
                        CloseChooseRolesCard();
                        CloseSpinneronDiv();
                        CloseSpinneronPage();
                        Thread.Sleep(500);
                    }
                    if (RoleNames[i].Contains("Design Review Surveyor"))
                    {
                        AddLABCQuotePage.SetDesignReviewSurveyorRole();
                        Thread.Sleep(500);
                        CloseCrispCard();
                        CloseChooseRolesCard();
                        CloseSpinneronDiv();
                        CloseSpinneronPage();
                        Thread.Sleep(500);
                    }
                    if (RoleNames[i].Contains("Hub Administrator"))
                    {
                        AddLABCQuotePage.SetHubAdministratorRole();
                        Thread.Sleep(500);
                        CloseCrispCard();
                        CloseChooseRolesCard();
                        CloseSpinneronDiv();
                        CloseSpinneronPage();
                        Thread.Sleep(500);
                    }
                }
            }
            ClickNextButton();
        }
        public void OtherDetails()
        {
            DescriptionOfWorkLabel.Click();
            WaitForElement(DescriptionOfWorkInput);
            DescriptionOfWorkInput.SendKeys("Test");
            WaitForElement(ConstructionStartDateLabel);
            ConstructionStartDateLabel.Click();
            WaitForElement(ConstructionStartDateInput);
            ConstructionStartDateInput.Click();
            WaitForElement(ConstructionEndDateLabel);
            ConstructionEndDateLabel.Click();
            WaitForElement(ConstructionEndDateInput);
            ConstructionEndDateInput.Click();
            WaitForElement(DecesionDateLabel);
            DecesionDateLabel.Click();
            WaitForElement(DecesionDateInput);
            DecesionDateInput.Click();
            WaitForElement(CurrentPlanningStageDropdown);
            CurrentPlanningStageDropdown.Click();
            WaitForElement(CurrentPlanningStageDropdownInput);
            CurrentPlanningStageDropdownInput.Click();
            WaitForElement(NumberOffStoreysAboveGround);
            NumberOffStoreysAboveGround.Click();
            WaitForElement(NumberOffStoreysAboveGroundInput);
            NumberOffStoreysAboveGroundInput.SendKeys("1");
            WaitForElement(NumberOffStoreysBelowGround);
            NumberOffStoreysBelowGround.Click();
            WaitForElement(NumberOffStoreysBelowGroundInput);
            NumberOffStoreysBelowGroundInput.SendKeys("1");
            WaitForElement(TotalFloors);
            TotalFloors.Click();
            WaitForElement(TotalFloorsInput);
            TotalFloorsInput.SendKeys("2");
            WaitForElement(ConversionElement);
            ConversionElement.Click();
            WaitForElement(SiteRatingDropdown);
            SiteRatingDropdown.Click();
            WaitForElement(SiteRatingDropdownInput);
            SiteRatingDropdownInput.Click();
           
        }
        public void CheckEmailCorrespondenceOnLead()
        {
            Dashboardpage.SelectLead();
            WaitForElement(CorrespondenceTab);
            CorrespondenceTab.Click();
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            Assert.IsTrue(EmailList.Count > 0, "Failed To allocate Email" );
            if(EmailList.Count>0)
            {
                WaitForElement(EmailList[0]);
                EmailList[0].Click();
                CloseSpinneronDiv();
                CloseSpinneronPage();
                Thread.Sleep(500);
                Assert.IsTrue(ViewEmail.Displayed, "Error Occured When User View the Allocated Email");
            }
        }

        public void ClickNextButton()
        {
            WaitForElement(AddLeadPage.NextButton);
            AddLeadPage.NextButton.Click();
        }       

    }
}
