﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.PageObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace CrispAutomation.Pages
{
    public class SurveyorDocsSiteInspection : Support.Pages
    {
        public IWebDriver wdriver;
        public SurveyorDocsSiteInspection(ISearchContext driver)
        {
            PageFactory.InitElements(driver, this);
            wdriver = (RemoteWebDriver)driver;
        }

        int Defects = SurveyorDocs.DefectsList.Count;

        [FindsBy(How = How.XPath, Using = "//h2[contains(text(),'Defects')]")]
        public IList<IWebElement> DefectsTab;

        public void CheckInspectionCondition()
        {
            WaitForElementonSurveyor(SiteRiskAssessmentPage.SRAStatus);
            WaitForElementonSurveyor(RefurbishmentAssessmentPage.RAStatus);
            WaitForElementonSurveyor(DesignReviewPage.DRStatus);
            var siteRiskAssessment = SiteRiskAssessmentPage.SRAStatus.Text;
            var refurbishmentAssessment = RefurbishmentAssessmentPage.RAStatus.Text;
            var designReviewAssessment = DesignReviewPage.DRStatus.Text;
   
            if (siteRiskAssessment.Contains("Outstanding"))
            {
                SiteRiskAssessmentPage.SiteRiskAssessmentMain();
            }
            if (refurbishmentAssessment.Contains("Outstanding"))
            {
                RefurbishmentAssessmentPage.RefurbishmentAssessmentMain();
            }
            if (designReviewAssessment.Contains("Outstanding"))
            {
                DesignReviewPage.DesignReviewMain();
            }

            SurveyorLoginPage.SelectSite();
            WaitForElement(EngineerReviewPage.ERStatus);
            var engineerReviewAssessment = EngineerReviewPage.ERStatus.Text;
            if (engineerReviewAssessment.Contains("Outstanding"))
            {
                EngineerReviewPage.EngineerReviewMain();
            }
        }
        //Opend Documents on SiteInspection
        public void OpenSurveyorDocsOnSiteInspection()
        {
            CheckInspectionCondition();
            SurveyorLoginPage.SelectSite();
            SiteInspectionPage.OpenSiteInspection();
            SurveyorDocs.OpenDocuments();
            Defects = SurveyorDocs.DefectsList.Count;
            //Thread.Sleep(1500);
            CloseSpinnerOnSurveyorDiv();
            CloseSpinnerOnSurveyorPage();
            //Thread.Sleep(1500);
            WaitForLoadElement(SiteInspectionPage.NextButton);
            SiteInspectionPage.NextButton.Click();
            //Thread.Sleep(1500);
            CloseSpinnerOnSurveyorDiv();
            CloseSpinnerOnSurveyorPage();
            //Thread.Sleep(1500);
            SiteInspectionPage.SignOffPlots();
            WaitForLoadElement(SiteInspectionPage.NextButton);
            SiteInspectionPage.NextButton.Click();
            //Thread.Sleep(1500);
            CloseSpinnerOnSurveyorDiv();
            CloseSpinnerOnSurveyorPage();
            //Thread.Sleep(1500);
            SiteInspectionPage.AssessmentScoresSection();
            //Thread.Sleep(1500);
            CloseSpinnerOnSurveyorDiv();
            CloseSpinnerOnSurveyorPage();
            //Thread.Sleep(1500);
            WaitForLoadElements(SiteInspectionPage.SiteStatusDropdowns);
            //Thread.Sleep(1000);
            for (int i = 0; i <= SiteInspectionPage.SiteStatusDropdowns.Count; i++)
            {
                IJavaScriptExecutor js = Driver as IJavaScriptExecutor;
                js.ExecuteScript("scroll(0,300);");
                WaitForElementonSurveyor(SiteInspectionPage.SiteStatusDropdowns[0]);
                SiteInspectionPage.SiteStatusDropdowns[0].Click();
                //Thread.Sleep(500);
                WaitForElementonSurveyor(SiteInspectionPage.SiteStatusDropdownInput);
                SiteInspectionPage.SiteStatusDropdownInput.Click();
                //Thread.Sleep(1500);
                CloseSpinnerOnSurveyorDiv();
                CloseSpinnerOnSurveyorPage();
                //Thread.Sleep(1500);
            }
            SiteInspectionPage.SaveSiteInspection();

        }
       
        //Close Documents on SiteInspection
        public void CloseSurveyorDocsOnSiteInspection()
        {
            CheckInspectionCondition();
            SurveyorLoginPage.SelectSite();
            SiteInspectionPage.OpenSiteInspection();

            CloseDocsonSiteInpection();

            //Thread.Sleep(1500);
            CloseSpinnerOnSurveyorDiv();
            CloseSpinnerOnSurveyorPage();
            //Thread.Sleep(1500);
            WaitForLoadElement(SiteInspectionPage.NextButton);
            SiteInspectionPage.NextButton.Click();
            //Thread.Sleep(1500);
            CloseSpinnerOnSurveyorDiv();
            CloseSpinnerOnSurveyorPage();
            //Thread.Sleep(1500);
            SiteInspectionPage.SignOffPlots();
            WaitForLoadElement(SiteInspectionPage.NextButton);
            SiteInspectionPage.NextButton.Click();
            //Thread.Sleep(1500);
            CloseSpinnerOnSurveyorDiv();
            CloseSpinnerOnSurveyorPage();
            //Thread.Sleep(1500);
            SiteInspectionPage.AssessmentScoresSection();
            //Thread.Sleep(1500);
            CloseSpinnerOnSurveyorDiv();
            CloseSpinnerOnSurveyorPage();
            //Thread.Sleep(1500);
            WaitForLoadElements(SiteInspectionPage.SiteStatusDropdowns);
            //Thread.Sleep(1000);
            for (int i = 0; i <= SiteInspectionPage.SiteStatusDropdowns.Count; i++)
            {
                IJavaScriptExecutor js = Driver as IJavaScriptExecutor;
                js.ExecuteScript("scroll(0,300);");
                WaitForElementonSurveyor(SiteInspectionPage.SiteStatusDropdowns[0]);
                SiteInspectionPage.SiteStatusDropdowns[0].Click();
                //Thread.Sleep(500);
                WaitForElementonSurveyor(SiteInspectionPage.SiteStatusDropdownInput);
                SiteInspectionPage.SiteStatusDropdownInput.Click();
                //Thread.Sleep(1500);
                CloseSpinnerOnSurveyorDiv();
                CloseSpinnerOnSurveyorPage();
                //Thread.Sleep(1500);
            }
            SiteInspectionPage.SaveSiteInspection();
        }
        //Not Closing Documents on SiteInspection
        public void NotClosingDocsonSiteInspection()
        {
            CheckInspectionCondition();
            SurveyorLoginPage.SelectSite();
            SiteInspectionPage.OpenSiteInspection();

            NotCloseDocsonSiteInpection();

            //Thread.Sleep(1500);
            CloseSpinnerOnSurveyorDiv();
            CloseSpinnerOnSurveyorPage();
            //Thread.Sleep(1500);
            WaitForLoadElement(SiteInspectionPage.NextButton);
            SiteInspectionPage.NextButton.Click();
            //Thread.Sleep(1500);
            CloseSpinnerOnSurveyorDiv();
            CloseSpinnerOnSurveyorPage();
            //Thread.Sleep(1500);
            SiteInspectionPage.SignOffPlots();
            WaitForLoadElement(SiteInspectionPage.NextButton);
            SiteInspectionPage.NextButton.Click();
            //Thread.Sleep(1500);
            CloseSpinnerOnSurveyorDiv();
            CloseSpinnerOnSurveyorPage();
            //Thread.Sleep(1500);
            SiteInspectionPage.AssessmentScoresSection();
            //Thread.Sleep(1500);
            CloseSpinnerOnSurveyorDiv();
            CloseSpinnerOnSurveyorPage();
            //Thread.Sleep(1500);
            WaitForLoadElements(SiteInspectionPage.SiteStatusDropdowns);
            //Thread.Sleep(1000);
            for (int i = 0; i <= SiteInspectionPage.SiteStatusDropdowns.Count; i++)
            {
                IJavaScriptExecutor js = Driver as IJavaScriptExecutor;
                js.ExecuteScript("scroll(0,300);");
                WaitForElementonSurveyor(SiteInspectionPage.SiteStatusDropdowns[0]);
                SiteInspectionPage.SiteStatusDropdowns[0].Click();
                //Thread.Sleep(500);
                WaitForElementonSurveyor(SiteInspectionPage.SiteStatusDropdownInput);
                SiteInspectionPage.SiteStatusDropdownInput.Click();
                //Thread.Sleep(1500);
                CloseSpinnerOnSurveyorDiv();
                CloseSpinnerOnSurveyorPage();
                //Thread.Sleep(1500);
            }
            SiteInspectionPage.SaveSiteInspection();
        
        }

        //Verify Closed Documents on SiteInspection 
        public void VerifyClosedSurveyorDocsOnSiteInspection()
        {

            SiteInspectionPage.OpenSiteInspection();
            SurveyorLoginPage.SelectSite();
            SurveyorDocs.ShowClosedDocuments();

            //Thread.Sleep(1500);
            CloseSpinnerOnSurveyorDiv();
            CloseSpinnerOnSurveyorPage();
            //Thread.Sleep(1500);
            WaitForLoadElement(SiteInspectionPage.NextButton);
            SiteInspectionPage.NextButton.Click();
            //Thread.Sleep(1500);
            CloseSpinnerOnSurveyorDiv();
            CloseSpinnerOnSurveyorPage();
            //Thread.Sleep(1500);
            SiteInspectionPage.SignOffPlots();
            WaitForLoadElement(SiteInspectionPage.NextButton);
            SiteInspectionPage.NextButton.Click();
            //Thread.Sleep(1500);
            CloseSpinnerOnSurveyorDiv();
            CloseSpinnerOnSurveyorPage();
            //Thread.Sleep(1500);
            SiteInspectionPage.AssessmentScoresSection();
            //Thread.Sleep(1500);
            CloseSpinnerOnSurveyorDiv();
            CloseSpinnerOnSurveyorPage();
            //Thread.Sleep(1500);
            WaitForLoadElements(SiteInspectionPage.SiteStatusDropdowns);
            //Thread.Sleep(1000);
            for (int i = 0; i <= SiteInspectionPage.SiteStatusDropdowns.Count; i++)
            {
                IJavaScriptExecutor js = Driver as IJavaScriptExecutor;
                js.ExecuteScript("scroll(0,300);");
                WaitForElementonSurveyor(SiteInspectionPage.SiteStatusDropdowns[0]);
                SiteInspectionPage.SiteStatusDropdowns[0].Click();
                //Thread.Sleep(500);
                WaitForElementonSurveyor(SiteInspectionPage.SiteStatusDropdownInput);
                SiteInspectionPage.SiteStatusDropdownInput.Click();
                //Thread.Sleep(1500);
                CloseSpinnerOnSurveyorDiv();
                CloseSpinnerOnSurveyorPage();
                //Thread.Sleep(1500);
            }
            SiteInspectionPage.SaveSiteInspection();
        }
        //Close Docs on SIte Inspection 
        public void CloseDocsonSiteInpection()
        {
            WaitForLoadElements(SurveyorDocs.DocsTabs);
            if (SurveyorDocs.DocsTabs.Count > 0)
            {
                // if it is open on Site Inspection or SRA Close all documents 
                for (int i = 0; i < SurveyorDocs.DocsTabs.Count; i++)
                {
                    var TabStatus = SurveyorDocs.DocsTabs[i].GetAttribute("aria-expanded");
                    Assert.IsTrue(TabStatus == "false");
                    if (SurveyorDocs.RaisedDesignDocPartOf.Contains("Inspection") || SurveyorDocs.RaisedDesignDocPartOf.Contains("Site Risk Assessment"))
                    {
                        SurveyorDocs.CloseDesignDocuments(i);
                    }
                    else
                    {
                        SurveyorDocs.NotClosingDesignDocuments(i);
                    }

                    if (SurveyorDocs.RaisedDesignDocPartOf.Contains("Inspection") || SurveyorDocs.RaisedDesignDocPartOf.Contains("Site Risk Assessment")|| SurveyorDocs.RaisedDesignDocPartOf.Contains("Design Review") || SurveyorDocs.RaisedDesignDocPartOf.Contains("Refurbishment Assessment") ||  SurveyorDocs.RaisedDesignDocPartOf.Contains("Engineering Review"))
                    {
                        SurveyorDocs.CloseCompletionDocs(i);
                    }

                    if (SurveyorDocs.RaisedDesignDocPartOf.Contains("Inspection") || SurveyorDocs.RaisedDesignDocPartOf.Contains("Site Risk Assessment"))
                    {
                        SurveyorDocs.CloseDesignItems(i);
                    }else
                    {
                        SurveyorDocs.NotClosingDesignItems(i);
                    }
                    if (SurveyorDocs.RaisedDesignDocPartOf.Contains("Inspection") || SurveyorDocs.RaisedDesignDocPartOf.Contains("Site Risk Assessment") || SurveyorDocs.RaisedDesignDocPartOf.Contains("Design Review") || SurveyorDocs.RaisedDesignDocPartOf.Contains("Refurbishment Assessment") || SurveyorDocs.RaisedDesignDocPartOf.Contains("Engineering Review"))
                    {
                        SurveyorDocs.CloseRiskItems(i);
                    }
                    if (SurveyorDocs.RaisedDesignDocPartOf.Contains("Inspection"))
                    {
                        SurveyorDocs.CloseDefects(i);
                    }
                }              
            }
        }

        //Not Close Docs on SIte Inspection 
        public void NotCloseDocsonSiteInpection()
        {
            WaitForLoadElements(SurveyorDocs.DocsTabs);
            if (SurveyorDocs.DocsTabs.Count > 0)
            {
                for (int i = 0; i < SurveyorDocs.DocsTabs.Count; i++)
                {
                    var TabStatus = SurveyorDocs.DocsTabs[i].GetAttribute("aria-expanded");
                    Assert.IsTrue(TabStatus == "false");
                    if (SurveyorDocs.RaisedDesignDocPartOf.Contains("Design Review") || SurveyorDocs.RaisedDesignDocPartOf.Contains("Refurbishment Assessment") || SurveyorDocs.RaisedDesignDocPartOf.Contains("Engineering Review"))
                    {
                        SurveyorDocs.NotClosingDesignDocuments(i);
                    }
                    if (SurveyorDocs.RaisedDesignDocPartOf.Contains("Design Review") || SurveyorDocs.RaisedDesignDocPartOf.Contains("Refurbishment Assessment") || SurveyorDocs.RaisedDesignDocPartOf.Contains("Engineering Review"))
                    {
                        SurveyorDocs.NotClosingDesignItems(i);
                    }
                    if (SurveyorDocs.RaisedDesignDocPartOf.Contains("Design Review")||SurveyorDocs.RaisedDesignDocPartOf.Contains("Refurbishment Assessment") || SurveyorDocs.RaisedDesignDocPartOf.Contains("Engineering Review")|| SurveyorDocs.RaisedDesignDocPartOf.Contains("Site Risk Assessment"))
                    {
                        Assert.IsTrue(DefectsTab.Count == 0);
                    }
                }
            }

        }
    }
}
