﻿using CrispAutomation.Features;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.PageObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace CrispAutomation.Pages
{
    public class TasksPage : Support.Pages
    {
        public IWebDriver wdriver;
        public TasksPage(ISearchContext driver)
        {
            PageFactory.InitElements(driver, this);
            wdriver = (RemoteWebDriver)driver;
        }
        [FindsBy(How = How.XPath, Using = "//crisp-tabs//div//ul[@ref='tabHeader']/li[@class='au-target tab']//span//a[contains(text(),'Tasks')]")]
        public IWebElement TasksTab;

        [FindsBy(How = How.XPath, Using = "//task-list//crisp-list[@list-title='Tasks']//div[@class='au-target list-title']//span//crisp-header-button[@btn-title='Refresh your results']//button")]
        public IWebElement TasksRefreshButton;

        [FindsBy(How = How.XPath, Using = "//task-list//crisp-list[@list-title='Tasks']//ul[@ref='theList']//li[contains(@class,'au-target collection-item')]")]
        public IList<IWebElement> TasksList;

        [FindsBy(How = How.XPath, Using = "//task-list//crisp-list[@list-title='Tasks']//ul[@ref='theList']//li[contains(@class,'au-target collection-item')]//crisp-list-item-title//div[@class='title']//div[@class='crisp-row']//div[1]")]
        public IList<IWebElement> TasksTitle;

        [FindsBy(How = How.XPath, Using = "//task-list//div//crisp-card-actions[@class='au-target']//div[@class='card-action']//crisp-button[@click.delegate='showFilter()'][@class='au-target']//button[@class='au-target waves-effect waves-light btn-flat']")]
        public IWebElement ShowFilterButton;

        [FindsBy(How = How.XPath, Using = "//crisp-card[@class='au-target']//crisp-card-content//div[@class='card-content']//crisp-input-bool[@value.two-way='includeClosed']")]
        public IWebElement IncludeClosedTasks;

        [FindsBy(How = How.XPath, Using = "//crisp-card-actions[@class='au-target']//div//crisp-button[@click.delegate='doFilter()']//span/button[@class='au-target waves-effect waves-light btn-flat']")]
        public IWebElement FilterButton;

        

        public void OpenTasks()
        {
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(500);
            WaitForElement(TasksTab);
            TasksTab.Click();
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(500);
            WaitForElement(TasksRefreshButton);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            WaitForElementToClick(TasksRefreshButton);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            TasksRefreshButton.Click();
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(500);
        }

        public void TaskRefresh()
        {
            //Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(500);
            WaitForElement(TasksRefreshButton);
            WaitForElementToClick(TasksRefreshButton);
            TasksRefreshButton.Click();
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(500);
        }
      
        public void TasksCreatedWhenSubmitAQuote()
        {
            if (TasksList.Count == 0)
            {
                OpenTasks();
                Thread.Sleep(500);
                CloseSpinneronDiv();
                CloseSpinneronPage();
            }
            else
            {
                TaskRefresh();
                Thread.Sleep(500);
                CloseSpinneronDiv();
                CloseSpinneronPage();
            }
            try
            {
                Thread.Sleep(500);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                WaitForElements(TasksTitle);
                Assert.IsTrue(TasksTitle.Any(x => x.Text.Contains("Make Quote Decision")), "Failed to Make Quote Decision Task");
            }
            catch(Exception ex)
            {
                throw new Exception("Failed to Create tasks when application submitted", ex);
            }
            List<String> TastNames = TasksTitle.Select(i => i.Text.ToString()).ToList(); 
            if(TastNames.Count > 0 )
            {
                for(int i = 0; i< TastNames.Count;  i++)
                {
                    if(TastNames[i].Contains("Make Quote Decision"))
                    {
                        var temp = $"//task-list//crisp-list[@list-title='Tasks']//ul[@ref='theList']//li[contains(@class,'au-target collection-item')]//crisp-list-item-title//div[@class='title']//div[@class='crisp-row']//div[1][contains(text(),'{TastNames[i]}')]//parent::div//parent::div//parent::crisp-list-item-title//following-sibling::crisp-list-item-details//div[@class='crisp-row']//div[2]";
                        var Status = Driver.FindElement(By.XPath(temp));
                        Assert.IsTrue(Status.Text.Contains("Open"), "Failed to display the task status Open");                        
                    }
                }
            }
        }

        public void TasksCreatedWhenSendAQuote()
        {
            if (TasksList.Count == 0)
            {
                OpenTasks();
                Thread.Sleep(500);
                CloseSpinneronDiv();
                CloseSpinneronPage();
            }
            else
            {
                TaskRefresh();
                Thread.Sleep(500);
                CloseSpinneronDiv();
                CloseSpinneronPage();
            }
            try
            {
                Thread.Sleep(500);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                WaitForElements(TasksTitle);
                Assert.IsTrue(TasksTitle.Any(x => x.Text.Contains("Notify Sales Account Manager of New Quote")),"Failed to create Sales Account Manager of New Quote Task");
                Assert.IsTrue(TasksTitle.Any(x => x.Text.Contains("Chase Quote Decision from Client")),"Failed to create Chase Quote Decision From Client Task");
            }
            catch (Exception ex)
            {
                throw new Exception("Failed to Crate Tasks when send a quote", ex);
            }
          
            List<String> TastNames = TasksTitle.Select(i => i.Text.ToString()).ToList();
            if (TastNames.Count > 0)
            {
                for (int i = 0; i < TastNames.Count; i++)
                {
                    if (TastNames[i].Contains("Notify Sales Account Manager of New Quote"))
                    {
                        var temp = $"//task-list//crisp-list[@list-title='Tasks']//ul[@ref='theList']//li[contains(@class,'au-target collection-item')]//crisp-list-item-title//div[@class='title']//div[@class='crisp-row']//div[1][contains(text(),'{TastNames[i]}')]//parent::div//parent::div//parent::crisp-list-item-title//following-sibling::crisp-list-item-details//div[@class='crisp-row']//div[2]";                        var Status = Driver.FindElement(By.XPath(temp));
                        Assert.IsTrue(Status.Text.Contains("Open"), "Failed to display the task status Open");
                    }
                    if (TastNames[i].Contains("Chase Quote Decision from Client"))
                    {
                        var temp = $"//task-list//crisp-list[@list-title='Tasks']//ul[@ref='theList']//li[contains(@class,'au-target collection-item')]//crisp-list-item-title//div[@class='title']//div[@class='crisp-row']//div[1][contains(text(),'{TastNames[i]}')]//parent::div//parent::div//parent::crisp-list-item-title//following-sibling::crisp-list-item-details//div[@class='crisp-row']//div[2]";                        var Status = Driver.FindElement(By.XPath(temp));
                        Assert.IsTrue(Status.Text.Contains("Open"), "Failed to display the task status Open");
                    }
                }
            }
        }
        public void TasksClosedWhenSendAQuote()
        {
            if (TasksList.Count == 0)
            {
                OpenTasks();
                Thread.Sleep(500);
                CloseSpinneronDiv();
                CloseSpinneronPage();
            }
            else
            {
                TaskRefresh();
                Thread.Sleep(500);
                CloseSpinneronDiv();
                CloseSpinneronPage();
            }
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            WaitForElements(TasksTitle);
            Assert.IsFalse(TasksTitle.Any(x => x.Text.Contains("Make Quote Decision")), "Failed to close Make Quote Decision Task");
        }

        public void TasksCreatedWhenAcceptAQuote()
        {
            if (TasksList.Count == 0)
            {
                OpenTasks();
                Thread.Sleep(500);
                CloseSpinneronDiv();
                CloseSpinneronPage();
            }
            else
            {
                TaskRefresh();
                Thread.Sleep(500);
                CloseSpinneronDiv();
                CloseSpinneronPage();
            }
            try
            {
                Thread.Sleep(500);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                WaitForElements(TasksTitle);
                Assert.IsTrue(TasksTitle.Any(x => x.Text.Contains("Notify Sales Account Manager of New Quote")),"Failed to create Sales Account Manager of New Quote Task");
                Assert.IsTrue(TasksTitle.Any(x => x.Text.Contains("New Order Email Sent")),"Failed to create New Order Email Sent Task");
                Assert.IsTrue(TasksTitle.Any(x => x.Text.Contains("Make Surveyor Contact")),"Failed to create Make Surveyor Contact Task");
                Assert.IsTrue(TasksTitle.Any(x => x.Text.Contains("Complete Site Risk Assessment")),"Failed to create Complete SiteRisk Assessment Task");
                Assert.IsTrue(TasksTitle.Any(x => x.Text.Contains("Chase Outstanding Design Documents")),"Failed to create Chase Outstanding Design Documents Task");
                if (Statics.OrderNumber.Contains("PL-PG")&&(Statics.BCProducts.Contains("Building Control")))
                {
                    Assert.IsTrue(TasksTitle.Any(x => x.Text.Contains("Manually Trigger Start of Design Review Assessment")),"Failed to create Manually Trigger Start of Design Review Assessment Task");
                    Assert.IsTrue(TasksTitle.Any(x => x.Text.Contains("Chase O/S Documents To Issue Initial Notice")), "Failed to create Chase O/S Documents To Issue Initial Notice Task");
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Failed to Crate Tasks When User Accept the Order", ex);
            }

            List<String> TastNames = TasksTitle.Select(i => i.Text.ToString()).ToList();
            if (TastNames.Count > 0)
            {
                for (int i = 0; i < TastNames.Count; i++)
                {
                    if (TastNames[i].Contains("Notify Sales Account Manager of New Quote"))
                    {
                        var temp = $"//task-list//crisp-list[@list-title='Tasks']//ul[@ref='theList']//li[contains(@class,'au-target collection-item')]//crisp-list-item-title//div[@class='title']//div[@class='crisp-row']//div[1][contains(text(),'{TastNames[i]}')]//parent::div//parent::div//parent::crisp-list-item-title//following-sibling::crisp-list-item-details//div[@class='crisp-row']//div[2]";
                        var Status = Driver.FindElement(By.XPath(temp));
                        Assert.IsTrue(Status.Text.Contains("Open"), "Failed to display the task status Open");
                    }
                    if (TastNames[i].Contains("New Order Email Sent"))
                    {
                        var temp = $"//task-list//crisp-list[@list-title='Tasks']//ul[@ref='theList']//li[contains(@class,'au-target collection-item')]//crisp-list-item-title//div[@class='title']//div[@class='crisp-row']//div[1][contains(text(),'{TastNames[i]}')]//parent::div//parent::div//parent::crisp-list-item-title//following-sibling::crisp-list-item-details//div[@class='crisp-row']//div[2]";
                        var Status = Driver.FindElement(By.XPath(temp));
                        Assert.IsTrue(Status.Text.Contains("Open"), "Failed to display the task status Open");
                    }
                    
                    if (TastNames[i].Contains("Make Surveyor Contact"))
                    {
                        var temp = $"//task-list//crisp-list[@list-title='Tasks']//ul[@ref='theList']//li[contains(@class,'au-target collection-item')]//crisp-list-item-title//div[@class='title']//div[@class='crisp-row']//div[1][contains(text(),'{TastNames[i]}')]//parent::div//parent::div//parent::crisp-list-item-title//following-sibling::crisp-list-item-details//div[@class='crisp-row']//div[2]";
                        var Status = Driver.FindElement(By.XPath(temp));
                        Assert.IsTrue(Status.Text.Contains("Open"), "Failed to display the task status Open");
                    }
                    if (TastNames[i].Contains("Complete Site Risk Assessment"))
                    {
                        var temp = $"//task-list//crisp-list[@list-title='Tasks']//ul[@ref='theList']//li[contains(@class,'au-target collection-item')]//crisp-list-item-title//div[@class='title']//div[@class='crisp-row']//div[1][contains(text(),'{TastNames[i]}')]//parent::div//parent::div//parent::crisp-list-item-title//following-sibling::crisp-list-item-details//div[@class='crisp-row']//div[2]";
                        var Status = Driver.FindElement(By.XPath(temp));
                        Assert.IsTrue(Status.Text.Contains("Open"), "Failed to display the task status Open");
                    }
                    if (TastNames[i].Contains("Chase Outstanding Design Documents"))
                    {
                        var temp = $"//task-list//crisp-list[@list-title='Tasks']//ul[@ref='theList']//li[contains(@class,'au-target collection-item')]//crisp-list-item-title//div[@class='title']//div[@class='crisp-row']//div[1][contains(text(),'{TastNames[i]}')]//parent::div//parent::div//parent::crisp-list-item-title//following-sibling::crisp-list-item-details//div[@class='crisp-row']//div[2]";
                        var Status = Driver.FindElement(By.XPath(temp));
                        Assert.IsTrue(Status.Text.Contains("Open"), "Failed to display the task status Open");
                    }
                }
            }
        }
        public void TasksClosedWhenAcceptAQuote()
        {
            if (TasksList.Count == 0)
            {
                OpenTasks();
                Thread.Sleep(500);
                CloseSpinneronDiv();
                CloseSpinneronPage();
            }
            else
            {
                TaskRefresh();
                Thread.Sleep(500);
                CloseSpinneronDiv();
                CloseSpinneronPage();
            }
            WaitForElements(TasksTitle);
            Assert.IsFalse(TasksTitle.Any(x => x.Text.Contains("Make Quote Decision")), "Failed to close Make Quote Decision Task");
            Assert.IsFalse(TasksTitle.Any(x => x.Text.Contains("Chase Quote Decision from Client")), "Failed to close Chase Quote Decision from Client Task");
        }
        

        public void VerifyClosedTasks()
        {
            if (TasksList.Count == 0)
            {
                OpenTasks();
                Thread.Sleep(500);
                CloseSpinneronDiv();
                CloseSpinneronPage();
            }
            else
            {
                TaskRefresh();
                Thread.Sleep(500);
                CloseSpinneronDiv();
                CloseSpinneronPage();
            }
            WaitForElement(ShowFilterButton);
            WaitForElementToClick(ShowFilterButton);
            ShowFilterButton.Click();
            CloseSpinneronDiv();
            CloseSpinneronPage();
            WaitForElement(IncludeClosedTasks);
            WaitForElementToClick(IncludeClosedTasks);
            IncludeClosedTasks.Click();
            CloseSpinneronDiv();
            CloseSpinneronPage();
            WaitForElement(FilterButton);
            WaitForElementToClick(FilterButton);
            FilterButton.Click();
            CloseSpinneronDiv();
            CloseSpinneronPage();
            List<String> TastNames = TasksTitle.Select(i => i.Text.ToString()).ToList();
            if (TastNames.Count > 0)
            {
                for (int i = 0; i < TastNames.Count; i++)
                {
                    if (TastNames[i].Contains("Notify Sales Account Manager of New Quote"))
                    {
                        var temp = $"//task-list//crisp-list[@list-title='Tasks']//ul[@ref='theList']//li[contains(@class,'au-target collection-item')]//crisp-list-item-title//div[@class='title']//div[@class='crisp-row']//div[1][contains(text(),'{TastNames[i]}')]//parent::div//parent::div//parent::crisp-list-item-title//following-sibling::crisp-list-item-details//div[@class='crisp-row']//div[2]";
                        var Status = Driver.FindElement(By.XPath(temp));
                        Assert.IsTrue(Status.Text.Contains("Closed"), "Failed to display the task status Closed");
                    }
                    if (TastNames[i].Contains("New Order Email Sent"))
                    {
                        var temp = $"//task-list//crisp-list[@list-title='Tasks']//ul[@ref='theList']//li[contains(@class,'au-target collection-item')]//crisp-list-item-title//div[@class='title']//div[@class='crisp-row']//div[1][contains(text(),'{TastNames[i]}')]//parent::div//parent::div//parent::crisp-list-item-title//following-sibling::crisp-list-item-details//div[@class='crisp-row']//div[2]";
                        var Status = Driver.FindElement(By.XPath(temp));
                        Assert.IsTrue(Status.Text.Contains("Closed"), "Failed to display the task status Closed");
                    }

                    if (TastNames[i].Contains("Make Surveyor Contact"))
                    {
                        var temp = $"//task-list//crisp-list[@list-title='Tasks']//ul[@ref='theList']//li[contains(@class,'au-target collection-item')]//crisp-list-item-title//div[@class='title']//div[@class='crisp-row']//div[1][contains(text(),'{TastNames[i]}')]//parent::div//parent::div//parent::crisp-list-item-title//following-sibling::crisp-list-item-details//div[@class='crisp-row']//div[2]";
                        var Status = Driver.FindElement(By.XPath(temp));
                        Assert.IsTrue(Status.Text.Contains("Closed"), "Failed to display the task status Closed");
                    }
                    if (TastNames[i].Contains("Complete Site Risk Assessment"))
                    {
                        var temp = $"//task-list//crisp-list[@list-title='Tasks']//ul[@ref='theList']//li[contains(@class,'au-target collection-item')]//crisp-list-item-title//div[@class='title']//div[@class='crisp-row']//div[1][contains(text(),'{TastNames[i]}')]//parent::div//parent::div//parent::crisp-list-item-title//following-sibling::crisp-list-item-details//div[@class='crisp-row']//div[2]";
                        var Status = Driver.FindElement(By.XPath(temp));
                        Assert.IsTrue(Status.Text.Contains("Closed"), "Failed to display the task status Closed");
                    }
                    if (TastNames[i].Contains("Chase Outstanding Design Documents"))
                    {
                        var temp = $"//task-list//crisp-list[@list-title='Tasks']//ul[@ref='theList']//li[contains(@class,'au-target collection-item')]//crisp-list-item-title//div[@class='title']//div[@class='crisp-row']//div[1][contains(text(),'{TastNames[i]}')]//parent::div//parent::div//parent::crisp-list-item-title//following-sibling::crisp-list-item-details//div[@class='crisp-row']//div[2]";
                        var Status = Driver.FindElement(By.XPath(temp));
                        Assert.IsTrue(Status.Text.Contains("Closed"), "Failed to display the task status Closed");
                    }
                }
            }
        }
        public void TasksCreatedWhenIntialNoticeSent()
        {
            if (TasksList.Count == 0)
            {
                OpenTasks();
                Thread.Sleep(500);
                CloseSpinneronDiv();
                CloseSpinneronPage();
            }
            else
            {
                TaskRefresh();
                Thread.Sleep(500);
                CloseSpinneronDiv();
                CloseSpinneronPage();
            }
            try
            {
                Thread.Sleep(500);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                WaitForElements(TasksTitle);
                Assert.IsTrue(TasksTitle.Any(x => x.Text.Contains("Notify Sales Account Manager of New Quote")), "Failed to create Sales Account Manager of New Quote Task");
                Assert.IsTrue(TasksTitle.Any(x => x.Text.Contains("New Order Email Sent")), "Failed to create New Order Email Sent Task");
                if (Statics.BCProducts.Any(x => x.Contains("Bulding Control")))
                {
                    Assert.IsTrue(TasksTitle.Any(x => x.Text.Contains("Water Consultation Required")), "Failed to create Water Consultation Required Task");
                    Assert.IsTrue(TasksTitle.Any(x => x.Text.Contains("Fire Consultation Required")), "Failed to create Fire Consultation Required Task");
                }
                Assert.IsTrue(TasksTitle.Any(x => x.Text.Contains("Make Surveyor Contact")), "Failed to create Make Surveyor Contact Task");
                Assert.IsTrue(TasksTitle.Any(x => x.Text.Contains("Record Response From Local Authority For Initial Notice")),"Failed to create Record Response From Local Authority For Initial Notice Task");
                Assert.IsTrue(TasksTitle.Any(x => x.Text.Contains("Complete Site Risk Assessment")),"Failed to create Complete Site Risk Assessment Task");
                Assert.IsTrue(TasksTitle.Any(x => x.Text.Contains("Chase Outstanding Design Documents")), "Failed to create Chase Outstanding Design Documents Task");

                if (Statics.OrderNumber.Contains("PL-PG") && (Statics.BCProducts.Contains("Building Control")))
                {
                    Assert.IsTrue(TasksTitle.Any(x => x.Text.Contains("Manually Trigger Start of Design Review Assessment")));                    
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Failed to Crate Tasks When Initial Notice Sent", ex);
            }

            List<String> TastNames = TasksTitle.Select(i => i.Text.ToString()).ToList();
            if (TastNames.Count > 0)
            {
                for (int i = 0; i < TastNames.Count; i++)
                {
                    if (TastNames[i].Contains("Notify Sales Account Manager of New Quote"))
                    {
                        var temp = $"//task-list//crisp-list[@list-title='Tasks']//ul[@ref='theList']//li[contains(@class,'au-target collection-item')]//crisp-list-item-title//div[@class='title']//div[@class='crisp-row']//div[1][contains(text(),'{TastNames[i]}')]//parent::div//parent::div//parent::crisp-list-item-title//following-sibling::crisp-list-item-details//div[@class='crisp-row']//div[2]";
                        var Status = Driver.FindElement(By.XPath(temp));
                        Assert.IsTrue(Status.Text.Contains("Open"), "Failed to display the task status Open");
                    }
                    if (TastNames[i].Contains("New Order Email Sent"))
                    {
                        var temp = $"//task-list//crisp-list[@list-title='Tasks']//ul[@ref='theList']//li[contains(@class,'au-target collection-item')]//crisp-list-item-title//div[@class='title']//div[@class='crisp-row']//div[1][contains(text(),'{TastNames[i]}')]//parent::div//parent::div//parent::crisp-list-item-title//following-sibling::crisp-list-item-details//div[@class='crisp-row']//div[2]";
                        var Status = Driver.FindElement(By.XPath(temp));
                        Assert.IsTrue(Status.Text.Contains("Open"), "Failed to display the task status Open");
                    }
                    if (TastNames[i].Contains("Water Consultation Required"))
                    {
                        var temp = $"//task-list//crisp-list[@list-title='Tasks']//ul[@ref='theList']//li[contains(@class,'au-target collection-item')]//crisp-list-item-title//div[@class='title']//div[@class='crisp-row']//div[1][contains(text(),'{TastNames[i]}')]//parent::div//parent::div//parent::crisp-list-item-title//following-sibling::crisp-list-item-details//div[@class='crisp-row']//div[2]";
                        var Status = Driver.FindElement(By.XPath(temp));
                        Assert.IsTrue(Status.Text.Contains("Open"), "Failed to display the task status Open");
                    }
                    if (TastNames[i].Contains("Fire Consultation Required"))
                    {
                        var temp = $"//task-list//crisp-list[@list-title='Tasks']//ul[@ref='theList']//li[contains(@class,'au-target collection-item')]//crisp-list-item-title//div[@class='title']//div[@class='crisp-row']//div[1][contains(text(),'{TastNames[i]}')]//parent::div//parent::div//parent::crisp-list-item-title//following-sibling::crisp-list-item-details//div[@class='crisp-row']//div[2]";
                        var Status = Driver.FindElement(By.XPath(temp));
                        Assert.IsTrue(Status.Text.Contains("Open"), "Failed to display the task status Open");
                    }
                    if (TastNames[i].Contains("Make Surveyor Contact"))
                    {
                        var temp = $"//task-list//crisp-list[@list-title='Tasks']//ul[@ref='theList']//li[contains(@class,'au-target collection-item')]//crisp-list-item-title//div[@class='title']//div[@class='crisp-row']//div[1][contains(text(),'{TastNames[i]}')]//parent::div//parent::div//parent::crisp-list-item-title//following-sibling::crisp-list-item-details//div[@class='crisp-row']//div[2]";
                        var Status = Driver.FindElement(By.XPath(temp));
                        Assert.IsTrue(Status.Text.Contains("Open"), "Failed to display the task status Open");
                    }                    
                    if (TastNames[i].Contains("Record Response From Local Authority For Initial Notice"))
                    {
                        var temp = $"//task-list//crisp-list[@list-title='Tasks']//ul[@ref='theList']//li[contains(@class,'au-target collection-item')]//crisp-list-item-title//div[@class='title']//div[@class='crisp-row']//div[1][contains(text(),'{TastNames[i]}')]//parent::div//parent::div//parent::crisp-list-item-title//following-sibling::crisp-list-item-details//div[@class='crisp-row']//div[2]";
                        var Status = Driver.FindElement(By.XPath(temp));
                        Assert.IsTrue(Status.Text.Contains("Open"), "Failed to display the task status Open");
                    }
                    if (TastNames[i].Contains("Complete Site Risk Assessment"))
                    {
                        var temp = $"//task-list//crisp-list[@list-title='Tasks']//ul[@ref='theList']//li[contains(@class,'au-target collection-item')]//crisp-list-item-title//div[@class='title']//div[@class='crisp-row']//div[1][contains(text(),'{TastNames[i]}')]//parent::div//parent::div//parent::crisp-list-item-title//following-sibling::crisp-list-item-details//div[@class='crisp-row']//div[2]";
                        var Status = Driver.FindElement(By.XPath(temp));
                        Assert.IsTrue(Status.Text.Contains("Open"), "Failed to display the task status Open");
                    }
                    if (TastNames[i].Contains("Manually Trigger Start of Design Review Assessment"))
                    {
                        var temp = $"//task-list//crisp-list[@list-title='Tasks']//ul[@ref='theList']//li[contains(@class,'au-target collection-item')]//crisp-list-item-title//div[@class='title']//div[@class='crisp-row']//div[1][contains(text(),'{TastNames[i]}')]//parent::div//parent::div//parent::crisp-list-item-title//following-sibling::crisp-list-item-details//div[@class='crisp-row']//div[2]";
                        var Status = Driver.FindElement(By.XPath(temp));
                        Assert.IsTrue(Status.Text.Contains("Open"), "Failed to display the task status Open");
                    }
                    if (TastNames[i].Contains("Chase Outstanding Design Documents"))
                    {
                        var temp = $"//task-list//crisp-list[@list-title='Tasks']//ul[@ref='theList']//li[contains(@class,'au-target collection-item')]//crisp-list-item-title//div[@class='title']//div[@class='crisp-row']//div[1][contains(text(),'{TastNames[i]}')]//parent::div//parent::div//parent::crisp-list-item-title//following-sibling::crisp-list-item-details//div[@class='crisp-row']//div[2]";
                        var Status = Driver.FindElement(By.XPath(temp));
                        Assert.IsTrue(Status.Text.Contains("Open"), "Failed to display the task status Open");
                    }
                }
            }
        }
        public void TasksClosedWhenIntialNoticeSent()
        {
            if (TasksList.Count == 0)
            {
                OpenTasks();
                Thread.Sleep(500);
                CloseSpinneronDiv();
                CloseSpinneronPage();
            }
            else
            {
                TaskRefresh();
                Thread.Sleep(500);
                CloseSpinneronDiv();
                CloseSpinneronPage();
            }
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            WaitForElements(TasksTitle);
            Assert.IsFalse(TasksTitle.Any(x => x.Text.Contains("Make Quote Decision")), "Failed to close Make Quote Decision Task");
            Assert.IsFalse(TasksTitle.Any(x => x.Text.Contains("Chase Quote Decision from Client")), "Failed to close Chase Quote Decision from Client Task");
            Assert.IsFalse(TasksTitle.Any(x => x.Text.Contains("Chase O/S Documents To Issue Initial Notice")), "Failed to close Chase O/S Documents To Issue Initial Notice Task");

        }
        public void TasksCreatedWhenIntialNoticeAccepted()
        {
            if (TasksList.Count == 0)
            {
                OpenTasks();
                Thread.Sleep(500);
                CloseSpinneronDiv();
                CloseSpinneronPage();
            }
            else
            {
                TaskRefresh();
                Thread.Sleep(500);
                CloseSpinneronDiv();
                CloseSpinneronPage();
            }
            try
            {
                Thread.Sleep(500);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                WaitForElements(TasksTitle);
                Assert.IsTrue(TasksTitle.Any(x => x.Text.Contains("Notify Sales Account Manager of New Quote")), "Failed to create Sales Account Manager of New Quote Task");
                Assert.IsTrue(TasksTitle.Any(x => x.Text.Contains("New Order Email Sent")), "Failed to create New Order Email Sent Task");
                if (Statics.BCProducts.Any(x => x.Contains("Bulding Control")))
                {
                    Assert.IsTrue(TasksTitle.Any(x => x.Text.Contains("Water Consultation Required")), "Failed to create Water Consultation Required Task");
                    Assert.IsTrue(TasksTitle.Any(x => x.Text.Contains("Fire Consultation Required")), "Failed to create Fire Consultation Required Task");
                }
                Assert.IsTrue(TasksTitle.Any(x => x.Text.Contains("Make Surveyor Contact")), "Failed to create Make Surveyor Contact Task");
                Assert.IsTrue(TasksTitle.Any(x => x.Text.Contains("Complete Site Risk Assessment")), "Failed to create Complete SiteRisk Assessment Task");
                Assert.IsTrue(TasksTitle.Any(x => x.Text.Contains("Chase Outstanding Design Documents")), "Failed to create Chase Outstanding Design Documents Task");                
                if (Statics.OrderNumber.Contains("PL-PG") && (Statics.BCProducts.Contains("Building Control")))
                {
                    Assert.IsTrue(TasksTitle.Any(x => x.Text.Contains("Manually Trigger Start of Design Review Assessment")), "Failed to create Manually Trigger Start of Design Review Assessment Task");                   
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Failed to Crate Tasks When Intial Notice Accepted", ex);
            }

            List<String> TastNames = TasksTitle.Select(i => i.Text.ToString()).ToList();
            if (TastNames.Count > 0)
            {
                for (int i = 0; i < TastNames.Count; i++)
                {
                    if (TastNames[i].Contains("Notify Sales Account Manager of New Quote"))
                    {
                        var temp = $"//task-list//crisp-list[@list-title='Tasks']//ul[@ref='theList']//li[contains(@class,'au-target collection-item')]//crisp-list-item-title//div[@class='title']//div[@class='crisp-row']//div[1][contains(text(),'{TastNames[i]}')]//parent::div//parent::div//parent::crisp-list-item-title//following-sibling::crisp-list-item-details//div[@class='crisp-row']//div[2]";
                        var Status = Driver.FindElement(By.XPath(temp));
                        Assert.IsTrue(Status.Text.Contains("Open"), "Failed to display the task status Open");
                    }
                    if (TastNames[i].Contains("New Order Email Sent"))
                    {
                        var temp = $"//task-list//crisp-list[@list-title='Tasks']//ul[@ref='theList']//li[contains(@class,'au-target collection-item')]//crisp-list-item-title//div[@class='title']//div[@class='crisp-row']//div[1][contains(text(),'{TastNames[i]}')]//parent::div//parent::div//parent::crisp-list-item-title//following-sibling::crisp-list-item-details//div[@class='crisp-row']//div[2]";
                        var Status = Driver.FindElement(By.XPath(temp));
                        Assert.IsTrue(Status.Text.Contains("Open"), "Failed to display the task status Open");
                    }
                    if (Statics.BCProducts.Any(x => x.Contains("Bulding Control")))
                    {
                        if (TastNames[i].Contains("Water Consultation Required"))
                        {
                            var temp = $"//task-list//crisp-list[@list-title='Tasks']//ul[@ref='theList']//li[contains(@class,'au-target collection-item')]//crisp-list-item-title//div[@class='title']//div[@class='crisp-row']//div[1][contains(text(),'{TastNames[i]}')]//parent::div//parent::div//parent::crisp-list-item-title//following-sibling::crisp-list-item-details//div[@class='crisp-row']//div[2]";
                            var Status = Driver.FindElement(By.XPath(temp));
                            Assert.IsTrue(Status.Text.Contains("Open"), "Failed to display the task status Open");
                        }
                        if (TastNames[i].Contains("Fire Consultation Required"))
                        {
                            var temp = $"//task-list//crisp-list[@list-title='Tasks']//ul[@ref='theList']//li[contains(@class,'au-target collection-item')]//crisp-list-item-title//div[@class='title']//div[@class='crisp-row']//div[1][contains(text(),'{TastNames[i]}')]//parent::div//parent::div//parent::crisp-list-item-title//following-sibling::crisp-list-item-details//div[@class='crisp-row']//div[2]";
                            var Status = Driver.FindElement(By.XPath(temp));
                            Assert.IsTrue(Status.Text.Contains("Open"), "Failed to display the task status Open");
                        }
                    }
                    if (TastNames[i].Contains("Make Surveyor Contact"))
                    {
                        var temp = $"//task-list//crisp-list[@list-title='Tasks']//ul[@ref='theList']//li[contains(@class,'au-target collection-item')]//crisp-list-item-title//div[@class='title']//div[@class='crisp-row']//div[1][contains(text(),'{TastNames[i]}')]//parent::div//parent::div//parent::crisp-list-item-title//following-sibling::crisp-list-item-details//div[@class='crisp-row']//div[2]";
                        var Status = Driver.FindElement(By.XPath(temp));
                        Assert.IsTrue(Status.Text.Contains("Open"), "Failed to display the task status Open");
                    }
                   
                    if (TastNames[i].Contains("Complete Site Risk Assessment"))
                    {
                        var temp = $"//task-list//crisp-list[@list-title='Tasks']//ul[@ref='theList']//li[contains(@class,'au-target collection-item')]//crisp-list-item-title//div[@class='title']//div[@class='crisp-row']//div[1][contains(text(),'{TastNames[i]}')]//parent::div//parent::div//parent::crisp-list-item-title//following-sibling::crisp-list-item-details//div[@class='crisp-row']//div[2]";
                        var Status = Driver.FindElement(By.XPath(temp));
                        Assert.IsTrue(Status.Text.Contains("Open"), "Failed to display the task status Open");
                    }
                    if (Statics.OrderNumber.Contains("PL-PG") && (Statics.BCProducts.Contains("Building Control")))
                    {
                        if (TastNames[i].Contains("Manually Trigger Start of Design Review Assessment"))
                        {
                            var temp = $"//task-list//crisp-list[@list-title='Tasks']//ul[@ref='theList']//li[contains(@class,'au-target collection-item')]//crisp-list-item-title//div[@class='title']//div[@class='crisp-row']//div[1][contains(text(),'{TastNames[i]}')]//parent::div//parent::div//parent::crisp-list-item-title//following-sibling::crisp-list-item-details//div[@class='crisp-row']//div[2]";
                            var Status = Driver.FindElement(By.XPath(temp));
                            Assert.IsTrue(Status.Text.Contains("Open"), "Failed to display the task status Open");
                        }
                    }
                    if (TastNames[i].Contains("Chase Outstanding Design Documents"))
                    {
                        var temp = $"//task-list//crisp-list[@list-title='Tasks']//ul[@ref='theList']//li[contains(@class,'au-target collection-item')]//crisp-list-item-title//div[@class='title']//div[@class='crisp-row']//div[1][contains(text(),'{TastNames[i]}')]//parent::div//parent::div//parent::crisp-list-item-title//following-sibling::crisp-list-item-details//div[@class='crisp-row']//div[2]";
                        var Status = Driver.FindElement(By.XPath(temp));
                        Assert.IsTrue(Status.Text.Contains("Open"), "Failed to display the task status Open");
                    }
                }
            }
        }
        public void TasksClosedWhenIntialNoticeAccepted()
        {
            if (TasksList.Count == 0)
            {
                OpenTasks();
                Thread.Sleep(500);
                CloseSpinneronDiv();
                CloseSpinneronPage();
            }
            else
            {
                TaskRefresh();
                Thread.Sleep(500);
                CloseSpinneronDiv();
                CloseSpinneronPage();
            }
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            WaitForElements(TasksTitle);
            Assert.IsFalse(TasksTitle.Any(x => x.Text.Contains("Make Quote Decision")), "Failed to close Make Quote Decision Task");
            Assert.IsFalse(TasksTitle.Any(x => x.Text.Contains("Chase Quote Decision from Client")), "Failed to close Chase Quote Decision from Client Task");
            Assert.IsFalse(TasksTitle.Any(x => x.Text.Contains("Chase O/S Documents To Issue Initial Notice")), "Failed to close Chase O/S Documents To Issue Initial Notice Task");
            Assert.IsFalse(TasksTitle.Any(x => x.Text.Contains("Record Response From Local Authority For Initial Notice")), "Failed to close Record Response From Local Authority For Initial Notice Task");

        }
        public void TasksCreatedWhenSRASubmit()
        {
            if (TasksList.Count == 0)
            {
                OpenTasks();
                Thread.Sleep(500);
                CloseSpinneronDiv();
                CloseSpinneronPage();
            }
            else
            {
                TaskRefresh();
                Thread.Sleep(500);
                CloseSpinneronDiv();
                CloseSpinneronPage();
            }
            try
            {
                Thread.Sleep(500);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                WaitForElements(TasksTitle);
                Assert.IsTrue(TasksTitle.Any(x => x.Text.Contains("Notify Sales Account Manager of New Quote")), "Failed to create Sales Account Manager of New Quote Task");
                Assert.IsTrue(TasksTitle.Any(x => x.Text.Contains("New Order Email Sent")), "Failed to create New Order Email Sent Task");
                Assert.IsTrue(TasksTitle.Any(x => x.Text.Contains("Review Stage Of Works Loading")), "Failed to create Review Stage Of Work Loading Task");
                Assert.IsTrue(TasksTitle.Any(x => x.Text.Contains("Manually Trigger Start of Engineering Review Assessment")), "Failed to Create Manually Trigger Start of Engineering Review Assessment Task");
                Assert.IsTrue(TasksTitle.Any(x => x.Text.Contains("Make Surveyor Contact")), "Failed to create Make Surveyor Contact Task");                
                if (Statics.BCProducts.Any(x => x.Contains("Bulding Control")))
                {
                    Assert.IsTrue(TasksTitle.Any(x => x.Text.Contains("Water Consultation Required")), "Failed to create Water Consultation Required Task");
                    Assert.IsTrue(TasksTitle.Any(x => x.Text.Contains("Fire Consultation Required")), "Failed to create Fire Consultation Required Task");
                }

                if (Statics.OrderNumber.Contains("PL-PG") && (Statics.BCProducts.Contains("Building Control")))
                {
                    Assert.IsTrue(TasksTitle.Any(x => x.Text.Contains("Chase Outstanding Design Documents")), "Failed to create Chase Outstanding Design Documents Task");
                    Assert.IsTrue(TasksTitle.Any(x => x.Text.Contains("Manually Trigger Start of Design Review Assessment")), "Failed to create Manually Trigger Start of Design Review Assessment Task");
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Failed to Crate Tasks When User Submit Site Risk Assessment", ex);
            }

            List<String> TastNames = TasksTitle.Select(i => i.Text.ToString()).ToList();
            if (TastNames.Count > 0)
            {
                for (int i = 0; i < TastNames.Count; i++)
                {
                    if (TastNames[i].Contains("Notify Sales Account Manager of New Quote"))
                    {
                        var temp = $"//task-list//crisp-list[@list-title='Tasks']//ul[@ref='theList']//li[contains(@class,'au-target collection-item')]//crisp-list-item-title//div[@class='title']//div[@class='crisp-row']//div[1][contains(text(),'{TastNames[i]}')]//parent::div//parent::div//parent::crisp-list-item-title//following-sibling::crisp-list-item-details//div[@class='crisp-row']//div[2]";
                        var Status = Driver.FindElement(By.XPath(temp));
                        Assert.IsTrue(Status.Text.Contains("Open"), "Failed to display the task status Open");
                    }
                    if (TastNames[i].Contains("New Order Email Sent"))
                    {
                        var temp = $"//task-list//crisp-list[@list-title='Tasks']//ul[@ref='theList']//li[contains(@class,'au-target collection-item')]//crisp-list-item-title//div[@class='title']//div[@class='crisp-row']//div[1][contains(text(),'{TastNames[i]}')]//parent::div//parent::div//parent::crisp-list-item-title//following-sibling::crisp-list-item-details//div[@class='crisp-row']//div[2]";
                        var Status = Driver.FindElement(By.XPath(temp));
                        Assert.IsTrue(Status.Text.Contains("Open"), "Failed to display the task status Open");
                    }
                    if (Statics.BCProducts.Any(x => x.Contains("Bulding Control")))
                    {
                        if (TastNames[i].Contains("Water Consultation Required"))
                        {
                            var temp = $"//task-list//crisp-list[@list-title='Tasks']//ul[@ref='theList']//li[contains(@class,'au-target collection-item')]//crisp-list-item-title//div[@class='title']//div[@class='crisp-row']//div[1][contains(text(),'{TastNames[i]}')]//parent::div//parent::div//parent::crisp-list-item-title//following-sibling::crisp-list-item-details//div[@class='crisp-row']//div[2]";
                            var Status = Driver.FindElement(By.XPath(temp));
                            Assert.IsTrue(Status.Text.Contains("Open"), "Failed to display the task status Open");
                        }
                        if (TastNames[i].Contains("Fire Consultation Required"))
                        {
                            var temp = $"//task-list//crisp-list[@list-title='Tasks']//ul[@ref='theList']//li[contains(@class,'au-target collection-item')]//crisp-list-item-title//div[@class='title']//div[@class='crisp-row']//div[1][contains(text(),'{TastNames[i]}')]//parent::div//parent::div//parent::crisp-list-item-title//following-sibling::crisp-list-item-details//div[@class='crisp-row']//div[2]";
                            var Status = Driver.FindElement(By.XPath(temp));
                            Assert.IsTrue(Status.Text.Contains("Open"), "Failed to display the task status Open");
                        }
                    }
                 
                    if (TastNames[i].Contains("Review Stage Of Works Loading"))
                    {
                        var temp = $"//task-list//crisp-list[@list-title='Tasks']//ul[@ref='theList']//li[contains(@class,'au-target collection-item')]//crisp-list-item-title//div[@class='title']//div[@class='crisp-row']//div[1][contains(text(),'{TastNames[i]}')]//parent::div//parent::div//parent::crisp-list-item-title//following-sibling::crisp-list-item-details//div[@class='crisp-row']//div[2]";
                        var Status = Driver.FindElement(By.XPath(temp));
                        Assert.IsTrue(Status.Text.Contains("Open"), "Failed to display the task status Open");
                    }                   

                    if (TastNames[i].Contains("Make Surveyor Contact"))
                    {
                        var temp = $"//task-list//crisp-list[@list-title='Tasks']//ul[@ref='theList']//li[contains(@class,'au-target collection-item')]//crisp-list-item-title//div[@class='title']//div[@class='crisp-row']//div[1][contains(text(),'{TastNames[i]}')]//parent::div//parent::div//parent::crisp-list-item-title//following-sibling::crisp-list-item-details//div[@class='crisp-row']//div[2]";
                        var Status = Driver.FindElement(By.XPath(temp));
                        Assert.IsTrue(Status.Text.Contains("Open"), "Failed to display the task status Open");
                    }
                    if (Statics.OrderNumber.Contains("PL-PG") && (Statics.BCProducts.Contains("Building Control")))
                    {
                        if (TastNames[i].Contains("Chase Outstanding Design Documents"))
                        {
                            var temp = $"//task-list//crisp-list[@list-title='Tasks']//ul[@ref='theList']//li[contains(@class,'au-target collection-item')]//crisp-list-item-title//div[@class='title']//div[@class='crisp-row']//div[1][contains(text(),'{TastNames[i]}')]//parent::div//parent::div//parent::crisp-list-item-title//following-sibling::crisp-list-item-details//div[@class='crisp-row']//div[2]";
                            var Status = Driver.FindElement(By.XPath(temp));
                            Assert.IsTrue(Status.Text.Contains("Open"), "Failed to display the task status Open");
                        }
                        if (TastNames[i].Contains("Manually Trigger Start of Design Review Assessment"))
                        {
                            var temp = $"//task-list//crisp-list[@list-title='Tasks']//ul[@ref='theList']//li[contains(@class,'au-target collection-item')]//crisp-list-item-title//div[@class='title']//div[@class='crisp-row']//div[1][contains(text(),'{TastNames[i]}')]//parent::div//parent::div//parent::crisp-list-item-title//following-sibling::crisp-list-item-details//div[@class='crisp-row']//div[2]";
                            var Status = Driver.FindElement(By.XPath(temp));
                            Assert.IsTrue(Status.Text.Contains("Open"), "Failed to display the task status Open");
                        }
                    }                    
                    if (TastNames[i].Contains("Manually Trigger Start of Engineering Review Assessment"))
                    {
                        var temp = $"//task-list//crisp-list[@list-title='Tasks']//ul[@ref='theList']//li[contains(@class,'au-target collection-item')]//crisp-list-item-title//div[@class='title']//div[@class='crisp-row']//div[1][contains(text(),'{TastNames[i]}')]//parent::div//parent::div//parent::crisp-list-item-title//following-sibling::crisp-list-item-details//div[@class='crisp-row']//div[2]";
                        var Status = Driver.FindElement(By.XPath(temp));
                        Assert.IsTrue(Status.Text.Contains("Open"), "Failed to display the task status Open");
                    }
                 
                    if (TastNames[i].Contains("Chase Outstanding Design Documents"))
                    {
                        var temp = $"//task-list//crisp-list[@list-title='Tasks']//ul[@ref='theList']//li[contains(@class,'au-target collection-item')]//crisp-list-item-title//div[@class='title']//div[@class='crisp-row']//div[1][contains(text(),'{TastNames[i]}')]//parent::div//parent::div//parent::crisp-list-item-title//following-sibling::crisp-list-item-details//div[@class='crisp-row']//div[2]";
                        var Status = Driver.FindElement(By.XPath(temp));
                        Assert.IsTrue(Status.Text.Contains("Open"), "Failed to display the task status Open");
                    }
                }
            }
        }
        public void TasksClosedWhenSRASubmitted()
        {
            if (TasksList.Count == 0)
            {
                OpenTasks();
                Thread.Sleep(500);
                CloseSpinneronDiv();
                CloseSpinneronPage();
            }
            else
            {
                TaskRefresh();
                Thread.Sleep(500);
                CloseSpinneronDiv();
                CloseSpinneronPage();
            }
            Assert.IsFalse(TasksTitle.Any(x => x.Text.Contains("Make Quote Decision")), "Failed to close Make Quote Decision Task");
            Assert.IsFalse(TasksTitle.Any(x => x.Text.Contains("Chase Quote Decision from Client")), "Failed to close Chase Quote Decision from Client Task");
            Assert.IsFalse(TasksTitle.Any(x => x.Text.Contains("Chase O/S Documents To Issue Initial Notice")), "Failed to close Chase O/S Documents To Issue Initial Notice Task");
            Assert.IsFalse(TasksTitle.Any(x => x.Text.Contains("Record Response From Local Authority For Initial Notice")), "Failed to close Record Response From Local Authority For Initial Notice Task");  
            Assert.IsFalse(TasksTitle.Any(x => x.Text.Contains("Complete Site Risk Assessment")), "Failed to close Complete Site Risk Assessment Task");
        }
        public void TasksCreatedWhenDRSubmit()
        {
            if (TasksList.Count == 0)
            {
                OpenTasks();
                Thread.Sleep(500);
                CloseSpinneronDiv();
                CloseSpinneronPage();
            }
            else
            {
                TaskRefresh();
                Thread.Sleep(500);
                CloseSpinneronDiv();
                CloseSpinneronPage();
            }
            try
            {
                Thread.Sleep(500);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                WaitForElements(TasksTitle);
                Assert.IsTrue(TasksTitle.Any(x => x.Text.Contains("Notify Sales Account Manager of New Quote")), "Failed to create Sales Account Manager of New Quote Task");
                Assert.IsTrue(TasksTitle.Any(x => x.Text.Contains("New Order Email Sent")), "Failed to create New Order Email Sent Task");
                if (Statics.BCProducts.Any(x => x.Contains("Bulding Control")))
                {
                    Assert.IsTrue(TasksTitle.Any(x => x.Text.Contains("Water Consultation Required")), "Failed to create Water Consultation Required Task");
                    Assert.IsTrue(TasksTitle.Any(x => x.Text.Contains("Fire Consultation Required")), "Failed to create Fire Consultation Required Task");
                }
                Assert.IsTrue(TasksTitle.Any(x => x.Text.Contains("Review Stage Of Works Loading")), "Failed to create Review Stage Of Work Loading Task");
                Assert.IsTrue(TasksTitle.Any(x => x.Text.Contains("Make Surveyor Contact")), "Failed to create Make Surveyor Contact Task");
                            
                if (Statics.OrderNumber.Contains("PL-PG") && (Statics.BCProducts.Contains("Building Control")))
                {
                    Assert.IsTrue(TasksTitle.Any(x => x.Text.Contains("Manually Trigger Start of Design Review Assessment")), "Failed to create Manually Trigger Start of Design Review Assessment Task");                   
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Failed to Crate Tasks When User Submit Design Review Assessment", ex);
            }

            List<String> TastNames = TasksTitle.Select(i => i.Text.ToString()).ToList();
            if (TastNames.Count > 0)
            {
                for (int i = 0; i < TastNames.Count; i++)
                {
                    if (TastNames[i].Contains("Notify Sales Account Manager of New Quote"))
                    {
                        var temp = $"//task-list//crisp-list[@list-title='Tasks']//ul[@ref='theList']//li[contains(@class,'au-target collection-item')]//crisp-list-item-title//div[@class='title']//div[@class='crisp-row']//div[1][contains(text(),'{TastNames[i]}')]//parent::div//parent::div//parent::crisp-list-item-title//following-sibling::crisp-list-item-details//div[@class='crisp-row']//div[2]";
                        var Status = Driver.FindElement(By.XPath(temp));
                        Assert.IsTrue(Status.Text.Contains("Open"), "Failed to display the task status Open");
                    }
                    if (TastNames[i].Contains("New Order Email Sent"))
                    {
                        var temp = $"//task-list//crisp-list[@list-title='Tasks']//ul[@ref='theList']//li[contains(@class,'au-target collection-item')]//crisp-list-item-title//div[@class='title']//div[@class='crisp-row']//div[1][contains(text(),'{TastNames[i]}')]//parent::div//parent::div//parent::crisp-list-item-title//following-sibling::crisp-list-item-details//div[@class='crisp-row']//div[2]";
                        var Status = Driver.FindElement(By.XPath(temp));
                        Assert.IsTrue(Status.Text.Contains("Open"), "Failed to display the task status Open");
                    }
                    if (Statics.BCProducts.Any(x => x.Contains("Bulding Control")))
                    {
                        if (TastNames[i].Contains("Water Consultation Required"))
                        {
                            var temp = $"//task-list//crisp-list[@list-title='Tasks']//ul[@ref='theList']//li[contains(@class,'au-target collection-item')]//crisp-list-item-title//div[@class='title']//div[@class='crisp-row']//div[1][contains(text(),'{TastNames[i]}')]//parent::div//parent::div//parent::crisp-list-item-title//following-sibling::crisp-list-item-details//div[@class='crisp-row']//div[2]";
                            var Status = Driver.FindElement(By.XPath(temp));
                            Assert.IsTrue(Status.Text.Contains("Open"), "Failed to display the task status Open");
                        }
                        if (TastNames[i].Contains("Fire Consultation Required"))
                        {
                            var temp = $"//task-list//crisp-list[@list-title='Tasks']//ul[@ref='theList']//li[contains(@class,'au-target collection-item')]//crisp-list-item-title//div[@class='title']//div[@class='crisp-row']//div[1][contains(text(),'{TastNames[i]}')]//parent::div//parent::div//parent::crisp-list-item-title//following-sibling::crisp-list-item-details//div[@class='crisp-row']//div[2]";
                            var Status = Driver.FindElement(By.XPath(temp));
                            Assert.IsTrue(Status.Text.Contains("Open"), "Failed to display the task status Open");
                        }
                    }
                    
                    if (TastNames[i].Contains("Review Stage Of Works Loading"))
                    {
                        var temp = $"//task-list//crisp-list[@list-title='Tasks']//ul[@ref='theList']//li[contains(@class,'au-target collection-item')]//crisp-list-item-title//div[@class='title']//div[@class='crisp-row']//div[1][contains(text(),'{TastNames[i]}')]//parent::div//parent::div//parent::crisp-list-item-title//following-sibling::crisp-list-item-details//div[@class='crisp-row']//div[2]";
                        var Status = Driver.FindElement(By.XPath(temp));
                        Assert.IsTrue(Status.Text.Contains("Open"), "Failed to display the task status Open");
                    }
                    if (TastNames[i].Contains("Review Email Received"))
                    {
                        var temp = $"//task-list//crisp-list[@list-title='Tasks']//ul[@ref='theList']//li[contains(@class,'au-target collection-item')]//crisp-list-item-title//div[@class='title']//div[@class='crisp-row']//div[1][contains(text(),'{TastNames[i]}')]//parent::div//parent::div//parent::crisp-list-item-title//following-sibling::crisp-list-item-details//div[@class='crisp-row']//div[2]";
                        var Status = Driver.FindElement(By.XPath(temp));
                        Assert.IsTrue(Status.Text.Contains("Open"), "Failed to display the task status Open");
                    }                  

                    if (TastNames[i].Contains("Make Surveyor Contact"))
                    {
                        var temp = $"//task-list//crisp-list[@list-title='Tasks']//ul[@ref='theList']//li[contains(@class,'au-target collection-item')]//crisp-list-item-title//div[@class='title']//div[@class='crisp-row']//div[1][contains(text(),'{TastNames[i]}')]//parent::div//parent::div//parent::crisp-list-item-title//following-sibling::crisp-list-item-details//div[@class='crisp-row']//div[2]";
                        var Status = Driver.FindElement(By.XPath(temp));
                        Assert.IsTrue(Status.Text.Contains("Open"), "Failed to display the task status Open");
                    }
                      
                    if (TastNames[i].Contains("Manually Trigger Start of Engineering Review Assessment"))
                    {
                        var temp = $"//task-list//crisp-list[@list-title='Tasks']//ul[@ref='theList']//li[contains(@class,'au-target collection-item')]//crisp-list-item-title//div[@class='title']//div[@class='crisp-row']//div[1][contains(text(),'{TastNames[i]}')]//parent::div//parent::div//parent::crisp-list-item-title//following-sibling::crisp-list-item-details//div[@class='crisp-row']//div[2]";
                        var Status = Driver.FindElement(By.XPath(temp));
                        Assert.IsTrue(Status.Text.Contains("Open"), "Failed to display the task status Open");
                    }                    
                   
                }
            }
        }
        public void TasksClosedWhenDRSubmitted()
        {
            if (TasksList.Count == 0)
            {
                OpenTasks();
                Thread.Sleep(500);
                CloseSpinneronDiv();
                CloseSpinneronPage();
            }
            else
            {
                TaskRefresh();
                Thread.Sleep(500);
                CloseSpinneronDiv();
                CloseSpinneronPage();
            }
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            WaitForElements(TasksTitle);
            Assert.IsFalse(TasksTitle.Any(x => x.Text.Contains("Make Quote Decision")), "Failed to close Make Quote Decision Task");
            Assert.IsFalse(TasksTitle.Any(x => x.Text.Contains("Chase Quote Decision from Client")), "Failed to close Chase Quote Decision from Client Task");
            Assert.IsFalse(TasksTitle.Any(x => x.Text.Contains("Chase O/S Documents To Issue Initial Notice")), "Failed to close Chase O/S Documents To Issue Initial Notice Task");
            Assert.IsFalse(TasksTitle.Any(x => x.Text.Contains("Record Response From Local Authority For Initial Notice")), "Failed to close Record Response From Local Authority For Initial Notice Task");
            Assert.IsFalse(TasksTitle.Any(x => x.Text.Contains("Complete Site Risk Assessment")),"Failed to close Complete Site Risk Assessment Task");
            Assert.IsFalse(TasksTitle.Any(x => x.Text.Contains("Manually Trigger Start of Design Review Assessment")), "Failed to close Manually Trigger Start of Design Review Assessment Task");
            Assert.IsFalse(TasksTitle.Any(x => x.Text.Contains("Chase Outstanding Design Documents")), "Failed to Chase Outstanding Design Documents Task");
        }
        public void TasksCreatedWhenERSubmit()
        {
            if (TasksList.Count == 0)
            {
                OpenTasks();
                Thread.Sleep(500);
                CloseSpinneronDiv();
                CloseSpinneronPage();
            }
            else
            {
                TaskRefresh();
                Thread.Sleep(500);
                CloseSpinneronDiv();
                CloseSpinneronPage();
            }
            try
            {
                Thread.Sleep(500);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                WaitForElements(TasksTitle);
                Assert.IsTrue(TasksTitle.Any(x => x.Text.Contains("Notify Sales Account Manager of New Quote")), "Failed to create Sales Account Manager of New Quote Task");
                Assert.IsTrue(TasksTitle.Any(x => x.Text.Contains("New Order Email Sent")), "Failed to create New Order Email Sent Task");              
                if (Statics.BCProducts.Any(x => x.Contains("Bulding Control")))
                {
                    Assert.IsTrue(TasksTitle.Any(x => x.Text.Contains("Water Consultation Required")), "Failed to create Water Consultation Required Task");
                    Assert.IsTrue(TasksTitle.Any(x => x.Text.Contains("Fire Consultation Required")), "Failed to create Fire Consultation Required Task");
                }
                Assert.IsTrue(TasksTitle.Any(x => x.Text.Contains("Review Stage Of Works Loading")), "Failed to create Review Stage Of Work Loading Task");              
                Assert.IsTrue(TasksTitle.Any(x => x.Text.Contains("Make Surveyor Contact")), "Failed to create Make Surveyor Contact Task");                          
            }
            catch (Exception ex)
            {
                throw new Exception("Failed to Crate Tasks When User Submit Engineer Review Assessment", ex);
            }

            List<String> TastNames = TasksTitle.Select(i => i.Text.ToString()).ToList();
            if (TastNames.Count > 0)
            {
                for (int i = 0; i < TastNames.Count; i++)
                {
                    if (TastNames[i].Contains("Notify Sales Account Manager of New Quote"))
                    {
                        var temp = $"//task-list//crisp-list[@list-title='Tasks']//ul[@ref='theList']//li[contains(@class,'au-target collection-item')]//crisp-list-item-title//div[@class='title']//div[@class='crisp-row']//div[1][contains(text(),'{TastNames[i]}')]//parent::div//parent::div//parent::crisp-list-item-title//following-sibling::crisp-list-item-details//div[@class='crisp-row']//div[2]";
                        var Status = Driver.FindElement(By.XPath(temp));
                        Assert.IsTrue(Status.Text.Contains("Open"), "Failed to display the task status Open");
                    }
                    if (TastNames[i].Contains("New Order Email Sent"))
                    {
                        var temp = $"//task-list//crisp-list[@list-title='Tasks']//ul[@ref='theList']//li[contains(@class,'au-target collection-item')]//crisp-list-item-title//div[@class='title']//div[@class='crisp-row']//div[1][contains(text(),'{TastNames[i]}')]//parent::div//parent::div//parent::crisp-list-item-title//following-sibling::crisp-list-item-details//div[@class='crisp-row']//div[2]";
                        var Status = Driver.FindElement(By.XPath(temp));
                        Assert.IsTrue(Status.Text.Contains("Open"), "Failed to display the task status Open");
                    }
                    if (Statics.BCProducts.Any(x => x.Contains("Bulding Control")))
                    {
                        if (TastNames[i].Contains("Water Consultation Required"))
                        {
                            var temp = $"//task-list//crisp-list[@list-title='Tasks']//ul[@ref='theList']//li[contains(@class,'au-target collection-item')]//crisp-list-item-title//div[@class='title']//div[@class='crisp-row']//div[1][contains(text(),'{TastNames[i]}')]//parent::div//parent::div//parent::crisp-list-item-title//following-sibling::crisp-list-item-details//div[@class='crisp-row']//div[2]";
                            var Status = Driver.FindElement(By.XPath(temp));
                            Assert.IsTrue(Status.Text.Contains("Open"), "Failed to display the task status Open");
                        }
                        if (TastNames[i].Contains("Fire Consultation Required"))
                        {
                            var temp = $"//task-list//crisp-list[@list-title='Tasks']//ul[@ref='theList']//li[contains(@class,'au-target collection-item')]//crisp-list-item-title//div[@class='title']//div[@class='crisp-row']//div[1][contains(text(),'{TastNames[i]}')]//parent::div//parent::div//parent::crisp-list-item-title//following-sibling::crisp-list-item-details//div[@class='crisp-row']//div[2]";
                            var Status = Driver.FindElement(By.XPath(temp));
                            Assert.IsTrue(Status.Text.Contains("Open"), "Failed to display the task status Open");
                        }
                    }
                    if (TastNames[i].Contains("Review Stage Of Works Loading"))
                    {
                        var temp = $"//task-list//crisp-list[@list-title='Tasks']//ul[@ref='theList']//li[contains(@class,'au-target collection-item')]//crisp-list-item-title//div[@class='title']//div[@class='crisp-row']//div[1][contains(text(),'{TastNames[i]}')]//parent::div//parent::div//parent::crisp-list-item-title//following-sibling::crisp-list-item-details//div[@class='crisp-row']//div[2]";
                        var Status = Driver.FindElement(By.XPath(temp));
                        Assert.IsTrue(Status.Text.Contains("Open"), "Failed to display the task status Open");
                    }                 

                    if (TastNames[i].Contains("Make Surveyor Contact"))
                    {
                        var temp = $"//task-list//crisp-list[@list-title='Tasks']//ul[@ref='theList']//li[contains(@class,'au-target collection-item')]//crisp-list-item-title//div[@class='title']//div[@class='crisp-row']//div[1][contains(text(),'{TastNames[i]}')]//parent::div//parent::div//parent::crisp-list-item-title//following-sibling::crisp-list-item-details//div[@class='crisp-row']//div[2]";
                        var Status = Driver.FindElement(By.XPath(temp));
                        Assert.IsTrue(Status.Text.Contains("Open"), "Failed to display the task status Open");
                    }   
                }
            }
        }
        public void TasksClosedWhenERSubmitted()
        {
            if (TasksList.Count == 0)
            {
                OpenTasks();
                Thread.Sleep(500);
                CloseSpinneronDiv();
                CloseSpinneronPage();
            }
            else
            {
                TaskRefresh();
                Thread.Sleep(500);
                CloseSpinneronDiv();
                CloseSpinneronPage();
            }
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            WaitForElements(TasksTitle);
            Assert.IsFalse(TasksTitle.Any(x => x.Text.Contains("Make Quote Decision")), "Failed to close Make Quote Decision Task");
            Assert.IsFalse(TasksTitle.Any(x => x.Text.Contains("Chase Quote Decision from Client")), "Failed to close Chase Quote Decision from Client Task");
            Assert.IsFalse(TasksTitle.Any(x => x.Text.Contains("Chase O/S Documents To Issue Initial Notice")), "Failed to close Chase O/S Documents To Issue Initial Notice Task");
            Assert.IsFalse(TasksTitle.Any(x => x.Text.Contains("Record Response From Local Authority For Initial Notice")), "Failed to close Record Response From Local Authority For Initial Notice Task");
            Assert.IsFalse(TasksTitle.Any(x => x.Text.Contains("Complete Site Risk Assessment")), "Failed to close Complete Site Risk Assessment Task");
            Assert.IsFalse(TasksTitle.Any(x => x.Text.Contains("Manually Trigger Start of Design Review Assessment")), "Failed to close Manually Trigger Start of Design Review Assessment Task");
            Assert.IsFalse(TasksTitle.Any(x => x.Text.Contains("Chase Outstanding Design Documents")), "Failed to Chase Outstanding Design Documents Task");
            Assert.IsFalse(TasksTitle.Any(x => x.Text.Contains("Manually Trigger Start of Engineering Review Assessment")), "Failed to Manually Trigger Start of Engineering Review Assessment Task");
        }
        public void TasksCreatedWhenInspectionSubmit()
        {
            if (TasksList.Count == 0)
            {
                OpenTasks();
                Thread.Sleep(500);
                CloseSpinneronDiv();
                CloseSpinneronPage();
            }
            else
            {
                TaskRefresh();
                Thread.Sleep(500);
                CloseSpinneronDiv();
                CloseSpinneronPage();
            }
            try
            {
                Thread.Sleep(500);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                WaitForElements(TasksTitle);
                Assert.IsTrue(TasksTitle.Any(x => x.Text.Contains("Notify Sales Account Manager of New Quote")), "Failed to create Sales Account Manager of New Quote Task");
                Assert.IsTrue(TasksTitle.Any(x => x.Text.Contains("New Order Email Sent")), "Failed to create New Order Email Sent Task");
                if (Statics.BCProducts.Any(x => x.Contains("Bulding Control")))
                {
                    Assert.IsTrue(TasksTitle.Any(x => x.Text.Contains("Water Consultation Required")), "Failed to create Water Consultation Required Task");
                    Assert.IsTrue(TasksTitle.Any(x => x.Text.Contains("Fire Consultation Required")), "Failed to create Fire Consultation Required Task");
                    Assert.IsTrue(TasksTitle.Any(x => x.Text.Contains("Issue Final Notice")), "Failed to create Issue Final Notice Task");

                }
                Assert.IsTrue(TasksTitle.Any(x => x.Text.Contains("Review Stage Of Works Loading")),"Failed to create Review Stage Of Work Loading Task");
                Assert.IsTrue(TasksTitle.Any(x => x.Text.Contains("Issue Certificate Of Insurance")), "Failed to create Issue Certificate Of Insurance Task");
                Assert.IsTrue(TasksTitle.Any(x => x.Text.Contains("File Review")), "Failed to create File Review Task");
            }
            catch (Exception ex)
            {
                throw new Exception("Failed to Crate Tasks When User Submit Engineer Review Assessment", ex);
            }

            List<String> TastNames = TasksTitle.Select(i => i.Text.ToString()).ToList();
            if (TastNames.Count > 0)
            {
                for (int i = 0; i < TastNames.Count; i++)
                {
                    if (TastNames[i].Contains("Notify Sales Account Manager of New Quote"))
                    {
                        var temp = $"//task-list//crisp-list[@list-title='Tasks']//ul[@ref='theList']//li[contains(@class,'au-target collection-item')]//crisp-list-item-title//div[@class='title']//div[@class='crisp-row']//div[1][contains(text(),'{TastNames[i]}')]//parent::div//parent::div//parent::crisp-list-item-title//following-sibling::crisp-list-item-details//div[@class='crisp-row']//div[2]";
                        var Status = Driver.FindElement(By.XPath(temp));
                        Assert.IsTrue(Status.Text.Contains("Open"), "Failed to display the task status Open");
                    }
                    if (TastNames[i].Contains("New Order Email Sent"))
                    {
                        var temp = $"//task-list//crisp-list[@list-title='Tasks']//ul[@ref='theList']//li[contains(@class,'au-target collection-item')]//crisp-list-item-title//div[@class='title']//div[@class='crisp-row']//div[1][contains(text(),'{TastNames[i]}')]//parent::div//parent::div//parent::crisp-list-item-title//following-sibling::crisp-list-item-details//div[@class='crisp-row']//div[2]";
                        var Status = Driver.FindElement(By.XPath(temp));
                        Assert.IsTrue(Status.Text.Contains("Open"), "Failed to display the task status Open");
                    }
                    if (Statics.BCProducts.Any(x => x.Contains("Bulding Control")))
                    {
                        if (TastNames[i].Contains("Water Consultation Required"))
                        {
                            var temp = $"//task-list//crisp-list[@list-title='Tasks']//ul[@ref='theList']//li[contains(@class,'au-target collection-item')]//crisp-list-item-title//div[@class='title']//div[@class='crisp-row']//div[1][contains(text(),'{TastNames[i]}')]//parent::div//parent::div//parent::crisp-list-item-title//following-sibling::crisp-list-item-details//div[@class='crisp-row']//div[2]";
                            var Status = Driver.FindElement(By.XPath(temp));
                            Assert.IsTrue(Status.Text.Contains("Open"), "Failed to display the task status Open");
                        }
                        if (TastNames[i].Contains("Fire Consultation Required"))
                        {
                            var temp = $"//task-list//crisp-list[@list-title='Tasks']//ul[@ref='theList']//li[contains(@class,'au-target collection-item')]//crisp-list-item-title//div[@class='title']//div[@class='crisp-row']//div[1][contains(text(),'{TastNames[i]}')]//parent::div//parent::div//parent::crisp-list-item-title//following-sibling::crisp-list-item-details//div[@class='crisp-row']//div[2]";
                            var Status = Driver.FindElement(By.XPath(temp));
                            Assert.IsTrue(Status.Text.Contains("Open"), "Failed to display the task status Open");
                        }
                        if (TastNames[i].Contains("Issue Final Notice"))
                        {
                            var temp = $"//task-list//crisp-list[@list-title='Tasks']//ul[@ref='theList']//li[contains(@class,'au-target collection-item')]//crisp-list-item-title//div[@class='title']//div[@class='crisp-row']//div[1][contains(text(),'{TastNames[i]}')]//parent::div//parent::div//parent::crisp-list-item-title//following-sibling::crisp-list-item-details//div[@class='crisp-row']//div[2]";
                            var Status = Driver.FindElement(By.XPath(temp));
                            Assert.IsTrue(Status.Text.Contains("Open"), "Failed to display the task status Open");
                        }
                    }
                    if (TastNames[i].Contains("Review Stage Of Works Loading"))
                    {
                        var temp = $"//task-list//crisp-list[@list-title='Tasks']//ul[@ref='theList']//li[contains(@class,'au-target collection-item')]//crisp-list-item-title//div[@class='title']//div[@class='crisp-row']//div[1][contains(text(),'{TastNames[i]}')]//parent::div//parent::div//parent::crisp-list-item-title//following-sibling::crisp-list-item-details//div[@class='crisp-row']//div[2]";
                        var Status = Driver.FindElement(By.XPath(temp));
                        Assert.IsTrue(Status.Text.Contains("Open"), "Failed to display the task status Open");
                    }
                    
                    if (TastNames[i].Contains("Issue Certificate Of Insurance"))
                    {
                        var temp = $"//task-list//crisp-list[@list-title='Tasks']//ul[@ref='theList']//li[contains(@class,'au-target collection-item')]//crisp-list-item-title//div[@class='title']//div[@class='crisp-row']//div[1][contains(text(),'{TastNames[i]}')]//parent::div//parent::div//parent::crisp-list-item-title//following-sibling::crisp-list-item-details//div[@class='crisp-row']//div[2]";
                        var Status = Driver.FindElement(By.XPath(temp));
                        Assert.IsTrue(Status.Text.Contains("Open"), "Failed to display the task status Open");
                    }
                  
                    if (TastNames[i].Contains("File Review"))
                    {
                        var temp = $"//task-list//crisp-list[@list-title='Tasks']//ul[@ref='theList']//li[contains(@class,'au-target collection-item')]//crisp-list-item-title//div[@class='title']//div[@class='crisp-row']//div[1][contains(text(),'{TastNames[i]}')]//parent::div//parent::div//parent::crisp-list-item-title//following-sibling::crisp-list-item-details//div[@class='crisp-row']//div[2]";
                        var Status = Driver.FindElement(By.XPath(temp));
                        Assert.IsTrue(Status.Text.Contains("Open"), "Failed to display the task status Open");
                    }
                }
            }
        }
        public void TasksClosedWhenInspectionSubmitted()
        {
            if (TasksList.Count == 0)
            {
                OpenTasks();
                Thread.Sleep(500);
                CloseSpinneronDiv();
                CloseSpinneronPage();
            }
            else
            {
                TaskRefresh();
                Thread.Sleep(500);
                CloseSpinneronDiv();
                CloseSpinneronPage();
            }
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            WaitForElements(TasksTitle);
            Assert.IsFalse(TasksTitle.Any(x => x.Text.Contains("Make Quote Decision")), "Failed to close Make Quote Decision Task");
            Assert.IsFalse(TasksTitle.Any(x => x.Text.Contains("Chase Quote Decision from Client")), "Failed to close Chase Quote Decision from Client Task");
            Assert.IsFalse(TasksTitle.Any(x => x.Text.Contains("Chase O/S Documents To Issue Initial Notice")), "Failed to close Chase O/S Documents To Issue Initial Notice Task");
            Assert.IsFalse(TasksTitle.Any(x => x.Text.Contains("Record Response From Local Authority For Initial Notice")), "Failed to close Record Response From Local Authority For Initial Notice Task");
            Assert.IsFalse(TasksTitle.Any(x => x.Text.Contains("Complete Site Risk Assessment")), "Failed to close Complete Site Risk Assessment Task");
            Assert.IsFalse(TasksTitle.Any(x => x.Text.Contains("Manually Trigger Start of Design Review Assessment")), "Failed to close Manually Trigger Start of Design Review Assessment Task");
            Assert.IsFalse(TasksTitle.Any(x => x.Text.Contains("Chase Outstanding Design Documents")), "Failed to close Chase Outstanding Design Documents Task");
            Assert.IsFalse(TasksTitle.Any(x => x.Text.Contains("Manually Trigger Start of Engineering Review Assessment")), "Failed to close Manually Trigger Start of Engineering Review Assessment Task");
            Assert.IsFalse(TasksTitle.Any(x => x.Text.Contains("Make Surveyor Contact")), "Failed to close Make Surveyor Contact Task");
        }

        public void TasksCreatedWhenFinalNoticeSent()
        {
            if (TasksList.Count == 0)
            {
                OpenTasks();
                Thread.Sleep(500);
                CloseSpinneronDiv();
                CloseSpinneronPage();
            }
            else
            {
                TaskRefresh();
                Thread.Sleep(500);
                CloseSpinneronDiv();
                CloseSpinneronPage();
            }
            try
            {
                Thread.Sleep(500);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                WaitForElements(TasksTitle);
                Assert.IsTrue(TasksTitle.Any(x => x.Text.Contains("Notify Sales Account Manager of New Quote")),"Failed to create Sales Account Manager of New Quote Task");
                Assert.IsTrue(TasksTitle.Any(x => x.Text.Contains("New Order Email Sent")),"Failed to create New Order Email Sent Task");
                if (Statics.BCProducts.Any(x => x.Contains("Bulding Control")))
                {
                    Assert.IsTrue(TasksTitle.Any(x => x.Text.Contains("Water Consultation Required")),"Failed to create Water Consultation Required Task");
                    Assert.IsTrue(TasksTitle.Any(x => x.Text.Contains("Fire Consultation Required")),"Failed to create Fire Consultation Required Task");
                }
                Assert.IsTrue(TasksTitle.Any(x => x.Text.Contains("Review Stage Of Works Loading")),"Failed to create Review Stage Of Works Loading Task");
                Assert.IsTrue(TasksTitle.Any(x => x.Text.Contains("Issue Certificate Of Insurance")),"Failed to create Issue Certificate Of Insurance Task");
                Assert.IsTrue(TasksTitle.Any(x => x.Text.Contains("File Review")),"Failed to create File Review Task");

                if (Statics.OrderNumber.Contains("PL-PG") && (Statics.BCProducts.Contains("Building Control")))
                {
                    Assert.IsTrue(TasksTitle.Any(x => x.Text.Contains("Manually Trigger Start of Design Review Assessment")), "Failed to create Manually Trigger Start of Design Review Assessment Task");
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Failed to Crate Tasks When Final Notice Sent", ex);
            }

            List<String> TastNames = TasksTitle.Select(i => i.Text.ToString()).ToList();
            if (TastNames.Count > 0)
            {
                for (int i = 0; i < TastNames.Count; i++)
                {
                    if (TastNames[i].Contains("Notify Sales Account Manager of New Quote"))
                    {
                        var temp = $"//task-list//crisp-list[@list-title='Tasks']//ul[@ref='theList']//li[contains(@class,'au-target collection-item')]//crisp-list-item-title//div[@class='title']//div[@class='crisp-row']//div[1][contains(text(),'{TastNames[i]}')]//parent::div//parent::div//parent::crisp-list-item-title//following-sibling::crisp-list-item-details//div[@class='crisp-row']//div[2]";
                        var Status = Driver.FindElement(By.XPath(temp));
                        Assert.IsTrue(Status.Text.Contains("Open"), "Failed to display the task status Open");
                    }
                    if (TastNames[i].Contains("New Order Email Sent"))
                    {
                        var temp = $"//task-list//crisp-list[@list-title='Tasks']//ul[@ref='theList']//li[contains(@class,'au-target collection-item')]//crisp-list-item-title//div[@class='title']//div[@class='crisp-row']//div[1][contains(text(),'{TastNames[i]}')]//parent::div//parent::div//parent::crisp-list-item-title//following-sibling::crisp-list-item-details//div[@class='crisp-row']//div[2]";
                        var Status = Driver.FindElement(By.XPath(temp));
                        Assert.IsTrue(Status.Text.Contains("Open"), "Failed to display the task status Open");
                    }
                    if (Statics.BCProducts.Any(x => x.Contains("Bulding Control")))
                    {
                        if (TastNames[i].Contains("Water Consultation Required"))
                        {
                            var temp = $"//task-list//crisp-list[@list-title='Tasks']//ul[@ref='theList']//li[contains(@class,'au-target collection-item')]//crisp-list-item-title//div[@class='title']//div[@class='crisp-row']//div[1][contains(text(),'{TastNames[i]}')]//parent::div//parent::div//parent::crisp-list-item-title//following-sibling::crisp-list-item-details//div[@class='crisp-row']//div[2]";
                            var Status = Driver.FindElement(By.XPath(temp));
                            Assert.IsTrue(Status.Text.Contains("Open"), "Failed to display the task status Open");
                        }
                        if (TastNames[i].Contains("Fire Consultation Required"))
                        {
                            var temp = $"//task-list//crisp-list[@list-title='Tasks']//ul[@ref='theList']//li[contains(@class,'au-target collection-item')]//crisp-list-item-title//div[@class='title']//div[@class='crisp-row']//div[1][contains(text(),'{TastNames[i]}')]//parent::div//parent::div//parent::crisp-list-item-title//following-sibling::crisp-list-item-details//div[@class='crisp-row']//div[2]";
                            var Status = Driver.FindElement(By.XPath(temp));
                            Assert.IsTrue(Status.Text.Contains("Open"), "Failed to display the task status Open");
                        }
                    }
                    if (TastNames[i].Contains("Review Stage Of Works Loading"))
                    {
                        var temp = $"//task-list//crisp-list[@list-title='Tasks']//ul[@ref='theList']//li[contains(@class,'au-target collection-item')]//crisp-list-item-title//div[@class='title']//div[@class='crisp-row']//div[1][contains(text(),'{TastNames[i]}')]//parent::div//parent::div//parent::crisp-list-item-title//following-sibling::crisp-list-item-details//div[@class='crisp-row']//div[2]";
                        var Status = Driver.FindElement(By.XPath(temp));
                        Assert.IsTrue(Status.Text.Contains("Open"), "Failed to display the task status Open");
                    }
                    if (TastNames[i].Contains("Issue Certificate Of Insurance"))
                    {
                        var temp = $"//task-list//crisp-list[@list-title='Tasks']//ul[@ref='theList']//li[contains(@class,'au-target collection-item')]//crisp-list-item-title//div[@class='title']//div[@class='crisp-row']//div[1][contains(text(),'{TastNames[i]}')]//parent::div//parent::div//parent::crisp-list-item-title//following-sibling::crisp-list-item-details//div[@class='crisp-row']//div[2]";
                        var Status = Driver.FindElement(By.XPath(temp));
                        Assert.IsTrue(Status.Text.Contains("Open"), "Failed to display the task status Open");
                    }
                    if (TastNames[i].Contains("File Review"))
                    {
                        var temp = $"//task-list//crisp-list[@list-title='Tasks']//ul[@ref='theList']//li[contains(@class,'au-target collection-item')]//crisp-list-item-title//div[@class='title']//div[@class='crisp-row']//div[1][contains(text(),'{TastNames[i]}')]//parent::div//parent::div//parent::crisp-list-item-title//following-sibling::crisp-list-item-details//div[@class='crisp-row']//div[2]";
                        var Status = Driver.FindElement(By.XPath(temp));
                        Assert.IsTrue(Status.Text.Contains("Open"), "Failed to display the task status Open");
                    }                   
                }
            }
        }
        public void TasksClosedWhenFinalNoticeSent()
        {
            if (TasksList.Count == 0)
            {
                OpenTasks();
                Thread.Sleep(500);
                CloseSpinneronDiv();
                CloseSpinneronPage();
            }
            else
            {
                TaskRefresh();
                Thread.Sleep(500);
                CloseSpinneronDiv();
                CloseSpinneronPage();
            }
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            WaitForElements(TasksTitle);
            Assert.IsFalse(TasksTitle.Any(x => x.Text.Contains("Make Quote Decision")), "Failed to close Make Quote Decision Task");
            Assert.IsFalse(TasksTitle.Any(x => x.Text.Contains("Chase Quote Decision from Client")), "Failed to close Chase Quote Decision from Client Task");
            Assert.IsFalse(TasksTitle.Any(x => x.Text.Contains("Chase O/S Documents To Issue Initial Notice")), "Failed to close Chase O/S Documents To Issue Initial Notice Task");
            Assert.IsFalse(TasksTitle.Any(x => x.Text.Contains("Record Response From Local Authority For Initial Notice")), "Failed to close Record Response From Local Authority For Initial Notice Task");
            Assert.IsFalse(TasksTitle.Any(x => x.Text.Contains("Complete Site Risk Assessment")), "Failed to close Complete Site Risk Assessment Task");
            Assert.IsFalse(TasksTitle.Any(x => x.Text.Contains("Manually Trigger Start of Design Review Assessment")), "Failed to close Manually Trigger Start of Design Review Assessment Task");
            Assert.IsFalse(TasksTitle.Any(x => x.Text.Contains("Chase Outstanding Design Documents")), "Failed to close Chase Outstanding Design Documents Task");
            Assert.IsFalse(TasksTitle.Any(x => x.Text.Contains("Manually Trigger Start of Engineering Review Assessment")), "Failed to close Manually Trigger Start of Engineering Review Assessment Task");
            Assert.IsFalse(TasksTitle.Any(x => x.Text.Contains("Make Surveyor Contact")), "Failed to close Make Surveyor Contact Task");
            Assert.IsFalse(TasksTitle.Any(x => x.Text.Contains("Issue Final Notice")), "Failed to close Issue Final Notice Task");
        }
        public void TasksCreatedWhenCOISent()
        {
            if (TasksList.Count == 0)
            {
                OpenTasks();
                Thread.Sleep(500);
                CloseSpinneronDiv();
                CloseSpinneronPage();
            }
            else
            {
                TaskRefresh();
                Thread.Sleep(500);
                CloseSpinneronDiv();
                CloseSpinneronPage();
            }
            try
            {
                Thread.Sleep(500);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                WaitForElements(TasksTitle);
                Assert.IsTrue(TasksTitle.Any(x => x.Text.Contains("Notify Sales Account Manager of New Quote")), "Failed to create Sales Account Manager of New Quote Task");
                Assert.IsTrue(TasksTitle.Any(x => x.Text.Contains("New Order Email Sent")), "Failed to create New Order Email Sent Task");
                if (Statics.BCProducts.Any(x => x.Contains("Bulding Control")))
                {
                    Assert.IsTrue(TasksTitle.Any(x => x.Text.Contains("Water Consultation Required")), "Failed to create Water Consultation Required Task");
                    Assert.IsTrue(TasksTitle.Any(x => x.Text.Contains("Fire Consultation Required")), "Failed to create Fire Consultation Required Task");

                }
                Assert.IsTrue(TasksTitle.Any(x => x.Text.Contains("Review Stage Of Works Loading")), "Failed to create Review Stage Of Work Loading Task");
               Assert.IsTrue(TasksTitle.Any(x => x.Text.Contains("File Review")), "Failed to create File Review Task");                                
            }
            catch (Exception ex)
            {
                throw new Exception("Failed to Crate Tasks When COI Sent", ex);
            }

            List<String> TastNames = TasksTitle.Select(i => i.Text.ToString()).ToList();
            if (TastNames.Count > 0)
            {
                for (int i = 0; i < TastNames.Count; i++)
                {
                    if (TastNames[i].Contains("Notify Sales Account Manager of New Quote"))
                    {
                        var temp = $"//task-list//crisp-list[@list-title='Tasks']//ul[@ref='theList']//li[contains(@class,'au-target collection-item')]//crisp-list-item-title//div[@class='title']//div[@class='crisp-row']//div[1][contains(text(),'{TastNames[i]}')]//parent::div//parent::div//parent::crisp-list-item-title//following-sibling::crisp-list-item-details//div[@class='crisp-row']//div[2]";
                        var Status = Driver.FindElement(By.XPath(temp));
                        Assert.IsTrue(Status.Text.Contains("Open"), "Failed to display the task status Open");
                    }
                    if (TastNames[i].Contains("New Order Email Sent"))
                    {
                        var temp = $"//task-list//crisp-list[@list-title='Tasks']//ul[@ref='theList']//li[contains(@class,'au-target collection-item')]//crisp-list-item-title//div[@class='title']//div[@class='crisp-row']//div[1][contains(text(),'{TastNames[i]}')]//parent::div//parent::div//parent::crisp-list-item-title//following-sibling::crisp-list-item-details//div[@class='crisp-row']//div[2]";
                        var Status = Driver.FindElement(By.XPath(temp));
                        Assert.IsTrue(Status.Text.Contains("Open"), "Failed to display the task status Open");
                    }
                    if (Statics.BCProducts.Any(x => x.Contains("Bulding Control")))
                    {
                        if (TastNames[i].Contains("Water Consultation Required"))
                        {
                            var temp = $"//task-list//crisp-list[@list-title='Tasks']//ul[@ref='theList']//li[contains(@class,'au-target collection-item')]//crisp-list-item-title//div[@class='title']//div[@class='crisp-row']//div[1][contains(text(),'{TastNames[i]}')]//parent::div//parent::div//parent::crisp-list-item-title//following-sibling::crisp-list-item-details//div[@class='crisp-row']//div[2]";
                            var Status = Driver.FindElement(By.XPath(temp));
                            Assert.IsTrue(Status.Text.Contains("Open"), "Failed to display the task status Open");
                        }
                        if (TastNames[i].Contains("Fire Consultation Required"))
                        {
                            var temp = $"//task-list//crisp-list[@list-title='Tasks']//ul[@ref='theList']//li[contains(@class,'au-target collection-item')]//crisp-list-item-title//div[@class='title']//div[@class='crisp-row']//div[1][contains(text(),'{TastNames[i]}')]//parent::div//parent::div//parent::crisp-list-item-title//following-sibling::crisp-list-item-details//div[@class='crisp-row']//div[2]";
                            var Status = Driver.FindElement(By.XPath(temp));
                            Assert.IsTrue(Status.Text.Contains("Open"), "Failed to display the task status Open");
                        }
                    }
                    if (TastNames[i].Contains("Review Stage Of Works Loading"))
                    {
                        var temp = $"//task-list//crisp-list[@list-title='Tasks']//ul[@ref='theList']//li[contains(@class,'au-target collection-item')]//crisp-list-item-title//div[@class='title']//div[@class='crisp-row']//div[1][contains(text(),'{TastNames[i]}')]//parent::div//parent::div//parent::crisp-list-item-title//following-sibling::crisp-list-item-details//div[@class='crisp-row']//div[2]";
                        var Status = Driver.FindElement(By.XPath(temp));
                        Assert.IsTrue(Status.Text.Contains("Open"), "Failed to display the task status Open");
                    }                 
                    if (TastNames[i].Contains("File Review"))
                    {
                        var temp = $"//task-list//crisp-list[@list-title='Tasks']//ul[@ref='theList']//li[contains(@class,'au-target collection-item')]//crisp-list-item-title//div[@class='title']//div[@class='crisp-row']//div[1][contains(text(),'{TastNames[i]}')]//parent::div//parent::div//parent::crisp-list-item-title//following-sibling::crisp-list-item-details//div[@class='crisp-row']//div[2]";
                        var Status = Driver.FindElement(By.XPath(temp));
                        Assert.IsTrue(Status.Text.Contains("Open"), "Failed to display the task status Open");
                    }
                }
            }
        }
        public void TasksClosedWhenCOISent()
        {
            if (TasksList.Count == 0)
            {
                OpenTasks();
                Thread.Sleep(500);
                CloseSpinneronDiv();
                CloseSpinneronPage();
            }
            else
            {
                TaskRefresh();
                Thread.Sleep(500);
                CloseSpinneronDiv();
                CloseSpinneronPage();
            }
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            WaitForElements(TasksTitle);
            Assert.IsFalse(TasksTitle.Any(x => x.Text.Contains("Make Quote Decision")), "Failed to close Make Quote Decision Task");
            Assert.IsFalse(TasksTitle.Any(x => x.Text.Contains("Chase Quote Decision from Client")), "Failed to close Chase Quote Decision from Client Task");
            Assert.IsFalse(TasksTitle.Any(x => x.Text.Contains("Chase O/S Documents To Issue Initial Notice")), "Failed to close Chase O/S Documents To Issue Initial Notice Task");
            Assert.IsFalse(TasksTitle.Any(x => x.Text.Contains("Record Response From Local Authority For Initial Notice")), "Failed to close Record Response From Local Authority For Initial Notice Task");
            Assert.IsFalse(TasksTitle.Any(x => x.Text.Contains("Complete Site Risk Assessment")), "Failed to close Complete Site Risk Assessment Task");
            Assert.IsFalse(TasksTitle.Any(x => x.Text.Contains("Manually Trigger Start of Design Review Assessment")), "Failed to close Manually Trigger Start of Design Review Assessment Task");
            Assert.IsFalse(TasksTitle.Any(x => x.Text.Contains("Chase Outstanding Design Documents")), "Failed to close Chase Outstanding Design Documents Task");
            Assert.IsFalse(TasksTitle.Any(x => x.Text.Contains("Manually Trigger Start of Engineering Review Assessment")), "Failed to close Manually Trigger Start of Engineering Review Assessment Task");
            Assert.IsFalse(TasksTitle.Any(x => x.Text.Contains("Make Surveyor Contact")), "Failed to close Make Surveyor Contact Task");
            Assert.IsFalse(TasksTitle.Any(x => x.Text.Contains("Issue Final Notice")), "Failed to close Issue Final Notice Task");
            Assert.IsFalse(TasksTitle.Any(x => x.Text.Contains("Issue Certificate Of Insurance")), "Failed to close Issue Certificate Of Insurance Task");

        }
        public void TasksCreatedWhenRescindCOI()
        {
            if (TasksList.Count == 0)
            {
                OpenTasks();
                Thread.Sleep(500);
                CloseSpinneronDiv();
                CloseSpinneronPage();
            }
            else
            {
                TaskRefresh();
                Thread.Sleep(500);
                CloseSpinneronDiv();
                CloseSpinneronPage();
            }
            try
            {
                Thread.Sleep(500);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                WaitForElements(TasksTitle);
                Assert.IsTrue(TasksTitle.Any(x => x.Text.Contains("Notify Sales Account Manager of New Quote")), "Failed to create Sales Account Manager of New Quote Task");
                Assert.IsTrue(TasksTitle.Any(x => x.Text.Contains("New Order Email Sent")), "Failed to create New Order Email Sent Task");
                if (Statics.BCProducts.Any(x => x.Contains("Bulding Control")))
                {
                    Assert.IsTrue(TasksTitle.Any(x => x.Text.Contains("Water Consultation Required")), "Failed to create Water Consultation Required Task");
                    Assert.IsTrue(TasksTitle.Any(x => x.Text.Contains("Fire Consultation Required")), "Failed to create Fire Consultation Required Task");
                }
                Assert.IsTrue(TasksTitle.Any(x => x.Text.Contains("Review Stage Of Works Loading")), "Failed to create Review Stage Of Work Loading Task");
                Assert.IsTrue(TasksTitle.Any(x => x.Text.Contains("Issue Certificate Of Insurance")), "Failed to create Issue Certificate Of Insurance Task");
                Assert.IsTrue(TasksTitle.Any(x => x.Text.Contains("File Review")), "Failed to create File Review Task");

            }
            catch (Exception ex)
            {
                throw new Exception("Failed to Crate Tasks When Initial Notice Sent", ex);
            }

            List<String> TastNames = TasksTitle.Select(i => i.Text.ToString()).ToList();
            if (TastNames.Count > 0)
            {
                for (int i = 0; i < TastNames.Count; i++)
                {
                    if (TastNames[i].Contains("Notify Sales Account Manager of New Quote"))
                    {
                        var temp = $"//task-list//crisp-list[@list-title='Tasks']//ul[@ref='theList']//li[contains(@class,'au-target collection-item')]//crisp-list-item-title//div[@class='title']//div[@class='crisp-row']//div[1][contains(text(),'{TastNames[i]}')]//parent::div//parent::div//parent::crisp-list-item-title//following-sibling::crisp-list-item-details//div[@class='crisp-row']//div[2]";
                        var Status = Driver.FindElement(By.XPath(temp));
                        Assert.IsTrue(Status.Text.Contains("Open"), "Failed to display the task status Open");
                    }
                    if (TastNames[i].Contains("New Order Email Sent"))
                    {
                        var temp = $"//task-list//crisp-list[@list-title='Tasks']//ul[@ref='theList']//li[contains(@class,'au-target collection-item')]//crisp-list-item-title//div[@class='title']//div[@class='crisp-row']//div[1][contains(text(),'{TastNames[i]}')]//parent::div//parent::div//parent::crisp-list-item-title//following-sibling::crisp-list-item-details//div[@class='crisp-row']//div[2]";
                        var Status = Driver.FindElement(By.XPath(temp));
                        Assert.IsTrue(Status.Text.Contains("Open"), "Failed to display the task status Open");
                    }
                    if (Statics.BCProducts.Any(x => x.Contains("Bulding Control")))
                    {
                        if (TastNames[i].Contains("Water Consultation Required"))
                        {
                            var temp = $"//task-list//crisp-list[@list-title='Tasks']//ul[@ref='theList']//li[contains(@class,'au-target collection-item')]//crisp-list-item-title//div[@class='title']//div[@class='crisp-row']//div[1][contains(text(),'{TastNames[i]}')]//parent::div//parent::div//parent::crisp-list-item-title//following-sibling::crisp-list-item-details//div[@class='crisp-row']//div[2]";
                            var Status = Driver.FindElement(By.XPath(temp));
                            Assert.IsTrue(Status.Text.Contains("Open"), "Failed to display the task status Open");
                        }
                        if (TastNames[i].Contains("Fire Consultation Required"))
                        {
                            var temp = $"//task-list//crisp-list[@list-title='Tasks']//ul[@ref='theList']//li[contains(@class,'au-target collection-item')]//crisp-list-item-title//div[@class='title']//div[@class='crisp-row']//div[1][contains(text(),'{TastNames[i]}')]//parent::div//parent::div//parent::crisp-list-item-title//following-sibling::crisp-list-item-details//div[@class='crisp-row']//div[2]";
                            var Status = Driver.FindElement(By.XPath(temp));
                            Assert.IsTrue(Status.Text.Contains("Open"), "Failed to display the task status Open");
                        }
                    }
                    if (TastNames[i].Contains("Review Stage Of Works Loading"))
                    {
                        var temp = $"//task-list//crisp-list[@list-title='Tasks']//ul[@ref='theList']//li[contains(@class,'au-target collection-item')]//crisp-list-item-title//div[@class='title']//div[@class='crisp-row']//div[1][contains(text(),'{TastNames[i]}')]//parent::div//parent::div//parent::crisp-list-item-title//following-sibling::crisp-list-item-details//div[@class='crisp-row']//div[2]";
                        var Status = Driver.FindElement(By.XPath(temp));
                        Assert.IsTrue(Status.Text.Contains("Open"), "Failed to display the task status Open");
                    }
                    if (TastNames[i].Contains("Issue Certificate Of Insurance"))
                    {
                        var temp = $"//task-list//crisp-list[@list-title='Tasks']//ul[@ref='theList']//li[contains(@class,'au-target collection-item')]//crisp-list-item-title//div[@class='title']//div[@class='crisp-row']//div[1][contains(text(),'{TastNames[i]}')]//parent::div//parent::div//parent::crisp-list-item-title//following-sibling::crisp-list-item-details//div[@class='crisp-row']//div[2]";
                        var Status = Driver.FindElement(By.XPath(temp));
                        Assert.IsTrue(Status.Text.Contains("Open"), "Failed to display the task status Open");
                    }
                    if (TastNames[i].Contains("File Review"))
                    {
                        var temp = $"//task-list//crisp-list[@list-title='Tasks']//ul[@ref='theList']//li[contains(@class,'au-target collection-item')]//crisp-list-item-title//div[@class='title']//div[@class='crisp-row']//div[1][contains(text(),'{TastNames[i]}')]//parent::div//parent::div//parent::crisp-list-item-title//following-sibling::crisp-list-item-details//div[@class='crisp-row']//div[2]";
                        var Status = Driver.FindElement(By.XPath(temp));
                        Assert.IsTrue(Status.Text.Contains("Open"), "Failed to display the task status Open");
                    }
                }
            }
        }
        public void TasksCreatedWhenRescindActivities()
        {
            if (TasksList.Count == 0)
            {
                OpenTasks();
                Thread.Sleep(500);
                CloseSpinneronDiv();
                CloseSpinneronPage();
            }
            else
            {
                TaskRefresh();
                Thread.Sleep(500);
                CloseSpinneronDiv();
                CloseSpinneronPage();
            }
            try
            {
                Thread.Sleep(500);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                WaitForElements(TasksTitle);
                Assert.IsTrue(TasksTitle.Any(x => x.Text.Contains("Notify Sales Account Manager of New Quote")),"Failed to create Sales Account Manager of New Quote Task");
                Assert.IsTrue(TasksTitle.Any(x => x.Text.Contains("New Order Email Sent")),"Failed to create New Order Email Sent Task");
                if (Statics.BCProducts.Any(x => x.Contains("Bulding Control")))
                {
                    Assert.IsTrue(TasksTitle.Any(x => x.Text.Contains("Removal of BC Sign-off")),"Failed to create Removal of BC Sign-off Task");
                    Assert.IsTrue(TasksTitle.Any(x => x.Text.Contains("Water Consultation Required")),"Failed to create Water Consultation Required Task");
                    Assert.IsTrue(TasksTitle.Any(x => x.Text.Contains("Fire Consultation Required")),"Failed to create Fire Consultation Required Task");
                }
                Assert.IsTrue(TasksTitle.Any(x => x.Text.Contains("Review Stage Of Works Loading")),"Failed to create Review Stage Of Work Loading Task");
                Assert.IsTrue(TasksTitle.Any(x => x.Text.Contains("Issue Initial Notice")),"Failed to create Initial Notice Task");
                Assert.IsTrue(TasksTitle.Any(x => x.Text.Contains("File Review")), "Failed to create File Review Task");
                Assert.IsTrue(TasksTitle.Any(x => x.Text.Contains("Removal of COA")),"Failed to create Removal Of COA Task");

            }
            catch (Exception ex)
            {
                throw new Exception("Failed to Crate Tasks When Initial Notice Sent", ex);
            }

            List<String> TastNames = TasksTitle.Select(i => i.Text.ToString()).ToList();
            if (TastNames.Count > 0)
            {
                for (int i = 0; i < TastNames.Count; i++)
                {
                    if (TastNames[i].Contains("Notify Sales Account Manager of New Quote"))
                    {
                        var temp = $"//task-list//crisp-list[@list-title='Tasks']//ul[@ref='theList']//li[contains(@class,'au-target collection-item')]//crisp-list-item-title//div[@class='title']//div[@class='crisp-row']//div[1][contains(text(),'{TastNames[i]}')]//parent::div//parent::div//parent::crisp-list-item-title//following-sibling::crisp-list-item-details//div[@class='crisp-row']//div[2]";
                        var Status = Driver.FindElement(By.XPath(temp));
                        Assert.IsTrue(Status.Text.Contains("Open"), "Failed to display the task status Open");
                    }
                    if (TastNames[i].Contains("New Order Email Sent"))
                    {
                        var temp = $"//task-list//crisp-list[@list-title='Tasks']//ul[@ref='theList']//li[contains(@class,'au-target collection-item')]//crisp-list-item-title//div[@class='title']//div[@class='crisp-row']//div[1][contains(text(),'{TastNames[i]}')]//parent::div//parent::div//parent::crisp-list-item-title//following-sibling::crisp-list-item-details//div[@class='crisp-row']//div[2]";
                        var Status = Driver.FindElement(By.XPath(temp));
                        Assert.IsTrue(Status.Text.Contains("Open"), "Failed to display the task status Open");
                    }
                    if (Statics.BCProducts.Any(x => x.Contains("Bulding Control")))
                    {
                        if (TastNames[i].Contains("Removal of BC Sign-off"))
                        {
                            var temp = $"//task-list//crisp-list[@list-title='Tasks']//ul[@ref='theList']//li[contains(@class,'au-target collection-item')]//crisp-list-item-title//div[@class='title']//div[@class='crisp-row']//div[1][contains(text(),'{TastNames[i]}')]//parent::div//parent::div//parent::crisp-list-item-title//following-sibling::crisp-list-item-details//div[@class='crisp-row']//div[2]";
                            var Status = Driver.FindElement(By.XPath(temp));
                            Assert.IsTrue(Status.Text.Contains("Open"), "Failed to display the task status Open");
                        }
                        if (TastNames[i].Contains("Water Consultation Required"))
                        {
                            var temp = $"//task-list//crisp-list[@list-title='Tasks']//ul[@ref='theList']//li[contains(@class,'au-target collection-item')]//crisp-list-item-title//div[@class='title']//div[@class='crisp-row']//div[1][contains(text(),'{TastNames[i]}')]//parent::div//parent::div//parent::crisp-list-item-title//following-sibling::crisp-list-item-details//div[@class='crisp-row']//div[2]";
                            var Status = Driver.FindElement(By.XPath(temp));
                            Assert.IsTrue(Status.Text.Contains("Open"), "Failed to display the task status Open");
                        }
                        if (TastNames[i].Contains("Fire Consultation Required"))
                        {
                            var temp = $"//task-list//crisp-list[@list-title='Tasks']//ul[@ref='theList']//li[contains(@class,'au-target collection-item')]//crisp-list-item-title//div[@class='title']//div[@class='crisp-row']//div[1][contains(text(),'{TastNames[i]}')]//parent::div//parent::div//parent::crisp-list-item-title//following-sibling::crisp-list-item-details//div[@class='crisp-row']//div[2]";
                            var Status = Driver.FindElement(By.XPath(temp));
                            Assert.IsTrue(Status.Text.Contains("Open"), "Failed to display the task status Open");
                        }
                    }
                    if (TastNames[i].Contains("Review Stage Of Works Loading"))
                    {
                        var temp = $"//task-list//crisp-list[@list-title='Tasks']//ul[@ref='theList']//li[contains(@class,'au-target collection-item')]//crisp-list-item-title//div[@class='title']//div[@class='crisp-row']//div[1][contains(text(),'{TastNames[i]}')]//parent::div//parent::div//parent::crisp-list-item-title//following-sibling::crisp-list-item-details//div[@class='crisp-row']//div[2]";
                        var Status = Driver.FindElement(By.XPath(temp));
                        Assert.IsTrue(Status.Text.Contains("Open"), "Failed to display the task status Open");
                    }
                    if (TastNames[i].Contains("Issue Certificate Of Insurance"))
                    {
                        var temp = $"//task-list//crisp-list[@list-title='Tasks']//ul[@ref='theList']//li[contains(@class,'au-target collection-item')]//crisp-list-item-title//div[@class='title']//div[@class='crisp-row']//div[1][contains(text(),'{TastNames[i]}')]//parent::div//parent::div//parent::crisp-list-item-title//following-sibling::crisp-list-item-details//div[@class='crisp-row']//div[2]";
                        var Status = Driver.FindElement(By.XPath(temp));
                        Assert.IsTrue(Status.Text.Contains("Open"), "Failed to display the task status Open");
                    }
                    if (TastNames[i].Contains("File Review"))
                    {
                        var temp = $"//task-list//crisp-list[@list-title='Tasks']//ul[@ref='theList']//li[contains(@class,'au-target collection-item')]//crisp-list-item-title//div[@class='title']//div[@class='crisp-row']//div[1][contains(text(),'{TastNames[i]}')]//parent::div//parent::div//parent::crisp-list-item-title//following-sibling::crisp-list-item-details//div[@class='crisp-row']//div[2]";
                        var Status = Driver.FindElement(By.XPath(temp));
                        Assert.IsTrue(Status.Text.Contains("Open"), "Failed to display the task status Open");
                    }
                }
            }
        }
        public void TasksCreatedWhenReferralsCreated()
        {
            if (TasksList.Count == 0)
            {
                OpenTasks();
                Thread.Sleep(500);
                CloseSpinneronDiv();
                CloseSpinneronPage();
            }
            else
            {
                TaskRefresh();
                Thread.Sleep(500);
                CloseSpinneronDiv();
                CloseSpinneronPage();
            }
            try
            {
                Thread.Sleep(500);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                WaitForElements(TasksTitle);
                Assert.IsTrue(TasksTitle.Any(x => x.Text.Contains("Notify Sales Account Manager of New Quote")), "Failed to create Sales Account Manager of New Quote Task");
                Assert.IsTrue(TasksTitle.Any(x => x.Text.Contains("New Order Email Sent")), "Failed to create New Order Email Sent Task");
                Assert.IsTrue(TasksTitle.Any(x => x.Text.Contains("Technical Referral Decision")), "Failed to create Technical Referral Decision Task");
                Assert.IsTrue(TasksTitle.Any(x => x.Text.Contains("Insurer Referral Decision")), "Failed to create Insurer Referral Decision Task");
                Assert.IsTrue(TasksTitle.Any(x => x.Text.Contains("Make Surveyor Contact")), "Failed to create Make Surveyor Contact Task");
                Assert.IsTrue(TasksTitle.Any(x => x.Text.Contains("Chase O/S Documents To Issue Initial Notice")), "Failed to create Chase O/S Documents To Issue Initial Notice Task");
                Assert.IsTrue(TasksTitle.Any(x => x.Text.Contains("Complete Site Risk Assessment")), "Failed to create Complete Site Risk Assessment Task");
                Assert.IsTrue(TasksTitle.Any(x => x.Text.Contains("Request For Cancellation Referral Decision")), "Failed to create Request For Cancellation Referral Decision Task");
                Assert.IsTrue(TasksTitle.Any(x => x.Text.Contains("Manually Trigger Start of Design Review Assessment")), "Failed to Create Manually Trigger Start of Design Review Assessment Task");
                Assert.IsTrue(TasksTitle.Any(x => x.Text.Contains("Chase Outstanding Design Documents")), "Failed to create Chase Outstanding Design Documents Task");
                Assert.IsTrue(TasksTitle.Any(x => x.Text.Contains("Technical Escalation Decision")), "Failed to create Technical Escalation Decision Task");

            }
            catch (Exception ex)
            {
                throw new Exception("Failed to Crate Tasks When Referrals created", ex);
            }

            List<String> TastNames = TasksTitle.Select(i => i.Text.ToString()).ToList();
            if (TastNames.Count > 0)
            {
                for (int i = 0; i < TastNames.Count; i++)
                {
                    if (TastNames[i].Contains("Notify Sales Account Manager of New Quote"))
                    {
                        var temp = $"//task-list//crisp-list[@list-title='Tasks']//ul[@ref='theList']//li[contains(@class,'au-target collection-item')]//crisp-list-item-title//div[@class='title']//div[@class='crisp-row']//div[1][contains(text(),'{TastNames[i]}')]//parent::div//parent::div//parent::crisp-list-item-title//following-sibling::crisp-list-item-details//div[@class='crisp-row']//div[2]";
                        var Status = Driver.FindElement(By.XPath(temp));
                        Assert.IsTrue(Status.Text.Contains("Open"), "Failed to display the task status Open");
                    }
                    if (TastNames[i].Contains("New Order Email Sent"))
                    {
                        var temp = $"//task-list//crisp-list[@list-title='Tasks']//ul[@ref='theList']//li[contains(@class,'au-target collection-item')]//crisp-list-item-title//div[@class='title']//div[@class='crisp-row']//div[1][contains(text(),'{TastNames[i]}')]//parent::div//parent::div//parent::crisp-list-item-title//following-sibling::crisp-list-item-details//div[@class='crisp-row']//div[2]";
                        var Status = Driver.FindElement(By.XPath(temp));
                        Assert.IsTrue(Status.Text.Contains("Open"), "Failed to display the task status Open");
                    }
                  
                    if (TastNames[i].Contains("Technical Referral Decision"))
                    {
                            var temp = $"//task-list//crisp-list[@list-title='Tasks']//ul[@ref='theList']//li[contains(@class,'au-target collection-item')]//crisp-list-item-title//div[@class='title']//div[@class='crisp-row']//div[1][contains(text(),'{TastNames[i]}')]//parent::div//parent::div//parent::crisp-list-item-title//following-sibling::crisp-list-item-details//div[@class='crisp-row']//div[2]";
                            var Status = Driver.FindElement(By.XPath(temp));
                            Assert.IsTrue(Status.Text.Contains("Open"), "Failed to display the task status Open");
                    }
                    if (TastNames[i].Contains("Insurer Referral Decision"))
                    {
                            var temp = $"//task-list//crisp-list[@list-title='Tasks']//ul[@ref='theList']//li[contains(@class,'au-target collection-item')]//crisp-list-item-title//div[@class='title']//div[@class='crisp-row']//div[1][contains(text(),'{TastNames[i]}')]//parent::div//parent::div//parent::crisp-list-item-title//following-sibling::crisp-list-item-details//div[@class='crisp-row']//div[2]";
                            var Status = Driver.FindElement(By.XPath(temp));
                            Assert.IsTrue(Status.Text.Contains("Open"), "Failed to display the task status Open");
                    }
                    if (TastNames[i].Contains("Make Surveyor Contact"))
                    {
                            var temp = $"//task-list//crisp-list[@list-title='Tasks']//ul[@ref='theList']//li[contains(@class,'au-target collection-item')]//crisp-list-item-title//div[@class='title']//div[@class='crisp-row']//div[1][contains(text(),'{TastNames[i]}')]//parent::div//parent::div//parent::crisp-list-item-title//following-sibling::crisp-list-item-details//div[@class='crisp-row']//div[2]";
                            var Status = Driver.FindElement(By.XPath(temp));
                            Assert.IsTrue(Status.Text.Contains("Open"), "Failed to display the task status Open");
                    }
                    
                    if (TastNames[i].Contains("Chase O/S Documents To Issue Initial Notice"))
                    {
                        var temp = $"//task-list//crisp-list[@list-title='Tasks']//ul[@ref='theList']//li[contains(@class,'au-target collection-item')]//crisp-list-item-title//div[@class='title']//div[@class='crisp-row']//div[1][contains(text(),'{TastNames[i]}')]//parent::div//parent::div//parent::crisp-list-item-title//following-sibling::crisp-list-item-details//div[@class='crisp-row']//div[2]";
                        var Status = Driver.FindElement(By.XPath(temp));
                        Assert.IsTrue(Status.Text.Contains("Open"), "Failed to display the task status Open");
                    }
                    if (TastNames[i].Contains("Complete Site Risk Assessment"))
                    {
                        var temp = $"//task-list//crisp-list[@list-title='Tasks']//ul[@ref='theList']//li[contains(@class,'au-target collection-item')]//crisp-list-item-title//div[@class='title']//div[@class='crisp-row']//div[1][contains(text(),'{TastNames[i]}')]//parent::div//parent::div//parent::crisp-list-item-title//following-sibling::crisp-list-item-details//div[@class='crisp-row']//div[2]";
                        var Status = Driver.FindElement(By.XPath(temp));
                        Assert.IsTrue(Status.Text.Contains("Open"), "Failed to display the task status Open");
                    }
                    if (TastNames[i].Contains("Request For Cancellation Referral Decision"))
                    {
                        var temp = $"//task-list//crisp-list[@list-title='Tasks']//ul[@ref='theList']//li[contains(@class,'au-target collection-item')]//crisp-list-item-title//div[@class='title']//div[@class='crisp-row']//div[1][contains(text(),'{TastNames[i]}')]//parent::div//parent::div//parent::crisp-list-item-title//following-sibling::crisp-list-item-details//div[@class='crisp-row']//div[2]";
                        var Status = Driver.FindElement(By.XPath(temp));
                        Assert.IsTrue(Status.Text.Contains("Open"), "Failed to display the task status Open");
                    }
                    if (TastNames[i].Contains("Manually Trigger Start of Design Review Assessment"))
                    {
                        var temp = $"//task-list//crisp-list[@list-title='Tasks']//ul[@ref='theList']//li[contains(@class,'au-target collection-item')]//crisp-list-item-title//div[@class='title']//div[@class='crisp-row']//div[1][contains(text(),'{TastNames[i]}')]//parent::div//parent::div//parent::crisp-list-item-title//following-sibling::crisp-list-item-details//div[@class='crisp-row']//div[2]";
                        var Status = Driver.FindElement(By.XPath(temp));
                        Assert.IsTrue(Status.Text.Contains("Open"), "Failed to display the task status Open");
                    }
                    if (TastNames[i].Contains("Chase Outstanding Design Documents"))
                    {
                        var temp = $"//task-list//crisp-list[@list-title='Tasks']//ul[@ref='theList']//li[contains(@class,'au-target collection-item')]//crisp-list-item-title//div[@class='title']//div[@class='crisp-row']//div[1][contains(text(),'{TastNames[i]}')]//parent::div//parent::div//parent::crisp-list-item-title//following-sibling::crisp-list-item-details//div[@class='crisp-row']//div[2]";
                        var Status = Driver.FindElement(By.XPath(temp));
                        Assert.IsTrue(Status.Text.Contains("Open"), "Failed to display the task status Open");
                    }
                }
            }
        }

        public void TasksClosedWhenReferralClosed()
        {
            if (TasksList.Count == 0)
            {
                OpenTasks();
                Thread.Sleep(500);
                CloseSpinneronDiv();
                CloseSpinneronPage();
            }
            else
            {
                TaskRefresh();
                Thread.Sleep(500);
                CloseSpinneronDiv();
                CloseSpinneronPage();
            }
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            WaitForElements(TasksTitle);
            Assert.IsFalse(TasksTitle.Any(x => x.Text.Contains("Technical Referral Decision")), "Failed to close Technical Referral Decision Task");
            Assert.IsFalse(TasksTitle.Any(x => x.Text.Contains("Insurer Referral Decision")), "Failed to close Insurer Referral Decision Task");          
            Assert.IsFalse(TasksTitle.Any(x => x.Text.Contains("Request For Cancellation Referral Decision")), "Failed to close Request For Cancellation Referral Decision Task");
            Assert.IsFalse(TasksTitle.Any(x => x.Text.Contains("Technical Escalation Decision")), "Failed to close Technical Escalation Decision Task");
        }
    }
}
