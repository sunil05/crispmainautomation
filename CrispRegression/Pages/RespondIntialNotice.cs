﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.PageObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace CrispAutomation.Pages
{
    public class RespondIntialNotice : Support.Pages
    {
        public IWebDriver wdriver;
     public RespondIntialNotice(ISearchContext driver)
        {
            PageFactory.InitElements(driver, this);
            wdriver = (RemoteWebDriver)driver;
        }

        [FindsBy(How = How.XPath, Using = "//crisp-input-radio[@label='Formal acceptance provided by Local Authority?']/div/ul/li/label[text()='Yes']")]
        public IWebElement FormalAcceptanceButton;

        [FindsBy(How = How.XPath, Using = "//crisp-footer//crisp-footer-button[@click.delegate='goForward()'][@class='au-target']//button[text()='Next']")]
        public IWebElement AcceptanceIntialNoticeResponseNextButton;
       
        [FindsBy(How = How.XPath, Using = "//crisp-tabs/div/ul/li/span/a[contains(text(),'Blocking (0)')]")]
        public IWebElement Blockers;

        [FindsBy(How = How.XPath, Using = "//crisp-card-content/div/div[contains(text(),'There are no companies or individuals with blocks')]")]
        public IWebElement NoBlockers;

        [FindsBy(How = How.XPath, Using = "//crisp-tabs/div/ul/li/span/a[contains(text(),'Conditions (0)')]")]
        public IWebElement Conditions;

        [FindsBy(How = How.XPath, Using = "//crisp-card-content/div/div[contains(text(),'There are no Conditions')]")]
        public IWebElement NoConditions;

        [FindsBy(How = How.XPath, Using = "//crisp-card-header/div/span[text()='Complete send Initial Notice Response - Acceptance']")]
        public IWebElement ConfirmDetailsPage;


        [FindsBy(How = How.XPath, Using = "//crisp-card-header/div/span[text()='Complete send Initial Notice Response - Reject']")]
        public IWebElement ConfirmDetailsPageonResponseReject;

        [FindsBy(How = How.XPath, Using = "//div/table/tbody/tr[1]/td/p/a[@href='https://www.premierguarantee.co.uk/']/img")]
        public IWebElement PGGenerateQuote;
        
        [FindsBy(How = How.XPath, Using = "//crisp-footer-button[@class='au-target']//button[text()='Send Initial Notice Response - Acceptance']")]
        public IWebElement SendIntialNoticeResponseAcceptanceButton;

        [FindsBy(How = How.XPath, Using = "//crisp-tabs//div//ul[@ref='tabHeader'][@class='au-target tabs highlight-background']//li[@class='au-target tab']//span//a[contains(text(),'Building Control')][contains(@class,'au-target highlight-colour')]")]
        public IWebElement BuildingControlTab;

        [FindsBy(How = How.XPath, Using = "//crisp-list[@list-title='Processes']/ul[@ref='theList']//li[@class='au-target collection-item']//crisp-list-item//a//crisp-list-item-details")]
        public IWebElement Processes;

        [FindsBy(How = How.XPath, Using = "//dl[1]/dt[text()='Current Status']")]
        public IWebElement IntialNoticeCurrentStatus;

        [FindsBy(How = How.CssSelector, Using = "dl:nth-child(1) > dd:nth-child(2)")]
        public IWebElement IntialNoticeCurrentStatusValue;

        [FindsBy(How = How.XPath, Using = "//dl[2]/dt[text()='Response']")]
        public IWebElement IntialNoticeCurrentStatusResponse;

        [FindsBy(How = How.CssSelector, Using = "dl:nth-child(3) > dd:nth-child(2)")]
        public IWebElement IntialNoticeCurrentStatusResponseValue;

        [FindsBy(How = How.XPath, Using = "//ul/li/crisp-list-item/a/crisp-list-item-title/div/span[text()='Rejected']")]
        public IWebElement RejectIntialNotice;

        [FindsBy(How = How.XPath, Using = "//div/crisp-header/nav/div")]
        public IWebElement InitialNoticeResponseRejectWizard;

        [FindsBy(How = How.XPath, Using = "//crisp-content/crisp-input-object/div/label[text()='Rejection Reason']")]
        public IWebElement RejectionReason;

        [FindsBy(How = How.XPath, Using = "//crisp-list//ul/li[@class='au-target collection-item']/crisp-list-item//a[@class='au-target row list-item-contents clickable']")]
        public IList<IWebElement> RejectionReasonsList;
        
        [FindsBy(How = How.XPath, Using = "//crisp-input-text[@label='Rejection Notes']/div/label[text()='Rejection Notes']")]
        public IWebElement RejectionNotesLabel;
        
        [FindsBy(How = How.XPath, Using = "//crisp-input-text[@label='Rejection Notes']/div/input")]
        public IWebElement RejectionNotesInput;

        [FindsBy(How = How.XPath, Using = "//crisp-footer-button[@class='au-target']//button[text()='Send Initial Notice Response - Reject']")]
        public IWebElement SendIntialNoticeResponseRejectButton;

        


        public void FileReviewDetails()
        {
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(1000);
            Assert.IsTrue(Blockers.Displayed);
            Assert.IsTrue(NoBlockers.Displayed);
            WaitForElement(Conditions);
            Assert.IsTrue(Conditions.Displayed);
            //Thread.Sleep(500);

        }
        public void ConfirmDetails()
        {
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(1000);
            Assert.IsTrue(ConfirmDetailsPage.Displayed);
            //Thread.Sleep(500);
        }
        public void IntialNoticeAcceptanceDetails()
        {
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(1000);
            Assert.True(PGGenerateQuote.Displayed);
            //Thread.Sleep(500);
        }

        public void IntialNoticeStatusSent()
        {
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(1000);
            WaitForElement(IntialNoticeCurrentStatus);
            Assert.IsTrue(IntialNoticeCurrentStatus.Displayed);
            WaitForElement(IntialNoticeCurrentStatusValue);
            if (IntialNoticeCurrentStatusValue.Text.Contains("Sending"))
            {
                CloseSpinneronDiv();
                CloseSpinneronPage();
                //Thread.Sleep(1000);
                WaitForElement(Dashboardpage.ReloadButton);
                Dashboardpage.ReloadButton.Click();
                //Thread.Sleep(500);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                //Thread.Sleep(500);
                WaitForElement(BuildingControlTab);
                WaitForElementToClick(BuildingControlTab);
                //Thread.Sleep(500);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                //Thread.Sleep(500);
                BuildingControlTab.Click();
                //Thread.Sleep(500);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                //Thread.Sleep(500);
                WaitForElement(Processes);
                Processes.Click();
                WaitForElement(IntialNoticeCurrentStatus);
                Assert.IsTrue(IntialNoticeCurrentStatus.Displayed);
                WaitForElement(IntialNoticeCurrentStatusValue);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                Thread.Sleep(500);
                Assert.IsTrue(IntialNoticeCurrentStatusValue.Text.Contains("Initial Notice Acceptance issued to Client"));
                //Thread.Sleep(500);
                CloseSpinneronDiv();
            }
            else
            {
                Assert.IsTrue(IntialNoticeCurrentStatusValue.Text.Contains("Initial Notice Acceptance issued to Client"));
                //Thread.Sleep(1000);
            }
        }

        public void IntialNoticeStatusDetails()
        {
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(1000);
            WaitForElement(IntialNoticeCurrentStatus);
            Assert.IsTrue(IntialNoticeCurrentStatus.Displayed);
            WaitForElement(IntialNoticeCurrentStatusValue);
            Assert.True(IntialNoticeCurrentStatusValue.Text.Contains("Accepted"));
            WaitForElement(IntialNoticeCurrentStatusResponse);
            Assert.IsTrue(IntialNoticeCurrentStatusResponse.Displayed);
            WaitForElement(IntialNoticeCurrentStatusResponseValue);
            Assert.True(IntialNoticeCurrentStatusResponseValue.Text.Contains("Accepted"));
        }
        public void RejectionDetails()
        {
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(1000);
            WaitForElement(RejectionReason);
            RejectionReason.Click();
            WaitForElements(RejectionReasonsList);
            if (RejectionReasonsList.Count > 0)
            {
                WaitForElement(RejectionReasonsList[1]);
                RejectionReasonsList[1].Click();
                CloseSpinneronDiv();
                CloseSpinneronPage();
                Thread.Sleep(500);
            }
            WaitForElement(RejectionNotesLabel);
            RejectionNotesLabel.Click();
            WaitForElement(RejectionNotesInput);
            RejectionNotesInput.SendKeys("Rejected Today");
        }

        public void RejectionConfirmDetails()
        {
            //Thread.Sleep(500);
            Assert.IsTrue(ConfirmDetailsPageonResponseReject.Displayed);
            //Thread.Sleep(500);
        }

        public void IntialNoticeStatusDetailsonRejection()
        {
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(1000);
            WaitForElement(IntialNoticeCurrentStatus);
            Assert.IsTrue(IntialNoticeCurrentStatus.Displayed);
            WaitForElement(IntialNoticeCurrentStatusValue);
            Assert.True(IntialNoticeCurrentStatusValue.Text.Contains("Rejected"));
            WaitForElement(IntialNoticeCurrentStatusResponse);
            Assert.IsTrue(IntialNoticeCurrentStatusResponse.Displayed);
            WaitForElement(IntialNoticeCurrentStatusResponseValue);
            Assert.True(IntialNoticeCurrentStatusResponseValue.Text.Contains("Rejected"));
        }

    }

}
