﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using System.Configuration;
using OpenQA.Selenium.Chrome;
using CrispAutomation.Features;
using System.Threading;
using OpenQA.Selenium.Remote;

namespace CrispAutomation.Pages
{
    public class SurveyorLoginPage :Support.Pages
    {
        public IWebDriver wdriver;
        public SurveyorLoginPage(ISearchContext driver)
        {
            PageFactory.InitElements(driver,this);
            wdriver = (RemoteWebDriver)driver;
        }
        [FindsBy(How = How.Id, Using = "Input_Email")]
        public IWebElement Email;

        [FindsBy(How = How.Id, Using = "Input_Password")]
        public IWebElement Password;

        [FindsBy(How = How.XPath, Using = "//div[@class='login-box']/div[@class='logo']/img")]
        public IWebElement SureveyorLoginPage;

        [FindsBy(How = How.XPath, Using = "//button[text()='SIGN IN']")]
        public IWebElement SignInButton;

        [FindsBy(How = How.XPath, Using = "//div[@class='navbar-header']//a[text()='Crisp Surveyors Portal']")]
        public IWebElement SurveyorPortal;

        [FindsBy(How = How.XPath, Using = "//a/span[text()='Sites']")]
        public IWebElement Sites;

        [FindsBy(How = How.XPath, Using = "//div[@id='sitesTable_filter']//input[@placeholder='Search sites...']")]
        public IWebElement Search;

        [FindsBy(How = How.XPath, Using = "//div/div/button[text()='Search']")]
        public IWebElement SearchButton;

        [FindsBy(How = How.XPath, Using = "//div/label[@for='allSites']")]
        public IWebElement AllSitesCheckbox;

        [FindsBy(How = How.XPath, Using = "//div/table/tbody/tr/th/a[1]")]
        public IWebElement OrderRef;


        public void  SurveyorLoginMethod()
        {
            //Thread.Sleep(500);           
            Driver.Navigate().GoToUrl(ConfigurationManager.AppSettings["SurveyorUrl"]);
            //Thread.Sleep(500);
            CloseSpinnerOnSurveyorPage();
            //Thread.Sleep(500);
            WaitForElement(SureveyorLoginPage);
            //Thread.Sleep(500);
            CloseSpinnerOnSurveyorPage();
            //Thread.Sleep(500);
            WaitForElement(Email);
            Email.SendKeys(ConfigurationManager.AppSettings["SurveyorUsername"]);
            WaitForElement(Password);
            Password.SendKeys(ConfigurationManager.AppSettings["SurveyorPassword"]); ;
            WaitForElement(SignInButton);
            SignInButton.Click();
            //Thread.Sleep(500);
            CloseSpinnerOnSurveyorPage();
            CloseSpinnerOnSurveyorDiv();
            //Thread.Sleep(500);    
            Assert.IsTrue(SurveyorPortal.Displayed, "User login failed on Surveyor Portal");            
        }
        public void SelectSite()
        {
            string currentURL = ConfigurationManager.AppSettings["SurveyorUrl"];
            StringBuilder newurl = new StringBuilder();
            currentURL = newurl.Append(currentURL).Append($"/sites/overview/{Statics.OrderId}").ToString(); 
            //Thread.Sleep(500);
            Driver.Navigate().GoToUrl(currentURL);
            //Thread.Sleep(500);
            CloseSpinnerOnSurveyorPage();
            CloseSpinnerOnSurveyorDiv();
            //Thread.Sleep(500);
        }

        public void SiteOverviewPage()
        {

        }
    }
}
