﻿using OpenQA.Selenium;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.PageObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CrispAutomation.Pages
{
    public class LABCRegistrations : Support.Pages
    {
        public IWebDriver wdriver;
        public LABCRegistrations(ISearchContext driver)
        {
            PageFactory.InitElements(driver, this);
            wdriver = (RemoteWebDriver)driver;
        }
        [FindsBy(How = How.XPath, Using = "//crisp-tabs//ul[@ref='tabHeader'][@class='au-target tabs highlight-background']//li[@class='au-target tab']//span//a[contains(text(),'LABC Registration')][contains(@class,'au-target highlight-colour')]")]
        public IWebElement LABCRegistrationTab;

        [FindsBy(How = How.XPath, Using = " //registration-view//crisp-header//nav//div//crisp-header-title//div[contains(text(),'Reference')]")]
        public IWebElement RegistrationReference;

        [FindsBy(How = How.XPath, Using = "//button[@ref='button'][text()='Create Registration'][@class='au-target waves-effect waves-light btn-flat']")]
        public IList<IWebElement> CreateRegistrationButton;
    }
}
