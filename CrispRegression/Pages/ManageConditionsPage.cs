﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using System.Threading;
using CrispAutomation.Support;
using NUnit.Framework;
using OpenQA.Selenium.Remote;

namespace CrispAutomation.Pages
{

    public class ManageConditionsPage : Support.Pages
    {
        public IWebDriver wdriver;
        string SiteCondition = "Manual Condition" + ExtensionMethods.RunId.Id;
        string ProductCondition = "Manual Condition" + ExtensionMethods.RunId.Id;
        string PlotCondition = "Manual Condition" + ExtensionMethods.RunId.Id;

        public ManageConditionsPage(ISearchContext driver)
        {
            PageFactory.InitElements(driver, this);
            wdriver = (RemoteWebDriver)driver;
        }

        [FindsBy(How = How.XPath, Using = "//div//crisp-tabs//div/ul//li//span//a[contains(text(),'Conditions')]")]
        public IWebElement ConditionsOption;

        [FindsBy(How = How.XPath, Using = "//div//condition-list//div//div//crisp-table//div//div//div//span[text()='Conditions']")]
        public IWebElement ConditionsTitle;

        [FindsBy(How = How.XPath, Using = "//crisp-action-button[@click.delegate='addCondition()']//div//a//i[text()='add']")]
        public IWebElement AddConditionButton;

        [FindsBy(How = How.XPath, Using = "//crisp-dialog//div//crisp-header//nav//div//crisp-header-title//div[text()='New Condition']")]
        public IWebElement NewConditionWizard;

        [FindsBy(How = How.XPath, Using = "//condition-list//div//div//crisp-table//div//div//table//tbody")]
        public IList<IWebElement> ConditionsTableList;

        [FindsBy(How = How.XPath, Using = "//condition-list//div//div//crisp-table//div//div//table//tbody//tr")]
        public IList<IWebElement> ConditionsRowsList;

        [FindsBy(How = How.XPath, Using = "//condition-list//div//div//crisp-table//div//div//table//tbody//tr//td//div//div")]
        public IList<IWebElement> ConditionsList;

        [FindsBy(How = How.XPath, Using = "//crisp-input-textarea//div//label[text()='Condition text']")]
        public IWebElement ConditionsTextLabel;

        [FindsBy(How = How.XPath, Using = "//crisp-input-textarea[@label='Condition text']//div//textarea")]
        public IWebElement ConditionsTextinput;

        [FindsBy(How = How.XPath, Using = "//crisp-card-content[@class='au-target']//div[@class='card-content']/dl/dd[1]")]
        public IWebElement ConditionName;

        [FindsBy(How = How.XPath, Using = "//crisp-input-object//label[text()='Role to chase']")]
        public IWebElement RoletoChase;

        [FindsBy(How = How.XPath, Using = "//crisp-list//ul[@ref='theList']//li[@class='au-target collection-item']//crisp-list-item//a[@class='au-target row list-item-contents clickable']//crisp-list-item-title//div")]
        public IList<IWebElement> RoletoChaseInput;

        [FindsBy(How = How.XPath, Using = "//crisp-picker[@label='When to chase']//div//input[@class='select-dropdown']")]
        public IWebElement WhentoChaseDropdown;

        [FindsBy(How = How.XPath, Using = "//ul[@class='dropdown-content select-dropdown active']//li//span[text()='Quote']")]
        public IWebElement WhentoChaseDropdownInput;

        [FindsBy(How = How.XPath, Using = "//crisp-input-bool[@label='Is internal?']//label[text()='Is internal?']")]
        public IWebElement IsInternalCheckBox;

        [FindsBy(How = How.XPath, Using = "//crisp-input-object[@editor-focus.delegate='addFile()']//div//span//span")]
        public IWebElement RelatedFiles;

        [FindsBy(How = How.XPath, Using = "//crisp-card-actions//crisp-button[@click.delegate='ok()']//button[@class='au-target waves-effect waves-light btn']")]
        public IWebElement SelectButton;

        [FindsBy(How = How.XPath, Using = "//crisp-input-object[@editor-focus.delegate='focus()']//div//label[text()='Processes']//parent::div//span[@class='au-target']")]
        public IWebElement ProcessesLabel;

        [FindsBy(How = How.XPath, Using = "//crisp-card-content[@class='au-target']//span/input[@type='text']")]
        public IWebElement SearchLabel;

        [FindsBy(How = How.XPath, Using = "//div[@class='card-content']//crisp-list//ul[@ref='theList']//li[@class='au-target collection-item']")]
        public IList<IWebElement> ProcessList;

        [FindsBy(How = How.XPath, Using = "//crisp-picker[@label='Scope']//div//input[@class='select-dropdown']")]
        public IWebElement ScopeDropdown;

        [FindsBy(How = How.XPath, Using = "//ul[@class='dropdown-content select-dropdown active']//li//span[text()='Site']")]
        public IWebElement ScopeDropdownInputSite;

        [FindsBy(How = How.XPath, Using = "//ul[@class='dropdown-content select-dropdown active']//li//span[text()='Products']")]
        public IWebElement ScopeDropdownInputProducts;

        [FindsBy(How = How.XPath, Using = "//crisp-input-object[2][@editor-focus.delegate='focus()']//div//span//span")]
        public IWebElement ProductsLabel;

        [FindsBy(How = How.XPath, Using = "//div[@class='card-content']//crisp-list//ul[@ref='theList']//li[@class='au-target collection-item']")]
        public IList<IWebElement> ProductsList;

        [FindsBy(How = How.XPath, Using = "//ul[@class='dropdown-content select-dropdown active']//li//span[text()='Plots']")]
        public IWebElement ScopeDropdownInputPlots;

        [FindsBy(How = How.XPath, Using = "//crisp-input-object[2][@editor-focus.delegate='focus()']//div//span//span")]
        public IWebElement PlotsLabel;

        [FindsBy(How = How.XPath, Using = "//div[@class='card-content']//crisp-list//ul[@ref='theList']//li[@class='au-target collection-item']")]
        public IList<IWebElement> PlotsList;

        [FindsBy(How = How.XPath, Using = "//crisp-button[@icon='save']//button[@class='au-target waves-effect waves-light btn-flat'][text()='Save']")]
        public IWebElement SaveButton;

        //Send Quote Elements When Conditions Are Opened 

        [FindsBy(How = How.XPath, Using = "//conditions//crisp-table//div//div//table//tbody//tr")]
        public IList<IWebElement> SendQuoteConditionsList;

        [FindsBy(How = How.XPath, Using = "//crisp-tabs/div/ul/li/span/a[contains(text(),'Conditions (3)')]")]
        public IWebElement SendQuoteFileReviewConditionsList;

        [FindsBy(How = How.XPath, Using = "//crisp-wizard-step[@step-title='Confirm'][@class='au-target active']//confirm//crisp-card[@class='au-target']//crisp-card-content[@class='au-target']//div//p")]
        public IWebElement Confirmchase;

        [FindsBy(How = How.XPath, Using = "//crisp-input-radio//label[text()='Chase Outstanding Information']")]
        public IWebElement ChaseInformationRadioButton;

        

        //Closing Manual Conditions 
        [FindsBy(How = How.XPath, Using = "//condition-list//div//div//crisp-table//div//div//table//tbody//tr//td//crisp-table-td//div//div[text()='Manual']")]
        public IList<IWebElement> ManualConditionsList;

        [FindsBy(How = How.XPath, Using = "//crisp-button[@click.delegate='close()']//span//button")]
        public IWebElement ConditionCloseButton;

        [FindsBy(How = How.XPath, Using = "//crisp-input-textarea[@label='Closing comments']/div/textarea")]
        public IWebElement ClosingComments;

        [FindsBy(How = How.XPath, Using = "//crisp-dialog/div/crisp-header[@text='Close Condition']")]
        public IWebElement CloseConditionDiv;

        // Clone Manual Condition Elements  
        [FindsBy(How = How.XPath, Using = "//crisp-button[@click.delegate='clone()']//span//button")]
        public IWebElement ConditionCloneButton;

        [FindsBy(How = How.XPath, Using = "//div/crisp-header//nav//div//crisp-header-title//div[text()='New Condition']")]
        public IWebElement NewConditionDiv;

        //Clone Automatic Condition Elements 
        [FindsBy(How = How.XPath, Using = "//condition-list//div//div//crisp-table//div//div//table//tbody//tr//td//crisp-table-td//div//div[text()='Automatic']")]
        public IList<IWebElement> AutomaticConditionsList;

        [FindsBy(How = How.XPath, Using = "//div[@class='modal-content']//crisp-input-object//div//span[@class='error'][text()='At least one process is required']")]
        public IList<IWebElement> ProcessInput;        

        //Edit Condition Elememnts 
        [FindsBy(How = How.XPath, Using = "//crisp-button[@click.delegate='amend()']//span//button")]
        public IWebElement ConditionEditButton;

        [FindsBy(How = How.XPath, Using = "//div/crisp-header//nav//div//crisp-header-title//div[text()='Edit Condition']")]
        public IWebElement EditConditionDiv;

        [FindsBy(How = How.XPath, Using = "//crisp-input-textarea//div//label[text()='Amendment comments']")]
        public IWebElement AmendmentCommentsLabel;

        [FindsBy(How = How.XPath, Using = "//crisp-input-textarea[@label='Amendment comments']//div//textarea")]
        public IWebElement AmendmentCommentsInput;

        [FindsBy(How = How.XPath, Using = "//crisp-card-content[@class='au-target']//div[@class='card-content']//dl//dd[9]")]
        public IWebElement LastAmendmentsInput;

        [FindsBy(How = How.XPath, Using = "//ai-dialog-container//crisp-dialog//div[contains(@class,'au-target modal dialog')]")]
        public IList<IWebElement> ModelDialogues;

      


        public void AddAndVerifySiteCondition()
        {
            //Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(500);
            WaitForElements(ConditionsRowsList);
            int numberOfOriginalRows = ConditionsTableList.Count;
            //Thread.Sleep(1000);
            WaitForElement(AddConditionButton);
            //Thread.Sleep(1500);
            AddConditionButton.Click();
            WaitForElement(NewConditionWizard);
            Assert.IsTrue(NewConditionWizard.Displayed);
            WaitForElement(ConditionsTextLabel);
            ConditionsTextLabel.Click();
            WaitForElement(ConditionsTextinput);
            ConditionsTextinput.SendKeys(SiteCondition);
            WaitForElement(RoletoChase);
            WaitForElementToClick(RoletoChase);
            RoletoChase.Click();
            //Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            WaitForElements(RoletoChaseInput);
            if(RoletoChaseInput.Count>0)
            {
                var roleToChase = RoletoChaseInput.FirstOrDefault(x => x.Text.Contains("Associated Developer"));
                roleToChase.Click();
                CloseCrispCard();
                //Thread.Sleep(500);
                CloseSpinneronDiv();
                CloseSpinneronPage();
            }            
            WaitForElement(WhentoChaseDropdown);
            WaitForElementToClick(WhentoChaseDropdown);
            WhentoChaseDropdown.Click();
            WaitForElement(WhentoChaseDropdownInput);
            WhentoChaseDropdownInput.Click();
            WaitForElement(IsInternalCheckBox);
            IsInternalCheckBox.Click();
            WaitForElement(RelatedFiles);
            RelatedFiles.Click();
            PageUtils.UploadNewFile();
            CloseCrispCard();
            WaitForElement(ProcessesLabel);
            ProcessesLabel.Click();
            //Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            WaitForElements(ProcessList);
            if (ProcessList.Count > 0)
            {
                foreach (var eachprocess in ProcessList)
                {
                    WaitForElement(eachprocess);
                    eachprocess.Click();
                }
            }
            //Thread.Sleep(1000);
            WaitForElement(SelectButton);
            SelectButton.Click();
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            WaitForElement(ScopeDropdown);
            ScopeDropdown.Click();
            WaitForElement(ScopeDropdownInputSite);
            ScopeDropdownInputSite.Click();
            //Thread.Sleep(1000);
            WaitForElement(SaveButton);
            SaveButton.Click();
            //Thread.Sleep(1000);
            WaitForElements(ConditionsTableList);
            //Thread.Sleep(1000);
            int actualOriginalRows = numberOfOriginalRows + 1;
            Assert.AreEqual(ConditionsTableList.Count, actualOriginalRows);
            Assert.IsTrue(ConditionsList.Any(x => x.Text == SiteCondition));
            Assert.IsTrue(ConditionsList.Count(x => x.Text == SiteCondition) >= 1);

            //Thread.Sleep(1000);
            //if (ConditionsTableList.Count == numberOfOriginalRows + 1)
            //{
            //   var result =  ConditionsList.Any(x => x.Text == SiteCondition);
            //   //var condition2 = ConditionsList.Any(x => x.Text == SiteCondition);
            //    foreach (var eachcondition in ConditionsList)
            //    {
            //        var addedSiteConditionText = eachcondition.Text.Contains(SiteCondition);
            //        if (addedSiteConditionText)
            //        {
            //            Assert.IsTrue(addedSiteConditionText);
            //        }
            //        else
            //        {
            //            break;
            //
            //        }
            //    }
            //    Console.WriteLine("Site Condition Is Added to the List");
            //}
        }

        public void AddAndVerifyProductsCondition()
        {
            //Thread.Sleep(1000);
            WaitForElements(ConditionsRowsList);
            int numberOfOriginalRows = ConditionsTableList.Count;
            WaitForElement(AddConditionButton);
            AddConditionButton.Click();
            WaitForElement(NewConditionWizard);
            Assert.IsTrue(NewConditionWizard.Displayed);
            WaitForElement(ConditionsTextLabel);
            ConditionsTextLabel.Click();
            WaitForElement(ConditionsTextinput);
            ConditionsTextinput.SendKeys(ProductCondition);
            WaitForElement(RoletoChase);
            WaitForElementToClick(RoletoChase);
            RoletoChase.Click();
            //Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            WaitForElements(RoletoChaseInput);
            if (RoletoChaseInput.Count > 0)
            {
                var roleToChase = RoletoChaseInput.FirstOrDefault(x => x.Text.Contains("Associated Developer"));
                roleToChase.Click();
                CloseCrispCard();
                //Thread.Sleep(500);
                CloseSpinneronDiv();
                CloseSpinneronPage();
            }
            WaitForElement(WhentoChaseDropdown);
            WhentoChaseDropdown.Click();
            WaitForElement(WhentoChaseDropdownInput);
            WhentoChaseDropdownInput.Click();
            WaitForElement(IsInternalCheckBox);
            IsInternalCheckBox.Click();
            WaitForElement(RelatedFiles);
            //RelatedFiles.Click();
            //PageUtils.UploadNewFile();
            WaitForElement(ProcessesLabel);
            WaitForElementToClick(ProcessesLabel);
            ProcessesLabel.Click();
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            WaitForElements(ProcessList);
            if (ProcessList.Count > 0)
            {
                foreach (var eachprocess in ProcessList)
                {
                    WaitForElement(eachprocess);
                    eachprocess.Click();
                    CloseSpinneronDiv();
                    CloseSpinneronPage();
                }
            }
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            WaitForElement(SelectButton);
            SelectButton.Click();
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            WaitForElement(ScopeDropdown);
            ScopeDropdown.Click();
            WaitForElement(ScopeDropdownInputProducts);
            ScopeDropdownInputProducts.Click();
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            WaitForElement(ProductsLabel);
            WaitForElementToClick(ProductsLabel);
            ProductsLabel.Click();
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            WaitForElements(ProductsList);
            foreach (var eachproduct in ProductsList)
            {
                eachproduct.Click();
                Thread.Sleep(500);               
                CloseSpinneronDiv();
                CloseSpinneronPage();
            }            
            CloseSpinneronDiv();
            CloseSpinneronPage();
            WaitForElement(SelectButton);
            SelectButton.Click();
            CloseSpinneronDiv();
            CloseSpinneronPage();
            WaitForElement(SaveButton);
            SaveButton.Click();
            CloseSpinneronDiv();
            CloseSpinneronPage();
            WaitForElements(ConditionsTableList);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            int actualOriginalRows = numberOfOriginalRows + 1;
            Assert.AreEqual(ConditionsTableList.Count, actualOriginalRows);
            Assert.IsTrue(ConditionsList.Any(x => x.Text == ProductCondition));
            Assert.IsTrue(ConditionsList.Count(x => x.Text == ProductCondition) >= 1);
            //Thread.Sleep(1000);
            //Assert.AreEqual(ConditionsTableList.Count, numberOfOriginalRows+1);
            //foreach (var eachcondition in ConditionsList)
            //{
            //    var addedProductConditionText = eachcondition.Text.Contains(ProductCondition);
            //    if (addedProductConditionText)
            //    {
            //        Assert.IsTrue(addedProductConditionText);
            //    }
            //    else
            //    {
            //        break;
            //    }
            //}
            //Console.WriteLine("Site Condition Is Added to the List");

        }
        public void AddAndVerifyPlotsCondition()
        {
            //Thread.Sleep(1000);
            WaitForElements(ConditionsRowsList);
            int numberOfOriginalRows = ConditionsTableList.Count;
            WaitForElement(AddConditionButton);
            AddConditionButton.Click();
            WaitForElement(NewConditionWizard);
            Assert.IsTrue(NewConditionWizard.Displayed);
            WaitForElement(ConditionsTextLabel);
            ConditionsTextLabel.Click();
            WaitForElement(ConditionsTextinput);
            ConditionsTextinput.SendKeys(PlotCondition);
            WaitForElement(RoletoChase);
            WaitForElementToClick(RoletoChase);
            RoletoChase.Click();
            //Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            WaitForElements(RoletoChaseInput);
            if (RoletoChaseInput.Count > 0)
            {
                var roleToChase = RoletoChaseInput.FirstOrDefault(x => x.Text.Contains("Associated Developer"));
                roleToChase.Click();
                CloseCrispCard();
                //Thread.Sleep(500);
                CloseSpinneronDiv();
                CloseSpinneronPage();
            }
            WaitForElement(WhentoChaseDropdown);
            WhentoChaseDropdown.Click();
            WaitForElement(WhentoChaseDropdownInput);
            WhentoChaseDropdownInput.Click();
            WaitForElement(IsInternalCheckBox);
            IsInternalCheckBox.Click();
            WaitForElement(RelatedFiles);
            RelatedFiles.Click();
            PageUtils.UploadNewFile();
            //Thread.Sleep(500);
            WaitForElement(ProcessesLabel);
            //Thread.Sleep(500);
            ProcessesLabel.Click();
            //Thread.Sleep(1000);
            WaitForElements(ProcessList);
            foreach (var eachproduct in ProductsList)
            {
                eachproduct.Click();
                Thread.Sleep(500);               
                CloseSpinneronDiv();
                CloseSpinneronPage();
            }
            CloseSpinneronDiv();
            CloseSpinneronPage();
            WaitForElement(SelectButton);
            SelectButton.Click();
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            WaitForElement(ScopeDropdown);
            ScopeDropdown.Click();
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            WaitForElement(ScopeDropdownInputPlots);
            ScopeDropdownInputPlots.Click();
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            WaitForElement(PlotsLabel);
            PlotsLabel.Click();           
            WaitForElements(PlotsList);
            foreach (var eachplot in PlotsList)
            {
                eachplot.Click();
                Thread.Sleep(500);
                CloseSpinneronDiv();
                CloseSpinneronPage();
            }
            WaitForElement(SelectButton);
            SelectButton.Click();
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            WaitForElement(SaveButton);
            SaveButton.Click();
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            WaitForElements(ConditionsTableList);            
            int actualOriginalRows = numberOfOriginalRows + 1;
            Assert.AreEqual(ConditionsTableList.Count, actualOriginalRows);
            Assert.IsTrue(ConditionsList.Any(x => x.Text == PlotCondition));
            Assert.IsTrue(ConditionsList.Count(x => x.Text == PlotCondition) >= 1);

            //Assert.AreEqual(ConditionsTableList.Count, numberOfOriginalRows+1);
            //foreach (var eachcondition in ConditionsList)
            //{
            //    var addedPlotConditionText = eachcondition.Text.Contains(PlotCondition);
            //    if (addedPlotConditionText)
            //    {
            //        Assert.IsTrue(addedPlotConditionText);
            //    }
            //    else
            //    {
            //        break;
            //    }
            //}
            //Console.WriteLine("Site Condition Is Added to the List");

        }

        public void VerifyConditionsList()
        {
            Assert.IsTrue(ConditionsRowsList.Count >= 3);
        }

        public void ClosingCondition()
        {
            //Thread.Sleep(500);
            WaitForElements(ManualConditionsList);
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            int actualListCount = ManualConditionsList.Count();
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            for (int i = 0; i < actualListCount; i++)
            {
                Thread.Sleep(500);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                WaitForElement(ManualConditionsList[0]);
                var conditionItem = ManualConditionsList[0];
                int numberOfOriginalRows = ManualConditionsList.Count;
                WaitForElements(ManualConditionsList);
                WaitForElement(conditionItem);
                WaitForElementToClick(conditionItem);
                Thread.Sleep(500);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                conditionItem.Click();               
                WaitForElement(ConditionCloseButton);
                WaitForElementToClick(ConditionCloneButton);                
                CloseSpinneronDiv();
                CloseSpinneronPage();
                ConditionCloseButton.Click();
                CloseSpinneronDiv();
                CloseSpinneronPage();
                WaitForElement(CloseConditionDiv);
                //Thread.Sleep(500);
                Assert.IsTrue(CloseConditionDiv.Displayed);
                WaitForElement(ClosingComments);
                //Thread.Sleep(500);
                ClosingComments.SendKeys("Closing Conditions");
                WaitForElement(RelatedFiles);
                //Thread.Sleep(500);
                RelatedFiles.Click();
                PageUtils.UploadNewFile();
                //Thread.Sleep(500);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                Thread.Sleep(500);
                WaitForElement(SaveButton);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                Thread.Sleep(500);
                WaitForElementToClick(SaveButton);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                Thread.Sleep(500);
                SaveButton.Click();
                CloseSpinneronDiv();
                CloseSpinneronPage();
                Thread.Sleep(500);
                CloseCrispDiv(2);  
                //Thread.Sleep(500);
                int actualOriginalRows = numberOfOriginalRows - 1;
                //Thread.Sleep(1000);
               // Assert.AreEqual(ManualConditionsList.Count, actualOriginalRows);
                //Thread.Sleep(500);
                CloseSpinneronDiv();
                CloseSpinneronPage();               
                Thread.Sleep(500);
            }
        }
        //Clone Manual Condition 
        public void CloneManualConditions()
        {
            Thread.Sleep(500);
            WaitForElements(ManualConditionsList);
            int numberOfOriginalRows = ManualConditionsList.Count();
            var conditionItem = ManualConditionsList[0];
            WaitForElement(conditionItem);
            conditionItem.Click();
            //Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(500);
            var getConditionTextInput = ConditionName.Text;
            //Thread.Sleep(500);
            WaitForElement(ConditionCloneButton);
            ConditionCloneButton.Click();
            //Thread.Sleep(500);
            WaitForElement(NewConditionDiv);
            //Thread.Sleep(500);
            Assert.IsTrue(NewConditionDiv.Displayed);
            WaitForElement(ConditionsTextinput);
            WaitForElement(SaveButton);
            WaitForElementToClick(SaveButton);
            SaveButton.Click();
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            int actualOriginalRows = numberOfOriginalRows + 1;
            Thread.Sleep(500);
            Assert.AreEqual(ManualConditionsList.Count, actualOriginalRows);
            //Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(500);
            Assert.IsTrue(ConditionsList.Any(x => x.Text == getConditionTextInput));
            Assert.IsTrue(ConditionsList.Count(x => x.Text == getConditionTextInput) >= 2);
            //Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(500);

        }
        //Clone Automatic Condition
        public void CloneAutomaticConditions()
        {
            //Thread.Sleep(500);
            WaitForElements(AutomaticConditionsList);
            int numberOfOriginalRows = ManualConditionsList.Count();
            int numberofAutomaticRows = AutomaticConditionsList.Count();
            var conditionItem = AutomaticConditionsList[0];
            WaitForElement(conditionItem);
            conditionItem.Click();
            //Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(500);

            var getConditionTextInput = ConditionName.Text;
            //Thread.Sleep(500);
            WaitForElement(ConditionCloneButton);
            ConditionCloneButton.Click();
            //Thread.Sleep(500);
            WaitForElement(NewConditionDiv);
            //Thread.Sleep(1000);
            Assert.IsTrue(NewConditionDiv.Displayed);
            WaitForElement(ConditionsTextinput);   
           if(ProcessInput.Count>0)
            {
                //Thread.Sleep(500);
                WaitForElement(ProcessesLabel);
                //Thread.Sleep(500);
                ProcessesLabel.Click();
                //Thread.Sleep(1000);
                WaitForElements(ProcessList);
                if (ProcessList.Count > 0)
                {
                    foreach (var eachprocess in ProcessList)
                    {
                        WaitForElement(eachprocess);
                        eachprocess.Click();
                    }
                }
                WaitForElement(SelectButton);
                SelectButton.Click();
                //Thread.Sleep(1000);
            }
            //Thread.Sleep(1000);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            WaitForElement(SaveButton);
            CloseSpinneronDiv();
            //Thread.Sleep(1000);
            SaveButton.Click();
            //Thread.Sleep(1000);
            CloseSpinneronPage();
            CloseSpinneronDiv();
            //Thread.Sleep(1000);
            int actualOriginalRows = numberOfOriginalRows + 1;
            //Thread.Sleep(1000);
            CloseSpinneronPage();
            CloseSpinneronDiv();
            //Thread.Sleep(1000);
            Assert.AreEqual(ManualConditionsList.Count, actualOriginalRows);
            Assert.IsTrue(numberofAutomaticRows == AutomaticConditionsList.Count);
            Assert.IsTrue(ConditionsList.Any(x => x.Text == getConditionTextInput));
            Assert.IsTrue(ConditionsList.Count(x => x.Text == getConditionTextInput) == 2);

        }
        //Edit Manual Condition
        public void EditCondition()
        {
            string EditCondition = "Edit Condition Test";
            //Thread.Sleep(500);
            int numberOfOriginalRows = ManualConditionsList.Count();
            WaitForElements(ManualConditionsList);
            var conditionItem = ManualConditionsList[0];
            WaitForElement(conditionItem);
            conditionItem.Click();
            //Thread.Sleep(500);
            WaitForElement(ConditionEditButton);
            ConditionEditButton.Click();
            //Thread.Sleep(500);
            WaitForElement(EditConditionDiv);
            Assert.IsTrue(EditConditionDiv.Displayed);
            WaitForElement(ConditionsTextinput);
            ConditionsTextinput.Click();
            ConditionsTextinput.Clear();
            ConditionsTextinput.SendKeys(EditCondition);
            WaitForElement(AmendmentCommentsLabel);
            AmendmentCommentsLabel.Click();
            WaitForElement(AmendmentCommentsInput);
            AmendmentCommentsInput.SendKeys("Edit Codnitons");
            WaitForElement(SaveButton);
            WaitForElementToClick(SaveButton);
            SaveButton.Click();
            //Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(500);
            int actualOriginalRows = numberOfOriginalRows;
            //Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(500);
            Assert.IsTrue(ManualConditionsList.Count == actualOriginalRows);
            //Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(500);
            //WaitForElement(conditionItem);
            //conditionItem.Click();
            
            //WaitForElement(LastAmendmentsInput);
            //string actualLastAmendmentText = LastAmendmentsInput.Text;
            // Assert.AreSame(actualLastAmendmentText, "Edit Codnitons");
            Assert.IsTrue(ConditionsList.Any(x => x.Text == EditCondition));
            Assert.IsTrue(ConditionsList.Count(x => x.Text == EditCondition) == 1);
        }
    }
}
