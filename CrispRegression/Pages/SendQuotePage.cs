﻿using OpenQA.Selenium;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.PageObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using NUnit.Framework;
using CrispAutomation.Features;

namespace CrispAutomation.Pages
{
    public class SendQuotePage : Support.Pages
    {
        public IWebDriver wdriver;

        public SendQuotePage(ISearchContext driver)
        {
            PageFactory.InitElements(driver, this);
            wdriver = (RemoteWebDriver)driver;
        }
        //Sending Quote Elements 

        [FindsBy(How = How.XPath, Using = "//crisp-dialog//div[contains(@class,'modal-fixed-footer modal-full-bleed modal-wide modal-tall modal-has-header')]//crisp-header[@class='au-target']")]
        public IWebElement SendQuoteWizard;

        [FindsBy(How = How.XPath, Using = "//div/crisp-dialog//div//crisp-header//nav//div[text()='This is a non-standard quote']")]
        public IList<IWebElement> InsuranceAgreementDiv;

        [FindsBy(How = How.XPath, Using = "//button[@class='au-target waves-effect waves-light btn'][text()='Ok']")]
        public IWebElement OkButton;        

        [FindsBy(How = How.XPath, Using = "//crisp-dialog//div[@class='au-target modal dialog brand pg modal-fixed-footer modal-full-bleed modal-wide modal-tall modal-has-header']//crisp-header[@class='au-target']")]
        public IWebElement PGSendQuoteWizard;

        [FindsBy(How = How.XPath, Using = "//crisp-dialog//div[@class='au-target modal dialog brand labc modal-fixed-footer modal-full-bleed modal-wide modal-tall modal-has-header']//crisp-header[@class='au-target']")]
        public IWebElement LabcSendQuoteWizard;

        [FindsBy(How = How.XPath, Using = "//crisp-wizard-step[@valid.bind='rating.valid'][@class='au-target active']/rating[@class='custom-element au-target']")]
        public IList<IWebElement> RatingEle;

        [FindsBy(How = How.XPath, Using = "//crisp-card-content[@class='au-target']//crisp-input-number[@label='Builders experience in years']//div//label")]
        public IList<IWebElement> BuildersExperienceLabel;

        [FindsBy(How = How.XPath, Using = "//crisp-card-content[@class='au-target']//crisp-input-number[@label='Builders experience in years']//div//input")]
        public IWebElement BuildersExperienceInput;

        [FindsBy(How = How.XPath, Using = "//crisp-wizard-step[@valid.bind='rating.valid'][@class='au-target active']//rating[@class='custom-element au-target']//div[@class='card-title']//span[text()='Proposed Site Rating']")]
        public IList<IWebElement> ProposedSiteRating;

        [FindsBy(How = How.XPath, Using = "//crisp-wizard-step[@valid.bind='rating.valid'][@class='au-target active']//rating[@class='custom-element au-target']//div//dl/dt[text()='Technical Rating']/following-sibling::dd[1][text()='Reject']")]
        public IList<IWebElement> RejectedRatings;
        

        [FindsBy(How = How.XPath, Using = "//crisp-wizard-step[@step-title='Securities'][@class='au-target active']/securities[@class='custom-element au-target']")]
        public IList<IWebElement> SecuritiesEle;

        [FindsBy(How = How.XPath, Using = "//crisp-wizard-step[@step-title='Conditions'][@class='au-target active']//conditions//condition-list//crisp-table[@class='custom-element au-target custom-element']")]
        public IList<IWebElement> ConditionsEle;

        [FindsBy(How = How.XPath, Using = "//crisp-wizard-step[@step-title='Special terms'][@class='au-target active']//special-terms")]
        public IList<IWebElement> specialTermsEle;

        [FindsBy(How = How.XPath, Using = "//crisp-wizard-step[@step-title='Fees'][@class='au-target active']//fees[@class='custom-element au-target']")]
        public IList<IWebElement> FeesEle;

        [FindsBy(How = How.XPath, Using = "//crisp-wizard-step[@step-title='File Review'][@class='au-target active']//file-review[@class='custom-element au-target']")]
        public IList<IWebElement> FileReviewEle;

        [FindsBy(How = How.XPath, Using = "//crisp-wizard-step[@step-title='Endorsements'][@class='au-target active']//endorsements[@class='custom-element au-target']")]
        public IList<IWebElement> EndorsementEle;

        [FindsBy(How = How.XPath, Using = "//crisp-wizard-step/endorsements/div/div/crisp-list/div/div/span[text()='Available endorsements']")]
        public IWebElement AvailableEndorsements;

        [FindsBy(How = How.XPath, Using = "//crisp-wizard-step/endorsements/div/div/crisp-list/div/div/span[text()='Selected endorsements']")]
        public IWebElement SelectedEndorsements;

        [FindsBy(How = How.XPath, Using = "//crisp-wizard-step[@step-title='Confirm'][@class='au-target active']//confirm[@class='custom-element au-target']//crisp-card[@class='au-target']")]
        public IList<IWebElement> ConfirmPageEle;

        [FindsBy(How = How.XPath, Using = "//crisp-wizard-step[@step-title='Confirm'][@class='au-target active']//confirm[@class='custom-element au-target']//crisp-card//crisp-card-header//div//span[text()='Confirm Quote Acceptance Details']")]
        public IList<IWebElement> ConfirmExpiry;

        [FindsBy(How = How.XPath, Using = "//crisp-input-currency[@label='Limit of Indemnity']//div//label")]
        public IList<IWebElement> LimitOfIndemnity;

        [FindsBy(How = How.XPath, Using = "//crisp-input-currency[@label='Limit of Indemnity']//div//input")]
        public IWebElement LimitOfIndemnityInput;

        [FindsBy(How = How.XPath, Using = "//crisp-input-radio/div/ul/li/label[contains(text(),'Override Conditions/Blocks & Send Quote')]")]
        public IList<IWebElement> OverrideConditionsRadioButton;

        [FindsBy(How = How.XPath, Using = "//crisp-input-radio/div/ul/li/label[contains(text(),'Chase Outstanding Information')]")]
        public IList<IWebElement> ChaseOutstandingInformation;

        [FindsBy(How = How.XPath, Using = "//crisp-input-text[@label='Comments']/div/label")]
        public IWebElement CommentsLabel;

        [FindsBy(How = How.XPath, Using = "//crisp-input-text[@label='Comments']/div/input")]
        public IWebElement CommentsInput;

        [FindsBy(How = How.XPath, Using = "//correspondence-email[@class='au-target']//crisp-input-html[@class='au-target'][contains(@value.bind,'correspondenceTab.body & validate')]")]
        public IList<IWebElement> Correspondence;        

        [FindsBy(How = How.XPath, Using = "//crisp-input-html[@class='au-target']//div//table//tbody//tr//td//h3//a[@href='https://www.labcwarranty.co.uk/']//img")]
        public IList<IWebElement> LABCGenerateQuote;

        [FindsBy(How = How.XPath, Using = "//crisp-wizard/crisp-wizard-step[@class='au-target active']//rating//crisp-card-content//crisp-validation-message")]
        public IList<IWebElement> RatingRequired;
        
        [FindsBy(How = How.CssSelector, Using = "crisp-picker[label='Proposed Technical Rating'] >div>div>input[type=text].select-dropdown")]
        public IWebElement TechgRatingDropdown;

        [FindsBy(How = How.XPath, Using = "//ul[@class='dropdown-content select-dropdown active']/li[3]")]
        public IWebElement TechgRatingDropdownInput;

        [FindsBy(How = How.CssSelector, Using = "crisp-input-textarea[label='Comments']>div>label")]
        public IWebElement RatingComment;

        [FindsBy(How = How.CssSelector, Using = "crisp-input-textarea[label='Comments']>div>textarea")]
        public IWebElement RatingCommentText;

        [FindsBy(How = How.XPath, Using = "//span[3]/crisp-validation-message/span[text()='Developer Rating is required']")]
        public IWebElement DeveloperRatingRequired;

        [FindsBy(How = How.XPath, Using = "//rating//div//div//crisp-card[@class='au-target']//crisp-card-actions[@class='au-target']//div//crisp-button//span//button[text()='Create Rating']")]
        public IList<IWebElement> DevorBiulderCreateRatingButton;

        [FindsBy(How = How.XPath, Using = "//rating//div//div//crisp-card[@class='au-target']//crisp-card-actions[@class='au-target']//div//crisp-button//span//button[text()='Edit Rating']")]
        public IList<IWebElement> DevorBiulderEditRatingButton;

        [FindsBy(How = How.XPath, Using = "//crisp-dialog//div[@class='au-target modal dialog brand default modal-fixed-footer modal-has-header modal-full-bleed modal-tall modal-wide']//crisp-header")]
        public IWebElement ContactDiv;

        [FindsBy(How = How.XPath, Using = "//ai-dialog-container[2]//crisp-stepper-step//div[@class='highlight-colour title au-target']")]
        public IList<IWebElement> RatingStep;        

        [FindsBy(How = How.XPath, Using = "//ai-dialog-container[2]//crisp-stepper-step[1]//div[@class='highlight-colour title au-target']")]
        public IWebElement DetailsPage;

        [FindsBy(How = How.XPath, Using = "//ai-dialog-container[2]//crisp-stepper-step[2]//div[@class='highlight-colour title au-target']")]
        public IWebElement WarrentyProviderPage;

        [FindsBy(How = How.XPath, Using = "//ai-dialog-container[2]//crisp-stepper-step[3]//div[@class='highlight-colour title au-target']")]
        public IWebElement DirectorsPage;

        [FindsBy(How = How.XPath, Using = "//ai-dialog-container[2]//crisp-stepper-step[4]//div[@class='highlight-colour title au-target']")]
        public IWebElement DevelopmentsPage;

        [FindsBy(How = How.XPath, Using = "//ai-dialog-container[2]//crisp-stepper-step[5]//div[@class='highlight-colour title au-target']")]
        public IWebElement ProspectiveBusinessPage;

        [FindsBy(How = How.XPath, Using = "//ai-dialog-container[2]//crisp-stepper-step[6]//div[@class='highlight-colour title au-target']")]
        public IWebElement ClaimHistoryPage;

        [FindsBy(How = How.XPath, Using = "//ai-dialog-container[2]//crisp-stepper-step[7]//div[@class='highlight-colour title au-target']")]
        public IWebElement GroupOptionPage;

        [FindsBy(How = How.XPath, Using = "//ai-dialog-container[2]//crisp-stepper-step[8]//div[@class='highlight-colour title au-target']")]
        public IWebElement ScoresPage;

        [FindsBy(How = How.XPath, Using = "//crisp-footer[@class='au-target']//crisp-button[@click.delegate='save()'][@class='au-target']//button[text()='Save']")]
        public IWebElement ConfirmRatingButton;

        [FindsBy(How = How.CssSelector, Using = "crisp-picker[label='Principal Occupation'] input[type='text']")]
        public IWebElement OccupationDrodpDown;

        [FindsBy(How = How.XPath, Using = "//ul[@class='dropdown-content select-dropdown active']/li[10]/span")]
        public IWebElement OccupationDrodpDownInput;

        [FindsBy(How = How.CssSelector, Using = "crisp-picker[label='Company Segment'] input[type='text']")]
        public IList<IWebElement> CompanySegmentDrodpDown;

        [FindsBy(How = How.XPath, Using = "//ul[@class='dropdown-content select-dropdown active']//li//span[text()='National']")]
        public IWebElement CompanySegmentDrodpDownInput;

        [FindsBy(How = How.CssSelector, Using = "crisp-input-textarea[label='Technical Rating Comment']>div>label")]
        public IWebElement TechRatingComment;

        [FindsBy(How = How.XPath, Using = "//crisp-footer[@class='au-target']//crisp-button[@class='au-target'][@click.delegate='wizard.goForward()']//button[text()='Next']")]
        public IWebElement RatingNextButton;       

        [FindsBy(How = How.XPath, Using = "//crisp-footer[@class='au-target']//crisp-footer-button[@click.delegate='goForward()'][@class='au-target']//button[text()='Next'][@class='au-target waves-effect waves-light btn-flat']")]
        public IWebElement SendQuoteNextButton;

        //Additional Elememnts 
        
        [FindsBy(How = How.CssSelector, Using = "crisp-input-textarea[label='Technical Rating Comment']>div>textarea")]
        public IWebElement TechRatingCommentText;

        [FindsBy(How = How.XPath, Using = "//crisp-wizard//crisp-wizard-step[@step-title='Securities'][@class='au-target active']")]
        public IWebElement Securities;

        [FindsBy(How = How.XPath, Using = "//crisp-wizard-step[@step-title='Securities'][@class='au-target active']//securities//crisp-card//crisp-card-content[@class='au-target']//div[@class='card-content'][contains(text(),'No securities required, please proceed.')]")]
        public IWebElement NoSecuritiesRequired;

        [FindsBy(How = How.XPath, Using = "//rating/div[1]/div[2]/crisp-card/crisp-card-content/div/span[3]/crisp-validation-message/span[text()='Builder Rating is required']")]
        public IWebElement BuilderRatingRequired;

        [FindsBy(How = How.XPath, Using = "//crisp-wizard-step[1]/rating/div[1]/div[2]/crisp-card/crisp-card-actions/div/crisp-button/span/button[text()='Create Rating']")]
        public IWebElement BuilderCreateRatingButton;

        [FindsBy(How = How.XPath, Using = "//crisp-header-button[2]/span/button[text()='Add Rating']")]
        public IWebElement AddRatingButton;

        [FindsBy(How = How.XPath, Using = "//crisp-card-actions/div/crisp-button/span/button[text()='Edit Rating']")]
        public IWebElement EditRatingButton;

        [FindsBy(How = How.XPath, Using = "//crisp-card-actions/div/crisp-button/span/button[text()='Create Rating']")]
        public IWebElement CreateRatingButton;

        [FindsBy(How = How.XPath, Using = "//div[@ref='tabContainer']/div/div/crisp-card/crisp-card-content/div/crisp-picker[@label='Proposed Technical Rating']/div/div/input")]
        public IWebElement QuoteTechgRatingDropdown;

        [FindsBy(How = How.XPath, Using = "//crisp-input-textarea/div/span[text()='Site Rating Comments is required']")]
        public IWebElement SiteRatingCommentsRequired;

        [FindsBy(How = How.CssSelector, Using = "crisp-input-number[label='Proposed Financial Rating']>div>input")]
        public IWebElement FinancialRating;      

        [FindsBy(How = How.CssSelector, Using = "crisp-card > crisp-card-actions > div > crisp-button:nth-child(1) > span > button")]
        public IWebElement SaveButton;

        [FindsBy(How = How.XPath, Using = "//crisp-card-header/div/span[text()='Summary']")]
        public IWebElement RatingSummaryDisplayed;         

        [FindsBy(How = How.XPath, Using = "//crisp-footer-button[@class='au-target']/button[text()='Complete']")]
        public IWebElement CompleteButton;

        [FindsBy(How = How.XPath, Using = "//crisp-wizard-step[@step-title='Conditions'][@class='au-target active']//conditions//crisp-table[@class='custom-element au-target custom-element']//span[text()='Conditions']")]
        public IWebElement ConditionsOptions;

        [FindsBy(How = How.XPath, Using = "//crisp-wizard-step[@step-title='File Review'][@class='au-target active']//file-review[@class='custom-element au-target']//crisp-card[@class='au-target']//crisp-card-header[@class='au-target']//div//span[text()='File Review Summary']")]
        public IWebElement FilesReviewSummary;

        [FindsBy(How = How.XPath, Using = "//crisp-input-html[@class='au-target']//div//table//tbody//tr//td//p//a[@href='https://www.premierguarantee.co.uk/']//img")]
        public IList<IWebElement> PGGenerateQuote;

        //Confirm details page      

        [FindsBy(How = How.XPath,Using ="//crisp-wizard-step[@step-title='Conditions'][@class='au-target active']//conditions//condition-list//crisp-table[@class='custom-element au-target custom-element']//div//table[@class='au-target']")]
        public IWebElement Conditionstable;

        [FindsBy(How = How.XPath, Using = "//crisp-wizard-step[@step-title='Conditions'][@class='au-target active']//conditions//condition-list//crisp-table[@class='custom-element au-target custom-element']//div//table[@class='au-target']/tbody/tr[@class='au-target']")]
        public IList<IWebElement> ConditionsRows;    
        
        [FindsBy(How = How.XPath, Using = "//crisp-wizard-step[@step-title.bind='correspondenceTitle'][@class='au-target active']//correspondence-builder[@class='custom-element au-target']//correspondence-embed")]
        public IWebElement GenerateQuotePageEle;

        [FindsBy(How = How.XPath, Using = "//div[@class='modal-content']//crisp-wizard[@active-step.bind='activeStep']//crisp-stepper-step[contains(@class,'current')]//div[@title.bind='description'][contains(@class,'title')]")]
        public IWebElement StepTitle;

        [FindsBy(How = How.XPath, Using = "//div//crisp-header-title[@class='au-target'][1]/div[@class='title']")]
        public IWebElement QuoteRefDetails;
        //Send Quote  Rating Page 
        public void SendQuoteRatingPage()
        {
            if (RatingEle.Count > 0)
            {
                WaitForElements(RatingEle);
                WaitForElement(StepTitle);
                Assert.IsTrue(StepTitle.Text.Contains("BUILDERS EXPERIENCE") || StepTitle.Text.Contains("RATING"), $"Failed to Display {StepTitle.Text}");
                CloseSpinneronDiv();                
                if (BuildersExperienceLabel.Count > 0)
                {                  
                    WaitForElements(BuildersExperienceLabel);
                    BuildersExperienceLabel[0].Click();
                    WaitForElement(BuildersExperienceInput);
                    BuildersExperienceInput.SendKeys("2");                    
                    if (ProposedSiteRating.Count > 0)
                    {
                        AddRatingDetails();
                    }
                }
                else
                {
                    AddRatingDetails();
                }
                if(RejectedRatings.Count>0)
                {
                    EditRating();
                }

                CloseSpinneronDiv();
                CloseSpinneronPage();
                Thread.Sleep(500);
                SendQuoteNextButtonMethod();
                CloseSpinneronDiv();
                CloseSpinneronPage();
            
            }
        }
        // Send Quote Securities Page 
        public void SendQuoteSecuritiesPage()
        {
            if (SecuritiesEle.Count > 0)
            {
                WaitForElement(StepTitle);
                Assert.IsTrue(StepTitle.Text.Contains("SECURITIES"), $"Failed to Display {StepTitle.Text}");
                Assert.IsTrue(SecuritiesEle[0].Displayed);
                Console.WriteLine("security details verified");
                CloseSpinneronDiv();               
                CloseSpinneronDiv();
                Thread.Sleep(500);
                SendQuoteNextButtonMethod();
                
                CloseSpinneronDiv();
                CloseSpinneronPage();
                Thread.Sleep(500);
            }
        }
        //Send Quote Conditions Page 
        public void SendQuoteConditionsPage()
        {
            //Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(500);
            if (ConditionsEle.Count > 0)
            {
                WaitForElement(StepTitle);
                Assert.IsTrue(StepTitle.Text.Contains("CONDITIONS"), $"Failed to Display {StepTitle.Text}");
                //Thread.Sleep(500);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                //Thread.Sleep(500);
                WaitForElements(ConditionsEle);                
                WaitForElement(ConditionsOptions);                
                Assert.IsTrue(ConditionsOptions.Displayed); 
                Assert.IsTrue(ConditionsEle[0].Displayed);                
                WaitForElement(Conditionstable);                
                Assert.IsTrue(Conditionstable.Displayed);
                //Thread.Sleep(500);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                //Thread.Sleep(500);
                var OnlyBCProduct = Statics.ProductName == "";
                if (OnlyBCProduct == false)
                {
                    //Thread.Sleep(500);
                    CloseSpinneronDiv();
                    CloseSpinneronPage();
                  //  //Thread.Sleep(500);
                    int rows = SendQuotePage.ConditionsRows.Count;
                    if (rows > 0)
                    {
                        WaitForElements(SendQuotePage.ConditionsRows);
                        Assert.IsTrue(SendQuotePage.ConditionsRows.Count >= 0);
                        foreach (var eachrow in SendQuotePage.ConditionsRows)
                        {
                            Assert.IsTrue(eachrow.Displayed);
                        }
                    }
                    else if (rows <= 0)
                    {
                      // Assert.Fail($"No Automatic Conditions Are Opened on SendQuote Page,Automatic Condition Count is :  {SendQuotePage.ConditionsRows.Count}");
                    }
                    //Thread.Sleep(1000);
                    CloseSpinneronDiv();
                    CloseSpinneronPage();
                    //Thread.Sleep(1000);
                    if (ManageConditionsPage.ManualConditionsList.Count > 0)
                    {
                        ManageConditionsPage.ClosingCondition();
                        //Thread.Sleep(500);
                        CloseSpinneronDiv();
                        CloseSpinneronPage();
                       // //Thread.Sleep(500);
                    }
                }
                //Thread.Sleep(500);
                CloseSpinneronDiv();
                CloseSpinneronPage();
               // //Thread.Sleep(500);
                SendQuoteNextButtonMethod();
                //Thread.Sleep(500);
                CloseSpinneronDiv();
                CloseSpinneronPage();
               // //Thread.Sleep(500);
            }
        }
        //Send Quote special Terms 
        public void SendQuotespecialTermsPage()
        {
            //Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(500);
            if (specialTermsEle.Count > 0)
            {
                WaitForElement(StepTitle);
                Assert.IsTrue(StepTitle.Text.Contains("SPECIAL TERMS"), $"Failed to Display {StepTitle.Text}");
                WaitForElements(specialTermsEle);
                Assert.IsTrue(specialTermsEle[0].Displayed);
                Console.WriteLine("special terms page is verified");
                
                CloseSpinneronDiv();
                SendQuoteNextButtonMethod();
                
                CloseSpinneronDiv();
            }
        }

        //Send Quote Fees Page
        public void SendQuoteFeesPage()
        {
            if (FeesEle.Count > 0)
            {
                WaitForElement(StepTitle);
                Assert.IsTrue(StepTitle.Text.Contains("FEES"), $"Failed to Display {StepTitle.Text}");
                WaitForElements(FeesEle);
                Assert.IsTrue(FeesEle[0].Displayed);
                Console.WriteLine("Fees details verified");
                
                CloseSpinneronDiv();                
                SendQuoteNextButtonMethod();
                
                CloseSpinneronDiv();
            }
        }

        //Send Quote File Review Page 
        public void SendQuoteFileReviewPage()
        {
            if (FileReviewEle.Count > 0)
            {
                WaitForElement(StepTitle);
                Assert.IsTrue(StepTitle.Text.Contains("FILE REVIEW"), $"Failed to Display {StepTitle.Text}");
                WaitForElements(FileReviewEle);
                Assert.IsTrue(FileReviewEle[0].Displayed);
                
                WaitForElement(FilesReviewSummary);
                
                Assert.IsTrue(FilesReviewSummary.Displayed);
                Console.WriteLine("File review details verified");
                
                CloseSpinneronDiv();
                SendQuoteNextButtonMethod();
                //Thread.Sleep(500);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                //Thread.Sleep(500);
            }
        }

        //Send Quote Endorsement Page 
        public void SendQuoteEndorsementPage()
        {
            if (EndorsementEle.Count > 0)
            {
                WaitForElement(StepTitle);
                Assert.IsTrue(StepTitle.Text.Contains("ENDORSEMENTS"), $"Failed to Display {StepTitle.Text}");
                WaitForElements(EndorsementEle);
                Assert.IsTrue(EndorsementEle[0].Displayed);
                
                WaitForElement(AvailableEndorsements);
                Assert.IsTrue(AvailableEndorsements.Displayed);
                Console.WriteLine("Available Endorsement Displayed");
                
                WaitForElement(SelectedEndorsements);
                Assert.IsTrue(SelectedEndorsements.Displayed);
                Console.WriteLine("Selected Endorsement Displayed");
                               
                CloseSpinneronDiv();
                SendQuoteNextButtonMethod();
                //Thread.Sleep(500);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                //Thread.Sleep(500);
            }

        }
        //Send Quote Confirm Details Page
        public void SendQuoteConfirmDetailsPage()
        {
            if (ConfirmPageEle.Count > 0)
            {
                WaitForElements(ConfirmPageEle);
                if (ConfirmExpiry.Count > 0)
                {
                    WaitForElement(StepTitle);
                    Assert.IsTrue(StepTitle.Text.Contains("CONFIRM"), $"Failed to Display {StepTitle.Text}");
                    WaitForElements(ConfirmExpiry);                    
                    Assert.IsTrue(ConfirmExpiry[0].Displayed);
                    Console.WriteLine("confirm details verified");
                    //Thread.Sleep(1000);
                    CloseSpinneronDiv();
                    CloseSpinneronPage();
                    //Thread.Sleep(1000);
                }
                
                if (OverrideConditionsRadioButton.Count > 0)
                {
                    WaitForElement(OverrideConditionsRadioButton[0]);
                    OverrideConditionsRadioButton[0].Click();
                    WaitForElement(CommentsLabel);
                    CommentsLabel.Click();
                    WaitForElement(CommentsInput);
                    CommentsInput.SendKeys("ConditionsOverrideConfirmation");
                                        
                    CloseSpinneronDiv();
                }
                
                if (LimitOfIndemnity.Count>0)
                {
                    WaitForElements(LimitOfIndemnity);
                    LimitOfIndemnity[0].Click();
                    WaitForElement(LimitOfIndemnityInput);
                    LimitOfIndemnityInput.Click();
                    LimitOfIndemnityInput.Clear();
                    LimitOfIndemnityInput.SendKeys("2000");
                }
                //Thread.Sleep(1000);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                //Thread.Sleep(1000);
                SendQuoteNextButtonMethod();
                //Thread.Sleep(1000);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                //Thread.Sleep(1000);
            }
        }
        //Send Quote Correspondence Page 
        public void SendQuoteCorrespondencePage()
        {
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);               
            WaitForElement(StepTitle);
            var title = StepTitle.Text;
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            if (title.Contains("GENERATE QUOTE") || title.Contains("GENERATE CHASE"))
            {
                Assert.IsTrue(StepTitle.Text.Contains("GENERATE QUOTE") || StepTitle.Text.Contains("GENERATE CHASE"), $"Failed to Display {StepTitle.Text} Page");
            }else if(title.Contains("Correspondence"))
            {
                Assert.IsTrue(StepTitle.Text.Contains("Correspondence"), $"Failed to Display {StepTitle.Text}Page");

            }
            WaitForElements(Correspondence);
            if (LABCGenerateQuote.Count>0)
            {                
                CloseSpinneronDiv();                
                WaitForElement(LABCGenerateQuote[0]);                
                Assert.IsTrue(LABCGenerateQuote[0].Displayed,$"Failed to Display {StepTitle.Text} Correnspondence");
                Console.WriteLine("LABC Generate  Quote details verified");
            }
            if (PGGenerateQuote.Count > 0)
            {
                
                CloseSpinneronDiv();
                
                WaitForElement(PGGenerateQuote[0]);
                
                Assert.IsTrue(PGGenerateQuote[0].Displayed,$"Failed to Display {StepTitle.Text} Correnspondence");
                Console.WriteLine("LABC Generate  Quote details verified");
            }

            //Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(500);
            WaitForElement(CompleteButton);            
            SendQuotePage.CompleteButton.Click();
            CloseSpinneronDiv();
            
            // SendQuoteDetailsOnTheDashboard();
            //Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(500);

        }


        //Add Rating 
        public void AddRatingDetails()
        {
            var Rating1 = "";
            var Rating2 = "";
            if (RatingRequired.Count > 1)
            {
                Rating1 = RatingRequired[0].Text;
                Rating2 = RatingRequired[1].Text;
            }
            else if(RatingRequired.Count > 0 && RatingRequired.Count < 2)
            {
                Rating1 = RatingRequired[0].Text;                
            }  
            if (Rating1.Contains("Rating is required") && Rating2.Contains("Rating is required"))
            {
                WaitForElements(DevorBiulderCreateRatingButton);
                if(DevorBiulderCreateRatingButton.Count>0)
                {
                    int CreateratingButtons = DevorBiulderCreateRatingButton.Count;
                    for (int i = 0; i < CreateratingButtons; i++)
                    {
                        if (DevorBiulderCreateRatingButton.Count > 0)
                        {
                            AddRating();
                        }
                    }
                }
            }
            else if (Rating1.Contains("Rating is required") || Rating2.Contains("Rating is required"))
            {
                AddRating();
            }else if(RejectedRatings.Count > 0)
            {
                AddRating();
            }
            CloseSpinneronDiv();
            CloseSpinneronPage();
            WaitForElement(RatingCommentText);
            var RatingCommentTextValue = RatingCommentText.GetAttribute("GoodRate");
            if (string.IsNullOrEmpty(RatingCommentTextValue))
            {                
                WaitForElement(TechgRatingDropdown);
                TechgRatingDropdown.Click();
                Thread.Sleep(500);
                WaitForElement(TechgRatingDropdownInput);
                TechgRatingDropdownInput.Click();                
                WaitForElement(RatingComment);
                RatingComment.Click();
                WaitForElement(RatingCommentText);
                RatingCommentText.SendKeys("GoodRate");                
            }
        }     
       
        public void AddRating()
        {
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            //WaitForElements(DevorBiulderCreateRatingButton);
            if (DevorBiulderCreateRatingButton.Count > 0)
            {
                DevorBiulderCreateRatingButton[0].Click();
                WaitForElement(ContactDiv);
                Assert.IsTrue(ContactDiv.Displayed);
                List<string> RatingPage = RatingStep.Select(i => i.Text).ToList();    
                if (RatingPage.Contains("DETAILS"))
                {
                    WaitForElement(OccupationDrodpDown);
                    OccupationDrodpDown.Click();
                    WaitForElement(OccupationDrodpDownInput);
                    OccupationDrodpDownInput.Click();
                    if (CompanySegmentDrodpDown.Count > 0)
                    {
                        WaitForElement(CompanySegmentDrodpDown[0]);
                        CompanySegmentDrodpDown[0].Click();
                        WaitForElement(CompanySegmentDrodpDownInput);
                        CompanySegmentDrodpDownInput.Click();
                    }
                    WaitForElement(RatingNextButton);
                    RatingNextButton.Click();
                }


                if (RatingPage.Contains("WARRANTY PROVIDERS"))
                {
                    WaitForElement(RatingNextButton);
                    RatingNextButton.Click();
                }
                if (RatingPage.Contains("DIRECTORS"))
                {
                    WaitForElement(RatingNextButton);
                    RatingNextButton.Click();
                }
                if (RatingPage.Contains("DEVELOPMENTS"))
                {
                    WaitForElement(RatingNextButton);
                    RatingNextButton.Click();
                }
                if (RatingPage.Contains("PROSPECTIVE BUSINESS"))
                {
                    WaitForElement(RatingNextButton);
                    RatingNextButton.Click();
                }
                if (RatingPage.Contains("CLAIMS HISTORY"))
                {
                    WaitForElement(RatingNextButton);
                    RatingNextButton.Click();
                }
                if (RatingPage.Contains("GROUP"))
                {
                    WaitForElement(RatingNextButton);
                    RatingNextButton.Click();
                }
                if (RatingPage.Contains("SCORES"))
                {
                    AddRatingToCompanyPage.ScoresDetails();
                }
                WaitForElement(ConfirmRatingButton);
                ConfirmRatingButton.Click();
                
                CloseSpinneronDiv();
                CloseSpinneronPage();
                //Thread.Sleep(500);
            }
        }
        public void EditRating()
        {
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            //WaitForElements(DevorBiulderCreateRatingButton);
            if (DevorBiulderEditRatingButton.Count > 0)
            {
                DevorBiulderEditRatingButton[0].Click();
                WaitForElement(ContactDiv);
                Assert.IsTrue(ContactDiv.Displayed);
                List<string> RatingPage = RatingStep.Select(i => i.Text).ToList();
                if (RatingPage.Contains("DETAILS"))
                {
                    WaitForElement(OccupationDrodpDown);
                    OccupationDrodpDown.Click();
                    WaitForElement(OccupationDrodpDownInput);
                    OccupationDrodpDownInput.Click();
                    if (CompanySegmentDrodpDown.Count > 0)
                    {
                        WaitForElement(CompanySegmentDrodpDown[0]);
                        CompanySegmentDrodpDown[0].Click();
                        WaitForElement(CompanySegmentDrodpDownInput);
                        CompanySegmentDrodpDownInput.Click();
                    }
                    WaitForElement(AddRatingToCompanyPage.CompanyName);
                    AddRatingToCompanyPage.CompanyName.Click();
                    WaitForElement(AddRatingToCompanyPage.CompanyNameInput);
                    AddRatingToCompanyPage.CompanyNameInput.SendKeys("Test Company");
                    AddRatingToCompanyPage.DateField.Click();
                    WaitForElement(AddRatingToCompanyPage.DateFieldInput);
                    AddRatingToCompanyPage.DateFieldInput.Click();    
                    WaitForElement(RatingNextButton);
                    RatingNextButton.Click();
                }
                if (RatingPage.Contains("WARRANTY PROVIDERS"))
                {
                    WaitForElement(RatingNextButton);
                    RatingNextButton.Click();
                }
                if (RatingPage.Contains("DIRECTORS"))
                {
                    WaitForElement(RatingNextButton);
                    RatingNextButton.Click();
                }
                if (RatingPage.Contains("DEVELOPMENTS"))
                {
                    WaitForElement(RatingNextButton);
                    RatingNextButton.Click();
                }
                if (RatingPage.Contains("PROSPECTIVE BUSINESS"))
                {
                    WaitForElement(RatingNextButton);
                    RatingNextButton.Click();
                }
                if (RatingPage.Contains("CLAIMS HISTORY"))
                {
                    WaitForElement(RatingNextButton);
                    RatingNextButton.Click();
                }
                if (RatingPage.Contains("GROUP"))
                {
                    WaitForElement(RatingNextButton);
                    RatingNextButton.Click();
                }
                if (RatingPage.Contains("SCORES"))
                {
                    AddRatingToCompanyPage.ScoresDetails();
                }   
                CloseSpinneronDiv();
                CloseSpinneronPage();
                //Thread.Sleep(500);
                WaitForElement(ConfirmRatingButton);
                ConfirmRatingButton.Click();
                CloseSpinneronDiv();
                CloseSpinneronPage();
                Thread.Sleep(500);
            }
        }
        public void SendQuoteNextButtonMethod()
        {            
            WaitForElement(SendQuotePage.SendQuoteNextButton);
            WaitForElementToClick(SendQuotePage.SendQuoteNextButton);
            SendQuotePage.SendQuoteNextButton.Click();            
        }

        public void SendQuoteDetailsOnTheDashboard()
        {
            try
            {
                CloseSpinneronDiv();
                CloseSpinneronPage();
                WaitForElement(Dashboardpage.QuoteApplication);
                Assert.IsTrue(Dashboardpage.QuoteApplication.Displayed);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                var SendQuoteStatus = Dashboardpage.QuoteApplicationStatus.Text;
                CloseSpinneronDiv();
                CloseSpinneronPage();
                if (SendQuoteStatus.Contains("Quoting") == true)
                {
                    //Thread.Sleep(2000);
                    CloseSpinneronDiv();
                    CloseSpinneronPage();
                    //Thread.Sleep(2000);
                    WaitForElement(QuoteRefDetails);
                    var quoteRefNumber = QuoteRefDetails.Text;
                    String quoteRef = quoteRefNumber.Replace(" - Quoting", "");
                    Statics.QuoteNumber = quoteRef;
                    WaitForElement(Dashboardpage.ReloadButton);
                    Dashboardpage.ReloadButton.Click();
                    CloseSpinneronDiv();
                    CloseSpinneronPage();       
             
                }
                //Thread.Sleep(1000);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                //Thread.Sleep(1000);
                WaitForElement(Dashboardpage.QuoteApplicationStatus);
                var QuotedStatus = Dashboardpage.QuoteApplicationStatus.Text;
                //Thread.Sleep(1000);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                //Thread.Sleep(1000);
                if (QuotedStatus.Contains("Quoted") == true)
                {
                    Assert.IsTrue(QuotedStatus.Contains("Quoted"));
                    WaitForElement(QuoteRefDetails);
                    var quoteRefNumber = QuoteRefDetails.Text;
                    String quoteRef = quoteRefNumber.Replace(" - Quoted", "");
                    Statics.QuoteNumber = quoteRef;
                }
              
               
            }
            catch(Exception ex)
            {
                throw new Exception("Failed to update the quoting state to quoted state , Please check RabbitMQ Queues", ex);
            }
        }
        //Main Method for Sending  Quote 
        public void SendquoteMainMethod()
        {
            //Thread.Sleep(1000);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(1000);
            WaitForElement(Dashboardpage.QuoteDecisionButton);            
            Dashboardpage.QuoteDecisionButton.Click();
            CloseSpinneronDiv();
            CloseSpinneronPage();
            WaitForElement(Dashboardpage.SendQuoteButton);
            WaitForElementToClick(Dashboardpage.SendQuoteButton);
            Dashboardpage.SendQuoteButton.Click();
            CloseSpinneronDiv();
            CloseSpinneronPage();            
            WaitForElement(SendQuoteWizard);
            Assert.IsTrue(SendQuoteWizard.Displayed);            
            CloseSpinneronDiv();
            if (SendQuotePage.InsuranceAgreementDiv.Count > 0)
            {
                SendQuotePage.OkButton.Click();
            }            
            CloseSpinneronDiv();
            SendQuoteRatingPage();
            SendQuoteSecuritiesPage();
            SendQuoteConditionsPage();
            SendQuotespecialTermsPage();
            SendQuoteFeesPage();
            SendQuoteFileReviewPage();
            SendQuoteEndorsementPage();
            SendQuoteConfirmDetailsPage();
            SendQuoteCorrespondencePage();             
            CloseSpinneronDiv();            
            SendQuoteDetailsOnTheDashboard();
        }
    }
}
