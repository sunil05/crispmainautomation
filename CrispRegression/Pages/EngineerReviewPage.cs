﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace CrispAutomation.Pages
{
     public class EngineerReviewPage : Support.Pages
    {
        public IWebDriver wdriver;
        public EngineerReviewPage(ISearchContext driver)
        {
            PageFactory.InitElements(driver, this);
        }
        //Engineer Review Page Elements 
        [FindsBy(How = How.XPath, Using = "//div//a[@class='info-box bg-grey hover-expand-effect']//div[text()='Engineer review']")]
        public IWebElement EngineerReviewOption;

        [FindsBy(How = How.XPath, Using = "//div//a[@class='info-box bg-grey hover-expand-effect']//div[@class='content']")]
        public IWebElement ERStatus;

        [FindsBy(How = How.XPath, Using = "//div//a[text()='Edit']")]
        public IWebElement EditButton;
        
        [FindsBy(How = How.XPath, Using = "//div//button[text()='Next']")]
        public IWebElement NextButton;

        [FindsBy(How = How.XPath, Using = "//div[@class='form-control']//label[contains(@for,'AcceptableRisk_true')]")]
        public IWebElement AcceptableRiskRadioButton;

        [FindsBy(How = How.XPath, Using = "//div[@id='page2']//div[@class='form-contents']//button[@type='submit'][contains(text(),'Save')]")]
        public IWebElement AssessmentSaveButton;

        [FindsBy(How = How.XPath, Using = "//div[@class='form-contents']//button[@type='submit'][contains(text(),'Submit')]")]
        public IWebElement SubmitButton;

        [FindsBy(How = How.XPath, Using = "//section[@class='content']//form[@method='post']//button[@type='submit'][contains(text(),'Send')]")]
        public IWebElement SendButton;

        [FindsBy(How = How.XPath, Using = "//h2[text()='Engineering Review Assessment']")]
        public IWebElement EngineerReviewMainPage;
        //open ER 
        public void OpenER()
        {
            Scrollup();
            CloseSpinnerOnSurveyorDiv();
            CloseSpinnerOnSurveyorPage();
            Thread.Sleep(500);
            WaitForElementonSurveyor(EngineerReviewOption);
            CloseSpinnerOnSurveyorDiv();
            CloseSpinnerOnSurveyorPage();
            Thread.Sleep(500);
            EngineerReviewOption.Click();
            CloseSpinnerOnSurveyorDiv();
            CloseSpinnerOnSurveyorPage();
            Thread.Sleep(500);
            Assert.IsTrue(EditButton.Displayed, "No permissions applied to the user to submit SiteRisk Assessment");
            WaitForElementonSurveyor(EditButton);
            EditButton.Click();
            CloseSpinnerOnSurveyorDiv();
            CloseSpinnerOnSurveyorPage();
            Thread.Sleep(500);
        }
        // Submit DR 

        public void SubmitER()
        {
            CloseSpinnerOnSurveyorDiv();
            CloseSpinnerOnSurveyorPage();
            Thread.Sleep(500);
            WaitForElementonSurveyor(AcceptableRiskRadioButton);
            WaitForLoadElementtobeclickable(AcceptableRiskRadioButton);
            AcceptableRiskRadioButton.Click();
            CloseSpinnerOnSurveyorDiv();
            CloseSpinnerOnSurveyorPage();
            WaitForElementonSurveyor(SubmitButton);
            SubmitButton.Click();
            CloseSpinnerOnSurveyorDiv();
            CloseSpinnerOnSurveyorPage();
            WaitForElementonSurveyor(SendButton);
            SendButton.Click();
            CloseSpinnerOnSurveyorDiv();
            CloseSpinnerOnSurveyorPage();
            WaitForElementonSurveyor(EngineerReviewMainPage);
            Assert.IsTrue(EngineerReviewMainPage.Displayed);
            CloseSpinnerOnSurveyorDiv();
            CloseSpinnerOnSurveyorPage();
            Thread.Sleep(500);
        }
        // Save ER
        public void SaveER()
        {
            Thread.Sleep(1000);
            CloseSpinnerOnSurveyorDiv();
            CloseSpinnerOnSurveyorPage();
            Thread.Sleep(500);
            WaitForElementonSurveyor(AcceptableRiskRadioButton);
            AcceptableRiskRadioButton.Click();
            CloseSpinnerOnSurveyorDiv();
            CloseSpinnerOnSurveyorPage();
            Thread.Sleep(500);
            WaitForElementonSurveyor(SubmitButton);
            var SubmitButtonStatus = SubmitButton.GetAttribute("disabled");
            var SaveButtonStatus = AssessmentSaveButton.GetAttribute("disabled");
            if (SubmitButtonStatus != null || SubmitButtonStatus == "true")
            {
                WaitForElementonSurveyor(AssessmentSaveButton);
                AssessmentSaveButton.Click();
                CloseSpinnerOnSurveyorPage();
                CloseSpinnerOnSurveyorDiv();
                Thread.Sleep(500);
                WaitForElementonSurveyor(EngineerReviewMainPage);
                Assert.IsTrue(EngineerReviewMainPage.Displayed, "Failed to send correspondence on Engineer Review Assessment");
            }else
            {
                WaitForElementonSurveyor(SubmitButton);
                SubmitButton.Click();
                CloseSpinnerOnSurveyorPage();
                WaitForElementonSurveyor(SendButton);
                SendButton.Click();
                CloseSpinnerOnSurveyorPage();
                CloseSpinnerOnSurveyorDiv();
                Thread.Sleep(500);
                WaitForElementonSurveyor(EngineerReviewMainPage);
                Assert.IsTrue(EngineerReviewMainPage.Displayed, "Failed to send correspondence on Engineer Review Assessment");
            }           
        }

        public void EngineerReviewMain()
        {
            SurveyorLoginPage.SelectSite();
            // SurveyorAssessments.VerifySiteStatusBeforeAssessment();
            Thread.Sleep(500);
            CloseSpinnerOnSurveyorPage();
            CloseSpinnerOnSurveyorDiv();
            Thread.Sleep(500);
            WaitForElementonSurveyor(EngineerReviewOption);
            WaitForElementonSurveyor(ERStatus);
            var erStatus = ERStatus.Text.ToString();
            Thread.Sleep(500);
            CloseSpinnerOnSurveyorPage();
            CloseSpinnerOnSurveyorDiv();
            Thread.Sleep(500);
            if (erStatus.Contains("Outstanding"))
            {
                OpenER();
                EngineerReviewAssessment();
                SurveyorDocs.CloseSurveyorDocuments();
                SubmitER();
            }
        }

        public void EngineerReviewAssessment()
        {
            CloseSpinnerOnSurveyorDiv();
            CloseSpinnerOnSurveyorPage();
            WaitForElementonSurveyor(NextButton);
            NextButton.Click();
            CloseSpinnerOnSurveyorDiv();
            CloseSpinnerOnSurveyorPage();

        }
        public void SubmitEngineerReviewAssessmentMethod()
        {
            SurveyorLoginPage.SelectSite();
            OpenER();
            EngineerReviewAssessment();
            SurveyorDocs.CloseSurveyorDocuments();
            SubmitER();
        }
    }
}