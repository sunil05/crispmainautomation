﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.PageObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace CrispAutomation.Pages
{
    public class SurveyorDocsSRA : Support.Pages
    {
        public IWebDriver wdriver;
        public SurveyorDocsSRA(ISearchContext driver)
        {
            PageFactory.InitElements(driver, this);
            wdriver = (RemoteWebDriver)driver;
        }


        //Opend Documents on SRA 
        public void OpenSurveyorDocsOnSRA()
        {
            WaitForElementonSurveyor(SiteRiskAssessmentPage.SRAStatus);
            if (SiteRiskAssessmentPage.SRAStatus.Text.Contains("Outstanding"))
            {
                SiteRiskAssessmentPage.OpenSRA();
                WaitForElementonSurveyor(SiteRiskAssessmentPage.AlertInfoOnEdit);
                Assert.IsTrue(SiteRiskAssessmentPage.AlertInfoOnEdit.Displayed);
                SiteRiskAssessmentPage.SiteOverviewSection();
                SiteRiskAssessmentPage.SraEditPage();
                SiteRiskAssessmentPage.AssessmentScoresSection();
                SurveyorDocs.OpenDocuments();
                SiteRiskAssessmentPage.SubmitSRA();

            }else
            {                
                SiteRiskAssessmentPage.OpenSRA();
                //Thread.Sleep(500);
                CloseSpinnerOnSurveyorPage();
                //Thread.Sleep(500);
                WaitForElementonSurveyor(SiteRiskAssessmentPage.NextButton);
                SiteRiskAssessmentPage.NextButton.Click();
                SurveyorDocs.OpenDocuments();
                SiteRiskAssessmentPage.SubmitSRA();
            }
        }      

        //Close Documents on SRA
        public void CloseSurveyorDocsOnSRA()
        {
            WaitForElementonSurveyor(SiteRiskAssessmentPage.SRAStatus);
            if (SiteRiskAssessmentPage.SRAStatus.Text.Contains("Outstanding"))
            {
                SiteRiskAssessmentPage.OpenSRA();
                WaitForElementonSurveyor(SiteRiskAssessmentPage.AlertInfoOnEdit);
                Assert.IsTrue(SiteRiskAssessmentPage.AlertInfoOnEdit.Displayed);
                SiteRiskAssessmentPage.SiteOverviewSection();
                SiteRiskAssessmentPage.SraEditPage();
                SiteRiskAssessmentPage.AssessmentScoresSection();
                CloseDocsonSRA();
                SiteRiskAssessmentPage.SubmitSRA();
            }
            else
            {
                SiteRiskAssessmentPage.OpenSRA();
                //Thread.Sleep(500);
                CloseSpinnerOnSurveyorPage();
                //Thread.Sleep(500);
                WaitForElementonSurveyor(SiteRiskAssessmentPage.NextButton);
                SiteRiskAssessmentPage.NextButton.Click();
                CloseDocsonSRA();
                SiteRiskAssessmentPage.SubmitSRA();
            }
        }
        //Not Closing Documents on SRA
        public void NotClosingDocsonSRA()
        {
            WaitForElementonSurveyor(SiteRiskAssessmentPage.SRAStatus);
            if (SiteRiskAssessmentPage.SRAStatus.Text.Contains("Outstanding")||(SiteRiskAssessmentPage.SRAStatus.Text.Contains("Not required")))
            {
                SiteRiskAssessmentPage.OpenSRA();
                WaitForElementonSurveyor(SiteRiskAssessmentPage.AlertInfoOnEdit);
                Assert.IsTrue(SiteRiskAssessmentPage.AlertInfoOnEdit.Displayed);
                SiteRiskAssessmentPage.SiteOverviewSection();
                SiteRiskAssessmentPage.SraEditPage();
                SiteRiskAssessmentPage.AssessmentScoresSection();
                NotCloseDocsonSRA();
                SiteRiskAssessmentPage.SaveSRA();
            }
            else
            {
                SiteRiskAssessmentPage.OpenSRA();
                //Thread.Sleep(500);
                CloseSpinnerOnSurveyorPage();
                //Thread.Sleep(500);
                WaitForElementonSurveyor(SiteRiskAssessmentPage.NextButton);
                SiteRiskAssessmentPage.NextButton.Click();
                NotCloseDocsonSRA();
                SiteRiskAssessmentPage.SaveSRA();
            }
        }

        //Verify Closed Documents on SRA 
        public void VerifyClosedSurveyorDocsOnSRA()
        {
            SiteRiskAssessmentPage.OpenSRA();
            //Thread.Sleep(500);
            CloseSpinnerOnSurveyorPage();
            //Thread.Sleep(500);
            WaitForElementonSurveyor(SiteRiskAssessmentPage.NextButton);
            SiteRiskAssessmentPage.NextButton.Click();
            SurveyorDocs.ShowClosedDocuments();
            SiteRiskAssessmentPage.SubmitSRA();
        }

        //Close Docs on SRA
        public void CloseDocsonSRA()
        {
            WaitForLoadElements(SurveyorDocs.DocsTabs);
            if (SurveyorDocs.DocsTabs.Count > 0)
            {
                // if it is open on Site Inspection or SRA Close all documents 
                for (int i = 0; i < SurveyorDocs.DocsTabs.Count; i++)
                {
                    var TabStatus = SurveyorDocs.DocsTabs[i].GetAttribute("aria-expanded");
                    Assert.IsTrue(TabStatus == "false");
                   
                    if (SurveyorDocs.RaisedDesignDocPartOf.Contains("Inspection") || SurveyorDocs.RaisedDesignDocPartOf.Contains("Site Risk Assessment")|| SurveyorDocs.RaisedDesignDocPartOf.Contains("Design Review"))
                    {
                        SurveyorDocs.CloseDesignDocuments(i);
                    }
                    else
                    {
                        SurveyorDocs.NotClosingDesignDocuments(i);
                    }

                    if (SurveyorDocs.RaisedDesignDocPartOf.Contains("Inspection") || SurveyorDocs.RaisedDesignDocPartOf.Contains("Site Risk Assessment")|| SurveyorDocs.RaisedDesignDocPartOf.Contains("Design Review"))
                    {
                        SurveyorDocs.CloseDesignItems(i);
                    }
                    else
                    {
                        SurveyorDocs.NotClosingDesignItems(i);
                    }
                    if (SurveyorDocs.RaisedDesignDocPartOf.Contains("Inspection") || SurveyorDocs.RaisedDesignDocPartOf.Contains("Site Risk Assessment")|| SurveyorDocs.RaisedDesignDocPartOf.Contains("Design Review"))
                    {
                        SurveyorDocs.CloseRiskItems(i);
                    }
                    else
                    {
                        SurveyorDocs.NotClosingRiskItems(i);
                    }

                    if (SurveyorDocs.RaisedDesignDocPartOf.Contains("Inspection") || SurveyorDocs.RaisedDesignDocPartOf.Contains("Site Risk Assessment") || SurveyorDocs.RaisedDesignDocPartOf.Contains("Design Review"))
                    {
                        SurveyorDocs.CloseCompletionDocs(i);
                    }
                    else
                    {
                        SurveyorDocs.NotClosingCompletionDocuments(i);
                    }
                }
            }
        }

        //Not Close Docs on SRA 
        public void NotCloseDocsonSRA()
        {
            WaitForLoadElements(SurveyorDocs.DocsTabs);
            if (SurveyorDocs.DocsTabs.Count > 0)
            {
                for (int i = 0; i < SurveyorDocs.DocsTabs.Count; i++)
                {
                    var TabStatus = SurveyorDocs.DocsTabs[i].GetAttribute("aria-expanded");
                    Assert.IsTrue(TabStatus == "false");
                  
                    if (SurveyorDocs.RaisedDesignDocPartOf.Contains("Refurbishment Assessment") || SurveyorDocs.RaisedDesignDocPartOf.Contains("Engineering Review"))
                    {
                        SurveyorDocs.NotClosingDesignDocuments(i);
                    }
                    if (SurveyorDocs.RaisedDesignDocPartOf.Contains("Refurbishment Assessment") || SurveyorDocs.RaisedDesignDocPartOf.Contains("Engineering Review"))
                    {
                        SurveyorDocs.NotClosingCompletionDocuments(i);
                    }
                    if (SurveyorDocs.RaisedDesignDocPartOf.Contains("Refurbishment Assessment") || SurveyorDocs.RaisedDesignDocPartOf.Contains("Engineering Review"))
                    {
                        SurveyorDocs.NotClosingDesignItems(i);
                    }
                    if (SurveyorDocs.RaisedDesignDocPartOf.Contains("Refurbishment Assessment") || SurveyorDocs.RaisedDesignDocPartOf.Contains("Engineering Review"))
                    {
                        SurveyorDocs.NotClosingRiskItems(i);
                    }
                }
            }

        }

    }
}
