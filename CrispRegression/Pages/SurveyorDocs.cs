﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.PageObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace CrispAutomation.Pages
{
    public class SurveyorDocs : Support.Pages
    {
        public IWebDriver wdriver;
        public SurveyorDocs(ISearchContext driver)
        {
            PageFactory.InitElements(driver, this);
            wdriver = (RemoteWebDriver)driver;

        }
        [FindsBy(How = How.XPath, Using = "//div[@role='tab']")]
        public IList<IWebElement> DocsTabs;

        [FindsBy(How = How.XPath, Using = "//div[@role='tab']//h2")]
        public IList<IWebElement> DesignDocs;
        //Design Documents Section

        [FindsBy(How = How.XPath, Using = "//h2[contains(text(),'Design Documents')]")]
        public IWebElement DesignDocsTab;

        [FindsBy(How = How.XPath, Using = "//h2[contains(text(),'Design Documents')]//span[@id='updatedDesignOpenCount']")]
        public IWebElement DesignDocsCount;

        [FindsBy(How = How.XPath, Using = "//table[@id='technicalConditionDocumentsTableDesign']//tbody//tr//td")]
        public IWebElement DesignDocTableData;

        [FindsBy(How = How.XPath, Using = "//div[@role='tab']//button[text()='Add Design Document']")]
        public IWebElement AddDesignDocButton;

        [FindsBy(How = How.XPath, Using = "//div[@id='addDesignDocumentModal']//div[@class='modal-content']")]
        public IWebElement AddDesignDocDiv;

        [FindsBy(How = How.XPath, Using = "//div[@id='addDesignDocumentModal']//div//label[@for='quoteLinkDesign']")]
        public IWebElement DesignDocLinkSite;

        [FindsBy(How = How.XPath, Using = "//div[@id='addDesignDocumentModal']//div//label[@for='plotLinkDesign']")]
        public IWebElement DesignDocLinkPlot;

        [FindsBy(How = How.XPath, Using = "//div[@id='addDesignDocumentModal']//div[@class='modal-content']//label[text()='Document']//parent::div//div//button[@data-toggle='dropdown']")]
        public IList<IWebElement> DesignDocDropdown;

        [FindsBy(How = How.XPath, Using = "//div[@id='addDesignDocumentModal']//div[@class='modal-content']//div[@class='btn-group bootstrap-select form-control other-toggle open']//ul[@class='dropdown-menu inner']//li//span[@class='text']")]
        public IList<IWebElement> DesignDocDropdownInput;

        [FindsBy(How = How.XPath, Using = "//div[@id='addDesignDocumentModal']//div//textarea[@name='managementNotes']")]
        public IWebElement DesignDocNotesInput;

        [FindsBy(How = How.XPath, Using = "//div[@id='addDesignDocumentModal']//div//button[@id='addDesignDocument']")]
        public IWebElement DesignDocAddButton;

        [FindsBy(How = How.XPath, Using = "//table[@id='technicalConditionDocumentsTableDesign']//tbody//tr[@role='row']")]
        public IList<IWebElement> DesignDocsTableRows;

        [FindsBy(How = How.XPath, Using = "//table[@id='technicalConditionDocumentsTableDesign']//tbody//tr[@role='row']//td[4]")]
        public IWebElement RaisedDocPartOf;

        [FindsBy(How = How.XPath, Using = "//table[@id='technicalConditionDocumentsTableDesign']//button[text()='Manage']")]
        public IList<IWebElement> DesignDocViewButton;

        [FindsBy(How = How.XPath, Using = "//div[@id='viewDesignDocumentModal']//div[@class='modal-content']")]
        public IWebElement ViewDesignDocDiv;

        [FindsBy(How = How.XPath, Using = "//div[@id='viewDesignDocumentModal']//div[@class='modal-content']//button[text()='OK']")]
        public IWebElement ViewDesignDocOkButton;

        [FindsBy(How = How.XPath, Using = "//table[@id='technicalConditionDocumentsTableDesign']//button[text()='Remove']")]
        public IWebElement DesignDocRemoveButton;

        //Completion Documents Section

        [FindsBy(How = How.XPath, Using = "//h2[contains(text(),'Completion Documents')]")]
        public IWebElement CompletionDocsTab;

        [FindsBy(How = How.XPath, Using = "//h2[contains(text(),'Completion Documents')]//span[@id='updatedCompletionOpenCount']")]
        public IWebElement CompletionDocsCount;

        [FindsBy(How = How.XPath, Using = "//table[@id='technicalConditionDocumentsTableCompletion']//tbody//tr//td")]
        public IWebElement CompletionDocTableData;

        [FindsBy(How = How.XPath, Using = "//div[@role='tab']//button[text()='Add Completion Document']")]
        public IWebElement AddCompletionDocButton;

        [FindsBy(How = How.XPath, Using = "//div[@id='addCompletionDocumentModal']//div[@class='modal-content']")]
        public IWebElement AddCompletionDocDiv;

        [FindsBy(How = How.XPath, Using = "//div[@id='addCompletionDocumentModal']//div//label[@for='quoteLinkCompletion']")]
        public IWebElement CompletionDocLinkSite;

        [FindsBy(How = How.XPath, Using = "//div[@id='addCompletionDocumentModal']//div//label[@for='plotLinkCompletion']")]
        public IWebElement ComplationDocLinkPlot;

        [FindsBy(How = How.XPath, Using = "//div[@id='addCompletionDocumentModal']//div[@class='modal-content']//label[text()='Document']//parent::div//div//button[@data-toggle='dropdown']")]
        public IList<IWebElement> CompletionDocDropdown;

        [FindsBy(How = How.XPath, Using = "//div[@id='addCompletionDocumentModal']//div[@class='modal-content']//div[@class='btn-group bootstrap-select form-control other-toggle open']//ul[@class='dropdown-menu inner']//li//span[@class='text']")]
        public IList<IWebElement> CompletionDocDropdownInput;

        [FindsBy(How = How.XPath, Using = "//div[@id='addCompletionDocumentModal']//div//textarea[@name='managementNotes']")]
        public IWebElement CompletionDocNotesInput;

        [FindsBy(How = How.XPath, Using = "//div[@id='addCompletionDocumentModal']//div//button[@id='addCompletionDocument']")]
        public IWebElement CompletionDocAddButton;

        [FindsBy(How = How.XPath, Using = "//table[@id='technicalConditionDocumentsTableCompletion']//tbody//tr[@role='row']")]
        public IList<IWebElement> CompletionDocsTableRows;

        [FindsBy(How = How.XPath, Using = "//table[@id='technicalConditionDocumentsTableCompletion']//button[text()='Manage']")]
        public IList<IWebElement> CompletionDocViewButton;

        [FindsBy(How = How.XPath, Using = "//div[@id='viewCompletionDocumentModal']//div[@class='modal-content']")]
        public IWebElement ViewCompletionDocDiv;

        [FindsBy(How = How.XPath, Using = "//div[@id='viewCompletionDocumentModal']//div[@class='modal-content']//button[text()='OK']")]
        public IWebElement ViewCompletionDocOkButton;

        [FindsBy(How = How.XPath, Using = "//table[@id='technicalConditionDocumentsTableCompletion']//button[text()='Remove']")]
        public IWebElement CompletionDocRemoveButton;

        //Design Items Section

        [FindsBy(How = How.XPath, Using = "//h2[contains(text(),'Design Items')]")]
        public IWebElement DesignItemsTab;

        [FindsBy(How = How.XPath, Using = "//h2[contains(text(),'Design Items')]//span[@id='updatedDesignOpenCount']")]
        public IWebElement DesignItemsCount;

        [FindsBy(How = How.XPath, Using = "//table[@id='technicalConditionItemsTableDesign']//tbody//tr//td")]
        public IWebElement DesignItemsDocTableData;

        [FindsBy(How = How.XPath, Using = "//div[@role='tab']//button[text()='Add Design Item']")]
        public IWebElement AddDesignItemsDocButton;

        [FindsBy(How = How.XPath, Using = "//div[@id='addDesignItemModal']//div[@class='modal-content']")]
        public IWebElement AddDesignItemsDocDiv;

        [FindsBy(How = How.XPath, Using = "//div[@id='addDesignItemModal']//div//label[@for='quoteLinkItemDesign']")]
        public IWebElement DesignItemLinkSite;

        [FindsBy(How = How.XPath, Using = "//div[@id='addDesignItemModal']//div//label[@for='plotLinkItemDesign']")]
        public IWebElement DesignItemLinkPlot;

        [FindsBy(How = How.XPath, Using = "//div[@id='addDesignItemModal']//div[@class='modal-content']//label[text()='Technical Condition Area']//parent::div//div//button[@data-toggle='dropdown']")]
        public IList<IWebElement> DesignItemsDocDropdown;

        [FindsBy(How = How.XPath, Using = "//div[@id='addDesignItemModal']//div[@class='modal-content']//div[@class='btn-group bootstrap-select form-control other-toggle open']//ul[@class='dropdown-menu inner']//li//span[@class='text']")]
        public IList<IWebElement> DesignItemsDocDropdownInput;

        [FindsBy(How = How.XPath, Using = "//div[@id='addDesignItemModal']//div[@class='modal-content']//label[text()='Description']//following-sibling::input")]
        public IWebElement DesignItemsDescriptionInput;

        [FindsBy(How = How.XPath, Using = "//div[@id='addDesignItemModal']//div//textarea[@name='managementNotes']")]
        public IWebElement DesignItemsDocNotesInput;

        [FindsBy(How = How.XPath, Using = "//div[@id='addDesignItemModal']//div//button[@id='addDesignItem']")]
        public IWebElement DesignItemsDocAddButton;

        [FindsBy(How = How.XPath, Using = "//table[@id='technicalConditionItemsTableDesign']//tbody//tr[@role='row']")]
        public IList<IWebElement> DesignItemsDocsTableRows;

        [FindsBy(How = How.XPath, Using = "//table[@id='technicalConditionItemsTableDesign']//button[text()='Manage']")]
        public IList<IWebElement> DesignItemsDocViewButton;

        [FindsBy(How = How.XPath, Using = "//div[@id='viewDesignItemModal']//div[@class='modal-content']")]
        public IWebElement ViewDesignItemsDocDiv;

        [FindsBy(How = How.XPath, Using = "//div[@id='viewDesignItemModal']//div[@class='modal-content']//button[text()='OK']")]
        public IWebElement ViewDesignItemsDocOkButton;

        [FindsBy(How = How.XPath, Using = "//table[@id='technicalConditionItemsTableDesign']//button[text()='Remove']")]
        public IWebElement DesignItemsDocRemoveButton;

        //Risk Item Section

        [FindsBy(How = How.XPath, Using = "//h2[contains(text(),'Risk Items')]")]
        public IWebElement RiskItemsTab;

        [FindsBy(How = How.XPath, Using = "//h2[contains(text(),'Risk Items')]//span[@id='updatedRiskOpenCount']")]
        public IWebElement RiskItemsCount;

        [FindsBy(How = How.XPath, Using = "//table[@id='technicalConditionItemsTableRisk']//tbody//tr//td")]
        public IWebElement RiskItemsDocTableData;

        [FindsBy(How = How.XPath, Using = "//div[@role='tab']//button[text()='Add Risk Item']")]
        public IWebElement AddRiskItemsDocButton;

        [FindsBy(How = How.XPath, Using = "//div[@id='addRiskItemModal']//div[@class='modal-content']")]
        public IWebElement AddRiskItemsDocDiv;

        [FindsBy(How = How.XPath, Using = "//div[@id='addRiskItemModal']//div//label[@for='quoteLinkItemRisk']")]
        public IWebElement RiskItemLinkSite;

        [FindsBy(How = How.XPath, Using = "//div[@id='addRiskItemModal']//div//label[@for='plotLinkItemRisk']")]
        public IWebElement RiskItemLinkPlot;

        [FindsBy(How = How.XPath, Using = "//div[@id='addRiskItemModal']//div[@class='modal-content']//label[text()='Technical Condition Area']//parent::div//div//button[@data-toggle='dropdown']")]
        public IList<IWebElement> RiskItemsDocDropdown;

        [FindsBy(How = How.XPath, Using = "//div[@id='addRiskItemModal']//div[@class='modal-content']//div[@class='btn-group bootstrap-select form-control other-toggle open']//ul[@class='dropdown-menu inner']//li//span[@class='text']")]
        public IList<IWebElement> RiskItemsDocDropdownInput;

        [FindsBy(How = How.XPath, Using = "//div[@id='addRiskItemModal']//div[@class='modal-content']//label[text()='Description']//following-sibling::input")]
        public IWebElement RiskItemsDescriptionInput;

        [FindsBy(How = How.XPath, Using = "//div[@id='addRiskItemModal']//div//textarea[@name='managementNotes']")]
        public IWebElement RiskItemsDocNotesInput;

        [FindsBy(How = How.XPath, Using = "//div[@id='addRiskItemModal']//div//button[@id='addRiskItem']")]
        public IWebElement RiskItemsDocAddButton;

        [FindsBy(How = How.XPath, Using = "//table[@id='technicalConditionItemsTableRisk']//tbody//tr[@role='row']")]
        public IList<IWebElement> RiskItemsDocsTableRows;

        [FindsBy(How = How.XPath, Using = "//table[@id='technicalConditionItemsTableRisk']//button[text()='Manage']")]
        public IList<IWebElement> RiskItemsDocViewButton;

        [FindsBy(How = How.XPath, Using = "//div[@id='viewRiskItemModal']//div[@class='modal-content']")]
        public IWebElement ViewRiskItemsDocDiv;

        [FindsBy(How = How.XPath, Using = "//div[@id='viewRiskItemModal']//div[@class='modal-content']//button[text()='OK']")]
        public IWebElement ViewRiskItemsDocOkButton;

        [FindsBy(How = How.XPath, Using = "//table[@id='technicalConditionItemsTableRisk']//button[text()='Remove']")]
        public IWebElement RiskItemsDocRemoveButton;

        //Defects Section

        [FindsBy(How = How.XPath, Using = "//h2[contains(text(),'Defects')]")]
        public IWebElement DefectsTab;

        [FindsBy(How = How.XPath, Using = "//h2[contains(text(),'Defects')]//span[@id='updatedDefectOpenCount']")]
        public IWebElement DefectsCount;

        [FindsBy(How = How.XPath, Using = "//table[@id='defectsTable']//tbody//tr//td")]
        public IWebElement DefectsTableData;

        [FindsBy(How = How.XPath, Using = "//div[@role='tab']//button[text()='Add Defect']")]
        public IWebElement AddDefectsButton;

        [FindsBy(How = How.XPath, Using = "//div[@id='addDefectModal']//div[@class='modal-content']")]
        public IWebElement CreateDefectsDiv;


        [FindsBy(How = How.XPath, Using = "//table[@id='defectsTable']//tr[@role='row'][@class='odd']")]
        public IList<IWebElement> DefectsList;

        public static int Defects { get; set; }

        [FindsBy(How = How.XPath, Using = "//div[@id='SummaryPage_Plots']//table//tbody//tr//div//p[text()='Cannot be signed off since it has open defects']")]
        public IWebElement OpenDefects;

        [FindsBy(How = How.XPath, Using = "//div[@id='addDefectModal']//div[@class='modal-content']//button[@data-toggle='dropdown'][@data-id='housePart']")]
        public IWebElement HousePartDropdown;

        [FindsBy(How = How.XPath, Using = "//div[@id='addDefectModal']//div[@class='modal-content']//button[@data-id='housePart']//following-sibling::div[@class='dropdown-menu open']//ul//li")]
        public IList<IWebElement> HousePartDropdownInput;

        [FindsBy(How = How.XPath, Using = "//div[@id='addDefectModal']//div[@class='modal-content']//button[@data-toggle='dropdown'][@data-id='partType']")]
        public IWebElement PartTypeDropdown;

        [FindsBy(How = How.XPath, Using = "//div[@id='addDefectModal']//div[@class='modal-content']//button[@data-id='partType']//following-sibling::div[@class='dropdown-menu open']//ul//li")]
        public IList<IWebElement> PartTypeInput;

        [FindsBy(How = How.XPath, Using = "//div[@id='addDefectModal']//div[@class='modal-content']//button[@data-toggle='dropdown'][@data-id='defectCauses']")]
        public IWebElement DefectCauseDropdown;

        [FindsBy(How = How.XPath, Using = "//div[@id='addDefectModal']//div[@class='modal-content']//button[@data-id='defectCauses']//following-sibling::div[@class='dropdown-menu open']//ul//li")]
        public IList<IWebElement> DefectCauseInput;

        [FindsBy(How = How.XPath, Using = "//div[@id='addDefectModal']//div[@class='modal-content']//button[@data-toggle='dropdown'][@data-id='plots']")]
        public IWebElement PlotsDropdown;

        [FindsBy(How = How.XPath, Using = "//div[@id='addDefectModal']//div[@class='modal-content']//button[@data-id='plots']//following-sibling::div[@class='dropdown-menu open']//ul//li")]
        public IList<IWebElement> PlotsInput;

        [FindsBy(How = How.XPath, Using = "//div[@id='addDefectModal']//div[@class='modal-content']//button[@data-toggle='dropdown'][@data-id='products']")]
        public IWebElement ProductsDropdown;

        [FindsBy(How = How.XPath, Using = "//div[@id='addDefectModal']//div[@class='modal-content']//button[@data-id='products']//following-sibling::div[@class='dropdown-menu open']//ul//li")]
        public IList<IWebElement> ProductsInput;

        [FindsBy(How = How.XPath, Using = "//div[@id='addDefectModal']//div[@class='modal-content']//textarea[@id='details']")]
        public IWebElement DefectsDetailsInput;

        [FindsBy(How = How.XPath, Using = "//div[@id='addDefectModal']//div//textarea[@name='comments']")]
        public IWebElement DefectsCommentsInput;

        [FindsBy(How = How.XPath, Using = "//div[@id='addDefectModal']//div//button[@id='addFile']")]
        public IWebElement AddFilesButtonOnDefects;

        [FindsBy(How = How.XPath, Using = "//div[@id='addDefectModal']//div//button[@id='addDefect']")]
        public IWebElement DefectsAddButton;

        [FindsBy(How = How.XPath, Using = "//table[@id='defectsTable']//tbody//tr[@role='row']")]
        public IList<IWebElement> DefectsTableRows;

        [FindsBy(How = How.XPath, Using = "//table[@id='defectsTable']//button[text()='View']")]
        public IWebElement DefectsViewButton;

        [FindsBy(How = How.XPath, Using = "//div[@id='viewDefectModal']//div[@class='modal-content']")]
        public IWebElement ViewDefectsDiv;

        [FindsBy(How = How.XPath, Using = "//div[@id='viewDefectModal']//div[@class='modal-content']//button[text()='OK']")]
        public IWebElement ViewDefectsOkButton;

        [FindsBy(How = How.XPath, Using = "//table[@id='defectsTable']//button[text()='Remove']")]
        public IWebElement DefectsRemoveButton;

        //Closing Design Documents 

        [FindsBy(How = How.XPath, Using = "//div[@id='technicalConditionDocumentsTableDesign_paginate']//span//a")]
        public IList<IWebElement> DesignDocPages;

        [FindsBy(How = How.XPath, Using = "//div[@id='technicalConditionDocumentsDesign']//span[@id='updatedDesignOpenCount']")]
        public IWebElement OpenDesignDocCount;

        [FindsBy(How = How.XPath, Using = "//table[@id='technicalConditionDocumentsTableDesign']//tbody//tr[@role='row']//td[5]//parent::tr//td//button[text()='Manage']")]
        public IList<IWebElement> DesignDocManageButton;

        [FindsBy(How = How.XPath, Using = "//div[@id='viewDesignDocumentModal']//div//div[@class='modal-content']")]
        public IWebElement ManageDesignDocDiv;

        [FindsBy(How = How.XPath, Using = "//div[@id='viewDesignDocumentModal']//label[text()='Management Notes']//following-sibling::textarea")]
        public IWebElement ManagementNotesOnManageDesignDoc;

        [FindsBy(How = How.XPath, Using = "//div[@id='viewDesignDocumentModal']//label[text()='Document Status']//parent::div//div//button[@data-toggle='dropdown']")]
        public IWebElement DesignDocDocumentStatusDropdown;

        [FindsBy(How = How.XPath, Using = "//div[@id='viewDesignDocumentModal']//div[@class='dropdown-menu open']//ul//li//a//span[text()='Approved']")]
        public IWebElement DesignDocumentApproved;

        [FindsBy(How = How.XPath, Using = "//div[@id='viewDesignDocumentModal']//label[text()='Closing Comments']//following-sibling::textarea")]
        public IWebElement DesignDocClosingComments;

        [FindsBy(How = How.XPath, Using = "//div[@id='viewDesignDocumentModal']//div//button[text()='Save']")]
        public IWebElement CloseDesignDocSaveButton;

        [FindsBy(How = How.XPath, Using = "//div[@class='checkbox']//label[text()='Show Closed']")]
        public IList<IWebElement> ShowClosedCheckBox;

        [FindsBy(How = How.XPath, Using = "//table//tr[@role='row'][@class='odd']")]
        public IList<IWebElement> ShowClosedDocs;

        [FindsBy(How = How.XPath, Using = "//div[@id='viewDesignDocumentModal']//div[@class='modal-content']//div[@class='close-overlay']")]
        public IWebElement NotClosingDesignDocTitle;

        [FindsBy(How = How.XPath, Using = "//div[@id='viewDesignDocumentModal']//div[@class='modal-content']//button[text()='OK']")]
        public IWebElement NotClosingDesignDocOkButton;

        //Closing Completion Documents 

        [FindsBy(How = How.XPath, Using = "//div[@id='technicalConditionDocumentsTableCompletion_paginate']//span//a")]
        public IList<IWebElement> CompletionDocsPages;

        [FindsBy(How = How.XPath, Using = "//table[@id='technicalConditionDocumentsTableCompletion']//tbody//tr[@role='row']//td[5]//parent::tr//td//button[text()='Manage']")]
        public IList<IWebElement> CompletionDocManageButton;

        [FindsBy(How = How.XPath, Using = "//div[@id='viewCompletionDocumentModal']//div//div[@class='modal-content']")]
        public IWebElement ManageCompletionDocDiv;

        [FindsBy(How = How.XPath, Using = "//div[@id='viewCompletionDocumentModal']//label[text()='Management Notes']//following-sibling::textarea")]
        public IWebElement ManagementNotesOnCompletionDoc;

        [FindsBy(How = How.XPath, Using = "//div[@id='viewCompletionDocumentModal']//label[text()='Document Status']//parent::div//div//button[@data-toggle='dropdown']")]
        public IWebElement CompletionDocDocumentStatusDropdown;

        [FindsBy(How = How.XPath, Using = "//div[@id='viewCompletionDocumentModal']//div[@class='dropdown-menu open']//ul//li//a//span[text()='Approved']")]
        public IWebElement CompletionDocApproved;

        [FindsBy(How = How.XPath, Using = "//div[@id='viewCompletionDocumentModal']//label[text()='Closing Comments']//following-sibling::textarea")]
        public IWebElement CompletionDocClosingComments;

        [FindsBy(How = How.XPath, Using = "//div[@id='viewCompletionDocumentModal']//div//button[text()='Save']")]
        public IWebElement CloseCompletionDocSaveButton;

        [FindsBy(How = How.XPath, Using = "//div[@id='viewCompletionDocumentModal']//div[@class='modal-content']//div[@class='close-overlay']")]
        public IWebElement NotClosingCompletionDocTitle;

        [FindsBy(How = How.XPath, Using = "//div[@id='viewCompletionDocumentModal']//div[@class='modal-content']//button[text()='OK']")]
        public IWebElement NotClosingCompletionDocOkButton;


        //Closing Design Item Model 
        [FindsBy(How = How.XPath, Using = "//div[@id='technicalConditionItemsTableDesign_paginate']//span//a")]
        public IList<IWebElement> DesignItemPages;

        [FindsBy(How = How.XPath, Using = "//table[@id='technicalConditionItemsTableDesign']//tbody//tr[@role='row']//td[5]//parent::tr//td//button[text()='Manage']")]
        public IList<IWebElement> DesignItemManageButton;

        [FindsBy(How = How.XPath, Using = "//div[@id='viewDesignItemModal']//div//div[@class='modal-content']")]
        public IWebElement ManageDesignItemDiv;

        [FindsBy(How = How.XPath, Using = "//div[@id='viewDesignItemModal']//label[text()='Management Notes']//following-sibling::textarea")]
        public IWebElement ManagementNotesOnDesignItem;

        [FindsBy(How = How.XPath, Using = "//div[@id='viewDesignItemModal']//label[text()='Item Status']//parent::div//div//button[@data-toggle='dropdown']")]
        public IWebElement DesignItemDocumentStatusDropdown;

        [FindsBy(How = How.XPath, Using = "//div[@id='viewDesignItemModal']//div[@class='dropdown-menu open']//ul//li//a//span[text()='Closed']")]
        public IWebElement DesignItemClosed;

        [FindsBy(How = How.XPath, Using = "//div[@id='viewDesignItemModal']//label[text()='Closing Comments']//following-sibling::textarea")]
        public IWebElement DesignItemClosingComments;

        [FindsBy(How = How.XPath, Using = "//div[@id='viewDesignItemModal']//div//button[text()='Save']")]
        public IWebElement CloseDesignItemSaveButton;

        [FindsBy(How = How.XPath, Using = "//div[@id='viewDesignItemModal']//div[@class='modal-content']//div[@class='close-overlay']")]
        public IWebElement NotClosingDesignItemTitle;

        [FindsBy(How = How.XPath, Using = "//div[@id='viewDesignItemModal']//div[@class='modal-content']//button[text()='OK']")]
        public IWebElement NotClosingDesignItemOkButton;

        //Closing Risk Item Model 

        [FindsBy(How = How.XPath, Using = "//div[@id='technicalConditionItemsTableRisk_paginate']//span//a")]
        public IList<IWebElement> RiskItemPages;

        [FindsBy(How = How.XPath, Using = "//table[@id='technicalConditionItemsTableRisk']//tbody//tr[@role='row']//td[5]//parent::tr//td//button[text()='Manage']")]
        public IList<IWebElement> RiskItemManageButton;

        [FindsBy(How = How.XPath, Using = "//div[@id='viewRiskItemModal']//div//div[@class='modal-content']")]
        public IWebElement ManageRiskItemDiv;

        [FindsBy(How = How.XPath, Using = "//div[@id='viewRiskItemModal']//label[text()='Management Notes']//following-sibling::textarea")]
        public IWebElement ManagementNotesOnRiskItem;

        [FindsBy(How = How.XPath, Using = "//div[@id='viewRiskItemModal']//label[text()='Item Status']//parent::div//div//button[@data-toggle='dropdown']")]
        public IWebElement RiskItemDocumentStatusDropdown;

        [FindsBy(How = How.XPath, Using = "//div[@id='viewRiskItemModal']//div[@class='dropdown-menu open']//ul//li//a//span[text()='Closed']")]
        public IWebElement RiskItemClosed;

        [FindsBy(How = How.XPath, Using = "//div[@id='viewRiskItemModal']//label[text()='Closing Comments']//following-sibling::textarea")]
        public IWebElement RiskItemClosingComments;

        [FindsBy(How = How.XPath, Using = "//div[@id='viewRiskItemModal']//div//button[text()='Save']")]
        public IWebElement CloseRiskItemSaveButton;

        [FindsBy(How = How.XPath, Using = "//div[@id='viewRiskItemModal']//div[@class='modal-content']//div[@class='close-overlay']")]
        public IWebElement NotClosingRiskItemTitle;

        [FindsBy(How = How.XPath, Using = "//div[@id='viewRiskItemModal']//div[@class='modal-content']//button[text()='OK']")]
        public IWebElement NotClosingRiskItemOkButton;

        //Closing Defects 

        [FindsBy(How = How.XPath, Using = "//table[@id='defectsTable']//button[text()='Close']")]
        public IList<IWebElement> DefectsCloseButton;

        [FindsBy(How = How.XPath, Using = "//div[@id='resolveDefect']//div[@class='modal-content']")]
        public IWebElement CloseDefectDiv;

        [FindsBy(How = How.Id, Using = "resolutionComments")]
        public IWebElement DefectResolutionComment;

        [FindsBy(How = How.XPath, Using = "//button[@data-id='defectPlots']")]
        public IWebElement ResolvedPlotsDropdown;

        [FindsBy(How = How.XPath, Using = "//div[@id='resolveDefect']//div[@class='dropdown-menu open']//ul[@role='menu']//li")]
        public IWebElement ResolvedPlotsDropdownInput;

        [FindsBy(How = How.Id, Using = "//button[@id='resolve']")]
        public IWebElement ResolveButton;

        //Raised Documents As Part of 

        [FindsBy(How = How.XPath, Using = "//table[@id='technicalConditionDocumentsTableDesign']//tbody//tr[@role='row']//td[5]")]
        public IList<IWebElement> DesignDocPartOf;

        [FindsBy(How = How.XPath, Using = "//table[@id='technicalConditionDocumentsTableCompletion']//tbody//tr[@role='row']//td[5]")]
        public IList<IWebElement> CompletionDocPartOf;

        [FindsBy(How = How.XPath, Using = "//table[@id='technicalConditionItemsTableDesign']//tbody//tr[@role='row']//td[5]")]
        public IList<IWebElement> DesignItemDocPartOf;

        [FindsBy(How = How.XPath, Using = "//table[@id='technicalConditionItemsTableRisk']//tbody//tr[@role='row']//td[5]")]
        public IList<IWebElement> RiskItemDocPartOf;

        [FindsBy(How = How.XPath, Using = "//table[@id='defectsTable']//tbody//tr[@role='row']//td[5]")]
        public IList<IWebElement> DefectsPartOf;

        [FindsBy(How = How.XPath, Using = "//section[@class='content']//h2")]
        public IList<IWebElement> Assessment;

        [FindsBy(How = How.XPath, Using = "//section[@class='content']//h2[contains(text(),'Site Risk Assessment')]")]
        public IWebElement SRAAssessment;

        [FindsBy(How = How.XPath, Using = "//section[@class='content']//h2[contains(text(),'Refurbishment Assessment')]")]
        public IWebElement RAAssessment;

        [FindsBy(How = How.XPath, Using = "//section[@class='content']//h2[contains(text(),'Design Review')]")]
        public IWebElement DRAssessment;

        [FindsBy(How = How.XPath, Using = "//section[@class='content']//h2[contains(text(),'Engineering Review')]")]
        public IWebElement ERAssessment;

        //Design Documents Count 

        [FindsBy(How = How.XPath, Using = "//table[@id='technicalConditionDocumentsTableDesign']//tbody//tr[@role='row']//td[5][text()='Site Risk Assessment']//parent::tr//td//button[text()='Manage']")]
        public IList<IWebElement> SRADesignDocs;

        [FindsBy(How = How.XPath, Using = "//table[@id='technicalConditionDocumentsTableDesign']//tbody//tr[@role='row']//td[5][text()='Refurbishment Assessment']//parent::tr//td//button[text()='Manage']")]
        public IList<IWebElement> RADesignDocs;

        [FindsBy(How = How.XPath, Using = "//table[@id='technicalConditionDocumentsTableDesign']//tbody//tr[@role='row']//td[5][text()='Design Review']//parent::tr//td//button[text()='Manage']")]
        public IList<IWebElement> DRDesignDocs;

        [FindsBy(How = How.XPath, Using = "//table[@id='technicalConditionDocumentsTableDesign']//tbody//tr[@role='row']//td[5][text()='Engineering Review']//parent::tr//td//button[text()='Manage']")]
        public IList<IWebElement> ERDesignDocs;

        //Design Items Count 

        [FindsBy(How = How.XPath, Using = "//table[@id='technicalConditionItemsTableDesign']//tbody//tr[@role='row']//td[5][text()='Site Risk Assessment']//parent::tr//td//button[text()='Manage']")]
        public IList<IWebElement> SRADesignItems;

        [FindsBy(How = How.XPath, Using = "//table[@id='technicalConditionItemsTableDesign']//tbody//tr[@role='row']//td[5][text()='Refurbishment Assessment']//parent::tr//td//button[text()='Manage']")]
        public IList<IWebElement> RADesignItems;

        [FindsBy(How = How.XPath, Using = "//table[@id='technicalConditionItemsTableDesign']//tbody//tr[@role='row']//td[5][text()='Design Review']//parent::tr//td//button[text()='Manage']")]
        public IList<IWebElement> DRDesignItems;

        [FindsBy(How = How.XPath, Using = "//table[@id='technicalConditionItemsTableDesign']//tbody//tr[@role='row']//td[5][text()='Engineering Review']//parent::tr//td//button[text()='Manage']")]
        public IList<IWebElement> ERDesignItems;

        //Risk Items Count 

        [FindsBy(How = How.XPath, Using = "//table[@id='technicalConditionItemsTableRisk']//tbody//tr[@role='row']//td[5][text()='Site Risk Assessment']//parent::tr//td//button[text()='Manage']")]
        public IList<IWebElement> SRARiskItems;

        [FindsBy(How = How.XPath, Using = "//table[@id='technicalConditionItemsTableRisk']//tbody//tr[@role='row']//td[5][text()='Refurbishment Assessment']//parent::tr//td//button[text()='Manage']")]
        public IList<IWebElement> RARiskItems;

        [FindsBy(How = How.XPath, Using = "//table[@id='technicalConditionItemsTableRisk']//tbody//tr[@role='row']//td[5][text()='Design Review']//parent::tr//td//button[text()='Manage']")]
        public IList<IWebElement> DRRiskItems;

        [FindsBy(How = How.XPath, Using = "//table[@id='technicalConditionItemsTableRisk']//tbody//tr[@role='row']//td[5][text()='Engineering Review']//parent::tr//td//button[text()='Manage']")]
        public IList<IWebElement> ERRiskItems;

        //Completion Documents Count 

        [FindsBy(How = How.XPath, Using = "//table[@id='technicalConditionDocumentsTableCompletion']//tbody//tr[@role='row']//td[5][text()='Site Risk Assessment']//parent::tr//td//button[text()='Manage']")]
        public IList<IWebElement> SRACompletionDocs;

        [FindsBy(How = How.XPath, Using = "//table[@id='technicalConditionDocumentsTableCompletion']//tbody//tr[@role='row']//td[5][text()='Refurbishment Assessment']//parent::tr//td//button[text()='Manage']")]
        public IList<IWebElement> RACompletionDocs;

        [FindsBy(How = How.XPath, Using = "//table[@id='technicalConditionDocumentsTableCompletion']//tbody//tr[@role='row']//td[5][text()='Design Review']//parent::tr//td//button[text()='Manage']")]
        public IList<IWebElement> DRCompletionDocs;

        [FindsBy(How = How.XPath, Using = "//table[@id='technicalConditionDocumentsTableCompletion']//tbody//tr[@role='row']//td[5][text()='Engineering Review']//parent::tr//td//button[text()='Manage']")]
        public IList<IWebElement> ERCompletionDocs;

        public static List<string> RaisedDesignDocPartOf = new List<string>();
        public static List<string> RaisedCompletionDocPartOf = new List<string>();
        public static List<string> RaisedDesignItemDocPartOf = new List<string>();
        public static List<string> RaisedRiskItemDocPartOf = new List<string>();
        public static List<string> RaisedDefectsPartOf = new List<string>();
        //Actual SRA Document Count  
        public static int sraDesignDocCount = 0;
        public static int sraDesignItemCount = 0;
        public static int sraRiskItemCount = 0;
        public static int sraCompletionDocCount = 0;

        //Actual RA Document Count  
        public static int raDesignDocCount = 0;
        public static int raDesignItemCount = 0;
        public static int raRiskItemCount = 0;
        public static int raCompletionDocCount = 0;

        //Actual DR Document Count  
        public static int drDesignDocCount = 0;
        public static int drDesignItemCount = 0;
        public static int drRiskItemCount = 0;
        public static int drCompletionDocCount = 0;

        //Actual ER Document Count  
        public static int erDesignDocCount = 0;
        public static int erDesignItemCount = 0;
        public static int erRiskItemCount = 0;
        public static int erCompletionDocCount = 0;

        int actualDesignDocCount = 0;
        int actualDesignItemCount = 0;
        int actualRiskItemCount = 0;
        int actualCompletionDocCount = 0;

        public void OpenDocuments()
        {
            WaitForLoadElements(DocsTabs);
            if (DocsTabs.Count > 0)
            {
                for (int i = 0; i < DocsTabs.Count; i++)
                {
                    var TabStatus = DocsTabs[i].GetAttribute("aria-expanded");
                    Assert.IsTrue(TabStatus == "false");
                    OpenDesignDocument(i);
                    OpenDesignItems(i);
                    OpenRiskItems(i);
                    OpenCompletionDocument(i);
                    OpenDefectItem(i);
                }
            }
        }
        public void OpenDesignDocument(int i)
        {
            if (DocsTabs[i].Text.Contains("Design Documents"))
            {
                WaitForElementonSurveyor(DesignDocsTab);
                WaitForElementonSurveyor(DesignDocsCount);
                //Assert.IsTrue(DesignDocsCount.Text == "0");
                DesignDocsTab.Click();
                var TabStatusOnClick = DocsTabs[i].GetAttribute("aria-expanded");
                Assert.IsTrue(TabStatusOnClick == "true");
                WaitForElementonSurveyor(DesignDocTableData);
                var TableDataBeforeUpload = DesignDocTableData.Text;
                //  Assert.IsTrue(TableDataBeforeUpload == "No data available in table");
                AddDesignDocument();
                Scrollup();
                WaitForElementonSurveyor(DesignDocsTab);
                DesignDocsTab.Click();
            }
        }


        public void OpenDesignItems(int i)
        {
            if (DocsTabs[i].Text.Contains("Design Items"))
            {
                WaitForElementonSurveyor(DesignItemsTab);
                WaitForElementonSurveyor(DesignItemsCount);
                //Assert.IsTrue(DesignItemsCount.Text == "0");
                DesignItemsTab.Click();
                var TabStatusOnClick = DocsTabs[i].GetAttribute("aria-expanded");
                Assert.IsTrue(TabStatusOnClick == "true");
                WaitForElementonSurveyor(DesignItemsDocTableData);
                var TableDataBeforeUpload = DesignItemsDocTableData.Text;
                // Assert.IsTrue(TableDataBeforeUpload == "No data available in table");
                AddDesignItems();
                Scrollup();
                WaitForElementonSurveyor(DesignItemsTab);
                DesignItemsTab.Click();
            }
        }

        public void OpenRiskItems(int i)
        {
            if (DocsTabs[i].Text.Contains("Risk Items"))
            {
                WaitForElementonSurveyor(RiskItemsTab);
                WaitForElementonSurveyor(RiskItemsCount);
                //Assert.IsTrue(RiskItemsCount.Text == "0");
                RiskItemsTab.Click();
                var TabStatusOnClick = DocsTabs[i].GetAttribute("aria-expanded");
                Assert.IsTrue(TabStatusOnClick == "true");
                WaitForElementonSurveyor(RiskItemsDocTableData);
                var TableDataBeforeUpload = RiskItemsDocTableData.Text;
                // Assert.IsTrue(TableDataBeforeUpload == "No data available in table");
                AddRiskItems();
                Scrollup();
                WaitForElementonSurveyor(RiskItemsTab);
                RiskItemsTab.Click();
            }
        }
        public void OpenCompletionDocument(int i)
        {
            if (DocsTabs[i].Text.Contains("Completion Documents"))
            {
                WaitForElementonSurveyor(CompletionDocsTab);
                WaitForElementonSurveyor(CompletionDocsCount);
                // Assert.IsTrue(CompletionDocsCount.Text == "0");
                CompletionDocsTab.Click();
                var TabStatusOnClick = DocsTabs[i].GetAttribute("aria-expanded");
                Assert.IsTrue(TabStatusOnClick == "true");
                WaitForElementonSurveyor(CompletionDocTableData);
                var TableDataBeforeUpload = CompletionDocTableData.Text;
                // Assert.IsTrue(TableDataBeforeUpload == "No data available in table");
                AddCompletionDocument();
                Scrollup();
                WaitForElementonSurveyor(CompletionDocsTab);
                CompletionDocsTab.Click();
            }
        }


        public void OpenDefectItem(int i)
        {

            if (DocsTabs[i].Text.Contains("Defects"))
            {
                WaitForElementonSurveyor(DefectsTab);
                WaitForElementonSurveyor(DefectsCount);
                //  Assert.IsTrue(DefectsCount.Text == "0");
                DefectsTab.Click();
                var TabStatusOnClick = DocsTabs[i].GetAttribute("aria-expanded");
                Assert.IsTrue(TabStatusOnClick == "true");
                WaitForElementonSurveyor(DefectsTableData);
                var TableDataBeforeUpload = DefectsTableData.Text;
                //  Assert.IsTrue(TableDataBeforeUpload == "No data available in table");
                AddDefects();
                Scrollup();
                Defects = DefectsList.Count;
                WaitForElementonSurveyor(DefectsTab);
                DefectsTab.Click();
            }
        }

        public void CloseDocuments()
        {
            WaitForLoadElements(DocsTabs);
            if (DocsTabs.Count > 0)
            {
                for (int i = 0; i < DocsTabs.Count; i++)
                {
                    var TabStatus = DocsTabs[i].GetAttribute("aria-expanded");
                    Assert.IsTrue(TabStatus == "false");
                    CloseDesignDocuments(i);
                    CloseDesignItems(i);
                    CloseRiskItems(i);
                    CloseCompletionDocs(i);
                    CloseDefects(i);

                }
            }
        }
        public void CloseDesignDocuments(int i)
        {
            if (DocsTabs[i].Text.Contains("Design Documents"))
            {

                WaitForElementonSurveyor(DesignDocsTab);
                WaitForElementonSurveyor(DesignDocsCount);
                // Assert.IsTrue(DesignDocsCount.Text == "1");
                DesignDocsTab.Click();
                var TabStatusOnClick = DocsTabs[i].GetAttribute("aria-expanded");
                Assert.IsTrue(TabStatusOnClick == "true");
                //Thread.Sleep(500);
                CloseSpinnerOnSurveyorDiv();
                CloseSpinnerOnSurveyorPage();
                //Thread.Sleep(500);
                WaitForElementonSurveyor(DesignDocManageButton[0]);
                if (DesignDocManageButton.Count > 0)
                {
                    int actualDesignDocCount = 0;
                    if (DesignDocPages.Count > 1)
                    {
                        for (int p = 0; p < DesignDocPages.Count; p++)
                        {
                            DesignDocPages[p].Click();
                            //Thread.Sleep(500);
                            CloseSpinnerOnSurveyorDiv();
                            CloseSpinnerOnSurveyorPage();
                            //Thread.Sleep(500);
                            actualDesignDocCount += DesignDocManageButton.Count;
                            //Thread.Sleep(500);
                            CloseSpinnerOnSurveyorDiv();
                            CloseSpinnerOnSurveyorPage();
                            //Thread.Sleep(500);
                        }
                        DesignDocPages[0].Click();
                    }
                    else
                    {
                        actualDesignDocCount = DesignDocManageButton.Count;
                    }

                    for (int j = 0; j < actualDesignDocCount; j++)
                    {
                        //Thread.Sleep(500);
                        CloseSpinnerOnSurveyorDiv();
                        CloseSpinnerOnSurveyorPage();
                        //Thread.Sleep(500);
                        if (DesignDocManageButton.Count != 0)
                        {
                            //Thread.Sleep(500);
                            CloseSpinnerOnSurveyorDiv();
                            CloseSpinnerOnSurveyorPage();
                            //Thread.Sleep(500);
                            Scrollup();
                            WaitForElementonSurveyor(DesignDocManageButton[0]);
                            DesignDocManageButton[0].Click();
                            //Thread.Sleep(500);
                            CloseSpinnerOnSurveyorDiv();
                            CloseSpinnerOnSurveyorPage();
                            //Thread.Sleep(500);
                            WaitForElementonSurveyor(ManageDesignDocDiv);
                            Assert.IsTrue(ManageDesignDocDiv.Displayed);
                            //Thread.Sleep(500);
                            CloseSpinnerOnSurveyorDiv();
                            CloseSpinnerOnSurveyorPage();
                            //Thread.Sleep(500);
                            WaitForElementonSurveyor(ManagementNotesOnManageDesignDoc);
                            ManagementNotesOnManageDesignDoc.Click();
                            ManagementNotesOnManageDesignDoc.Clear();
                            ManagementNotesOnManageDesignDoc.SendKeys("Closing Design Documents");
                            //Thread.Sleep(500);
                            CloseSpinnerOnSurveyorDiv();
                            CloseSpinnerOnSurveyorPage();
                            //Thread.Sleep(500);
                            WaitForElementonSurveyor(DesignDocDocumentStatusDropdown);
                            WaitForElementToClick(DesignDocDocumentStatusDropdown);
                            DesignDocDocumentStatusDropdown.Click();
                            WaitForElementonSurveyor(DesignDocumentApproved);
                            DesignDocumentApproved.Click();
                            //Thread.Sleep(500);
                            CloseSpinnerOnSurveyorDiv();
                            CloseSpinnerOnSurveyorPage();
                            //Thread.Sleep(500);
                            WaitForElementonSurveyor(DesignDocClosingComments);
                            DesignDocClosingComments.Click();
                            DesignDocClosingComments.Clear();
                            DesignDocClosingComments.SendKeys("Closing Design Documents");
                            //Thread.Sleep(500);
                            CloseSpinnerOnSurveyorDiv();
                            CloseSpinnerOnSurveyorPage();
                            //Thread.Sleep(500);
                            WaitForElementonSurveyor(CloseDesignDocSaveButton);
                            //Thread.Sleep(500);
                            CloseDesignDocSaveButton.Click();
                            //Thread.Sleep(500);
                            CloseSpinnerOnSurveyorDiv();
                            CloseSpinnerOnSurveyorPage();
                            //Thread.Sleep(500);
                        }
                    }
                }
                //Thread.Sleep(500);
                CloseSpinnerOnSurveyorDiv();
                CloseSpinnerOnSurveyorPage();
                //Thread.Sleep(500);
                WaitForElementonSurveyor(DesignDocTableData);
                var TableDataBeforeUpload = DesignDocTableData.Text;
                ScrollIntoView(DesignDocsTab);
                Scrollup();
                WaitForElementonSurveyor(DesignDocsTab);
                //Assert.IsTrue(TableDataBeforeUpload == "No matching records found");
                DesignDocsTab.Click();
            }
        }
        public void CloseDesignItems(int i)
        {
            if (DocsTabs[i].Text.Contains("Design Items"))
            {
                WaitForElementonSurveyor(DesignItemsTab);
                WaitForElementonSurveyor(DesignItemsCount);
                //  Assert.IsTrue(DesignItemsCount.Text == "1");
                DesignItemsTab.Click();
                var TabStatusOnClick = DocsTabs[i].GetAttribute("aria-expanded");
                Assert.IsTrue(TabStatusOnClick == "true");
                //Thread.Sleep(500);
                CloseSpinnerOnSurveyorDiv();
                CloseSpinnerOnSurveyorPage();
                //Thread.Sleep(500);
                WaitForElementonSurveyor(DesignItemManageButton[0]);
                if (DesignItemManageButton.Count > 0)
                {
                    int actualDesignItemManage = 0;
                    if (DesignItemPages.Count > 1)
                    {
                        for (int p = 0; p < DesignItemPages.Count; p++)
                        {
                            DesignItemPages[p].Click();
                            //Thread.Sleep(500);
                            CloseSpinnerOnSurveyorDiv();
                            CloseSpinnerOnSurveyorPage();
                            //Thread.Sleep(500);
                            actualDesignItemManage += DesignItemManageButton.Count;
                            //Thread.Sleep(500);
                            CloseSpinnerOnSurveyorDiv();
                            CloseSpinnerOnSurveyorPage();
                            //Thread.Sleep(500);
                        }
                        DesignItemPages[0].Click();
                    }
                    else
                    {
                        actualDesignItemManage = DesignItemManageButton.Count;
                    }
                    for (int j = 0; j < actualDesignItemManage; j++)
                    {
                        WaitForLoadElements(DesignItemManageButton);
                        DesignItemManageButton[0].Click();
                        //Thread.Sleep(500);
                        CloseSpinnerOnSurveyorDiv();
                        CloseSpinnerOnSurveyorPage();
                        //Thread.Sleep(500);
                        WaitForElementonSurveyor(ManageDesignItemDiv);
                        Assert.IsTrue(ManageDesignItemDiv.Displayed);
                        //Thread.Sleep(500);
                        CloseSpinnerOnSurveyorDiv();
                        CloseSpinnerOnSurveyorPage();
                        //Thread.Sleep(500);
                        WaitForElementonSurveyor(ManagementNotesOnDesignItem);
                        ManagementNotesOnDesignItem.Click();
                        ManagementNotesOnDesignItem.Clear();
                        ManagementNotesOnDesignItem.SendKeys("Closing Design Documents");
                        //Thread.Sleep(500);
                        CloseSpinnerOnSurveyorDiv();
                        CloseSpinnerOnSurveyorPage();
                        //Thread.Sleep(500);
                        WaitForElementonSurveyor(DesignItemDocumentStatusDropdown);
                        DesignItemDocumentStatusDropdown.Click();
                        //Thread.Sleep(500);
                        CloseSpinnerOnSurveyorDiv();
                        CloseSpinnerOnSurveyorPage();
                        //Thread.Sleep(500);
                        WaitForElementonSurveyor(DesignItemClosed);
                        DesignItemClosed.Click();
                        WaitForElementonSurveyor(DesignItemClosingComments);
                        DesignItemClosingComments.Click();
                        DesignItemClosingComments.Clear();
                        DesignItemClosingComments.SendKeys("Closing Design Documents");
                        WaitForElementonSurveyor(CloseDesignItemSaveButton);
                        //Thread.Sleep(500);
                        CloseSpinnerOnSurveyorDiv();
                        CloseSpinnerOnSurveyorPage();
                        //Thread.Sleep(500);
                        CloseDesignItemSaveButton.Click();
                        //Thread.Sleep(500);
                        CloseSpinnerOnSurveyorDiv();
                        CloseSpinnerOnSurveyorPage();
                        //Thread.Sleep(500);
                    }
                }
                //Thread.Sleep(500);
                CloseSpinnerOnSurveyorDiv();
                CloseSpinnerOnSurveyorPage();
                //Thread.Sleep(500);
                WaitForElementonSurveyor(DesignItemsDocTableData);
                var TableDataBeforeUpload = DesignItemsDocTableData.Text;
                Scrollup();
                //  Assert.IsTrue(TableDataBeforeUpload == "No matching records found");
                WaitForElementonSurveyor(DesignItemsTab);
                DesignItemsTab.Click();
            }
        }
        public void CloseRiskItems(int i)
        {
            if (DocsTabs[i].Text.Contains("Risk Items"))
            {
                WaitForElementonSurveyor(RiskItemsTab);
                WaitForElementonSurveyor(RiskItemsCount);
                // Assert.IsTrue(RiskItemsCount.Text == "1");
                RiskItemsTab.Click();
                var TabStatusOnClick = DocsTabs[i].GetAttribute("aria-expanded");
                Assert.IsTrue(TabStatusOnClick == "true");
                //Thread.Sleep(500);
                CloseSpinnerOnSurveyorDiv();
                CloseSpinnerOnSurveyorPage();
                //Thread.Sleep(500);
                WaitForElementonSurveyor(RiskItemManageButton[0]);
                if (RiskItemManageButton.Count > 0)
                {
                    int actualRiskItemManage = 0;
                    if (RiskItemPages.Count > 1)
                    {
                        for (int p = 0; p < RiskItemPages.Count; p++)
                        {
                            RiskItemPages[p].Click();
                            //Thread.Sleep(500);
                            CloseSpinnerOnSurveyorDiv();
                            CloseSpinnerOnSurveyorPage();
                            //Thread.Sleep(500);
                            actualRiskItemManage += RiskItemManageButton.Count;
                            //Thread.Sleep(500);
                            CloseSpinnerOnSurveyorDiv();
                            CloseSpinnerOnSurveyorPage();
                            //Thread.Sleep(500);
                        }
                        RiskItemPages[0].Click();
                    }
                    else
                    {
                        actualRiskItemManage = RiskItemManageButton.Count;
                    }

                    for (int j = 0; j < actualRiskItemManage; j++)
                    {
                        CloseSpinnerOnSurveyorDiv();
                        CloseSpinnerOnSurveyorPage();
                        Thread.Sleep(500);                        
                        WaitForElementonSurveyor(RiskItemManageButton[0]);
                        ScrollIntoView(RiskItemManageButton[0]);
                        RiskItemManageButton[0].Click();
                        //Thread.Sleep(500);
                        CloseSpinnerOnSurveyorDiv();
                        CloseSpinnerOnSurveyorPage();
                        //Thread.Sleep(500);
                        WaitForElementonSurveyor(ManageRiskItemDiv);
                        Assert.IsTrue(ManageRiskItemDiv.Displayed);
                        //Thread.Sleep(500);
                        CloseSpinnerOnSurveyorDiv();
                        CloseSpinnerOnSurveyorPage();
                        //Thread.Sleep(500);
                        WaitForElementonSurveyor(ManagementNotesOnRiskItem);
                        ManagementNotesOnRiskItem.Click();
                        ManagementNotesOnRiskItem.Clear();
                        ManagementNotesOnRiskItem.SendKeys("Closing Risk Items");
                        //Thread.Sleep(500);
                        CloseSpinnerOnSurveyorDiv();
                        CloseSpinnerOnSurveyorPage();
                        //Thread.Sleep(500);
                        WaitForElementonSurveyor(RiskItemDocumentStatusDropdown);
                        RiskItemDocumentStatusDropdown.Click();
                        WaitForElementonSurveyor(RiskItemClosed);
                        RiskItemClosed.Click();
                        WaitForElementonSurveyor(RiskItemClosingComments);
                        RiskItemClosingComments.Click();
                        RiskItemClosingComments.Clear();
                        RiskItemClosingComments.SendKeys("Closing Risk Items");
                        WaitForElementonSurveyor(CloseRiskItemSaveButton);
                        CloseRiskItemSaveButton.Click();
                        Thread.Sleep(500);
                        CloseSpinnerOnSurveyorDiv();
                        CloseSpinnerOnSurveyorPage();
                        Thread.Sleep(500);
                    }
                }
                WaitForElementonSurveyor(RiskItemsDocTableData);
                var TableDataBeforeUpload = RiskItemsDocTableData.Text;
                Scrollup();
                //Assert.IsTrue(TableDataBeforeUpload == "No matching records found");
                WaitForElementonSurveyor(RiskItemsTab);
                RiskItemsTab.Click();
            }
        }
        public void CloseCompletionDocs(int i)
        {
            if (DocsTabs[i].Text.Contains("Completion Documents"))
            {
                WaitForElementonSurveyor(CompletionDocsTab);
                WaitForElementonSurveyor(CompletionDocsCount);
                // Assert.IsTrue(CompletionDocsCount.Text == "1");
                CompletionDocsTab.Click();
                var TabStatusOnClick = DocsTabs[i].GetAttribute("aria-expanded");
                Assert.IsTrue(TabStatusOnClick == "true");
                //Thread.Sleep(500);
                CloseSpinnerOnSurveyorDiv();
                CloseSpinnerOnSurveyorPage();
                //Thread.Sleep(500);
                WaitForElementonSurveyor(CompletionDocManageButton[0]);
                if (CompletionDocManageButton.Count > 0)
                {
                    int actualCompletionDocManage = 0;
                    if (CompletionDocsPages.Count > 1)
                    {
                        for (int p = 0; p < CompletionDocsPages.Count; p++)
                        {
                            CompletionDocsPages[p].Click();
                            //Thread.Sleep(500);
                            CloseSpinnerOnSurveyorDiv();
                            CloseSpinnerOnSurveyorPage();
                            //Thread.Sleep(500);
                            actualCompletionDocManage += CompletionDocManageButton.Count;
                            //Thread.Sleep(500);
                            CloseSpinnerOnSurveyorDiv();
                            CloseSpinnerOnSurveyorPage();
                            //Thread.Sleep(500);
                        }
                        CompletionDocsPages[0].Click();
                    }
                    else
                    {
                        actualCompletionDocManage = CompletionDocManageButton.Count;
                    }

                    for (int j = 0; j < actualCompletionDocManage; j++)
                    {
                        WaitForLoadElements(CompletionDocManageButton);
                        CompletionDocManageButton[0].Click();
                        //Thread.Sleep(500);
                        CloseSpinnerOnSurveyorDiv();
                        CloseSpinnerOnSurveyorPage();
                        //Thread.Sleep(500);
                        WaitForElementonSurveyor(ManageCompletionDocDiv);
                        Assert.IsTrue(ManageCompletionDocDiv.Displayed);
                        //Thread.Sleep(500);
                        CloseSpinnerOnSurveyorDiv();
                        CloseSpinnerOnSurveyorPage();
                        //Thread.Sleep(500);
                        WaitForElementonSurveyor(ManagementNotesOnCompletionDoc);
                        ManagementNotesOnCompletionDoc.Click();
                        ManagementNotesOnCompletionDoc.Clear();
                        ManagementNotesOnCompletionDoc.SendKeys("Closing Completion Documents");
                        //Thread.Sleep(500);
                        CloseSpinnerOnSurveyorDiv();
                        CloseSpinnerOnSurveyorPage();
                        //Thread.Sleep(500);
                        WaitForElementonSurveyor(CompletionDocDocumentStatusDropdown);
                        CompletionDocDocumentStatusDropdown.Click();
                        WaitForElementonSurveyor(CompletionDocApproved);
                        CompletionDocApproved.Click();
                        WaitForElementonSurveyor(CompletionDocClosingComments);
                        CompletionDocClosingComments.Click();
                        CompletionDocClosingComments.Clear();
                        CompletionDocClosingComments.SendKeys("Closing Completion Documents");
                        WaitForElementonSurveyor(CloseCompletionDocSaveButton);
                        CloseCompletionDocSaveButton.Click();
                        //Thread.Sleep(500);
                        CloseSpinnerOnSurveyorDiv();
                        CloseSpinnerOnSurveyorPage();
                        //Thread.Sleep(500);
                    }
                }

                //Thread.Sleep(500);
                CloseSpinnerOnSurveyorDiv();
                CloseSpinnerOnSurveyorPage();
                //Thread.Sleep(500);
                WaitForElementonSurveyor(CompletionDocTableData);
                var TableDataBeforeUpload = CompletionDocTableData.Text;
                Scrollup();
                //Assert.IsTrue(TableDataBeforeUpload == "No matching records found");
                WaitForElementonSurveyor(CompletionDocsTab);
                CompletionDocsTab.Click();
            }
        }
        public void CloseDefects(int i)
        {

            if (DocsTabs[i].Text.Contains("Defects"))
            {
                WaitForElementonSurveyor(DefectsTab);
                WaitForElementonSurveyor(DefectsCount);
                // Assert.IsTrue(DefectsCount.Text == "1");
                DefectsTab.Click();
                var TabStatusOnClick = DocsTabs[i].GetAttribute("aria-expanded");
                Assert.IsTrue(TabStatusOnClick == "true");
                //Thread.Sleep(500);
                CloseSpinnerOnSurveyorDiv();
                CloseSpinnerOnSurveyorPage();
                //Thread.Sleep(500);
                WaitForElementonSurveyor(DefectsCloseButton[0]);
                if (DefectsCloseButton.Count > 0)
                {

                    for (int j = 0; j <= DefectsCloseButton.Count; j++)
                    {
                        WaitForLoadElements(DefectsCloseButton);
                        var CloseButtonStatus = DefectsCloseButton[j].GetAttribute("disabled");
                        WaitForLoadElements(DefectsCloseButton);
                        DefectsCloseButton[0].Click();
                        CloseDefects();

                    }
                }
                WaitForElementonSurveyor(DefectsTableData);
                var TableDataBeforeUpload = DefectsTableData.Text;
                Scrollup();
                WaitForElementonSurveyor(DefectsTab);
                // Assert.IsTrue(TableDataBeforeUpload == "No matching records found");
                DefectsTab.Click();
            }
        }

        public void NotClosingDocuments()
        {
            WaitForLoadElements(DocsTabs);
            if (DocsTabs.Count > 0)
            {
                for (int i = 0; i < DocsTabs.Count; i++)
                {
                    var TabStatus = DocsTabs[i].GetAttribute("aria-expanded");
                    Assert.IsTrue(TabStatus == "false");

                    NotClosingDesignDocuments(i);
                    NotClosingDesignItems(i);
                    NotClosingRiskItems(i);
                    NotClosingCompletionDocuments(i);
                    NotClosingDefectItems(i);
                }
            }
        }

        public void NotClosingDesignDocuments(int i)
        {
            if (DocsTabs[i].Text.Contains("Design Documents"))
            {
                WaitForElementonSurveyor(DesignDocsTab);
                WaitForElementonSurveyor(DesignDocsCount);
                //Assert.IsTrue(DesignDocsCount.Text == "1");
                DesignDocsTab.Click();
                var TabStatusOnClick = DocsTabs[i].GetAttribute("aria-expanded");
                Assert.IsTrue(TabStatusOnClick == "true");
                //Thread.Sleep(500);
                CloseSpinnerOnSurveyorDiv();
                CloseSpinnerOnSurveyorPage();
                //Thread.Sleep(500);
                WaitForElementonSurveyor(DesignDocManageButton[0]);
                if (DesignDocManageButton.Count > 0)
                {
                    int actualDesignDocCount = DesignDocManageButton.Count;
                    for (int j = 0; j < actualDesignDocCount; j++)
                    {
                        WaitForLoadElements(DesignDocManageButton);
                        DesignDocManageButton[0].Click();
                        //Thread.Sleep(500);
                        CloseSpinnerOnSurveyorDiv();
                        CloseSpinnerOnSurveyorPage();
                        //Thread.Sleep(500);
                        WaitForElementonSurveyor(ManageDesignDocDiv);
                        Assert.IsTrue(ManageDesignDocDiv.Displayed);
                        string raisedDocPartOf = RaisedDocPartOf.Text;
                        string output = raisedDocPartOf.ToLower();
                        string RaisedAsPartOf = $"Cannot change the status of this technical condition since it was raised as part of a previous {output}";
                        var title = NotClosingDesignDocTitle.Text;
                        Assert.IsTrue(title.Contains(RaisedAsPartOf));
                        //Thread.Sleep(500);
                        CloseSpinnerOnSurveyorDiv();
                        CloseSpinnerOnSurveyorPage();
                        //Thread.Sleep(500);
                        WaitForElementonSurveyor(NotClosingDesignDocOkButton);
                        NotClosingDesignDocOkButton.Click();
                        //Thread.Sleep(500);
                        CloseSpinnerOnSurveyorDiv();
                        CloseSpinnerOnSurveyorPage();
                        //Thread.Sleep(500);
                    }
                }
                //Thread.Sleep(500);
                CloseSpinnerOnSurveyorDiv();
                CloseSpinnerOnSurveyorPage();
                //Thread.Sleep(500);
                WaitForElementonSurveyor(DesignDocTableData);
                WaitForElementonSurveyor(DesignDocsCount);
                Thread.Sleep(500);
                CloseSpinnerOnSurveyorPage();
                WaitForElementonSurveyor(DesignDocsTab);
                //  Assert.IsTrue(DesignDocsCount.Text == "1"); 
                DesignDocsTab.Click();
            }
        }

        public void NotClosingCompletionDocuments(int i)
        {

            if (DocsTabs[i].Text.Contains("Completion Documents"))
            {
                WaitForElementonSurveyor(CompletionDocsTab);
                WaitForElementonSurveyor(CompletionDocsCount);
                // Assert.IsTrue(CompletionDocsCount.Text == "1");
                CompletionDocsTab.Click();
                var TabStatusOnClick = DocsTabs[i].GetAttribute("aria-expanded");
                Assert.IsTrue(TabStatusOnClick == "true");
                //Thread.Sleep(500);
                CloseSpinnerOnSurveyorDiv();
                CloseSpinnerOnSurveyorPage();
                //Thread.Sleep(500);
                WaitForElementonSurveyor(CompletionDocManageButton[0]);
                if (CompletionDocManageButton.Count > 0)
                {
                    int actualCompletionDocCount = CompletionDocManageButton.Count;
                    for (int j = 0; j < actualCompletionDocCount; j++)
                    {
                        WaitForLoadElements(CompletionDocManageButton);
                        CompletionDocManageButton[0].Click();
                        //Thread.Sleep(500);
                        CloseSpinnerOnSurveyorDiv();
                        CloseSpinnerOnSurveyorPage();
                        //Thread.Sleep(500);
                        WaitForElementonSurveyor(ManageCompletionDocDiv);
                        Assert.IsTrue(ManageCompletionDocDiv.Displayed);
                        string raisedDocPartOf = RaisedDocPartOf.Text;
                        string output = raisedDocPartOf.ToLower();
                        string RaisedAsPartOf = $"Cannot change the status of this technical condition since it was raised as part of a previous {output}";
                        var title = NotClosingCompletionDocTitle.Text;
                        Assert.IsTrue(title.Contains(RaisedAsPartOf));
                        //Thread.Sleep(500);
                        CloseSpinnerOnSurveyorDiv();
                        CloseSpinnerOnSurveyorPage();
                        //Thread.Sleep(500);
                        WaitForElementonSurveyor(NotClosingCompletionDocOkButton);
                        NotClosingCompletionDocOkButton.Click();
                        //Thread.Sleep(500);
                        CloseSpinnerOnSurveyorDiv();
                        CloseSpinnerOnSurveyorPage();
                        //Thread.Sleep(500);
                    }
                }
                WaitForElementonSurveyor(CompletionDocTableData);
                WaitForElementonSurveyor(CompletionDocsCount);
                Thread.Sleep(500);
                CloseSpinnerOnSurveyorPage();
                //Assert.IsTrue(CompletionDocsCount.Text == "1");
                WaitForElementonSurveyor(CompletionDocsTab);
                CompletionDocsTab.Click();
            }
        }

        public void NotClosingDesignItems(int i)
        {
            if (DocsTabs[i].Text.Contains("Design Items"))
            {
                WaitForElementonSurveyor(DesignItemsTab);
                WaitForElementonSurveyor(DesignItemsCount);
                //  Assert.IsTrue(DesignItemsCount.Text == "1");
                DesignItemsTab.Click();
                var TabStatusOnClick = DocsTabs[i].GetAttribute("aria-expanded");
                Assert.IsTrue(TabStatusOnClick == "true");
                //Thread.Sleep(500);
                CloseSpinnerOnSurveyorDiv();
                CloseSpinnerOnSurveyorPage();
                Thread.Sleep(500);
                WaitForElementonSurveyor(DesignItemManageButton[0]);
                if (DesignItemManageButton.Count > 0)
                {
                    int actualDesignItemCount = DesignItemManageButton.Count;
                    for (int j = 0; j < actualDesignItemCount; j++)
                    {
                        WaitForLoadElements(DesignItemManageButton);
                        DesignItemManageButton[0].Click();
                        //Thread.Sleep(500);
                        CloseSpinnerOnSurveyorDiv();
                        CloseSpinnerOnSurveyorPage();
                        //Thread.Sleep(500);
                        WaitForElementonSurveyor(ManageDesignItemDiv);
                        Assert.IsTrue(ManageDesignItemDiv.Displayed);
                        string raisedDocPartOf = RaisedDocPartOf.Text;
                        string output = raisedDocPartOf.ToLower();
                        string RaisedAsPartOf = $"Cannot change the status of this technical condition since it was raised as part of a previous {output}";
                        var title = NotClosingDesignItemTitle.Text;
                        Assert.IsTrue(title.Contains(RaisedAsPartOf));
                        //Thread.Sleep(500);
                        CloseSpinnerOnSurveyorDiv();
                        CloseSpinnerOnSurveyorPage();
                        //Thread.Sleep(500);
                        WaitForElementonSurveyor(NotClosingDesignItemOkButton);
                        NotClosingDesignItemOkButton.Click();
                        //Thread.Sleep(500);
                        CloseSpinnerOnSurveyorDiv();
                        CloseSpinnerOnSurveyorPage();
                        Thread.Sleep(500);
                    }
                }
                WaitForElementonSurveyor(DesignItemsDocTableData);
                WaitForElementonSurveyor(DesignItemsCount);
                Thread.Sleep(500);
                CloseSpinnerOnSurveyorPage();
                //   Assert.IsTrue(DesignItemsCount.Text == "1");
                WaitForElementonSurveyor(DesignItemsTab);
                DesignItemsTab.Click();
            }
        }

        public void NotClosingRiskItems(int i)
        {
            if (DocsTabs[i].Text.Contains("Risk Items"))
            {
                WaitForElementonSurveyor(RiskItemsTab);
                WaitForElementonSurveyor(RiskItemsCount);
                //  Assert.IsTrue(RiskItemsCount.Text == "1");
                RiskItemsTab.Click();
                var TabStatusOnClick = DocsTabs[i].GetAttribute("aria-expanded");
                Assert.IsTrue(TabStatusOnClick == "true");
                //Thread.Sleep(500);
                CloseSpinnerOnSurveyorDiv();
                CloseSpinnerOnSurveyorPage();
                //Thread.Sleep(500);
                WaitForElementonSurveyor(RiskItemManageButton[0]);
                if (RiskItemManageButton.Count > 0)
                {
                    int actualRiskItemCount = RiskItemManageButton.Count;
                    for (int j = 0; j < actualRiskItemCount; j++)
                    {
                        WaitForLoadElements(RiskItemManageButton);
                        RiskItemManageButton[0].Click();
                        //Thread.Sleep(500);
                        CloseSpinnerOnSurveyorDiv();
                        CloseSpinnerOnSurveyorPage();
                        //Thread.Sleep(500);
                        WaitForElementonSurveyor(ManageRiskItemDiv);
                        Assert.IsTrue(ManageRiskItemDiv.Displayed);
                        string raisedDocPartOf = RaisedDocPartOf.Text;
                        string output = raisedDocPartOf.ToLower();
                        string RaisedAsPartOf = $"Cannot change the status of this technical condition since it was raised as part of a previous {output}";
                        var title = NotClosingRiskItemTitle.Text;
                        Assert.IsTrue(title.Contains(RaisedAsPartOf));
                        //Thread.Sleep(500);
                        CloseSpinnerOnSurveyorDiv();
                        CloseSpinnerOnSurveyorPage();
                        //Thread.Sleep(500);
                        WaitForElementonSurveyor(NotClosingRiskItemOkButton);
                        NotClosingRiskItemOkButton.Click();
                        //Thread.Sleep(500);
                        CloseSpinnerOnSurveyorDiv();
                        CloseSpinnerOnSurveyorPage();
                        Thread.Sleep(500);
                    }
                }
                WaitForElementonSurveyor(RiskItemsDocTableData);
                WaitForElementonSurveyor(RiskItemsCount);
                Thread.Sleep(500);
                CloseSpinnerOnSurveyorPage();
                //  Assert.IsTrue(RiskItemsCount.Text == "1");
                WaitForElementonSurveyor(RiskItemsTab);
                RiskItemsTab.Click();
            }
        }
        public void NotClosingDefectItems(int i)
        {
            if (DocsTabs[i].Text.Contains("Defects"))
            {
                WaitForElementonSurveyor(DefectsTab);
                WaitForElementonSurveyor(DefectsCount);
                //  Assert.IsTrue(DefectsCount.Text == "1");
                DefectsTab.Click();
                var TabStatusOnClick = DocsTabs[i].GetAttribute("aria-expanded");
                Assert.IsTrue(TabStatusOnClick == "true");
                //Thread.Sleep(500);
                CloseSpinnerOnSurveyorDiv();
                CloseSpinnerOnSurveyorPage();
                //Thread.Sleep(500);
                if (DefectsCloseButton.Count > 0)
                {
                    WaitForLoadElements(DefectsCloseButton);
                    var CloseButtonStatus = DefectsCloseButton[0].GetAttribute("disabled");
                    if (CloseButtonStatus != null || CloseButtonStatus == "true")
                    {
                        string raisedDocPartOf = RaisedDocPartOf.Text;
                        string output = raisedDocPartOf.ToLower();
                        string RaisedAsPartOf = $"Cannot close since this doc was raised during a previous {output}";
                        var CloseButtonTitle = DefectsCloseButton[0].GetAttribute("title");
                        Assert.IsTrue(CloseButtonTitle.Contains(RaisedAsPartOf));
                    }
                }
                WaitForElementonSurveyor(DefectsTableData);
                WaitForElementonSurveyor(DefectsCount);
                Thread.Sleep(500);
                CloseSpinnerOnSurveyorPage();
                // Assert.IsTrue(DefectsCount.Text == "1");
                WaitForElementonSurveyor(DefectsTab);
                DefectsTab.Click();
            }
        }


        public void ShowClosedDocuments()
        {
            WaitForLoadElements(DocsTabs);
            if (DocsTabs.Count > 0)
            {
                for (int i = 0; i < DocsTabs.Count; i++)
                {
                    var TabStatus = DocsTabs[i].GetAttribute("aria-expanded");
                    Assert.IsTrue(TabStatus == "false");
                    if (DocsTabs[i].Text.Contains("Design Documents"))
                    {
                        WaitForElementonSurveyor(DesignDocsTab);
                        WaitForElementonSurveyor(DesignDocsCount);
                        //Assert.IsTrue(DesignDocsCount.Text == "0");
                        DesignDocsTab.Click();
                        var TabStatusOnClick = DocsTabs[i].GetAttribute("aria-expanded");
                        Assert.IsTrue(TabStatusOnClick == "true");
                        WaitForElementonSurveyor(DesignDocTableData);
                        var TableDataBeforeUpload = DesignDocTableData.Text;
                        Assert.IsTrue(TableDataBeforeUpload == "No data available in table");
                        //Thread.Sleep(500);
                        CloseSpinnerOnSurveyorDiv();
                        CloseSpinnerOnSurveyorPage();
                        //Thread.Sleep(500);
                        if (ShowClosedCheckBox.Count > 0)
                        {
                            //Selecting ShowClosed Checkbox to verify closed Documents to be appear
                            WaitForLoadElements(ShowClosedCheckBox);
                            WaitForElementonSurveyor(ShowClosedCheckBox[i]);
                            //Thread.Sleep(500);
                            CloseSpinnerOnSurveyorDiv();
                            CloseSpinnerOnSurveyorPage();
                            //Thread.Sleep(500);
                            ShowClosedCheckBox[i].Click();
                            //Thread.Sleep(500);
                            CloseSpinnerOnSurveyorDiv();
                            CloseSpinnerOnSurveyorPage();
                            //Thread.Sleep(500);
                            WaitForLoadElements(ShowClosedDocs);
                            //Assert.IsTrue(ShowClosedDocs.Count == i);
                            //Thread.Sleep(500);
                            CloseSpinnerOnSurveyorDiv();
                            CloseSpinnerOnSurveyorPage();
                            //Thread.Sleep(500);
                            //Deseelcted ShowClosed Checkbox to verify closed Documents should be hidden
                            WaitForLoadElements(ShowClosedCheckBox);
                            ShowClosedCheckBox[i].Click();
                            //Thread.Sleep(500);
                            CloseSpinnerOnSurveyorDiv();
                            CloseSpinnerOnSurveyorPage();
                            //Thread.Sleep(500);
                            WaitForElementonSurveyor(DesignDocTableData);
                            var TableDataAfterClosed = DesignDocTableData.Text;
                            Assert.IsTrue(TableDataAfterClosed == "No data available in table");
                            //Thread.Sleep(500);
                            CloseSpinnerOnSurveyorDiv();
                            CloseSpinnerOnSurveyorPage();
                            //Thread.Sleep(500);
                            WaitForElementonSurveyor(DesignDocsTab);
                            DesignDocsTab.Click();
                            //Thread.Sleep(500);
                            CloseSpinnerOnSurveyorDiv();
                            CloseSpinnerOnSurveyorPage();
                            Thread.Sleep(500);
                        }
                    }


                    if (DocsTabs[i].Text.Contains("Design Items"))
                    {
                        WaitForElementonSurveyor(DesignItemsTab);
                        WaitForElementonSurveyor(DesignItemsCount);
                        //Assert.IsTrue(DesignItemsCount.Text == "0");
                        DesignItemsTab.Click();
                        var TabStatusOnClick = DocsTabs[i].GetAttribute("aria-expanded");
                        Assert.IsTrue(TabStatusOnClick == "true");
                        WaitForElementonSurveyor(DesignItemsDocTableData);
                        var TableDataBeforeUpload = DesignItemsDocTableData.Text;
                        //Thread.Sleep(500);
                        CloseSpinnerOnSurveyorDiv();
                        CloseSpinnerOnSurveyorPage();
                        //Thread.Sleep(500);
                        // Assert.IsTrue(TableDataBeforeUpload == "No data available in table");
                        if (ShowClosedCheckBox.Count > 0)
                        {
                            //Selecting ShowClosed Checkbox to verify closed Documents to be appear
                            WaitForLoadElements(ShowClosedCheckBox);
                            WaitForElementonSurveyor(ShowClosedCheckBox[i]);
                            //Thread.Sleep(500);
                            CloseSpinnerOnSurveyorDiv();
                            CloseSpinnerOnSurveyorPage();
                            //Thread.Sleep(500);
                            ShowClosedCheckBox[i].Click();
                            //Thread.Sleep(500);
                            CloseSpinnerOnSurveyorDiv();
                            CloseSpinnerOnSurveyorPage();
                            //Thread.Sleep(500);
                            WaitForLoadElements(ShowClosedDocs);
                            //Assert.IsTrue(ShowClosedDocs.Count == i + 1);
                            //Thread.Sleep(500);
                            CloseSpinnerOnSurveyorDiv();
                            CloseSpinnerOnSurveyorPage();
                            //Thread.Sleep(500);
                            //Deseelcted ShowClosed Checkbox to verify closed Documents should be hidden
                            WaitForLoadElements(ShowClosedCheckBox);
                            ShowClosedCheckBox[i].Click();
                            //Thread.Sleep(500);
                            CloseSpinnerOnSurveyorDiv();
                            CloseSpinnerOnSurveyorPage();
                            //Thread.Sleep(500);
                            //Assert.IsTrue(ShowClosedDocs.Count == i);
                            WaitForElementonSurveyor(DesignItemsDocTableData);
                            var TableDataAfterClosed = DesignItemsDocTableData.Text;
                            // Assert.IsTrue(TableDataAfterClosed == "No data available in table");
                            //Thread.Sleep(500);
                            CloseSpinnerOnSurveyorDiv();
                            CloseSpinnerOnSurveyorPage();
                            //Thread.Sleep(500);
                            WaitForElementonSurveyor(DesignItemsTab);
                            DesignItemsTab.Click();
                            //Thread.Sleep(500);
                            CloseSpinnerOnSurveyorDiv();
                            CloseSpinnerOnSurveyorPage();
                            Thread.Sleep(500);
                        }
                    }

                    if (DocsTabs[i].Text.Contains("Risk Items"))
                    {
                        WaitForElementonSurveyor(RiskItemsTab);
                        WaitForElementonSurveyor(RiskItemsCount);
                        // Assert.IsTrue(RiskItemsCount.Text == "0");
                        RiskItemsTab.Click();
                        var TabStatusOnClick = DocsTabs[i].GetAttribute("aria-expanded");
                        Assert.IsTrue(TabStatusOnClick == "true");
                        WaitForElementonSurveyor(RiskItemsDocTableData);
                        var TableDataBeforeUpload = RiskItemsDocTableData.Text;
                        //Thread.Sleep(500);
                        CloseSpinnerOnSurveyorDiv();
                        CloseSpinnerOnSurveyorPage();
                        //Thread.Sleep(500);
                        // Assert.IsTrue(TableDataBeforeUpload == "No data available in table");
                        if (ShowClosedCheckBox.Count > 0)
                        {
                            //Selecting ShowClosed Checkbox to verify closed Documents to be appear
                            WaitForLoadElements(ShowClosedCheckBox);
                            WaitForElementonSurveyor(ShowClosedCheckBox[i]);
                            //Thread.Sleep(500);
                            CloseSpinnerOnSurveyorDiv();
                            CloseSpinnerOnSurveyorPage();
                            //Thread.Sleep(500);
                            ShowClosedCheckBox[i].Click();
                            //Thread.Sleep(500);
                            CloseSpinnerOnSurveyorDiv();
                            CloseSpinnerOnSurveyorPage();
                            //Thread.Sleep(500);
                            WaitForLoadElements(ShowClosedDocs);
                            // Assert.IsTrue(ShowClosedDocs.Count == i + 1);
                            //Thread.Sleep(500);
                            CloseSpinnerOnSurveyorDiv();
                            CloseSpinnerOnSurveyorPage();
                            //Thread.Sleep(500);
                            //Deseelcted ShowClosed Checkbox to verify closed Documents should be hidden
                            WaitForLoadElements(ShowClosedCheckBox);
                            ShowClosedCheckBox[i].Click();
                            //Thread.Sleep(500);
                            CloseSpinnerOnSurveyorDiv();
                            CloseSpinnerOnSurveyorPage();
                            //Thread.Sleep(500);
                            WaitForElementonSurveyor(RiskItemsDocTableData);
                            //Assert.IsTrue(ShowClosedDocs.Count == i);
                            var TableDataAfterClosed = RiskItemsDocTableData.Text;
                            // Assert.IsTrue(TableDataAfterClosed == "No data available in table");
                            //Thread.Sleep(500);
                            CloseSpinnerOnSurveyorDiv();
                            CloseSpinnerOnSurveyorPage();
                            //Thread.Sleep(500);
                            WaitForElementonSurveyor(RiskItemsTab);
                            RiskItemsTab.Click();
                            //Thread.Sleep(500);
                            CloseSpinnerOnSurveyorDiv();
                            CloseSpinnerOnSurveyorPage();
                            Thread.Sleep(500);
                        }

                    }
                    if (DocsTabs[i].Text.Contains("Completion Documents"))
                    {
                        WaitForElementonSurveyor(CompletionDocsTab);
                        WaitForElementonSurveyor(CompletionDocsCount);
                        //Assert.IsTrue(CompletionDocsCount.Text == "0");
                        CompletionDocsTab.Click();
                        var TabStatusOnClick = DocsTabs[i].GetAttribute("aria-expanded");
                        Assert.IsTrue(TabStatusOnClick == "true");
                        WaitForElementonSurveyor(CompletionDocTableData);
                        var TableDataBeforeUpload = CompletionDocTableData.Text;
                        //Thread.Sleep(500);
                        CloseSpinnerOnSurveyorDiv();
                        CloseSpinnerOnSurveyorPage();
                        //Thread.Sleep(500);
                        Assert.IsTrue(TableDataBeforeUpload == "No data available in table");
                        if (ShowClosedCheckBox.Count > 0)
                        {
                            //Selecting ShowClosed Checkbox to verify closed Documents to be appear
                            WaitForLoadElements(ShowClosedCheckBox);
                            WaitForElementonSurveyor(ShowClosedCheckBox[i]);
                            //Thread.Sleep(500);
                            CloseSpinnerOnSurveyorDiv();
                            CloseSpinnerOnSurveyorPage();
                            //Thread.Sleep(500);
                            ShowClosedCheckBox[i].Click();
                            //Thread.Sleep(500);
                            CloseSpinnerOnSurveyorDiv();
                            CloseSpinnerOnSurveyorPage();
                            //Thread.Sleep(500);
                            WaitForLoadElements(ShowClosedDocs);
                            // Assert.IsTrue(ShowClosedDocs.Count == i - 1);
                            //Thread.Sleep(500);
                            CloseSpinnerOnSurveyorDiv();
                            CloseSpinnerOnSurveyorPage();
                            //Thread.Sleep(500);
                            //Deseelcted ShowClosed Checkbox to verify closed Documents should be hidden
                            WaitForLoadElements(ShowClosedCheckBox);
                            ShowClosedCheckBox[i].Click();
                            //Thread.Sleep(500);
                            CloseSpinnerOnSurveyorDiv();
                            CloseSpinnerOnSurveyorPage();
                            //Thread.Sleep(500);
                            //Assert.IsTrue(ShowClosedDocs.Count == i);
                            WaitForElementonSurveyor(CompletionDocTableData);
                            var TableDataAfterClosed = CompletionDocTableData.Text;
                            Assert.IsTrue(TableDataAfterClosed == "No data available in table");
                            //Thread.Sleep(500);
                            CloseSpinnerOnSurveyorDiv();
                            CloseSpinnerOnSurveyorPage();
                            //Thread.Sleep(500);
                            WaitForElementonSurveyor(CompletionDocsTab);
                            CompletionDocsTab.Click();
                            //Thread.Sleep(500);
                            CloseSpinnerOnSurveyorDiv();
                            CloseSpinnerOnSurveyorPage();
                            Thread.Sleep(500);
                        }

                    }
                    if (DocsTabs[i].Text.Contains("Defects"))
                    {
                        WaitForElementonSurveyor(DefectsTab);
                        WaitForElementonSurveyor(DefectsCount);
                        Assert.IsTrue(DefectsCount.Text == "0");
                        DefectsTab.Click();
                        var TabStatusOnClick = DocsTabs[i].GetAttribute("aria-expanded");
                        Assert.IsTrue(TabStatusOnClick == "true");
                        WaitForElementonSurveyor(DefectsTableData);
                        var TableDataBeforeUpload = DefectsTableData.Text;
                        Assert.IsTrue(TableDataBeforeUpload == "No data available in table");
                        //Thread.Sleep(500);
                        CloseSpinnerOnSurveyorDiv();
                        CloseSpinnerOnSurveyorPage();
                        //Thread.Sleep(500);
                        if (ShowClosedCheckBox.Count > 0)
                        {
                            //Selecting ShowClosed Checkbox to verify closed Documents to be appear
                            WaitForLoadElements(ShowClosedCheckBox);
                            WaitForElementonSurveyor(ShowClosedCheckBox[i]);
                            //Thread.Sleep(500);
                            CloseSpinnerOnSurveyorDiv();
                            CloseSpinnerOnSurveyorPage();
                            //Thread.Sleep(500);
                            ShowClosedCheckBox[i].Click();
                            //Thread.Sleep(500);
                            CloseSpinnerOnSurveyorDiv();
                            CloseSpinnerOnSurveyorPage();
                            //Thread.Sleep(500);
                            WaitForLoadElements(ShowClosedDocs);
                            // Assert.IsTrue(ShowClosedDocs.Count == i + 1);
                            //Thread.Sleep(500);
                            CloseSpinnerOnSurveyorDiv();
                            CloseSpinnerOnSurveyorPage();
                            //Thread.Sleep(500);
                            //Deseelcted ShowClosed Checkbox to verify closed Documents should be hidden
                            WaitForLoadElements(ShowClosedCheckBox);
                            ShowClosedCheckBox[i].Click();
                            //Thread.Sleep(500);
                            CloseSpinnerOnSurveyorDiv();
                            CloseSpinnerOnSurveyorPage();
                            //Thread.Sleep(500);
                            WaitForElementonSurveyor(DefectsTableData);
                            //Assert.IsTrue(ShowClosedDocs.Count == i);
                            var TableDataAfterClosed = DefectsTableData.Text;
                            Assert.IsTrue(TableDataAfterClosed == "No data available in table");
                            //Thread.Sleep(500);
                            CloseSpinnerOnSurveyorDiv();
                            CloseSpinnerOnSurveyorPage();
                            Thread.Sleep(500);
                            WaitForElementonSurveyor(DefectsTab);
                            DefectsTab.Click();
                        }

                    }
                }
            }
        }

        public void CloseDefects()
        {
            //Thread.Sleep(500);
            CloseSpinnerOnSurveyorDiv();
            CloseSpinnerOnSurveyorPage();
            //Thread.Sleep(500);
            WaitForElementonSurveyor(CloseDefectDiv);
            Assert.IsTrue(CloseDefectDiv.Displayed, "Failed to Close the Document");
            WaitForElementonSurveyor(DefectResolutionComment);
            DefectResolutionComment.Click();
            DefectResolutionComment.Clear();
            DefectResolutionComment.SendKeys("Closing Defect");
            //Thread.Sleep(500);
            CloseSpinnerOnSurveyorDiv();
            CloseSpinnerOnSurveyorPage();
            //Thread.Sleep(500);
            WaitForElementonSurveyor(ResolvedPlotsDropdown);
            ResolvedPlotsDropdown.Click();
            //Thread.Sleep(500);
            CloseSpinnerOnSurveyorDiv();
            CloseSpinnerOnSurveyorPage();
            //Thread.Sleep(500);
            WaitForElementonSurveyor(ResolvedPlotsDropdownInput);
            ResolvedPlotsDropdownInput.Click();
            //Thread.Sleep(500);
            CloseSpinnerOnSurveyorDiv();
            CloseSpinnerOnSurveyorPage();
            //Thread.Sleep(500);
            WaitForElementonSurveyor(ResolveButton);
            ResolveButton.Click();
            Thread.Sleep(500);
            CloseSpinnerOnSurveyorDiv();
            CloseSpinnerOnSurveyorPage();
            //Thread.Sleep(500);
        }

        public void AddDesignDocument()
        {
            WaitForElementonSurveyor(AddDesignDocButton);
            AddDesignDocButton.Click();
            //Thread.Sleep(500);
            CloseSpinnerOnSurveyorDiv();
            CloseSpinnerOnSurveyorPage();
            //Thread.Sleep(500);
            WaitForElementonSurveyor(AddDesignDocDiv);
            Assert.IsTrue(AddDesignDocDiv.Displayed);
            WaitForElement(DesignDocLinkSite);
            if (DesignDocLinkSite.Selected == false)
            {
                DesignDocLinkSite.Click();
            }
            WaitForLoadElements(DesignDocDropdown);
            DesignDocDropdown[0].Click();
            CloseSpinnerOnSurveyorDiv();
            CloseSpinnerOnSurveyorPage();
            WaitForLoadElements(DesignDocDropdownInput);
            if(DesignDocDropdownInput.Count>0)
            {
                WaitForElementonSurveyor(DesignDocDropdownInput[0]);
                DesignDocDropdownInput[0].Click();
                CloseSpinnerOnSurveyorDiv();
                CloseSpinnerOnSurveyorPage();
                Thread.Sleep(500);
            }
            CloseSpinnerOnSurveyorDiv();
            CloseSpinnerOnSurveyorPage();
            Thread.Sleep(500);
            WaitForElementonSurveyor(DesignDocNotesInput);
            DesignDocNotesInput.Click();
            DesignDocNotesInput.Clear();
            DesignDocNotesInput.SendKeys("Adding Design Document");
            WaitForElementonSurveyor(DesignDocAddButton);
            DesignDocAddButton.Click();
            //Thread.Sleep(500);
            CloseSpinnerOnSurveyorDiv();
            CloseSpinnerOnSurveyorPage();
            //Thread.Sleep(500);
            WaitForLoadElements(DesignDocsTableRows);
            // Assert.IsTrue(DesignDocsTableRows.Count == 1);
            if (DesignDocViewButton.Count > 0)
            {
                foreach (var eachdesigndocView in DesignDocViewButton)
                {
                    WaitForElement(eachdesigndocView);
                    eachdesigndocView.Click();
                    //Thread.Sleep(500);
                    CloseSpinnerOnSurveyorDiv();
                    CloseSpinnerOnSurveyorPage();
                    //Thread.Sleep(500);
                    WaitForElementonSurveyor(ViewDesignDocDiv);
                    Assert.IsTrue(ViewDesignDocDiv.Displayed);
                    WaitForElementonSurveyor(ViewDesignDocOkButton);
                    ViewDesignDocOkButton.Click();
                    //Thread.Sleep(500);
                    CloseSpinnerOnSurveyorDiv();
                    CloseSpinnerOnSurveyorPage();
                    Thread.Sleep(500);
                }
            }

            WaitForElementonSurveyor(DesignDocsCount);
            CloseSpinnerOnSurveyorDiv();
            CloseSpinnerOnSurveyorPage();
            Thread.Sleep(500);
            //  Assert.IsTrue(DesignDocsCount.Text == "1");
            RaisedDesignDocPartOf = DesignDocPartOf.Select(i => i.Text).ToList();
            CloseSpinnerOnSurveyorDiv();
            CloseSpinnerOnSurveyorPage();
            Thread.Sleep(500);

        }

        public void AddCompletionDocument()
        {
            WaitForElementonSurveyor(AddCompletionDocButton);
            AddCompletionDocButton.Click();
            //Thread.Sleep(500);
            CloseSpinnerOnSurveyorDiv();
            CloseSpinnerOnSurveyorPage();
            //Thread.Sleep(500);
            WaitForElementonSurveyor(AddCompletionDocDiv);
            Assert.IsTrue(AddCompletionDocDiv.Displayed);
            WaitForElement(CompletionDocLinkSite);
            if (CompletionDocLinkSite.Selected == false)
            {
                CompletionDocLinkSite.Click();
            }
            WaitForLoadElements(CompletionDocDropdown);
            CompletionDocDropdown[0].Click();
            CloseSpinnerOnSurveyorDiv();
            CloseSpinnerOnSurveyorPage();
            WaitForLoadElements(CompletionDocDropdownInput);
            if (CompletionDocDropdownInput.Count > 0)
            {
                WaitForElementonSurveyor(CompletionDocDropdownInput[0]);
                CompletionDocDropdownInput[0].Click();
                CloseSpinnerOnSurveyorDiv();
                CloseSpinnerOnSurveyorPage();
                Thread.Sleep(500);
            }

            CloseSpinnerOnSurveyorDiv();
            CloseSpinnerOnSurveyorPage();
            Thread.Sleep(500);
            WaitForElementonSurveyor(CompletionDocNotesInput);
            CompletionDocNotesInput.Click();
            CompletionDocNotesInput.Clear();
            CompletionDocNotesInput.SendKeys("Adding Completion Document");
            WaitForElementonSurveyor(CompletionDocAddButton);
            CompletionDocAddButton.Click();
            //Thread.Sleep(500);
            CloseSpinnerOnSurveyorDiv();
            CloseSpinnerOnSurveyorPage();
            //Thread.Sleep(500);
            WaitForLoadElements(CompletionDocsTableRows);
            // Assert.IsTrue(CompletionDocsTableRows.Count == 1);
            if (CompletionDocViewButton.Count > 0)
            {
                foreach (var eachcompletionndocView in CompletionDocViewButton)
                {

                    WaitForElementonSurveyor(eachcompletionndocView);
                    eachcompletionndocView.Click();
                    //Thread.Sleep(500);
                    CloseSpinnerOnSurveyorDiv();
                    CloseSpinnerOnSurveyorPage();
                    //Thread.Sleep(500);
                    WaitForElementonSurveyor(ViewCompletionDocDiv);
                    Assert.IsTrue(ViewCompletionDocDiv.Displayed);
                    WaitForElementonSurveyor(ViewCompletionDocOkButton);
                    ViewCompletionDocOkButton.Click();
                    //Thread.Sleep(500);
                    CloseSpinnerOnSurveyorDiv();
                    CloseSpinnerOnSurveyorPage();
                    Thread.Sleep(500);

                }
            }
            WaitForElementonSurveyor(CompletionDocsCount);
            CloseSpinnerOnSurveyorDiv();
            CloseSpinnerOnSurveyorPage();
            Thread.Sleep(500);
            // Assert.IsTrue(CompletionDocsCount.Text == "1");
            RaisedCompletionDocPartOf = CompletionDocPartOf.Select(i => i.Text).ToList();
            CloseSpinnerOnSurveyorDiv();
            CloseSpinnerOnSurveyorPage();
            Thread.Sleep(500);
        }

        public void AddDesignItems()
        {
            WaitForElementonSurveyor(AddDesignItemsDocButton);
            AddDesignItemsDocButton.Click();
            //Thread.Sleep(500);
            CloseSpinnerOnSurveyorDiv();
            CloseSpinnerOnSurveyorPage();
            //Thread.Sleep(500);
            WaitForElementonSurveyor(AddDesignItemsDocDiv);
            Assert.IsTrue(AddDesignItemsDocDiv.Displayed);
            WaitForElement(DesignItemLinkSite);
            if (DesignItemLinkSite.Selected == false)
            {
                DesignItemLinkSite.Click();
            }
            WaitForLoadElements(DesignItemsDocDropdown);
            DesignItemsDocDropdown[0].Click();
            CloseSpinnerOnSurveyorDiv();
            CloseSpinnerOnSurveyorPage();
            WaitForLoadElements(DesignItemsDocDropdownInput);
            if (DesignItemsDocDropdownInput.Count > 0)
            {
                WaitForElementonSurveyor(DesignItemsDocDropdownInput[0]);
                DesignItemsDocDropdownInput[0].Click();
                CloseSpinnerOnSurveyorDiv();
                CloseSpinnerOnSurveyorPage();
                Thread.Sleep(500);
            }
            //Thread.Sleep(500);
            CloseSpinnerOnSurveyorDiv();
            CloseSpinnerOnSurveyorPage();
            Thread.Sleep(500);
            WaitForElementonSurveyor(DesignItemsDescriptionInput);
            DesignItemsDescriptionInput.Click();
            DesignItemsDescriptionInput.Clear();
            DesignItemsDescriptionInput.SendKeys("Test DesignItems");
            Thread.Sleep(500);
            CloseSpinnerOnSurveyorDiv();
            CloseSpinnerOnSurveyorPage();
            //Thread.Sleep(500);
            WaitForElementonSurveyor(DesignItemsDocNotesInput);
            DesignItemsDocNotesInput.Click();
            DesignItemsDocNotesInput.Clear();
            DesignItemsDocNotesInput.SendKeys("Adding DesignItems");
            WaitForElementonSurveyor(DesignItemsDocAddButton);
            DesignItemsDocAddButton.Click();
            //Thread.Sleep(500);
            CloseSpinnerOnSurveyorDiv();
            CloseSpinnerOnSurveyorPage();
            //Thread.Sleep(500);
            WaitForLoadElements(DesignItemsDocsTableRows);
            if (DesignItemsDocViewButton.Count > 0)
            {
                foreach (var eachdesignItemView in DesignItemsDocViewButton)
                {
                    // Assert.IsTrue(DesignItemsDocsTableRows.Count == 1);
                    WaitForElementonSurveyor(eachdesignItemView);
                    eachdesignItemView.Click();
                    //Thread.Sleep(500);
                    CloseSpinnerOnSurveyorDiv();
                    CloseSpinnerOnSurveyorPage();
                    //Thread.Sleep(500);
                    WaitForElementonSurveyor(ViewDesignItemsDocDiv);
                    Assert.IsTrue(ViewDesignItemsDocDiv.Displayed);
                    WaitForElementonSurveyor(ViewDesignItemsDocOkButton);
                    ViewDesignItemsDocOkButton.Click();
                    //Thread.Sleep(500);
                    CloseSpinnerOnSurveyorDiv();
                    CloseSpinnerOnSurveyorPage();
                    Thread.Sleep(500);
                }
            }
            WaitForElementonSurveyor(DesignItemsCount);
            CloseSpinnerOnSurveyorDiv();
            CloseSpinnerOnSurveyorPage();
            Thread.Sleep(500);
            //  Assert.IsTrue(DesignItemsCount.Text == "1");
            RaisedDesignItemDocPartOf = DesignItemDocPartOf.Select(i => i.Text).ToList();
            CloseSpinnerOnSurveyorDiv();
            CloseSpinnerOnSurveyorPage();
            Thread.Sleep(500);
        }

        public void AddRiskItems()
        {
            WaitForElementonSurveyor(AddRiskItemsDocButton);
            AddRiskItemsDocButton.Click();
            //Thread.Sleep(500);
            CloseSpinnerOnSurveyorDiv();
            CloseSpinnerOnSurveyorPage();
            //Thread.Sleep(500);
            WaitForElementonSurveyor(AddRiskItemsDocDiv);
            Assert.IsTrue(AddRiskItemsDocDiv.Displayed);
            WaitForElement(RiskItemLinkSite);
            if (RiskItemLinkSite.Selected == false)
            {
                RiskItemLinkSite.Click();
            }
            WaitForLoadElements(RiskItemsDocDropdown);
            RiskItemsDocDropdown[0].Click();
            CloseSpinnerOnSurveyorDiv();
            CloseSpinnerOnSurveyorPage();
            WaitForLoadElements(RiskItemsDocDropdownInput);
            if (RiskItemsDocDropdownInput.Count > 0)
            {
                WaitForElementonSurveyor(RiskItemsDocDropdownInput[0]);
                RiskItemsDocDropdownInput[0].Click();
                CloseSpinnerOnSurveyorDiv();
                CloseSpinnerOnSurveyorPage();
            }

            //Thread.Sleep(500);
            CloseSpinnerOnSurveyorDiv();
            CloseSpinnerOnSurveyorPage();
            Thread.Sleep(500);
            WaitForElementonSurveyor(RiskItemsDescriptionInput);
            RiskItemsDescriptionInput.Click();
            RiskItemsDescriptionInput.Clear();
            RiskItemsDescriptionInput.SendKeys("Test Risk Items");
            Thread.Sleep(500);
            CloseSpinnerOnSurveyorDiv();
            CloseSpinnerOnSurveyorPage();
            //Thread.Sleep(500);
            WaitForElementonSurveyor(RiskItemsDocNotesInput);
            RiskItemsDocNotesInput.Click();
            RiskItemsDocNotesInput.Clear();
            RiskItemsDocNotesInput.SendKeys("Adding RiskItems Document");
            WaitForElementonSurveyor(RiskItemsDocAddButton);
            RiskItemsDocAddButton.Click();
            //Thread.Sleep(500);
            CloseSpinnerOnSurveyorDiv();
            CloseSpinnerOnSurveyorPage();
            //Thread.Sleep(500);
            WaitForLoadElements(RiskItemsDocsTableRows);
            // Assert.IsTrue(RiskItemsDocsTableRows.Count == 1);
            if (RiskItemsDocViewButton.Count > 0)
            {
                foreach (var eachriskItemView in RiskItemsDocViewButton)
                {
                    WaitForElementonSurveyor(eachriskItemView);
                    eachriskItemView.Click();
                    //Thread.Sleep(500);
                    CloseSpinnerOnSurveyorDiv();
                    CloseSpinnerOnSurveyorPage();
                    //Thread.Sleep(500);
                    WaitForElementonSurveyor(ViewRiskItemsDocDiv);
                    Assert.IsTrue(ViewRiskItemsDocDiv.Displayed);
                    WaitForElementonSurveyor(ViewRiskItemsDocOkButton);
                    ViewRiskItemsDocOkButton.Click();
                    //Thread.Sleep(500);
                    CloseSpinnerOnSurveyorDiv();
                    CloseSpinnerOnSurveyorPage();
                    Thread.Sleep(500);
                }
            }
            WaitForElementonSurveyor(RiskItemsCount);
            CloseSpinnerOnSurveyorDiv();
            CloseSpinnerOnSurveyorPage();
            Thread.Sleep(500);
            //  Assert.IsTrue(RiskItemsCount.Text == "1");
            RaisedRiskItemDocPartOf = RiskItemDocPartOf.Select(i => i.Text).ToList();
            CloseSpinnerOnSurveyorDiv();
            CloseSpinnerOnSurveyorPage();
            Thread.Sleep(500);
        }

        public void AddDefects()
        {
            WaitForElementonSurveyor(AddDefectsButton);
            AddDefectsButton.Click();
            //Thread.Sleep(500);
            CloseSpinnerOnSurveyorDiv();
            CloseSpinnerOnSurveyorPage();
            //Thread.Sleep(500);
            WaitForElementonSurveyor(CreateDefectsDiv);
            Assert.IsTrue(CreateDefectsDiv.Displayed);
            WaitForElementonSurveyor(HousePartDropdown);
            HousePartDropdown.Click();
            //Thread.Sleep(500);
            CloseSpinnerOnSurveyorDiv();
            CloseSpinnerOnSurveyorPage();
            //Thread.Sleep(500);
            WaitForLoadElements(HousePartDropdownInput);
            HousePartDropdownInput[0].Click();
            //Thread.Sleep(500);
            CloseSpinnerOnSurveyorDiv();
            CloseSpinnerOnSurveyorPage();
            //Thread.Sleep(500);
            if (PartTypeInput.Count > 0)
            {
                WaitForLoadElements(PartTypeInput);
                PartTypeInput[0].Click();
            }
            else
            {
                WaitForElementonSurveyor(PartTypeDropdown);
                PartTypeDropdown.Click();
                //Thread.Sleep(500);
                CloseSpinnerOnSurveyorDiv();
                CloseSpinnerOnSurveyorPage();
                //Thread.Sleep(500);
                WaitForLoadElements(PartTypeInput);
                PartTypeInput[0].Click();
            }
            //Thread.Sleep(500);
            CloseSpinnerOnSurveyorDiv();
            CloseSpinnerOnSurveyorPage();
            //Thread.Sleep(500);
            WaitForElementonSurveyor(DefectCauseDropdown);
            DefectCauseDropdown.Click();
            //Thread.Sleep(500);
            CloseSpinnerOnSurveyorDiv();
            CloseSpinnerOnSurveyorPage();
            //Thread.Sleep(500);
            WaitForLoadElements(DefectCauseInput);
            DefectCauseInput[0].Click();
            DefectCauseDropdown.SendKeys(Keys.Tab);
            //Thread.Sleep(500);
            CloseSpinnerOnSurveyorDiv();
            CloseSpinnerOnSurveyorPage();
            //Thread.Sleep(500);
            WaitForElementonSurveyor(PlotsDropdown);
            PlotsDropdown.Click();
            //Thread.Sleep(500);
            CloseSpinnerOnSurveyorDiv();
            CloseSpinnerOnSurveyorPage();
            //Thread.Sleep(500);
            WaitForLoadElements(PlotsInput);
            PlotsInput[1].Click();
            PlotsDropdown.SendKeys(Keys.Tab);
            //Thread.Sleep(500);
            CloseSpinnerOnSurveyorDiv();
            CloseSpinnerOnSurveyorPage();
            //Thread.Sleep(500);
            WaitForElementonSurveyor(ProductsDropdown);
            ProductsDropdown.Click();
            //Thread.Sleep(500);
            CloseSpinnerOnSurveyorDiv();
            CloseSpinnerOnSurveyorPage();
            //Thread.Sleep(500);
            WaitForLoadElements(ProductsInput);
            ProductsInput[0].Click();
            ProductsDropdown.SendKeys(Keys.Tab);
            WaitForElementonSurveyor(DefectsDetailsInput);
            DefectsDetailsInput.Click();
            DefectsDetailsInput.Clear();
            DefectsDetailsInput.SendKeys("Test Defects");
            //Thread.Sleep(500);
            CloseSpinnerOnSurveyorDiv();
            CloseSpinnerOnSurveyorPage();
            //Thread.Sleep(500);
            WaitForElementonSurveyor(DefectsCommentsInput);
            DefectsCommentsInput.Click();
            DefectsCommentsInput.Clear();
            DefectsCommentsInput.SendKeys("Creating Defects");
            WaitForElementonSurveyor(DefectsAddButton);
            DefectsAddButton.Click();
            //Thread.Sleep(500);
            CloseSpinnerOnSurveyorDiv();
            CloseSpinnerOnSurveyorPage();
            //Thread.Sleep(500);
            WaitForLoadElements(DefectsTableRows);
            //  Assert.IsTrue(DefectsTableRows.Count == 1);
            WaitForElementonSurveyor(DefectsViewButton);
            DefectsViewButton.Click();
            //Thread.Sleep(500);
            CloseSpinnerOnSurveyorDiv();
            CloseSpinnerOnSurveyorPage();
            //Thread.Sleep(500);
            WaitForElementonSurveyor(ViewDefectsDiv);
            Assert.IsTrue(ViewDefectsDiv.Displayed);
            WaitForElementonSurveyor(ViewDefectsOkButton);
            ViewDefectsOkButton.Click();
            //Thread.Sleep(500);
            CloseSpinnerOnSurveyorDiv();
            CloseSpinnerOnSurveyorPage();
            //Thread.Sleep(500);
            WaitForElementonSurveyor(DefectsCount);
            CloseSpinnerOnSurveyorDiv();
            CloseSpinnerOnSurveyorPage();
            Thread.Sleep(500);
            //Assert.IsTrue(DefectsCount.Text == "1");
            RaisedDefectsPartOf = DefectsPartOf.Select(i => i.Text).ToList();
            CloseSpinnerOnSurveyorDiv();
            CloseSpinnerOnSurveyorPage();
            Thread.Sleep(500);
        }

        public void CloseOpenDocs()
        {
            WaitForLoadElements(DocsTabs);
            if (DocsTabs.Count > 0)
            {
                for (int i = 0; i < DocsTabs.Count; i++)
                {
                    var TabStatus = DocsTabs[i].GetAttribute("aria-expanded");
                    Assert.IsTrue(TabStatus == "false");
                    int DesignDocCountValue = Convert.ToInt32(DesignDocsCount.Text);
                    if (DesignDocCountValue > 0)
                    {
                        CloseDesignDocuments(i);
                    }
                    int DesignItemValue = Convert.ToInt32(DesignItemsCount.Text);
                    if (DesignItemValue > 0)
                    {
                        CloseDesignItems(i);
                    }
                    int RiskItemValue = Convert.ToInt32(RiskItemsCount.Text);
                    if (RiskItemValue > 0)
                    {
                        CloseRiskItems(i);
                    }
                    int CompletionDocCountValue = Convert.ToInt32(CompletionDocsCount.Text);
                    if (CompletionDocCountValue > 0)
                    {
                        CloseCompletionDocs(i);
                    }
                    int DefectsValue = Convert.ToInt32(DefectsCount.Text);
                    if (DefectsValue > 0)
                    {
                        CloseDefects(i);
                    }
                }
            }
        }



        public void CloseSurveyorDocumentsMain()
        {
            WaitForLoadElements(DocsTabs);
            if (DocsTabs.Count > 0)
            {
                for (int i = 0; i < DocsTabs.Count; i++)
                {
                    var TabStatus = DocsTabs[i].GetAttribute("aria-expanded");
                    Assert.IsTrue(TabStatus == "false");
                    int DesignDocCountValue = Convert.ToInt32(DesignDocsCount.Text);
                    if (DesignDocCountValue > 0)
                    {
                        if (DocsTabs[i].Text.Contains("Design Documents"))
                        {
                            WaitForElementonSurveyor(DesignDocsTab);
                            WaitForElementonSurveyor(DesignDocsCount);
                            // Assert.IsTrue(DesignDocsCount.Text == "1");
                            DesignDocsTab.Click();
                            var TabStatusOnClick = DocsTabs[i].GetAttribute("aria-expanded");
                            Assert.IsTrue(TabStatusOnClick == "true");
                            //Thread.Sleep(500);
                            CloseSpinnerOnSurveyorDiv();
                            CloseSpinnerOnSurveyorPage();
                            //Thread.Sleep(500);
                            DesignDocCount();
                            //Thread.Sleep(500);
                            CloseSpinnerOnSurveyorDiv();
                            CloseSpinnerOnSurveyorPage();
                            //Thread.Sleep(500);
                            for (int j = 0; j < DesignDocCountValue; j++)
                            {
                                var temp = "";
                                var assessment = "";
                                IWebElement closeButton = null;
                                string raisedDocAsPartof = RaisedDesignDocPartOf[j];                             
                                temp = Assessment[0].Text;
                                assessment = temp.Replace("Edit ", "");
                                //Thread.Sleep(500);
                                CloseSpinnerOnSurveyorDiv();
                                CloseSpinnerOnSurveyorPage();
                                //Thread.Sleep(500);
                                if (string.Equals(assessment, raisedDocAsPartof))
                                {
                                    if (assessment == ("Site Risk Assessment") && raisedDocAsPartof == ("Site Risk Assessment"))
                                    {
                                        var row = $"//table[@id='technicalConditionDocumentsTableDesign']//tbody//tr[@role='row']//td[5][text()='Site Risk Assessment']//parent::tr//td//button[text()='Manage']";
                                        closeButton = Driver.FindElement(By.XPath(row));
                                        closeButton.Click();
                                        closedesigndoc();                                      
                                    }
                                    if (assessment == ("Refurbishment Assessment")  && raisedDocAsPartof == ("Refurbishment Assessment"))
                                    {
                                        var row = $"//table[@id='technicalConditionDocumentsTableDesign']//tbody//tr[@role='row']//td[5][text()='Refurbishment Assessment']//parent::tr//td//button[text()='Manage']";
                                        closeButton = Driver.FindElement(By.XPath(row));
                                        closeButton.Click();
                                        closedesigndoc();                                      
                                    }
                                    if (assessment == ("Design Review")  && raisedDocAsPartof == ("Design Review"))
                                    {
                                        var row = $"//table[@id='technicalConditionDocumentsTableDesign']//tbody//tr[@role='row']//td[5][text()='Design Review']//parent::tr//td//button[text()='Manage']";
                                        closeButton = Driver.FindElement(By.XPath(row));
                                        closeButton.Click();
                                        closedesigndoc();
                                    }
                                    if (assessment == ("Engineering Review")  && raisedDocAsPartof == ("Engineering Review"))
                                    {
                                        var row = $"//table[@id='technicalConditionDocumentsTableDesign']//tbody//tr[@role='row']//td[5][text()='Engineering Review']//parent::tr//td//button[text()='Manage']";
                                        closeButton = Driver.FindElement(By.XPath(row));
                                        closeButton.Click();
                                        closedesigndoc();
                                    }
                                }
                                //Thread.Sleep(500);
                                CloseSpinnerOnSurveyorDiv();
                                CloseSpinnerOnSurveyorPage();
                                //Thread.Sleep(500);
                            }
                            ScrollIntoView(DesignDocsTab);
                            Scrollup();
                            WaitForElementonSurveyor(DesignDocsTab);
                            //Assert.IsTrue(TableDataBeforeUpload == "No matching records found");
                            DesignDocsTab.Click();
                        }
                    }
                    //Thread.Sleep(500);
                    CloseSpinnerOnSurveyorDiv();
                    CloseSpinnerOnSurveyorPage();
                    //Thread.Sleep(500);
                    int DesignItemCountValue = Convert.ToInt32(DesignItemsCount.Text);
                    if (DesignItemCountValue > 0)
                    {
                        if (DocsTabs[i].Text.Contains("Design Items"))
                        {
                            WaitForElementonSurveyor(DesignItemsTab);
                            WaitForElementonSurveyor(DesignItemsCount);
                            //  Assert.IsTrue(DesignItemsCount.Text == "1");
                            DesignItemsTab.Click();
                            var TabStatusOnClick = DocsTabs[i].GetAttribute("aria-expanded");
                            Assert.IsTrue(TabStatusOnClick == "true");
                            //Thread.Sleep(500);
                            CloseSpinnerOnSurveyorDiv();
                            CloseSpinnerOnSurveyorPage();
                            //Thread.Sleep(500);
                            DesignItemCount();
                            //Thread.Sleep(500);
                            CloseSpinnerOnSurveyorDiv();
                            CloseSpinnerOnSurveyorPage();
                            //Thread.Sleep(500);
                            for (int j = 0; j < DesignItemCountValue; j++)
                            {
                                var temp = "";
                                var assessment = "";
                                IWebElement closeButton  = null;
                                string raisedDocAsPartof = RaisedDesignItemDocPartOf[j];
                                temp = Assessment[0].Text;
                                assessment = temp.Replace("Edit ", "");
                                //Thread.Sleep(500);
                                CloseSpinnerOnSurveyorDiv();
                                CloseSpinnerOnSurveyorPage();
                                //Thread.Sleep(500);
                                if (string.Equals(assessment, raisedDocAsPartof))
                                {
                                    if (assessment == ("Site Risk Assessment") && raisedDocAsPartof == ("Site Risk Assessment"))
                                    {
                                        var row = $"//table[@id='technicalConditionItemsTableDesign']//tbody//tr[@role='row']//td[5][text()='Site Risk Assessment']//parent::tr//td//button[text()='Manage']";
                                        closeButton = Driver.FindElement(By.XPath(row));
                                        closeButton.Click();
                                        closedesignitem();
                                    }
                                    if (assessment == ("Refurbishment Assessment") && raisedDocAsPartof == ("Refurbishment Assessment"))
                                    {
                                        var row = $"//table[@id='technicalConditionItemsTableDesign']//tbody//tr[@role='row']//td[5][text()='Refurbishment Assessment']//parent::tr//td//button[text()='Manage']";
                                        closeButton = Driver.FindElement(By.XPath(row));
                                        closeButton.Click();
                                        closedesignitem();
                                    }
                                    if (assessment == ("Design Review") && raisedDocAsPartof == ("Design Review"))
                                    {
                                        var row = $"//table[@id='technicalConditionItemsTableDesign']//tbody//tr[@role='row']//td[5][text()='Design Review']//parent::tr//td//button[text()='Manage']";
                                        closeButton = Driver.FindElement(By.XPath(row));
                                        closeButton.Click();
                                        closedesignitem();
                                    }
                                    if (assessment == ("Engineering Review") && raisedDocAsPartof == ("Engineering Review"))
                                    {
                                        var row = $"//table[@id='technicalConditionItemsTableDesign']//tbody//tr[@role='row']//td[5][text()='Engineering Review']//parent::tr//td//button[text()='Manage']";
                                        closeButton = Driver.FindElement(By.XPath(row));
                                        closeButton.Click();
                                        closedesignitem();
                                    }
                                }
                                //Thread.Sleep(500);
                                CloseSpinnerOnSurveyorDiv();
                                CloseSpinnerOnSurveyorPage();
                                //Thread.Sleep(500);
                            }
                            ScrollIntoView(DesignItemsTab);
                            Scrollup();
                            WaitForElementonSurveyor(DesignItemsTab);
                            DesignItemsTab.Click();
                        }
                    }
                    //Thread.Sleep(500);
                    CloseSpinnerOnSurveyorDiv();
                    CloseSpinnerOnSurveyorPage();
                    //Thread.Sleep(500);
                    int RiskItemCountValue = Convert.ToInt32(RiskItemsCount.Text);
                    if (RiskItemCountValue > 0)
                    {
                        if (DocsTabs[i].Text.Contains("Risk Items"))
                        {
                            WaitForElementonSurveyor(RiskItemsTab);
                            WaitForElementonSurveyor(RiskItemsCount);
                            // Assert.IsTrue(RiskItemsCount.Text == "1");
                            RiskItemsTab.Click();
                            var TabStatusOnClick = DocsTabs[i].GetAttribute("aria-expanded");
                            Assert.IsTrue(TabStatusOnClick == "true");
                            //Thread.Sleep(500);
                            CloseSpinnerOnSurveyorDiv();
                            CloseSpinnerOnSurveyorPage();
                            //Thread.Sleep(500);                           
                            RiskItemCount();
                            //Thread.Sleep(500);
                            CloseSpinnerOnSurveyorDiv();
                            CloseSpinnerOnSurveyorPage();
                            //Thread.Sleep(500);
                            for (int j = 0; j < RiskItemCountValue; j++)
                            {                                
                                var temp = "";
                                var assessment = "";
                                IWebElement closeButton = null;
                                string raisedDocAsPartof = RaisedRiskItemDocPartOf[j];
                                temp = Assessment[0].Text;
                                assessment = temp.Replace("Edit ", "");
                                //Thread.Sleep(500);
                                CloseSpinnerOnSurveyorDiv();
                                CloseSpinnerOnSurveyorPage();
                                //Thread.Sleep(500);
                                if (string.Equals(assessment, raisedDocAsPartof))
                                {
                                    if (assessment == ("Site Risk Assessment") && raisedDocAsPartof == ("Site Risk Assessment"))
                                    {
                                        var row = $"//table[@id='technicalConditionItemsTableRisk']//tbody//tr[@role='row']//td[5][text()='Site Risk Assessment']//parent::tr//td//button[text()='Manage']";
                                        closeButton = Driver.FindElement(By.XPath(row));
                                        closeButton.Click();
                                        closeriskitem();
                                    }
                                    if (assessment == ("Refurbishment Assessment") && raisedDocAsPartof == ("Refurbishment Assessment"))
                                    {
                                        var row = $"//table[@id='technicalConditionItemsTableRisk']//tbody//tr[@role='row']//td[5][text()='Refurbishment Assessment']//parent::tr//td//button[text()='Manage']";
                                        closeButton = Driver.FindElement(By.XPath(row));
                                        closeButton.Click();
                                        closeriskitem();
                                    }
                                    if (assessment == ("Design Review") && raisedDocAsPartof == ("Design Review"))
                                    {
                                        var row = $"//table[@id='technicalConditionItemsTableRisk']//tbody//tr[@role='row']//td[5][text()='Design Review']//parent::tr//td//button[text()='Manage']";
                                        closeButton = Driver.FindElement(By.XPath(row));
                                        closeButton.Click();
                                        closeriskitem();
                                    }
                                    if (assessment == ("Engineering Review") && raisedDocAsPartof == ("Engineering Review"))
                                    {
                                        var row = $"//table[@id='technicalConditionItemsTableRisk']//tbody//tr[@role='row']//td[5][text()='Engineering Review']//parent::tr//td//button[text()='Manage']";
                                        closeButton = Driver.FindElement(By.XPath(row));
                                        closeButton.Click();
                                        closeriskitem();
                                    }
                                }
                                //Thread.Sleep(500);
                                CloseSpinnerOnSurveyorDiv();
                                CloseSpinnerOnSurveyorPage();
                                //Thread.Sleep(500);
                            }
                            Scrollup();
                            //Assert.IsTrue(TableDataBeforeUpload == "No matching records found");
                            WaitForElementonSurveyor(RiskItemsTab);
                            RiskItemsTab.Click();
                        }
                    }
                    //Thread.Sleep(500);
                    CloseSpinnerOnSurveyorDiv();
                    CloseSpinnerOnSurveyorPage();
                    //Thread.Sleep(500);
                    int CompletionDocCountValue = Convert.ToInt32(CompletionDocsCount.Text);
                    if (CompletionDocCountValue > 0)
                    {
                        if (DocsTabs[i].Text.Contains("Completion Documents"))
                        {
                            WaitForElementonSurveyor(CompletionDocsTab);
                            WaitForElementonSurveyor(CompletionDocsCount);
                            // Assert.IsTrue(CompletionDocsCount.Text == "1");
                            CompletionDocsTab.Click();
                            var TabStatusOnClick = DocsTabs[i].GetAttribute("aria-expanded");
                            Assert.IsTrue(TabStatusOnClick == "true");
                            //Thread.Sleep(500);
                            CloseSpinnerOnSurveyorDiv();
                            CloseSpinnerOnSurveyorPage();
                            //Thread.Sleep(500);
                            CompletionDocCount();
                            //Thread.Sleep(500);
                            CloseSpinnerOnSurveyorDiv();
                            CloseSpinnerOnSurveyorPage();
                            //Thread.Sleep(500);
                            for (int j = 0; j < CompletionDocCountValue; j++)
                            {
                                var temp = "";
                                var assessment = "";
                                IWebElement closeButton = null;
                                string raisedDocAsPartof = RaisedCompletionDocPartOf[j];
                                temp = Assessment[0].Text;
                                assessment = temp.Replace("Edit ", "");
                                //Thread.Sleep(500);
                                CloseSpinnerOnSurveyorDiv();
                                CloseSpinnerOnSurveyorPage();
                                //Thread.Sleep(500);
                                if (string.Equals(assessment, raisedDocAsPartof))
                                {
                                    if (assessment == ("Site Risk Assessment") && raisedDocAsPartof == ("Site Risk Assessment"))
                                    {
                                        var row = $"//table[@id='technicalConditionDocumentsTableCompletion']//tbody//tr[@role='row']//td[5][text()='Site Risk Assessment']//parent::tr//td//button[text()='Manage']";
                                        closeButton = Driver.FindElement(By.XPath(row));
                                        closeButton.Click();
                                        closecompletiondoc();
                                    }
                                    if (assessment == ("Refurbishment Assessment") && raisedDocAsPartof == ("Refurbishment Assessment"))
                                    {
                                        var row = $"//table[@id='technicalConditionDocumentsTableCompletion']//tbody//tr[@role='row']//td[5][text()='Refurbishment Assessment']//parent::tr//td//button[text()='Manage']";
                                        closeButton = Driver.FindElement(By.XPath(row));
                                        closeButton.Click();
                                        closecompletiondoc();
                                    }
                                    if (assessment == ("Design Review") && raisedDocAsPartof == ("Design Review"))
                                    {
                                        var row = $"//table[@id='technicalConditionDocumentsTableCompletion']//tbody//tr[@role='row']//td[5][text()='Design Review']//parent::tr//td//button[text()='Manage']";
                                        closeButton = Driver.FindElement(By.XPath(row));
                                        closeButton.Click();
                                        closecompletiondoc();
                                    }
                                    if (assessment == ("Engineering Review") && raisedDocAsPartof == ("Engineering Review"))
                                    {
                                        var row = $"//table[@id='technicalConditionDocumentsTableCompletion']//tbody//tr[@role='row']//td[5][text()='Engineering Review']//parent::tr//td//button[text()='Manage']";
                                        closeButton = Driver.FindElement(By.XPath(row));
                                        closeButton.Click();
                                        closecompletiondoc();
                                    }
                                }
                                //Thread.Sleep(500);
                                CloseSpinnerOnSurveyorDiv();
                                CloseSpinnerOnSurveyorPage();
                                //Thread.Sleep(500);
                            }
                            Scrollup();
                            //Assert.IsTrue(TableDataBeforeUpload == "No matching records found");
                            WaitForElementonSurveyor(CompletionDocsTab);
                            CompletionDocsTab.Click();
                        }
                    }
                }
            }

        }
        public void closedesigndoc()
        {
            //Thread.Sleep(500);
            CloseSpinnerOnSurveyorDiv();
            CloseSpinnerOnSurveyorPage();
            //Thread.Sleep(500);
            WaitForElementonSurveyor(ManageDesignDocDiv);
            Assert.IsTrue(ManageDesignDocDiv.Displayed);
            //Thread.Sleep(500);
            CloseSpinnerOnSurveyorDiv();
            CloseSpinnerOnSurveyorPage();
            //Thread.Sleep(500);
            WaitForElementonSurveyor(ManagementNotesOnManageDesignDoc);
            ManagementNotesOnManageDesignDoc.Click();
            ManagementNotesOnManageDesignDoc.Clear();
            ManagementNotesOnManageDesignDoc.SendKeys("Closing Design Documents");
            //Thread.Sleep(500);
            CloseSpinnerOnSurveyorDiv();
            CloseSpinnerOnSurveyorPage();
            //Thread.Sleep(500);
            WaitForElementonSurveyor(DesignDocDocumentStatusDropdown);
            WaitForElementToClick(DesignDocDocumentStatusDropdown);
            DesignDocDocumentStatusDropdown.Click();
            WaitForElementonSurveyor(DesignDocumentApproved);
            DesignDocumentApproved.Click();
            //Thread.Sleep(500);
            CloseSpinnerOnSurveyorDiv();
            CloseSpinnerOnSurveyorPage();
            //Thread.Sleep(500);
            WaitForElementonSurveyor(DesignDocClosingComments);
            DesignDocClosingComments.Click();
            DesignDocClosingComments.Clear();
            DesignDocClosingComments.SendKeys("Closing Design Documents");
            //Thread.Sleep(500);
            CloseSpinnerOnSurveyorDiv();
            CloseSpinnerOnSurveyorPage();
            //Thread.Sleep(500);
            WaitForElementonSurveyor(CloseDesignDocSaveButton);
            //Thread.Sleep(500);
            CloseDesignDocSaveButton.Click();
            //Thread.Sleep(500);
            CloseSpinnerOnSurveyorDiv();
            CloseSpinnerOnSurveyorPage();
            //Thread.Sleep(500);

        }
        public void closedesignitem()
        {
            WaitForElementonSurveyor(ManageDesignItemDiv);
            Assert.IsTrue(ManageDesignItemDiv.Displayed);
            //Thread.Sleep(500);
            CloseSpinnerOnSurveyorDiv();
            CloseSpinnerOnSurveyorPage();
            //Thread.Sleep(500);
            WaitForElementonSurveyor(ManagementNotesOnDesignItem);
            ManagementNotesOnDesignItem.Click();
            ManagementNotesOnDesignItem.Clear();
            ManagementNotesOnDesignItem.SendKeys("Closing Design Documents");
            //Thread.Sleep(500);
            CloseSpinnerOnSurveyorDiv();
            CloseSpinnerOnSurveyorPage();
            //Thread.Sleep(500);
            WaitForElementonSurveyor(DesignItemDocumentStatusDropdown);
            DesignItemDocumentStatusDropdown.Click();
            //Thread.Sleep(500);
            CloseSpinnerOnSurveyorDiv();
            CloseSpinnerOnSurveyorPage();
            //Thread.Sleep(500);
            WaitForElementonSurveyor(DesignItemClosed);
            DesignItemClosed.Click();
            WaitForElementonSurveyor(DesignItemClosingComments);
            DesignItemClosingComments.Click();
            DesignItemClosingComments.Clear();
            DesignItemClosingComments.SendKeys("Closing Design Documents");
            WaitForElementonSurveyor(CloseDesignItemSaveButton);
            //Thread.Sleep(500);
            CloseSpinnerOnSurveyorDiv();
            CloseSpinnerOnSurveyorPage();
            //Thread.Sleep(500);
            CloseDesignItemSaveButton.Click();
            //Thread.Sleep(500);
            CloseSpinnerOnSurveyorDiv();
            CloseSpinnerOnSurveyorPage();
            //Thread.Sleep(500);
        }
        public void closeriskitem()
        {
            //Thread.Sleep(500);
            CloseSpinnerOnSurveyorDiv();
            CloseSpinnerOnSurveyorPage();
            //Thread.Sleep(500);
            WaitForElementonSurveyor(ManageRiskItemDiv);
            Assert.IsTrue(ManageRiskItemDiv.Displayed);
            //Thread.Sleep(500);
            CloseSpinnerOnSurveyorDiv();
            CloseSpinnerOnSurveyorPage();
            //Thread.Sleep(500);
            WaitForElementonSurveyor(ManagementNotesOnRiskItem);
            ManagementNotesOnRiskItem.Click();
            ManagementNotesOnRiskItem.Clear();
            ManagementNotesOnRiskItem.SendKeys("Closing Risk Items");
            //Thread.Sleep(500);
            CloseSpinnerOnSurveyorDiv();
            CloseSpinnerOnSurveyorPage();
            //Thread.Sleep(500);
            WaitForElementonSurveyor(RiskItemDocumentStatusDropdown);
            RiskItemDocumentStatusDropdown.Click();
            WaitForElementonSurveyor(RiskItemClosed);
            RiskItemClosed.Click();
            WaitForElementonSurveyor(RiskItemClosingComments);
            RiskItemClosingComments.Click();
            RiskItemClosingComments.Clear();
            RiskItemClosingComments.SendKeys("Closing Risk Items");
            WaitForElementonSurveyor(CloseRiskItemSaveButton);
            CloseRiskItemSaveButton.Click();
            //Thread.Sleep(500);
            CloseSpinnerOnSurveyorDiv();
            CloseSpinnerOnSurveyorPage();
            //Thread.Sleep(500);


        }
        public void closecompletiondoc()
        {
            //Thread.Sleep(500);
            CloseSpinnerOnSurveyorDiv();
            CloseSpinnerOnSurveyorPage();
            //Thread.Sleep(500);
            WaitForElementonSurveyor(ManageCompletionDocDiv);
            Assert.IsTrue(ManageCompletionDocDiv.Displayed);
            //Thread.Sleep(500);
            CloseSpinnerOnSurveyorDiv();
            CloseSpinnerOnSurveyorPage();
            //Thread.Sleep(500);
            WaitForElementonSurveyor(ManagementNotesOnCompletionDoc);
            ManagementNotesOnCompletionDoc.Click();
            ManagementNotesOnCompletionDoc.Clear();
            ManagementNotesOnCompletionDoc.SendKeys("Closing Completion Documents");
            //Thread.Sleep(500);
            CloseSpinnerOnSurveyorDiv();
            CloseSpinnerOnSurveyorPage();
            //Thread.Sleep(500);
            WaitForElementonSurveyor(CompletionDocDocumentStatusDropdown);
            CompletionDocDocumentStatusDropdown.Click();
            WaitForElementonSurveyor(CompletionDocApproved);
            CompletionDocApproved.Click();
            WaitForElementonSurveyor(CompletionDocClosingComments);
            CompletionDocClosingComments.Click();
            CompletionDocClosingComments.Clear();
            CompletionDocClosingComments.SendKeys("Closing Completion Documents");
            WaitForElementonSurveyor(CloseCompletionDocSaveButton);
            CloseCompletionDocSaveButton.Click();
            //Thread.Sleep(500);
            CloseSpinnerOnSurveyorDiv();
            CloseSpinnerOnSurveyorPage();
            //Thread.Sleep(500);
        }
        public void CloseSurveyorDocuments()
        {           
            WaitForLoadElements(DocsTabs);
            if (DocsTabs.Count > 0)
            {
                for (int i = 0; i < DocsTabs.Count; i++)
                {
                    var TabStatus = DocsTabs[i].GetAttribute("aria-expanded");
                    Assert.IsTrue(TabStatus == "false");
                    int DesignDocCountValue = Convert.ToInt32(DesignDocsCount.Text);
                    if (DesignDocCountValue > 0)
                    {
                        if (DocsTabs[i].Text.Contains("Design Documents"))
                        {
                            WaitForElementonSurveyor(DesignDocsTab);
                            WaitForElementonSurveyor(DesignDocsCount);
                            // Assert.IsTrue(DesignDocsCount.Text == "1");
                            DesignDocsTab.Click();
                            var TabStatusOnClick = DocsTabs[i].GetAttribute("aria-expanded");
                            Assert.IsTrue(TabStatusOnClick == "true");
                            Thread.Sleep(500);
                            CloseSpinnerOnSurveyorDiv();
                            CloseSpinnerOnSurveyorPage();
                            Thread.Sleep(500);
                            DesignDocCount();
                            Thread.Sleep(500);
                            CloseSpinnerOnSurveyorDiv();
                            CloseSpinnerOnSurveyorPage();
                            Thread.Sleep(500);
                            CloseDesignDocsMain();                         
                            Scrollup();
                            CloseSpinnerOnSurveyorDiv();
                            CloseSpinnerOnSurveyorPage();
                            Thread.Sleep(500);
                            WaitForElementonSurveyor(DesignDocsTab);                            
                            DesignDocsTab.Click();
                        }
                    }
                    //Thread.Sleep(500);
                    CloseSpinnerOnSurveyorDiv();
                    CloseSpinnerOnSurveyorPage();
                    //Thread.Sleep(500);
                    int DesignItemCountValue = Convert.ToInt32(DesignItemsCount.Text);
                    if (DesignItemCountValue > 0)
                    {
                        if (DocsTabs[i].Text.Contains("Design Items"))
                        {
                            WaitForElementonSurveyor(DesignItemsTab);
                            WaitForElementonSurveyor(DesignItemsCount);
                            //  Assert.IsTrue(DesignItemsCount.Text == "1");
                            DesignItemsTab.Click();
                            var TabStatusOnClick = DocsTabs[i].GetAttribute("aria-expanded");
                            Assert.IsTrue(TabStatusOnClick == "true");
                            //Thread.Sleep(500);
                            CloseSpinnerOnSurveyorDiv();
                            CloseSpinnerOnSurveyorPage();
                            //Thread.Sleep(500);
                            DesignItemCount();
                            //Thread.Sleep(500);
                            CloseSpinnerOnSurveyorDiv();
                            CloseSpinnerOnSurveyorPage();
                            //Thread.Sleep(500);
                            CloseDesignItemsMain();                           
                            Scrollup();
                            WaitForElementonSurveyor(DesignItemsTab);
                            DesignItemsTab.Click();
                        }
                    }
                    //Thread.Sleep(500);
                    CloseSpinnerOnSurveyorDiv();
                    CloseSpinnerOnSurveyorPage();
                    //Thread.Sleep(500);
                    int RiskItemCountValue = Convert.ToInt32(RiskItemsCount.Text);
                    if (RiskItemCountValue > 0)
                    {
                        if (DocsTabs[i].Text.Contains("Risk Items"))
                        {
                            WaitForElementonSurveyor(RiskItemsTab);
                            WaitForElementonSurveyor(RiskItemsCount);
                            // Assert.IsTrue(RiskItemsCount.Text == "1");
                            RiskItemsTab.Click();
                            var TabStatusOnClick = DocsTabs[i].GetAttribute("aria-expanded");
                            Assert.IsTrue(TabStatusOnClick == "true");
                            //Thread.Sleep(500);
                            CloseSpinnerOnSurveyorDiv();
                            CloseSpinnerOnSurveyorPage();
                            //Thread.Sleep(500);
                            RiskItemCount();
                            //Thread.Sleep(500);
                            CloseSpinnerOnSurveyorDiv();
                            CloseSpinnerOnSurveyorPage();
                            Thread.Sleep(500);
                            CloseRiskItemsMain();
                            Scrollup();
                            CloseSpinnerOnSurveyorDiv();
                            CloseSpinnerOnSurveyorPage();
                            Thread.Sleep(500);
                            WaitForElementonSurveyor(RiskItemsTab);
                            CloseSpinnerOnSurveyorDiv();
                            CloseSpinnerOnSurveyorPage();
                            Thread.Sleep(500);
                            RiskItemsTab.Click();
                            CloseSpinnerOnSurveyorDiv();
                            CloseSpinnerOnSurveyorPage();
                            Thread.Sleep(500);
                        }
                    }
                    //Thread.Sleep(500);
                    CloseSpinnerOnSurveyorDiv();
                    CloseSpinnerOnSurveyorPage();
                    //Thread.Sleep(500);
                    int CompletionDocCountValue = Convert.ToInt32(CompletionDocsCount.Text);
                    if (CompletionDocCountValue > 0)
                    {
                        if (DocsTabs[i].Text.Contains("Completion Documents"))
                        {
                            WaitForElementonSurveyor(CompletionDocsTab);
                            WaitForElementonSurveyor(CompletionDocsCount);
                            // Assert.IsTrue(CompletionDocsCount.Text == "1");
                            CompletionDocsTab.Click();
                            var TabStatusOnClick = DocsTabs[i].GetAttribute("aria-expanded");
                            Assert.IsTrue(TabStatusOnClick == "true");
                            //Thread.Sleep(500);
                            CloseSpinnerOnSurveyorDiv();
                            CloseSpinnerOnSurveyorPage();
                            //Thread.Sleep(500);
                            CompletionDocCount();
                            //Thread.Sleep(500);
                            CloseSpinnerOnSurveyorDiv();
                            CloseSpinnerOnSurveyorPage();
                            Thread.Sleep(500);
                            CloseCompletionDocMain();                            
                            Scrollup();                          
                            WaitForElementonSurveyor(CompletionDocsTab);
                            CloseSpinnerOnSurveyorDiv();
                            CloseSpinnerOnSurveyorPage();
                            Thread.Sleep(500);
                            CompletionDocsTab.Click();
                            CloseSpinnerOnSurveyorDiv();
                            CloseSpinnerOnSurveyorPage();
                            Thread.Sleep(500);
                        }
                    }
                }
            }

        }


        public void CloseDesignDocsMain()
        {
            var temp = "";
            var assessment = "";                 
            temp = Assessment[0].Text;
            assessment = temp.Replace("Edit ", "");
            //Thread.Sleep(500);
            CloseSpinnerOnSurveyorDiv();
            CloseSpinnerOnSurveyorPage();
            //Thread.Sleep(500);
            if (assessment  == "Site Risk Assessment")
            {
                if(sraDesignDocCount > 0)
                {
                   for(int j= 0; j < sraDesignDocCount; j++ )
                    {
                        WaitForElements(SRADesignDocs);
                        WaitForElementToClick(SRADesignDocs[0]);
                        SRADesignDocs[0].Click();
                        closedesigndoc();
                        //Thread.Sleep(500);
                        CloseSpinnerOnSurveyorDiv();
                        CloseSpinnerOnSurveyorPage();
                        //Thread.Sleep(500);
                    }
                }
            }
            if (assessment == "Refurbishment Assessment")
            {
                if (raDesignDocCount > 0)
                {
                    for (int j = 0; j < raDesignDocCount; j++)
                    {
                        WaitForElements(RADesignDocs);
                        RADesignDocs[0].Click();
                        closedesigndoc();
                        //Thread.Sleep(500);
                        CloseSpinnerOnSurveyorDiv();
                        CloseSpinnerOnSurveyorPage();
                        //Thread.Sleep(500);
                    }
                }
            }
            if (assessment == "Design Review")
            {
                if (drDesignDocCount > 0)
                {
                    for (int j = 0; j < drDesignDocCount; j++)
                    {
                        WaitForElements(DRDesignDocs);
                        DRDesignDocs[0].Click();
                        closedesigndoc();
                        //Thread.Sleep(500);
                        CloseSpinnerOnSurveyorDiv();
                        CloseSpinnerOnSurveyorPage();
                        //Thread.Sleep(500);
                    }
                }
            }
            if (assessment == "Engineering Review")
            {
                if (erDesignDocCount > 0)
                {
                    for (int j = 0; j < erDesignDocCount; j++)
                    {
                        WaitForElements(ERDesignDocs);
                        ERDesignDocs[0].Click();
                        closedesigndoc();
                        //Thread.Sleep(500);
                        CloseSpinnerOnSurveyorDiv();
                        CloseSpinnerOnSurveyorPage();
                        //Thread.Sleep(500);
                    }
                }
            }
        }
        public void CloseDesignItemsMain()
        {
            var temp = "";
            var assessment = "";
            temp = Assessment[0].Text;
            assessment = temp.Replace("Edit ", "");
            //Thread.Sleep(500);
            CloseSpinnerOnSurveyorDiv();
            CloseSpinnerOnSurveyorPage();
            //Thread.Sleep(500);
            if (assessment == "Site Risk Assessment")
            {
                if (sraDesignItemCount > 0)
                {
                    for (int j = 0; j < sraDesignItemCount; j++)
                    {
                        WaitForElements(SRADesignItems);
                        SRADesignItems[0].Click();
                        closedesignitem();
                        //Thread.Sleep(500);
                        CloseSpinnerOnSurveyorDiv();
                        CloseSpinnerOnSurveyorPage();
                        //Thread.Sleep(500);
                    }
                }
            }
            if (assessment == "Refurbishment Assessment")
            {
                if (raDesignItemCount > 0)
                {
                    for (int i = 0; i < raDesignItemCount; i++)
                    {
                        WaitForElements(RADesignItems);
                        RADesignItems[0].Click();
                        closedesignitem();
                        //Thread.Sleep(500);
                        CloseSpinnerOnSurveyorDiv();
                        CloseSpinnerOnSurveyorPage();
                        //Thread.Sleep(500);
                    }
                }
            }
            if (assessment == "Design Review")
            {
                if (drDesignItemCount > 0)
                {
                    for (int i = 0; i < drDesignItemCount; i++)
                    {
                        WaitForElements(DRDesignItems);
                        DRDesignItems[0].Click();
                        closedesignitem();
                        //Thread.Sleep(500);
                        CloseSpinnerOnSurveyorDiv();
                        CloseSpinnerOnSurveyorPage();
                        //Thread.Sleep(500);
                    }
                }
            }
            if (assessment == "Engineering Review")
            {
                if (erDesignItemCount > 0)
                {
                    for (int i = 0; i < erDesignItemCount; i++)
                    {
                        WaitForElements(ERDesignItems);
                        ERDesignItems[0].Click();
                        closedesignitem();
                        //Thread.Sleep(500);
                        CloseSpinnerOnSurveyorDiv();
                        CloseSpinnerOnSurveyorPage();
                        //Thread.Sleep(500);
                    }
                }
            }
        }
        public void CloseRiskItemsMain()
        {
            var temp = "";
            var assessment = "";
            temp = Assessment[0].Text;
            assessment = temp.Replace("Edit ", "");
            //Thread.Sleep(500);
            CloseSpinnerOnSurveyorDiv();
            CloseSpinnerOnSurveyorPage();
            //Thread.Sleep(500);
            if (assessment == "Site Risk Assessment")
            {
                if (sraRiskItemCount > 0)
                {
                    for (int i = 0; i < sraRiskItemCount; i++)
                    {
                        WaitForElements(SRARiskItems);
                        SRARiskItems[0].Click();
                        closeriskitem();
                        //Thread.Sleep(500);
                        CloseSpinnerOnSurveyorDiv();
                        CloseSpinnerOnSurveyorPage();
                        //Thread.Sleep(500);
                    }
                }
            }
            if (assessment == "Refurbishment Assessment")
            {
                if (raRiskItemCount > 0)
                {
                    for (int i = 0; i < raRiskItemCount; i++)
                    {
                        WaitForElements(RARiskItems);
                        RARiskItems[0].Click();
                        closeriskitem();
                        //Thread.Sleep(500);
                        CloseSpinnerOnSurveyorDiv();
                        CloseSpinnerOnSurveyorPage();
                        //Thread.Sleep(500);
                    }
                }
            }
            if (assessment == "Design Review")
            {
                if (drRiskItemCount > 0)
                {
                    for (int i = 0; i < drRiskItemCount; i++)
                    {
                        WaitForElements(DRRiskItems);
                        DRRiskItems[0].Click();
                        closeriskitem();
                        //Thread.Sleep(500);
                        CloseSpinnerOnSurveyorDiv();
                        CloseSpinnerOnSurveyorPage();
                        //Thread.Sleep(500);
                    }
                }
            }
            if (assessment == "Engineering Review")
            {
                if (erRiskItemCount > 0)
                {
                    for (int i = 0; i < erRiskItemCount; i++)
                    {
                        WaitForElements(ERRiskItems);
                        ERRiskItems[0].Click();
                        closeriskitem();
                        //Thread.Sleep(500);
                        CloseSpinnerOnSurveyorDiv();
                        CloseSpinnerOnSurveyorPage();
                        //Thread.Sleep(500);
                    }
                }
            }
        }
        public void CloseCompletionDocMain()
        {
            var temp = "";
            var assessment = "";
            temp = Assessment[0].Text;
            assessment = temp.Replace("Edit ", "");
            //Thread.Sleep(500);
            CloseSpinnerOnSurveyorDiv();
            CloseSpinnerOnSurveyorPage();
            //Thread.Sleep(500);
            if (assessment == "Site Risk Assessment")
            {
                if (sraCompletionDocCount > 0)
                {
                    for (int i = 0; i < sraCompletionDocCount; i++)
                    {
                        WaitForElements(SRACompletionDocs);
                        SRACompletionDocs[0].Click();
                        closecompletiondoc();
                        //Thread.Sleep(500);
                        CloseSpinnerOnSurveyorDiv();
                        CloseSpinnerOnSurveyorPage();
                        //Thread.Sleep(500);
                    }
                }
            }
            if (assessment == "Refurbishment Assessment")
            {
                if (raCompletionDocCount > 0)
                {
                    for (int i = 0; i < raCompletionDocCount; i++)
                    {
                        WaitForElements(RACompletionDocs);
                        RACompletionDocs[0].Click();
                        closecompletiondoc();
                        //Thread.Sleep(500);
                        CloseSpinnerOnSurveyorDiv();
                        CloseSpinnerOnSurveyorPage();
                        //Thread.Sleep(500);
                    }
                }
            }
            if (assessment == "Design Review")
            {
                if (drCompletionDocCount > 0)
                {
                    for (int i = 0; i < drCompletionDocCount; i++)
                    {
                        WaitForElements(DRCompletionDocs);
                        DRCompletionDocs[0].Click();
                        closecompletiondoc();
                        //Thread.Sleep(500);
                        CloseSpinnerOnSurveyorDiv();
                        CloseSpinnerOnSurveyorPage();
                        //Thread.Sleep(500);
                    }
                }
            }
            if (assessment == "Engineering Review")
            {
                if (erCompletionDocCount > 0)
                {
                    for (int i = 0; i < erCompletionDocCount; i++)
                    {
                        WaitForElements(ERCompletionDocs);
                        ERCompletionDocs[0].Click();
                        closecompletiondoc();
                        //Thread.Sleep(500);
                        CloseSpinnerOnSurveyorDiv();
                        CloseSpinnerOnSurveyorPage();
                        //Thread.Sleep(500);
                    }
                }
            }
        }
        public void DesignDocCount()
        {
            sraDesignDocCount = 0;
            raDesignDocCount = 0;
            drDesignDocCount = 0;
            erDesignDocCount = 0; 
            if (DesignDocPages.Count > 1)
            {    
                for (int p = 0; p < DesignDocPages.Count; p++)
                {                    
                    CloseSpinnerOnSurveyorDiv();
                    CloseSpinnerOnSurveyorPage();
                    Thread.Sleep(500);
                    WaitForElementonSurveyor(DesignDocPages[p]);
                    DesignDocPages[p].Click();                   
                    CloseSpinnerOnSurveyorDiv();
                    CloseSpinnerOnSurveyorPage();                   
                    RaisedDesignDocPartOf.AddRange(DesignDocPartOf.Select(o => o.Text).ToList());                  
                    CloseSpinnerOnSurveyorDiv();
                    CloseSpinnerOnSurveyorPage();                   
                    if(RaisedDesignDocPartOf.Count>0)
                    {
                        sraDesignDocCount += SRADesignDocs.Count;
                        raDesignDocCount += RADesignDocs.Count;
                        drDesignDocCount += DRDesignDocs.Count;
                        erDesignDocCount += ERDesignDocs.Count;
                    }
                 
                }                
                actualDesignDocCount = RaisedDesignDocPartOf.Count;
                WaitForElementonSurveyor(DesignDocPages[0]);
                DesignDocPages[0].Click();
            }
            else
            {
                RaisedDesignDocPartOf.AddRange(DesignDocPartOf.Select(o => o.Text).ToList());
                RaisedDesignDocPartOf = DesignDocPartOf.Select(o => o.Text).ToList();
                //Thread.Sleep(500);
                CloseSpinnerOnSurveyorDiv();
                CloseSpinnerOnSurveyorPage();
                //Thread.Sleep(500);
                if (RaisedDesignDocPartOf.Count > 0)
                {
                    sraDesignDocCount += SRADesignDocs.Count;
                    raDesignDocCount += RADesignDocs.Count;
                    drDesignDocCount += DRDesignDocs.Count;
                    erDesignDocCount += ERDesignDocs.Count;
                }
                actualDesignDocCount = RaisedDesignDocPartOf.Count;
            }
        }

        public void DesignItemCount()
        {
            sraDesignItemCount = 0;
            raDesignItemCount = 0;
            drDesignItemCount = 0;
            erDesignItemCount = 0;
            if (DesignItemPages.Count > 1)
            {
                for (int p = 0; p < DesignItemPages.Count; p++)
                {
                    DesignItemPages[p].Click();
                    //Thread.Sleep(500);
                    CloseSpinnerOnSurveyorDiv();
                    CloseSpinnerOnSurveyorPage();
                    //Thread.Sleep(500);
                    RaisedDesignItemDocPartOf.AddRange(DesignItemDocPartOf.Select(o => o.Text).ToList());
                    //Thread.Sleep(500);
                    CloseSpinnerOnSurveyorDiv();
                    CloseSpinnerOnSurveyorPage();
                    //Thread.Sleep(500);
                    if (DesignItemDocPartOf.Count > 0)
                    {
                        sraDesignItemCount += SRADesignItems.Count;
                        raDesignItemCount += RADesignItems.Count;
                        drDesignItemCount += DRDesignItems.Count;
                        erDesignItemCount += ERDesignItems.Count;
                    }
                }
                actualDesignItemCount = RaisedDesignItemDocPartOf.Count;
                DesignItemPages[0].Click();
            }
            else
            {
                RaisedDesignItemDocPartOf.AddRange(DesignItemDocPartOf.Select(o => o.Text).ToList());
                //Thread.Sleep(500);
                CloseSpinnerOnSurveyorDiv();
                CloseSpinnerOnSurveyorPage();
                //Thread.Sleep(500);
                if (DesignItemDocPartOf.Count > 0)
                {
                    sraDesignItemCount += SRADesignItems.Count;
                    raDesignItemCount += RADesignItems.Count;
                    drDesignItemCount += DRDesignItems.Count;
                    erDesignItemCount += ERDesignItems.Count;
                }
                actualDesignItemCount = RaisedDesignItemDocPartOf.Count;
            }
        }
        public void RiskItemCount()
        {
            sraRiskItemCount = 0;
            raRiskItemCount = 0;
            drRiskItemCount = 0;
            erRiskItemCount = 0;
            if (RiskItemPages.Count > 1)
            {
                for (int p = 0; p < RiskItemPages.Count; p++)
                {
                    RiskItemPages[p].Click();
                    //Thread.Sleep(500);
                    CloseSpinnerOnSurveyorDiv();
                    CloseSpinnerOnSurveyorPage();
                    //Thread.Sleep(500);
                    RaisedRiskItemDocPartOf.AddRange(RiskItemDocPartOf.Select(o => o.Text).ToList());
                    //Thread.Sleep(500);
                    CloseSpinnerOnSurveyorDiv();
                    CloseSpinnerOnSurveyorPage();
                    //Thread.Sleep(500);
                    if (RiskItemDocPartOf.Count > 0)
                    {
                        sraRiskItemCount += SRARiskItems.Count;
                        raRiskItemCount += RARiskItems.Count;
                        drRiskItemCount += DRRiskItems.Count;
                        erRiskItemCount += ERRiskItems.Count;
                    }
                }
                actualRiskItemCount = RaisedRiskItemDocPartOf.Count;
                RiskItemPages[0].Click();
            }
            else
            {
                RaisedRiskItemDocPartOf.AddRange(RiskItemDocPartOf.Select(o => o.Text).ToList());
                //Thread.Sleep(500);
                CloseSpinnerOnSurveyorDiv();
                CloseSpinnerOnSurveyorPage();
                //Thread.Sleep(500);
                if (RiskItemDocPartOf.Count > 0)
                {
                    sraRiskItemCount += SRARiskItems.Count;
                    raRiskItemCount += RARiskItems.Count;
                    drRiskItemCount += DRRiskItems.Count;
                    erRiskItemCount += ERRiskItems.Count;
                }
                actualRiskItemCount = RaisedRiskItemDocPartOf.Count;
            }
        }
        public void CompletionDocCount()
        {
            sraCompletionDocCount = 0;
            raCompletionDocCount = 0;
            drCompletionDocCount = 0;
            erCompletionDocCount = 0;
            if (CompletionDocsPages.Count > 1)
            {
                for (int p = 0; p < CompletionDocsPages.Count; p++)
                {
                    CompletionDocsPages[p].Click();
                    //Thread.Sleep(500);
                    CloseSpinnerOnSurveyorDiv();
                    CloseSpinnerOnSurveyorPage();
                    //Thread.Sleep(500);
                    RaisedCompletionDocPartOf.AddRange(CompletionDocPartOf.Select(o => o.Text).ToList());
                    //Thread.Sleep(500);
                    CloseSpinnerOnSurveyorDiv();
                    CloseSpinnerOnSurveyorPage();
                    //Thread.Sleep(500);
                    if (CompletionDocPartOf.Count > 0)
                    {
                        sraCompletionDocCount += SRACompletionDocs.Count;
                        raCompletionDocCount += RACompletionDocs.Count;
                        drCompletionDocCount += DRCompletionDocs.Count;
                        erCompletionDocCount += ERCompletionDocs.Count;
                    }
                }
                actualCompletionDocCount = RaisedCompletionDocPartOf.Count;
                CompletionDocsPages[0].Click();
            }
            else
            {
                RaisedCompletionDocPartOf.AddRange(CompletionDocPartOf.Select(o => o.Text).ToList());
                //Thread.Sleep(500);
                CloseSpinnerOnSurveyorDiv();
                CloseSpinnerOnSurveyorPage();
                //Thread.Sleep(500);
                if (CompletionDocPartOf.Count > 0)
                {
                    sraCompletionDocCount += SRACompletionDocs.Count;
                    raCompletionDocCount += RACompletionDocs.Count;
                    drCompletionDocCount += DRCompletionDocs.Count;
                    erCompletionDocCount += ERCompletionDocs.Count;
                }
                actualCompletionDocCount = RaisedCompletionDocPartOf.Count;
            }
        }
    }
}
