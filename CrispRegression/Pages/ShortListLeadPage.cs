﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Remote;

namespace CrispAutomation.Pages
{
    public class ShortListLeadPage : Support.Pages
    {
        public IWebDriver wdriver;
        public ShortListLeadPage(ISearchContext driver)
        {
            PageFactory.InitElements(driver, this);
            wdriver = (RemoteWebDriver)driver;
        }

        [FindsBy(How = How.XPath, Using = "//crisp-header-actions[@class='au-target']//ul[@class='dropdown-content au-target active']//li[1]//button")]
        public IWebElement ShortListOption;

        [FindsBy(How = How.XPath, Using = "//ai-dialog-container[@class='active']//crisp-dialog[@class='au-target']//div[@class='au-target modal dialog brand default modal-has-header']//crisp-header//div[@class='nav-wrapper au-target']")]
        public IList<IWebElement> DialogueContainerDiv;        

        [FindsBy(How = How.XPath, Using = "//ai-dialog-container[@class='active']//crisp-dialog[@class='au-target']//div[@class='au-target modal dialog brand default modal-has-header']")]
        public IWebElement ShortListDiv;

        [FindsBy(How = How.XPath, Using = "//crisp-footer[@class='au-target']//crisp-button[@class='au-target']//button[text()='OK'][@class='au-target waves-effect waves-light btn']")]
        public IWebElement ShortListOkButton;

        [FindsBy(How = How.XPath, Using = "//crisp-header//nav//div//crisp-header-title[@class='au-target'][1]//div[@class='title']")]
        public IWebElement LeadTitle;

        [FindsBy(How = How.XPath, Using = "//crisp-footer[@class='au-target']//crisp-button[@class='au-target']//button[text()='OK'][@class='au-target waves-effect waves-light btn']")]
        public IWebElement SalesAccountOkButton;

        [FindsBy(How = How.XPath, Using = "//crisp-card//crisp-card-header//div//span[text()='Choose Sales Account Manager']")]
        public IWebElement ChooseSalesAccountManager;

        [FindsBy(How = How.XPath, Using = "//contact-list//crisp-list//ul//li[@class='au-target collection-item']")]
        public IList<IWebElement> SalesAccountManagerList;

        
        public void ShortListMethod()
        {
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            WaitForElement(Dashboardpage.ActionsButton);
            Dashboardpage.ActionsButton.Click();
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            WaitForElement(ShortListOption);
            ShortListOption.Click();
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            if(DialogueContainerDiv.Count>0)
            {
                List<String> divTitle = DialogueContainerDiv.Select(i => i.Text.ToString()).ToList();
                CloseSpinneronDiv();
                CloseSpinneronPage();
                Thread.Sleep(500);
                if(divTitle.Any(x=>x.Contains("No Sales Account Manager")))
                {
                    CloseSpinneronDiv();
                    CloseSpinneronPage();
                    Thread.Sleep(500);
                    WaitForElement(SalesAccountOkButton);
                    SalesAccountOkButton.Click();
                    CloseSpinneronDiv();
                    CloseSpinneronPage();
                    Thread.Sleep(100);
                    WaitForElement(ChooseSalesAccountManager);
                    WaitForLoadElements(SalesAccountManagerList);
                    if(SalesAccountManagerList.Count>0)
                    {
                        SalesAccountManagerList[0].Click();
                        CloseCrispCard();
                        CloseSpinneronDiv();
                        CloseSpinneronPage();
                        Thread.Sleep(100);
                    }
                }
            }
            if (DialogueContainerDiv.Count > 0)
            {
                List<String> divTitle = DialogueContainerDiv.Select(i => i.Text.ToString()).ToList();
                CloseSpinneronDiv();
                CloseSpinneronPage();
                Thread.Sleep(500);
                if (divTitle.Any(x => x.Contains("Shortlist")))
                {
                    WaitForElement(ShortListDiv);
                    Assert.IsTrue(ShortListDiv.Displayed);
                    WaitForElement(ShortListOkButton);
                    ShortListOkButton.Click();
                    Thread.Sleep(500);
                    CloseSpinneronDiv();
                    CloseSpinneronPage();
                    //Thread.Sleep(1000);
                    WaitForElement(LeadTitle);
                    Thread.Sleep(500);
                    CloseSpinneronDiv();
                    CloseSpinneronPage();
                    //Thread.Sleep(1000);
                    Assert.IsTrue(LeadTitle.Text.Contains("Shortlist for Sales"), "Failed to shortlist the lead");
                }
            } 
        }
    }
}
