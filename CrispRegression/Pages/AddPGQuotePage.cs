﻿
using CrispAutomation.Features;
using CrispAutomation.Support;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.PageObjects;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CrispAutomation.Pages
{
    public class AddPGQuotePage : Support.Pages
    {
        public IWebDriver wdriver;

        public AddPGQuotePage(ISearchContext driver)
        {
            PageFactory.InitElements(driver, this);
            wdriver = (RemoteWebDriver)driver;
        }

        //Key Site Details Page Elements

        [FindsBy(How = How.XPath,
            Using = "//crisp-input-radio[@label='Brand']/div/ul/li/label[text()='LABC Warranty']")]
        public IWebElement LABCRadioButton;

        [FindsBy(How = How.XPath, Using = "//crisp-input-object/div[@class='au-target input-field']/span[@ref='inputContainer']/span/div/span[@class='au-target']")]
        public IList<IWebElement> QuoteRecipientInput;

        [FindsBy(How = How.XPath, Using = "//crisp-content/crisp-input-object//div//label[text()='Quote Recipient']")]
        public IWebElement QuoteRecipientLabel;

        [FindsBy(How = How.XPath, Using = "//contact-list/crisp-list[@ref='listElm']/ul[@ref='theList']/li[@class='au-target collection-item']")]
        public IList<IWebElement> ChooseBuilder;

        [FindsBy(How = How.CssSelector, Using = "crisp-input-text[label = 'Search'] > div > label")]
        public IWebElement SearchLabel;

        [FindsBy(How = How.CssSelector, Using = "crisp-input-text[label = 'Search'] > div > input")]
        public IWebElement SearchInput;

        [FindsBy(How = How.XPath, Using = "//crisp-list[@ref='listElm']//ul//li[@class='au-target collection-item']//crisp-list-item[@class='au-target']//a[contains(@class,'au-target row list-item-contents clickable hasAvatar')]")]
        public IList<IWebElement> QuoteRecipientList;

        [FindsBy(How = How.XPath, Using = "//crisp-list[@ref='listElm']//ul//li[@class='au-target collection-item']//crisp-list-item[@class='au-target']//a[contains(@class,'au-target row list-item-contents clickable hasAvatar')]//crisp-list-item-title//span")]
        public IList<IWebElement> QuoteRecipienttitle;
        public IWebElement quoteRecipientTitle => QuoteRecipienttitle.FirstOrDefault(x => x.Text.Contains(ConfigurationManager.AppSettings["QuoteRecipient"]));

        [FindsBy(How = How.XPath, Using = "//crisp-input-object//div//label[contains(text(),'Office')]")]
        public IList<IWebElement> QuoteRecipientOfficeLabel;

        [FindsBy(How = How.XPath, Using = "//ul//li[@class='au-target collection-item']//crisp-list-item/a")]
        public IList<IWebElement> QuoteRecipientOfficeInput;

        [FindsBy(How = How.XPath, Using = "//crisp-content/crisp-input-object//div//label[text()='Site Address']")]
        public IWebElement SiteAddressLabel;

        [FindsBy(How = How.XPath, Using = "//crisp-content/crisp-input-object//div//span//view-address")]
        public IList<IWebElement> SiteAddressInput;

        [FindsBy(How = How.XPath, Using = "//crisp-button/span/button[text()='Enter Address Using Postcode'][@class='au-target waves-effect waves-light btn-flat']")]
        public IWebElement EnterAddressUsingPostcodeButton;

        [FindsBy(How = How.CssSelector, Using = "crisp-input-text[label='Enter postcode to search'] > div > label")]
        public IWebElement PostcodeSearchLabel;

        [FindsBy(How = How.CssSelector, Using = "crisp-input-text[label='Enter postcode to search'] > div > input")]
        public IWebElement PostcodeInput;

        [FindsBy(How = How.XPath, Using = "//crisp-picker[@label='Address']/div/div/input[@class='select-dropdown']")]
        public IWebElement AddressDropdown;

        [FindsBy(How = How.XPath, Using = "//ul[@class='dropdown-content select-dropdown active']/li[1]")]
        public IWebElement AddressDropdownInput;

        [FindsBy(How = How.XPath, Using = "//crisp-button/span/button[@class='au-target waves-effect waves-light btn'][text()='Set Address']")]
        public IWebElement SetAddressButton;

        [FindsBy(How = How.XPath, Using = "//crisp-picker[@label='Insurance Agreement Group']//div[@class='au-target input-field']//div")]
        public IList<IWebElement> InsuranceGroup;

        [FindsBy(How = How.XPath, Using = "//ul[@class='dropdown-content select-dropdown active']//li//span")]
        public IList<IWebElement> InsuranceGroupInput;

        [FindsBy(How = How.XPath, Using = "//crisp-input-object[@label='Quote Application Documents']//span[contains(@class,'au-target input-container multi-item')]//span")]
        public IWebElement AddQuoteAppDocs;

        [FindsBy(How = How.XPath, Using = "//crisp-dialog//crisp-header//div[text()='Upload document']")]
        public IWebElement UploadDocWizard;

        [FindsBy(How = How.XPath, Using = "//crisp-input-file[@label='Select file...']//div//div//input[@class='au-target']")]
        public IWebElement UploadDoc;

        [FindsBy(How = How.XPath, Using = "//crisp-button[@click.delegate='ok()'][@class='au-target']//span//button[text()='OK'][@class='au-target waves-effect waves-light btn-flat']")]
        public IWebElement UploadOkButton;

        [FindsBy(How = How.XPath, Using = "//crisp-button[@class='au-target'][@click.delegate='wizard.goForward()']//span//button[@class='au-target waves-effect waves-light btn-flat'][text()='Next']")]
        public IWebElement NextButton;

        //Oter Site Details Page Elements 

        [FindsBy(How = How.XPath, Using = "//crisp-input-date/div/label[text()='Construction start date']")]
        public IWebElement ConstructionStartDateLabel;

        [FindsBy(How = How.XPath, Using = "//div[contains(@class,'picker--focused')]/div/div/div/div/div[2]/table[@class='picker__table']/tbody//tr//td/div[text()='10']")]
        public IWebElement ConstructionStartDateInput;

        [FindsBy(How = How.XPath, Using = "//crisp-input-date[@label='Construction end date']/div/label")]
        public IWebElement ConstructionEndDateLabel;

        [FindsBy(How = How.XPath, Using = "//div[contains(@class,'picker--focused')]/div/div/div/div/div[2]/table[@class='picker__table']/tbody//tr//td/div[contains(@class,'day--infocus')][text()='28']")]
        public IWebElement ConstructionEndDateInput;

        [FindsBy(How = How.CssSelector, Using = "div:nth-child(1) > crisp-input-number:nth-child(3) > div > label")]
        public IWebElement NumberOffStoreysAboveGround;

        [FindsBy(How = How.CssSelector, Using = "div:nth-child(1) > crisp-input-number:nth-child(3) > div > input")]
        public IWebElement NumberOffStoreysAboveGroundInput;

        [FindsBy(How = How.CssSelector, Using = "div:nth-child(1) > crisp-input-number:nth-child(4) > div > label")]
        public IWebElement NumberOffStoreysBelowGround;

        [FindsBy(How = How.CssSelector, Using = "div:nth-child(1) > crisp-input-number:nth-child(4) > div > input")]
        public IWebElement NumberOffStoreysBelowGroundInput;

        [FindsBy(How = How.XPath, Using = "//crisp-input-radio[@label='Any innovative construction methods?']/div/ul/li/label[text()='No']")]
        public IWebElement InnovativeMethodsRadioButton;

        [FindsBy(How = How.XPath, Using = "//crisp-input-radio[@label='Is site in administration?']/div/ul/li/label[text()='No']")]
        public IWebElement SiteAdminRadioButton;

        //Plot Schedule Page Elements

        [FindsBy(How = How.XPath, Using = "//crisp-header-button[@click.delegate='importPlots()']/span/button")]
        public IWebElement ImportPlotScheduleButton;

        [FindsBy(How = How.XPath, Using = "//crisp-wizard-step[@step-title='Plot Schedule']//tbody//tr[@class='au-target collection-item']/td[2]")]
        public IWebElement WarrentyProduct;

        [FindsBy(How = How.XPath, Using = "//crisp-wizard-step[@step-title='Plot Schedule']//tbody//tr[@class='au-target collection-item']/td[3]")]
        public IWebElement BCProduct;

        [FindsBy(How = How.XPath, Using = "//crisp-wizard-step[@step-title='Plot Schedule']//tbody//tr[@class='au-target collection-item']/td[4]")]
        public IWebElement UnitType;

        [FindsBy(How = How.XPath, Using = "//crisp-wizard-step[@step-title='Plot Schedule']//tbody//tr[@class='au-target collection-item']/td[5]")]
        public IWebElement StagesOfWorks;

        //Product Details Page Elements

        [FindsBy(How = How.XPath, Using = "//div/crisp-input-object/div/label[text()='Product version']")]
        public IList<IWebElement> ProductVersion;

        [FindsBy(How = How.XPath, Using = "//crisp-wizard-step[@step-title='Products']//ul//li//crisp-list-item//div[@class='crisp-row']//span//dl//span//dd[contains(text(),'Aviva')]")]
        public IWebElement AvivaInsurer;

        [FindsBy(How = How.XPath, Using = "//crisp-input-radio[@label='Cover length']//div//ul//li//label[text()='10']")]
        public IList<IWebElement> CoverLengthList;

        [FindsBy(How = How.XPath, Using = "//crisp-list/ul[@class='au-target collection has-header has-filter']//li[@class='au-target collection-item']")]
        public IList<IWebElement> ChooseProductVersionList;

        [FindsBy(How = How.XPath, Using = "//crisp-input-radio[@label='Are the works contracts under seal?']/div/ul/li/label[text()='No']")]
        public IList<IWebElement> ContractsUnderSeal;

        [FindsBy(How = How.XPath,
         Using = "//crisp-input-currency[@label='Contract cost']//div//label[text()='Contract cost']")]
        public IList<IWebElement> ContractCostLabel;

        [FindsBy(How = How.XPath, Using = "//crisp-input-currency[@label='Contract cost']//div//input")]
        public IWebElement ContractCostInput;

        [FindsBy(How = How.XPath, Using = "//crisp-list-item[@class='au-target']//crisp-input-object[@enabled.bind='enabled']//div[@class='au-target input-field']/i[@class='fa fa-plus add action au-target']")]
        public IList<IWebElement> CoreandServicesAddList;

        //Choosecore and Services List
        [FindsBy(How = How.XPath, Using = "//ul[@ref='theList'][@class='au-target collection has-header has-filter']//li[contains(@class,'collection-item')]")]
        public IList<IWebElement> ChooseCoreServicesList;

        [FindsBy(How = How.XPath, Using = "//crisp-card-actions// div //crisp-button[@click.delegate='ok()']// span// button[@class='au-target waves-effect waves-light btn']")]
        public IList<IWebElement> SelectButton;

        [FindsBy(How = How.XPath, Using = "//crisp-card-actions// div //crisp-button// span// button[@class='au-target waves-effect waves-light btn-flat'][text()='Cancel']")]
        public IWebElement CancelButton;


        //Additional Questions Page Elements

        [FindsBy(How = How.XPath, Using = "//crisp-input-radio[@label='Are any units attached or structurally connected to any other structure not included within this application?']/div/ul/li/label[text()='No']")]
        public IList<IWebElement> NoOfUnitsAttachedRadioButton;

        [FindsBy(How = How.XPath, Using = "//crisp-input-radio[@label='Does the site include Curtain Walling/Glazing?']/div/ul/li/label[text()='No']")]
        public IList<IWebElement> WallingandGlazingRadioButton;

        [FindsBy(How = How.XPath, Using = "//crisp-content/div/h3[text()='Coversions / Refurbishments']")]
        public IList<IWebElement> ConversionsAndRefurbishments;

        [FindsBy(How = How.XPath, Using = "//crisp-input-text[@label='Description of Works']//div//label")]
        public IWebElement DescriptionOfWorkLabel;

        [FindsBy(How = How.XPath, Using = "//crisp-input-text[@label='Description of Works']//div//input")]
        public IWebElement DescriptionOfWorkInput;

        [FindsBy(How = How.XPath, Using = "//div//crisp-input-year[@label='What was the approximate year of original build?']//div//label")]
        public IWebElement OrginalBuildLabel;

        [FindsBy(How = How.XPath, Using = "//div//crisp-input-year[@label='What was the approximate year of original build?']//div//input")]
        public IWebElement OrginalBuildInput;

        [FindsBy(How = How.XPath, Using = "//crisp-picker[@label='What was the previous use of the building?']//div/input")]
        public IWebElement PreviousBuildLabel;

        [FindsBy(How = How.XPath, Using = "//ul[@class='dropdown-content select-dropdown active']/li/span[text()='Residential']")]
        public IWebElement PreviousBuildInput;

        [FindsBy(How = How.XPath, Using = " //crisp-picker[@label='What is the listed status of the building?']//div/input")]
        public IWebElement StatusOfBuildLabel;

        [FindsBy(How = How.XPath, Using = "//ul[@class='dropdown-content select-dropdown active']/li/span[text()='Grade II']")]
        public IWebElement StatusOfBuildInput;

        [FindsBy(How = How.XPath, Using = "//crisp-input-radio[@label='Is the site in a conservation area?']//label[text()='No']")]
        public IWebElement ConversionArea;

        [FindsBy(How = How.XPath, Using = "//crisp-wizard-step//crisp-content//div//h3[text()='Self Build']")]
        public IList<IWebElement> SelfBuildAddtionalQuetions;

        [FindsBy(How = How.XPath, Using = "//crisp-input-radio[contains(@label,'Is an architect involved in this project?')]/div/ul/li/label[text()='No']")]
        public IWebElement IsArchitectOpen;

        [FindsBy(How = How.XPath, Using = "//crisp-input-radio[contains(@label,'Has the home owner built')]/div/ul/li/label[text()='No']")]
        public IWebElement HomeOwnerBuilt;

        [FindsBy(How = How.XPath, Using = "//crisp-input-radio[contains(@label,'Is sole place of residence?')]/div/ul/li/label[text()='No']")]
        public IWebElement SolePlaceOFResidence;

        [FindsBy(How = How.XPath, Using = "//crisp-input-number[@label='Number of Stage Payments']//div//label[text()='Number of Stage Payments']")]
        public IWebElement StagePaymentLabel;

        [FindsBy(How = How.XPath, Using = "//crisp-input-number[@label='Number of Stage Payments']//div//input")]
        public IWebElement StagePaymentInput;

        [FindsBy(How = How.XPath, Using = "//crisp-wizard-step//crisp-content//div//h3[text()='Completed Housing']")]
        public IList<IWebElement> CompletedHousingAdditionalQuetions;

        [FindsBy(How = How.XPath, Using = "//crisp-wizard-step[@step-title='Additional Questions']/crisp-content/div/crisp-input-textarea[contains(@label,'Reason why a structural warranty was not previously put in place')]/div/label")]
        public IWebElement StructuralWarrentyReasonLabel;

        [FindsBy(How = How.XPath, Using = "//crisp-wizard-step[@step-title='Additional Questions']/crisp-content/div/crisp-input-textarea[contains(@label,'Reason why a structural warranty was not previously put in place')]/div/textarea")]
        public IWebElement StructuralWarrentyReasonInput;

        [FindsBy(How = How.XPath, Using = "//crisp-input-number[@label='Loss of Gross Profit - Amount']/div/label")]
        public IList<IWebElement> LossOfGrossProfit;

        [FindsBy(How = How.XPath, Using = "//crisp-input-number[@label='Loss of Gross Profit - Amount']/div/input")]
        public IWebElement LossOfGrossProfitInput;

        [FindsBy(How = How.XPath, Using = "//crisp-input-number[@label='Loss of Rent Receivable - Amount']/div/label")]
        public IList<IWebElement> LossOfRentReceivable;

        [FindsBy(How = How.XPath, Using = "//crisp-input-number[@label='Loss of Rent Receivable - Amount']/div/input")]
        public IWebElement LossOfRentReceivableInput;

        [FindsBy(How = How.XPath, Using = "//crisp-input-number[@label='Loss of Rent Payable - Amount']/div/label")]
        public IList<IWebElement> LossOfRentPayable;

        [FindsBy(How = How.XPath, Using = "//crisp-input-number[@label='Loss of Rent Payable - Amount']/div/input")]
        public IWebElement LossOfRentPayableInput;

        [FindsBy(How = How.XPath, Using = "//crisp-input-number[@label='Loss of Gross Profit - Number of years']/div/label")]
        public IList<IWebElement> LossOfGrossProfitYears;

        [FindsBy(How = How.XPath, Using = "//crisp-input-number[@label='Loss of Gross Profit - Number of years']/div/input")]
        public IWebElement LossOfGrossProfitYearsInput;

        [FindsBy(How = How.XPath, Using = "//crisp-input-number[@label='Loss of Rent Receivable - Number of years']/div/label")]
        public IList<IWebElement> LossOfRentReceivableYears;

        [FindsBy(How = How.XPath, Using = "//crisp-input-number[@label='Loss of Rent Receivable - Number of years']/div/input")]
        public IWebElement LossOfRentReceivableYearsInput;

        [FindsBy(How = How.XPath, Using = "//crisp-input-number[@label='Loss of Rent Payable - Number of years']/div/label")]
        public IList<IWebElement> LossOfRentPayableYears;

        [FindsBy(How = How.XPath, Using = "//crisp-input-number[@label='Loss of Rent Payable - Number of years']/div/input")]
        public IWebElement LossOfRentPayableYearsInput;

        //Roles Page Elements    

        [FindsBy(How = How.CssSelector, Using = "div.au-target.list-title.has-filter > div > crisp-header-button > span > button")]
        public IWebElement EditRoleButton;

        [FindsBy(How = How.XPath, Using = "//ul[@ref='theList']//li[@class='au-target collection-item']//crisp-list-item//crisp-list-item-title//div//span[text()='Administrator']")]
        public IWebElement AdminRole;

        [FindsBy(How = How.XPath, Using = "//ul[@ref='theList']//li[@class='au-target collection-item']//crisp-list-item//crisp-list-item-title//div//span[text()='Builder']")]
        public IList<IWebElement> BuilderRole;

        [FindsBy(How = How.XPath, Using = "//ul[@ref='theList']//li[@class='au-target collection-item']//crisp-list-item//crisp-list-item-title//div//span[text()='Structural Referral Administrator']")]
        public IList<IWebElement> StructuralRefAdminRole;

        [FindsBy(How = How.XPath, Using = "//ul[@ref='theList']//li[@class='au-target collection-item']//crisp-list-item//crisp-list-item-title//div//span[text()='Sales Account Manager']")]
        public IList<IWebElement> SalesAccountMaangerRole;

        [FindsBy(How = How.XPath, Using = "//ul[@ref='theList']//li[@class='au-target collection-item']//crisp-list-item//crisp-list-item-title//div//span[text()='Design Review Administrator']")]
        public IList<IWebElement> DesignReviewAdminRole;

        [FindsBy(How = How.XPath, Using = "//ul[@ref='theList']//li[@class='au-target collection-item']//crisp-list-item//crisp-list-item-title//div//span[text()='Applicant (Self-build)']")]
        public IList<IWebElement> SelfBuildApplicantRole;

        [FindsBy(How = How.XPath, Using = "//ul[@ref='theList']//li[@class='au-target collection-item']//crisp-list-item//crisp-list-item-title//div//span[text()='Builder']")]
        public IWebElement SelfBuildBuilderRole;

        [FindsBy(How = How.XPath, Using = "//ul[@ref='theList']//li[@class='au-target collection-item']//crisp-list-item//crisp-list-item-title//div//span[text()='Building Control Provider']")]
        public IWebElement SelfBuildBCProviderRole;

        [FindsBy(How = How.XPath, Using = "//ul[@ref='theList']//li[@class='au-target collection-item']//crisp-list-item//crisp-list-item-title//div//span[text()='Building Control Provider']")]
        public IList<IWebElement> BuildingControlProviderRole;



        [FindsBy(How = How.XPath, Using = "//ul[@ref='theList']//li[@class='au-target collection-item']//crisp-list-item//crisp-list-item-title//div//span[text()='Housing Association']")]
        public IList<IWebElement> HousingAssociationRole;

        //[FindsBy(How = How.XPath, Using = "//ul[@ref='theList']//li[@class='au-target collection-item']//crisp-list-item//crisp-list-item-title//div//span[text()='PRS Company']")]
        //public IList<IWebElement> PRSCompanyRole;

        [FindsBy(How = How.XPath, Using = "//ul[@ref='theList']//li[@class='au-target collection-item']//crisp-list-item//crisp-list-item-title//div//span[text()='Developer']")]
        public IWebElement LABCDeveloperRole;

        [FindsBy(How = How.XPath, Using = "//ul[@ref='theList']//li[@class='au-target collection-item']//crisp-list-item//crisp-list-item-title//div//span[text()='Developer']")]
        public IList<IWebElement> DeveloperRole;


        [FindsBy(How = How.XPath, Using = "//ul[@ref='theList']//li[@class='au-target collection-item']//crisp-list-item//crisp-list-item-title//div//span[text()='PRS Company']")]
        public IList<IWebElement> PGPRSCompanyRole;

        [FindsBy(How = How.XPath, Using = "//table[@class='au-target']//tbody//tr//td//em[text()='Not selected']")]
        public IWebElement MDWSSAdminNotSelected;


        //Company List 
        [FindsBy(How = How.XPath, Using = "//crisp-list[@ref='listElm']//ul[@ref='theList']//li[@class='au-target collection-item']")]
        public IList<IWebElement> CompanyList;

        [FindsBy(How = How.XPath, Using = "//ul[@ref='theList']//li[@class='au-target collection-item']//crisp-list-item[@class='au-target']")]
        public IList<IWebElement> EmpList;

        [FindsBy(How = How.XPath, Using = "//crisp-card-content[@class='au-target']//span/input[@type='text']")]
        public IWebElement SearchRoleLabel;

        [FindsBy(How = How.CssSelector, Using = "crisp-card > crisp-card-actions > div > crisp-button:nth-child(1) > span > button")]
        public IWebElement ChooseCompanyButton;

        [FindsBy(How = How.XPath, Using = "//crisp-card-content/div/div/div/crisp-input-text/div/label[text()='Search']")]
        public IWebElement SearchEmployeeLabel;

        [FindsBy(How = How.XPath, Using = "//crisp-card-content/div/div/div/crisp-input-text/div/input")]
        public IWebElement SearchEmployeeInput;

        //Declaration Page Elements 

        [FindsBy(How = How.XPath, Using = "//crisp-input-radio[@label='Applicant has confirmed that all details are correct']/div/ul/li/label[text()='Yes']")]
        public IWebElement ApplicationConfirmationRadiobutton;

        [FindsBy(How = How.XPath, Using = "//crisp-wizard-step[@step-title='Declaration']//crisp-input-radio/div/ul/li/label[text()='No']")]
        public IList<IWebElement> DeclarationPageRadioButtons;

        [FindsBy(How = How.XPath, Using = "//crisp-footer/nav/div/crisp-button[@class='au-target']/span/button[text()='Save']")]
        public IWebElement SaveButton;

        [FindsBy(How = How.XPath, Using = "//crisp-footer/nav/div/crisp-button[@class='au-target']/span/button[text()='Submit']")]
        public IWebElement SubmitButton;

        [FindsBy(How = How.XPath, Using = "//crisp-button[@class='au-target']/span/button[text()='Ok']")]
        public IWebElement OkButton;

        [FindsBy(How = How.XPath, Using = "//div//crisp-header-title[@class='au-target'][1]/div[@class='title']")]
        public IWebElement QuoteRefDetails;

        //Additional Buttons 

        [FindsBy(How = How.XPath, Using = "//crisp-button[@click.delegate='additionalButtonClicked(btn)'][@class='au-target']//button[text()='Add Person']")]
        public IWebElement AddPersonButton;

        [FindsBy(How = How.XPath, Using = "//crisp-button[@click.delegate='additionalButtonClicked(btn)'][@class='au-target']//button[text()='Add Company']")]
        public IWebElement AddCompanyButton;

        [FindsBy(How = How.XPath, Using = "//crisp-button[@click.delegate='additionalButtonClicked(btn)'][@class='au-target']//button[text()='Add Employee']")]
        public IWebElement AddEmployeeButton;

        [FindsBy(How = How.XPath, Using = "//crisp-input-radio[@label='Brand']/div/ul/li/label[text()='Premier Guarantee']")]
        public IList<IWebElement> PGRadioButton;

        // PG Create Quote - Developmenet information details 
        public void PGKeySiteDetailsPage()
        {
            WaitForElement(AddLABCQuotePage.StepTitle);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Assert.IsTrue(AddLABCQuotePage.StepTitle.Text.Contains("KEY SITE DETAILS"), $"Failed to Display {AddLABCQuotePage.StepTitle.Text}");
            SelectPGBrand();
            AddLABCQuotePage.QuoteRecipientDetails();
            AddLABCQuotePage.SiteAddressDetails();
            AddLABCQuotePage.InsurerGroupDetails();
            AddLABCQuotePage.QuoteApplicationDocumentsDetails();
            WaitForElement(NextButton);
            NextButton.Click();
            CloseSpinneronDiv();
            CloseSpinneronPage();
        }


        public void PGKeySiteDetailsWithMigratedData()
        {
            WaitForElement(AddLABCQuotePage.StepTitle);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Assert.IsTrue(AddLABCQuotePage.StepTitle.Text.Contains("KEY SITE DETAILS"), $"Failed to Display {AddLABCQuotePage.StepTitle.Text}");
            SelectPGBrand();
            AddLABCQuotePage.QuoteRecipientDetailsWithMigratedCompany();
            AddLABCQuotePage.SiteAddressDetails();
            AddLABCQuotePage.InsurerGroupDetails();
            AddLABCQuotePage.QuoteApplicationDocumentsDetails();
            WaitForElement(NextButton);
            NextButton.Click();
            CloseSpinneronDiv();
            CloseSpinneronPage();
           
        }

        public void PGKeySiteDetailsPageOnExtranet()
        {
            WaitForElement(AddLABCQuotePage.StepTitle);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Assert.IsTrue(AddLABCQuotePage.StepTitle.Text.Contains("KEY SITE DETAILS"), $"Failed to Display {AddLABCQuotePage.StepTitle.Text}");
            SelectPGBrand();
            AddLABCQuotePage.QuoteRecipientDetailsOnExtranet();
            AddLABCQuotePage.SiteAddressDetails();
            AddLABCQuotePage.InsurerGroupDetails();
            AddLABCQuotePage.QuoteApplicationDocumentsDetails();
            WaitForElement(NextButton);
            NextButton.Click();
            CloseSpinneronDiv();
            CloseSpinneronPage();
        }
        public void SelectPGBrand()
        {
            WaitForElements(PGRadioButton);
            WaitForLoadElements(PGRadioButton);
            if (PGRadioButton.Count > 0)
            {
                CloseSpinneronDiv();
                CloseSpinneronPage();
                WaitForElement(PGRadioButton[0]);
                WaitForElementToClick(PGRadioButton[0]);
                PGRadioButton[0].Click();
                CloseSpinneronDiv();
                CloseSpinneronPage();
            }
            CloseSpinneronDiv();
            CloseSpinneronPage();
        }

        
        // PG Create Quote - Other Details Page 
        public void OtherDetails()
        {
            
            WaitForElement(ConstructionStartDateLabel);
            ConstructionStartDateLabel.Click();
            WaitForElement(ConstructionStartDateInput);
            ConstructionStartDateInput.Click();
            
            WaitForElement(ConstructionEndDateLabel);
            ConstructionEndDateLabel.Click();
            WaitForElement(ConstructionEndDateInput);
            ConstructionEndDateInput.Click();
            WaitForElement(NumberOffStoreysAboveGround);
            NumberOffStoreysAboveGround.Click();
            WaitForElement(NumberOffStoreysAboveGroundInput);
            NumberOffStoreysAboveGroundInput.SendKeys("2");
            WaitForElement(NumberOffStoreysBelowGround);
            NumberOffStoreysBelowGround.Click();
            WaitForElement(NumberOffStoreysBelowGroundInput);
            NumberOffStoreysBelowGroundInput.SendKeys("1");
            WaitForElement(InnovativeMethodsRadioButton);
            InnovativeMethodsRadioButton.Click();
            WaitForElement(SiteAdminRadioButton);
            SiteAdminRadioButton.Click();
            WaitForElement(NextButton);
            NextButton.Click();
            
        }
        // PG Create Quote - Import PlotSchedule Data 
        public void PlotScheduleDetails(string plotdata)
        {
            ExtensionMethods.ReadExcelPlotData(plotdata);
            try
            {

                WaitForElement(ImportPlotScheduleButton);
                ImportPlotScheduleButton.Click();
                CloseSpinneronDiv();
                
                UploadDoc.SendKeys(plotdata);
                log.Info("File has been uploaded");
                
            }
            catch (Exception e)

            {
                log.Info(e);
                Console.WriteLine("element not found upload folder path");
            }
            WaitForElement(UploadOkButton);
            UploadOkButton.Click();
            
            CloseSpinneronDiv();
            WaitForElement(WarrentyProduct);
            var productName = WarrentyProduct.Text;
            Statics.ProductName = productName;
            WaitForElement(NextButton);
            NextButton.Click();
            CloseSpinneronDiv();
            
        }
        // PG Create Quote - Product Details Page
        public void ProductsDetails()
        {
            
            if (CoverLengthList.Count > 0)
            {
                WaitForElements(CoverLengthList);
                foreach (var coverlenghthinput in CoverLengthList)
                {
                    if (coverlenghthinput.Selected == false)
                    {
                        coverlenghthinput.Click();
                    }
                }
            }

            
            if (ProductVersion.Count > 0)
            {
                WaitForElements(ProductVersion);
                foreach (var eachproductversion in ProductVersion)
                {
                    eachproductversion.Click();
                    
                    WaitForElements(ChooseProductVersionList);
                    if (ChooseProductVersionList.Count > 0)
                    {
                        WaitForElements(ChooseProductVersionList);
                        
                        ChooseProductVersionList[1].Click();
                        
                        CloseSpinneronDiv();
                    }
                }
            }

            if (ContractsUnderSeal.Count > 0)
            {
                foreach (var eachcontractunderseal in ContractsUnderSeal)
                {
                    if (eachcontractunderseal.Selected == false)
                    {
                        eachcontractunderseal.Click();
                    }
                    CloseSpinneronDiv();

                }
            }
            if (ContractCostLabel.Count > 0)
            {
                CloseSpinneronDiv();

                foreach (var eachcontractcost in ContractCostLabel)
                {
                    eachcontractcost.Click();
                    WaitForElement(ContractCostInput);
                    ContractCostInput.Clear();
                    ContractCostInput.SendKeys("120000");
                }
            }

            
            if (CoreandServicesAddList.Count > 0)
            {
                WaitForElements(CoreandServicesAddList);
                foreach (var addcoreservice in CoreandServicesAddList)
                {
                    addcoreservice.Click();
                    
                    if (ChooseCoreServicesList.Count > 0)
                    {
                        //Choose Core Services List
                        foreach (var eachcoreservice in ChooseCoreServicesList)
                        {
                            eachcoreservice.Click();
                        }

                        
                        WaitForElements(SelectButton);
                        SelectButton[0].Click();
                        
                    }
                }
            }
            CloseSpinneronDiv();
            WaitForElement(NextButton);
            NextButton.Click();
            CloseSpinneronDiv();
            
        }
        public void AdditionalQuestionsPage()
        {
            
            if (NoOfUnitsAttachedRadioButton.Count > 0)
            {
                foreach (var eachunit in NoOfUnitsAttachedRadioButton)
                {
                    eachunit.Click();
                    CloseSpinneronDiv();
                }
            }
            
            if (WallingandGlazingRadioButton.Count > 0)
            {
                foreach (var eachunit in WallingandGlazingRadioButton)
                {
                    eachunit.Click();
                    CloseSpinneronDiv();
                }
            }
            
            if (ConversionsAndRefurbishments.Count > 0)
            {
                if (DescriptionOfWorkLabel.Displayed)
                {
                    WaitForElement(DescriptionOfWorkLabel);
                    DescriptionOfWorkLabel.Click();
                    WaitForElement(DescriptionOfWorkInput);
                    DescriptionOfWorkInput.Click();
                    DescriptionOfWorkInput.Clear();
                    DescriptionOfWorkInput.SendKeys("Conversion");
                }

                if (OrginalBuildLabel.Displayed)
                {
                    WaitForElement(OrginalBuildLabel);
                    OrginalBuildLabel.Click();
                    WaitForElement(OrginalBuildInput);
                    OrginalBuildInput.Click();
                    OrginalBuildInput.Clear();
                    OrginalBuildInput.SendKeys("2016");
                }

                if (PreviousBuildLabel.Displayed)
                {
                    WaitForElement(PreviousBuildLabel);
                    PreviousBuildLabel.Click();
                    WaitForElement(PreviousBuildInput);
                    PreviousBuildInput.Click();
                }

                if (StatusOfBuildLabel.Displayed)
                {
                    WaitForElement(StatusOfBuildLabel);
                    StatusOfBuildLabel.Click();
                    WaitForElement(StatusOfBuildInput);
                    StatusOfBuildInput.Click();
                }

                if (ConversionArea.Displayed)
                {
                    WaitForElement(ConversionArea);
                    ConversionArea.Click();
                }
                CloseSpinneronDiv();
            }
            //Selfbuild AdditionalQuotions
            if (SelfBuildAddtionalQuetions.Count > 0)
            {
                WaitForElement(IsArchitectOpen);
                IsArchitectOpen.Click();
                WaitForElement(HomeOwnerBuilt);
                HomeOwnerBuilt.Click();
                WaitForElement(SolePlaceOFResidence);
                SolePlaceOFResidence.Click();
                WaitForElement(StagePaymentLabel);
                StagePaymentLabel.Click();
                WaitForElement(StagePaymentInput);
                StagePaymentInput.Clear();
                StagePaymentInput.SendKeys("12");
                
            }
            //Completed Houing Additional Quetions
            if (CompletedHousingAdditionalQuetions.Count > 0)
            {
                WaitForElement(StructuralWarrentyReasonLabel);
                StructuralWarrentyReasonLabel.Click();
                WaitForElement(StructuralWarrentyReasonInput);
                StructuralWarrentyReasonInput.SendKeys("There was no availability");
                
            }
            //Commercials Conversion Addtional Questions 
            if (LossOfGrossProfit.Count > 0)
            {
                WaitForElements(LossOfGrossProfit);
                LossOfGrossProfit[0].Click();
                WaitForElement(LossOfGrossProfitInput);
                LossOfGrossProfitInput.SendKeys("20000");
            }
            if (LossOfRentReceivable.Count > 0)
            {
                WaitForElements(LossOfRentReceivable);
                LossOfRentReceivable[0].Click();
                WaitForElement(LossOfRentReceivableInput);
                LossOfRentReceivableInput.SendKeys("10000");
            }
            if (LossOfRentPayable.Count > 0)
            {
                WaitForElements(LossOfRentPayable);
                LossOfRentPayable[0].Click();
                WaitForElement(LossOfRentPayableInput);
                LossOfRentPayableInput.SendKeys("10000");
            }
            if (LossOfGrossProfitYears.Count > 0)
            {
                WaitForElements(LossOfGrossProfitYears);
                LossOfGrossProfitYears[0].Click();
                WaitForElement(LossOfGrossProfitYearsInput);
                LossOfGrossProfitYearsInput.SendKeys("5");
            }
            if (LossOfRentReceivableYears.Count > 0)
            {
                WaitForElements(LossOfRentReceivableYears);
                LossOfRentReceivableYears[0].Click();
                WaitForElement(LossOfRentReceivableYearsInput);
                LossOfRentReceivableYearsInput.SendKeys("3");
            }
            if (LossOfRentPayableYears.Count > 0)
            {
                WaitForElements(LossOfRentPayableYears);
                LossOfRentPayableYears[0].Click();
                WaitForElement(LossOfRentPayableYearsInput);
                LossOfRentPayableYearsInput.SendKeys("2");
            }
            WaitForElement(NextButton);
            NextButton.Click();
            CloseSpinneronDiv();
        }
        //Create Quote - Declaration Details Page
        public void DeclarationDetailsPage()
        {
            CloseSpinneronDiv();
            WaitForElements(DeclarationPageRadioButtons);
            foreach (var eachradiobutton in DeclarationPageRadioButtons)
            {
                eachradiobutton.Click();
            }
            WaitForElement(ApplicationConfirmationRadiobutton);
            ApplicationConfirmationRadiobutton.Click();
            
        }

        public void SubmitPGQuote(string masterPlotData)
        {
            ExtensionMethods.ReadExcelPlotData(masterPlotData);
            CloseSpinneronPage();
            CloseSpinneronDiv();
            //Thread.Sleep(2000);
            WaitForElement(Dashboardpage.PlusButton);
            Dashboardpage.PlusButton.Click();
            WaitForElement(Dashboardpage.QuoteOption);
            Dashboardpage.QuoteOption.Click();
            WaitForElement(AddLABCQuotePage.AddNewQuoteDiv);
            CloseSpinneronPage();
            CloseSpinneronDiv();
            PGKeySiteDetailsPage();
            AddLABCQuotePage.OtherSiteDetailsPage();
            AddLABCQuotePage.PlotSchedulePage(masterPlotData);
            AddLABCQuotePage.ProductsDetailsPage();
            AddLABCQuotePage.AdditionalQuestionsPage();
            AddLABCQuotePage.RolesDetails();
            AddLABCQuotePage.DeclarationDetailsPage();
            WaitForElement(SaveButton);
            SaveButton.Click();
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(2000);
            WaitForElement(SubmitButton);
            SubmitButton.Click();
            //Thread.Sleep(2000);
            CloseSpinneronPage();
            CloseSpinneronDiv();
            //Thread.Sleep(2000);
            WaitForElement(Dashboardpage.QuoteApplicationStatusSubmitted);
            Assert.IsTrue(Dashboardpage.QuoteApplicationStatusSubmitted.Displayed);
        }
        public void SubmitPGQuoteForExtranet(string masterPlotData)
        {
            ExtensionMethods.ReadExcelPlotData(masterPlotData);
            CloseSpinneronPage();
            CloseSpinneronDiv();
            //Thread.Sleep(2000);
            WaitForElement(Dashboardpage.PlusButton);
            Dashboardpage.PlusButton.Click();
            WaitForElement(Dashboardpage.QuoteOption);
            Dashboardpage.QuoteOption.Click();
            WaitForElement(AddLABCQuotePage.AddNewQuoteDiv);
            CloseSpinneronPage();
            CloseSpinneronDiv();
            PGKeySiteDetailsPageOnExtranet();
            AddLABCQuotePage.OtherSiteDetailsPage();
            AddLABCQuotePage.PlotSchedulePage(masterPlotData);
            AddLABCQuotePage.ProductsDetailsPage();
            AddLABCQuotePage.AdditionalQuestionsPage();
            AddLABCQuotePage.RolesDetails();
            AddLABCQuotePage.DeclarationDetailsPage();
            WaitForElement(SaveButton);
            SaveButton.Click();
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(2000);
            WaitForElement(SubmitButton);
            SubmitButton.Click();
            //Thread.Sleep(2000);
            CloseSpinneronPage();
            CloseSpinneronDiv();
            //Thread.Sleep(2000);
            WaitForElement(Dashboardpage.QuoteApplicationStatusSubmitted);
            Assert.IsTrue(Dashboardpage.QuoteApplicationStatusSubmitted.Displayed);
        }

        public void SubmitQuoteAPI()
        {
           // { "products":[{"productId":"68bc9325-9dc5-50c1-a694-cce10daacb82","length":10,"productItems":["d3af1c52-1d06-5f72-a404-a2adfc41d5f1","5be3c9eb-94b0-5018-bc75-6355c2121122","3d391c31-9ba8-5dbe-8183-fe40eed63ad7","3bce8bef-fa18-529f-b5ed-63b61dfbadd9","490bffac-48fe-55e4-a30e-a641540fb76d","2b991b5c-93d9-5e82-a6e4-ed6dbcc486a7","56869b7b-8676-5219-a127-10dc375c0bf6","53dfd375-615c-5da5-9ff6-7897f9621dda"]}],"brand":2,"address":{"id":"ae25c058-c945-4d1f-9f54-9fbd8634a1e2","address1":"M D Insurance Services Ltd","address2":"2 Shore Lines Building","address3":"Shore Road","town":"Merseyside","postcode":"CH41 1AU","countryCodeType":214},"quoteRecipientsOfficeId":"d46061d2-749a-46b0-ba61-bf947b9a52e7","documents":[{"fileSize":309698,"fileId":"54c30811-20aa-47c5-a9cf-a9ac00d2435c","fileName":"plots (1).xlsx","dateReceived":"2018-12-04T12:45:28.733Z"}],"constructionStartDate":"2018-12-01T00:00:00.000Z","constructionEndDate":"2019-02-06T00:00:00.000Z","maxStoreysAboveGround":1,"maxStoreysBelowGround":1,"hasInnovativeMethods":false,"hasUnitsOrStructureAttached":true,"isSiteInAdministration":false,"quoteRecipient":"40cb7716-0d3c-4bb3-b907-a9a701016891","additionalRoles":[{"role":30,"ids":["cde59849-e04f-4605-86c5-b770dea9e4b8"]},{"role":39,"ids":["40cb7716-0d3c-4bb3-b907-a9a701016891"]},{"role":14,"ids":["40cb7716-0d3c-4bb3-b907-a9a701016891"]},{"role":37,"ids":["40cb7716-0d3c-4bb3-b907-a9a701016891"]},{"role":5,"ids":["40cb7716-0d3c-4bb3-b907-a9a701016891"]},{"role":15,"ids":["4ef240ae-347d-447e-9894-1e4b58d93358"]},{"role":16,"ids":["4ef240ae-347d-447e-9894-1e4b58d93358"]},{"role":32,"ids":["e2ac35ab-e72d-451e-a5b7-296575ce7311"]},{"role":19,"ids":["7446a7ec-d415-4d89-92cf-0086bd07c565"]},{"role":7,"ids":["0725b012-d203-4a56-9833-a9a701016887"]}],"selfBuildAdditionalQuestions":{},"completedHousingAdditionalQuestions":{},"inAdminstrationAdditionalQuestions":{},"generalInsuranceQuestions":{"sustainedAnyLossesOrHadAnyClaimsInTheLastThreeYears":false,"everBeenRefusedPropertyInsuranceOrSpecialTermsImposed":false,"everBeenConvictedOrProsecutionPendingInvolvingDishonesty":false,"everBeenProsecutedOrReceivedNotificationUnderHswa":false,"everBeenInvolvedWithLiquidatedOrBankruptBuilderOnConstructionCompany":false},"conversionQuestions":{"originalBuildYear":2014,"buildingPreviousUses":3,"buildingListedStatus":1,"siteInConservationArea":true,"descriptionOfWorks":"test","hasExperienceInConversion":true,"hasConditionSurveyCarriedOut":true,"hasOtherSurveysCarriedOut":true,"hasBarnConversions":true},"hasApplicantConfirmedDetailsAreCorrect":true,"plotScheduleFileId":"228b6419-4171-4a46-ac48-a9ac00d2af5e","brokerAgreementId":"5414b4f2-a8e9-e811-baa4-a08cfdf4bbef","lossOfGrossRentAnnualAmountNumberOfYears":null,"lossOfGrossRentAnnualAmount":null,"lossOfRentPayableAnnualAmountNumberOfYears":null,"lossOfRentPayableAnnualAmount":null,"lossOfRentRecievableAmmountNumberOfYears":null,"lossofRentReceivableAnnualAmount":null,"quoteVersionsParentQuoteId":null,"insuranceAgreementGroup":2,"productVersionDate":"2018-12-04"}
        }
        // PG Create Quote - Developmenet information details 
        public void PGKeySiteDetailsSelectAXHVSInsurer()
        {
            WaitForElement(AddLABCQuotePage.StepTitle);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Assert.IsTrue(AddLABCQuotePage.StepTitle.Text.Contains("KEY SITE DETAILS"), $"Failed to Display {AddLABCQuotePage.StepTitle.Text}");
            SelectPGBrand();
            AddLABCQuotePage.QuoteRecipientDetails();
            AddLABCQuotePage.SiteAddressDetails();
            if (Statics.ProductNameList.Any(x => x.Contains("High Value")))
            {
                SelectAXAHVSInsurer();
            }
            else
            {
                if (InsuranceGroup.Count > 0)
                {
                    //Thread.Sleep(500);
                    CloseSpinneronPage();
                    CloseSpinneronDiv();
                    //Thread.Sleep(500);
                    WaitForElement(InsuranceGroup[0]);
                    WaitForElementToClick(InsuranceGroup[0]);
                    InsuranceGroup[0].Click();
                    CloseSpinneronDiv();
                    WaitForElements(InsuranceGroupInput);
                    var insuranceGroup = InsuranceGroupInput.FirstOrDefault(o => o.Text.Contains("AmTrust"));
                    if (insuranceGroup.Displayed)
                    {
                        insuranceGroup.Click();
                    }
                    CloseSpinneronDiv();
                }
            }
            CloseSpinneronDiv();
            CloseSpinneronPage();
            AddLABCQuotePage.QuoteApplicationDocumentsDetails();
     

            WaitForElement(NextButton);
            NextButton.Click();
            CloseSpinneronDiv();
            CloseSpinneronPage();
        }
        // PG Create Quote - Developmenet information details 
        public void PGKeySiteDetailsSelectSompoCanopiusInsurer()
        {
            WaitForElement(AddLABCQuotePage.StepTitle);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Assert.IsTrue(AddLABCQuotePage.StepTitle.Text.Contains("KEY SITE DETAILS"), $"Failed to Display {AddLABCQuotePage.StepTitle.Text}");
            SelectPGBrand();
            AddLABCQuotePage.QuoteRecipientDetails();
            AddLABCQuotePage.SiteAddressDetails();

            if (Statics.ProductNameList.Any(x => x.Contains("High Value")))
            {
                SelectSompoCanopiusInsurer();
            }
            else
            {
                if (InsuranceGroup.Count > 0)
                {
                    //Thread.Sleep(500);
                    CloseSpinneronPage();
                    CloseSpinneronDiv();
                    //Thread.Sleep(500);
                    WaitForElement(InsuranceGroup[0]);
                    WaitForElementToClick(InsuranceGroup[0]);
                    InsuranceGroup[0].Click();
                    CloseSpinneronDiv();
                    WaitForElements(InsuranceGroupInput);
                    var insuranceGroup = InsuranceGroupInput.FirstOrDefault(o => o.Text.Contains("AmTrust"));
                    if (insuranceGroup.Displayed)
                    {
                        insuranceGroup.Click();
                    }
                    CloseSpinneronDiv();
                }
            }
            CloseSpinneronDiv();
            CloseSpinneronPage();
            AddLABCQuotePage.QuoteApplicationDocumentsDetails();

            WaitForElement(NextButton);
            NextButton.Click();
            CloseSpinneronDiv();
            CloseSpinneronPage();
        }
        public void SelectAXAHVSInsurer()
        {
            if (InsuranceGroup.Count > 0)
            {
                //Thread.Sleep(500);
                CloseSpinneronPage();
                CloseSpinneronDiv();
                //Thread.Sleep(500);
                WaitForElement(InsuranceGroup[0]);
                WaitForElementToClick(InsuranceGroup[0]);
                InsuranceGroup[0].Click();
                CloseSpinneronDiv();
                WaitForElements(InsuranceGroupInput);
                var insuranceGroup = InsuranceGroupInput.FirstOrDefault(o => o.Text.Contains("Axa HVS"));
                if (insuranceGroup.Displayed)
                {
                    insuranceGroup.Click();
                }
                CloseSpinneronDiv();
            }
        }
        public void SelectSompoCanopiusInsurer()
        {
            if (InsuranceGroup.Count > 0)
            {
                //Thread.Sleep(500);
                CloseSpinneronPage();
                CloseSpinneronDiv();
                //Thread.Sleep(500);
                WaitForElement(InsuranceGroup[0]);
                WaitForElementToClick(InsuranceGroup[0]);
                InsuranceGroup[0].Click();
                CloseSpinneronDiv();

                WaitForElements(InsuranceGroupInput);
                var insuranceGroup = InsuranceGroupInput.FirstOrDefault(o => o.Text.Contains("Sompo Canopius (Certain Underwriters at Lloyds)"));
                if (insuranceGroup.Displayed)
                {
                    insuranceGroup.Click();
                }
                CloseSpinneronDiv();
            }
        }
    }
}
