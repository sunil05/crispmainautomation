﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using System.Threading;

namespace CrispAutomation.Pages
{
   public class RejectLeadPage :Support.Pages
    {
        public RejectLeadPage(ISearchContext driver)
        {
            PageFactory.InitElements(driver,this);
        }

        [FindsBy(How = How.XPath, Using = "//crisp-input-object[@label.bind='label']/div")]
        public IWebElement RejectionReasonLabel;
        
        [FindsBy(How = How.XPath, Using = "//crisp-card-content//div[@class='card-content']//crisp-list//ul[@ref='theList']//li[@class='au-target collection-item']")]
        public IList<IWebElement> RejectionReasonsList;

        [FindsBy(How = How.XPath, Using = "//crisp-card-actions//div//crisp-button//span//button[@class='au-target waves-effect waves-light btn']")]
        public IWebElement SelectButton;

        [FindsBy(How = How.XPath, Using = "//crisp-input-text[@label='Notes']//div//label[contains(@class,'au-target')]")]
        public IWebElement NotesLabel;

        [FindsBy(How = How.XPath, Using = "//crisp-input-text[@label='Notes']//div//input")]
        public IWebElement NotesInput;

        [FindsBy(How = How.XPath, Using = "//crisp-picker[@label='Competitor']//div//div//input[@class='select-dropdown']")]
        public IWebElement CompetitorDropdown;

        [FindsBy(How = How.XPath, Using = "//ul[@class='dropdown-content select-dropdown active']//li//span[text()='Premier Guarantee']")]
        public IWebElement CompetitorDropdownInput;

        [FindsBy(How = How.XPath, Using = "//crisp-button[@click.delegate='reject()']//span//button")]
        public IWebElement RejectOptionButton;

        [FindsBy(How = How.XPath, Using = "//li[2]//crisp-header-dropdown-button//span//button")]
        public IWebElement RejectButton;

        public void RejectingLead()
        {
            //Thread.Sleep(1000);
            RejectionReasonLabel.Click();
            //Thread.Sleep(1000);
            WaitForElements(RejectionReasonsList);
            if (RejectionReasonsList.Count > 0)
            {
                WaitForElementToClick(RejectionReasonsList[0]);
                RejectionReasonsList[0].Click();
                //Thread.Sleep(500);
                WaitForElement(SelectButton);
                //Thread.Sleep(500);
               
                SelectButton.Click();
                CloseCrispCard();
                CloseSpinneronDiv();
                CloseSpinneronPage();

            }
            //Thread.Sleep(1000);
            WaitForElement(NotesLabel);
            NotesLabel.Click();
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(500);
            WaitForElement(NotesInput);
            NotesInput.SendKeys("Rejecting Lead");
            //Thread.Sleep(500);
            WaitForElement(CompetitorDropdown);
            CompetitorDropdown.Click();
            //Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            WaitForElement(CompetitorDropdownInput);
            CompetitorDropdownInput.Click();
            //Thread.Sleep(1000);
            CloseSpinneronDiv();
            CloseSpinneronPage();
        }
    }
}
