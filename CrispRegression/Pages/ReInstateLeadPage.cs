﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using System.Threading;

namespace CrispAutomation.Pages
{
    public class ReInstateLeadPage : Support.Pages
    {
        public ReInstateLeadPage(ISearchContext driver)
        {
            PageFactory.InitElements(driver, this);
        }

        [FindsBy(How = How.XPath, Using = "//crisp-input-text[@label='Notes']/div/label")]
        public IWebElement NotesLabel;

        [FindsBy(How = How.XPath, Using = "//crisp-input-text[@label='Notes']/div/input")]
        public IWebElement NotesInput;

        [FindsBy(How = How.XPath, Using = "//crisp-button[@click.delegate='reinstate()']//span//button")]
        public IWebElement ReInstateOptionButton;

        [FindsBy(How = How.XPath, Using = "//li[1]/crisp-header-dropdown-button/span/button")]
        public IWebElement ReInstateButton;

        public void ReInstateLead()
        {
            //Thread.Sleep(1000);
            WaitForElement(NotesLabel);
            NotesLabel.Click();
            //Thread.Sleep(500);
            WaitForElement(NotesInput);
            NotesInput.SendKeys("ReInstiating Lead");
            //Thread.Sleep(1000);
        }
    }
}
