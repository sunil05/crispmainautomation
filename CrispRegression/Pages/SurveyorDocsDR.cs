﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.PageObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace CrispAutomation.Pages
{
    public class SurveyorDocsDR : Support.Pages
    {
        public IWebDriver wdriver;
        public SurveyorDocsDR(ISearchContext driver)
        {
            PageFactory.InitElements(driver, this);
            wdriver = (RemoteWebDriver)driver;
        }


        //Opend Documents on DR 
        public void OpenSurveyorDocsDR()
        {
            WaitForElementonSurveyor(DesignReviewPage.DRStatus);

            if (DesignReviewPage.DRStatus.Text.Contains("Outstanding"))
            {
                DesignReviewPage.OpenDR();
                DesignReviewPage.DesignReviewAssessment();
                SurveyorDocs.OpenDocuments();
                DesignReviewPage.SubmitDR();
            }
            else
            {
                DesignReviewPage.OpenDR();
                //Thread.Sleep(500);
                CloseSpinnerOnSurveyorPage();
                CloseSpinnerOnSurveyorDiv();
                //Thread.Sleep(500);
                WaitForElementonSurveyor(DesignReviewPage.NextButton);
                DesignReviewPage.NextButton.Click();
                SurveyorDocs.OpenDocuments();
                DesignReviewPage.SubmitDR();
            }
        }
        //Close Documents on DR
        public void CloseSurveyorDocsDR()
        {
            WaitForElementonSurveyor(DesignReviewPage.DRStatus);
            if (DesignReviewPage.DRStatus.Text.Contains("Outstanding") || DesignReviewPage.DRStatus.Text.Contains("Not required"))
            {
                DesignReviewPage.OpenDR();
                DesignReviewPage.DesignReviewAssessment();
                CloseDocsonDR();
                DesignReviewPage.SubmitDR();
            }
            else
            {
                DesignReviewPage.OpenDR();
                Thread.Sleep(500);
                CloseSpinnerOnSurveyorPage();
                CloseSpinnerOnSurveyorDiv();
                //Thread.Sleep(500);
                WaitForElementonSurveyor(DesignReviewPage.NextButton);
                DesignReviewPage.NextButton.Click();
                CloseDocsonDR();
                DesignReviewPage.SubmitDR();
            }
        }
        //Close Documents on DR
        public void NotClosingSurveyorDocsDR()
        {
            WaitForElementonSurveyor(DesignReviewPage.DRStatus);
            if (DesignReviewPage.DRStatus.Text.Contains("Outstanding") || DesignReviewPage.DRStatus.Text.Contains("Not required"))
            {
                DesignReviewPage.OpenDR();
                DesignReviewPage.DesignReviewAssessment();
                NotCloseDocsonDR();
                DesignReviewPage.SaveDR();
            }
            else
            {
                DesignReviewPage.OpenDR();
                //Thread.Sleep(500);
                CloseSpinnerOnSurveyorPage();
                CloseSpinnerOnSurveyorDiv();
                //Thread.Sleep(500);
                WaitForElementonSurveyor(DesignReviewPage.NextButton);
                DesignReviewPage.NextButton.Click();
                NotCloseDocsonDR();
                DesignReviewPage.SaveDR();
            }
        }

        //Verify Closed Documents on DR 
        public void VerifyClosedSurveyorDocsOnDR()
        {
            DesignReviewPage.OpenDR();
            //Thread.Sleep(500);
            CloseSpinnerOnSurveyorPage();
            //Thread.Sleep(500);
            WaitForElementonSurveyor(DesignReviewPage.NextButton);
            DesignReviewPage.NextButton.Click();
            SurveyorDocs.ShowClosedDocuments();
            DesignReviewPage.SubmitDR();
        }

        //Close Docs on DR
        public void CloseDocsonDR()
        {
            WaitForLoadElements(SurveyorDocs.DocsTabs);
            if (SurveyorDocs.DocsTabs.Count > 0)
            {
                // if it is open on Site Inspection or SRA Close all documents 
                for (int i = 0; i < SurveyorDocs.DocsTabs.Count; i++)
                {
                    var TabStatus = SurveyorDocs.DocsTabs[i].GetAttribute("aria-expanded");
                    Assert.IsTrue(TabStatus == "false");
                    if (SurveyorDocs.RaisedDesignDocPartOf.Contains("Inspection") || SurveyorDocs.RaisedDesignDocPartOf.Contains("Site Risk Assessment") || SurveyorDocs.RaisedDesignDocPartOf.Contains("Design Review"))
                    {
                        SurveyorDocs.CloseDesignDocuments(i);
                    }
                    else
                    {
                        SurveyorDocs.NotClosingDesignDocuments(i);
                    }

                    if (SurveyorDocs.RaisedDesignDocPartOf.Contains("Inspection") || SurveyorDocs.RaisedDesignDocPartOf.Contains("Site Risk Assessment") || SurveyorDocs.RaisedDesignDocPartOf.Contains("Design Review"))
                    {
                        SurveyorDocs.CloseCompletionDocs(i);
                    }
                    else
                    {
                        SurveyorDocs.NotClosingCompletionDocuments(i);
                    }

                    if (SurveyorDocs.RaisedDesignDocPartOf.Contains("Inspection") || SurveyorDocs.RaisedDesignDocPartOf.Contains("Site Risk Assessment") || SurveyorDocs.RaisedDesignDocPartOf.Contains("Design Review"))
                    {
                        SurveyorDocs.CloseDesignItems(i);
                    }
                    else
                    {
                        SurveyorDocs.NotClosingDesignItems(i);
                    }
                    if (SurveyorDocs.RaisedDesignDocPartOf.Contains("Inspection") || SurveyorDocs.RaisedDesignDocPartOf.Contains("Site Risk Assessment") || SurveyorDocs.RaisedDesignDocPartOf.Contains("Design Review"))
                    {
                        SurveyorDocs.CloseRiskItems(i);
                    }
                    else
                    {
                        SurveyorDocs.NotClosingRiskItems(i);
                    }
                }
            }
        }

        //Not Close Docs on DR 
        public void NotCloseDocsonDR()
        {
            WaitForLoadElements(SurveyorDocs.DocsTabs);
            if (SurveyorDocs.DocsTabs.Count > 0)
            {
                for (int i = 0; i < SurveyorDocs.DocsTabs.Count; i++)
                {
                    var TabStatus = SurveyorDocs.DocsTabs[i].GetAttribute("aria-expanded");
                    Assert.IsTrue(TabStatus == "false");

                    if (SurveyorDocs.RaisedDesignDocPartOf.Contains("Refurbishment Assessment") || SurveyorDocs.RaisedDesignDocPartOf.Contains("Engineering Review"))
                    {
                        SurveyorDocs.NotClosingDesignDocuments(i);
                    }
                    if (SurveyorDocs.RaisedDesignDocPartOf.Contains("Refurbishment Assessment") || SurveyorDocs.RaisedDesignDocPartOf.Contains("Engineering Review"))
                    {
                        SurveyorDocs.NotClosingCompletionDocuments(i);
                    }
                    if (SurveyorDocs.RaisedDesignDocPartOf.Contains("Refurbishment Assessment") || SurveyorDocs.RaisedDesignDocPartOf.Contains("Engineering Review"))
                    {
                        SurveyorDocs.NotClosingDesignItems(i);
                    }
                    if (SurveyorDocs.RaisedDesignDocPartOf.Contains("Refurbishment Assessment") || SurveyorDocs.RaisedDesignDocPartOf.Contains("Engineering Review"))
                    {
                        SurveyorDocs.NotClosingRiskItems(i);
                    }
                }
            }

        }
    }
}