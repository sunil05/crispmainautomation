﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.PageObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace CrispAutomation.Pages
{
    public class SurveyorDocsRA : Support.Pages
    {
        public IWebDriver wdriver;
        public SurveyorDocsRA(ISearchContext driver)
        {
            PageFactory.InitElements(driver, this);
            wdriver = (RemoteWebDriver)driver;
        }

        //Opend Documents on RA 
        public void OpenSurveyorDocsRA()
        {
            WaitForElementonSurveyor(RefurbishmentAssessmentPage.RAStatus);

            if (RefurbishmentAssessmentPage.RAStatus.Text.Contains("Outstanding"))
            {
                RefurbishmentAssessmentPage.OpenRA();
                WaitForElementonSurveyor(RefurbishmentAssessmentPage.AlertStatusonEdit);
                Assert.IsTrue(RefurbishmentAssessmentPage.AlertStatusonEdit.Displayed);
                RefurbishmentAssessmentPage.SiteOverviewSection();
                RefurbishmentAssessmentPage.SraEditPage();
                RefurbishmentAssessmentPage.AssessmentScoresSection();
                SurveyorDocs.OpenDocuments();
                RefurbishmentAssessmentPage.SubmitRA();
            }
            else
            {
                RefurbishmentAssessmentPage.OpenRA();
                //Thread.Sleep(500);
                CloseSpinnerOnSurveyorPage();
                //Thread.Sleep(500);
                //Thread.Sleep(500);
                CloseSpinnerOnSurveyorPage();
                //Thread.Sleep(500);
                WaitForElementonSurveyor(RefurbishmentAssessmentPage.NextButton);
                RefurbishmentAssessmentPage.NextButton.Click();

                SurveyorDocs.OpenDocuments();
                RefurbishmentAssessmentPage.SubmitRA();
            }

        }
        //Close Documents on RA
        public void CloseSurveyorDocsRA()
        {
            WaitForElementonSurveyor(RefurbishmentAssessmentPage.RAStatus);
            if (RefurbishmentAssessmentPage.RAStatus.Text.Contains("Outstanding") || RefurbishmentAssessmentPage.RAStatus.Text.Contains("Not required"))
            {
                // 
                RefurbishmentAssessmentPage.OpenRA();
                WaitForElementonSurveyor(RefurbishmentAssessmentPage.AlertStatusonEdit);
                Assert.IsTrue(RefurbishmentAssessmentPage.AlertStatusonEdit.Displayed);
                RefurbishmentAssessmentPage.SiteOverviewSection();
                RefurbishmentAssessmentPage.SraEditPage();
                RefurbishmentAssessmentPage.AssessmentScoresSection();
                SurveyorDocs.CloseDocuments();
                RefurbishmentAssessmentPage.SubmitRA();
            }
            else
            {
                RefurbishmentAssessmentPage.OpenRA();
                //Thread.Sleep(500);
                CloseSpinnerOnSurveyorPage();
                //Thread.Sleep(500);
                //Thread.Sleep(500);
                CloseSpinnerOnSurveyorPage();
                //Thread.Sleep(500);
                WaitForElementonSurveyor(RefurbishmentAssessmentPage.NextButton);
                RefurbishmentAssessmentPage.NextButton.Click();

                SurveyorDocs.CloseDocuments();
                RefurbishmentAssessmentPage.SubmitRA();
            }
        }
        //Not Closing Documents on RA
        public void NotClosingSurveyorDocsRA()
        {
            WaitForElementonSurveyor(RefurbishmentAssessmentPage.RAStatus);
            if (RefurbishmentAssessmentPage.RAStatus.Text.Contains("Outstanding") || RefurbishmentAssessmentPage.RAStatus.Text.Contains("Not required"))
            {
                RefurbishmentAssessmentPage.OpenRA();
                WaitForElementonSurveyor(RefurbishmentAssessmentPage.AlertStatusonEdit);
                Assert.IsTrue(RefurbishmentAssessmentPage.AlertStatusonEdit.Displayed);
                RefurbishmentAssessmentPage.SiteOverviewSection();
                RefurbishmentAssessmentPage.SraEditPage();
                RefurbishmentAssessmentPage.AssessmentScoresSection();
                SurveyorDocs.NotClosingDocuments();
                RefurbishmentAssessmentPage.SaveRA();
            }
            else
            {
                RefurbishmentAssessmentPage.OpenRA();
                //Thread.Sleep(500);
                CloseSpinnerOnSurveyorPage();
                //Thread.Sleep(500);
                //Thread.Sleep(500);
                CloseSpinnerOnSurveyorPage();
                //Thread.Sleep(500);
                WaitForElementonSurveyor(RefurbishmentAssessmentPage.NextButton);
                RefurbishmentAssessmentPage.NextButton.Click();
                SurveyorDocs.NotClosingDocuments();
                RefurbishmentAssessmentPage.SaveRA();
            }
        }
        //Verify Closed Documents on RA 
        public void VerifyClosedSurveyorDocsOnRA()
        {
            RefurbishmentAssessmentPage.OpenRA();
            //Thread.Sleep(500);
            CloseSpinnerOnSurveyorPage();
            //Thread.Sleep(500);
            WaitForElementonSurveyor(RefurbishmentAssessmentPage.NextButton);
            RefurbishmentAssessmentPage.NextButton.Click();
            SurveyorDocs.ShowClosedDocuments();
            RefurbishmentAssessmentPage.SubmitRA();
        }

        //Close Docs on RA
        public void CloseDocsonRA()
        {
            WaitForLoadElements(SurveyorDocs.DocsTabs);
            if (SurveyorDocs.DocsTabs.Count > 0)
            {
                // if it is open on Site Inspection or SRA Close all documents 
                for (int i = 0; i < SurveyorDocs.DocsTabs.Count; i++)
                {
                    var TabStatus = SurveyorDocs.DocsTabs[i].GetAttribute("aria-expanded");
                    Assert.IsTrue(TabStatus == "false");
                    if (SurveyorDocs.RaisedDesignDocPartOf.Contains("Refurbishment Assessment"))
                    {
                        SurveyorDocs.CloseDesignDocuments(i);
                    }
                    else
                    {
                        SurveyorDocs.NotClosingDesignDocuments(i);
                    }

                    if (SurveyorDocs.RaisedDesignDocPartOf.Contains("Inspection") || SurveyorDocs.RaisedDesignDocPartOf.Contains("Refurbishment Assessment"))
                    {
                        SurveyorDocs.CloseCompletionDocs(i);
                    }
                    else
                    {
                        SurveyorDocs.NotClosingDesignDocuments(i);
                    }

                    if (SurveyorDocs.RaisedDesignDocPartOf.Contains("Refurbishment Assessment"))
                    {
                        SurveyorDocs.CloseDesignItems(i);
                    }
                    else
                    {
                        SurveyorDocs.NotClosingDesignItems(i);
                    }
                    if (SurveyorDocs.RaisedDesignDocPartOf.Contains("Inspection") || SurveyorDocs.RaisedDesignDocPartOf.Contains("Refurbishment Assessment"))
                    {
                        SurveyorDocs.CloseRiskItems(i);
                    }
                }
            }
        }

        //Not Close Docs on RA
        public void NotCloseDocsonRA()
        {
            WaitForLoadElements(SurveyorDocs.DocsTabs);
            if (SurveyorDocs.DocsTabs.Count > 0)
            {
                for (int i = 0; i < SurveyorDocs.DocsTabs.Count; i++)
                {
                    var TabStatus = SurveyorDocs.DocsTabs[i].GetAttribute("aria-expanded");
                    Assert.IsTrue(TabStatus == "false");
                    if (SurveyorDocs.RaisedDesignDocPartOf.Contains("Design Review") || SurveyorDocs.RaisedDesignDocPartOf.Contains("Engineering Review") || SurveyorDocs.RaisedDesignDocPartOf.Contains("Site Risk Assessment") || SurveyorDocs.RaisedDesignDocPartOf.Contains("Inspection"))
                    {
                        SurveyorDocs.NotClosingDesignDocuments(i);
                    }
                    if (SurveyorDocs.RaisedDesignDocPartOf.Contains("Design Review") || SurveyorDocs.RaisedDesignDocPartOf.Contains("Engineering Review") || SurveyorDocs.RaisedDesignDocPartOf.Contains("Site Risk Assessment"))
                    {
                        SurveyorDocs.NotClosingCompletionDocuments(i);
                    }

                    if (SurveyorDocs.RaisedDesignDocPartOf.Contains("Design Review") || SurveyorDocs.RaisedDesignDocPartOf.Contains("Engineering Review") || SurveyorDocs.RaisedDesignDocPartOf.Contains("Site Risk Assessment") || SurveyorDocs.RaisedDesignDocPartOf.Contains("Inspection"))
                    {
                        SurveyorDocs.NotClosingDesignItems(i);
                    }
                    if (SurveyorDocs.RaisedDesignDocPartOf.Contains("Design Review") || SurveyorDocs.RaisedDesignDocPartOf.Contains("Engineering Review") || SurveyorDocs.RaisedDesignDocPartOf.Contains("Site Risk Assessment"))
                    {
                        SurveyorDocs.NotClosingRiskItems(i);
                    }
                }
            }
        }
    }
}
