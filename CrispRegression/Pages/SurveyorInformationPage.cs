﻿using CrispAutomation.Features;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.PageObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace CrispAutomation.Pages
{
     public class SurveyorInformationPage : Support.Pages
     {
        public IWebDriver wdriver;
        public SurveyorInformationPage(ISearchContext driver)
        {
            PageFactory.InitElements(driver, this);
            wdriver = (RemoteWebDriver)driver;
        }
        [FindsBy(How = How.XPath, Using = "//crisp-tabs//div//ul[@ref='tabHeader']/li[@class='au-target tab']//span//a[contains(text(),'Surveying Information')]")]
        public IWebElement SurveyingInformationTab;

        [FindsBy(How = How.XPath, Using = "//surveyor-information[@class='custom-element au-target']//crisp-tabs//ul[@ref='tabHeader']//li[@class='au-target tab']//a")]
        public IList<IWebElement> SurveyingInformationTabList;

        [FindsBy(How = How.XPath, Using = "//surveyor-information[@class='custom-element au-target']//crisp-tabs//ul[@ref='tabHeader']//li[@class='au-target tab']//a[contains(text(),'Overview')]")]
        public IWebElement OverviewTab;

        [FindsBy(How = How.XPath, Using = "//overview[@class='au-target']//div//crisp-card-header//div[@class='card-title']//span//span//crisp-button//button[@class='au-target waves-effect waves-light btn-flat']")]
        public IList<IWebElement> SetAsNotRequiredButtons;

        [FindsBy(How = How.XPath, Using = "//crisp-dialog[@class='au-target']//div[@class='au-target modal dialog brand default modal-has-header']//div[@class='modal-content']//crisp-input-text[@label='Reason for setting as not required ']//label[contains(@class,'au-target')]")]
        public IWebElement DetailsLabel;

        [FindsBy(How = How.XPath, Using = "//crisp-dialog[@class='au-target']//div[@class='au-target modal dialog brand default modal-has-header']//div[@class='modal-content']//crisp-input-text[@label='Reason for setting as not required ']//label[contains(@class,'au-target')]//following-sibling::input")]
        public IWebElement DetailsInput;

        [FindsBy(How = How.XPath, Using = "//crisp-dialog[@class='au-target']//div[@class='au-target modal dialog brand default modal-has-header']//crisp-footer//crisp-button//span/button[@class='au-target waves-effect waves-light btn'][text()='Save']")]
        public IWebElement SaveButton;

        public void SetNotRequiredOnAssessments()
        {
            WaitForElement(SurveyingInformationTab);
            SurveyingInformationTab.Click();
            WaitForElements(SurveyingInformationTabList);
            if (SurveyingInformationTabList.Count > 0)
            {
                WaitForElement(OverviewTab);
                OverviewTab.Click();
                WaitForLoadElements(SetAsNotRequiredButtons);
                if(SetAsNotRequiredButtons.Count>0)
                {
                    foreach(var button in SetAsNotRequiredButtons)
                    {
                        CloseSpinneronDiv();
                        CloseSpinneronPage();
                        Thread.Sleep(500);
                        WaitForElement(button);
                        button.Click();
                        CloseSpinneronDiv();
                        WaitForElement(DetailsLabel);
                        DetailsLabel.Click();
                        WaitForElement(DetailsInput);
                        DetailsInput.SendKeys("Test Verification");
                        CloseSpinneronDiv();
                        CloseSpinneronPage();
                        WaitForElement(SaveButton);
                        SaveButton.Click();
                        Thread.Sleep(500);
                        CloseSpinneronDiv();
                        CloseSpinneronPage();
                        Thread.Sleep(500);
                    }
                }
            }           
        }
        public void VerifySiteAssessmentStatus()
        {
            if (Statics.ConstructionType.Any(x => x.Contains("New Build")) && Statics.ConstructionType.Any(x => x.Contains("Conversion")))
            {
                CloseSpinnerOnSurveyorDiv();
                CloseSpinnerOnSurveyorPage();
                Thread.Sleep(500);
                WaitForElementonSurveyor(SiteRiskAssessmentPage.SiteRiskAssessmentOption);
                WaitForElement(SiteRiskAssessmentPage.SRAStatus);
                Assert.IsTrue(SiteRiskAssessmentPage.SRAStatus.Text.Contains("Not required"));
                WaitForElementonSurveyor(RefurbishmentAssessmentPage.RefurbishmentAssessmentOption);
                WaitForElement(RefurbishmentAssessmentPage.RAStatus);
                Assert.IsTrue(RefurbishmentAssessmentPage.RAStatus.Text.Contains("Not required"));
            }
            else if (Statics.ConstructionType.Any(x => x.Contains("New Build")))
            {
                WaitForElementonSurveyor(SiteRiskAssessmentPage.SiteRiskAssessmentOption);
                WaitForElement(SiteRiskAssessmentPage.SRAStatus);
                Assert.IsTrue(SiteRiskAssessmentPage.SRAStatus.Text.Contains("Not required"));
                WaitForElementonSurveyor(RefurbishmentAssessmentPage.RefurbishmentAssessmentOption);
                WaitForElement(RefurbishmentAssessmentPage.RAStatus);
                Assert.IsTrue(RefurbishmentAssessmentPage.RAStatus.Text.Contains("Not required"));
            }
            else if (Statics.ConstructionType.Any(x => x.Contains("Conversion")))
            {
                WaitForElementonSurveyor(RefurbishmentAssessmentPage.RefurbishmentAssessmentOption);
                WaitForElement(RefurbishmentAssessmentPage.RAStatus);
                Assert.IsTrue(RefurbishmentAssessmentPage.RAStatus.Text.Contains("Not required"));
                WaitForElementonSurveyor(SiteRiskAssessmentPage.SiteRiskAssessmentOption);
                WaitForElement(SiteRiskAssessmentPage.SRAStatus);
                Assert.IsTrue(SiteRiskAssessmentPage.SRAStatus.Text.Contains("Not required"));
            }

            if (Statics.ProductNameList.Any(x => x.Contains("High Value")) || Statics.BCProducts.Any(x => x.Contains("Building Control")) || Statics.Plots.Count > 100)
            {
                WaitForElementonSurveyor(DesignReviewPage.DesignReviewOption);
                WaitForElement(DesignReviewPage.DRStatus);
                Assert.IsTrue(DesignReviewPage.DRStatus.Text.Contains("Not required"));
            }
            else
            {
                WaitForElementonSurveyor(DesignReviewPage.DesignReviewOption);
                WaitForElement(DesignReviewPage.DRStatus);
                Assert.IsTrue(DesignReviewPage.DRStatus.Text.Contains("Not required"));
            }

            if (Statics.ProductNameList.Any(x => x.Contains("High Value")) || Statics.Plots.Count > 6)
            {
                WaitForElementonSurveyor(EngineerReviewPage.EngineerReviewOption);
                WaitForElement(EngineerReviewPage.ERStatus);
                Assert.IsTrue(EngineerReviewPage.ERStatus.Text.Contains("Not required"));
            }
            else
            {
                WaitForElementonSurveyor(EngineerReviewPage.EngineerReviewOption);
                WaitForElement(EngineerReviewPage.ERStatus);
                Assert.IsTrue(EngineerReviewPage.ERStatus.Text.Contains("Not required"));
            }
            WaitForElementonSurveyor(SiteInspectionPage.LastInspectionStatus);
            WaitForElement(SiteInspectionPage.LastInspectionStatus);
            Assert.IsTrue(SiteInspectionPage.LastInspectionStatus.Text.Contains("No updates yet, please provide one."));

        }
    }
}
