﻿using OpenQA.Selenium;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.PageObjects;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using System.Threading;
using ExcelDataReader;
using LinqToExcel;
using DataTable = Microsoft.Office.Interop.Excel.DataTable;
using System.Windows.Forms;
using LinqToExcel.Extensions;
using Microsoft.Build.BuildEngine;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.Extensions;
using CrispAutomation.Features;
using CrispAutomation.Support;

namespace CrispAutomation.Pages
{
    public class FinancesPage : Support.Pages
    {
        public IWebDriver wdriver;

        public FinancesPage(ISearchContext driver)
        {
            PageFactory.InitElements(driver, this);
            wdriver = (RemoteWebDriver) driver;
        }
        //Filter Operation Elements 

        [FindsBy(How = How.XPath, Using = "//i[@class='au-target fa fa-gbp']")]
        public IWebElement Finances;

        [FindsBy(How = How.XPath,
            Using = "//ul[@class='dropdown-content au-target active']/li/a[text()='Bank statement']")]
        public IWebElement BankStatement;

        [FindsBy(How = How.XPath, Using = "//crisp-header-title/div[@class='title'][text()='Bank statement']")]
        public IWebElement BankStatementTitle;

        [FindsBy(How = How.XPath, Using = "//crisp-header-button[@click.delegate='filter()']//span//button")]
        public IWebElement FilterButton;

        [FindsBy(How = How.XPath, Using = "//crisp-card-content[@class='au-target']//crisp-input-bool[@class='au-target']//input[@class='au-target']")]
        public IList<IWebElement> FilterCheckBoxes;

        [FindsBy(How = How.XPath, Using = "//crisp-card-content[@class='au-target']//crisp-input-bool[@class='au-target']//label[@class='au-target']")]
        public IList<IWebElement> FilterCheckBoxesLabels;

        [FindsBy(How = How.XPath, Using = "//crisp-footer-button[@click.delegate='apply()'][@class='au-target']//button")]
        public IWebElement FilterApplyButton;

        [FindsBy(How = How.XPath, Using = "//crisp-header-button[@click.delegate='performMatch()']//span//button[@class='au-target waves-effect waves-light btn-flat']")]
        public IWebElement MainAllocateButton;

        [FindsBy(How = How.XPath, Using = "//crisp-button[@click.delegate='selectAll()']//span//button[text()='Select Top 250'][@class='au-target waves-effect waves-light btn']")]
        public IWebElement SelectAllButton;        

        //Bank Statement Import 

        [FindsBy(How = How.XPath, Using = "//crisp-header-button[@click.delegate='performImport()']//span//button[@class='au-target waves-effect waves-light btn-flat']")]
        public IWebElement ImportButton;

        [FindsBy(How = How.XPath, Using = "//ai-dialog-container//div//crisp-dialog//crisp-header//nav//div[@class='nav-wrapper au-target']")]
        public IWebElement BankAccountImportDiv;

        [FindsBy(How = How.XPath, Using = "//ul//li[contains(@id,'crisp-input-radio')]//label[text()='Statement']")]
        public IWebElement ImportTypeRadioButton;

        [FindsBy(How = How.XPath, Using = "//crisp-picker[@label='Bank Account']//div[contains(@id,'crisp-picker')]//div//input[@class='select-dropdown']")]
        public IWebElement BankAccountsDropdown;

        [FindsBy(How = How.XPath, Using = "//crisp-header-button[@click.delegate='showBankImport()']//span//button")]
        public IWebElement StatementImportButton;

        [FindsBy(How = How.XPath, Using = "//ul[@class='dropdown-content select-dropdown active']/li/span[text()='IBA']")]
        public IWebElement BankAccountsDropdownInputIBA;

        [FindsBy(How = How.XPath, Using = "//ul[@class='dropdown-content select-dropdown active']/li/span[text()='Escrow']")]
        public IWebElement BankAccountsDropdownInputEscrow;

        [FindsBy(How = How.XPath, Using = "//div/crisp-input-text[@label='Comment']//div//label")]
        public IWebElement CommentLabel;

        [FindsBy(How = How.XPath, Using = "//div/crisp-input-text[@label='Comment']//div//input")]
        public IWebElement CommentInput;

        [FindsBy(How = How.XPath, Using = "//crisp-input-file[@label='Select file...']//div//div//input[@ref='fileInput']")]
        public IWebElement BankStatementFile;
      
        [FindsBy(How = How.XPath, Using = "//crisp-button[@click.delegate='import()']//button[@class='au-target waves-effect waves-light btn-flat'][text()='Import']")]
        public IWebElement ImportUploadButton;        

        [FindsBy(How = How.XPath,Using = "//statement-import//crisp-card/crisp-card-content/div/copy-paste-control/crisp-input-textarea[@ref='copyPasteControl']/div/label")]
        public IWebElement Copyandpastelabel;

        [FindsBy(How = How.XPath,Using = "//statement-import//crisp-card/crisp-card-content/div/copy-paste-control/crisp-input-textarea[@ref='copyPasteControl']/div/textarea")]
        public IWebElement CopyandpasteInput;

        [FindsBy(How = How.XPath,Using ="//div[@class='au-target crisp-list-table has-header']//div[@ref='theTable']//table[@class='au-target']//tbody[@class='collection']//tr[contains(@class,'au-target collection-item')]")]
        public IList<IWebElement> TransactionsList;

        [FindsBy(How = How.XPath, Using = "//div[@class='au-target crisp-list-table has-header']//div[@ref='theTable']//table[@class='au-target']//tbody[@class='collection']//tr[@class='au-target collection-item']//td[@class='value']")]
        public IList<IWebElement> TransactionsValue;

        [FindsBy(How = How.XPath,
            Using = "//table[@class='au-target']//tbody//tr//td[@class='icon-button']//crisp-button//span/button")]
        public IList<IWebElement> Editbutton;

        [FindsBy(How = How.XPath,Using = "//crisp-list[@title='Accounts']//ul[@class='au-target collection']//li//crisp-list-item")]
        public IList<IWebElement> AccountsList;

        [FindsBy(How = How.XPath, Using = "//crisp-card/crisp-card-header/div/span[text()='Matched account']")]
        public IWebElement MatchedAccount;

        [FindsBy(How = How.XPath, Using = "//crisp-button//span//button[text()='Ok']")] public IWebElement OkButton;

        [FindsBy(How = How.XPath, Using = "//table[@class='au-target']//tbody//tr[contains(@class,'au-target collection-item')]//td[@class='selectable']//crisp-input-bool//label[@class='au-target']")]
        public IList<IWebElement> checkBoxes;

        [FindsBy(How = How.XPath,  Using ="//crisp-dialog/div/div[@class='modal-footer']/crisp-footer/nav/div/crisp-button/span/button[text()='OK']")]
        public IWebElement ImportOkButton;

        //Allocate Payment Receipts Page 

        [FindsBy(How = How.XPath,Using = "//ul[@class='dropdown-content au-target active']/li/a[text()='Allocate Payment Receipts']")]
        public IWebElement AllocatePayments;

        [FindsBy(How = How.XPath,
            Using = "//crisp-header-title/div[@class='title'][contains(text(),'Allocate Payment Receipts')]")]
        public IWebElement AllocatePaymentsTitle;

        [FindsBy(How = How.XPath,Using = "//div[@class='card-content']//crisp-list[@list-title='Entries']//ul[@class='au-target collection has-header']//li[@class='au-target collection-item']//crisp-list-item")]
        public IList<IWebElement> PaymentList;

        [FindsBy(How = How.XPath, Using = "//div[@class='card-content']//crisp-list//ul[@class='au-target collection']//li//crisp-list-item//div//crisp-list-item-details/p[1][contains(text(),'Escrow')]")]
        public IList<IWebElement> EscrowPaymentList;
        
        [FindsBy(How = How.XPath, Using = "//crisp-card//crisp-card-content//div//div//dl//dd[@class='au-target']")]
        public IWebElement UnallocatedValue;

        [FindsBy(How = How.XPath, Using = "//crisp-card-content//crisp-input-text[@label='Comment']//div[@class='input-field']//label[text()='Comment']")]
        public IWebElement AllocatePaymentCommentLabel;

        [FindsBy(How = How.XPath, Using = "//crisp-card-content//crisp-input-text[@label='Comment']//div[@class='input-field']//Input")]
        public IWebElement AllocatePaymentCommentinput;

        [FindsBy(How = How.XPath,Using = "//crisp-list[@title='Accounts']//ul//li[@class='au-target collection-item']")]
        public IList<IWebElement> AllocatePaymentAccountsList;

        [FindsBy(How = How.XPath, Using = "//crisp-input-currency[@label='Escrow Amount']//div//label")]
        public IWebElement EscrowAmountLabel;

        [FindsBy(How = How.XPath, Using = "//crisp-input-currency[@label='Escrow Amount']//div//input")]
        public IWebElement EscrowAmountInput;

        [FindsBy(How = How.XPath, Using = "//crisp-input-currency[@label='Fee Amount']//div//label")]
        public IWebElement FeeAmountLabel;

        [FindsBy(How = How.XPath, Using = "//crisp-input-currency[@label='Fee Amount']//div//input")]
        public IWebElement FeeAmountInput;

        [FindsBy(How = How.XPath, Using = "//crisp-card//crisp-card-content//div//div//dl[2]//dd[1]")]
        public IWebElement EscrowAllocatedValue;

        [FindsBy(How = How.XPath, Using = "//crisp-card//crisp-card-content//div//div//dl[2]//dd[2]")]
        public IWebElement FeeAllocatedValue;

        [FindsBy(How = How.XPath, Using = "//crisp-button/span/button[text()='Add']")]
        public IWebElement AddButton;

        [FindsBy(How = How.XPath, Using = "//crisp-button//span//button[text()='Allocate'][@class='au-target waves-effect waves-light btn-flat']")]
        public IWebElement AllocateButton;

        [FindsBy(How = How.XPath, Using = "//crisp-input-currency[@label='Escrow Amount']//div//span[@class='formatted-value']")]
        public IWebElement AssignedEscrowAmount;

        [FindsBy(How = How.XPath, Using = "//crisp-input-currency[@label='Fee Amount']//div//span[@class='formatted-value']")]
        public IWebElement AssignedFeeAmount;

        [FindsBy(How = How.XPath, Using = "//crisp-button//span/button[text()='OK'][@class='au-target waves-effect waves-light btn']")]
        public IWebElement AllocateOkButton;

        [FindsBy(How = How.XPath, Using = "//div[@id='toast-container']/div[text()='The entries have been allocated']")]
        public IWebElement AllocationConfirm;

        //Transfer Payments Elements 
        [FindsBy(How = How.XPath, Using = "//ul[@class='dropdown-content au-target active']/li/a[text()='Transfer money']")]
        public IWebElement TransferMoneyOption;

        [FindsBy(How = How.XPath, Using = "//div/crisp-dialog[@class='au-target']/div[@class='au-target modal dialog brand default modal-fixed-footer modal-has-header']")]
        public IWebElement TransferMoneyDialogueWizard;

        [FindsBy(How = How.XPath, Using = "//div[@class='card-title']//span[contains(text(),'From')]//crisp-button[@class='right au-target']//span//button")]
        public IWebElement FromButton;

        [FindsBy(How = How.XPath, Using = "//div[@class='card-title']//span[contains(text(),'To')]//crisp-button[@class='right au-target']//span//button")]
        public IWebElement ToButton;

        [FindsBy(How = How.XPath, Using = "//ul//li//crisp-list-item//a//crisp-list-item-details//account-fees//table//tbody//tr[2]//td[3]")]
        public IList<IWebElement> TransferAccountsList;

        [FindsBy(How = How.XPath, Using = "//ul//li//crisp-list-item//a//crisp-list-item-details//account-fees//table//tbody//tr[2]//td[3]")]
        public IList<IWebElement> TransferAccountsList2;

        [FindsBy(How = How.XPath, Using = "//ul//li//crisp-list-item//a//crisp-list-item-details//account-fees//table//tbody//tr[2]//td[3]")]
        public IList<IWebElement> TransferToAccountsList;

        [FindsBy(How = How.XPath, Using = "//account-summary[@summary.bind='from & validate']/dl/dt[text()='Escrow paid']/following-sibling::dd[1]")]
        public IWebElement FromEscrowAmountPaid;

        [FindsBy(How = How.XPath, Using = "//account-summary[@summary.bind='from & validate']/dl/dt[text()='Fee paid']/following-sibling::dd[1]")]
        public IWebElement FromFeeAmountPaid;
        

        [FindsBy(How = How.XPath, Using = "//crisp-input-currency[@value.bind='fromEscrowAmount & validate']/div/label")]
        public IWebElement FromEscrowAmountLabel;

        [FindsBy(How = How.XPath, Using = "//crisp-input-currency[@value.bind='fromEscrowAmount & validate']/div/input")]
        public IWebElement FromEscrowAmountInput;
        
        [FindsBy(How = How.XPath, Using = "//crisp-input-currency[@value.bind='fromFeeAmount & validate']/div/label")]
        public IWebElement FromFeeAmountLabel;

        [FindsBy(How = How.XPath, Using = "//crisp-input-currency[@value.bind='fromFeeAmount & validate']/div/input")]
        public IWebElement FromFeeAmountInput;


        [FindsBy(How = How.XPath, Using = "//crisp-input-currency[@value.bind='toEscrowAmount & validate']/div/label")]
        public IWebElement ToEscrowAmountLabel;

        [FindsBy(How = How.XPath, Using = "//crisp-input-currency[@value.bind='toEscrowAmount & validate']/div/input")]
        public IWebElement ToEscrowAmountInput;

        [FindsBy(How = How.XPath, Using = "//crisp-input-currency[@value.bind='toFeeAmount & validate']/div/label")]
        public IWebElement ToFeeAmountLabel;

        [FindsBy(How = How.XPath, Using = "//crisp-input-currency[@value.bind='toFeeAmount & validate']/div/input")]
        public IWebElement ToFeeAmountInput;

        [FindsBy(How = How.XPath, Using = "//div[@class='modal-content']//crisp-input-text[@label='Comment']/div/label")]
        public IWebElement TransferCommentLabel;

        [FindsBy(How = How.XPath, Using = "//div[@class='modal-content']//crisp-input-text[@label='Comment']/div/input")]
        public IWebElement TransferCommentInput;

        [FindsBy(How = How.XPath, Using = "//crisp-button[@click.delegate='transfer()']//span//button[text()='Transfer'][@class='au-target waves-effect waves-light btn-flat']")]
        public IWebElement TransferButton;
        
        [FindsBy(How = How.XPath, Using = "//crisp-footer//crisp-button//span//button[text()='OK'][@class='au-target waves-effect waves-light btn']")]
        public IWebElement TransferOkButton;

        [FindsBy(How = How.XPath, Using = "//div[@id='toast-container']/div[text()='The transfer is complete']")]
        public IWebElement TransferComplete;

        //Make Direct Payment 

        [FindsBy(How = How.XPath, Using = "//account-summary[@summary.bind='to & validate']/dl/dt[text()='Fee due']/following-sibling::dd[1]")]
        public IWebElement ToRegFeeDue;

        [FindsBy(How = How.XPath, Using = "//account-summary[@summary.bind='to & validate']/dl/dt[text()='Escrow due']/following-sibling::dd[1]")]
        public IWebElement ToRegEscrowDue;


        //Apply Filtering on Statements 
        public void ApplyFilteronStatement()
        {
            WaitForElement(FilterButton);
            FilterButton.Click();
            WaitForElements(FilterCheckBoxes);
            for (int i=0; i<FilterCheckBoxes.Count -1; i++)
            {
                if (!FilterCheckBoxes[i].Selected)
                {
                    FilterCheckBoxesLabels[i].Click();
                }
            }
            WaitForElement(FilterApplyButton);
            FilterApplyButton.Click();
            if (checkBoxes.Count > 0)
            {
                AllocateExistingTransactions();
            }
        }
        public void AllocateExistingTransactions()
        {
            if (checkBoxes.Count > 0)
            {
                WaitForElement(SelectAllButton);
                SelectAllButton.Click();
                WaitForElements(checkBoxes);
                //for (int i = 0; i < checkBoxes.Count; i++)
                //{
                //    checkBoxes[i].Click();
                //}
                WaitForElement(MainAllocateButton);
                MainAllocateButton.Click();
                Thread.Sleep(500);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                WaitForElement(ImportOkButton);
                ImportOkButton.Click();
                Thread.Sleep(500);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                //Assert.IsTrue(TransactionsList.Count == 0);
            }
        }

        //Importing Bank Statements 

        public void BankStatementimportIBAAccount()
        {
            
            int numberOfTransactionRows = TransactionsList.Count;
            string IBAStatement = $@"31/10/2016	DAMAMERE 1 LTD  {Statics.OrderNumber} BGCFrom: 60-17-11 89531582	Inward Payment via FP		85,555.98
";
            WaitForElement(ImportButton);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            WaitForElementToClick(ImportButton);
            ImportButton.Click();
            CloseSpinneronDiv();
            CloseSpinneronPage();
            WaitForElement(BankAccountImportDiv);
            Assert.IsTrue(BankAccountImportDiv.Displayed,"Bank Account Dialogue Window is failed to open");
            WaitForElement(ImportTypeRadioButton);
            if(!ImportTypeRadioButton.Selected)
            {
                ImportTypeRadioButton.Click();
            }
            WaitForElement(BankAccountsDropdown);
            BankAccountsDropdown.Click();            
            WaitForElement(BankAccountsDropdownInputIBA);
            BankAccountsDropdownInputIBA.Click();            
            WaitForElement(CommentLabel);
            CommentLabel.Click();            
            WaitForElement(CommentInput);
            CommentInput.SendKeys("Importing IBA");               
            var fileInfo = new FileInfo(ExtensionMethods.GetAbsolutePath(@"TestData\Statements\IBA Import.xslx.xlsx"));
            BankStatementFile.SendKeys($"{fileInfo}");
            //BankStatementFile.SendKeys(@"\\tpgfile\NewCompany\IT\Department\QA Team\Current Projects\CRISP\CrispAutomation\CrispData\Statements\IBA Import.xslx.xlsx");
            //WaitForElement(Copyandpastelabel);
            //Copyandpastelabel.Click();
            //
            //WaitForElement(CopyandpasteInput);
            //CopyandpasteInput.Click();
            //// Excuting Java Script to set the value 
            //// ((IJavaScriptExecutor) wdriver).ExecuteScript("document.getElementByXPath('//div[@id='crisp-drawer-0']/crisp-card/crisp-card-content/div/copy-paste-control/crisp-input-textarea/div/textarea').value = arguments[0];",IBAStatement);
            //((IJavaScriptExecutor)wdriver).ExecuteScript($"arguments[0].value='31/10/2016	DAMAMERE 1 LTD {Statics.OrderNumber} BGCFrom: 60-17-11 89531582	Inward Payment via FP		85,555.98'", CopyandpasteInput);
            //CopyandpasteInput.SendKeys(OpenQA.Selenium.Keys.Enter);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            WaitForElement(ImportUploadButton);
            WaitForElementToClick(ImportUploadButton);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            ImportUploadButton.Click();
            WaitForElements(TransactionsList);
            //Thread.Sleep(1000);
            //int actualOriginalRows = numberOfTransactionRows + 1;
            //Assert.AreEqual(TransactionsList.Count, actualOriginalRows);
            if (TransactionsValue != null)
            {
                Assert.IsTrue(TransactionsList.Count > 0);
                //Assert.IsTrue(TransactionsValue.Any(x => x.Text == "£10,000,000.00"));
                //Assert.IsTrue(TransactionsValue.Count(x => x.Text == "£10,000,000.00") >= 1);
            }

        }

        public void BankStatementimportEscrowAccount()
        {
            string EscrowStatement = $@"31/10/2016	DAMAMERE 1 LTD {Statics.OrderNumber} BGCFrom: 60-17-11 89531582	Inward Payment via FP		85,555.98
";
            int numberOfTransactionRows = TransactionsList.Count;
            WaitForElement(ImportButton);
            ImportButton.Click();            
            WaitForElement(BankAccountImportDiv);
            Assert.IsTrue(BankAccountImportDiv.Displayed, "Bank Account Dialogue Window is failed to open");
            WaitForElement(ImportTypeRadioButton);
            if (!ImportTypeRadioButton.Selected)
            {
                ImportTypeRadioButton.Click();
            }
            WaitForElement(BankAccountsDropdown);
            BankAccountsDropdown.Click();                 
            WaitForElement(BankAccountsDropdownInputEscrow);            
            BankAccountsDropdownInputEscrow.Click();            
            WaitForElement(CommentLabel);            
            CommentLabel.Click();            
            WaitForElement(CommentInput);
            CommentInput.SendKeys("Importing Escrow");            
            var fileInfo = new FileInfo(ExtensionMethods.GetAbsolutePath(@"TestData\Statements\Escrow Import.xlsx"));
            BankStatementFile.SendKeys($"{fileInfo}");
            // BankStatementFile.SendKeys(@"\\tpgfile\NewCompany\IT\Department\QA Team\Current Projects\CRISP\CrispAutomation\CrispData\Statements\Escrow Import.xlsx");
            //WaitForElement(Copyandpastelabel);
            //Copyandpastelabel.Click();
            //
            //WaitForElement(CopyandpasteInput);
            //CopyandpasteInput.Click();
            //// Excuting Java Script to set the value 
            //// ((IJavaScriptExecutor) wdriver).ExecuteScript("document.getElementByXPath('//div[@id='crisp-drawer-0']/crisp-card/crisp-card-content/div/copy-paste-control/crisp-input-textarea/div/textarea').value = arguments[0];",IBAStatement);
            //((IJavaScriptExecutor)wdriver).ExecuteScript($"arguments[0].value='31/10/2016	DAMAMERE 1 LTD {Statics.OrderNumber} BGCFrom: 60-17-11 89531582	Inward Payment via FP		85,555.98'", CopyandpasteInput);
            //CopyandpasteInput.SendKeys(OpenQA.Selenium.Keys.Enter);
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            WaitForElement(ImportUploadButton);
            ImportUploadButton.Click();
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            WaitForElements(TransactionsList);            
            //int actualOriginalRows = numberOfTransactionRows + 1;
            //Assert.AreEqual(TransactionsList.Count, actualOriginalRows);
            if (TransactionsValue != null)
            {
                Assert.IsTrue(TransactionsList.Count > 0);
                //Assert.IsTrue(TransactionsValue.Any(x => x.Text == "£9,000,000.00"));
                //Assert.IsTrue(TransactionsValue.Count(x => x.Text == "£9,000,000.00") >= 1);
            }           

            //WaitForElement(Copyandpastelabel);
            //Copyandpastelabel.Click();
            //
            //WaitForElement(CopyandpasteInput);
            //CopyandpasteInput.Click();
            ////Using Clipboard Insert the data into textarea 
            ////Clipboard.SetText(EscrowStatement);
            ////CopyandpasteInput.SendKeys(OpenQA.Selenium.Keys.Control + "v");
            //((IJavaScriptExecutor)wdriver).ExecuteScript($"arguments[0].value='31/10/2016	DAMAMERE 1 LTD {Statics.OrderNumber} BGCFrom: 60-17-11 89531582	Inward Payment via FP		85,555.98'", CopyandpasteInput);
            //CopyandpasteInput.SendKeys(OpenQA.Selenium.Keys.Enter);
            //
            //WaitForElements(TransactionsList);
            //
            //int actualOriginalRows = numberOfTransactionRows + 1;
            //Assert.AreEqual(TransactionsList.Count, actualOriginalRows);
            //if (TransactionsValue != null)
            //{
            //    Assert.IsTrue(TransactionsValue.Any(x => x.Text == "£85,555.98"));
            //    Assert.IsTrue(TransactionsValue.Count(x => x.Text == "£85,555.98") >= 1);
            //}
        }

        public void OriginalImport()
        {
            for (int i = 0; i <= TransactionsList.Count; i++)
            {
                WaitForElement(TransactionsList[i]);
                TransactionsList[i].Click();
                CloseSpinneronDiv();
                
                WaitForElements(Editbutton);
                Editbutton[i].Click();
                WaitForElements(AccountsList);
                
                CloseSpinneronDiv();
                if (AccountsList.Count > 0)
                {
                    WaitForElement(AccountsList[1]);
                    
                    CloseSpinneronDiv();
                    AccountsList[1].Click();
                    WaitForElement(MatchedAccount);
                    Assert.IsTrue(MatchedAccount.Displayed);
                    WaitForElement(OkButton);
                    OkButton.Click();
                    
                }
            }
            //BankStatementimportIBAAccount();
            //BankStatementimportEscrowAccount();
            
            if (checkBoxes.Count > 0)
            {
                WaitForElement(SelectAllButton);
                SelectAllButton.Click();
                //WaitForElements(checkBoxes);
                //for (int i = 0; i < checkBoxes.Count; i++)
                //{
                //    checkBoxes[i].Click();
                   
                //}
                
                
                CloseSpinneronDiv();
            }
        }
        public void AllocatePayment()
        {
            
            if (PaymentList.Count > 0)
            {
                CloseSpinneronDiv();
                
                WaitForElement(PaymentList[0]);
                CloseSpinneronDiv();
                PaymentList[0].Click();
                WaitForElement(UnallocatedValue);
                var UnallocatedPaymentvalue = UnallocatedValue.Text.Replace("£", "");
                decimal actualUnallocatedValue = Convert.ToDecimal(UnallocatedPaymentvalue);
                if (actualUnallocatedValue > 0)
                {
                    WaitForElement(AllocatePaymentCommentLabel);
                    AllocatePaymentCommentLabel.Click();
                    WaitForElement(AllocatePaymentCommentinput);
                    AllocatePaymentCommentinput.SendKeys("AllocatePayments");
                    //WaitForElement(AddButton);
                    //AddButton.Click();
                    //CloseSpinneronDiv();
                    
                    WaitForElements(AllocatePaymentAccountsList);
                    if (AllocatePaymentAccountsList.Count > 0)
                    {
                        
                        var escrowPayment = actualUnallocatedValue / 2;
                        var feePayment = actualUnallocatedValue / 2;
                        AllocatePaymentAccountsList[0].Click();
                        WaitForElement(EscrowAmountLabel);
                        EscrowAmountLabel.Click();
                        WaitForElement(EscrowAmountInput);
                        EscrowAmountInput.SendKeys(escrowPayment.ToString(CultureInfo.InvariantCulture));
                        WaitForElement(FeeAmountLabel);
                        FeeAmountLabel.Click();
                        WaitForElement(FeeAmountInput);
                        FeeAmountInput.SendKeys(feePayment.ToString(CultureInfo.InvariantCulture));
                    }
                    //WaitForElement(EscrowAllocatedValue);
                    //var EscrowAllocated = EscrowAllocatedValue.Text.Replace("£", "");
                    //decimal actualEscrowAllocatedValue = Convert.ToDecimal(EscrowAllocated);
                    //WaitForElement(AssignedEscrowAmount);
                    //var assignedEscrowAmountValue = AssignedEscrowAmount.Text.Replace("£", "");
                    //decimal actualAssignedEscrowAmountValue = Convert.ToDecimal(assignedEscrowAmountValue);
                    //Assert.AreEqual(actualAssignedEscrowAmountValue, actualEscrowAllocatedValue);

                    //WaitForElement(FeeAllocatedValue);
                    //var FeeAmountAllocated = FeeAllocatedValue.Text.Replace("£", "");
                    //decimal actualFeeAllocatedValue = Convert.ToDecimal(FeeAmountAllocated);
                    //WaitForElement(AssignedFeeAmount);
                    //var assignedFeeAmountValue = AssignedFeeAmount.Text.Replace("£", "");
                    //decimal actualAssignedFeeAmountValue = Convert.ToDecimal(assignedFeeAmountValue);
                    //Assert.AreEqual(actualAssignedFeeAmountValue, actualFeeAllocatedValue);
                    WaitForElement(AllocateButton);
                    AllocateButton.Click();
                }
            }
        }
        
        //Transfer Money Scenario
        public void TransferMoney()
        {
            
            //Selecting from List Accounts 
            FromButton.Click();
            CloseSpinneronDiv();
            
            WaitForElements(TransferAccountsList);
            var fromAccount = TransferAccountsList 
                        .Select(tal => new {balance = Convert.ToDecimal(tal.Text.Replace("£", "")), element = tal})
                        .Where(tal => tal.balance > 0)
                        .ToList();            
            CloseSpinneronDiv();
                        
            var fromAccountInput = fromAccount.First();
            
            fromAccountInput.element.Click();
            CloseSpinneronDiv();   
            //Selecting TO List Accounts 

            WaitForElement(ToButton);
            ToButton.Click();
            CloseSpinneronDiv();
            
            WaitForElements(TransferToAccountsList);
            var toAccount = TransferToAccountsList
                .Select(tal => new { balance = Convert.ToDecimal(tal.Text.Replace("£", "")), element = tal })
                .Where(tal => tal.balance >= 0)
                .ToList();
            var toAccount2 = TransferToAccountsList
                .Select(tal => new { balance = Convert.ToDecimal(tal.Text.Replace("£", "")), element = tal })
                .Where(tal => tal.balance < fromAccountInput.balance)
                .ToList();

            CloseSpinneronDiv();
            
            if (toAccount.Count > 0)
            {
                var toAccountImput = toAccount.LastOrDefault();                
                CloseSpinneronDiv();                
                toAccountImput.element.Click();
            }
            else
            {
               // ((IJavaScriptExecutor)wdriver).ExecuteScript("arguments[0].scrollIntoView(true);", toAccount);
                //Actions actions = new Actions(wdriver);
                //actions.MoveToElement(toAccount);
                //actions.Perform();
                var toAccountImput = toAccount2.LastOrDefault();                
                toAccountImput.element.Click();
            }

            CloseSpinneronDiv();
            //Perform TranferMoney operation 
            CurrenyTransfer();            
            WaitForElement(TransferCommentLabel);
            TransferCommentLabel.Click();
            WaitForElement(TransferCommentInput);
            TransferCommentInput.SendKeys("Transfering Amount");
        }

        public void CurrenyTransfer()
        {
            //Retrieving Escrow Amount Paid Value             
            WaitForElement(FromEscrowAmountPaid);
            var escrowAmountPaidValue = FromEscrowAmountPaid.Text.Replace("£", "");
            decimal actualEscrowAmountPaidValue = (Convert.ToDecimal(escrowAmountPaidValue)) / 2;
            //Retrieving Fee Amount Paid Value             
            WaitForElement(FromFeeAmountPaid);
            var feeAmountPaidValue = FromFeeAmountPaid.Text.Replace("£", "");
            decimal actualFeeAmountPaidValue = (Convert.ToDecimal(feeAmountPaidValue)) / 2;

            if (actualEscrowAmountPaidValue > 0)
            {
                //Enter the value on From Account Escrow Amount field 
                WaitForElement(FromEscrowAmountLabel);
                FromEscrowAmountLabel.Click();
                WaitForElement(FromEscrowAmountInput);
                FromEscrowAmountInput.SendKeys(actualEscrowAmountPaidValue.ToString(CultureInfo.InvariantCulture));
            }
            if (actualFeeAmountPaidValue > 0)
            {

                //Enter the value on From Account Fee Amount field 
                WaitForElement(FromFeeAmountLabel);
                FromFeeAmountLabel.Click();
                WaitForElement(FromFeeAmountInput);
                FromFeeAmountInput.SendKeys(actualFeeAmountPaidValue.ToString(CultureInfo.InvariantCulture));
            }
            if (actualEscrowAmountPaidValue > 0)
            {
                //Enter the value on To Account Escrow Amount field 
                WaitForElement(ToEscrowAmountLabel);
                ToEscrowAmountLabel.Click();
                WaitForElement(ToEscrowAmountInput);
                ToEscrowAmountInput.SendKeys(actualEscrowAmountPaidValue.ToString(CultureInfo.InvariantCulture));
            }
            if (actualFeeAmountPaidValue > 0)
            {
                //Enter the value on To Account Fee Amount field 
                WaitForElement(ToFeeAmountLabel);
                ToFeeAmountLabel.Click();
                WaitForElement(ToFeeAmountInput);
                ToFeeAmountInput.SendKeys(actualFeeAmountPaidValue.ToString(CultureInfo.InvariantCulture));
            }
        }
        public void MakeDirectPyament()
        {
            decimal d = 1.01m;
            //Retrieving Escrow Amount Paid Value 
            
            WaitForElement(ToRegEscrowDue);
            var escrowAmountValue = ToRegEscrowDue.Text.Replace("£", "");            
            decimal actualEscrowAmountValue = (Convert.ToDecimal(escrowAmountValue))+d;
            //Retrieving Fee Amount Paid Value 
            
            WaitForElement(ToRegFeeDue);
            var feeAmountValue = ToRegFeeDue.Text.Replace("£", ""); 
            decimal actualFeeAmountValue = (Convert.ToDecimal(feeAmountValue))+d;
            

            //Enter the value on To Account Fee Amount field 
            WaitForElement(ToFeeAmountLabel);
            ToFeeAmountLabel.Click();
            WaitForElement(ToFeeAmountInput);
            ToFeeAmountInput.SendKeys(actualFeeAmountValue.ToString(CultureInfo.InvariantCulture));

            //Enter the value on To Account Escrow Amount field 
            WaitForElement(ToEscrowAmountLabel);
            ToEscrowAmountLabel.Click();
            WaitForElement(ToEscrowAmountInput);
            ToEscrowAmountInput.SendKeys(actualEscrowAmountValue.ToString(CultureInfo.InvariantCulture));

            // Enter Comment
            WaitForElement(TransferCommentLabel);
            TransferCommentLabel.Click();
            WaitForElement(TransferCommentInput);
            TransferCommentInput.SendKeys("Making Registration Fee");
            
            WaitForElement(TransferButton);
            TransferButton.Click();
            
            WaitForElement(TransferOkButton);
            TransferOkButton.Click();
            
            CloseSpinneronDiv();

        }

    }
}
