﻿using OpenQA.Selenium;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.PageObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using NUnit.Framework;
using OpenQA.Selenium.Support.UI;
using System.IO;
using CrispAutomation.Support;

namespace CrispAutomation.Pages
{
    public class AddRatingToPersonPage : Support.Pages
    {
        public IWebDriver wdriver;
        public AddRatingToPersonPage(ISearchContext driver)
        {
            PageFactory.InitElements(driver, this);
            wdriver = (RemoteWebDriver)driver;
        }
        [FindsBy(How = How.XPath, Using = "//crisp-tabs/div/ul/li[@class='au-target tab']/span/a[contains(text(),'Rating')]")]
        public IWebElement RatingOption;

        [FindsBy(How = How.XPath,Using = "//crisp-footer[@class='au-target']//crisp-button[@class='au-target'][@click.delegate='wizard.goForward()']//button[text()='Next']")]
        public IWebElement NextButton;
        
        [FindsBy(How = How.XPath, Using = "//crisp-header-button[2]/span/button[text()='Add Rating']")]
        public IWebElement AddRatingButton;

        [FindsBy(How = How.XPath, Using = "//crisp-dialog[@class='au-target']//div//crisp-header[@class='au-target']")]
        public IWebElement RatingDialogue;
        
        //Details Page Elements 

        [FindsBy(How = How.XPath, Using = "//crisp-stepper-step[1]//div[@class='highlight-colour title au-target']")]
        public IWebElement DetailsOption;

        [FindsBy(How = How.CssSelector, Using = "crisp-picker[label='Principal Occupation'] input[type='text']")]
        public IWebElement OccupationDrodpDown;

        [FindsBy(How = How.XPath, Using = "//ul[@class='dropdown-content select-dropdown active']//li//span[text()='Architect']")]
        public IWebElement OccupationDrodpDownInput;

        [FindsBy(How = How.XPath, Using = "//crisp-input-bool[@value.two-way='genInsuranceLossesSustained']//label")]
        public IWebElement GIQuestion1;

        [FindsBy(How = How.XPath, Using = "//crisp-input-textarea[contains(@value.two-way,'genInsuranceLossesSustainedDetails')]//div//label[@class='au-target']")]
        public IWebElement GIQuestion1Details;

        [FindsBy(How = How.XPath, Using = "//crisp-input-textarea[contains(@value.two-way,'genInsuranceLossesSustainedDetails')]//div//textarea")]
        public IWebElement GIQuestion1DetailsInput;

        [FindsBy(How = How.XPath, Using = "//crisp-input-bool[@value.two-way='genInsuranceRefusedInsurance']/label")]
        public IWebElement GIQuestion2;

        [FindsBy(How = How.XPath, Using = "//crisp-input-textarea[contains(@value.two-way,'RefusedInsuranceDetails')]//div//label[@class='au-target'][text()='Details']")]
        public IWebElement GIQuestion2Details;

        [FindsBy(How = How.XPath, Using = "//crisp-input-textarea[contains(@value.two-way,'RefusedInsuranceDetails')]//div//textarea")]
        public IWebElement GIQuestion2DetailsInput;

        [FindsBy(How = How.XPath, Using = "//crisp-input-bool[@value.two-way='genInsuranceDishonesty']/label")]
        public IWebElement GIQuestion3;

        [FindsBy(How = How.XPath, Using = "//crisp-input-textarea[contains(@value.two-way,'genInsuranceDishonestyDetails')]//div//label")]
        public IWebElement GIQuestion3Details;

        [FindsBy(How = How.XPath, Using = "//crisp-input-textarea[contains(@value.two-way,'genInsuranceDishonestyDetails')]//div//textarea")]
        public IWebElement GIQuestion3DetailsInput;

        [FindsBy(How = How.XPath, Using = "//crisp-input-bool[@value.two-way='genInsuranceProsecution']//label")]
        public IWebElement GIQuestion4;

        [FindsBy(How = How.XPath, Using = "//crisp-input-textarea[contains(@value.two-way,'genInsuranceProsecutionDetails')]//div//label")]
        public IWebElement GIQuestion4Details;

        [FindsBy(How = How.XPath, Using = "//crisp-input-textarea[contains(@value.two-way,'genInsuranceProsecutionDetails')]//div//textarea")]
        public IWebElement GIQuestion4DetailsInput;

        [FindsBy(How = How.XPath, Using = "//crisp-input-bool[@value.two-way='genInsuranceLiquidation']/label")]
        public IWebElement GIQuestion5;

        [FindsBy(How = How.XPath, Using = "//crisp-input-textarea[contains(@value.two-way,'genInsuranceLiquidationDetails')]/div/label")]
        public IWebElement GIQuestion5Details;

        [FindsBy(How = How.XPath, Using = "//crisp-input-textarea[contains(@value.two-way,'genInsuranceLiquidationDetails')]/div/textarea")]
        public IWebElement GIQuestion5DetailsInput;

        [FindsBy(How = How.XPath, Using = "//crisp-input-text[contains(@value.two-way,'genInsuranceLiquidationCompanyName')]/div/label")]
        public IWebElement CompanyName;

        [FindsBy(How = How.XPath, Using = "//crisp-input-text[contains(@value.two-way,'genInsuranceLiquidationCompanyName')]/div/input")]
        public IWebElement CompanyNameInput;

        [FindsBy(How = How.CssSelector, Using = "div > crisp-input-date > div > label")]
        public IWebElement DateField;

        [FindsBy(How = How.XPath, Using = "//div[@class='picker__footer']/button[@class='btn-flat picker__today']")]
        public IWebElement DateFieldInput;

        //Warranty Providers Page Elements 

        [FindsBy(How = How.XPath, Using = "//crisp-stepper-step[2]//div[@class='highlight-colour title au-target']")]
        public IWebElement WarrentyProvider;

        [FindsBy(How = How.XPath, Using = "//crisp-wizard-step[@step-title='Warranty Providers']//crisp-card-content//div[@class='card-content']//crisp-input-bool//input[@type='checkbox']//following-sibling::label")]
        public IList<IWebElement> OtherWarrentyProviders;

        [FindsBy(How = How.CssSelector, Using = "crisp-card-content > div > crisp-input-bool:nth-child(1) > label")]
        public IWebElement NHBCCheckbox;

        [FindsBy(How = How.CssSelector, Using = "crisp-input-year > div > label")]
        public IWebElement NHBCRatingYear;

        [FindsBy(How = How.CssSelector, Using = "crisp-input-year > div > input")]
        public IWebElement NHBCRatingYearInput;

        [FindsBy(How = How.CssSelector, Using = "crisp-picker[label='Rate'] [value='A1']")]
        public IWebElement NHBCRateDropDown;

        [FindsBy(How = How.XPath, Using = "//ul[@class='dropdown-content select-dropdown active']//li//span[text()='A1']")]
        public IWebElement NHBCRateDropDownInput;

        [FindsBy(How = How.XPath, Using = "//crisp-input-bool[@value.two-way='nhbcRatingIsConfirmed']//label")]
        public IWebElement IsConfirmedCheckbox;


        [FindsBy(How = How.XPath, Using = "//crisp-input-file[@label='Upload Supporting Document']/div/div[@class='btn']/input")]
        public IWebElement FileUploadButton;

        [FindsBy(How = How.CssSelector, Using = "crisp-card-content > div > crisp-input-bool:nth-child(2) > label")]
        public IWebElement CRLCheckbox;

        [FindsBy(How = How.CssSelector, Using = "div:nth-child(1) > crisp-card > crisp-card-content > div > crisp-input-bool:nth-child(3) > label")]
        public IWebElement CheckMateCheckbox;

        [FindsBy(How = How.CssSelector, Using = "div:nth-child(1) > crisp-card > crisp-card-content > div > crisp-input-bool:nth-child(4) > label")]
        public IWebElement LABCWarrantyCheckbox;

        [FindsBy(How = How.CssSelector, Using = "div:nth-child(1) > crisp-card > crisp-card-content > div > crisp-input-bool:nth-child(5) > label")]
        public IWebElement PremierGuaranteeCheckbox;

        [FindsBy(How = How.CssSelector, Using = "div:nth-child(1) > crisp-card > crisp-card-content > div > crisp-input-bool:nth-child(6) > label")]
        public IWebElement BuildZoneCheckbox;

        [FindsBy(How = How.CssSelector, Using = "div:nth-child(1) > crisp-card > crisp-card-content > div > crisp-input-bool:nth-child(7) > label")]
        public IWebElement BLPCheckbox;

        [FindsBy(How = How.CssSelector, Using = "div:nth-child(1) > crisp-card > crisp-card-content > div > crisp-input-bool:nth-child(8) > label")]
        public IWebElement ProtecCheckbox;

        [FindsBy(How = How.CssSelector, Using = "div:nth-child(1) > crisp-card > crisp-card-content > div > crisp-input-bool:nth-child(9) > label")]
        public IWebElement SelfBuildCheckbox;

        [FindsBy(How = How.CssSelector, Using = "div:nth-child(1) > crisp-card > crisp-card-content > div > crisp-input-bool:nth-child(10) > label")]
        public IWebElement AEDISCheckbox;

        [FindsBy(How = How.CssSelector, Using = "div:nth-child(1) > crisp-card > crisp-card-content > div > crisp-input-bool:nth-child(11) > label")]
        public IWebElement OtherCheckbox;

        //Developements Page Elements 

        [FindsBy(How = How.XPath, Using = "//crisp-stepper-step[3]//div[@class='highlight-colour title au-target']")]
        public IWebElement Developments;

        [FindsBy(How = How.XPath, Using = "//crisp-dialog[@class='au-target']//crisp-header[@text='Previous Development']")]
        public IWebElement PreviousDevDiv;

        [FindsBy(How = How.XPath, Using = "//crisp-wizard-step[@step-title='Developments'][@class='au-target active']//crisp-action-button[@click.delegate='addPreviousDevelopment()'][@class='au-target']//div//a//i")]
        public IWebElement PreDevsPlusButton;

        [FindsBy(How = How.CssSelector, Using = "crisp-input-text[label='Address'] > div > label")]
        public IWebElement PreDevsAddress;

        [FindsBy(How = How.CssSelector, Using = "crisp-input-text[label='Address'] > div > input")]
        public IWebElement PreDevsAddressInput;

        [FindsBy(How = How.CssSelector, Using = "crisp-input-currency[label='Reconstruction Cost'] > div > label")]
        public IWebElement ReconstructionCost;

        [FindsBy(How = How.CssSelector, Using = "crisp-input-currency[label='Reconstruction Cost'] > div > input")]
        public IWebElement ReconstructionCostInput;

        [FindsBy(How = How.CssSelector, Using = "crisp-input-number[label='Units'] > div > label")]
        public IWebElement Unitslabel;

        [FindsBy(How = How.CssSelector, Using = "crisp-input-number[label='Units'] > div > input")]
        public IWebElement UnitsInput;

        [FindsBy(How = How.CssSelector, Using = "crisp-picker[label='Warranty Provider']")]
        public IWebElement WarrentyProviderDropdown;

        [FindsBy(How = How.XPath, Using = "//ul[@class='dropdown-content select-dropdown active']/li/span[text()='NHBC']")]
        public IWebElement WarrentyProviderDropdownInput;

        [FindsBy(How = How.CssSelector, Using = "crisp-picker[label='Construction Type']")]
        public IWebElement ConstructionTypeDropdown;

        [FindsBy(How = How.XPath, Using = "//ul[@class='dropdown-content select-dropdown active']/li/span[text()='New Build']")]
        public IWebElement ConstructionTypeDropdownInput;

        [FindsBy(How = How.XPath, Using = "//ul[@class='dropdown-content select-dropdown active']/li/span[text()='Conversion']")]
        public IWebElement ConstructionTypeDropdownInputConversion;

        [FindsBy(How = How.XPath, Using = "//crisp-footer-button[@class='au-target'][@click.delegate='ok()']//button[text()='Ok'][@class='au-target waves-effect waves-light btn-flat']")]
        public IWebElement OkButton;
        
        //Prospective Business Details Page Elements 

        [FindsBy(How = How.XPath, Using = "//crisp-wizard-step[@step-title='Prospective Business'][@class='au-target active']//crisp-action-button[@click.delegate='addUnitsConstructedByYear()'][@class='au-target']//div//a//i")]
        public IWebElement AddProspectiveBusinessButton;

        [FindsBy(How = How.XPath, Using = "//crisp-dialog[@class='au-target']//crisp-header[@text='Prospective Business']")]
        public IWebElement ProspectiveBusinessDiv;

        [FindsBy(How = How.XPath, Using = "//crisp-stepper-step[4]//div[@class='highlight-colour title au-target']")]
        public IWebElement ProspectiveBusiness;

        [FindsBy(How = How.CssSelector, Using = "crisp-input-currency > div > label")]
        public IWebElement LandBankHeld;

        [FindsBy(How = How.CssSelector, Using = "crisp-input-currency > div > input")]
        public IWebElement LandBankHeldInput;

        [FindsBy(How = How.XPath, Using = "//div[@class='modal-content']//crisp-input-year[@label='Year']//label[text()='Year'][@class='au-target']")]
        public IWebElement ConstructedHistoricYear;

        [FindsBy(How = How.XPath, Using = "//div[@class='modal-content']//crisp-input-year[@label='Year'][@value.bind='year & validate']//input")]
        public IWebElement ConstructedHistoricYearInput;

        [FindsBy(How = How.XPath, Using = "//crisp-input-number[@label='Units Constructed']//label[text()='Units Constructed']")]
        public IWebElement ConstructedHistoricUnits;

        [FindsBy(How = How.XPath, Using = "//crisp-input-number[@label='Units Constructed']//input")]
        public IWebElement ConstructedHistoricUnitsInput;

        //Claims History Page Elements 

        [FindsBy(How = How.XPath, Using = "//crisp-stepper-step[5]//div[@class='highlight-colour title au-target']")]
        public IWebElement ClaimHistory;

        [FindsBy(How = How.XPath, Using = "//crisp-dialog[@class='au-target']//div//crisp-header[@text='Historic Claim']")]
        public IWebElement ClaimHistoryDiv;

        [FindsBy(How = How.XPath, Using = "//crisp-wizard-step[@step-title='Claims History'][@class='au-target active']//crisp-input-bool//label")]
        public IWebElement AssociatedClaims;

        [FindsBy(How = How.XPath, Using = "//crisp-wizard-step[@step-title='Claims History'][@class='au-target active']//crisp-action-button[@click.delegate='addClaimsHistory()']//div//a/i")]
        public IWebElement AddHistoricClaimsButton;

        [FindsBy(How = How.CssSelector, Using = "div.modal-content > crisp-input-date.au-target > div > label")]
        public IWebElement ClaimsDate;

        [FindsBy(How = How.XPath, Using = "//div[@class='picker picker--focused picker--opened']/div/div/div/div/div/button[@class='btn-flat picker__today']")]
        public IWebElement ClaimsDateInput;

        [FindsBy(How = How.XPath, Using = "//crisp-dialog[@class='au-target']//crisp-input-currency[@label='Value']//label")]
        public IWebElement ClaimsValue;

        [FindsBy(How = How.XPath, Using = "//crisp-dialog[@class='au-target']//crisp-input-currency[@label='Value']//input")]
        public IWebElement ClaimsValueInput;

        [FindsBy(How = How.XPath, Using = "//crisp-dialog[@class='au-target']//crisp-input-textarea[@label='Comments']//label")]
        public IWebElement ClaimsComments;

        [FindsBy(How = How.XPath, Using = "//crisp-dialog[@class='au-target']//crisp-input-textarea[@label='Comments']//textarea")]
        public IWebElement ClaimsCommentsInput;

        [FindsBy(How = How.XPath, Using = "//crisp-footer[@class='au-target']//crisp-button[@click.delegate='save()'][@class='au-target']//button[text()='Save']")]
        public IWebElement ConfirmRatingButton;

        [FindsBy(How = How.XPath, Using = "//crisp-card-header/div/span[text()='Summary']")]
        public IWebElement RatingSummaryDisplayed;

        [FindsBy(How = How.CssSelector,Using = "body > div.custom-element > router-view > crisp-header > nav > div > crisp-header-avatar > img")]
        public IWebElement Imageperson;

        [FindsBy(How = How.CssSelector, Using = "body > ai-dialog-container > div > div > crisp-dialog > div")]
        public IWebElement RatingPersonDialogueWindow;

        //Scores Page Elements 

        [FindsBy(How = How.XPath, Using = "//crisp-stepper-step[5]//div[@class='highlight-colour title au-target']")]
        public IWebElement ScoresOption;

        [FindsBy(How = How.XPath, Using = "//crisp-picker[@label='Financial Score']//div//input[@class='select-dropdown']")]
        public IWebElement FinancialScoresDropdown;

        [FindsBy(How = How.XPath, Using = "//ul[@class='dropdown-content select-dropdown active']/li/span[text()='1']")]
        public IWebElement FinancialScoresDropdownInput;

        [FindsBy(How = How.XPath, Using = "//crisp-picker[@label='Land Bank Score']//div//input[@class='select-dropdown']")]
        public IWebElement LandBankScoresDropdown;

        [FindsBy(How = How.XPath, Using = "//ul[@class='dropdown-content select-dropdown active']/li/span[text()='1']")]
        public IWebElement LandBankScoresDropdownInput;

        [FindsBy(How = How.XPath, Using = "//crisp-picker[@label='Units Score']//div//input[@class='select-dropdown']")]
        public IWebElement UnitsScoresDropdown;

        [FindsBy(How = How.XPath, Using = "//ul[@class='dropdown-content select-dropdown active']/li/span[text()='1']")]
        public IWebElement UnitsScoresDropdownInput;

        [FindsBy(How = How.XPath, Using = "//crisp-picker[@label='Years Trading Score']//div//input[@class='select-dropdown']")]
        public IWebElement TradingScoresDropdown;

        [FindsBy(How = How.XPath, Using = "//ul[@class='dropdown-content select-dropdown active']/li/span[text()='1']")]
        public IWebElement TradingScoresDropdownInput;

        [FindsBy(How = How.XPath, Using = "//crisp-picker[@label='Company Structure Score']//div//input[@class='select-dropdown']")]
        public IWebElement CompanyStructureScoresDropdown;

        [FindsBy(How = How.XPath, Using = "//ul[@class='dropdown-content select-dropdown active']/li/span[text()='1']")]
        public IWebElement CompanyStructureScoresDropdownInput;

        [FindsBy(How = How.XPath, Using = "//crisp-display-value[@value.bind='totalScore']//span[@class='au-target value']")]
        public IWebElement TotalScrores;
        

        [FindsBy(How = How.CssSelector, Using = "crisp-picker[label='Actual Technical Rating'] >div>div>input[type=text].select-dropdown")]
        public IWebElement TechgRatingDropdown;

        [FindsBy(How = How.XPath, Using = "//ul[@class='dropdown-content select-dropdown active']/li/span[text()='1']")]
        public IWebElement TechgRatingDropdownInput;

        [FindsBy(How = How.CssSelector, Using = "crisp-input-textarea[label='Technical Rating Comment']>div>label")]
        public IWebElement TechRatingComment;

        [FindsBy(How = How.CssSelector, Using = "crisp-input-textarea[label='Technical Rating Comment']>div>textarea")]
        public IWebElement TechRatingCommentText;

        public void DetailsPage()
        {
            WaitForElement(OccupationDrodpDown);
            OccupationDrodpDown.Click();
            WaitForElement(OccupationDrodpDownInput);
            OccupationDrodpDownInput.Click();
            WaitForElement(GIQuestion1);
            GIQuestion1.Click();
            WaitForElement(GIQuestion1Details);
            GIQuestion1Details.Click();
            WaitForElement(GIQuestion1DetailsInput);
            GIQuestion1DetailsInput.SendKeys("Claims History");
            WaitForElement(GIQuestion2);
            GIQuestion2.Click();
            WaitForElement(GIQuestion2Details);
            GIQuestion2Details.Click();
            GIQuestion2DetailsInput.SendKeys("Refused Details ");
            WaitForElement(GIQuestion3);
            GIQuestion3.Click();
            WaitForElement(GIQuestion3Details);
            GIQuestion3Details.Click();
            GIQuestion3DetailsInput.SendKeys("Dishonesty Details ");
            WaitForElement(GIQuestion4);
            GIQuestion4.Click();
            WaitForElement(GIQuestion4Details);
            GIQuestion4Details.Click();
            GIQuestion4DetailsInput.SendKeys("Health and Safety Details");
            WaitForElement(GIQuestion5);
            GIQuestion5.Click();
            WaitForElement(CompanyName);
            CompanyName.Click();
            WaitForElement(CompanyNameInput);
            CompanyNameInput.SendKeys("Test Company");

            DateField.Click();
            WaitForElement(DateFieldInput);
            DateFieldInput.Click();
            WaitForElement(GIQuestion5Details);
            GIQuestion5Details.Click();
            WaitForElement(GIQuestion5DetailsInput);
            GIQuestion5DetailsInput.SendKeys("Liquidation Details");
            WaitForElement(NextButton);
            NextButton.Click();
        }


        public void WarrantyDetails()
        {
            WaitForElements(OtherWarrentyProviders);
            if (OtherWarrentyProviders.Count > 0)
            {
                foreach (var eachwarrentyprovider in OtherWarrentyProviders)
                {
                    if (!eachwarrentyprovider.Selected)
                    {
                        eachwarrentyprovider.Click();
                    }
                }
            }

            WaitForElement(NHBCCheckbox);
            NHBCCheckbox.Click();
            WaitForElement(NHBCRatingYear);
            NHBCRatingYear.Click();
            WaitForElement(NHBCRatingYearInput);
            NHBCRatingYearInput.SendKeys("2014");
            WaitForElement(IsConfirmedCheckbox);
            IsConfirmedCheckbox.Click();
            WaitForElement(NHBCRateDropDown);
            NHBCRateDropDown.Click();
            WaitForElement(NHBCRateDropDownInput);
            NHBCRateDropDownInput.Click();

            var fileInfo = new FileInfo(ExtensionMethods.GetAbsolutePath(@"TestData\QuotesData\CRISP install guide.docx"));
            FileUploadButton.SendKeys($"{fileInfo}");

            //FileUploadButton.SendKeys(@"\\tpgfile\NewCompany\IT\Department\QA Team\Current Projects\CRISP\CrispAutomation\CrispData\QuotesData\CRISP install guide.docx");

            //WaitForElement(CRLCheckbox);
            //CRLCheckbox.Click();
            //CheckMateCheckbox.Click();
            //LABCWarrantyCheckbox.Click();
            //PremierGuaranteeCheckbox.Click();
            //BuildZoneCheckbox.Click();
            //BLPCheckbox.Click();
            //ProtecCheckbox.Click();
            //SelfBuildCheckbox.Click();
            //AEDISCheckbox.Click();
            //OtherCheckbox.Click();
            //
            WaitForElement(NextButton);
            NextButton.Click();
        }

        public void DevelopmentDetails()
        {
            //Adding New Build Type
            WaitForElement(PreDevsPlusButton);
            PreDevsPlusButton.Click();

            WaitForElement(PreviousDevDiv);
            WaitForElement(PreDevsAddress);
            PreDevsAddress.Click();
            PreDevsAddressInput.SendKeys("Liverpool");
            WaitForElement(ReconstructionCost);
            ReconstructionCost.Click();
            WaitForElement(ReconstructionCostInput);
            ReconstructionCostInput.SendKeys("20000");
            WaitForElement(Unitslabel);
            Unitslabel.Click();
            WaitForElement(UnitsInput);
            UnitsInput.SendKeys("4");
            WaitForElement(WarrentyProviderDropdown);
            WarrentyProviderDropdown.Click();
            WaitForElement(WarrentyProviderDropdownInput);
            WarrentyProviderDropdownInput.Click();
            WaitForElement(ConstructionTypeDropdown);
            ConstructionTypeDropdown.Click();
            WaitForElement(ConstructionTypeDropdownInput);
            ConstructionTypeDropdownInput.Click();

            WaitForElement(OkButton);
            OkButton.Click();
            CloseSpinneronDiv();


            //Adding Conversion Type

            WaitForElement(PreDevsPlusButton);
            WaitForElementToClick(PreDevsPlusButton);
            PreDevsPlusButton.Click();

            WaitForElement(PreviousDevDiv);
            WaitForElement(PreDevsAddress);
            PreDevsAddress.Click();
            PreDevsAddressInput.SendKeys("Manchester");
            WaitForElement(ReconstructionCost);
            ReconstructionCost.Click();
            WaitForElement(ReconstructionCostInput);
            ReconstructionCostInput.SendKeys("100000");
            WaitForElement(Unitslabel);
            Unitslabel.Click();
            WaitForElement(UnitsInput);
            UnitsInput.SendKeys("2");
            WaitForElement(WarrentyProviderDropdown);
            WarrentyProviderDropdown.Click();
            WaitForElement(WarrentyProviderDropdownInput);
            WarrentyProviderDropdownInput.Click();
            WaitForElement(ConstructionTypeDropdown);
            ConstructionTypeDropdown.Click();
            WaitForElement(ConstructionTypeDropdownInputConversion);
            ConstructionTypeDropdownInputConversion.Click();

            WaitForElement(OkButton);
            OkButton.Click();
            CloseSpinneronDiv();
            WaitForElement(NextButton);
            WaitForElementToClick(NextButton);
            NextButton.Click();

        }

        public void ProspectiveBusinessDetails()
        {

            WaitForElement(LandBankHeld);
            LandBankHeld.Click();
            WaitForElement(LandBankHeldInput);
            LandBankHeldInput.SendKeys("10000");
            WaitForElement(AddProspectiveBusinessButton);
            AddProspectiveBusinessButton.Click();
            WaitForElement(ProspectiveBusinessDiv);
            WaitForElement(ConstructedHistoricYear);
            ConstructedHistoricYear.Click();
            WaitForElement(ConstructedHistoricYearInput);
            ConstructedHistoricYearInput.SendKeys("2017");
            WaitForElement(ConstructedHistoricUnits);
            ConstructedHistoricUnits.Click();
            WaitForElement(ConstructedHistoricUnitsInput);
            ConstructedHistoricUnitsInput.SendKeys("25");
            WaitForElement(OkButton);
            OkButton.Click();
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            WaitForElement(NextButton);
            NextButton.Click();
        }
        public void ClaimsHistoryDetails()
        {
            WaitForElement(AssociatedClaims);
            AssociatedClaims.Click();
            WaitForElement(AddHistoricClaimsButton);
            AddHistoricClaimsButton.Click();
            WaitForElement(ClaimHistoryDiv);
            WaitForElement(ClaimsDate);
            ClaimsDate.Click();

            WaitForElement(ClaimsDateInput);
            ClaimsDateInput.Click();

            WaitForElement(ClaimsValue);
            ClaimsValue.Click();
            ClaimsValueInput.SendKeys("20000");
            WaitForElement(ClaimsComments);
            ClaimsComments.Click();
            WaitForElement(ClaimsCommentsInput);
            ClaimsCommentsInput.SendKeys("SuccessFul");
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            WaitForElement(OkButton);
            OkButton.Click();
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            WaitForElement(NextButton);
            NextButton.Click();

        }
        public void ScoresDetails()
        {
            WaitForElement(FinancialScoresDropdown);
            FinancialScoresDropdown.Click();
            WaitForElement(FinancialScoresDropdownInput);
            FinancialScoresDropdownInput.Click();
            WaitForElement(LandBankScoresDropdown);
            LandBankScoresDropdown.Click();
            WaitForElement(LandBankScoresDropdownInput);
            LandBankScoresDropdownInput.Click();
            WaitForElement(UnitsScoresDropdown);
            UnitsScoresDropdown.Click();
            WaitForElement(UnitsScoresDropdownInput);
            UnitsScoresDropdownInput.Click();
            WaitForElement(TradingScoresDropdown);
            TradingScoresDropdown.Click();
            WaitForElement(TradingScoresDropdownInput);
            TradingScoresDropdownInput.Click();
            WaitForElement(CompanyStructureScoresDropdown);
            CompanyStructureScoresDropdown.Click();
            WaitForElement(CompanyStructureScoresDropdownInput);
            CompanyStructureScoresDropdownInput.Click();
            WaitForElement(TotalScrores);
            var totalScoreValue = TotalScrores.Text;
            //Assert.AreEqual("5", totalScoreValue);
            WaitForElement(TechgRatingDropdown);
            TechgRatingDropdown.Click();
            WaitForElement(TechgRatingDropdownInput);
            TechgRatingDropdownInput.Click();
            WaitForElement(TechRatingComment);
            TechRatingComment.Click();
            WaitForElement(TechRatingCommentText);
            TechRatingCommentText.Click();
            TechRatingCommentText.Clear();
            TechRatingCommentText.SendKeys("Proposed Rate 1 applied as NHBC A1L Rating.");

        }
    }
}