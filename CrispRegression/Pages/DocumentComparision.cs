﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading; 
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.PageObjects;
using System.IO;
using CrispAutomation.Features;
using iTextSharp.text.pdf.parser;
using iTextSharp.text.pdf;
using System.Collections;
using System.Security.Cryptography;
using NUnit.Framework;
using Bytescout.PDFExtractor;
using CrispAutomation.Support;

namespace CrispAutomation.Pages
{
    public class DocumentComparisionPage : Support.Pages
    {
        public IWebDriver wdriver;
        static string FirstFile, SecondFile;
        public DocumentComparisionPage(ISearchContext driver)
        {
            PageFactory.InitElements(driver, this);
            wdriver = (RemoteWebDriver) driver;
        }

        //Intial Notice Documemnt Elements 

        [FindsBy(How = How.XPath, Using = "//dd//crisp-button[@class='au-target']//button")]
        public IList<IWebElement> ViewDocumentButtons;

        [FindsBy(How = How.XPath,
            Using = "//view-email-attachments//table//tbody//tr[contains(text(),'Initial Notice')]//td[5]")]
        public IList<IWebElement> FileNameRows;

        [FindsBy(How = How.XPath,
            Using = "//crisp-tabs[@class='au-target']//ul[@ref='tabHeader']//li//a[contains(text(),'Activities')]")]
        public IWebElement ActivitiesTab;

        //crisp-card-content//div//view-email-attachments//table//tbody//tr//td[text()='Initial Notice']
        [FindsBy(How = How.XPath, Using = "//crisp-download-file//crisp-button//span//button//i")]
        public IWebElement DownloadButton;

        //Files and Docs Tab 

        [FindsBy(How = How.XPath, Using = "//crisp-tabs/div/ul/li/span/a[contains(text(),'Files')]")]
        public IWebElement FilesandDocsTab;

        [FindsBy(How = How.XPath, Using = "//table//tbody//tr//td")]
        public IList<IWebElement> DocRows;

        [FindsBy(How = How.XPath,
            Using = "//tr[@class='au-target collection-item active']//crisp-download-file//crisp-button//button")]
        public IWebElement DownloadDoc;

        [FindsBy(How = How.XPath, Using = "//tbody//tr//td//crisp-input-bool//label")]
        public IList<IWebElement> DocCheckBox;

        [FindsBy(How = How.XPath, Using = "//crisp-card-actions//crisp-button//button[text()='Ok']")]
        public IWebElement OkButton;

        [FindsBy(How = How.XPath, Using = "//crisp-list[@list-title='Document']//div[@ref='theTable']//tbody//tr//td")]
        public IList<IWebElement> DocList;

        [FindsBy(How = How.XPath, Using = "//div[contains(@class,'au-target side-nav right-aligned')]//crisp-card//crisp-card-content//dl//dd[1]")]
        public IWebElement OriginalDoc;
       
        //Compare Quote Document
        public void QuoteDocComparision()
        {
            string quoteId = wdriver.Url.Split('/').Last();
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(1000);
            string folderName = "QGU";
            string fileName = $"Quote - {Statics.OrderNumber}_1.pdf";
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(1000);
            HTTPClient.Download(quoteId, fileName,folderName);
            log.Info($" {fileName}  :: Document Downloaded from CrispUI ");
            string DownloadedUIFile = Statics.UIPdf;
            //"\\tpgfile\NewCompany\IT\Department\QA Team\Current Projects\CRISP\CrispAutomation\CrispData\Documents\ActualData\Quotation Document.pdf"
          //  string ExpectedDocument = @"\\tpgfile\NewCompany\IT\Department\QA Team\Current Projects\CRISP\CrispAutomation\CrispData\Documents\ActualData\Quotation Document.pdf";
          //  CompareTwoPDF(DownloadedUIFile, ExpectedDocument);
            log.Info($" {fileName}  :: Document Comparison Done Successfully");
        }
        //Compare Intial Notice Document
        public void IntialNoticeDocComparision()
        {
            string quoteId = wdriver.Url.Split('/').Last();
            //string entityId = "3";
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(1000);
            string folderName = $"Building Control";
            string fileName = $"Initial Notice - {Statics.OrderNumber}.pdf";           
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(1000);    
            HTTPClient.Download(quoteId, fileName, folderName);
            log.Info($" {fileName}  :: Document Downloaded from CrispUI ");
            string DownloadedUIFile = Statics.UIPdf;
          //  string ExpectedDocument = @"\\tpgfile\NewCompany\IT\Department\QA Team\Current Projects\CRISP\CrispAutomation\CrispData\Documents\ActualData\Initial Notice.pdf";
         //   CompareTwoPDF(DownloadedUIFile, ExpectedDocument);
            log.Info($" { fileName}  :: Document Comparison Done Successfully");

        }
        //Compare Final Notice Document
        public void FinalNoticeDocComparision()
        {
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(1000);
            string quoteId = wdriver.Url.Split('/').Last();
            //string entityId = "3";
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(1000);
            string folderName = $"Building Control";
            string fileName = $"Final Notice - {Statics.OrderNumber}.pdf";
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(1000);
            //HTTPClient.DownloadSubfolders(quoteId, folder);
            //string folderName = folder;
            HTTPClient.Download(quoteId, fileName, folderName);
            log.Info($" {fileName}  :: Document Downloaded from CrispUI ");
            string DownloadedUIFile = Statics.UIPdf;
          //  string ExpectedDocument = @"\\tpgfile\NewCompany\IT\Department\QA Team\Current Projects\CRISP\CrispAutomation\CrispData\Documents\ActualData\Final Notice.pdf";
          //  CompareTwoPDF(DownloadedUIFile, ExpectedDocument);
            log.Info($" {fileName}  :: Document Comparison Done Successfully");

        }
        public void PartialFinalNoticeDocComparision()
        {
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(1000);
            string quoteId = wdriver.Url.Split('/').Last();
            //string entityId = "3";
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(1000);
            string folderName = $"Building Control";
            string fileName = $"Partial Final Notice - {Statics.OrderNumber}.pdf";
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(1000);
            //HTTPClient.DownloadSubfolders(quoteId, folder);
            //string folderName = folder;
            HTTPClient.Download(quoteId, fileName, folderName);
            log.Info($" {fileName}  :: Document Downloaded from CrispUI ");
            string DownloadedUIFile = Statics.UIPdf;
            //  string ExpectedDocument = @"\\tpgfile\NewCompany\IT\Department\QA Team\Current Projects\CRISP\CrispAutomation\CrispData\Documents\ActualData\Final Notice.pdf";
            //  CompareTwoPDF(DownloadedUIFile, ExpectedDocument);
            log.Info($" {fileName}  :: Document Comparison Done Successfully");

        }
        //Compare PlotIc Document
        public void PlotICDocComparision()
        {
            string actualdocument = "";
            string quoteId = wdriver.Url.Split('/').Last();
            //string entityId = "3";
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(1000);
            string folderName = $"CSU";
            string fileName = $"Plot Initial Certificate - {Statics.OrderNumber}-1.pdf";
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(1000);
            HTTPClient.Download(quoteId, fileName,folderName);
            log.Info($" {fileName}  :: Document Downloaded from CrispUI ");
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(1000);
            string DownloadedUIFile = Statics.UIPdf;
            if (Statics.OrderNumber.Contains("PL-LABC"))
            {
                actualdocument = $"Home Initial Certificate-LABC-{Statics.ProductName}.pdf";
            }
            if (Statics.OrderNumber.Contains("PL-PG"))
            {
                actualdocument = $"Home Initial Certificate-PG-{Statics.ProductName}.pdf";
            }
           // string ExpectedDocument = $@"\\tpgfile\NewCompany\IT\Department\QA Team\Current Projects\CRISP\CrispAutomation\CrispData\Documents\ActualData\PlotIC\{actualdocument}";
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(1000);
           // CompareTwoPDF(DownloadedUIFile, ExpectedDocument);
           // log.Info($" {fileName}  :: Document Comparison Done Successfully");

        }
        //Compare DevIC Document
        public void DevICDocComparision()
        {
            string actualdocument = "";
            string quoteId = wdriver.Url.Split('/').Last();
            //string entityId = "3";
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(1000);
            string folderName = $"CSU";
            string fileName = $"Development Initial Certificate - {Statics.OrderNumber} - {Statics.ProductName}.pdf";
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            HTTPClient.Download(quoteId, fileName, folderName);
            log.Info($" {fileName}  :: Document Downloaded from CrispUI ");
            string DownloadedUIFile = Statics.UIPdf;
            if (Statics.OrderNumber.Contains("PL-LABC"))
            {
                actualdocument = $"Development Initial Certificate-LABC-{Statics.ProductName}.pdf";
            }
            if (Statics.OrderNumber.Contains("PL-PG"))
            {
                actualdocument = $"Development Initial Certificate-PG-{Statics.ProductName}.pdf";
            }

            //string ExpectedDocument = $@"\\tpgfile\NewCompany\IT\Department\QA Team\Current Projects\CRISP\CrispAutomation\CrispData\Documents\ActualData\DevIC\{actualdocument}";
           // CompareTwoPDF(DownloadedUIFile, ExpectedDocument);
            log.Info($" {fileName}  :: Document Comparision Done Successfully");

        }


        //public void GetCOIDocName()
        //{
        //    CloseSpinneronDiv();
        //    CloseSpinneronPage();
        //    //Thread.Sleep(1000);
        //    WaitForElement(FilesandDocsTab);
        //    FilesandDocsTab.Click();
        //    CloseSpinneronDiv();
        //    CloseSpinneronPage();
        //    //Thread.Sleep(1000);
        //    var item = DocList.FirstOrDefault(x => x.Text.Contains("Certificate Of Insurance"));
        //    CloseSpinneronDiv();
        //    CloseSpinneronPage();
        //    //Thread.Sleep(1500);

        //        CloseSpinneronPage();
        //        WaitForElement(item);
        //        item.Click();

        //    CloseSpinneronDiv();
        //    CloseSpinneronPage();
        //    //Thread.Sleep(1000);
        //    WaitForElement(OriginalDoc);
        //    //Thread.Sleep(1000);
        //    var orginalCOIName = OriginalDoc.Text;
        //    Statics.COIName = orginalCOIName;
        //}

        //Compare COI Document
        public void COIDocComparision()
        {
           // GetCOIDocName();

            string quoteId = wdriver.Url.Split('/').Last();
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(1000);
            //string entityId = "3";
            string folderName = $"CSU";
            string fileName = $"Certificate of Insurance - {Statics.OrderNumber}-1 - M D Insurance Services Ltd.pdf";
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(1000);
            HTTPClient.Download(quoteId, fileName, folderName);
            log.Info($" {fileName}  :: Document Downloaded from CrispUI ");
            string DownloadedUIFile = Statics.UIPdf;
            //string ExpectedDocument = $@"\\tpgfile\NewCompany\IT\Department\QA Team\Current Projects\CRISP\CrispAutomation\CrispData\Documents\ActualData\{fileName}";
           // CompareTwoPDF(DownloadedUIFile, ExpectedDocument);
            log.Info($" {fileName}  :: Document Comparison Done Successfully");

        }       

        public static void CompareTwoPDF(string FirstPDF, string SecondPDF)
        {           

            if (File.Exists(FirstPDF) && File.Exists(SecondPDF))
            {
                PdfReader reader = new PdfReader(FirstPDF);
                for (int page = 1; page <= reader.NumberOfPages; page++)
                {

                    ITextExtractionStrategy strategy = new SimpleTextExtractionStrategy();
                    FirstFile += PdfTextExtractor.GetTextFromPage(reader, page, strategy);
                }
                PdfReader reader1 = new PdfReader(SecondPDF);
                for (int page = 1; page <= reader.NumberOfPages; page++)
                {
                    ITextExtractionStrategy strategy = new SimpleTextExtractionStrategy();
                    SecondFile += PdfTextExtractor.GetTextFromPage(reader1, page, strategy);
                }                
            }
            else
            {
                Console.WriteLine("Files does not exist.");
                //log.Info("Files does not exist.");
            }

            List<string> File1diff;
            List<string> File2diff;
            IEnumerable<string> file1 = FirstFile.Trim().Split('\r', '\n');
            IEnumerable<string> file2 = SecondFile.Trim().Split('\r', '\n');
            File1diff = file1.ToList();
            File2diff = file2.ToList();

            if (file2.Count() > file1.Count())
            {
                Console.WriteLine("File 1 has less number of lines than File 2.");
                log.Info($"{file1} has less number of lines than {file2}");
                for (int i = 0; i < File1diff.Count; i++)
                {
                    if (!File1diff[i].Equals(File2diff[i]))
                    {
                        Console.WriteLine("File 1 content: " + File1diff[i] + "\r\n" + "File 2 content: " + File2diff[i]);
                       // log.Info("File 1 content: " + File1diff[i] + "\r\n" + "File 2 content: " + File2diff[i]);

                    }

                }

                for (int i = File1diff.Count; i < File2diff.Count; i++)
                {
                   Console.WriteLine("File 2 extra content: " + File2diff[i]);
                    //log.Info("File 2 extra content: " + File2diff[i]);
                }

            }
            else if (file2.Count() < file1.Count())
            {
                Console.WriteLine("File 2 has less number of lines than File 1.");

                for (int i = 0; i < File2diff.Count; i++)
                {
                    if (!File1diff[i].Equals(File2diff[i]))
                    {
                      Console.WriteLine("File 1 content: " + File1diff[i] + "\r\n" + "File 2 content: " + File2diff[i]);
                        log.Info("File 1 content: " + File1diff[i] + "\r\n" + "File 2 content: " + File2diff[i]);

                    }

                }

                for (int i = File2diff.Count; i < File1diff.Count; i++)
                {
                   Console.WriteLine("File 1 extra content: " + File1diff[i]);
                    //log.Info("File 1 extra content: " + File1diff[i]);
                }
            }
            else
            {
               Console.WriteLine("File 1 and File 2, both are having same number of lines.");
                log.Info($"File 1 and File 2, both are having same number of lines");

              

                for (int i = 0; i < File1diff.Count; i++)
                {
                    if (!File1diff[i].Equals(File2diff[i]))
                    {

                        Console.WriteLine("File 1 content: " + File1diff[i] + "\r\n" + "File 2 Content: " + File2diff[i]);
                       // log.Info("File 1 content: " + File1diff[i] + "\r\n" + "File 2 Content: " + File2diff[i]);
                    }

                }
              

            }
        }
        public static string GetFileHash(string filename)
        {
            var hash = new SHA1Managed();
            var clearBytes = File.ReadAllBytes(filename);
            var hashedBytes = hash.ComputeHash(clearBytes);
            return ConvertBytesToHex(hashedBytes);
        }
        public static string ConvertBytesToHex(byte[] bytes)
        {
            var sb = new StringBuilder();

            for (var i = 0; i < bytes.Length; i++)
            {
                sb.Append(bytes[i].ToString("x"));
            }
            return sb.ToString();
        }
        public static void CompareTwoFiles(string FirstPDF, string SecondPDF)
        {
            var originalHash = GetFileHash(FirstPDF);
            var copiedHash = GetFileHash(SecondPDF);

            //Assert.AreEqual(copiedHash, originalHash);
        }
        public static void CompareTwoDocs(string FirstPDF, string SecondPDF)
        {
            TextExtractor doc1 = new TextExtractor();
            doc1.LoadDocumentFromFile(FirstPDF);
            doc1.RegistrationKey = "demo";
            doc1.RegistrationName ="demo";
             doc1.SaveTextToFile("");
            TextExtractor doc2 = new TextExtractor();
            doc2.LoadDocumentFromFile(SecondPDF);
            doc2.RegistrationKey = "demo";
            doc2.RegistrationName = "demo";
            doc2.SaveTextToFile("");
            TextComparer Comparer = new TextComparer();
            Comparer.RegistrationKey = "demo";
            Comparer.RegistrationName = "demo";
            Comparer.Compare(doc1, doc2);           
           // IComparer result = Comparer.Compare(doc1, doc2,new Comparison());
            var Report = new FileInfo(ExtensionMethods.GetAbsolutePath($@"TestData\RegDocuments\pdfOutput.html")).ToString();
            Comparer.GenerateHtmlReport(Report);
            doc1.Dispose();
            doc2.Dispose();
            System.Diagnostics.Process.Start(Report);
        }
    }
}
