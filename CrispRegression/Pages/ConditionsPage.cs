﻿using CrispAutomation.Features;
using CrispAutomation.Support;
using ExcelDataReader;
using NUnit.Framework;
using OfficeOpenXml;
using OpenQA.Selenium;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.PageObjects;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace CrispAutomation.Pages
{
    public class ConditionsPage : Support.Pages
    {
        public IWebDriver wdriver;
        public ConditionsPage(ISearchContext driver)
        {
            PageFactory.InitElements(driver, this);
            wdriver = (RemoteWebDriver)driver;
        }

        
        System.Data.DataTable excelTable;
        bool setlandOwnerRoleCondition = false;
        bool setLAWithEmailAddressCondition = false;
        bool setHousingAssociationRoleCondition = false;
        bool builderSecurityDocCondition = false;
        bool builderRegistrationFeeCondition = false;
        bool developerSecurityDocCondition = false;
        bool builderActiveTermCondition = false;
        bool developerActiveTermCondition = false;
        bool developerRegistrationFeeCondition = false;
        bool selfBuildIndemnityDocCondition = false;
        bool drainagePlanDocumentCondition = false;
        bool siteLocationPlanDocumentCondition = false;
        bool feeCondition = false;
        bool escrowFeeCondition = false;    
        bool manualCondition = false;
        bool initialNoticeCondition = false;
        bool finalNoticeCondition = false;
        bool completedHouseConditions = false;
        bool completedHouseLABCConditions = false;
        bool propertyOwnerCondition = false;
        bool technicalSignOffConditon = false;
        bool activeTerm = true;


        [FindsBy(How = How.XPath, Using = "//crisp-tabs//div//ul[@ref='tabHeader']/li[@class='au-target tab']//span//a[contains(text(),'Conditions')]")]
        public IWebElement ConditionsTab;

        [FindsBy(How = How.XPath, Using = "//condition-list[@class='custom-element au-target']//div[@class='au-target crisp-row table-body with-header']//table[@ref='table']//tbody")]
        public IList<IWebElement> ConditionsTable;        

        [FindsBy(How = How.XPath, Using = "//condition-list[@class='custom-element au-target']//div[@class='au-target crisp-row table-body with-header']//table[@ref='table']//tbody//tr//td[1]//crisp-table-td//div//div")]
        public IList<IWebElement> ConditionsRows;
        
        // Additional Conditions 
        [FindsBy(How = How.XPath, Using = "//crisp-tabs//div//ul[@ref='tabHeader']/li[@class='au-target tab']//span//a[contains(text(),'Plot Schedule')]")]
        public IWebElement PlotScheduleTab;

        [FindsBy(How = How.XPath, Using = "//plot-schedule-order//crisp-header-button[@click.delegate='amendPlotSchedule()']//button[@class='au-target waves-effect waves-light btn-flat'][text()='Amend']")]
        public IWebElement AmendOrderButton;

        [FindsBy(How = How.XPath, Using = "//ai-dialog-container//crisp-dialog//div[contains(@class,'modal-fixed-footer modal-has-header modal-full-bleed modal-tall modal-wide')]//crisp-header[@class='au-target']")]
        public IWebElement AmendOrderDiv;

        [FindsBy(How = How.XPath, Using = "//crisp-wizard-step[@step-title='Plot Schedule']//crisp-header-button[@icon='download']//button[@class='au-target waves-effect waves-light btn-flat'][text()='Download Plot Schedule']")]
        public IWebElement DownloadPlotScheduleButton;

        [FindsBy(How = How.XPath, Using = "//crisp-wizard-step[@step-title='Plot Schedule']//crisp-header-button[@click.delegate='importPlots()']//button[@class='au-target waves-effect waves-light btn-flat'][text()='Import']")]
        public IWebElement ImportButton;

        [FindsBy(How = How.XPath, Using = "//crisp-button[@click.delegate='ok()'][@class='au-target']//span//button[text()='OK'][@class='au-target waves-effect waves-light btn-flat']")]
        public IWebElement UploadOkButton;

        [FindsBy(How = How.XPath, Using = "//crisp-button[@class='au-target'][@click.delegate='wizard.goForward()']//span//button[@class='au-target waves-effect waves-light btn-flat'][text()='Next']")]
        public IWebElement NextButton;

        [FindsBy(How = How.XPath, Using = "//crisp-button[@class='au-target'][@click.delegate='save()']//span//button[@class='au-target waves-effect waves-light btn-flat'][text()='Confirm']")]
        public IWebElement ConfirmButton;

        [FindsBy(How = How.XPath, Using = "//div[@class='modal-content']//span[@class='message'][contains(text(),'No QGU Referral is required to confirm these changes for this order amendment request. Would you like to manually trigger a QGU Referral?')]")]
        public IWebElement QGUReferralConfirmDiv;

        [FindsBy(How = How.XPath, Using = "//crisp-button[@class='au-target']//button[@class='au-target waves-effect waves-light btn'][text()='No']")]
        public IWebElement QGUReferralNoButton;

        [FindsBy(How = How.XPath, Using = "//crisp-input-radio/div/ul/li/label[contains(text(),'Override Conditions/Blocks & Send')]")]
        public IList<IWebElement> OverrideConditionsRadioButton;

        [FindsBy(How = How.XPath, Using = "//crisp-wizard-step[@class='au-target active']//complete//crisp-card//crisp-card-content//div//p[text()='There are no blocks on issuing the Final Notice for the selected plots']")]
        public IList<IWebElement> ConfirmDetails;

        [FindsBy(How = How.XPath, Using = "//crisp-input-textarea[@label='Override comments']//div//label")]
        public IWebElement CommentsLabel;

        [FindsBy(How = How.XPath, Using = "//crisp-input-textarea[@label='Override comments']//div//textarea")]
        public IWebElement CommentsInput;

        [FindsBy(How = How.XPath, Using = "//crisp-input-file[@label='Select file...']//div//div//input[@class='au-target']")]
        public IWebElement UploadPlotSchedule;

        [FindsBy(How = How.XPath, Using = "//crisp-tabs//div//ul[@ref='tabHeader']/li[@class='au-target tab']//span//a[contains(text(),'Roles')]")]
        public IWebElement RolesTab;

        [FindsBy(How = How.XPath, Using = "//role-editor//crisp-list//div[@ref='theTable']//table//tbody[@class='collection']//tr[@class='au-target collection-item']//td")]
        public IList<IWebElement> RolesRows;

        [FindsBy(How = How.XPath, Using = "//role-editor//crisp-list//div[@ref='theTable']//table//tbody[@class='collection']//tr[@class='au-target collection-item']//td[contains(text(),'Builder')]/following-sibling::td[1]")]
        public IList<IWebElement> BuilderRole;

        [FindsBy(How = How.XPath, Using = "//role-editor//crisp-list//div[@ref='theTable']//table//tbody[@class='collection']//tr[@class='au-target collection-item']//td[contains(text(),'Developer')]/following-sibling::td[1]")]
        public IList<IWebElement> DeveloperRole;


        [FindsBy(How = How.XPath, Using = "//i[@class='au-target fa fa-search']")]
        public IWebElement SearchButton;

        [FindsBy(How = How.XPath, Using = "//a[@click.delegate='subItem.onClick()'][text()='Contacts']")]
        public IWebElement ContactOption;

        [FindsBy(How = How.XPath, Using = "//crisp-tabs//ul[@ref='tabHeader']//li[@class='au-target tab']//span//a[contains(text(),'PG Registration')]")]
        public IWebElement PGRegistrationTab;

        [FindsBy(How = How.XPath, Using = "//crisp-tabs//ul[@ref='tabHeader']//li[@class='au-target tab']//span//a[contains(text(),'LABC Registration')]")]
        public IWebElement LABCRegistrationTab;

        [FindsBy(How = How.XPath, Using = "//div[@ref='tabContainer']//ul[@ref='tabHeader']//li[@class='au-target tab']//span//a[contains(text(),'Reg Documents')]")]
        public IWebElement RegDocumentTab;

        [FindsBy(How = How.XPath, Using = "//security-documents//crisp-list[contains(@source.bind,'SecurityDocuments')]//crisp-input-bool[@label='Show Site Specific Documents']//label")]
        public IList<IWebElement> ShowSiteSpecificDocs;

        [FindsBy(How = How.XPath, Using = "//security-documents//crisp-list[contains(@source.bind,'SecurityDocuments')]//ul[@ref='theList']//li[@class='au-target collection-item']")]
        public IList<IWebElement> SecurityDocsList;

        [FindsBy(How = How.XPath, Using = "//crisp-card-actions//div//crisp-button//span//button[text()='Upload']")]
        public IWebElement UploadButton;

        [FindsBy(How = How.XPath, Using = "//ai-dialog-container[2]/div/div/crisp-dialog/div/crisp-header/nav/div")]
        public IWebElement UploadDiv;

        [FindsBy(How = How.XPath, Using = "//div[@class='modal-content']//crisp-input-object[@icon='file']//div[@class='au-target input-field']//span[@ref='inputContainer']")]
        public IWebElement RelatedFiles;

        [FindsBy(How = How.XPath, Using = "//button[text()='Upload New Files']")]
        public IWebElement UploadNewFilesButton;

        [FindsBy(How = How.XPath, Using = "//crisp-card-content//div//upload-documents-embed//crisp-input-file[@class='au-target']//div//div//input[@ref='fileInput']")]
        public IWebElement SendFileInput;

        [FindsBy(How = How.XPath, Using = "//crisp-input-file/div/div[1]/input")]
        public IWebElement UploadDoc;

        [FindsBy(How = How.XPath, Using = "//tr//td//crisp-input-object[@class='au-target']//div[contains(@class,'au-target input-field')]//span[@ref='inputContainer']")]
        public IWebElement DocTypeDropdown1;

        [FindsBy(How = How.XPath, Using = "//crisp-card-content[@class='au-target']//div[@class='au-target list-title has-filter']//span[@class='filter-input-field']//input[@class='au-target']")]
        public IWebElement SearchDocumentType;

        [FindsBy(How = How.XPath, Using = "//ul[@class='au-target collection has-header has-filter']//li[@class='au-target collection-item']//crisp-list-item")]
        public IList<IWebElement> SearchDocumentTypeInputList;

        [FindsBy(How = How.XPath, Using = "//ul/li/span[text()='Site Location Plan']")]
        public IWebElement SiteLocationPlanType;

        [FindsBy(How = How.XPath, Using = "//crisp-input-date/div/label[text()='Date Received']")]
        public IWebElement DateReceivedLabel;

        [FindsBy(How = How.XPath, Using = "//div[contains(@class,'picker--opened')]/div/div/div/div/div[@class='picker__footer']/button[text()='Today']")]
        public IWebElement DateReceivedInput;

        [FindsBy(How = How.XPath, Using = "//crisp-input-date[@label='Date Received']//div//label")]
        public IWebElement DateOnUploadDiv;

        [FindsBy(How = How.XPath, Using = "//crisp-button[@class='au-target']//span//button[text()='Select']")]
        public IWebElement Select;

        [FindsBy(How = How.XPath, Using = "//button[text()='OK'][@class='au-target waves-effect waves-light btn-flat']")]
        public IWebElement OkButton;

        [FindsBy(How = How.XPath, Using = "//crisp-card-actions//div//crisp-button//span//button[text()='Review']")]
        public IWebElement ReviewButton;

        [FindsBy(How = How.XPath, Using = "//ai-dialog-container[2]/div/div/crisp-dialog/div")]
        public IWebElement ReviewDiv;

        [FindsBy(How = How.XPath, Using = "//crisp-picker[@label='Review Status']//div//input[@class='select-dropdown']")]
        public IWebElement ReviewStatusDropdown;

        [FindsBy(How = How.XPath, Using = "//ul[@class='dropdown-content select-dropdown active']//li//span[text()='Confirmed']")]
        public IWebElement ReviewStatusDropdownInput;

        [FindsBy(How = How.XPath, Using = "//crisp-card-actions//div//crisp-button//span//button[text()='Close']")]
        public IWebElement CloseButton;

        //Making Registration Fee for the Builder and Developer 
        [FindsBy(How = How.XPath, Using = "//div[@ref='tabContainer']//ul[@ref='tabHeader']//li[@class='au-target tab']//span//a[contains(text(),'Account')]")]
        public IWebElement RoleAccountTab;

        [FindsBy(How = How.XPath, Using = "//crisp-card-content//div[@class='card-content']//account-summary//dl//dt[text()='Reference/name']//following-sibling::dd[1]")]
        public IWebElement RegistrationRef;

        [FindsBy(How = How.XPath, Using = "//crisp-card-content//div[@class='card-content']//account-summary//dl//dt[text()='Fee balance']//following-sibling::dd[1]")]
        public IWebElement RegFeeBalance;

        [FindsBy(How = How.XPath, Using = "//crisp-card-content//div[@class='card-content']//account-summary//dl//dt[text()='Fee paid']//following-sibling::dd[1]")]
        public IWebElement RegFeePaid;

        [FindsBy(How = How.XPath, Using = "//crisp-card-content//div[@class='card-content']//crisp-button[@click.delegate='paymentToClient()']//button")]
        public IWebElement EnterPaymentButton;

        [FindsBy(How = How.XPath, Using = "//ai-dialog-container/div/div/crisp-dialog/div/crisp-header/nav/div")]
        public IWebElement MakePaymentDiv;
           
        [FindsBy(How = How.XPath, Using = "//crisp-button[@class='au-target']//span//button[text()='Save'][@class='au-target waves-effect waves-light btn-flat']")]
        public IWebElement SaveButton;

        //Pay the Fee Balance for issue the order 
        [FindsBy(How = How.XPath, Using = "//crisp-tabs//div//ul[@ref='tabHeader'][@class='au-target tabs highlight-background']//li[@class='au-target tab']//span//a[contains(text(),'Account')][contains(@class,'au-target highlight-colour')]")]
        public IWebElement AccountTab;

        [FindsBy(How = How.XPath, Using = "//div//account-summary[@summary.bind='summary']//dl/dt[text()='Fee balance']//following-sibling::dd[1]")]
        public IWebElement FeeBalance;

        [FindsBy(How = How.XPath, Using = "//div//account-summary[@summary.bind='summary']//dl/dt[text()='Escrow balance']//following-sibling::dd[1]")]
        public IWebElement EscrowBalance;

        //Local Autority email address 
     

        [FindsBy(How = How.XPath, Using = "//div[@ref='theTable']//tbody//tr//td[contains(text(),'Planning and Local Authority')]//following-sibling::td[2][contains(@title.bind,'Email')]")]
        public IList<IWebElement> LocalAuthorityEmails;

        [FindsBy(How = How.XPath, Using = "//crisp-header-actions//crisp-header-button[@click.delegate='edit()']//span//button[@class='au-target waves-effect waves-light btn-flat']")]
        public IWebElement EditCompany;

        [FindsBy(How = How.XPath, Using = "//div//crisp-dialog//div//crisp-header//nav/div[@ref='wrapper']")]
        public IWebElement EditCompanyDiv;

        [FindsBy(How = How.XPath, Using = "//crisp-button[@click.delegate='goForward()']//span//button[@class='au-target waves-effect waves-light btn-flat']")]
        public IWebElement EditCompanyNextButton;

        [FindsBy(How = How.XPath, Using = "//crisp-list[@list-title='Offices']/ul[@ref='theList']//li[contains(@class,'au-target collection-item')]//crisp-list-item//a//crisp-list-item-details//div//div[contains(text(),'Registered office')]")]
        public IList<IWebElement> OfficeAddress;

        [FindsBy(How = How.XPath, Using = "//crisp-card-actions//div/crisp-action-button[@click.delegate='editOffice(selectedOffice)']//div//a//i[text()='edit']")]
        public IWebElement EditOfficeAddressButton;

        [FindsBy(How = How.XPath, Using = "//crisp-tabs//div//ul[@ref='tabHeader']/li[@class='au-target tab']//span//a[contains(text(),'Overview')]")]
        public IWebElement OverviewTab;

        [FindsBy(How = How.XPath, Using = "//div[@class='card-content']//crisp-card-actions//crisp-button[@click.delegate='viewContact(selectedRole)']//span//button[@class='au-target waves-effect waves-light btn-flat']")]
        public IWebElement ViewCompanyButton;

        [FindsBy(How = How.XPath, Using = "//div[@ref='tabContainer']//ul[@ref='tabHeader']//li[@class='au-target tab']//span//a[contains(text(),'Terms')]")]
        public IWebElement TermsTab;

      
        String plotData { get; set; }
        public System.Data.DataTable ReadExcelPlotData(string plotData)
        {
            //open file and returns as Stream
            var result = new DataSet();
            using (var stream = File.Open(plotData, FileMode.Open, FileAccess.Read))
            using (var excelReader = ExcelReaderFactory.CreateOpenXmlReader(stream)) //.xlsx
            {
                //Createopenxmlreader via ExcelReaderFactory

                //Set the First Row as Column Name

                //Return as DataSet
                result = excelReader.AsDataSet(new ExcelDataSetConfiguration()
                {
                    ConfigureDataTable = (_) => new ExcelDataTableConfiguration()
                    {
                        UseHeaderRow = true
                    }
                });
            }
            //Get all the Tables
            DataTableCollection table = result.Tables;
            //Store it in DataTable
            System.Data.DataTable resultTable = table["Plots"];
            //Get The Warrenty Product List 
            var Productlist = resultTable.DefaultView
     .ToTable(true, "Warranty Product")
     .Rows
     .Cast<DataRow>()
     .Select(row => row["Warranty Product"])
     .ToList();
            Statics.ProductNameList = Productlist.Select(i => i.ToString()).ToList();
            return resultTable;
        }
        
        public void GetConditionsList()
        {
            //Thread.Sleep(1000);
            if (Dashboardpage.DecisionButton.Count <= 0)
            {
                Dashboardpage.SelectOrder();
            }
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            WaitForElement(ConditionsTab);
            ConditionsTab.Click();
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            if(ConditionsTable.Count>0)
            {
                WaitForElements(ConditionsRows);
                if (ConditionsRows.Count > 0)
                {
                    Statics.ConditionsList = ConditionsRows.Select(i => i.Text.ToString()).ToList();
                    Statics.ExtranetConditions = ConditionsRows.Select(i => i.Text.ToString()).ToList();
                }
            }           
        }       

        public void CloseConditions()
        {
            GetConditionsList();
            CloseSpinneronDiv();
            CloseSpinneronPage();
            if (ConditionsTable.Count > 0)
            {
                WaitForElements(ConditionsRows);
                if (ConditionsRows.Count > 0)
                {
                    GetConditionsList();
                    setlandOwnerRoleCondition = Statics.ConditionsList.Any(x => x.Contains("Setting of Landowner Role with a default office or address"));
                    if (setlandOwnerRoleCondition == true)
                    {
                        SetupLandOwnerRole();
                    }

                    GetConditionsList();
                    setHousingAssociationRoleCondition = Statics.ConditionsList.Any(x => x.Contains("Confirmation of the Housing Association for the Social Housing element"));
                    if (setHousingAssociationRoleCondition == true)
                    {
                        SetupHousingAssociationRole();
                    }
                    GetConditionsList();
                    setLAWithEmailAddressCondition = Statics.ConditionsList.Any(x => x.Contains("Setting of Local Authority role - with valid email address"));
                    if (setLAWithEmailAddressCondition == true)
                    {
                        SetupEmailonLocalAuthority();
                    }
                    GetConditionsList();
                    drainagePlanDocumentCondition = Statics.ConditionsList.Any(x => x.Contains("Receipt of the Drainage Plan"));
                    if (drainagePlanDocumentCondition == true)
                    {
                        CloseDrainagePlanCondition();
                    }
                    GetConditionsList();
                    siteLocationPlanDocumentCondition = Statics.ConditionsList.Any(x => x.Contains("Receipt of the Site Location Plan"));
                    if (siteLocationPlanDocumentCondition == true)
                    {
                        CloseSiteLocationPlanCondition();
                    }
                    GetConditionsList();
                    feeCondition = Statics.ConditionsList.Any(x => x.Contains("Confirmation and approval of the amount of deposit taken against "));
                    escrowFeeCondition = Statics.ConditionsList.Any(x => x.Contains("escrow is required"));

                    if (feeCondition == true || escrowFeeCondition == true)
                    {
                        MakeOrderPayment();
                    }
                    GetConditionsList();
                    builderActiveTermCondition = Statics.ConditionsList.Any(x => x.Contains("Builder does not have an Active Registration Term"));
                    if (builderActiveTermCondition == true)
                    {
                        BuilderActiveTermsCondition();
                    }
                    GetConditionsList();
                    developerActiveTermCondition = Statics.ConditionsList.Any(x => x.Contains("Developer does not have an Active Registration Term"));
                    if (developerActiveTermCondition == true)
                    {
                        DeveloperActiveTermsCondition();
                    }
                    GetConditionsList();
                    builderSecurityDocCondition = Statics.ConditionsList.Any(x => x.Contains("Builder has Outstanding Security Documents"));
                    builderRegistrationFeeCondition = Statics.ConditionsList.Any(x => x.Contains("Builder has outstanding Registration Fee"));
                    selfBuildIndemnityDocCondition = Statics.ConditionsList.Any(x => x.Contains("The self build indemnity agreement for the builder is outstanding"));
                    if (builderSecurityDocCondition == true || builderRegistrationFeeCondition == true || selfBuildIndemnityDocCondition == true)
                    {
                        BuilderSecurityDocsandRegistrationFee();
                    }
                    GetConditionsList();
                    developerSecurityDocCondition = Statics.ConditionsList.Any(x => x.Contains("Developer has Outstanding Security Documents"));
                    developerRegistrationFeeCondition = Statics.ConditionsList.Any(x => x.Contains("Developer has outstanding Registration Fee"));
                    if (developerSecurityDocCondition == true || developerRegistrationFeeCondition == true)
                    {
                        DeveloperSecurityDocsAndRegistrationFee();
                    }
                    GetConditionsList();
                    //if (Statics.ConditionsList.Any(x => x.Contains("Building Control Final Notice is required")) || Statics.ConditionsList.Any(x => x.Contains("This is a CH site, you need to input the construction end date (from App form or from confirmation from the client i.e a utility bill show the date they moved in) for the required plot, into the plot uploader, to calculate the COI dates")) || Statics.ConditionsList.Any(x => x.Contains("This is a CH SOB site, you need to input the LA completion certificate date on the plot uploader, to calculate the COI dates.")))
                    completedHouseConditions = Statics.ConditionsList.Any(x => x.Contains("This is a CH site, you need to input the construction end date (from App form or from confirmation from the client i.e a utility bill show the date they moved in) for the required plot, into the plot uploader, to calculate the COI dates")) || Statics.ConditionsList.Any(x => x.Contains("This is a CH SOB site, you need to input the LA completion certificate date on the plot uploader, to calculate the COI dates."));
                    completedHouseLABCConditions = Statics.ConditionsList.Any(x => x.Contains("Building Control Final Notice is required"));
                    if (completedHouseConditions == true || completedHouseLABCConditions == true)
                    {
                        CloseCompletedHousingConditions();
                    }
                    GetConditionsList();
                    propertyOwnerCondition = Statics.ConditionsList.Any(x => x.Contains("A property owner needs to be noted for the plot"));
                    if (propertyOwnerCondition == true)
                    {
                        ClosePropertyOwnerCondition();
                    }
                    GetConditionsList();
                    manualCondition = ManageConditionsPage.ManualConditionsList.Count > 0;
                    if (manualCondition == true)
                    {
                        CloseManualConditions();
                    }
                    GetConditionsList();
                    technicalSignOffConditon = Statics.ConditionsList.Any(x => x.Contains("Issuance of Warranty Technical Sign Off(Certificate of Approval)"));
                    if (technicalSignOffConditon == true)
                    {
                        SurveyorAssessments.SurveyorAssessment();
                    }
                    GetConditionsList();
                    initialNoticeCondition = Statics.ConditionsList.Any(x => x.Contains("Please issue the Initial Notice"));
                    if (initialNoticeCondition == true)
                    {
                        CloseIntialNoticeCondtions();
                    }
                    GetConditionsList();
                    finalNoticeCondition = Statics.ConditionsList.Any(x => x.Contains("Issue Building Control Final Notice"));
                    if (finalNoticeCondition == true)
                    {
                        CloseFinalNoticeCondtions();
                    }
                }
            }
            
        }
        public void CloseConditionsForExtranet()
        {
            GetConditionsList();
            if (ConditionsTable.Count > 0)
            {
                WaitForElements(ConditionsRows);
                if (ConditionsRows.Count > 0)
                {
                    GetConditionsList();
                    setlandOwnerRoleCondition = Statics.ExtranetConditions.Any(x => x.Contains("Setting of Landowner Role with a default office or address"));
                    if (setlandOwnerRoleCondition == true)
                    {
                        SetupLandOwnerRole();
                    }
                    GetConditionsList();
                    setHousingAssociationRoleCondition = Statics.ExtranetConditions.Any(x => x.Contains("Confirmation of the Housing Association for the Social Housing element"));
                    if (setHousingAssociationRoleCondition == true)
                    {
                        SetupHousingAssociationRole();
                    }
                    GetConditionsList();
                    setLAWithEmailAddressCondition = Statics.ExtranetConditions.Any(x => x.Contains("Setting of Local Authority role - with valid email address"));
                    if (setLAWithEmailAddressCondition == true)
                    {
                        SetupEmailonLocalAuthority();
                    }
                    GetConditionsList();
                    drainagePlanDocumentCondition = Statics.ExtranetConditions.Any(x => x.Contains("Receipt of the Drainage Plan"));
                    if (drainagePlanDocumentCondition == true)
                    {
                        CloseDrainagePlanCondition();
                    }
                    GetConditionsList();
                    siteLocationPlanDocumentCondition = Statics.ExtranetConditions.Any(x => x.Contains("Receipt of the Site Location Plan"));
                    if (siteLocationPlanDocumentCondition == true)
                    {
                        CloseSiteLocationPlanCondition();
                    }
                    GetConditionsList();
                    builderActiveTermCondition = Statics.ExtranetConditions.Any(x => x.Contains("Builder does not have an Active Registration Term"));
                    if (builderActiveTermCondition == true)
                    {
                        BuilderActiveTermsCondition();
                    }
                    GetConditionsList();
                    developerActiveTermCondition = Statics.ExtranetConditions.Any(x => x.Contains("Developer does not have an Active Registration Term"));
                    if (developerActiveTermCondition == true)
                    {
                        DeveloperActiveTermsCondition();
                    }
                    GetConditionsList();
                    feeCondition = Statics.ExtranetConditions.Any(x => x.Contains("Confirmation and approval of the amount of deposit taken against "));
                    escrowFeeCondition = Statics.ExtranetConditions.Any(x => x.Contains("escrow is required"));
                    var newHomesEscrowFeeCondition = Statics.ExtranetConditions.Any(x => x.Contains("Additional security of £5,000.00 escrow is required with regards to New Homes Defects"));
                    if (feeCondition == true || escrowFeeCondition == true || newHomesEscrowFeeCondition == true)
                    {
                        MakeOrderPayment();
                    }
                    GetConditionsList();
                    builderSecurityDocCondition = Statics.ExtranetConditions.Any(x => x.Contains("Builder has Outstanding Security Documents"));
                    builderRegistrationFeeCondition = Statics.ExtranetConditions.Any(x => x.Contains("Builder has outstanding Registration Fee"));
                    selfBuildIndemnityDocCondition = Statics.ExtranetConditions.Any(x => x.Contains("The self build indemnity agreement for the builder is outstanding"));
                    if (builderSecurityDocCondition == true || builderRegistrationFeeCondition == true || selfBuildIndemnityDocCondition == true)
                    {
                        BuilderSecurityDocsandRegistrationFee();
                    }
                    GetConditionsList();
                    developerSecurityDocCondition = Statics.ExtranetConditions.Any(x => x.Contains("Developer has Outstanding Security Documents"));
                    developerRegistrationFeeCondition = Statics.ExtranetConditions.Any(x => x.Contains("Developer has outstanding Registration Fee"));
                    if (developerSecurityDocCondition == true || developerRegistrationFeeCondition == true)
                    {
                        DeveloperSecurityDocsAndRegistrationFee();
                    }
                    GetConditionsList();
                    completedHouseConditions = Statics.ExtranetConditions.Any(x => x.Contains("This is a CH site, you need to input the construction end date (from App form or from confirmation from the client i.e a utility bill show the date they moved in) for the required plot, into the plot uploader, to calculate the COI dates")) || Statics.ConditionsList.Any(x => x.Contains("This is a CH SOB site, you need to input the LA completion certificate date on the plot uploader, to calculate the COI dates."));
                    completedHouseLABCConditions = Statics.ExtranetConditions.Any(x => x.Contains("Building Control Final Notice is required"));
                    if (completedHouseConditions == true || completedHouseLABCConditions == true)
                    {
                        CloseCompletedHousingConditions();
                    }
                    GetConditionsList();
                    propertyOwnerCondition = Statics.ExtranetConditions.Any(x => x.Contains("A property owner needs to be noted for the plot"));
                    if (propertyOwnerCondition == true)
                    {
                        ClosePropertyOwnerCondition();
                    }
                    GetConditionsList();
                    manualCondition = ManageConditionsPage.ManualConditionsList.Count > 0;
                    if (manualCondition == true)
                    {
                        CloseManualConditions();
                    }
                    GetConditionsList();
                    technicalSignOffConditon = Statics.ExtranetConditions.Any(x => x.Contains("Issuance of Warranty Technical Sign Off (Certificate of Approval)"));
                    if (technicalSignOffConditon == true)
                    {
                        SurveyorAssessments.SurveyorAssessment();
                    }
                    GetConditionsList();
                    initialNoticeCondition = Statics.ExtranetConditions.Any(x => x.Contains("Please issue the Initial Notice"));
                    if (initialNoticeCondition == true)
                    {
                        CloseIntialNoticeCondtions();
                    }
                    GetConditionsList();
                    finalNoticeCondition = Statics.ExtranetConditions.Any(x => x.Contains("Issue Building Control Final Notice"));
                    if (finalNoticeCondition == true)
                    {
                        CloseFinalNoticeCondtions();
                    }
                  
                }
            }
            CloseConditions();
        }
        //Close Site Location Plan Condition 
        public void CloseSiteLocationPlanCondition()
        {
            WaitForElement(SendIntialNotice.FilesandDocsTab);
            SendIntialNotice.FilesandDocsTab.Click();
            CloseSpinneronDiv();
            CloseSpinneronPage();
            WaitForElement(SendIntialNotice.UploadButton);
            WaitForElementToClick(SendIntialNotice.UploadButton);
            SendIntialNotice.UploadButton.Click();
            CloseSpinneronDiv();
            CloseSpinneronPage();
            WaitForElement(SendIntialNotice.FileButton);
            //Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(500);
            var fileInfo = new FileInfo(ExtensionMethods.GetAbsolutePath(@"TestData\QuotesData\CRISP install guide.docx"));
            SendFileInput.SendKeys($"{fileInfo}");
            //Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(500);
            WaitForElement(DocTypeDropdown1);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(500);
            WaitForElementToClick(DocTypeDropdown1);
            DocTypeDropdown1.Click();
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(500);
            WaitForElement(SearchDocumentType);
            SearchDocumentType.Click();
            SearchDocumentType.Clear();
            SearchDocumentType.SendKeys("Site Location Plan");
            //Thread.Sleep(500);
            SearchDocumentType.SendKeys(OpenQA.Selenium.Keys.Enter);
            //Thread.Sleep(500);
            WaitForElements(SearchDocumentTypeInputList);
            if (SearchDocumentTypeInputList.Count >= 1)
            {
                SearchDocumentTypeInputList[0].Click();
                CloseSpinneronDiv();
                CloseSpinneronPage();
                //Thread.Sleep(500);
            }
            WaitForElement(DateReceivedLabel);
            DateReceivedLabel.Click();
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            WaitForElement(DateReceivedInput);
            DateReceivedInput.Click();
            WaitForElement(SaveButton);
            SaveButton.Click();
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(500);
        }
        //Close Drainage Plan Condition       
        public void CloseDrainagePlanCondition()
        {
            WaitForElement(SendIntialNotice.FilesandDocsTab);
            SendIntialNotice.FilesandDocsTab.Click();
            CloseSpinneronDiv();
            CloseSpinneronPage();
            WaitForElement(SendIntialNotice.UploadButton);
            WaitForElementToClick(SendIntialNotice.UploadButton);
            SendIntialNotice.UploadButton.Click();
            CloseSpinneronDiv();
            CloseSpinneronPage();
            WaitForElement(SendIntialNotice.FileButton);
            //Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(500);
            var fileInfo = new FileInfo(ExtensionMethods.GetAbsolutePath(@"TestData\QuotesData\CRISP install guide.docx"));
            SendFileInput.SendKeys($"{fileInfo}");
            //Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(500);
            WaitForElement(DocTypeDropdown1);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(500);
            WaitForElementToClick(DocTypeDropdown1);
            DocTypeDropdown1.Click();
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(500);
            WaitForElement(SearchDocumentType);
            SearchDocumentType.Click();
            SearchDocumentType.Clear();
            SearchDocumentType.SendKeys("Drainage Plan");
            //Thread.Sleep(500);
            SearchDocumentType.SendKeys(OpenQA.Selenium.Keys.Enter);
            //Thread.Sleep(500);
            WaitForElements(SearchDocumentTypeInputList);
            if (SearchDocumentTypeInputList.Count >= 1)
            {
                SearchDocumentTypeInputList[0].Click();
                Thread.Sleep(500);
            }
            WaitForElement(DateReceivedLabel);
            DateReceivedLabel.Click();
            WaitForElement(DateReceivedInput);
            DateReceivedInput.Click();
            WaitForElement(SaveButton);
            SaveButton.Click();
            //Thread.Sleep(500);
        }
        //Making Order Payment 
        public void MakeOrderPayment()
        {
            GetConditionsList();
            if(Statics.ConditionsList.Any(x=>x.Contains("Confirmation and approval of the amount of deposit taken against "))||(Statics.ConditionsList.Any(x => x.Contains("escrow is required"))))
            {
                //Thread.Sleep(1500);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                //Thread.Sleep(1500);
                WaitForElement(AccountTab);
                WaitForElementToClick(AccountTab);
                AccountTab.Click();
                //Thread.Sleep(1000);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                WaitForElement(FeeBalance);
                //Thread.Sleep(1000);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                var FeeBalanceValue = FeeBalance.Text.Replace("£", "");
                decimal actualFeeBal = Convert.ToDecimal(FeeBalanceValue);
                //Thread.Sleep(1000);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                WaitForElement(EscrowBalance);
                var EscrowBalanceValue = EscrowBalance.Text.Replace("£", "");
                decimal actualEscrowBal = Convert.ToDecimal(EscrowBalanceValue);
                //Thread.Sleep(1000);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                //Thread.Sleep(1000);
                if (actualFeeBal > 0 || actualEscrowBal > 0)
                {
                    //Thread.Sleep(1000);
                    CloseSpinneronDiv();
                    CloseSpinneronPage();
                    WaitForElement(EnterPaymentButton);
                    EnterPaymentButton.Click();
                    //Thread.Sleep(1000);
                    WaitForElement(MakePaymentDiv);
                    Assert.IsTrue(MakePaymentDiv.Displayed);
                    FinancesPage.MakeDirectPyament();
                    //Thread.Sleep(1000);
                    CloseSpinneronDiv();
                    CloseSpinneronPage();
                    //Thread.Sleep(1000);
                }
            }            
        }
        public void CloseIntialNoticeCondtions()
        {
            if (Dashboardpage.DecisionButton.Count <= 0)
            {
                Dashboardpage.SelectOrder();
            }
            WaitForElement(ConditionsTab);
            ConditionsTab.Click();
            Thread.Sleep(500);
            CloseSpinneronPage();
            CloseSpinneronDiv();
            Thread.Sleep(500);
            var conditions = ConditionsRows.Count > 0;
            Thread.Sleep(500);
            CloseSpinneronPage();
            CloseSpinneronDiv();
            Thread.Sleep(500);
            if (conditions == true)
            {
                if (ConditionsRows.Count > 0)
                {
                    var IntialNoticeCondition = ConditionsRows.Any(x => x.Text.Contains("Please issue the Initial Notice"));
                    if (IntialNoticeCondition == true)
                    {
                        SendIntialNotice.SendIntialNoticeMethod();
                        SendIntialNotice.RespondtoIntialNoticeMethod();
                    }
                }
            }
        }
        public void CloseFinalNoticeCondtions()
        {
            if (Dashboardpage.DecisionButton.Count <= 0)
            {
                Dashboardpage.SelectOrder();
            }
            WaitForElement(ConditionsTab);
            ConditionsTab.Click();
            Thread.Sleep(500);
            CloseSpinneronPage();
            CloseSpinneronDiv();
            Thread.Sleep(500);
            var conditions = ConditionsRows.Count > 0;
            Thread.Sleep(500);
            CloseSpinneronPage();
            CloseSpinneronDiv();
            Thread.Sleep(500);
            if (conditions == true)
            {
                if (ConditionsRows.Count > 0)
                {
                    var FinalNoticeCondition = ConditionsRows.Any(x => x.Text.Contains("Issue Building Control Final Notice"));
                    if (FinalNoticeCondition == true)
                    {
                        FinalNoticePage.SendFinalNotice();
                    }
                }
            }
        }

     
        public void CloseManualConditions()
        {
            if (Statics.ProductName.Contains("High Value"))
            {
                Thread.Sleep(1000);
                if (IssueCertificatePage.ConditionsRows.Count <= 0)
                {
                    Dashboardpage.SelectOrder();
                }
                WaitForElement(IssueCertificatePage.ConditionsTab);
                IssueCertificatePage.ConditionsTab.Click();
                Thread.Sleep(500);
                CloseSpinneronPage();
                CloseSpinneronDiv();
                Thread.Sleep(500);
                var conditions = IssueCertificatePage.ConditionsRows.Count > 0;
                Thread.Sleep(500);
                CloseSpinneronPage();
                CloseSpinneronDiv();
                Thread.Sleep(500);
                if (conditions == true)
                {
                    WaitForElements(IssueCertificatePage.ConditionsRows);

                    if (ManageConditionsPage.ManualConditionsList.Count > 0)
                    {
                        ManageConditionsPage.ClosingCondition();
                        Thread.Sleep(1000);
                    }
                }
            }
        }
        public void DownloadPlotDataFromOrderAmendment()
        {
            WaitForElement(PlotScheduleTab);
            PlotScheduleTab.Click();
            //Thread.Sleep(1000);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            WaitForElement(AmendOrderButton);
            AmendOrderButton.Click();
            //Thread.Sleep(1000);
            CloseSpinneronPage();
            CloseSpinneronDiv();
            WaitForElement(AmendOrderDiv);
            Assert.IsTrue(AmendOrderDiv.Displayed);
            WaitForElement(DownloadPlotScheduleButton);
            //Thread.Sleep(1000);
            CloseSpinneronPage();
            CloseSpinneronDiv();
            DownloadPlotScheduleButton.Click();
            //Thread.Sleep(3000);
            CloseSpinneronPage();
            CloseSpinneronDiv();
        }

        public void UploadPlotDataOnOrderAmendment(string plotData)
        {
            WaitForElement(ImportButton);
            ImportButton.Click();
            CloseSpinneronDiv();
            //Thread.Sleep(1000);
            UploadPlotSchedule.SendKeys(plotData);
            log.Info("File has been uploaded");        
            CloseSpinneronDiv();
            CloseSpinneronPage();
            WaitForElement(UploadOkButton);
            UploadOkButton.Click();
            //Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            WaitForElement(ConfirmButton);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            WaitForElementToClick(ConfirmButton);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            ConfirmButton.Click();
            Thread.Sleep(500);
            WaitForElement(QGUReferralConfirmDiv);
            WaitForElement(QGUReferralNoButton);
            QGUReferralNoButton.Click();
            //Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
        }

        //Closing Final Notice Conditions on LABC Completed Housing 
        public void CloseCompletedHousingConditions()
        {
            GetConditionsList();
            //This is a CH site, you need to input the construction end date (from App form or from confirmation from the client i.e a utility bill show the date they moved in) for the required plot, into the plot uploader, to calculate the COI dates
            if (Statics.ConditionsList.Any(x => x.Contains("Building Control Final Notice is required")) || Statics.ConditionsList.Any(x => x.Contains("This is a CH site, you need to input the construction end date (from App form or from confirmation from the client i.e a utility bill show the date they moved in) for the required plot, into the plot uploader, to calculate the COI dates")) || Statics.ConditionsList.Any(x => x.Contains("This is a CH SOB site, you need to input the LA completion certificate date on the plot uploader, to calculate the COI dates.")))
            {
                DownloadPlotDataFromOrderAmendment();             
                 //Download to following directory 
                 var directory = new DirectoryInfo($@"{Statics.DownloadFolder}");
                //Thread.Sleep(1000);
                var myFile = directory.GetFiles()
         .OrderByDescending(f => f.LastWriteTime)
         .First();
                //Thread.Sleep(1000);

                var filePath = new DirectoryInfo($@"{Statics.DownloadFolder}\{myFile}");
                var excel = new ExcelPackage(myFile);

                excelTable = ReadExcelPlotData(filePath.ToString());
                //Read Excel Table 
                var completedHousingRow = -1;
                if (Statics.ProductNameList.Any(o => o.Equals("Completed Housing")))
                {
                    for (int i = 0; i < excelTable.Rows.Count; i++)
                    {
                        if (excelTable.Rows[i][1].ToString() == "Completed Housing")
                        {
                            completedHousingRow = i;
                            DateTime date = DateTime.Today; // will give the date for today
                            string dateWithFormat = date.ToString("dd/MM/yyyy HH:mm");
                            excel.Workbook.Worksheets["Plots"].Cells[$"Z{completedHousingRow + 2}"].Value = dateWithFormat;

                        }
                    }
                    if (completedHousingRow == -1)
                        throw new Exception("completedHousingRow Row Not Found");

                    var fileInfo = new FileInfo(ExtensionMethods.GetAbsolutePath($@"TestData\Conditions\plots.xlsx"));
                    plotData = fileInfo.ToString();
                    excel.SaveAs(new FileInfo(plotData));
                    //Thread.Sleep(1000);
                    CloseSpinneronPage();
                    CloseSpinneronDiv();
                    UploadPlotDataOnOrderAmendment(plotData);
                }  
            }
        }
        //Closing Final Notice Conditions on LABC Completed Housing 
        public void ClosePropertyOwnerCondition()
        {
            GetConditionsList();
            if (Statics.ConditionsList.Any(x => x.Contains("A property owner needs to be noted for the plot")))
            {
                DownloadPlotDataFromOrderAmendment();
                //Download to following directory 
                var directory = new DirectoryInfo($@"{Statics.DownloadFolder}");
                //Thread.Sleep(1000);
                var myFile = directory.GetFiles()
         .OrderByDescending(f => f.LastWriteTime)
         .First();
                //Thread.Sleep(1000);

                var filePath = new DirectoryInfo($@"{Statics.DownloadFolder}\{myFile}");
                var excel = new ExcelPackage(myFile);

                excelTable = ReadExcelPlotData(filePath.ToString());
                //Read Excel Table 
                var selfBuildRow = -1;
                var commercialsRow = -1;
                var commercialsHVSRow = -1;
                var ciCommercialsHVSRow = -1;
                var ciCommercialsRow = -1;
                string ownerName = "SunilKumar";
                if (Statics.ProductNameList.Any(o => o.Equals("Self Build")))
                {
                    for (int i = 0; i < excelTable.Rows.Count; i++)
                    {
                        if (excelTable.Rows[i][1].ToString() == "Self Build")
                        {
                            selfBuildRow = i;
                            excel.Workbook.Worksheets["Plots"].Cells[$"S{selfBuildRow + 2}"].Value = ownerName;
                        }
                    }
                    if (selfBuildRow == -1)
                        throw new Exception("selfBuildRow Row Not Found");
                    if (Statics.ProductNameList.Any(o => o.Equals("Commercial")))
                    {
                        for (int i = 0; i < excelTable.Rows.Count; i++)
                        {
                            if (excelTable.Rows[i][1].ToString() == "Commercial")
                            {
                                commercialsRow = i;
                                excel.Workbook.Worksheets["Plots"].Cells[$"S{commercialsRow + 2}"].Value = ownerName;
                            }
                        }
                        if (commercialsRow == -1)
                            throw new Exception("Commercial Row Not Found");
                    }
                    if (Statics.ProductNameList.Any(o => o.Equals("Commercial - High Value")))
                    {
                        for (int i = 0; i < excelTable.Rows.Count; i++)
                        {
                            if (excelTable.Rows[i][1].ToString() == "Commercial - High Value")
                            {
                                commercialsHVSRow = i;
                                excel.Workbook.Worksheets["Plots"].Cells[$"S{commercialsHVSRow + 2}"].Value = ownerName;
                            }
                        }
                        if (commercialsHVSRow == -1)
                            throw new Exception("Commercial - High Value Row Not Found");
                    }
                    if (Statics.ProductNameList.Any(o => o.Equals("Channel Islands Commercial - High Value")))
                    {
                        for (int i = 0; i < excelTable.Rows.Count; i++)
                        {
                            if (excelTable.Rows[i][1].ToString() == "Channel Islands Commercial - High Value")
                            {
                                ciCommercialsHVSRow = i;
                                excel.Workbook.Worksheets["Plots"].Cells[$"S{ciCommercialsHVSRow + 2}"].Value = ownerName;
                            }
                        }
                        if (ciCommercialsHVSRow == -1)
                            throw new Exception("Channel Island Commercial HVS Row Not Found");
                    }
                    if (Statics.ProductNameList.Any(o => o.Equals("Channel Islands Commercial")))
                    {
                        for (int i = 0; i < excelTable.Rows.Count; i++)
                        {
                            if (excelTable.Rows[i][1].ToString() == "Channel Islands Commercial")
                            {
                                ciCommercialsRow = i;
                                excel.Workbook.Worksheets["Plots"].Cells[$"S{ciCommercialsRow + 2}"].Value = ownerName;
                            }
                        }
                        if (ciCommercialsRow == -1)
                            throw new Exception("Channel Island Commercial Row Not Found");
                    }
                    var fileInfo = new FileInfo(ExtensionMethods.GetAbsolutePath($@"TestData\Conditions\plots.xlsx"));
                    plotData = fileInfo.ToString();
                    excel.SaveAs(new FileInfo(plotData));
                    //Thread.Sleep(1000);
                    CloseSpinneronPage();
                    CloseSpinneronDiv();
                    UploadPlotDataOnOrderAmendment(plotData);
                }
            }
        }
     
        //Upload security docs to builder and developer to clear the conditions on issue COI
        public void SecurityDocsUpload()
        {
            //Thread.Sleep(1000);
            if (Dashboardpage.DecisionButton.Count <= 0)
            {
                Dashboardpage.SelectOrder();
            }
            WaitForElement(ConditionsTab);
            ConditionsTab.Click();
            //Thread.Sleep(1000);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(1000);
            WaitForElements(ConditionsRows);
            builderSecurityDocCondition = ConditionsRows.Any(x => x.Text.Contains("Builder has Outstanding Security Documents"));
            builderRegistrationFeeCondition = ConditionsRows.Any(x => x.Text.Contains("Builder has outstanding Registration Fee"));
            selfBuildIndemnityDocCondition = ConditionsRows.Any(x => x.Text.Contains("The self build indemnity agreement for the builder is outstanding"));
            //Thread.Sleep(1000);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(1000);
            if (builderSecurityDocCondition == true || builderRegistrationFeeCondition == true || selfBuildIndemnityDocCondition == true)
            {
                BuilderSecurityDocsandRegistrationFee();
            }
            //Thread.Sleep(1000);
            if (ConditionsRows.Count <= 0)
            {
                Dashboardpage.SelectOrder();
            }
            WaitForElement(ConditionsTab);
            ConditionsTab.Click();
            //Thread.Sleep(1000);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(1000);
            developerSecurityDocCondition = ConditionsRows.Any(x => x.Text.Contains("Developer has Outstanding Security Documents"));
            developerRegistrationFeeCondition = ConditionsRows.Any(x => x.Text.Contains("Developer has outstanding Registration Fee"));
            //Thread.Sleep(1000);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(1000);
            if (developerSecurityDocCondition == true || developerRegistrationFeeCondition == true)
            {
                DeveloperSecurityDocsAndRegistrationFee();
            }


        }
       
        public void BuilderSecurityDocsandRegistrationFee()
        {
            AddLABCQuotePage.SelectBuilderRegistration();
            //Thread.Sleep(1000);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            if (Statics.OrderNumber.Contains("PL-PG"))
            {
                //Thread.Sleep(1000);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                WaitForElement(PGRegistrationTab);
                PGRegistrationTab.Click();
                //Thread.Sleep(1000);
                UploadSecurityDocument();
                //Thread.Sleep(1000);
                if (builderRegistrationFeeCondition == true)
                {
                    MakeRegistationFee();
                }
            }
            if (Statics.OrderNumber.Contains("PL-LABC"))
            {
                WaitForElement(LABCRegistrationTab);
                LABCRegistrationTab.Click();
                //Thread.Sleep(1000);
                UploadSecurityDocument();
                //Thread.Sleep(1000);
                if (builderRegistrationFeeCondition == true)
                {
                    MakeRegistationFee();
                }
            }
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(1000);
        }
        public void DeveloperSecurityDocsAndRegistrationFee()
        {
            AddLABCQuotePage.SelectDeveloperRegistration();
            //Thread.Sleep(1000);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(1000);
            if (Statics.OrderNumber.Contains("PL-PG"))
            {
                WaitForElement(PGRegistrationTab);
                PGRegistrationTab.Click();
                //Thread.Sleep(1000);
                UploadSecurityDocument();
                //Thread.Sleep(1000);
                if (developerRegistrationFeeCondition == true)
                {
                    MakeRegistationFee();
                }
            }
            if (Statics.OrderNumber.Contains("PL-LABC"))
            {
                WaitForElement(LABCRegistrationTab);
                LABCRegistrationTab.Click();
                //Thread.Sleep(1000);
                UploadSecurityDocument();
                //Thread.Sleep(1000);
                if (developerRegistrationFeeCondition == true)
                {
                    MakeRegistationFee();
                }
            }
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(1000);
        }

        public void BuilderActiveTermsCondition()
        {
            AddLABCQuotePage.SelectBuilderRegistration();
            //Thread.Sleep(1000);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            if (Statics.OrderNumber.Contains("PL-PG"))
            {
                Thread.Sleep(500);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                WaitForElement(PGRegistrationTab);
                PGRegistrationTab.Click();
                CloseActiveTermCondition();
            }
            if (Statics.OrderNumber.Contains("PL-LABC"))
            {
                Thread.Sleep(500);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                WaitForElement(LABCRegistrationTab);
                LABCRegistrationTab.Click();
                CloseActiveTermCondition();
            }
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
        }
        public void DeveloperActiveTermsCondition()
        {
            AddLABCQuotePage.SelectDeveloperRegistration();
            //Thread.Sleep(1000);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            if (Statics.OrderNumber.Contains("PL-PG"))
            {
                Thread.Sleep(500);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                WaitForElement(PGRegistrationTab);
                PGRegistrationTab.Click();
                CloseActiveTermCondition();
            }
            if (Statics.OrderNumber.Contains("PL-LABC"))
            {
                Thread.Sleep(500);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                WaitForElement(LABCRegistrationTab);
                LABCRegistrationTab.Click();
                CloseActiveTermCondition();
            }
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
        }
        //Upload document method 
        public void UploadSecurityDocument()
        {
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            WaitForElement(RegDocumentTab);
            WaitForElementToClick(RegDocumentTab);
            RegDocumentTab.Click();
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            WaitForElements(ShowSiteSpecificDocs);
            if (ShowSiteSpecificDocs.Count > 0)
            {
                CloseSpinneronDiv();
                CloseSpinneronPage();
                WaitForElement(ShowSiteSpecificDocs[0]);
                WaitForLoadElementtobeclickable(ShowSiteSpecificDocs[0]);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                ShowSiteSpecificDocs[0].Click();
                Thread.Sleep(500);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                Thread.Sleep(500);
            }
            WaitForElements(SecurityDocsList);
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            if (SecurityDocsList.Count > 0)
            {
                int actualsecuritydocs = SecurityDocsList.Count;
                for (int i = 0; i < actualsecuritydocs; i++)
                {
                    Thread.Sleep(500);
                    CloseSpinneronRegPage();
                    CloseSpinneronDiv();
                    CloseSpinneronPage();
                    WaitForElement(SecurityDocsList[i]);
                    Thread.Sleep(500);
                    CloseSpinneronRegPage();
                    CloseSpinneronDiv();
                    CloseSpinneronPage();
                    WaitForElementToClick(SecurityDocsList[i]);
                    Thread.Sleep(500);
                    CloseSpinneronRegPage();
                    CloseSpinneronDiv();
                    CloseSpinneronPage();
                    SecurityDocsList[i].Click();
                    Thread.Sleep(500);
                    CloseSpinneronDiv();
                    CloseSpinneronPage();
                    WaitForElement(UploadButton);
                    UploadButton.Click();
                    WaitForElement(UploadDiv);
                    Assert.IsTrue(UploadDiv.Displayed);
                    Thread.Sleep(500);
                    CloseSpinneronDiv();
                    CloseSpinneronPage();
                    WaitForElement(RelatedFiles);
                    RelatedFiles.Click();
                    Thread.Sleep(500);
                    CloseSpinneronDiv();
                    CloseSpinneronPage();
                    WaitForElement(UploadNewFilesButton);
                    UploadNewFilesButton.Click();
                    //Upload Site location Plan 
                    Thread.Sleep(500);
                    CloseSpinneronDiv();
                    CloseSpinneronPage();
                    var fileInfo = new FileInfo(ExtensionMethods.GetAbsolutePath(@"TestData\QuotesData\CRISP install guide.docx"));
                    SendFileInput.SendKeys($"{fileInfo}");
                    Thread.Sleep(500);
                    CloseSpinneronDiv();
                    CloseSpinneronPage();
                    Thread.Sleep(500);
                    WaitForElement(Select);
                    WaitForElementToClick(Select);
                    Select.Click();
                    Thread.Sleep(500);
                    CloseSpinneronDiv();
                    CloseSpinneronPage();
                    Thread.Sleep(500);
                    WaitForElement(DateOnUploadDiv);
                    WaitForElementToClick(DateOnUploadDiv);
                    DateOnUploadDiv.Click();
                    Thread.Sleep(500);
                    CloseSpinneronDiv();
                    CloseSpinneronPage();
                    Thread.Sleep(500);
                    WaitForElement(DateReceivedInput);
                    WaitForElementToClick(DateReceivedInput);
                    DateReceivedInput.Click();
                    Thread.Sleep(500);
                    CloseSpinneronDiv();
                    CloseSpinneronPage();
                    WaitForElement(OkButton);
                    WaitForElementToClick(OkButton);
                    OkButton.Click();
                    CloseSpinneronDiv();
                    Thread.Sleep(500);
                    CloseSpinneronDiv();
                    CloseSpinneronPage();
                    Thread.Sleep(500);
                    WaitForElement(ReviewButton);
                    WaitForElementToClick(ReviewButton);
                    ReviewButton.Click();
                    Thread.Sleep(500);
                    CloseSpinneronDiv();
                    CloseSpinneronPage();
                    Thread.Sleep(500);
                    WaitForElement(ReviewDiv);
                    Assert.IsTrue(ReviewDiv.Displayed);
                    Thread.Sleep(500);
                    CloseSpinneronDiv();
                    CloseSpinneronPage();
                    Thread.Sleep(500);
                    WaitForElement(ReviewStatusDropdown);
                    ReviewStatusDropdown.Click();
                    Thread.Sleep(500);
                    CloseSpinneronDiv();
                    CloseSpinneronPage();
                    WaitForElement(ReviewStatusDropdownInput);
                    WaitForElementToClick(ReviewStatusDropdownInput);
                    ReviewStatusDropdownInput.Click();
                    Thread.Sleep(500);
                    CloseSpinneronDiv();
                    CloseSpinneronPage();
                    WaitForElement(OkButton);
                    WaitForElementToClick(OkButton);
                    OkButton.Click();
                    Thread.Sleep(500);
                    CloseSpinneronPage();
                    CloseSpinneronDiv();
                    Thread.Sleep(500);
                    WaitForElement(CloseButton);
                    WaitForElementToClick(CloseButton);
                    CloseButton.Click();
                    Thread.Sleep(500);
                    CloseSpinneronPage();
                    CloseSpinneronDiv();
                    CloseSpinneronRegPage();
                    Thread.Sleep(500);
                }
            }
        }      


        //Pay Off registration fee to builder and developer to clear the conditions on issue COI
        public void RegistrationFee()
        {
            if (Dashboardpage.DecisionButton.Count <= 0)
            {
                Dashboardpage.SelectOrder();
            }
            WaitForElement(ConditionsTab);
            ConditionsTab.Click();
            //Thread.Sleep(1000);
            CloseSpinneronPage();
            CloseSpinneronDiv();
            //Thread.Sleep(1000);
            WaitForElements(ConditionsRows);
            //Thread.Sleep(1000);
            CloseSpinneronPage();
            CloseSpinneronDiv();
            //Thread.Sleep(1000);
            if (ConditionsRows.Count > 0)
            {
                var registrationFeeCondition = ConditionsRows.Any(x => x.Text.Contains("Builder has outstanding Registration Fee") || x.Text.Contains("Developer has outstanding Registration Fee"));
                if (registrationFeeCondition == true)
                {
                    string builderRole = "";
                    string developerRole = "";
                    WaitForElement(RolesTab);
                    RolesTab.Click();
                    WaitForElements(RolesRows);
                    if (BuilderRole.Count > 0)
                    {
                        builderRole = BuilderRole[0].Text;
                    }
                    if (DeveloperRole.Count > 0)
                    {
                        developerRole = DeveloperRole[0].Text;
                    }
                    //Thread.Sleep(1500);
                    CloseSpinneronDiv();
                    CloseSpinneronPage();
                    //Thread.Sleep(1500);
                    AddLABCQuotePage.SelectBuilderRegistration();

                    CloseSpinneronDiv();
                    CloseSpinneronPage();
                    if (Statics.OrderNumber.Contains("PL-PG"))
                    {
                        WaitForElement(PGRegistrationTab);
                        PGRegistrationTab.Click();
                        //Thread.Sleep(1000);
                        MakeRegistationFee();
                        //Thread.Sleep(1000);
                    }
                    if (Statics.OrderNumber.Contains("PL-LABC"))
                    {
                        WaitForElement(LABCRegistrationTab);
                        LABCRegistrationTab.Click();
                        //Thread.Sleep(1000);
                        MakeRegistationFee();
                        //Thread.Sleep(1000);
                    }

                }
            }
        }

        //Make Registration fee Method
        public void MakeRegistationFee()
        {
            //Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(500);
            WaitForElement(RoleAccountTab);
            WaitForElementToClick(RoleAccountTab);
            RoleAccountTab.Click();
            //Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(500);
            WaitForElement(RegistrationRef);
            var regRefNumber = RegistrationRef.Text;
            //Thread.Sleep(1000);
            WaitForElement(RegFeePaid);
            var FeePaidValue = RegFeePaid.Text.Replace("£", "");
            decimal actualRegFeePaid = Convert.ToDecimal(FeePaidValue);
            WaitForElement(RegFeeBalance);
            var RegBalanceValue = RegFeeBalance.Text.Replace("£", "");
            decimal actualRegFeeBal = Convert.ToDecimal(RegBalanceValue);
            if (actualRegFeeBal > 0)
            {
                WaitForElement(EnterPaymentButton);
                EnterPaymentButton.Click();
                //Thread.Sleep(1000);
                WaitForElement(MakePaymentDiv);
                Assert.IsTrue(MakePaymentDiv.Displayed);
                FinancesPage.MakeDirectPyament();
                CloseSpinneronPage();
            }
            //Thread.Sleep(1000);
        }
        //Setting up Local Authority Email address 
        public void SetupEmailonLocalAuthority()
        {
            Thread.Sleep(500);
            CloseSpinneronPage();
            CloseSpinneronDiv();     
            WaitForElement(ConditionsTab);
            ConditionsTab.Click();
            Thread.Sleep(500);
            CloseSpinneronPage();
            CloseSpinneronDiv();           
            if (ConditionsRows.Count > 0)
            {
                Thread.Sleep(500);
                CloseSpinneronPage();
                CloseSpinneronDiv();                
                var SettingofLocalAuthorityRole = ConditionsRows.Any(x => x.Text.Contains("Setting of Local Authority role - with valid email address"));
                Thread.Sleep(500);
                CloseSpinneronDiv();
                CloseSpinneronPage();           
                if (SettingofLocalAuthorityRole == true)
                {
                    WaitForElement(RolesTab);
                    RolesTab.Click();
                    CloseSpinneronPage();
                    CloseSpinneronDiv();
                    if (LocalAuthorityEmails.Count > 0)
                    {
                        WaitForElement(LocalAuthorityEmails[0]);
                        LocalAuthorityEmails[0].Click();
                        WaitForElement(ViewCompanyButton);
                        ViewCompanyButton.Click();
                        CloseSpinneronPage();
                        CloseSpinneronDiv();
                        WaitForElement(EditCompany);
                        WaitForElementToClick(EditCompany);
                        EditCompany.Click();
                        CloseSpinneronPage();
                        CloseSpinneronDiv();
                        WaitForElement(EditCompanyDiv);
                        Assert.IsTrue(EditCompanyDiv.Displayed);
                        WaitForElement(EditCompanyNextButton);
                        EditCompanyNextButton.Click();
                        CloseSpinneronPage();
                        CloseSpinneronDiv();
                        if (OfficeAddress.Count > 0)
                        {
                            WaitForElements(OfficeAddress);
                            OfficeAddress[0].Click();
                            WaitForElement(EditOfficeAddressButton);
                            EditOfficeAddressButton.Click();
                            WaitForElement(EditCompanyDetailsPage.OfficeNameInput);
                            CloseSpinneronDiv();
                            CloseSpinneronPage();
                            if (EditCompanyDetailsPage.EmailLabel.Count > 0)
                            {
                                WaitForElement(EditCompanyDetailsPage.EmailLabel[0]);
                                EditCompanyDetailsPage.EmailLabel[0].Click();
                            }
                            EditCompanyDetailsPage.EmailInput.Click();
                            EditCompanyDetailsPage.EmailInput.Clear();
                            EditCompanyDetailsPage.EmailInput.SendKeys(ConfigurationManager.AppSettings["Email"]);
                            CloseSpinneronDiv();
                            CloseSpinneronPage();
                            WaitForElement(EditCompanyDetailsPage.AddOfficeDataButton);
                            EditCompanyDetailsPage.AddOfficeDataButton.Click();
                            CloseSpinneronDiv();
                            CloseSpinneronPage();
                            WaitForElement(EditCompanyDetailsPage.NextButton);
                            EditCompanyDetailsPage.NextButton.Click();
                            CloseSpinneronDiv();
                            CloseSpinneronPage();
                        }
                        WaitForElement(EditCompanyDetailsPage.SaveButton);
                        EditCompanyDetailsPage.SaveButton.Click();
                        CloseSpinneronDiv();
                        CloseSpinneronPage();
                    }
                }
                else
                {
                    WaitForElement(OverviewTab);
                    OverviewTab.Click();
                    CloseSpinneronPage();
                    CloseSpinneronDiv();
                }
            }
            else
            {
                WaitForElement(OverviewTab);
                OverviewTab.Click();
                CloseSpinneronPage();
                CloseSpinneronDiv();

            }
        }
        //Setting up LandOwner Role 
        public void SetupLandOwnerRole()
        {
            if (Dashboardpage.DecisionButton.Count <= 0)
            {
                Dashboardpage.SelectOrder();
            }
            WaitForElement(Dashboardpage.ActionsButton);
            Dashboardpage.ActionsButton.Click();
            CloseSpinneronDiv();
            CloseSpinneronPage();
            WaitForElement(Dashboardpage.EditQuoteButton);
            Dashboardpage.EditQuoteButton.Click();
            WaitForElement(AddLABCQuotePage.StepTitle);
            if (AddLABCQuotePage.StepTitle.Text.Contains("KEY SITE DETAILS"))
            {
                WaitForElement(AddLABCQuotePage.NextButton);
                AddLABCQuotePage.NextButton.Click();
            }
            WaitForElement(AddLABCQuotePage.StepTitle);
            if (AddLABCQuotePage.StepTitle.Text.Contains("OTHER SITE DETAILS"))
            {
                WaitForElement(AddLABCQuotePage.NextButton);
                AddLABCQuotePage.NextButton.Click();
            }
            WaitForElement(AddLABCQuotePage.StepTitle);
            if (AddLABCQuotePage.StepTitle.Text.Contains("PLOT SCHEDULE"))
            {
                WaitForElement(AddLABCQuotePage.NextButton);
                AddLABCQuotePage.NextButton.Click();
            }
            WaitForElement(AddLABCQuotePage.StepTitle);
            if (AddLABCQuotePage.StepTitle.Text.Contains("PRODUCTS"))
            {
                WaitForElement(AddLABCQuotePage.NextButton);
                AddLABCQuotePage.NextButton.Click();
            }
            WaitForElement(AddLABCQuotePage.StepTitle);
            if (AddLABCQuotePage.StepTitle.Text.Contains("ADDITIONAL QUESTIONS"))
            {
                WaitForElement(AddLABCQuotePage.NextButton);
                AddLABCQuotePage.NextButton.Click();
            }
            WaitForElement(AddLABCQuotePage.StepTitle);
            if (AddLABCQuotePage.StepTitle.Text.Contains("ROLES"))
            {
                AddLABCQuotePage.SetLandOwnerRole();
                WaitForElement(AddLABCQuotePage.NextButton);
                AddLABCQuotePage.NextButton.Click();              
            }
            CloseSpinneronDiv();
            WaitForElement(AddLABCQuotePage.StepTitle);
            if (AddLABCQuotePage.StepTitle.Text.Contains("DECLARATION"))
            {
                WaitForElement(AddLABCQuotePage.ApplicationConfirmationRadiobutton);
                AddLABCQuotePage.ApplicationConfirmationRadiobutton.Click();
            }
        }
        //Setting up Housing Association Role 
        public void SetupHousingAssociationRole()
        {
            if (Dashboardpage.DecisionButton.Count <= 0)
            {
                Dashboardpage.SelectOrder();
            }
            WaitForElement(Dashboardpage.ActionsButton);
            Dashboardpage.ActionsButton.Click();
            CloseSpinneronDiv();
            CloseSpinneronPage();
            WaitForElement(Dashboardpage.EditQuoteButton);
            Dashboardpage.EditQuoteButton.Click();
            WaitForElement(AddLABCQuotePage.StepTitle);
            if (AddLABCQuotePage.StepTitle.Text.Contains("KEY SITE DETAILS"))
            {
                WaitForElement(AddLABCQuotePage.NextButton);
                AddLABCQuotePage.NextButton.Click();
            }
            WaitForElement(AddLABCQuotePage.StepTitle);
            if (AddLABCQuotePage.StepTitle.Text.Contains("OTHER SITE DETAILS"))
            {
                WaitForElement(AddLABCQuotePage.NextButton);
                AddLABCQuotePage.NextButton.Click();
            }
            WaitForElement(AddLABCQuotePage.StepTitle);
            if (AddLABCQuotePage.StepTitle.Text.Contains("PLOT SCHEDULE"))
            {
                WaitForElement(AddLABCQuotePage.NextButton);
                AddLABCQuotePage.NextButton.Click();
            }
            WaitForElement(AddLABCQuotePage.StepTitle);
            if (AddLABCQuotePage.StepTitle.Text.Contains("PRODUCTS"))
            {
                WaitForElement(AddLABCQuotePage.NextButton);
                AddLABCQuotePage.NextButton.Click();
            }
            WaitForElement(AddLABCQuotePage.StepTitle);
            if (AddLABCQuotePage.StepTitle.Text.Contains("ADDITIONAL QUESTIONS"))
            {
                WaitForElement(AddLABCQuotePage.NextButton);
                AddLABCQuotePage.NextButton.Click();
            }
            WaitForElement(AddLABCQuotePage.StepTitle);
            if (AddLABCQuotePage.StepTitle.Text.Contains("ROLES"))
            {
                AddLABCQuotePage.SetSocialHousingRole();
                WaitForElement(AddLABCQuotePage.NextButton);
                AddLABCQuotePage.NextButton.Click();
            }
            CloseSpinneronDiv();
            WaitForElement(AddLABCQuotePage.StepTitle);
            if (AddLABCQuotePage.StepTitle.Text.Contains("DECLARATION"))
            {
                WaitForElement(AddLABCQuotePage.ApplicationConfirmationRadiobutton);
                AddLABCQuotePage.ApplicationConfirmationRadiobutton.Click();
            }
        }
        public void CloseActiveTermCondition()
        {
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            WaitForElement(PGRegistration.TermsTab);
            PGRegistration.TermsTab.Click();
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            CheckActiveTerms();
            if(activeTerm == false)
            {
                WaitForElement(PGRegistration.AddTermsButton);
                PGRegistration.AddTermsButton.Click();
                CloseSpinneronDiv();
                CloseSpinneronPage();
                Thread.Sleep(500);
                WaitForElement(PGRegistration.AddTermsDiv);
                Assert.IsTrue(PGRegistration.AddTermsDiv.Displayed);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                Thread.Sleep(500);
                PGRegistration.CreateActiveTerm();
                CloseSpinneronDiv();
                CloseSpinneronPage();
                Thread.Sleep(500);
            }           
        }
    
        public void CheckActiveTerms()
        {
            if (PGRegistration.TermsList.Count > 0)
            {
                Thread.Sleep(500);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                Thread.Sleep(500);
                for (int i = 0; i < PGRegistration.TermsList.Count; i++)
                {
                     activeTerm = PGRegistration.TermsList.Any(x => x.Text.Contains("Active"));
                }               

            }
        }
    }
}
