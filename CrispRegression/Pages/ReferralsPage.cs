﻿using CrispAutomation.Features;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.PageObjects;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace CrispAutomation.Pages
{
    public class ReferralsPage : Support.Pages
    {
        public IWebDriver wdriver;
        public ReferralsPage(ISearchContext driver)
        {
            PageFactory.InitElements(driver, this);
            wdriver = (RemoteWebDriver)driver;
        }

        [FindsBy(How = How.XPath, Using = "//crisp-tabs//div//ul[@ref='tabHeader']/li[@class='au-target tab']//span//a[contains(text(),'Referrals')]")]
        public IWebElement ReferralTab;

        [FindsBy(How = How.XPath, Using = "//crisp-list[@list-title='Referrals']//crisp-action-button[@click.delegate='addReferral()']//div//a//i[text()='add']")]
        public IWebElement AddRefrralButton;

        [FindsBy(How = How.XPath, Using = "//crisp-list//ul[@ref='theList']//li[@class='au-target collection-item']//crisp-list-item//a[@class='au-target row list-item-contents clickable']//crisp-list-item-title//div[@class='title']/span")]
        public IList<IWebElement> ReferralList;

        [FindsBy(How = How.XPath, Using = "//crisp-dialog//div//crisp-header[@class='au-target']//nav//div[contains(@class,'au-target')]")]
        public IWebElement ReferQuoteDiv;

        [FindsBy(How = How.XPath, Using = "//crisp-input-date[@label='Decision Expected']//div[@class='au-target input-field']//input")]
        public IWebElement DecesionDate;

        [FindsBy(How = How.XPath, Using = "//div[@class='modal-content']//crisp-input-textarea[@label='Notes']//label")]
        public IWebElement ReferralNotesLabel;

        [FindsBy(How = How.XPath, Using = "//div[@class='modal-content']//crisp-input-textarea[@label='Notes']//textarea")]
        public IWebElement ReferralNotesInput;

        [FindsBy(How = How.XPath, Using = "//crisp-button[@click.delegate='createReferral()']//span//button[@class='au-target waves-effect waves-light btn-flat']")]
        public IWebElement ReferralQuoteButton;

        [FindsBy(How = How.XPath, Using = "//crisp-list[@list-title='Referrals']//div[@ref='theTable']//tbody//tr[contains(@class,'au-target collection-item')]//td[1]")]
        public IList<IWebElement> ReferralTypesList;

        [FindsBy(How = How.XPath, Using = "//div[@class='modal-content']//crisp-input-object//div//label[text()='Cancellation Reason']")]
        public IWebElement CancellationReason;     

        [FindsBy(How = How.XPath, Using = "//crisp-list//ul[@ref='theList']//li[@class='au-target collection-item']//crisp-list-item//a[@class='au-target row list-item-contents clickable']//crisp-list-item-title//div[@class='title']/span")]
        public IList<IWebElement> CancellationReasonsList;

        [FindsBy(How = How.XPath, Using = "//crisp-button[@click.delegate='ok()']//span//button[@class='au-target waves-effect waves-light btn']")]
        public IWebElement SelectButton;

        [FindsBy(How = How.XPath, Using = "//div[@class='modal-content']//crisp-input-object//div//label[text()='Technical Escalation Reason']")]
        public IWebElement TechnicalEscalationReason;

        [FindsBy(How = How.XPath, Using = "//crisp-list//ul[@ref='theList']//li[@class='au-target collection-item']//crisp-list-item//a[@class='au-target row list-item-contents clickable']//crisp-list-item-title//div[@class='title']/span")]
        public IList<IWebElement> TechnicalEscalationReasonsList;

        [FindsBy(How = How.XPath, Using = "//crisp-dialog//crisp-header//div[@class='nav-wrapper au-target']")]
        public IWebElement ChaseTechnicalEscalationDetailsDiv;

        [FindsBy(How = How.XPath, Using = "//div[@class='modal-content']//crisp-input-textarea[@label='Amendment Notes']//label")]
        public IWebElement AmendNotesLabel;

        [FindsBy(How = How.XPath, Using = "//div[@class='modal-content']//crisp-input-textarea[@label='Amendment Notes']//textarea")]
        public IWebElement AmendNotesInput;

        [FindsBy(How = How.XPath, Using = "//crisp-footer-button[@click.delegate='goForward()']//button[@class='au-target waves-effect waves-light btn-flat']")]
        public IWebElement NextButton;

        [FindsBy(How = How.XPath, Using = "//crisp-footer-button[@click.delegate='complete()']//button[@class='au-target waves-effect waves-light btn-flat']")]
        public IWebElement CompleteButton;

        [FindsBy(How = How.XPath, Using = "//crisp-input-html[@class='au-target']//div//table//tbody//tr//td")]
        public IList<IWebElement> Correspondence;

        [FindsBy(How = How.XPath, Using = "//crisp-input-html[@class='au-target']//div//table//tbody//tr//td//p//a[@href='https://www.premierguarantee.co.uk/']//img")]
        public IList<IWebElement> PGGenerateQuote;

        [FindsBy(How = How.XPath, Using = "//crisp-input-html[@class='au-target']//div//table//tbody//tr//td//h3//a[@href='https://www.labcwarranty.co.uk/']//img")]
        public IList<IWebElement> LABCGenerateQuote;

        [FindsBy(How = How.XPath, Using = "//crisp-list[@list-title='Referrals']//div[@ref='theTable']//tbody//tr[contains(@class,'au-target collection-item')]//td[4]")]
        public IList<IWebElement> AssignedToList;

        [FindsBy(How = How.XPath, Using = "//referral-view[@view-model.ref='referralView']//crisp-card[@class='au-target']//crisp-card-actions//crisp-action-button//div//a//i[text()='edit']")]
        public IWebElement EditReferralbutton;

        [FindsBy(How = How.XPath, Using = "//crisp-input-object//div//span[@click.delegate='containerClicked()']//span[@class='au-target']//span")]
        public IWebElement AssignedTo;
        
        [FindsBy(How = How.CssSelector, Using = "crisp-input-text[label = 'Search'] > div > label")]
        public IWebElement SearchLabel;

        [FindsBy(How = How.CssSelector, Using = "crisp-input-text[label = 'Search'] > div > input")]
        public IWebElement SearchInput;

        [FindsBy(How = How.XPath, Using = "//crisp-list[@ref='listElm']//ul//li[@class='au-target collection-item']//crisp-list-item[@class='au-target']//a[contains(@class,'au-target row list-item-contents clickable hasAvatar')]")]
        public IList<IWebElement> ReferralAssigneeList;

        [FindsBy(How = How.XPath, Using = "//crisp-list[@ref='listElm']//ul//li[@class='au-target collection-item']//crisp-list-item[@class='au-target']//a[contains(@class,'au-target row list-item-contents clickable hasAvatar')]//crisp-list-item-title//span//small")]
        public IList<IWebElement> ReferralAssigneetitle;

        public IWebElement referralAssigneeTitle => ReferralAssigneetitle.FirstOrDefault(x => x.Text.Contains(Statics.Username));

        [FindsBy(How = How.XPath, Using = "//crisp-input-textarea[@label='Notes']//div//label")]
        public IWebElement NotesLabel;

        [FindsBy(How = How.XPath, Using = "//crisp-input-textarea[@label='Notes']//div//textarea")]
        public IWebElement NotesInput;

        [FindsBy(How = How.XPath, Using = "//crisp-footer//div//crisp-button[@click.delegate='saveChanges()']//span//button[@class='au-target waves-effect waves-light btn-flat']")]
        public IWebElement SaveChangesButton;
   

        [FindsBy(How = How.XPath, Using = "//crisp-button[@click.delegate='decideReferral()']//span//button[@class='au-target waves-effect waves-light btn-flat']")]
        public IWebElement DecideButton;

        [FindsBy(How = How.XPath, Using = "//crisp-dialog//crisp-header[@class='au-target']//nav//div[@class='nav-wrapper au-target']")]
        public IWebElement DecideReferralDiv;

        [FindsBy(How = How.XPath, Using = "//div[@class='modal-content']//crisp-input-object//label[text()='Decision']")]
        public IWebElement DecesionLabel;

        [FindsBy(How = How.XPath, Using = "//div[@class='card-content']//ul//li[@class='au-target collection-item']//crisp-list-item//a//crisp-list-item-title//div//span")]
        public IList<IWebElement> DecesionList;

        [FindsBy(How = How.XPath, Using = "//div[@class='card-content']//ul//li[@class='au-target collection-item']//crisp-list-item//a//crisp-list-item-title//div//span[text()='Accept']")]
        public IWebElement AcceptDecesion;

        [FindsBy(How = How.XPath, Using = "//div[@class='modal-content']//crisp-input-textarea//div//label[text()='Decision Notes']")]
        public IWebElement DecesionNotesLabel;

        [FindsBy(How = How.XPath, Using = "//div[@class='modal-content']//crisp-input-textarea//div//textarea")]
        public IWebElement DecesionNotesInput;

        [FindsBy(How = How.XPath, Using = "//crisp-list[@list-title='Referrals']//div[@ref='theTable']//tbody//tr[contains(@class,'au-target collection-item')]//td[2]")]
        public IList<IWebElement> ReferralStatusList;

        [FindsBy(How = How.XPath, Using = "//div[@class='card-content']//ul//li[@class='au-target collection-item']//crisp-list-item//a//crisp-list-item-title//div//span[text()='Close Technical Escalation']")]
        public IWebElement CloseTechnicalEscalation;        

        public IWebElement TechnicalReferral => ReferralList.FirstOrDefault(x => x.Text.Contains("Technical"));
        public IWebElement InsurerReferral => ReferralList.FirstOrDefault(x => x.Text.Contains("Insurer"));
        public IWebElement CancelationReferral => ReferralList.FirstOrDefault(x => x.Text.Contains("Cancellation"));
        public IWebElement TechnicalEscalationReferral => ReferralList.FirstOrDefault(x => x.Text.Contains("Technical Escalation"));

        public void CreateReferrals()
        {
            CreateTechnicalReferral();
            CreateInsurerReferral();
            CreateCancellationReferral();
            CreateTechnicalEscalationReferral();
        }
        public void OpenReferral()
        {
            WaitForElement(ReferralTab);
            WaitForElementToClick(ReferralTab);
            ReferralTab.Click();
            CloseSpinneronDiv();
            CloseSpinneronPage();
            WaitForElement(AddRefrralButton);
            WaitForElementToClick(AddRefrralButton);
            AddRefrralButton.Click();
            CloseSpinneronPage();
            CloseSpinneronDiv();
            WaitForElements(ReferralList);
            Assert.IsTrue(ReferralList.Count > 0);
            CloseSpinneronPage();
            CloseSpinneronDiv();
        }
        public void CreateTechnicalReferral()
        {
            OpenReferral();
            if(ReferralList.Count>0)
            {
                WaitForElement(TechnicalReferral);
                WaitForElementToClick(TechnicalReferral);
                TechnicalReferral.Click();
            }
            WaitForElement(ReferQuoteDiv);
            Assert.IsTrue(ReferQuoteDiv.Displayed);
            AddReferral();
            WaitForElements(ReferralTypesList);
            Assert.IsTrue(ReferralTypesList.Any(x => x.Text.Contains("Technical")));
        }
        public void CreateInsurerReferral()
        {
            OpenReferral();
            if (ReferralList.Count > 0)
            {
                WaitForElement(InsurerReferral);
                WaitForElementToClick(InsurerReferral);
                InsurerReferral.Click();
            }
            WaitForElement(ReferQuoteDiv);
            Assert.IsTrue(ReferQuoteDiv.Displayed);
            AddReferral();
            WaitForElements(ReferralTypesList);
            Assert.IsTrue(ReferralTypesList.Any(x => x.Text.Contains("Insurer")));
        }
        public void CreateCancellationReferral()
        {
            OpenReferral();
            if (ReferralList.Count > 0)
            {
                WaitForElement(CancelationReferral);
                WaitForElementToClick(CancelationReferral);
                CancelationReferral.Click();
            }
            WaitForElement(ReferQuoteDiv);
            Assert.IsTrue(ReferQuoteDiv.Displayed);
            SelectCancellationReasons();
            AddReferral();
            WaitForElements(ReferralTypesList);
            Assert.IsTrue(ReferralTypesList.Any(x => x.Text.Contains("Cancellation")));
        }
        public void SelectCancellationReasons()
        {
            WaitForElement(CancellationReason);
            WaitForElementToClick(CancellationReason);
            CancellationReason.Click();
            WaitForElements(CancellationReasonsList);
            if (CancellationReasonsList.Count > 0)
            {
                foreach (var eachcancelreason in CancellationReasonsList)
                {
                    eachcancelreason.Click();
                }
            }
            WaitForElement(SelectButton);
            WaitForElementToClick(SelectButton);
            SelectButton.Click();
        }
        public void CreateTechnicalEscalationReferral()
        {
            OpenReferral();
            if (ReferralList.Count > 0)
            {
                WaitForElement(TechnicalEscalationReferral);
                WaitForElementToClick(TechnicalEscalationReferral);
                TechnicalEscalationReferral.Click();
            }
            WaitForElement(ReferQuoteDiv);
            Assert.IsTrue(ReferQuoteDiv.Displayed);
            SelectTechnicalEscalationReasons();
            AddReferral();
            ChaseTechnicalEscalationDetails();
            WaitForElements(ReferralTypesList);
            Assert.IsTrue(ReferralTypesList.Any(x => x.Text.Contains("Technical Escalation")));
            CloseSpinneronPage();
            CloseSpinneronDiv();
        }
        public void SelectTechnicalEscalationReasons()
        {
            WaitForElement(TechnicalEscalationReason);
            WaitForElementToClick(TechnicalEscalationReason);
            TechnicalEscalationReason.Click();
            WaitForElements(TechnicalEscalationReasonsList);
            if (TechnicalEscalationReasonsList.Count > 0)
            {
                foreach (var eachtechescalationreason in TechnicalEscalationReasonsList)
                {
                    eachtechescalationreason.Click();
                }
            }
            WaitForElement(SelectButton);
            WaitForElementToClick(SelectButton);
            SelectButton.Click();
        }

        public void ChaseTechnicalEscalationDetails()
        {
            WaitForElement(ChaseTechnicalEscalationDetailsDiv);
            Assert.IsTrue(ChaseTechnicalEscalationDetailsDiv.Displayed);
            WaitForElement(AmendNotesLabel);
            AmendNotesLabel.Click();
            WaitForElement(AmendNotesInput);
            AmendNotesInput.SendKeys("Test");
            WaitForElement(NextButton);
            WaitForElementToClick(NextButton);
            NextButton.Click();
            CloseSpinneronDiv();
            CloseSpinneronPage();
            WaitForElements(Correspondence);          
            if (LABCGenerateQuote.Count > 0)
            {
                CloseSpinneronDiv();
                WaitForElement(LABCGenerateQuote[0]);
                Assert.IsTrue(LABCGenerateQuote[0].Displayed);
                Console.WriteLine("LABC Generate  Quote details verified");
            }
            if (PGGenerateQuote.Count > 0)
            {
                CloseSpinneronDiv();
                WaitForElement(PGGenerateQuote[0]);
                Assert.IsTrue(PGGenerateQuote[0].Displayed);
                Console.WriteLine("LABC Generate  Quote details verified");
            }
            WaitForElement(CompleteButton);
            CompleteButton.Click();
            CloseSpinneronDiv();
            CloseSpinneronPage();
        }

        public void AddReferral()
        {
            WaitForElement(DecesionDate);
            Assert.IsTrue(DecesionDate.Displayed);
            WaitForElement(ReferralNotesLabel);
            ReferralNotesLabel.Click();
            WaitForElement(ReferralNotesInput);
            ReferralNotesInput.SendKeys("Test Refrral");
            WaitForElement(ReferralQuoteButton);
            WaitForElementToClick(ReferralQuoteButton);
            ReferralQuoteButton.Click();
            CloseSpinneronPage();
            CloseSpinneronDiv();
        }   
        public void AssignReferralstoLoginUser()
        {
            
                WaitForElement(ReferralTab);
                WaitForElementToClick(ReferralTab);
                ReferralTab.Click();
                CloseSpinneronDiv();
                CloseSpinneronPage();
            
            WaitForElements(AssignedToList);
            Dashboardpage.UserDetails();
            if (AssignedToList.Count>0)
            {
                for(int i=0; i <AssignedToList.Count; i++)
                {
                    var AssigneName = AssignedToList[i].Text;                 
                    if (!AssigneName.Contains(Statics.Username))
                    {
                        WaitForElement(AssignedToList[i]);
                        AssignedToList[i].Click();
                        CloseSpinneronDiv();
                        CloseSpinneronPage();
                        WaitForElement(EditReferralbutton);
                        WaitForElementToClick(EditReferralbutton);
                        EditReferralbutton.Click();
                        WaitForElement(AssignedTo);
                        AssignedTo.Click();
                        CloseSpinneronDiv();
                        CloseSpinneronPage();
                        WaitForElement(SearchLabel);
                        CloseSpinneronDiv();
                        CloseSpinneronPage();
                        WaitForElementToClick(SearchLabel);
                        SearchLabel.Click();
                        WaitForElement(SearchInput);
                        string userName = Statics.Username;
                        for (int j = 0; j < userName.Length; j++)
                        {
                            SearchInput.SendKeys(userName[j].ToString());
                            //Thread.Sleep(200);
                        }
                        //Thread.Sleep(500);
                        CloseSpinneronDiv();
                        CloseSpinneronPage();
                        //Thread.Sleep(500);
                        if (ReferralAssigneeList.Count > 0)
                        {
                            WaitForElements(ReferralAssigneeList);
                            ReferralAssigneeList[0].Click();
                            Thread.Sleep(500);
                            CloseSpinneronDiv();
                            CloseSpinneronPage();
                            
                        }
                        Thread.Sleep(500);
                        CloseSpinneronDiv();
                        CloseSpinneronPage();
                        WaitForElement(NotesLabel);
                        NotesLabel.Click();
                        WaitForElement(NotesInput);
                        NotesInput.SendKeys("Assignee Changed");
                        WaitForElement(SaveChangesButton);
                        SaveChangesButton.Click();
                        CloseSpinneronDiv();
                        CloseSpinneronPage();
                    }              
                }
            }
        }

        public void CloseAllReferrals()
        {            
            WaitForElement(ReferralTab);
            WaitForElementToClick(ReferralTab);
            ReferralTab.Click();
            CloseSpinneronDiv();
            CloseSpinneronPage();            
            AssignReferralstoLoginUser();
            CloseTechnicalReferral();
            CloseInsurerReferral();
            CloseCancellationReferral();
            CloseTechnicalEscalationReferral();
        }
        public void CloseTechnicalReferral()
        {
            WaitForElements(ReferralTypesList);
            var technicalreferral = ReferralTypesList.FirstOrDefault(x => x.Text.Contains("Technical"));
            if (technicalreferral != null)
            {
                technicalreferral.Click();
                CloseSpinneronDiv();
                CloseSpinneronPage();
                Thread.Sleep(500);
                WaitForElement(DecideButton);
                DecideButton.Click();
                WaitForElement(DecideReferralDiv);
                Assert.IsTrue(DecideReferralDiv.Displayed);
                WaitForElement(DecesionLabel);
                DecesionLabel.Click();
                WaitForElements(DecesionList);
                WaitForElement(AcceptDecesion);
                AcceptDecesion.Click();
                CloseSpinneronDiv();
                CloseSpinneronPage();
                Thread.Sleep(500);
                WaitForElement(DecesionNotesLabel);
                DecesionNotesLabel.Click();
                WaitForElement(DecesionNotesInput);
                DecesionNotesInput.SendKeys("Referral closing");
                WaitForElement(SaveChangesButton);
                SaveChangesButton.Click();
                CloseSpinneronDiv();
                CloseSpinneronPage();
                Thread.Sleep(500);
                WaitForElements(ReferralStatusList);
                Assert.IsTrue(ReferralStatusList.Any(x => x.Text.Contains("Closed")));
            }
        }
        public void CloseInsurerReferral()
        {
            WaitForElements(ReferralTypesList);
            var insurerreferral = ReferralTypesList.FirstOrDefault(x => x.Text.Contains("Insurer"));
            if (insurerreferral != null)
            {
                WaitForElement(insurerreferral);
                insurerreferral.Click();
                CloseSpinneronDiv();
                CloseSpinneronPage();
                Thread.Sleep(500);
                WaitForElement(DecideButton);
                DecideButton.Click();
                WaitForElement(DecideReferralDiv);
                Assert.IsTrue(DecideReferralDiv.Displayed);
                WaitForElement(DecesionLabel);
                DecesionLabel.Click();
                WaitForElements(DecesionList);
                WaitForElement(AcceptDecesion);
                AcceptDecesion.Click();
                CloseSpinneronDiv();
                CloseSpinneronPage();
                Thread.Sleep(500);
                WaitForElement(DecesionNotesLabel);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                Thread.Sleep(500);
                WaitForElementToClick(DecesionNotesLabel);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                Thread.Sleep(500);
                DecesionNotesLabel.Click();
                CloseSpinneronDiv();
                CloseSpinneronPage();
                Thread.Sleep(500);
                WaitForElement(DecesionNotesInput);
                DecesionNotesInput.SendKeys("Referral closing");
                WaitForElement(SaveChangesButton);
                SaveChangesButton.Click();
                WaitForElements(ReferralStatusList);
                Assert.IsTrue(ReferralStatusList.Any(x => x.Text.Contains("Closed")));
            }
        }

        public void CloseCancellationReferral()
        {
            WaitForElements(ReferralTypesList);
            var technicalreferral = ReferralTypesList.FirstOrDefault(x => x.Text.Contains("Request For Cancellation"));
            if (technicalreferral != null)
            {
                WaitForElement(technicalreferral);
                technicalreferral.Click();
                CloseSpinneronDiv();
                CloseSpinneronPage();
                Thread.Sleep(500);
                WaitForElement(DecideButton);
                DecideButton.Click();
                WaitForElement(DecideReferralDiv);
                Assert.IsTrue(DecideReferralDiv.Displayed);
                WaitForElement(DecesionLabel);
                DecesionLabel.Click();
                CloseSpinneronDiv();
                CloseSpinneronPage();
                Thread.Sleep(500);
                WaitForElements(DecesionList);
                WaitForElement(AcceptDecesion);
                AcceptDecesion.Click();
                CloseSpinneronDiv();
                CloseSpinneronPage();
                Thread.Sleep(500);
                WaitForElement(DecesionNotesLabel);
                WaitForElementToClick(DecesionNotesLabel);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                Thread.Sleep(500);
                DecesionNotesLabel.Click();
                CloseSpinneronDiv();
                CloseSpinneronPage();
                Thread.Sleep(500);
                WaitForElement(DecesionNotesInput);
                DecesionNotesInput.SendKeys("Referral closing");
                WaitForElement(SaveChangesButton);
                SaveChangesButton.Click();
                WaitForElements(ReferralStatusList);
                Assert.IsTrue(ReferralStatusList.Any(x => x.Text.Contains("Closed")));
            }
        }

        public void CloseTechnicalEscalationReferral()
        {
            WaitForElements(ReferralTypesList);
            var technicalescalationreferral = ReferralTypesList.FirstOrDefault(x => x.Text.Contains("Technical Escalation"));
            if (technicalescalationreferral != null)
            {
                WaitForElement(technicalescalationreferral);
                technicalescalationreferral.Click();
                CloseSpinneronDiv();
                CloseSpinneronPage();
                Thread.Sleep(500);
                WaitForElement(DecideButton);
                DecideButton.Click();
                WaitForElement(DecideReferralDiv);
                Assert.IsTrue(DecideReferralDiv.Displayed);
                WaitForElement(DecesionLabel);
                DecesionLabel.Click();
                CloseSpinneronDiv();
                CloseSpinneronPage();
                Thread.Sleep(500);
                WaitForElements(DecesionList);
                WaitForElement(CloseTechnicalEscalation);
                CloseTechnicalEscalation.Click();
                CloseSpinneronDiv();
                CloseSpinneronPage();
                Thread.Sleep(500);
                WaitForElement(DecesionNotesLabel);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                Thread.Sleep(500);
                WaitForElementToClick(DecesionNotesLabel);
                CloseSpinneronDiv();
                CloseSpinneronPage();             
                DecesionNotesLabel.Click();
                CloseSpinneronDiv();
                CloseSpinneronPage();
                Thread.Sleep(500);
                WaitForElement(DecesionNotesInput);
                DecesionNotesInput.SendKeys("Technical Escalation closing");
                CloseSpinneronDiv();
                CloseSpinneronPage();
                Thread.Sleep(500);
                WaitForElement(NextButton);
                NextButton.Click();
                WaitForElements(Correspondence);
                if (LABCGenerateQuote.Count > 0)
                {
                    CloseSpinneronDiv();
                    WaitForElement(LABCGenerateQuote[0]);
                    Assert.IsTrue(LABCGenerateQuote[0].Displayed);
                    Console.WriteLine("LABC Generate  Quote details verified");
                }
                if (PGGenerateQuote.Count > 0)
                {
                    CloseSpinneronDiv();
                    WaitForElement(PGGenerateQuote[0]);
                    Assert.IsTrue(PGGenerateQuote[0].Displayed);
                    Console.WriteLine("LABC Generate  Quote details verified");
                }
                WaitForElement(CompleteButton);
                CompleteButton.Click();
                CloseSpinneronDiv();
                CloseSpinneronPage();
                Thread.Sleep(500);
                WaitForElements(ReferralStatusList);
                Assert.IsTrue(ReferralStatusList.Any(x => x.Text.Contains("Closed")));
            }
        }
    }
}
