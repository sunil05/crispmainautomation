﻿using OpenQA.Selenium;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.PageObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using OpenQA.Selenium.Support.UI;
using CrispAutomation.Pages;
using CrispAutomation.Support;
using NUnit.Framework;
using NUnit.Framework.Internal;
using CrispAutomation.Features;
using System.Configuration;
using static CrispAutomation.Support.ExtensionMethods;

namespace CrispAutomation.Pages
{


    public class AddCompanyPage : Support.Pages
    {
        public IWebDriver wdriver;
        public AddCompanyPage(ISearchContext driver)
        {
            PageFactory.InitElements(driver, this);
            wdriver = (RemoteWebDriver)driver;
        }

        [FindsBy(How = How.XPath, Using = "//crisp-dialog//div[@class='au-target modal dialog brand default modal-fixed-footer modal-has-header modal-full-bleed modal-tall modal-wide']//crisp-header")]
        public IWebElement ContactDiv;        

        [FindsBy(How = How.XPath, Using = "//crisp-wizard-step[@class='au-target active']//company-name//crisp-input-text//label[text()='Enter the name of the company you wish to create']")]
        public IWebElement CompanyNameToCreate;

        [FindsBy(How = How.XPath, Using = "//crisp-wizard-step[@step-title='Search Existing Companies'][@class='au-target active']//company-name//crisp-input-text[@label='Enter the name of the company you wish to create']//input")]
        public IWebElement EnterCompanyName;       

        [FindsBy(How = How.XPath, Using = "//crisp-list[@list-title='Results of Search from Existing Companies in CRISP']/div[@class='noResults']")]
        public IWebElement NoResultsElement;

        [FindsBy(How = How.XPath, Using = "//crisp-list[@list-title='Companies House matches']/div[@class='noResults']")]
        public IWebElement NoResults;

        [FindsBy(How = How.XPath, Using = "//button[@ref='button'][text()='No Existing Match Found'][@class='au-target waves-effect waves-light btn-flat']")]
        public IWebElement NoExistingMatchFound;

        [FindsBy(How = How.XPath, Using = "//button[@ref='button'][text()='No Match Found'][@class='au-target waves-effect waves-light btn-flat']")]
        public IWebElement NoMatchFound;

        [FindsBy(How = How.XPath, Using = "//crisp-input-text[@label='Name']/div/LABEL[@data-error.bind='error'][text()='Name']")]
        public IWebElement CompanyName;

        [FindsBy(How = How.XPath, Using = "//crisp-input-text[@label='Name']/div/input[@ref='input'][@type='text']")]
        public IWebElement CompanyNameInput;

        [FindsBy(How = How.XPath, Using = "//crisp-input-number[@label='Year of registration']/div/LABEL[text()='Year of registration']")]
        public IWebElement YearOfRegistration;

        [FindsBy(How = How.XPath, Using = "//crisp-input-number[@label='Year of registration']/div/input")]
        public IWebElement EnterYearOfRegistration;

        [FindsBy(How = How.XPath, Using = "//crisp-input-text[@label='Registration number (e.g. AB123456 or 12345678)']/div/LABEL[text()='Registration number (e.g. AB123456 or 12345678)']")]
        public IWebElement RegistartionNumber;

        [FindsBy(How = How.XPath, Using = "//crisp-input-text[@label='Registration number (e.g. AB123456 or 12345678)']/div/input")]
        public IWebElement RegistrationNumberInput;
            
        [FindsBy(How = How.XPath, Using = "//crisp-picker[@label='Legal form type']/div/div/input")]
        public IWebElement LegalFormType;

        [FindsBy(How = How.XPath, Using = "//ul[@class='dropdown-content select-dropdown active']/li/span[text()='Limited Company - LTD']")]
        public IWebElement LegalFormTypeDropdown;
        
        [FindsBy(How = How.XPath, Using = "//crisp-card/crisp-card-content/div/crisp-input-bool/label[text()='Is a VIP?']")]
        public IWebElement VIPCheckBox;

        [FindsBy(How = How.XPath, Using = "//crisp-card/crisp-card-content/div/crisp-input-bool/label[text()='Has enhanced SH insolvency endorsement?']")]
        public IWebElement EndorseMentCheckBox;

        [FindsBy(How = How.XPath, Using = "//crisp-card/crisp-card-content/div//crisp-picker[@label='Insurance Agreement Group']//div//label/parent::div")]
        public IWebElement InsuranceGroupLabel;

        [FindsBy(How = How.XPath, Using = "//ul[@class='dropdown-content select-dropdown active']//li//span[text()='AmTrust']")]
        public IWebElement InsuranceGroupInput;

        [FindsBy(How = How.XPath, Using = "//crisp-input-text[@label='Facebook']//LABEL[@data-error.bind='error'][text()='Facebook']")]
        public IWebElement FaceBook;

        [FindsBy(How = How.XPath, Using = "//crisp-input-text[@label='Facebook']/div/input[@ref='input']")]
        public IWebElement FaceBookInput;

        [FindsBy(How = How.XPath, Using = "//crisp-input-text[@label='Google+']//LABEL[@data-error.bind='error'][text()='Google+']")]
        public IWebElement GoogleID;

        [FindsBy(How = How.XPath, Using = "//crisp-input-text[@label='Google+']/div/input[@ref='input']")]
        public IWebElement GoogleIDInput;

        [FindsBy(How = How.XPath, Using = "//crisp-input-text[@label='LinkedIn']//LABEL[@data-error.bind='error'][text()='LinkedIn']")]
        public IWebElement Linkedin;

        [FindsBy(How = How.XPath, Using = "//crisp-input-text[@label='LinkedIn']/div/input[@ref='input']")]
        public IWebElement LinkedinInput;

        [FindsBy(How = How.XPath, Using = "//crisp-input-text[@label='Twitter']//LABEL[@data-error.bind='error'][text()='Twitter']")]
        public IWebElement Twitter;

        [FindsBy(How = How.XPath, Using = "//crisp-input-text[@label='Twitter']/div/input[@ref='input']")]
        public IWebElement TwitterInput;

        [FindsBy(How = How.XPath, Using = "//crisp-action-button[@icon='add'][@click.delegate='addWebsite()']/div/a/i[text()='add']")]
        public IWebElement AddWebsiteButton;

        [FindsBy(How = How.XPath, Using = "//crisp-input-text[@label='Website Address']/div/LABEL[@data-error.bind='error'][text()='Website Address']")]
        public IWebElement WebsiteAddress;

        [FindsBy(How = How.XPath, Using = "//crisp-input-text[@label='Website Address']/div/input[contains(@id,'crisp-input-text')][@ref='input']")]
        public IWebElement WebSiteInputField;

        [FindsBy(How = How.CssSelector, Using = "body > ai-dialog-container:nth-child(4) > div > div > crisp-dialog > div > div.modal-footer > crisp-footer > nav > div > crisp-footer-button:nth-child(1) > button")]
        public IWebElement AddButton;

        [FindsBy(How = How.XPath, Using = "//crisp-button[@enabled.bind='canGoForward'][@click.delegate='goForward()'][@title='Move to the next step of creating a company']/span/button[@class='au-target waves-effect waves-light btn-flat'][text()='Next']")]
        public IWebElement NextButton;

        [FindsBy(How = How.XPath, Using = "//crisp-button[@enabled.bind='canGoBack'][@click.delegate='goBack()']//span//button[@class='au-target waves-effect waves-light btn-flat'][text()='Previous']")]
        public IWebElement PreviousButton;
       

        [FindsBy(How = How.XPath, Using = "//crisp-input-bool[contains(@label,'Opt')]//label")]
        public IList<IWebElement> OptCalls;

        [FindsBy(How = How.XPath, Using = "//crisp-input-bool[contains(@label,'TPS')]//label")]
        public IWebElement NotedOnTPS;

        [FindsBy(How = How.XPath, Using = "//crisp-input-bool[contains(@label,'Extranet access')]//label")]
        public IList<IWebElement> EnableExtranetAccessCheckboxes;

        [FindsBy(How = How.XPath, Using = "//crisp-action-button[@icon='add'][@click.delegate='addOffice()']/div/a/i[text()='add']")]
        public IWebElement AddOfficeButton;

        [FindsBy(How = How.XPath, Using = "//crisp-action-button[@icon='add'][@click.delegate='addEmployee()']/div/a/i")]
        public IWebElement AddEmployeeButton;

        [FindsBy(How = How.XPath, Using = "//div[@class='modal-content']/crisp-input-text[@label='Name']/div/label[text()='Name']")]
        public IWebElement OfficeName;

        [FindsBy(How = How.XPath, Using = "//div[@class='modal-content']/crisp-input-text[@label='Name']/div/input[@ref='input'][@type='text']")]
        public IWebElement OfficeNameInput;

        [FindsBy(How = How.CssSelector, Using = "body > ai-dialog-container:nth-child(4) > div > div > crisp-dialog > div > div.modal-content > crisp-input-object:nth-child(2) > div > label")]
        public IWebElement OfficeAddress;     

        [FindsBy(How = How.XPath,
          Using = "//crisp-input-text[@label='Enter postcode to search']/div[@class='input-field']/input")]
        public IWebElement PostcodeInput;

        [FindsBy(How = How.XPath,
            Using = "//input[@type='text'][@class='select-dropdown'][@value='Labc Warranty, 2 Shore Lines Building, Shore Road, Merseyside, CH41 1AU']")]
        public IWebElement AddressDropdown;

        [FindsBy(How = How.XPath, Using = "//ul[@class='dropdown-content select-dropdown active']/li[2]")]
        public IWebElement AddressDropdownInput;

        [FindsBy(How = How.XPath, Using = "//button[text()='Enter Address Manually']")]
        public IWebElement EnterManuallyButton;

        [FindsBy(How = How.XPath, Using = "//LABEL[@data-error.bind='error'][text()='Address Line 1']")]
        public IWebElement AddressLine1;

        [FindsBy(How = How.XPath, Using = "//crisp-input-text[@label='Address Line 1']/div/input[@ref='input']")]
        public IWebElement AddressLine1EnterValue;

        [FindsBy(How = How.XPath, Using = "//crisp-button[@click.delegate='confirmManualAddressSelection()']/span/BUTTON[@ref='button']")]
        public IWebElement ConfirmSelection;

        [FindsBy(How = How.CssSelector, Using = "body > ai-dialog-container:nth-child(4) > div > div > crisp-dialog > div > div.modal-content > crisp-input-bool > label")]
        public IWebElement RegisteredAddress;

        [FindsBy(How = How.CssSelector, Using = "body > ai-dialog-container:nth-child(4) > div > div > crisp-dialog > div > div.modal-content > crisp-input-bool:nth-child(4) > label")]
        public IWebElement DefaultAddress;

        [FindsBy(How = How.XPath, Using = "//crisp-card-header[@class='au-target']/div/span[text()='Address details']")]
        public IWebElement AddressDetails;

        [FindsBy(How = How.XPath, Using = "//crisp-input-text[@label='Email']/div/label[text()='Email']")]
        public IWebElement Email;

        [FindsBy(How = How.XPath, Using = "//crisp-input-text[@label='Email']/div/input[@ref='input']")]
        public IWebElement EmailInput;

        [FindsBy(How = How.XPath, Using = "//crisp-input-text[@label='${label} Number']/div/label[text()='Phone Number']")]
        public IWebElement PhoneNumber;

        [FindsBy(How = How.XPath, Using = "//crisp-input-text[contains(@value.bind,'phoneNumber')]/div[@class='input-field']/input[@ref='input'][@type='text'][contains(@id,'crisp-input-text')]")]
        public IWebElement PhoneNumberInput;

        [FindsBy(How = How.XPath, Using = "//crisp-input-text[contains(@label,'Number')]/div/label[text()='Phone Number']")]
        public IWebElement EmpPhone;

        [FindsBy(How = How.XPath, Using = "//crisp-input-text[contains(@label,'Number')]/div/label[text()='Mobile Number']")]
        public IWebElement EmpMobile;

        [FindsBy(How = How.XPath, Using = "//div/div[2]/div/crisp-input-text[contains(@value.bind,'phoneNumber')]/div[@class='input-field']/input[@ref='input'][@type='text'][contains(@id,'crisp-input-text')]")]
        public IWebElement EmpMobileInput;

        [FindsBy(How = How.XPath, Using = "//div/div[1]/div/crisp-input-text[contains(@value.bind,'phoneNumber')]/div[@class='input-field']/input[@ref='input'][@type='text'][contains(@id,'crisp-input-text')]")]
        public IWebElement EmpPhoneInput;

        [FindsBy(How = How.XPath, Using = "//crisp-input-number[@label='Phone Extension']/div/label[text()='Phone Extension']")]
        public IWebElement EmpPhoneExtension;

        [FindsBy(How = How.XPath, Using = "//crisp-input-number[@label='Phone Extension']/div/input")]
        public IWebElement EmpPhoneExtensionInput;

        [FindsBy(How = How.XPath, Using = "//LABEL[@data-error.bind='error'][text()='Premier Guarantee account manager']")]
        public IWebElement PGAccountManager;

        [FindsBy(How = How.XPath, Using = "//crisp-list[@ref='listElm']//ul/li[@class='au-target collection-item']//crisp-list-item[@can-click='true']//a[@class='au-target row list-item-contents clickable hasAvatar']")]
        public IList<IWebElement> PGAccountPerson;

        [FindsBy(How = How.XPath, Using = "//LABEL[@data-error.bind='error'][text()='LABC account manager']")]
        public IWebElement LABCAccountManager;

        [FindsBy(How = How.XPath, Using = "//crisp-list[@ref='listElm']//ul/li[@class='au-target collection-item']//crisp-list-item[@can-click='true']//a[@class='au-target row list-item-contents clickable hasAvatar']")]
        public IList<IWebElement> LABCAccountPerson;

        [FindsBy(How = How.XPath, Using = "//LABEL[@data-error.bind='error'][text()='CSU account manager']")]
        public IWebElement CSUAccountManager;

        [FindsBy(How = How.XPath, Using = "//crisp-list[@ref='listElm']//ul/li[@class='au-target collection-item']//crisp-list-item[@can-click='true']//a[@class='au-target row list-item-contents clickable hasAvatar']")]
        public IList<IWebElement> CSUAccountPerson;

        [FindsBy(How = How.XPath, Using = "//crisp-button[@enabled.bind='isOfficeDataValid']/span/button")]
        public IWebElement AddOfficeDataButton;

        [FindsBy(How = How.XPath, Using = "//LABEL[@class='au-target'][text()='Title']")]
        public IWebElement EmpTitleLabel;

        [FindsBy(How = How.XPath, Using = "//crisp-input-autocomplete[@label='Title']/div/input")]
        public IWebElement EmpTitleInput;

        [FindsBy(How = How.XPath, Using = "//crisp-input-text[@label='First name']//LABEL[@class='au-target'][text()='First name']")]
        public IWebElement EmpFirstNameLabel;

        [FindsBy(How = How.XPath, Using = "//crisp-input-text[@label='First name']//input")]
        public IWebElement EmpFirstNameInput;

        [FindsBy(How = How.XPath, Using = "//crisp-input-text[@label='Surname']//LABEL[@class='au-target'][text()='Surname']")]
        public IWebElement EmpSurNameLabel;

        [FindsBy(How = How.XPath, Using = "//crisp-input-text[@label='Surname']//input")]
        public IWebElement EmpSurNameInput;

        [FindsBy(How = How.XPath, Using = "//crisp-input-text[@label='Suffix']//LABEL[@class='au-target'][text()='Suffix']")]
        public IWebElement EmpSuffixLabel;

        [FindsBy(How = How.XPath, Using = "//crisp-input-text[@label='Suffix']//input")]
        public IWebElement EmpSuffixInput;

        [FindsBy(How = How.XPath, Using = "//crisp-input-text[@label='Email']//LABEL[@class='au-target'][text()='Email']")]
        public IWebElement EmpEmailLabel;

        [FindsBy(How = How.XPath, Using = "//crisp-input-text[@label='Email']//input")]
        public IWebElement EmpEmailidInput;

        [FindsBy(How = How.XPath, Using = "//crisp-input-bool[@label='Company Director']//LABEL[@class='au-target'][text()='Company Director']")]
        public IWebElement EmpCompanyDirector;

        [FindsBy(How = How.XPath, Using = "//crisp-input-bool[@label='Purchasing Decision Maker']//LABEL[@class='au-target'][text()='Purchasing Decision Maker']")]
        public IWebElement EmpPurchasingDecisionMaker;

        [FindsBy(How = How.XPath, Using = "//crisp-input-text[@label='Job title']//div//label")]
        public IWebElement EmpJobTitle;

        [FindsBy(How = How.XPath, Using = "//crisp-input-text[@label='Job title']//div//input")]
        public IWebElement EmpJobTitleInput;

        [FindsBy(How = How.XPath, Using = "//label[@click.delegate='labelClicked()'][@data-error='Primary Office is invalid']")]
        public IWebElement EmpPrimaryOffice;

        [FindsBy(How = How.XPath, Using = "//crisp-list[@source.two-way='offices']//ul//li[@click.delegate='clicked(item)'][@class='au-target collection-item']")]
        public IWebElement EmpChooseOffice;

        [FindsBy(How = How.XPath, Using = "//crisp-button[@click.delegate='save()']//button[text()='Add']")]
        public IWebElement AddEmp;

        [FindsBy(How = How.XPath, Using = "//li[@click.delegate='clicked(item)']/crisp-list-item")]
        public IWebElement ChooseOffice;

        [FindsBy(How = How.XPath, Using = "//crisp-button[@enabled.bind='hasSelection']/span/button")]
        public IWebElement SelectButton;

        [FindsBy(How = How.XPath, Using = "//crisp-button[@click.delegate='save()']/span//button[@ref='button'][text()='Add']")]
        public IWebElement Add;

        [FindsBy(How = How.XPath, Using = "//i[@class='fa fa-plus add action au-target']")]
        public IWebElement Offices;

        [FindsBy(How = How.XPath, Using = "//BUTTON[@ref='button'][text()='Save']")]
        public IWebElement SaveButton;

        [FindsBy(How = How.CssSelector, Using = "body > div.custom-element > router-view > crisp-header > nav > div > crisp-header-avatar > img")]
        public IWebElement Imageperson;

        [FindsBy(How = How.XPath, Using = "//crisp-picker[@label='${label} Country code']/div/div/input")]
        public IWebElement PhoneCountry;

        [FindsBy(How = How.XPath, Using = "//div[1]/div/crisp-picker[@selected.bind='countryCode & validate']/div/div/input")]
        public IWebElement EmpPhoneCountry;

        [FindsBy(How = How.XPath, Using = "//ul[@class='dropdown-content select-dropdown active']/li/span[text()='United Kingdom']")]
        public IWebElement PhoneCountryDropdown;

        [FindsBy(How = How.XPath, Using = "//div[2]/div/crisp-picker[@selected.bind='countryCode & validate']/div/div/input")]
        public IWebElement EmpMobileCountry;

        [FindsBy(How = How.XPath, Using = "//ul[@class='dropdown-content select-dropdown active']/li/span[text()='United Kingdom']")]
        public IWebElement MobileCountryDropdown;


        [FindsBy(How = How.XPath, Using = "//crisp-list[@list-title='Results of Search from Existing Companies in CRISP']/ul[@ref='theList']/li[2]")]
        public IWebElement ExistingCompaniesFromCrispDB;

        // [FindsBy(How = How.XPath, Using = "//crisp-list[@list-title='Companies House matches']/ul/li[2]")]
        // public IWebElement SelectCompanyFromCompaniesHouse;

        [FindsBy(How = How.XPath, Using = "//crisp-wizard[@errors.bind='saveErrors']/div/crisp-button/span/button")]
        public IWebElement ErrorDisplayed;

        [FindsBy(How = How.XPath, Using = "//div[@class='title']/div[@class='au-target']")]
        public IList<IWebElement> SelectCompanyFromCompaniesHouse;

        [FindsBy(How = How.XPath, Using = "//crisp-wizard-step[@step-title='Offices']//crisp-list[@list-title='Offices']//ul[@ref='theList']//li[contains(@class,'au-target collection-item')]")]
        public IList<IWebElement> OfficesList;

        [FindsBy(How = How.XPath, Using = "//ul[@class='au-target collection has-header has-filter']//li[@class='au-target collection-item active']")]
        public IList<IWebElement> EmpList;

        [FindsBy(How = How.XPath, Using = "//crisp-list[@list-title='Employees']/ul[@ref='theList']/li[2]")]
        public IWebElement SingleEmp;

        [FindsBy(How = How.CssSelector, Using = "crisp-card-actions > div > crisp-action-button > div > a > i")]
        public IWebElement EmpEdit;

        [FindsBy(How = How.XPath, Using = "//crisp-input-autocomplete[@label='Title']/div/label")]
        public IWebElement SalutationLabel;

        [FindsBy(How = How.XPath, Using = "//crisp-input-autocomplete[@label='Title']/div/input")]
        public IWebElement SalutationInput;

        [FindsBy(How = How.XPath, Using = "//crisp-input-autocomplete[@label='Title']/div/span")]
        public IWebElement SalutationRequired;

        [FindsBy(How = How.XPath, Using = "//crisp-button/span/button[text()='Enter Address Using Postcode']")]
        public IWebElement EnterAddressUsingPostcodeButton;

        [FindsBy(How = How.XPath, Using = "//crisp-button[1]/span/button[text()='Set Address']")]
        public IWebElement SetAddressButton;





        public void SearchExistingCompanyDetails(string companyname, string registrationnumber)
        {
            WaitForElement(CompanyNameToCreate);
            CompanyNameToCreate.Click();
            EnterCompanyName.SendKeys(companyname);
            NoExistingMatchFound.Click();
        }
        public void SearchFromCompaniesHouse(string companyname, string registrationnumber)
        {
            NoMatchFound.Click();
        }
        public void FillCompanyDetails()
        {
            WaitForElement(CompanyName);
            CompanyName.Click();
            if (CompanyNameInput.Text != null)
            {
                WaitForElement(CompanyName);
                WaitForElementToClick(CompanyName);
                CompanyName.Click();
                WaitForElement(CompanyNameInput);
                WaitForElementToClick(CompanyNameInput);
                CompanyNameInput.Click();
                CompanyNameInput.Clear();
                CompanyNameInput.SendKeys("MDISCompany");
            }
            WaitForElement(YearOfRegistration);
            YearOfRegistration.Click();
            WaitForElement(EnterYearOfRegistration);
            EnterYearOfRegistration.Clear();
            EnterYearOfRegistration.SendKeys("2018");
            WaitForElement(RegistartionNumber);
            RegistartionNumber.Click();
            WaitForElement(RegistrationNumberInput);
            RegistrationNumberInput.Clear();
            RegistrationNumberInput.SendKeys("AB123456");
            WaitForElement(LegalFormType);
            LegalFormType.Click();
            WaitForElement(LegalFormTypeDropdown);
            LegalFormTypeDropdown.Click();
            WaitForElement(VIPCheckBox);
            VIPCheckBox.Click();
            WaitForElement(EndorseMentCheckBox);
            EndorseMentCheckBox.Click();
            WaitForElement(InsuranceGroupLabel);
            InsuranceGroupLabel.Click();
            WaitForElement(InsuranceGroupInput);
            WaitForElementToClick(InsuranceGroupInput);
            InsuranceGroupInput.Click();
            WaitForElement(FaceBook);
            FaceBook.Click();
            WaitForElement(FaceBookInput);
            FaceBookInput.Clear();
            FaceBookInput.SendKeys("https://www.facebook.com/MDISTestCompany");
            WaitForElement(GoogleID);
            GoogleID.Click();
            WaitForElement(GoogleIDInput);
            GoogleIDInput.Clear();
            GoogleIDInput.SendKeys("https://plus.google.com/MDISTestCompany");
            WaitForElement(Linkedin);
            Linkedin.Click();
            WaitForElement(LinkedinInput);
            LinkedinInput.Clear();
            LinkedinInput.SendKeys("https://www.linkedin.com/MDISTestCompany");
            WaitForElement(Twitter);
            Twitter.Click();
            WaitForElement(TwitterInput);
            TwitterInput.Clear();
            TwitterInput.SendKeys("@MDISTestCompany ");
            WaitForElement(AddWebsiteButton);
            AddWebsiteButton.Click();
            WaitForElement(WebsiteAddress);
            WebsiteAddress.Click();
            WaitForElement(WebSiteInputField);
            WebSiteInputField.SendKeys("www.MDISTestCompany.com");
            WaitForElement(AddButton);
            AddButton.Click();
            CloseSpinneronDiv();
            CloseSpinneronPage();
            WaitForElement(NextButton);
            NextButton.Click();
            CloseSpinneronDiv();
            CloseSpinneronPage();
        }
        public void SalesAndMarketingDetails()
        {
            //Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(500);
            WaitForElements(OptCalls);
            foreach (var eachCheckbox in OptCalls)
            {
                eachCheckbox.Click();
            }
            WaitForElement(NotedOnTPS);
            NotedOnTPS.Click();
            CloseSpinneronDiv();
            CloseSpinneronPage();
        }
        public void EntranetAccessDetails()
        {
            WaitForElements(EnableExtranetAccessCheckboxes);
            foreach (var eachCheckbox in EnableExtranetAccessCheckboxes)
            {
                eachCheckbox.Click();
            }
            CloseSpinneronDiv();
            CloseSpinneronPage();
        }


        public void AddOfficeDetails()
        {
            WaitForElement(OfficeName);
            OfficeName.Click();
            OfficeNameInput.SendKeys("MDIS Office");
            WaitForElement(OfficeAddress);
            OfficeAddress.Click();
            WaitForElement(PostcodeInput);
            PostcodeInput.SendKeys("CH41 1AU");
            CloseSpinneronDiv();
            WaitForElement(AddressDropdown);
            AddressDropdown.Click();
            WaitForElement(AddressDropdownInput);
            AddressDropdownInput.Click();
            WaitForElement(SetAddressButton);
            SetAddressButton.Click();
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            WaitForElement(RegisteredAddress);
            RegisteredAddress.Click();
            WaitForElement(DefaultAddress);
            DefaultAddress.Click();
            WaitForElement(Email);
            Email.Click();
            WaitForElement(EmailInput);
            EmailInput.SendKeys(ConfigurationManager.AppSettings["Email"]);
            WaitForElement(PhoneCountry);
            PhoneCountry.Click();
            //Thread.Sleep(500);
            WaitForElement(PhoneCountryDropdown);
            PhoneCountryDropdown.Click();
            WaitForElement(PhoneNumber);
            PhoneNumber.Click();
            WaitForElement(PhoneNumberInput);
            PhoneNumberInput.SendKeys("01483493735");
            CloseSpinneronDiv();
            CloseSpinneronPage();
            WaitForElement(PGAccountManager);
            WaitForElementToClick(PGAccountManager);
            PGAccountManager.Click();
            //Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(500);
            WaitForElements(PGAccountPerson);
            WaitForElementToClick(PGAccountPerson[0]);
            PGAccountPerson[0].Click();
            //Thread.Sleep(500);
            CloseCrispCard();
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            WaitForElement(LABCAccountManager);
            WaitForElementToClick(LABCAccountManager);
            LABCAccountManager.Click();
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            WaitForElements(LABCAccountPerson);
            WaitForElementToClick(LABCAccountPerson[0]);
            LABCAccountPerson[0].Click();
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            CloseCrispCard();
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(500);
            WaitForElement(CSUAccountManager);
            WaitForElementToClick(CSUAccountManager);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            CSUAccountManager.Click();
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(500);
            WaitForElements(CSUAccountPerson);
            WaitForElementToClick(CSUAccountPerson[0]);
            CSUAccountPerson[0].Click();
            Thread.Sleep(500);
            CloseCrispCard();
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(500);
            SalesAndMarketingDetails();
            CloseSpinneronDiv();
            WaitForElement(AddOfficeDataButton);
            AddOfficeDataButton.Click();
            CloseSpinneronDiv();
            WaitForElement(NextButton);
            NextButton.Click();
        }

        public void AddEmpDetails()
        {
            var TestEmp = $"Test{RunId.Id}";
            Statics.EmpName = TestEmp;
            WaitForElement(EmpTitleLabel);
            EmpTitleLabel.Click();
            EmpTitleInput.SendKeys("Mr");
            WaitForElement(EmpFirstNameLabel);
            EmpFirstNameLabel.Click();
            EmpFirstNameInput.SendKeys(Statics.EmpName);
            WaitForElement(EmpSurNameLabel);
            EmpSurNameLabel.Click();
            WaitForElement(EmpSurNameInput);
            EmpSurNameInput.SendKeys("Kumar");
            WaitForElement(EmpSuffixLabel);
            EmpSuffixLabel.Click();
            WaitForElement(EmpSuffixInput);
            EmpSuffixInput.SendKeys("k");
            WaitForElement(EmpEmailLabel);
            EmpEmailLabel.Click();
            WaitForElement(EmpEmailidInput);
            EmpEmailidInput.SendKeys(ConfigurationManager.AppSettings["Email"]);
            EmpCompanyDirector.Click();
            EmpPurchasingDecisionMaker.Click();
            WaitForElement(EmpJobTitle);
            EmpJobTitle.Click();
            WaitForElement(EmpJobTitleInput);
            EmpJobTitleInput.SendKeys("Automation Test Engineer");
            SalesAndMarketingDetails();
            //EntranetAccessDetails();
            WaitForElement(EmpPhone);
            EmpPhone.Click();
            EmpPhoneInput.SendKeys("01483493726");

            WaitForElement(EmpPhoneCountry);
            string phoneCountry = EmpPhoneCountry.GetAttribute("value");
            if (phoneCountry != "United Kingdom")
            {
                WaitForElementToClick(EmpPhoneCountry);
                EmpPhoneCountry.Click();

                WaitForElement(PhoneCountryDropdown);
                WaitForElementToClick(PhoneCountryDropdown);

                PhoneCountryDropdown.Click();

            }
            WaitForElement(EmpPhoneExtension);
            EmpPhoneExtension.Click();
            WaitForElement(EmpPhoneExtensionInput);
            EmpPhoneExtensionInput.SendKeys("752");
            WaitForElement(EmpMobile);
            EmpMobile.Click();
            WaitForElement(EmpMobileInput);
            EmpMobileInput.SendKeys("07764233944");
            WaitForElement(EmpMobileCountry);
            string mobileCountry = EmpPhoneCountry.GetAttribute("value");
            if (mobileCountry != "United Kingdom")
            {
                WaitForElementToClick(EmpMobileCountry);
                EmpMobileCountry.Click();

                WaitForElement(MobileCountryDropdown);

                MobileCountryDropdown.Click();

            }
            WaitForElement(EmpPrimaryOffice);
            EmpPrimaryOffice.Click();

            WaitForElement(EmpChooseOffice);
            EmpChooseOffice.Click();

            WaitForElement(Offices);
            Offices.Click();
            CloseSpinneronDiv();
            CloseSpinneronPage();
            WaitForElement(EmpChooseOffice);
            EmpChooseOffice.Click();
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            WaitForElement(SelectButton);
            SelectButton.Click();
            //Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            WaitForElement(AddEmp);
            AddEmp.Click();
            //Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(500);
            WaitForElement(NextButton);
            NextButton.Click();
        }

        public void AddExistingCompanyFromCrispDatabase()
        {
            WaitForElement(ExistingCompaniesFromCrispDB);
            ExistingCompaniesFromCrispDB.Click();
            CloseSpinneronDiv();
            CloseSpinneronPage();

        }

        public void AddExistingCompanyFromCompaniesHouse()
        {

            if (SelectCompanyFromCompaniesHouse.Count > 0)
            {
                WaitForElements(SelectCompanyFromCompaniesHouse);
                SelectCompanyFromCompaniesHouse[0].Click();
                CloseSpinneronDiv();

            }
            else
            {
                WaitForElement(PreviousButton);
                PreviousButton.Click();
                CloseSpinneronDiv();
                WaitForElement(EnterCompanyName);
                EnterCompanyName.Click();
                EnterCompanyName.Clear();
                EnterCompanyName.SendKeys("Axa");
                CloseSpinneronDiv();
                WaitForElement(NoExistingMatchFound);
                NoExistingMatchFound.Click();
                WaitForElements(SelectCompanyFromCompaniesHouse);
                SelectCompanyFromCompaniesHouse[0].Click();
                CloseSpinneronDiv();

            }
            WaitForElement(NextButton);

            NextButton.Click();
            CloseSpinneronDiv();

            WaitForElements(OfficesList);
            if (OfficesList.Count > 0)
            {
                foreach (var eachOffice in OfficesList)
                {
                    //Thread.Sleep(500);                
                    CloseSpinneronDiv();
                    //Thread.Sleep(1000);   
                    eachOffice.Click();

                    CloseSpinneronDiv();

                    WaitForElement(EditCompanyDetailsPage.EditOfficeAddressButton);
                    EditCompanyDetailsPage.EditOfficeAddressButton.Click();
                    WaitForElement(EditCompanyDetailsPage.OfficeNameInput);
                    EditCompanyDetailsPage.EditOfficeDetails();
                    //Thread.Sleep(500);
                    CloseSpinneronDiv();
                    //Thread.Sleep(1000);
                }
            }

            CloseSpinneronDiv();

            WaitForElement(NextButton);
            NextButton.Click();
            CloseSpinneronDiv();

            WaitForElement(NextButton);
            NextButton.Click();
            CloseSpinneronDiv();

            WaitForElement(SaveButton);
            SaveButton.Click();
            CloseSpinneronDiv();

            CloseSpinneronPage();
        }

        public void CreateCompany(string companyname)
        {
            WaitForElement(Dashboardpage.PlusButton);
            Dashboardpage.PlusButton.Click();
            WaitForElement(Dashboardpage.CompanyButton);
            Dashboardpage.CompanyButton.Click();
            CloseSpinneronDiv();
            CloseSpinneronPage();

            WaitForElement(ContactDiv);
            Assert.IsTrue(ContactDiv.Displayed);
            CloseSpinneronDiv();
            CloseSpinneronPage();

            WaitForElement(CompanyNameToCreate);
            EnterCompanyName.SendKeys(companyname);
            CloseSpinneronDiv();
            CloseSpinneronPage();

            WaitForElement(NoExistingMatchFound);
            NoExistingMatchFound.Click();
            CloseSpinneronDiv();
            CloseSpinneronPage();

            WaitForElement(AddCompanyPage.NoMatchFound);
            AddCompanyPage.NoMatchFound.Click();

            WaitForElement(AddCompanyPage.CompanyName);
            CloseSpinneronDiv();
            CloseSpinneronPage();

            FillCompanyDetails();

            CloseSpinneronDiv();
            CloseSpinneronPage();

            WaitForElement(AddOfficeButton);

            CloseSpinneronDiv();
            CloseSpinneronPage();

            AddOfficeButton.Click();

            CloseSpinneronDiv();
            CloseSpinneronPage();

            WaitForElement(OfficeNameInput);

            CloseSpinneronDiv();
            CloseSpinneronPage();

            AddOfficeDetails();
            CloseSpinneronDiv();
            CloseSpinneronPage();

            WaitForElement(AddEmployeeButton);
            AddEmployeeButton.Click();

            WaitForElement(EmpTitleLabel);
            CloseSpinneronDiv();
            CloseSpinneronPage();

            AddEmpDetails();
            CloseSpinneronDiv();
            CloseSpinneronPage();

            WaitForElement(SaveButton);
            SaveButton.Click();
            CloseSpinneronDiv();
            CloseSpinneronPage();
            CloseCrispDiv(1);

            WaitForElement(Dashboardpage.CompanyImage);
            Assert.True(Dashboardpage.CompanyImage.Displayed);
            WaitForElement(Dashboardpage.CompanyTitle);
            Statics.CompanyName = Dashboardpage.CompanyTitle.Text;
        }
    }
}



