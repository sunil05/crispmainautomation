﻿Feature: ManualConditions
	Background: 
	Given I am on login page
	When I enter crisp username 
	And I enter crisp password
	And I click on login button
	Then I should see all items displayed on dashboard page

@Smoketest
Scenario Outline: Verify Adding and Closing Manual Condition
When I click on plus button and select  quote option
And I read following '<plotdata>' details
And I provide details on key site details page
And I provide details on other site details page 
And I provide following '<plotdata>' data on plot schedule page
And I provide details on the product details page
And I provide details on additional questions page
And I select relavant roles on roles page
When I provide the details on declaration page
Then I click on complete button to see the application submitted details on the dashboard
#Creating Manual Conditions 
When I select on conditions option 
And I create conditions and verify them  
Then conditions should be displayed in the conditions page 
#When user try to Send LABC Brand Quote if there are blocking conditions
When I click on sendquote button 
And I verify the details on send quote rating page
And I verify the details on send quote securities page
And I verify above manual conditions details on conditions page
And I verify the details on send quote special terms page
And I verify the details on send quote fees page
And I verify above manual conditions on file review details page
And I verify the details on send quote endorsements page
And I verify the confirm chase on confirm details page
And I verify the details on send quote correspondence page
Then I should see quote is not changed into quoted
#Closing Manual Conditions and Send the quote
When I click on  manual conditions
And I perform the operation to close all conditions 
When I try to send the quote 
Then I should see quoted details on the dashboard

Examples: 
 | plotdata |
 |\\\tpgfile\NewCompany\IT\Department\QA Team\Current Projects\CRISP\CrispAutomation\CrispData\LABC-Products\NewHomes.xlsx |
 

@Smoketest
Scenario Outline: Verify Clone and Edit Conditions
When I click on plus button and select  quote option
And I read following '<plotdata>' details
And I provide details on key site details page
And I provide details on other site details page 
And I provide following '<plotdata>' data on plot schedule page
And I provide details on the product details page
And I provide details on additional questions page
And I select relavant roles on roles page
When I provide the details on declaration page
Then I click on complete button to see the application submitted details on the dashboard
#Creating Manual Condition and Clone Manual Condition 
When I select on conditions option 
And I create site condition and verify them  
And I perform the operation to clone all conditions 
Then I should see each condition has been cloned 
#Clone Automatic Conditions 
When I select automatic condition 
And I perform the operation to clone condition
Then I should see the condition has been cloned
#Edit Manual Condition 
When I perform the operation to edit condition
Then I should see the condition has been edited

Examples: 
 | plotdata                                                      |
 |\\\tpgfile\NewCompany\IT\Department\QA Team\Current Projects\CRISP\CrispAutomation\CrispData\LABC-Products\NewHomes.xlsx  | 
 
