﻿Feature: Finance
Background: 
	Given I am on login page
	When I enter crisp username 
	And I enter crisp password
	And I click on login button
	Then I should see all items displayed on dashboard page

#Finaces - Importing Bank Statements and Matching Accounts 
@Smoketest
Scenario: Verify Bank Statement Import
When I click on finances button 
And I Select the bank statement 
Then I perform operation on statement import section


#Finaces - Allocate Payments 
@Smoketest
Scenario:Verify Allocating Payments 
When I click on finances button 
And I Select the Allocate Payment Receipts option
When I perform operation to allocate payments
Then I should see the payments has been allocated


#Finaces - Transfer Money 
@Smoketest
Scenario: Verify Transfer Money 
When I click on finances button 
And I Select the transfer money option
When I perform operation to transfer the money
Then I should see the payments has been transfered