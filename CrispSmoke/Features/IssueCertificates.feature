﻿Feature: IssueCertificates
Background: 
    Given I am on login page
	When I enter crisp username 
	And I enter crisp password
	And I click on login button
	Then I should see all items displayed on dashboard page

#Issue COI DEVIC and PLOTIC  for PG HVS Products
@Smoketest
Scenario Outline: Verify Issue All Certificates on PG HVS Products
When I click on plus button and select  quote option
And I read following '<plotdata>' details
And I provide details on key site details page for PG Brand
And I provide details on other site details page 
And I provide following '<plotdata>' data on plot schedule page
And I provide details on the product details page
And I provide details on additional questions page
And I select relavant PG roles on roles page
When I provide the details on declaration page
Then I click on complete button to see the application submitted details on the dashboard
#Send Quote 
When I click on the send quote button 
And I verify the details on send quote rating page
And I verify the details on send quote securities page
And I verify the details on send quote conditions page
And I verify the details on send quote special terms page
And I verify the details on send quote fees page
And I verify the details on send quote file review page
And I verify the details on send quote endorsements page
And I verify the details on send quote confirm details page
And I verify the details on send quote correspondence page
Then I click on send quote button to see quoted details on the dashboard
#Accept Quote
When I click on acceptquote button 
And I verify the details on accept quote products page
And I verify the details on accept quote conditions page
And I verify the details on accept quote file review page 
And I verify the details on accept quote confirm page 
And I verify the details on accept quote internal roles page 
And I verify the details on accept quote correspondence page 
Then I click on accept quote button to see order details on the dashboard
#Pay the Fee balance condition
When I verify account and make the balance payments
#Security Document condition and Registration fee condition
When I clear security document conditions 
#Registration fee condition
#When I clear registration fee conditions
#Manual Conditions created automatically on High Value product
When I clear manual conditions
#Clear additional Conditions(Property Owner Condition etc)
When I clear PG additional conditions
#Surveyor Assessments 
When I login to surveyor site to submit surveyor assessments
#Clear Intial Notice Conditions 
When I clear intial notice condition
#Clear Intial Notice Conditions 
When I clear final notice conditions
#Send Final Notice and Issue COI
When I login back to crisp application to issue the COI
#Issue The DevelopmentIC
Then I login back to crisp application to issue the DevIC 
#Issue The PlotIC
Then I login back to crisp application to issue the plotIC

Examples: 
|plotdata                                                                                                                     |
|\\\tpgfile\NewCompany\IT\Department\QA Team\Current Projects\CRISP\CrispAutomation\CrispData\PG-Products\PG-AXA-HVS.xlsx|

#Issue COI DEVIC and PLOTIC  for PG LVS Products
@Smoketest
Scenario Outline: Verify Issue All Certificates on PG LVS Products
When I click on plus button and select  quote option
And I read following '<plotdata>' details
And I provide details on key site details page for PG Brand
And I provide details on other site details page 
And I provide following '<plotdata>' data on plot schedule page
And I provide details on the product details page
And I provide details on additional questions page
And I select relavant PG roles on roles page
When I provide the details on declaration page
Then I click on complete button to see the application submitted details on the dashboard
#Send Quote 
When I click on the send quote button 
And I verify the details on send quote rating page
And I verify the details on send quote securities page
And I verify the details on send quote conditions page
And I verify the details on send quote special terms page
And I verify the details on send quote fees page
And I verify the details on send quote file review page
And I verify the details on send quote endorsements page
And I verify the details on send quote confirm details page
And I verify the details on send quote correspondence page
Then I click on send quote button to see quoted details on the dashboard
#Accept Quote
When I click on acceptquote button 
And I verify the details on accept quote products page
And I verify the details on accept quote conditions page
And I verify the details on accept quote file review page 
And I verify the details on accept quote confirm page 
And I verify the details on accept quote internal roles page 
And I verify the details on accept quote correspondence page 
Then I click on accept quote button to see order details on the dashboard
#Pay the Fee balance condition
When I verify account and make the balance payments
#Security Document condition and Registration fee condition
When I clear security document conditions 
#Manual Conditions created automatically on High Value product
When I clear manual conditions
#Clear additional Conditions
When I clear PG additional conditions
#Surveyor Assessments 
When I login to surveyor site to submit surveyor assessments
#Clear Intial Notice Conditions 
When I clear intial notice condition
#Clear Intial Notice Conditions 
When I clear final notice conditions
#Send Final Notice and Issue COI
When I login back to crisp application to issue the COI
#Issue The DevelopmentIC
Then I login back to crisp application to issue the DevIC 
#Issue The PlotIC
Then I login back to crisp application to issue the plotIC

Examples: 
| plotdata                                                                                                                |
|\\\tpgfile\NewCompany\IT\Department\QA Team\Current Projects\CRISP\CrispAutomation\CrispData\PG-Products\PG-LVS.xlsx |


#Issue COI DEVIC and PLOTIC  for LABC LVS Products 
@Smoketest
Scenario Outline: Verify Issue All Certificates on LABC LVS Products
When I click on plus button and select  quote option
And I read following '<plotdata>' details
And I provide details on key site details page
And I provide details on other site details page 
And I provide following '<plotdata>' data on plot schedule page
And I provide details on the product details page
And I provide details on additional questions page
And I select relavant roles on roles page
When I provide the details on declaration page
Then I click on complete button to see the application submitted details on the dashboard
#Send Quote 
When I click on sendquote button 
And I verify the details on send quote rating page
And I verify the details on send quote securities page
And I verify the details on send quote conditions page
And I verify the details on send quote special terms page
And I verify the details on send quote fees page
And I verify the details on send quote file review page
And I verify the details on send quote endorsements page
And I verify the details on send quote confirm details page
And I verify the details on send quote correspondence page
Then I click on send quote button to see quoted details on the dashboard
#Accept Quote
When I click on acceptquote button 
And I verify the details on accept quote products page
And I verify the details on accept quote conditions page
And I verify the details on accept quote file review page 
And I verify the details on accept quote confirm page 
And I verify the details on accept quote internal roles page 
And I verify the details on accept quote correspondence page 
Then I click on accept quote button to see order details on the dashboard
#Pay the Fee balance condition
When I verify account and make the balance payments
#Security Document condition and Registration fee condition
When I clear security document conditions 
#Registration fee condition
#When I clear registration fee conditions
#Manual Conditions created automatically on High Value product
When I clear manual conditions
#Clear additional Conditions
When I clear LABC additional conditions
#Surveyor Assessments 
When I login to surveyor site to submit surveyor assessments
#Clear Intial Notice Conditions 
When I clear intial notice condition
#Clear Intial Notice Conditions 
When I clear final notice conditions
#Send Final Notice and Issue COI
When I login back to crisp application to issue the COI
#Issue The DevelopmentIC
Then I login back to crisp application to issue the DevIC 
#Issue The PlotIC
Then I login back to crisp application to issue the plotIC

Examples: 
| plotdata                                                                                                                        |
|\\\tpgfile\NewCompany\IT\Department\QA Team\Current Projects\CRISP\CrispAutomation\CrispData\LABC-Products\LABC-LVS.xlsx |
#Issue COI DEVIC and PLOTIC  for LABC Social Housing HVS Product 
@Smoketest
Scenario Outline: Verify Issue All Certificates on LABC HVS Products
When I click on plus button and select  quote option
And I read following '<plotdata>' details
And I provide details on key site details page
And I provide details on other site details page 
And I provide following '<plotdata>' data on plot schedule page
And I provide details on the product details page
And I provide details on additional questions page
And I select relavant roles on roles page
When I provide the details on declaration page
Then I click on complete button to see the application submitted details on the dashboard
#Send Quote 
When I click on sendquote button 
And I verify the details on send quote rating page
And I verify the details on send quote securities page
And I verify the details on send quote conditions page
And I verify the details on send quote special terms page
And I verify the details on send quote fees page
And I verify the details on send quote file review page
And I verify the details on send quote endorsements page
And I verify the details on send quote confirm details page
And I verify the details on send quote correspondence page
Then I click on send quote button to see quoted details on the dashboard
#Accept Quote
When I click on acceptquote button 
And I verify the details on accept quote products page
And I verify the details on accept quote conditions page
And I verify the details on accept quote file review page 
And I verify the details on accept quote confirm page 
And I verify the details on accept quote internal roles page 
And I verify the details on accept quote correspondence page 
Then I click on accept quote button to see order details on the dashboard
#Pay the Fee balance condition
When I verify account and make the balance payments
#Security Document condition and Registration fee condition
When I clear security document conditions 
#Registration fee condition
#When I clear registration fee conditions
#Manual Conditions created automatically on High Value product
When I clear manual conditions
#Clear additional Conditions
When I clear LABC additional conditions
#Surveyor Assessments 
When I login to surveyor site to submit surveyor assessments
#Clear Intial Notice Conditions 
When I clear intial notice condition
#Clear Intial Notice Conditions 
When I clear final notice conditions
#Send Final Notice and Issue COI
When I login back to crisp application to issue the COI
#Issue The DevelopmentIC
Then I login back to crisp application to issue the DevIC 
#Issue The PlotIC
Then I login back to crisp application to issue the plotIC

Examples: 
| plotdata                                                                                                                        |
| \\\tpgfile\NewCompany\IT\Department\QA Team\Current Projects\CRISP\CrispAutomation\CrispData\LABC-Products\LABC-HVS.xlsx |

#Issue COI for PG Channel Islands LVS Product
@Smoketest
Scenario Outline: Verify Issue COI on PG Channel Island LVS Product
When I click on plus button and select  quote option
And I read following '<plotdata>' details
And I provide details on key site details page for PG Brand
And I provide details on other site details page 
And I provide following '<plotdata>' data on plot schedule page
And I provide details on the product details page
And I provide details on additional questions page
And I select relavant PG roles on roles page
When I provide the details on declaration page
Then I click on complete button to see the application submitted details on the dashboard
#Send Quote 
When I click on the send quote button 
And I verify the details on send quote rating page
And I verify the details on send quote securities page
And I verify the details on send quote conditions page
And I verify the details on send quote special terms page
And I verify the details on send quote fees page
And I verify the details on send quote file review page
And I verify the details on send quote endorsements page
And I verify the details on send quote confirm details page
And I verify the details on send quote correspondence page
Then I click on send quote button to see quoted details on the dashboard
#Accept Quote
When I click on acceptquote button 
And I verify the details on accept quote products page
And I verify the details on accept quote conditions page
And I verify the details on accept quote file review page 
And I verify the details on accept quote confirm page 
And I verify the details on accept quote internal roles page 
And I verify the details on accept quote correspondence page 
Then I click on accept quote button to see order details on the dashboard
#Pay the Fee balance condition
When I verify account and make the balance payments
#Security Document condition and Registration fee condition
When I clear security document conditions 
#Registration fee condition
#When I clear registration fee conditions
#Manual Conditions created automatically on High Value product
When I clear manual conditions
#Clear additional Conditions
When I clear PG additional conditions
#Surveyor Assessments 
When I login to surveyor site to submit surveyor assessments
#Clear Intial Notice Conditions 
When I clear intial notice condition
#Clear Intial Notice Conditions 
When I clear final notice conditions
#Send Final Notice and Issue COI
When I login back to crisp application to issue the COI
#Issue The DevelopmentIC
Then I login back to crisp application to issue the DevIC 
#Issue The PlotIC
Then I login back to crisp application to issue the plotIC


Examples: 
| plotdata                                                                                                                       |
| \\\tpgfile\NewCompany\IT\Department\QA Team\Current Projects\CRISP\CrispAutomation\CrispData\PG-Products\CI-LVS.xlsx  |

#Issue COI for PG Channel Islands HVS Product
@Smoketest
Scenario Outline: Verify Issue COI on PG Channel Islands HVS Product
When I click on plus button and select  quote option
And I read following '<plotdata>' details
And I provide details on key site details page for PG Brand
And I provide details on other site details page 
And I provide following '<plotdata>' data on plot schedule page
And I provide details on the product details page
And I provide details on additional questions page
And I select relavant PG roles on roles page
When I provide the details on declaration page
Then I click on complete button to see the application submitted details on the dashboard
#Send Quote 
When I click on the send quote button 
And I verify the details on send quote rating page
And I verify the details on send quote securities page
And I verify the details on send quote conditions page
And I verify the details on send quote special terms page
And I verify the details on send quote fees page
And I verify the details on send quote file review page
And I verify the details on send quote endorsements page
And I verify the details on send quote confirm details page
And I verify the details on send quote correspondence page
Then I click on send quote button to see quoted details on the dashboard
#Accept Quote
When I click on acceptquote button 
And I verify the details on accept quote products page
And I verify the details on accept quote conditions page
And I verify the details on accept quote file review page 
And I verify the details on accept quote confirm page 
And I verify the details on accept quote internal roles page 
And I verify the details on accept quote correspondence page 
Then I click on accept quote button to see order details on the dashboard
#Pay the Fee balance condition
When I verify account and make the balance payments
#Security Document condition and Registration fee condition
When I clear security document conditions 
#Registration fee condition
#When I clear registration fee conditions
#Manual Conditions created automatically on High Value product
When I clear manual conditions
#Clear additional Conditions
When I clear PG additional conditions
#Surveyor Assessments 
When I login to surveyor site to submit surveyor assessments
#Clear Intial Notice Conditions 
When I clear intial notice condition
#Clear Intial Notice Conditions 
When I clear final notice conditions
#Send Final Notice and Issue COI
When I login back to crisp application to issue the COI
#Issue The DevelopmentIC
Then I login back to crisp application to issue the DevIC 
#Issue The PlotIC
Then I login back to crisp application to issue the plotIC


Examples: 
| plotdata                                                                                                                        |
| \\\tpgfile\NewCompany\IT\Department\QA Team\Current Projects\CRISP\CrispAutomation\CrispData\PG-Products\CI-HVS.xlsx |
