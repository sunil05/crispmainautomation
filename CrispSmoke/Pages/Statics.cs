﻿using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CrispSmoke.Pages
{
    public static class Statics
    {
        //public static ExcelPackage Excel { get; set; }       
        public static string DownloadFolder { get; set; }     
        public static List<string> Plots { get; set; }
        public static List<string> ProductNameList { get; set; }
        public static List<string> ConstructionType { get; set; }
        public static List<string> BCProducts { get; set; }
        //public static string CompanyName { get; set; }
        //public static string quoteId { get; set; }
        //public static string OrderNumber { get; set; }       
        public static string SiteRefNumber { get; set; }
        public static ExcelPackage Excel { get; set; }
        public static int CurrentExcelRow { get; set; }

    }
}
