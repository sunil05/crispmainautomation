﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TechTalk.SpecFlow;

namespace CrispSmoke.Steps
{
    [Binding]
    public sealed class AddCompanyGroupSteps : CrispAutomation.Support.Pages
    {
        [When(@"I click on administration button to select company groups option")]
        public void WhenIClickOnAdministrationButtonToSelectCompanyGroupsOption()
        {
            WaitForElement(Dashboardpage.AdministrationButton);
            Dashboardpage.AdministrationButton.Click();
            WaitForElement(Dashboardpage.CompanyGroupsOption);
            Dashboardpage.CompanyGroupsOption.Click();
            CloseSpinneronDiv();
            //Thread.Sleep(1000);
        }

        [When(@"I click on add button to create company group")]
        public void WhenIClickOnAddButtonToCreateCompanyGroup()
        {
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(500);
            WaitForElement(AddCompanyGroupPage.CreateCompanyGroupButton);
            //Thread.Sleep(500);
            AddCompanyGroupPage.CreateCompanyGroupButton.Click();
            //Thread.Sleep(1000);
        }

        [When(@"I perform the creating company group operation")]
        public void WhenIPerformTheCreatingCompanyGroupOperation()
        {
            WaitForElement(AddCompanyGroupPage.GroupNameLabel);
            AddCompanyGroupPage.CompanyGroupDetails();
        }

        [When(@"I search for the above created company group")]
        public void WhenISearchForTheAboveCreatedCompanyGroup()
        {
            WaitForElement(AddCompanyGroupPage.BackToSearchButton);
            AddCompanyGroupPage.SearchCreatedCompanyGroup();
        }

        [Then(@"above created company Group should be displayed")]
        public void ThenAboveCreatedCompanyGroupShouldBeDisplayed()
        {
            AddCompanyGroupPage.VerifyResults();
        }

        [Then(@"I perform the editing company group operation")]
        public void ThenIPerformTheEditingCompanyGroupOperation()
        {
            AddCompanyGroupPage.EditingCompanyGroup();
        }

        [Then(@"I perform discontinue company group operation")]
        public void ThenIPerformDiscontinueCompanyGroupOperation()
        {
            AddCompanyGroupPage.DiscontinueTheGroup();
        }

        [Then(@"I should verify discontinued company group")]
        public void ThenIShouldVerifyDiscontinuedCompanyGroup()
        {
            AddCompanyGroupPage.VerifyDiscontinuedGroup();
        }
    }
}
