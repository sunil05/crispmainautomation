﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TechTalk.SpecFlow;

namespace CrispSmoke.Steps
{
    [Binding]
    public sealed class FinanceSteps : CrispAutomation.Support.Pages
    {
        [When(@"I click on finances button")]
        public void WhenIClickOnFinancesButton()
        {
            WaitForElement(FinancesPage.Finances);
            FinancesPage.Finances.Click();
        }

        //Importing Bank Statements 

        [When(@"I Select the bank statement")]
        public void WhenISelectTheBankStatement()
        {
            WaitForElement(FinancesPage.BankStatement);
            FinancesPage.BankStatement.Click();
            WaitForElement(FinancesPage.BankStatementTitle);
            Assert.IsTrue(FinancesPage.BankStatementTitle.Displayed);
            WaitForElement(FinancesPage.FilterButton);
            FinancesPage.ApplyFilteronStatement();
        }

        [Then(@"I perform operation on statement import section")]
        public void ThenIPerformOperationOnStatementImportSection()
        {
            WaitForElement(FinancesPage.ImportButton);
            FinancesPage.BankStatementimportIBAAccount();
            //Thread.Sleep(1500);
            //WaitForElement(FinancesPage.ImportButton);
            //FinancesPage.BankStatementimportEscrowAccount();
            //Thread.Sleep(1500);
        }

        //Allocate Payment Steps

        [When(@"I Select the Allocate Payment Receipts option")]
        public void WhenISelectTheAllocatePaymentReceiptsOption()
        {
            WaitForElement(FinancesPage.AllocatePayments);
            FinancesPage.AllocatePayments.Click();
            WaitForElement(FinancesPage.AllocatePaymentsTitle);
            Assert.IsTrue(FinancesPage.AllocatePaymentsTitle.Displayed);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(500);
        }

        [When(@"I perform operation to allocate payments")]
        public void WhenIPerformOperationToAllocatePayments()
        {
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(500);
            WaitForElements(FinancesPage.PaymentList);
            if (FinancesPage.PaymentList.Count > 0)
            {
                WaitForElements(FinancesPage.PaymentList);
                FinancesPage.AllocatePayment();
            }
            else
            {
                Assert.IsTrue(FinancesPage.PaymentList.Count == 0, "No Payment List displayed to allocate payemts");
            }

        }

        [Then(@"I should see the payments has been allocated")]
        public void ThenIShouldSeeThePaymentsHasBeenAllocated()
        {
            CloseSpinneronDiv();
            CloseSpinneronPage();
            //Thread.Sleep(500);
            WaitForElement(FinancesPage.AllocateOkButton);
            WaitForElementToClick(FinancesPage.AllocateOkButton);
            FinancesPage.AllocateOkButton.Click();
            Console.WriteLine("Amount has been Allocated");
        }
        //Transfer Money Steps 

        [When(@"I Select the transfer money option")]
        public void WhenISelectTheTransferMoneyOption()
        {
            WaitForElement(FinancesPage.TransferMoneyOption);
            FinancesPage.TransferMoneyOption.Click();
            WaitForElement(FinancesPage.TransferMoneyDialogueWizard);
            Assert.IsTrue(FinancesPage.TransferMoneyDialogueWizard.Displayed);
        }

        [When(@"I perform operation to transfer the money")]
        public void WhenIPerformOperationToTransferTheMoney()
        {
            WaitForElement(FinancesPage.FromButton);
            FinancesPage.TransferMoney();
        }

        [Then(@"I should see the payments has been transfered")]
        public void ThenIShouldSeeThePaymentsHasBeenTransfered()
        {
            try
            {

                CloseSpinneronDiv();
                //Thread.Sleep(1000);
                WaitForElement(FinancesPage.TransferButton);
                FinancesPage.TransferButton.Click();
                //Thread.Sleep(1000);
                WaitForElement(FinancesPage.TransferOkButton);
                FinancesPage.TransferOkButton.Click();
                //Thread.Sleep(1000);
                WaitForElement(FinancesPage.TransferComplete);
                Assert.IsTrue(FinancesPage.TransferComplete.Displayed);
                Console.WriteLine("Amount has been transfered");
            }
            catch (Exception ex)
            {
                throw new Exception("Failed to transfer", ex);
            }
        }
    }
}
