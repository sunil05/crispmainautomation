﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TechTalk.SpecFlow;
using System.Threading;
using NUnit.Framework;
using OpenQA.Selenium;

namespace CrispSmoke.Steps
{
    [Binding]
    public class AddPersonSteps : CrispAutomation.Support.Pages
    {
        [When(@"I click on add button to create a person")]
        public void WhenIClickOnAddButtonToCreateAPerson()
        {
            WaitForElement(Dashboardpage.PlusButton);
            Dashboardpage.PlusButton.Click();
        }
        [When(@"Select Person Option")]
        public void WhenSelectPersonOption()
        {
            try
            {
                WaitForElement(Dashboardpage.PersonButton);
                Dashboardpage.PersonButton.Click();
                Thread.Sleep(500);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Failed To Display Add Person Option, Check User Permissions to Create Contact: {0}", ex);
            }
            
        }
        [When(@"I enter person details on details page")]
        public void WhenIEnterPersonDetailsOnDetailsPage()
        {           
            try
            {
                WaitForElement(AddPersonpage.Salutation);
                AddPersonpage.FillDetails();
            }
            catch (Exception ex)
            {
                Console.WriteLine("Failed To Add Data to Details Page : {0}", ex);
            }
        }
        [When(@"I enter address details on addresses page")]
        public void WhenIEnterAddressDetailsOnAddressesPage()
        {
            try
            {
                WaitForElement(AddPersonpage.AddressButton);
                AddPersonpage.AddAddress();
            }
            catch (Exception ex)
            {
                Console.WriteLine("Failed To Add Address Details to Addresses Page : {0}", ex);
            }
          
        }
        [When(@"I enter socialmedia details on social page")]
        public void WhenIEnterSocialmediaDetailsOnSocialPage()
        {
            try
            {
                WaitForElement(AddPersonpage.FaceBookId);
                AddPersonpage.AddSocialInfo();
            }
            catch (Exception ex)
            {
                Console.WriteLine("Failed To Add Social Media Details on Social Page : {0}", ex);
            }
       
        }
        [When(@"I click on save button to save person details")]
        public void WhenIClickOnSaveButtonToSavePersonDetails()
        {
            try
            {
                WaitForElement(AddPersonpage.SaveButton);
                AddPersonpage.SaveButton.Click();
            }
            catch (Exception ex)
            {
                Console.WriteLine("Failed To Save Person : {0}", ex);
            }
          
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
        }
        [Then(@"I should see person details on the dashboard")]
        public void ThenIShouldSeePersonDetailsOnTheDashboard()
        {
            try
            {
                WaitForElement(AddPersonpage.Imageperson);
                Thread.Sleep(500);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                Thread.Sleep(500);
                Assert.IsTrue(AddPersonpage.Imageperson.Displayed);
                Dashboardpage.ContactGuidId();
            }
            catch (Exception ex)
            {
                Console.WriteLine("Failed To Retrive Person Details: {0}", ex);
            }
          
        }
        [When(@"I click on edit button to edit person details")]
        public void WhenIClickOnEditButtonToEditPersonDetails()
        {
            try
            {
                WaitForElement(EditPersonDetailsPage.EditPersonButton);
                EditPersonDetailsPage.EditPersonButton.Click();
            }
            catch (Exception ex)
            {
                Console.WriteLine("Failed To Display Edit Button Check User Permissions to Amend Contact: {0}", ex);
            }
          
        }
        [When(@"I edit the person details on details page")]
        public void WhenIEditThePersonDetailsOnDetailsPage()
        {
            try
            {
                WaitForElement(EditPersonDetailsPage.SalutationFieldEdit);
                EditPersonDetailsPage.EditPersonetails();
            }
            catch (Exception ex)
            {
                Console.WriteLine("Failed To Amend Data to Details Page : {0}", ex);
            }
          
        }
        [When(@"I edit the existing address details and add new address details")]
        public void WhenIEditTheExistingAddressDetailsAndAddNewAddressDetails()
        {
            try
            {
                WaitForElement(EditPersonDetailsPage.DefaultAddress);
                EditPersonDetailsPage.EditAddressDetails();
            }
            catch (Exception ex)
            {
                Console.WriteLine("Failed To Amend Address Details On Addresses Page : {0}", ex);
            }
            try
            {
                WaitForElement(EditPersonDetailsPage.AddressButton);
                EditPersonDetailsPage.AddNewAddressDetails();
            }
            catch (Exception ex)
            {
                Console.WriteLine("Failed To Add New Address Details On Addresses Page : {0}", ex);
            }

        
        }
        [When(@"I edit the socialmedia details on social page")]
        public void WhenIEditTheSocialmediaDetailsOnSocialPage()
        {
            WaitForElement(EditPersonDetailsPage.NextButton);
            EditPersonDetailsPage.NextButton.Click();
            try
            {               
                Thread.Sleep(500);
                CloseSpinneronDiv();
                WaitForElement(EditPersonDetailsPage.EditFacebookInput);
                Thread.Sleep(500);
                CloseSpinneronDiv();
                EditPersonDetailsPage.EditSocialInfo();
            }
            catch (Exception ex)
            {
                Console.WriteLine("Failed To Amend Social Media Details On Social Page : {0}", ex);
            }
         
        }

        [When(@"I click on save button to update the person details")]
        public void WhenIClickOnSaveButtonToUpdateThePersonDetails()
        {
            try
            {
                WaitForElement(EditPersonDetailsPage.SaveButton);
                WaitForElement(EditPersonDetailsPage.SaveButton);
                EditPersonDetailsPage.SaveButton.Click();
                Thread.Sleep(1000);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                Thread.Sleep(500);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Failed To Save Amend Person Details : {0}", ex);
            }
          
           
        }
        [Then(@"I should see updated contact details on the dashboard")]
        public void ThenIShouldSeeUpdatedContactDetailsOnTheDashboard()
        {
            WaitForElement(EditPersonDetailsPage.Imageperson);
            Thread.Sleep(500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            Assert.IsTrue(EditPersonDetailsPage.Imageperson.Displayed);
        }


        [When(@"I click on rating option to provide rating to a person")]
        public void WhenIClickOnRatingOptionToProvideRatingToAPerson()
        {
            try
            {
                WaitForElement(AddRatingToPersonPage.RatingOption);
                AddRatingToPersonPage.RatingOption.Click();
                Thread.Sleep(500);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Failed To Click on Rating Tab  : {0}", ex);
            }
          
        }

        [When(@"I select add rating button on person page from top right corner")]
        public void WhenISelectAddRatingButtonOnPersonPageFromTopRightCorner()
        {
            try
            {
                WaitForElement(AddRatingToPersonPage.AddRatingButton);
                AddRatingToPersonPage.AddRatingButton.Click();
                Thread.Sleep(500);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Add Rating Button is Not Displayed : {0}", ex);
            }
           
        }
        [When(@"I provide the details on details page")]
        public void WhenIProvideTheDetailsOnDetailsPage()
        {
            try
            {
                WaitForElement(AddRatingToPersonPage.RatingDialogue);
                WaitForElement(AddRatingToPersonPage.DetailsOption);
                AddRatingToPersonPage.DetailsPage();
            }
            catch (Exception ex)
            {
                Console.WriteLine("Failed To Add Data On Details Page: {0}", ex);
            }       
           
        }
        [When(@"I provide the details on warranty providers page")]
        public void WhenIProvideTheDetailsOnWarrantyProvidersPage()
        {

            try
            {
                WaitForElement(AddRatingToPersonPage.WarrentyProvider);
                AddRatingToPersonPage.WarrantyDetails();
            }
            catch (Exception ex)
            {
                Console.WriteLine("Failed To Add Warrenty Providers Details: {0}", ex);
            }
           
        }
        [When(@"I provide the details on developments page")]
        public void WhenIProvideTheDetailsOnDevelopmentsPage()
        {
            try
            {
                WaitForElement(AddRatingToPersonPage.Developments);
                AddRatingToPersonPage.DevelopmentDetails();
            }
            catch (Exception ex)
            {
                Console.WriteLine("Failed To Add Development Details: {0}", ex);
            }
         
        }
        [When(@"I provide the details on prospective business page")]
        public void WhenIProvideTheDetailsOnProspectiveBusinessPage()
        {
            try
            {
                WaitForElement(AddRatingToPersonPage.ProspectiveBusiness);
                AddRatingToPersonPage.ProspectiveBusinessDetails();
            }
            catch (Exception ex)
            {
                Console.WriteLine("Failed To Add Prospective Business Details: {0}", ex);
            }
          
        }
        [When(@"I provide the details on claims history page")]
        public void WhenIProvideTheDetailsOnClaimsHistoryPage()
        {
            try
            {
                WaitForElement(AddRatingToPersonPage.ClaimHistory);
                AddRatingToPersonPage.ClaimsHistoryDetails();
            }
            catch (Exception ex)
            {
                Console.WriteLine("Failed To Add Claims History Details: {0}", ex);
            }
            
        }
        [When(@"I provide the details on scores details page")]
        public void WhenIProvideTheDetailsOnScoresDetailsPage()
        {
            try
            {
                WaitForElement(AddRatingToPersonPage.ScoresOption);
                AddRatingToPersonPage.ScoresDetails();
            }
            catch (Exception ex)
            {
                Console.WriteLine("Failed To Add Scores Details: {0}", ex);
            }
          
        }
        [When(@"I click on the confirm rating button on the wizard")]
        public void WhenIClickOnTheConfirmRatingButtonOnTheWizard()
        {
            try
            {
                WaitForElement(AddRatingToPersonPage.ConfirmRatingButton);
                AddRatingToPersonPage.ConfirmRatingButton.Click();
                Thread.Sleep(1000);
                CloseSpinneronDiv();
                CloseSpinneronPage();
                Thread.Sleep(1000);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Failed To Save Rating Details To Person: {0}", ex);
            }
         
        }
        [Then(@"I should see the rating details for person")]
        public void ThenIShouldSeeTheRatingDetailsForPerson()
        {
            WaitForElement(AddRatingToPersonPage.Imageperson);
            Assert.IsTrue(AddRatingToPersonPage.Imageperson.Displayed);
            try
            {
                if (!AddRatingToPersonPage.RatingSummaryDisplayed.Displayed)
                {
                    // Driver.Navigate().Refresh();
                    WaitForElement(AddRatingToPersonPage.RatingOption);
                    AddRatingToPersonPage.RatingOption.Click();
                    WaitForElement(AddRatingToPersonPage.RatingSummaryDisplayed);
                    Assert.IsTrue(AddRatingToPersonPage.RatingSummaryDisplayed.Displayed);
                }
                else
                {
                    WaitForElement(AddRatingToPersonPage.RatingSummaryDisplayed);
                    Assert.IsTrue(AddRatingToPersonPage.RatingSummaryDisplayed.Displayed);

                }
            }
            catch (NoSuchElementException)
            {
                // Driver.Navigate().Refresh();
                WaitForElement(AddRatingToPersonPage.RatingOption);
                AddRatingToPersonPage.RatingOption.Click();
                WaitForElement(AddRatingToPersonPage.RatingSummaryDisplayed);
                Assert.IsTrue(AddRatingToPersonPage.RatingSummaryDisplayed.Displayed);
            }
        }

        //Edit Rating To A Person

        [When(@"I click on rating option to edit rating details to person")]
        public void WhenIClickOnRatingOptionToEditRatingDetailsToPerson()
        {
            WaitForElement(EditRatingToPersonPage.RatingOption);
            EditRatingToPersonPage.RatingOption.Click();
            Thread.Sleep(500);
        }

        [When(@"I select edit rating button on person rating page")]
        public void WhenISelectEditRatingButtonOnPersonRatingPage()
        {
            WaitForElement(EditRatingToPersonPage.EditRating);
            Thread.Sleep(500);
            EditRatingToPersonPage.EditRating.Click();
            WaitForElement(EditRatingToPersonPage.RatingDialogue);
            Assert.IsTrue(EditRatingToPersonPage.RatingDialogue.Displayed);
            Thread.Sleep(500);
        }

        [When(@"I edit the details on rating details page")]
        public void WhenIEditTheDetailsOnRatingDetailsPage()
        {
            WaitForElement(EditRatingToPersonPage.DetailsOption);
            EditRatingToPersonPage.EditDetailsPage();
        }
        [When(@"I edit the details on warranty providers page")]
        public void WhenIEditTheDetailsOnWarrantyProvidersPage()
        {
            WaitForElement(EditRatingToPersonPage.WarrentyProvider);
            EditRatingToPersonPage.WarrantyDetails();
        }

        [When(@"I edit the details on developments page")]
        public void WhenIEditTheDetailsOnDevelopmentsPage()
        {
            WaitForElement(EditRatingToPersonPage.Developments);
            EditRatingToPersonPage.EditDevelopmentDetails();
            WaitForElement(EditRatingToPersonPage.Developments);
            EditRatingToPersonPage.NewDevelopmentDetails();
        }
        [When(@"I edit the details on prospective business page")]
        public void WhenIEditTheDetailsOnProspectiveBusinessPage()
        {
            WaitForElement(EditRatingToPersonPage.ProspectiveBusiness);
            EditRatingToPersonPage.EditProspectiveBusinessDetails();
            WaitForElement(EditRatingToPersonPage.ProspectiveBusiness);
            EditRatingToPersonPage.NewProspectiveBusinessDetails();
        }
        [When(@"I edit the details on claims history page")]
        public void WhenIEditTheDetailsOnClaimsHistoryPage()
        {
            WaitForElement(EditRatingToPersonPage.ClaimHistory);
            EditRatingToPersonPage.EditClaimsHistoryDetails();
            WaitForElement(EditRatingToPersonPage.AddHistoricClaimsButton);
            EditRatingToPersonPage.NewClaimsHistoryDetails();

        }
        [When(@"I edit the details on scores details page")]
        public void WhenIEditTheDetailsOnScoresDetailsPage()
        {

            WaitForElement(EditRatingToPersonPage.ScoresOption);
            EditRatingToPersonPage.EditScoresDetails();
        }
        [When(@"I click on the confirm rating button to update amended rating details")]
        public void WhenIClickOnTheConfirmRatingButtonToUpdateAmendedRatingDetails()
        {
            WaitForElement(AddRatingToPersonPage.ConfirmRatingButton);
            AddRatingToPersonPage.ConfirmRatingButton.Click();
            Thread.Sleep(1000);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(500);
            WaitForElement(AddRatingToPersonPage.Imageperson);
            Assert.IsTrue(AddRatingToPersonPage.Imageperson.Displayed);
            Thread.Sleep(1000);
            WaitForElement(AddRatingToPersonPage.RatingOption);
            AddRatingToPersonPage.RatingOption.Click();
        }
        [Then(@"I should see updated rating details for the person")]
        public void ThenIShouldSeeUpdatedRatingDetailsForThePerson()
        {
            WaitForElement(EditRatingToPersonPage.RatingSummaryDisplayed);
            Assert.IsTrue(EditRatingToPersonPage.RatingSummaryDisplayed.Displayed);
            Thread.Sleep(500);
        }
        [Given(@"I have selected a person")]
        public void GivenIHaveSelectedAPerson()
        {            
            Dashboardpage.SelectPerson();
        }
    }
}
