﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using TechTalk.SpecFlow;

namespace CrispSmoke.Steps
{
    [Binding]
    public sealed class SurveyorAssessmentsSteps : CrispAutomation.Support.Pages
    {

        [When(@"I login to surveyor site")]
        public void WhenILoginToSurveyorSite()
        {
            SurveyorLoginPage.SurveyorLoginMethod();
        }

        [When(@"I select the one of the site from the list")]
        public void WhenISelectTheOneOfTheSiteFromTheList()
        {
            SurveyorLoginPage.SelectSite();
        }

        [When(@"I click on site risk assessment option")]
        public void WhenIClickOnSiteRiskAssessmentOption()
        {
            WaitForElementonSurveyor(SiteRiskAssessmentPage.SiteRiskAssessmentOption);
            //Thread.Sleep(500);
            CloseSpinnerOnSurveyorPage();
            //Thread.Sleep(500);
            SiteRiskAssessmentPage.SiteRiskAssessmentOption.Click();
            //Thread.Sleep(500);
            CloseSpinnerOnSurveyorPage();
            //Thread.Sleep(500);
            WaitForElementonSurveyor(SiteRiskAssessmentPage.SiteRiskSection);
            Assert.IsTrue(SiteRiskAssessmentPage.SiteRiskSection.Displayed);
            Assert.IsTrue(SiteRiskAssessmentPage.EditButton.Displayed, "No permissions applied to the user to submit SiteRisk Assessment");
        }


        [When(@"I Click on edit button to submit the site risk assessment")]
        public void WhenIClickOnEditButtonToSubmitTheSiteRiskAssessment()
        {
            //Thread.Sleep(1000);
            WaitForElementonSurveyor(SiteRiskAssessmentPage.EditButton);
            SiteRiskAssessmentPage.EditButton.Click();
            //Thread.Sleep(500);
            CloseSpinnerOnSurveyorPage();
            //Thread.Sleep(500);
            WaitForElementonSurveyor(SiteRiskAssessmentPage.AlertInfoOnEdit);
            Assert.IsTrue(SiteRiskAssessmentPage.AlertInfoOnEdit.Displayed);
            SiteRiskAssessmentPage.EditSiteRiSkAssessmentSection();
            //Thread.Sleep(500);
            CloseSpinnerOnSurveyorPage();
            //Thread.Sleep(500);
            WaitForElementonSurveyor(SiteRiskAssessmentPage.AcceptableRiskRadioButton);
            SiteRiskAssessmentPage.AcceptableRiskRadioButton.Click();
            //Thread.Sleep(500);
            CloseSpinnerOnSurveyorPage();
            //Thread.Sleep(500);
            WaitForElementonSurveyor(SiteRiskAssessmentPage.SubmitButton);
            SiteRiskAssessmentPage.SubmitButton.Click();
            //Thread.Sleep(500);
            CloseSpinnerOnSurveyorPage();
            //Thread.Sleep(500);
            WaitForElementonSurveyor(SiteRiskAssessmentPage.SendButton);
            SiteRiskAssessmentPage.SendButton.Click();
            //Thread.Sleep(500);
            CloseSpinnerOnSurveyorPage();
            //Thread.Sleep(500);
        }
        [When(@"I Click on edit button to submit the site risk assessment with no options")]
        public void WhenIClickOnEditButtonToSubmitTheSiteRiskAssessmentWithNoOptions()
        {

            //Thread.Sleep(1000);
            WaitForElementonSurveyor(SiteRiskAssessmentPage.EditButton);
            SiteRiskAssessmentPage.EditButton.Click();
            //Thread.Sleep(500);
            CloseSpinnerOnSurveyorPage();
            //Thread.Sleep(500);
            WaitForElementonSurveyor(SiteRiskAssessmentPage.AlertInfoOnEdit);
            Assert.IsTrue(SiteRiskAssessmentPage.AlertInfoOnEdit.Displayed);
            SiteRiskAssessmentPage.EditSRAWhenNoSelected();
            //Thread.Sleep(500);
            CloseSpinnerOnSurveyorPage();
            //Thread.Sleep(500);
            WaitForElementonSurveyor(SiteRiskAssessmentPage.AcceptableRiskRadioButton);
            SiteRiskAssessmentPage.AcceptableRiskRadioButton.Click();
            //Thread.Sleep(500);
            CloseSpinnerOnSurveyorPage();
            //Thread.Sleep(500);
            WaitForElementonSurveyor(SiteRiskAssessmentPage.SubmitButton);
            SiteRiskAssessmentPage.SubmitButton.Click();
            //Thread.Sleep(500);
            CloseSpinnerOnSurveyorPage();
            //Thread.Sleep(500);
            WaitForElementonSurveyor(SiteRiskAssessmentPage.SendButton);
            SiteRiskAssessmentPage.SendButton.Click();
            //Thread.Sleep(500);
            CloseSpinnerOnSurveyorPage();
            //Thread.Sleep(500);
        }


        [Then(@"Siterisk assessment should be submitted successfully")]
        public void ThenSiteriskAssessmentShouldBeSubmittedSuccessfully()
        {
            WaitForElementonSurveyor(SiteRiskAssessmentPage.SiteRiskSection);
            Assert.IsTrue(SiteRiskAssessmentPage.SiteRiskSection.Displayed, "Failed to send correspondence on Site Risk Assessment");
        }

        //Refurbishment Assessment Section


        [When(@"I click on the refurbishment risk assessment option")]
        public void WhenIClickOnTheRefurbishmentRiskAssessmentOption()
        {
            WaitForElementonSurveyor(RefurbishmentAssessmentPage.RefurbishmentAssessmentOption);
            RefurbishmentAssessmentPage.RefurbishmentAssessmentOption.Click();
        }
        [When(@"I click on edit button to submit the refurbishment risk assessment")]
        public void WhenIClickOnEditButtonToSubmitTheRefurbishmentRiskAssessment()
        {
            WaitForElementonSurveyor(RefurbishmentAssessmentPage.EditButton);
            RefurbishmentAssessmentPage.EditButton.Click();
            //Thread.Sleep(500);
            CloseSpinnerOnSurveyorPage();
            //Thread.Sleep(500);
            WaitForElementonSurveyor(RefurbishmentAssessmentPage.AlertStatusonEdit);
            Assert.IsTrue(RefurbishmentAssessmentPage.AlertStatusonEdit.Displayed);
            RefurbishmentAssessmentPage.EditRefurbishmentAssessmentSection();
            WaitForElementonSurveyor(RefurbishmentAssessmentPage.SubmitButton);
            RefurbishmentAssessmentPage.SubmitButton.Click();
            //Thread.Sleep(500);
            CloseSpinnerOnSurveyorPage();
            //Thread.Sleep(500);
            WaitForElementonSurveyor(SiteRiskAssessmentPage.SendButton);
            SiteRiskAssessmentPage.SendButton.Click();
            //Thread.Sleep(500);
            CloseSpinnerOnSurveyorPage();
            //Thread.Sleep(500);
        }

        [Then(@"Refurbishment assessment should be submitted successfully")]
        public void ThenRefurbishmentAssessmentShouldBeSubmittedSuccessfully()
        {
            WaitForElementonSurveyor(SiteRiskAssessmentPage.RefurbishmentSection);
            Assert.IsTrue(SiteRiskAssessmentPage.RefurbishmentSection.Displayed, "Failed to send correspondence on Refurbishment Assessment");

        }

        [When(@"I click on edit button to submit the refurbishment risk assessment with no options")]
        public void WhenIClickOnEditButtonToSubmitTheRefurbishmentRiskAssessmentWithNoOptions()
        {
            WaitForElementonSurveyor(RefurbishmentAssessmentPage.EditButton);
            RefurbishmentAssessmentPage.EditButton.Click();
            //Thread.Sleep(500);
            CloseSpinnerOnSurveyorPage();
            //Thread.Sleep(500);
            WaitForElementonSurveyor(RefurbishmentAssessmentPage.AlertStatusonEdit);
            Assert.IsTrue(RefurbishmentAssessmentPage.AlertStatusonEdit.Displayed);
            RefurbishmentAssessmentPage.EditRefurbishmentAssessmentWhenNoSelected();
            WaitForElementonSurveyor(RefurbishmentAssessmentPage.SubmitButton);
            RefurbishmentAssessmentPage.SubmitButton.Click();
            //Thread.Sleep(500);
            CloseSpinnerOnSurveyorPage();
            //Thread.Sleep(500);
            WaitForElementonSurveyor(SiteRiskAssessmentPage.SendButton);
            SiteRiskAssessmentPage.SendButton.Click();
            //Thread.Sleep(500);
            CloseSpinnerOnSurveyorPage();
            //Thread.Sleep(500);
        }


        // Techinical inspection Page
        [When(@"I click on site inspection option to add inspections")]
        public void WhenIClickOnSiteInspectionOptionToAddInspections()
        {

            SiteInspectionPage.InspectionUpdateMain();
        }

        [Then(@"site inspection should be added successfully")]
        public void ThenSiteInspectionShouldBeAddedSuccessfully()
        {
            //WaitForElementonSurveyor(SiteInspectionPage.AlertStatus);
            //Assert.IsTrue(SiteInspectionPage.AlertStatus.Displayed);
            Debug.WriteLine("Site Inspections Submitted Successfully");
        }

        //Design Review Page 
        [When(@"I click on Design Review option")]
        public void WhenIClickOnDesignReviewOption()
        {
            WaitForElementonSurveyor(DesignReviewPage.DesignReviewOption);
            DesignReviewPage.DesignReviewOption.Click();
            //Thread.Sleep(500);
            CloseSpinnerOnSurveyorPage();
            //Thread.Sleep(500);
        }
        [When(@"I Click on edit button to submit design review")]
        public void WhenIClickOnEditButtonToSubmitDesignReview()
        {
            WaitForElementonSurveyor(DesignReviewPage.EditButton);
            //Thread.Sleep(500);
            CloseSpinnerOnSurveyorPage();
            //Thread.Sleep(500);
            DesignReviewPage.EditButton.Click();
            //Thread.Sleep(500);
            CloseSpinnerOnSurveyorPage();
            //Thread.Sleep(500);
            DesignReviewPage.DesignReviewAssessment();
            //Thread.Sleep(500);
            CloseSpinnerOnSurveyorPage();
            //Thread.Sleep(500);

        }
        [Then(@"design review assessment should be submitted successfully")]
        public void ThenDesignReviewAssessmentShouldBeSubmittedSuccessfully()
        {
            DesignReviewPage.SubmitDR();
            //Thread.Sleep(500);
            CloseSpinnerOnSurveyorPage();
            //Thread.Sleep(500);
            WaitForElementonSurveyor(DesignReviewPage.DesignReviewMainPage);
            Assert.IsTrue(DesignReviewPage.DesignReviewMainPage.Displayed);
        }
        //Engineer Review 
        [When(@"I click on Engineer Review option")]
        public void WhenIClickOnEngineerReviewOption()
        {
            WaitForElementonSurveyor(EngineerReviewPage.EngineerReviewOption);
            EngineerReviewPage.EngineerReviewOption.Click();
            //Thread.Sleep(500);
            CloseSpinnerOnSurveyorPage();
            //Thread.Sleep(500);
        }
        [When(@"I Click on edit button to engineer review")]
        public void WhenIClickOnEditButtonToEngineerReview()
        {
            WaitForElementonSurveyor(EngineerReviewPage.EditButton);
            //Thread.Sleep(500);
            CloseSpinnerOnSurveyorPage();
            //Thread.Sleep(500);
            EngineerReviewPage.EditButton.Click();
            //Thread.Sleep(500);
            CloseSpinnerOnSurveyorPage();
            //Thread.Sleep(500);
            EngineerReviewPage.EngineerReviewAssessment();
        }
        [Then(@"engineer review assessment should be submitted successfully")]
        public void ThenEngineerReviewAssessmentShouldBeSubmittedSuccessfully()
        {
            EngineerReviewPage.SubmitER();
            //Thread.Sleep(500);
            CloseSpinnerOnSurveyorPage();
            //Thread.Sleep(500);
            WaitForElementonSurveyor(EngineerReviewPage.EngineerReviewMainPage);
            Assert.IsTrue(EngineerReviewPage.EngineerReviewMainPage.Displayed);
        }
        [When(@"I create new covernote")]
        public void WhenICreateNewCovernote()
        {
            CoverNotesPage.VerifyCoverNoteErrors();
            CoverNotesPage.CreatingCoverNote();
        }
        [Then(@"the covernote should be added successfully")]
        public void ThenTheCovernoteShouldBeAddedSuccessfully()
        {

        }
        //Submit Surveyor Assessments 
        [When(@"I login to surveyor site to submit surveyor assessments")]
        public void WhenILoginToSurveyorSiteToSubmitSurveyorAssessments()
        {

            SurveyorAssessments.SurveyorAssessment();
        }

        [Then(@"surveyor assessment should be submitted successfully")]
        public void ThenSurveyorAssessmentShouldBeSubmittedSuccessfully()
        {
            Debug.WriteLine("Surveyor Assessments Are Submitted Successfully");
        }

    }
}
