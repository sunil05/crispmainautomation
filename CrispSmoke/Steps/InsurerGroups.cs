﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TechTalk.SpecFlow;

namespace CrispSmoke.Steps
{
    [Binding]
    public sealed class InsurerGroups : CrispAutomation.Support.Pages
    {
        [When(@"I provide AXA HVS insurer on key site details page for PG Brand")]
        public void WhenIProvideAXAHVSInsurerOnKeySiteDetailsPageForPGBrand()
        {
            AddPGQuotePage.PGKeySiteDetailsSelectAXHVSInsurer();
        }
        [When(@"I provide Sompo Canopius Insurer on key site details page for PG Brand")]
        public void WhenIProvideSompoCanopiusInsurerOnKeySiteDetailsPageForPGBrand()
        {
            AddPGQuotePage.PGKeySiteDetailsSelectSompoCanopiusInsurer();

        }
        [When(@"I provide AXA HVS insurer on key site details pag")]
        public void WhenIProvideAXAHVSInsurerOnKeySiteDetailsPag()
        {
            AddLABCQuotePage.KeySiteDetailsSelectAXAHVSsInsurer();
        }

        [When(@"I provide Sompo Canopius Insurer on key site details page")]
        public void WhenIProvideSompoCanopiusInsurerOnKeySiteDetailsPage()
        {
            AddLABCQuotePage.KeySiteDetailsSelectSompoCanopiusInsurer();
        }

    }
}
