﻿using CrispAutomation.Features;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using TechTalk.SpecFlow;
using CrispAutomation.Support;


namespace CrispSmoke.Steps
{
    [Binding]
    public sealed class ConditionsSteps : Support.Pages
    {
        //Clearing conditions to issue certificates -Make balance payment
        [When(@"I verify account and make the balance payments")]
        public void WhenIVerifyAccountAndMakeTheBalancePayments()
        {
            IssueCertificatePage.PayFeeBalance();
        }
        //Clearing conditions to issue certificates  - Upload Security Documents
        [When(@"I clear security document conditions")]
        public void WhenIClearSecurityDocumentConditions()
        {

            IssueCertificatePage.SecurityDocsUpload();
        }

        //Clearing conditions to issue certificates  - Make Registration Fee

        [When(@"I clear registration fee conditions")]
        public void WhenIClearregistrationFeeConditions()
        {

            IssueCertificatePage.RegistrationFee();

        }

        //Clearing Manual Conditions to issue certificates  on HVS products

        [When(@"I clear manual conditions")]
        public void WhenIClearManualConditions()
        {
            if (Statics.ProductName.Contains("High Value"))
            {
                Thread.Sleep(1000);
                if (IssueCertificatePage.ConditionsRows.Count <= 0)
                {
                    Dashboardpage.SelectOrder();
                }
                WaitForElement(IssueCertificatePage.ConditionsTab);
                IssueCertificatePage.ConditionsTab.Click();
                Thread.Sleep(500);
                CloseSpinneronPage();
                CloseSpinneronDiv();
                Thread.Sleep(500);
                var conditions = IssueCertificatePage.ConditionsRows.Count > 0;
                Thread.Sleep(500);
                CloseSpinneronPage();
                CloseSpinneronDiv();
                Thread.Sleep(500);
                if (conditions == true)
                {
                    WaitForElements(IssueCertificatePage.ConditionsRows);

                    if (ManageConditionsPage.ManualConditionsList.Count > 0)
                    {
                        ManageConditionsPage.ClosingCondition();
                        Thread.Sleep(1000);
                    }
                }
            }
        }

        //Clearing conditions to assign property owner  on self build product 
        [When(@"I clear LABC additional conditions")]
        public void WhenIClearLABCAdditionalConditions()
        {
            if (Statics.ProductNameList.Any(o => o.Contains("Self Build")) || Statics.ProductNameList.Any(o => o.Contains("Commercial")) || Statics.ProductNameList.Any(o => o.Contains("Commercial - High Value")) || Statics.ProductNameList.Any(o => o.Contains("Completed Housing")))
            {
                CloseAdditionalConditions.LABCAdditionalConditions();
            }
        }

        //Clearing PG productconditions to assign property owner  on self build product 
        [When(@"I clear PG additional conditions")]
        public void WhenIClearPGAdditionalConditions()
        {
            CloseAdditionalConditions.PGAdditionalConditions();        
        }
        [When(@"I clear intial notice condition")]
        public void WhenIClearIntialNoticeCondition()
        {
            CloseAdditionalConditions.CloseIntialNoticeCondtions();
        }

        [When(@"I clear final notice conditions")]
        public void WhenIClearFinalNoticeConditions()
        {
            CloseAdditionalConditions.CloseFinalNoticeCondtions();
        }

        //Issue COI
        [When(@"I login back to crisp application to issue the COI")]
        public void WhenILoginBackToCrispApplicationToIssueTheCOI()
        {
            Thread.Sleep(1000);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(1000);
            Dashboardpage.CrispLoginMethod();
            Thread.Sleep(1000);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(1000);
            IssueCertificatePage.IssueCOI();
            Thread.Sleep(2000);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            Thread.Sleep(2000);
            WaitForElement(Dashboardpage.QuoteApplication);
            Assert.IsTrue(Dashboardpage.QuoteApplication.Displayed, $"User unable to issue the COI for following {Statics.ProductName}");
        }

        [Then(@"I should download COI document and compare the document")]
        public void ThenIShouldDownloadCOIDocumentAndCompareTheDocument()
        {
            DocumentComparisionPage.COIDocComparision();
        }

        [Then(@"I login back to crisp application to issue the DevIC")]
        public void ThenILoginBackToCrispApplicationToIssueTheDevIC()
        {
            if (Statics.ProductNameList.Any(o => o.Equals("Social Housing")) || Statics.ProductNameList.Any(o => o.Equals("Social Housing - High Value")) || Statics.ProductNameList.Any(o => o.Equals("Private Rental")) || Statics.ProductNameList.Any(o => o.Equals("Private Rental - High Value")))
            {
                IssueCertificatePage.IssueDevIC();
            }
            Thread.Sleep(1500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            WaitForElement(Dashboardpage.QuoteApplication);
            Assert.IsTrue(Dashboardpage.QuoteApplication.Displayed);
        }

        [Then(@"I login back to crisp application to issue the plotIC")]
        public void ThenILoginBackToCrispApplicationToIssueThePlotIC()
        {
            if (Statics.ProductNameList.Any(o => o.Equals("New Homes")) || Statics.ProductNameList.Any(o => o.Equals("New Homes - High Value")))
            {
                IssueCertificatePage.IssuePlotIC();
            }
            Thread.Sleep(1500);
            CloseSpinneronDiv();
            CloseSpinneronPage();
            WaitForElement(Dashboardpage.QuoteApplication);
            Assert.IsTrue(Dashboardpage.QuoteApplication.Displayed);
        }
    }
}
